<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="success" type="java.lang.String"--%>
<%--@elvariable id="error" type="java.lang.String"--%>
<%--@elvariable id="info" type="java.lang.String"--%>
<%--@elvariable id="user" type="net.grouplink.ehelpdesk.domain.User"--%>
<%--@elvariable id="rc" type="org.springframework.web.servlet.support.RequestContext"--%>

<c:set var="configSidebar">
    <jsp:attribute name="value">
        <decorator:getProperty property="meta.configSidebar"/>
    </jsp:attribute>
</c:set>

<c:set var="assetSidebar">
    <jsp:attribute name="value">
        <decorator:getProperty property="meta.assetSidebar"/>
    </jsp:attribute>
</c:set>

<!DOCTYPE html>
<html>
<head>
    <title><spring:message code="global.systemName"/> <decorator:title/></title>

    <%@ include file="/WEB-INF/jsp/includes/theme_styles_js.jsp" %>
    <script type="text/javascript" src="<c:url value="/js/underscore-min.js"/>"></script>

    <link rel="stylesheet" href="<c:url value="/js/dojo-1.6.2/dijit/themes/tundra/tundra.css"/>" type="text/css">
    <link rel="shortcut icon" href="<c:url value="/images/favicon.ico"/>" type="image/x-icon"/>

    <%-- uncomment to debug dojo errors --%>
    <%--<script type="text/javascript" src="<c:url value="/js/dojo-1.6.2/dojo/dojo.js.uncompressed.js"/>"--%>
    <script type="text/javascript" src="<c:url value="/js/dojo-1.6.2/dojo/dojo.js"/>"
            djConfig="locale: '<tags:dojoLocale locale="${rc.locale}" />' ">
    </script>

    <%-- uncomment to debug dojo errors --%>
    <%--<script type="text/javascript" src="<c:url value="/js/dojo-1.6.2/grouplink/includes.js.uncompressed.js"/>"></script>--%>
    <script type="text/javascript" src="<c:url value="/js/dojo-1.6.2/grouplink/includes.js"/>"></script>


    <script type="text/javascript" src="<c:url value="/js/scripts.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/cookies.js"/>"></script>
    <%@ include file="/WEB-INF/jsp/main.js.jsp" %>

    <script type="text/javascript">
        function newTicket(contactId) {
            var url = "<c:url value="/tickets/new"/>";
            if(contactId) {
                url += '?contactId=' + contactId;
            }
            openWindow(url, '_blank', '800px', '850px');
        }

        dojo.addOnLoad(function() {
            dojo.parser.parse();

            _.templateSettings = {
                interpolate: /\{\{(.+?)\}\}/g
            };

            var newTicketTab = dojo.query("#newticket > a")[0];
            if (newTicketTab) {
                dojo.connect(newTicketTab, "onclick", function() {
                    newTicket(null);
                    <%--var contactId = null;--%>
                    <%--<authz:authorize ifNotGranted="ROLE_MANAGER, ROLE_TECHNICIAN" >--%>
                    <%--contactId = <c:out value="${user.id}" default="null"/>;--%>
                    <%--</authz:authorize>--%>
                    <%--newTicket(contactId);--%>
                });
            }

            // Session Timeout
            var seshunLength = 1000 * ${pageContext.session.maxInactiveInterval};
            var warningLength = seshunLength - (1000 * 60); // One minute before actual timeout

            window.setTimeout(showTimeoutWarning, warningLength);
            var seshTimer = window.setTimeout(sessionTimedOut, seshunLength);

            function showTimeoutWarning(){
                dijit.byId("timeoutWarningDialog").show();
            }

            function sessionTimedOut(){
                location.replace("<c:url value="/login.glml"><c:param name="login_error" value="3"/></c:url>");
            }

            dojo.connect(dojo.byId("logoutBtn"), "onclick", function(){
                dijit.byId("timeoutWarningDialog").hide();
                location.replace("<c:url value="/logout.glml"/>");
            });
            dojo.connect(dojo.byId("stayConnectedBtn"), "onclick", function(){
                dijit.byId("timeoutWarningDialog").hide();
                // ajax call to keep session going
                dojo.xhrGet({
                    url: "<c:url value="/session/keepAlive"/>"
                });
                // cancel session timer and reset warning and session timers
                window.clearTimeout(seshTimer);
                window.setTimeout(showTimeoutWarning, warningLength);
                seshTimer = window.setTimeout(sessionTimedOut, seshunLength);
            });
            // End Session Timeout

            var sysMsg = dojo.byId("sysMsgClose");
            if(sysMsg){
                dojo.connect(sysMsg, "onclick", function(){
                    dojo.xhrGet({
                        url: "<c:url value="/session/closeSystemMessage"/>"
                    });
                });
            }

        });
    </script>

    <decorator:head/>
</head>
<body class="tundra">
<!--[if lt IE 9]><div class="ie"><![endif]-->
<!--[if lt IE 8]><div class="ie7"><![endif]-->
<%@ include file="/WEB-INF/jsp/includes/header.jsp" %>
<article class="container_12">
    <c:if test="${not empty applicationScope.appCustomizationMarquee and empty sessionScope.showSystemMessage}">
        <p class="message">
            <span id="sysMsgClose" class="close-bt"></span>
            <c:out value="${applicationScope.appCustomizationMarquee}"/>
        </p>
    </c:if>
    <div id="flashMessage" class="with-margin">
        <c:if test="${not empty success}">
            <ul class="message success">
                <li>
                    <c:out value="${success}"/>
                </li>
                <li class="close-bt"></li>
            </ul>
        </c:if>
        <c:if test="${not empty error}">
            <ul class="message error">
                <li>
                    <c:out value="${error}"/>
                </li>
                <li class="close-bt"></li>
            </ul>
        </c:if>
        <c:if test="${not empty info}">
            <ul class="message warning">
                <li>
                    <c:out value="${info}"/>
                </li>
                <li class="close-bt"></li>
            </ul>
        </c:if>
    </div>

    <c:if test="${configSidebar == true}">
        <%@ include file="/WEB-INF/jsp/includes/configSidebar.jsp" %>
    </c:if>

    <c:if test="${assetSidebar == true}">
        <%@ include file="/WEB-INF/jsp/includes/assetSidebar.jsp" %>
    </c:if>

    <decorator:body/>
</article>
<footer>
    <a target="_blank" href="http://www.grouplink.com/products/ehd.html">
        <spring:message code="global.systemName"/>
    </a>
    <span title="<spring:message code="header.buildNumber"/>: <ehd:versionInfo buildNumber="true"/>">
        <span><ehd:versionInfo/></span>
    </span>
    <ehd:checkForUpdate />
    <br>
    <ehd:checkLicense />
    <br>
    <p>
        <spring:message code="global.copyright"/>
        <spring:message code="global.helpDeskAndIncidentManagement"/> - <spring:message code="global.cloudHostedOnPremiseOrFreeTrial"/><br>
        <a target="_blank" href="http://www.grouplink.com">
            www.GroupLink.com
        </a>
    </p>
</footer>
<!--[if lt IE 9]></div><![endif]-->
<!--[if lt IE 8]></div><![endif]-->

<div id="timeoutWarningDialog" style="display:none;" data-dojo-type="dijit.Dialog" title="<spring:message code="sessiontimeout.title"/>">
    <div class="form">
        <fieldset>
            <spring:message code="sessiontimeout.message"/>
        </fieldset>
        <p>
            <button type="button" id="logoutBtn" name="save"><spring:message code="sessiontimeout.logout"/></button>
            <button type="button" id="stayConnectedBtn" name="cancel"><spring:message code="sessiontimeout.stayconnected"/></button>
        </p>
    </div>
</div>

</body>
</html>
