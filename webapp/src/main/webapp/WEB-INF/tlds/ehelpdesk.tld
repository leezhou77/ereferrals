<?xml version="1.0" encoding="UTF-8" ?>
<taglib xmlns="http://java.sun.com/xml/ns/j2ee"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://java.sun.com/xml/ns/j2ee http://java.sun.com/xml/ns/j2ee/web-jsptaglibrary_2_0.xsd"
    version="2.0">

    <description>Custom tags for GroupLink's eHelpDesk application</description>
    <display-name>JSTL ehd</display-name>
    <tlib-version>1.1</tlib-version>
    <short-name>ehd</short-name>
    <uri>http://www.grouplink.net</uri>

    <tag>
        <description>
            Trims incoming string to a specified length and appends an ellipses.
            It will also check backwards through the string for a space and trim it
            to there so that a trailing word will not be truncated.
        </description>
        <name>trimString</name>
        <tag-class>net.grouplink.ehelpdesk.web.tags.TrimString</tag-class>
        <body-content>empty</body-content>
        <attribute>
            <description>
                the string that will be trimmed
            </description>
            <name>value</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <description>
                the number of characters that value is to be trimmed to
            </description>
            <name>length</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
    </tag>
    
    <tag>
        <description>
            Displays an html select box with a list of java.util.Locale's countries
        </description>
        <name>countryCombo</name>
        <tag-class>net.grouplink.ehelpdesk.web.tags.CountriesSelect</tag-class>
        <body-content>empty</body-content>

        <attribute>
            <description>
                the name that will be given to the select box
            </description>
            <name>name</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <description>
                optional javascript code placed in the select's onchange directive
            </description>
            <name>onChange</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <description>
                the value that will be compared with all option values. If a match
                is found that option will be the selected value.
            </description>
            <name>selectedValue</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <description>
                true if this tag is to return the html version of a select box. if false,
                the javascript for creating a select box thru dhtml is returned.
            </description>
            <name>html</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <description>
                the javascript variable name for the select element
            </description>
            <name>jsSelectVar</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <description>
                the javascript variable name for the option elements
            </description>
            <name>jsOptionVar</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
    </tag>

    <tag>
        <description>Display application version and build number.</description>
        <name>versionInfo</name>
        <tag-class>net.grouplink.ehelpdesk.web.tags.VersionInfo</tag-class>
        <body-content>empty</body-content>
        <attribute>
            <description>true if the buildNumber should be output instead of the version nubmer</description>
            <name>buildNumber</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
    </tag>

    <tag>
        <description>Checks if an ehd update is available. If true, outputs a link to the product download page</description>
        <name>checkForUpdate</name>
        <tag-class>net.grouplink.ehelpdesk.web.tags.CheckForUpdate</tag-class>
        <body-content>empty</body-content>
    </tag>

    <tag>
        <description>Checks if an ehd update is available. If true, outputs a link to the product download page</description>
        <name>checkLicense</name>
        <tag-class>net.grouplink.ehelpdesk.web.tags.CheckLicense</tag-class>
        <body-content>empty</body-content>
    </tag>

    <tag>
        <description>Add HTML line breaks to ticket subject and comments when
                     the length exceeds a set threshold.
        </description>
        <name>cutLongString</name>
        <tag-class>net.grouplink.ehelpdesk.web.tags.CutLongString</tag-class>
        <body-content>empty</body-content>
        <attribute>
            <description>the string to be trimmed</description>
            <name>value</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <description>threshold length for trimming string</description>
            <name>length</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
    </tag>

    <tag>
        <description>
            Formats a TicketHistory note for proper display. If the
            note is text format, replaces newlines with &lt;br&gt; tags. If the
            note is html format, strips out all tags except for &lt;br&gt;.
        </description>
        <name>formatHistoryNote</name>
        <tag-class>net.grouplink.ehelpdesk.web.tags.FormatHistoryNote</tag-class>
        <body-content>empty</body-content>
        <attribute>
            <description>The TicketHistory object</description>
            <name>ticketHistory</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
    </tag>

    <tag>
        <description>
            Escape html characters in the given string
        </description>
        <name>stringEscape</name>
        <tag-class>net.grouplink.ehelpdesk.web.tags.StringEscape</tag-class>
        <body-content>empty</body-content>
        <attribute>
            <description>The String to escape</description>
            <name>value</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <description>The type of escape (valid values are: html, javascript, xml, urlEncode)</description>
            <name>type</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>

    </tag>

    <tag>
        <description>Builds tabbed navigation using ul's li's and a's</description>
        <name>tabs</name>
        <tag-class>net.grouplink.ehelpdesk.web.tags.Tabs</tag-class>
        <body-content>JSP</body-content>
        <attribute>
            <description>The current tab's li will have the html class "current" applied to it.</description>
            <name>currentTab</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <name>htmlClass</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
    </tag>

    <tag>
        <description>Creates tab given a title, url, and optional html name and html class.</description>
        <name>tab</name>
        <tag-class>net.grouplink.ehelpdesk.web.tags.Tab</tag-class>
        <body-content>JSP</body-content>
        <attribute>
            <description>
                If name is given, the html class "current" will be added to this tag's li if found in the tag mapping file
            </description>
            <name>name</name>
            <required>false</required>
            <rtexprvalue>false</rtexprvalue>
        </attribute>
        <attribute>
            <description>This is the text of the generated link.</description>
            <name>title</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <description>Any extra html classes can be supplied here.</description>
            <name>htmlClass</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <name>url</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
    </tag>

    <tag>
        <description>
            Check if current user is authorized for specified Global permission
        </description>
        <name>authorizeGlobal</name>
        <tag-class>net.grouplink.ehelpdesk.web.tags.AuthorizeGlobal</tag-class>
        <body-content>JSP</body-content>

        <attribute>
            <description>The permission key</description>
            <name>permission</name>
            <required>true</required>
            <rtexprvalue>false</rtexprvalue>
        </attribute>
    </tag>

    <tag>
        <description>
            Check if current user is authorized for specified Group permission
        </description>
        <name>authorizeGroup</name>
        <tag-class>net.grouplink.ehelpdesk.web.tags.AuthorizeGroup</tag-class>
        <body-content>JSP</body-content>

        <attribute>
            <description>The permission key</description>
            <name>permission</name>
            <required>true</required>
            <rtexprvalue>false</rtexprvalue>
        </attribute>
        <attribute>
            <description>The group</description>
            <name>group</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
    </tag>

    <tag>
        <description>
            Evaluates tag body if user is not authorized for [permission] in [group]
        </description>
        <name>notAuthorized</name>
        <tag-class>net.grouplink.ehelpdesk.web.tags.NotAuthorized</tag-class>
        <body-content>JSP</body-content>

        <attribute>
            <description>The permission key</description>
            <name>permission</name>
            <required>true</required>
            <rtexprvalue>false</rtexprvalue>
        </attribute>
        <attribute>
            <description>The group</description>
            <name>group</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
    </tag>

    <function>
        <description>Checks if the browser is on a PDA</description>
        <name>isPDA</name>
        <function-class>net.grouplink.ehelpdesk.web.tags.Functions</function-class>
        <function-signature>boolean isPDA(java.lang.String)</function-signature>
    </function>

    <function>
        <description>Checks if the browser is on an iPod or an iPhone</description>
        <name>isIPodPhone</name>
        <function-class>net.grouplink.ehelpdesk.web.tags.Functions</function-class>
        <function-signature>boolean isIPodPhone(java.lang.String)</function-signature>
    </function>

    <function>
        <description>Checks if the supplied collection contains the supplied argument</description>
        <name>collectionContains</name>
        <function-class>net.grouplink.ehelpdesk.web.tags.Functions</function-class>
        <function-signature>boolean collectionContains(java.util.Collection, java.lang.Object)</function-signature>
    </function>

    <function>
        <description>Checks if the supplied array contains the supplied argument</description>
        <name>arrayContains</name>
        <function-class>net.grouplink.ehelpdesk.web.tags.Functions</function-class>
        <function-signature>boolean arrayContains(java.lang.Object[], java.lang.Object)</function-signature>
    </function>

    <function>
        <name>mapContainsKey</name>
        <function-class>net.grouplink.ehelpdesk.web.tags.Functions</function-class>
        <function-signature>boolean mapContainsKey(java.util.Map,java.lang.Object)</function-signature>
    </function>

    <function>
        <name>mapContainsValue</name>
        <function-class>net.grouplink.ehelpdesk.web.tags.Functions</function-class>
        <function-signature>boolean mapContainsValue(java.util.Map,java.lang.Object)</function-signature>
    </function>

    <function>
        <name>truncateString</name>
        <function-class>net.grouplink.ehelpdesk.web.tags.Functions</function-class>
        <function-signature>java.lang.String truncateString(java.lang.String,int)</function-signature>
    </function>

    <function>
        <name>hasGroupPermission</name>
        <function-class>net.grouplink.ehelpdesk.web.tags.Functions</function-class>
        <function-signature>java.lang.Boolean hasGroupPermission(java.lang.String, net.grouplink.ehelpdesk.domain.Group, javax.servlet.jsp.PageContext)</function-signature>
    </function>

</taglib>
