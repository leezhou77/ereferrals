<%@ tag language="java" %>
<%@ attribute name="id" required="true" %>

<script id="${id}" type="text/html">
    <jsp:doBody/>
</script>