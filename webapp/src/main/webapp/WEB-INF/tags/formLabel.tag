<%@ tag language="java" %>
<%@ attribute name="path" required="true" %>
<%@ attribute name="connectId" required="true" %>
<%@ attribute name="messageCode" required="true" %>
<%@ attribute name="required" required="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ehd" uri="/WEB-INF/tlds/ehelpdesk.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--<%@ taglib prefix="authz" uri="http://acegisecurity.org/authz" %>--%>
<%@ taglib prefix="authz" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="errMsg">
    <form:errors path="${path}"/>
</c:set>
<c:choose>
    <c:when test="${not empty errMsg}">
        <span style="color: red">
            * <spring:message code="${messageCode}"/> :
        </span>

        <span dojoType="dijit.Tooltip" connectId="${connectId}">
            ${errMsg}
        </span>
    </c:when>
    <c:otherwise>
        <c:if test="${required eq 'true'}">
            *
        </c:if>
        <spring:message code="${messageCode}"/>
        :
    </c:otherwise>
</c:choose>
<c:remove var="errMsg"/>