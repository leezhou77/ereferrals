<%@ tag language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ attribute name="dispValue" required="true" type="java.lang.Object" %>
<%@ attribute name="assetId" required="true" %>
<%@ attribute name="isDate" required="false" %>
<td onclick="go('<c:out value="${assetId}"/>')">
    <c:choose>
        <c:when test="${isDate eq 'true'}">
            <fmt:formatDate value="${dispValue}" type="date" dateStyle="SHORT" />
        </c:when>
        <c:otherwise>
            <c:out value="${dispValue}"/>
        </c:otherwise>
    </c:choose>
</td>