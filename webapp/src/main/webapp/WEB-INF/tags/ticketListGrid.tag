<%@ tag language="java" %>
<%@ attribute name="massUpdate" required="false" %>
<%@ attribute name="gridContainerId" required="true" %>
<%@ attribute name="title" required="false" %>
<%@ attribute name="region" required="false" %>
<%@ attribute name="ticketFilterId" required="false" %>
<%@ attribute name="priorities" required="true" type="java.util.List" %>
<%@ attribute name="status" required="true" type="java.util.List" %>
<%@ attribute name="containerStyle" required="false" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ehd" uri="/WEB-INF/tlds/ehelpdesk.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="authz" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<%--@elvariable id="rc" type="org.springframework.web.servlet.support.RequestContext"--%>
<div class="no-margin">
    <div dojoType="dijit.layout.ContentPane" id="${gridContainerId}"<c:if test="${containerStyle ne null}"> style="${containerStyle}"></c:if></div>
</div>

<authz:authorize ifAnyGranted="ROLE_ADMINISTRATOR, ROLE_MANAGER, ROLE_TECHNICIAN">
<div dojotype="dijit.Dialog" id="massUpdateDialog" title="<spring:message code="ticketList.massUpdate"/>"
     execute="massUpdateSubmit(arguments[0]);">
    <div style="width:600px">
        <p class="info message">
            <spring:message code="ticketList.massUpdate.cbxMesg"/>
            <span class="close-bt"></span>
        </p>
        <table class="table">
            <tr>
                <td><input id="cbx_workTime" name="cbx_workTime" dojoType="dijit.form.CheckBox"/></td>
                <td><spring:message code="ticket.workTime"/>:</td>
                <td>
                    <input type="text" name="workTime" id="workTime" style="width:100px;"
                           dojoType="dijit.form.ValidationTextBox" regExp="\d+:[0-5]?[0-9]:[0-5]?[0-9]"
                           invalidmessage="<spring:message code="ticketList.massUpdate.invalidWorktime"/>"/>
                </td>
            </tr>
            <tr>
                <td><input id="cbx_contact" name="cbx_contact" dojoType="dijit.form.CheckBox"/></td>
                <td><spring:message code="ticket.contact"/>:</td>
                <td>
                    <div dojoType="dojox.data.QueryReadStore" jsId="userStore"
                         url="<c:url value="/stores/users.json"/>"/>
                    <input dojoType="dijit.form.FilteringSelect" store="userStore" pagesize="20" searchAttr="label"
                           name="contact" id="contact" autocomplete="true"/>
                </td>
            </tr>
            <tr>
                <td><input id="cbx_assignedTo" name="cbx_assignedTo" dojoType="dijit.form.CheckBox"/></td>
                <td><spring:message code="ticket.assignedTo"/>:</td>
                <td>
                    <div dojoType="dojox.data.QueryReadStore" jsId="techStore"
                         url="<c:url value="/stores/users.json"><c:param name="tech" value="true"/><c:param name="allUrgs" value="true"/></c:url>"/>

                    <input dojoType="dijit.form.FilteringSelect" store="techStore" pagesize="20"
                           searchAttr="label" name="assignedTo" id="assignedTo" autocomplete="true"/>
                </td>
            </tr>
            <tr>
                <td><input id="cbx_priority" name="cbx_priority" dojoType="dijit.form.CheckBox"/></td>
                <td><spring:message code="ticket.priority"/>:</td>
                <td>
                    <select name="priority" id="priority" dojoType="dijit.form.FilteringSelect">
                        <option value=""/>
                        <c:forEach var="prior" items="${priorities}">
                            <option value="${prior.id}">${prior.name}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td><input id="cbx_status" name="cbx_status" dojoType="dijit.form.CheckBox"/></td>
                <td><spring:message code="ticket.status"/>:</td>
                <td>
                    <select name="status" id="status" dojoType="dijit.form.FilteringSelect">
                        <option value=""/>
                        <c:forEach var="stat" items="${status}">
                            <option value="${stat.id}">${stat.name}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td><input id="cbx_estimatedDate" name="cbx_estimatedDate" dojoType="dijit.form.CheckBox"/></td>
                <td><spring:message code="ticket.estimatedDate"/>:</td>
                <td>
                    <input type="text" name="estimatedDate" id="estimatedDate" dojoType="dijit.form.DateTextBox"
                           lang="<tags:dojoLocale locale="${rc.locale}"/>"/>
                </td>
            </tr>
            <tr>
                <td><input id="cbx_addComment" name="cbx_addComment" dojoType="dijit.form.CheckBox"/></td>
                <td>
                    <spring:message code="ticketList.massUpdate.addComment"/>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td><spring:message code="ticket.subject"/></td>
                <td>
                    <input type="text" id="addCommentSubject" name="addCommentSubject" dojoType="dijit.form.TextBox" trim="true"/>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><spring:message code="ticket.note"/></td>
                <td>
                    <textarea id="addCommentNote" name="addCommentNote" dojoType="dijit.form.Textarea"
                              style="width:300px;min-height:5.5em !important;"></textarea>
                </td>
            </tr>
            <tr>
                <td><input id="cbx_groupAsChildren" name="cbx_groupAsChildren" dojoType="dijit.form.CheckBox"/></td>
                <td>
                    <spring:message code="ticketList.massUpdate.groupAsChildren"/>:
                </td>
                <td>
                    <input type="text" name="groupAsChildren" id="groupAsChildren" style="width:50px"
                           dojoType="dijit.form.ValidationTextBox" regExp="\d+"/>
                    <spring:message code="ticketList.massUpdate.groupMsg"/>
                </td>
            </tr>
        </table>
        <p>
            <button dojotype="dijit.form.Button" type="submit" onclick="return massUpdateDialogIsValid();">
                <spring:message code="global.save"/>
            </button>
            <button dojotype="dijit.form.Button" type="button" onclick="clearDialog();">
                <spring:message code="global.cancel"/>
            </button>
            <input type="hidden" dojoType="dijit.form.TextBox" name="gridId" id="gridId"/>
        </p>
    </div>
</div>
</authz:authorize>

