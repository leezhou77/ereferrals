<%@ tag language="java" %>
<%@ attribute name="name" required="true" %>
<%@ attribute name="selectedLanguage" required="false" %>
<%@ attribute name="selectedCountry" required="false" %>
<%@ attribute name="onChange" required="false" %>
<%@ attribute name="style" required="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ehd" uri="/WEB-INF/tlds/ehelpdesk.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="authz" uri="http://www.springframework.org/security/tags" %>

<select class="full-width" name="${name}" <c:if test="${not empty onChange}">onchange="${onChange}"</c:if>
        <c:if test="${not empty style}">style="${style}"</c:if>>

    <option value="en" <c:if test="${selectedLanguage == 'en'}">selected="selected"</c:if>>
        <spring:message code="language.english"/></option>
    <option value="en_AU" <c:if test="${selectedLanguage == 'en' && selectedCountry == 'AU'}">selected="selected"</c:if>>
        <spring:message code="language.english"/> (<spring:message code="language.english.australia"/>)</option>
    <option value="en_BZ" <c:if test="${selectedLanguage == 'en' && selectedCountry == 'BZ'}">selected="selected"</c:if>>
        <spring:message code="language.english"/> (<spring:message code="language.english.belize"/>)</option>
    <option value="en_CA" <c:if test="${selectedLanguage == 'en' && selectedCountry == 'CA'}">selected="selected"</c:if>>
        <spring:message code="language.english"/> (<spring:message code="language.english.canada"/>)</option>
    <option value="en_029" <c:if test="${selectedLanguage == 'en' && selectedCountry == '029'}">selected="selected"</c:if>>
        <spring:message code="language.english"/> (<spring:message code="language.english.caribbean"/>)</option>
    <option value="en_IE" <c:if test="${selectedLanguage == 'en' && selectedCountry == 'IE'}">selected="selected"</c:if>>
        <spring:message code="language.english"/> (<spring:message code="language.english.ireland"/>)</option>
    <option value="en_JM" <c:if test="${selectedLanguage == 'en' && selectedCountry == 'JM'}">selected="selected"</c:if>>
        <spring:message code="language.english"/> (<spring:message code="language.english.jamaica"/>)</option>
    <option value="en_NZ" <c:if test="${selectedLanguage == 'en' && selectedCountry == 'NZ'}">selected="selected"</c:if>>
        <spring:message code="language.english"/> (<spring:message code="language.english.newzealand"/>)</option>
    <option value="en_PH" <c:if test="${selectedLanguage == 'en' && selectedCountry == 'PH'}">selected="selected"</c:if>>
        <spring:message code="language.english"/> (<spring:message code="language.english.philippines"/>)</option>
    <option value="en_ZA" <c:if test="${selectedLanguage == 'en' && selectedCountry == 'ZA'}">selected="selected"</c:if>>
        <spring:message code="language.english"/> (<spring:message code="language.english.southafrica"/>)</option>
    <option value="en_TT" <c:if test="${selectedLanguage == 'en' && selectedCountry == 'TT'}">selected="selected"</c:if>>
        <spring:message code="language.english"/> (<spring:message code="language.english.trinidad"/>)</option>
    <option value="en_GB" <c:if test="${selectedLanguage == 'en' && selectedCountry == 'GB'}">selected="selected"</c:if>>
        <spring:message code="language.english"/> (<spring:message code="language.english.unitedkingdom"/>)</option>
    <option value="en_US" <c:if test="${selectedLanguage == 'en' && selectedCountry == 'US'}">selected="selected"</c:if>>
        <spring:message code="language.english"/> (<spring:message code="language.english.usa"/>)</option>
    <option value="en_ZW" <c:if test="${selectedLanguage == 'en' && selectedCountry == 'ZW'}">selected="selected"</c:if>>
        <spring:message code="language.english"/> (<spring:message code="language.english.simbabwe"/>)</option>
    <option value="fi_FI" <c:if test="${selectedLanguage == 'fi' && selectedCountry == 'FI'}">selected="selected"</c:if>>
        <spring:message code="language.finnish"/> (<spring:message code="language.finnish.finland"/>)</option>
    <option value="zh_CN"
            <c:if test="${selectedLanguage == 'zh' && selectedCountry == 'CN'}">selected="selected"</c:if>>
        <spring:message code="language.chinese.simplified"/></option>
    <option value="zh_TW"
            <c:if test="${selectedLanguage == 'zh' && selectedCountry == 'TW'}">selected="selected"</c:if>>
        <spring:message code="language.chinese.traditional"/></option>
    <option value="ja_JP"
            <c:if test="${selectedLocale == 'ja_JP'}">selected="selected"</c:if>>
        <spring:message code="language.japanese"/></option>
    <option value="nl_BE" <c:if test="${selectedLanguage == 'nl' && selectedCountry == 'BE'}">selected="selected"</c:if>>
        <spring:message code="language.dutch"/> (<spring:message code="language.dutch.belgium"/>)</option>
    <option value="nl_NL" <c:if test="${selectedLanguage == 'nl' && selectedCountry == 'NL'}">selected="selected"</c:if>>
        <spring:message code="language.dutch"/> (<spring:message code="language.dutch.netherlands"/>)</option>
    <option value="no" <c:if test="${selectedLanguage == 'no'}">selected="selected"</c:if>>
        <spring:message code="language.norwegian"/></option>
    <option value="fr_BE" <c:if test="${selectedLanguage == 'fr' && selectedCountry == 'BE'}">selected="selected"</c:if>>
        <spring:message code="language.french"/> (<spring:message code="language.dutch.belgium"/>)</option>
    <option value="fr_CA" <c:if test="${selectedLanguage == 'fr' && selectedCountry == 'CA'}">selected="selected"</c:if>>
        <spring:message code="language.french"/> (<spring:message code="language.english.canada"/>)</option>
    <option value="fr_FR" <c:if test="${selectedLanguage == 'fr' && selectedCountry == 'FR'}">selected="selected"</c:if>>
        <spring:message code="language.french"/> (<spring:message code="language.french.france"/>)</option>
    <option value="fr_LU" <c:if test="${selectedLanguage == 'fr' && selectedCountry == 'LU'}">selected="selected"</c:if>>
        <spring:message code="language.french"/> (<spring:message code="language.french.luxembourg"/>)</option>
    <option value="fr_MC" <c:if test="${selectedLanguage == 'fr' && selectedCountry == 'MC'}">selected="selected"</c:if>>
        <spring:message code="language.french"/> (<spring:message code="language.french.monaco"/>)</option>
    <option value="fr_CH" <c:if test="${selectedLanguage == 'fr' && selectedCountry == 'CH'}">selected="selected"</c:if>>
        <spring:message code="language.french"/> (<spring:message code="language.french.switzerland"/>)</option>
    <option value="de_AT" <c:if test="${selectedLanguage == 'de' && selectedCountry == 'AT'}">selected="selected"</c:if>>
        <spring:message code="language.german"/> (<spring:message code="language.german.austria"/>)</option>
    <option value="de_DE" <c:if test="${selectedLanguage == 'de' && selectedCountry == 'DE'}">selected="selected"</c:if>>
        <spring:message code="language.german"/> (<spring:message code="language.german.germany"/>)</option>
    <option value="de_LI" <c:if test="${selectedLanguage == 'de' && selectedCountry == 'LI'}">selected="selected"</c:if>>
        <spring:message code="language.german"/> (<spring:message code="language.german.liechtenstein"/>)</option>
    <option value="de_LU" <c:if test="${selectedLanguage == 'de' && selectedCountry == 'LU'}">selected="selected"</c:if>>
        <spring:message code="language.german"/> (<spring:message code="language.french.luxembourg"/>)</option>
    <option value="de_CH" <c:if test="${selectedLanguage == 'de' && selectedCountry == 'CH'}">selected="selected"</c:if>>
        <spring:message code="language.german"/> (<spring:message code="language.french.switzerland"/>)</option>
    <option value="pt_BR" <c:if test="${selectedLanguage == 'pt' && selectedCountry == 'BR'}">selected="selected"</c:if>>
        <spring:message code="language.portuguese"/> (<spring:message code="language.portuguese.brazil"/>)</option>
    <option value="pt_PT" <c:if test="${selectedLanguage == 'pt' && selectedCountry == 'PT'}">selected="selected"</c:if>>
        <spring:message code="language.portuguese"/> (<spring:message code="language.portuguese.portugal"/>)</option>
    <option value="es" <c:if test="${selectedLanguage == 'es'}">selected="selected"</c:if>>
        <spring:message code="language.spanish"/></option>
    <option value="sv_SE" <c:if test="${selectedLanguage == 'sv' && selectedCountry == 'SE'}">selected="selected"</c:if>>
        <spring:message code="language.swedish"/> (<spring:message code="language.swedish.sweden"/>)</option>
    <option value="sv_FI" <c:if test="${selectedLanguage == 'sv' && selectedCountry == 'FI'}">selected="selected"</c:if>>
        <spring:message code="language.swedish"/> (<spring:message code="language.swedish.finland"/>)</option>
    <option value="tr_TR" <c:if test="${selectedLanguage == 'tr' && selectedCountry == 'TR'}">selected="selected"</c:if>>
        <spring:message code="language.turkish"/> (<spring:message code="language.turkish.turkey"/>)</option>
    <option value="ja_JP" <c:if test="${selectedLanguage == 'ja' && selectedCountry == 'JP'}">selected="selected"</c:if>>
        <spring:message code="language.japanese"/> (<spring:message code="language.japanese.japan"/>)</option>
</select>