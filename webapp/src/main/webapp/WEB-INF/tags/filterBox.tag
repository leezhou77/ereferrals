<%@ tag language="java" %>
<%@ attribute name="titleMessageCode" %>
<%@ attribute name="title" %>
<%@ attribute name="widgets" fragment="true" %>
<%@ attribute name="columnName" %>
<%@ attribute name="filterName" required="true" %>
<%@ attribute name="filterValuesBox" fragment="true" %>
<%@ attribute name="filterValuesBoxId" %>
<%@ attribute name="hidden" %>
<%@ attribute name="extraActions" fragment="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${empty hidden}">
    <c:set var="hidden" value="true"/>
</c:if>

<c:choose>
    <c:when test="${not empty titleMessageCode}">
        <c:set var="boxTitle">
            <jsp:attribute name="value"><spring:message code="${titleMessageCode}"/></jsp:attribute>
        </c:set>
    </c:when>
    <c:otherwise>
        <c:set var="boxTitle" value="${title}"/>
    </c:otherwise>
</c:choose>


<li id="fd.ticketFilter.${filterName}" data-filter-colname="${columnName}" data-filter-title="${boxTitle}" class="filter-definition"<c:if test="${hidden eq true}"> style="display:none"</c:if>>
    <a href="javascript:void(0)" class="no-padding">
        <c:out value="${boxTitle}"/>
    </a>
    <ul class="mini-menu">
        <li>
            <a href="javascript:void(0)" class="hideFilterLi">
                <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>">
                <spring:message code="global.remove"/>
            </a>
        </li>
    </ul>
    <ul class="extended-options">
        <li>
            <jsp:invoke fragment="widgets"/>
        </li>
        <c:if test="${fn:length(filterValuesBoxId) > 0}">
            <li class="filterValuesBoxContainer">
                <div id="${filterValuesBoxId}" class="filterValuesBox box no-margin">
                    <jsp:invoke fragment="filterValuesBox"/>
                </div>
            </li>
        </c:if>
    </ul>
</li>