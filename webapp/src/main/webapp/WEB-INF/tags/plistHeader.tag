<%@ tag language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="messageCode" required="true" %>
<%@ attribute name="sortProperty" required="true" %>
<%--@elvariable id="refreshUrl" type="java.lang.String"--%>
<%--@elvariable id="listHolder" type="java.util.Map"--%>
<th>
    <c:choose>
        <c:when test="${sortProperty eq 'nosort'}">
            <c:out value="${messageCode}"/>
        </c:when>
        <c:otherwise>
            <a href=" <c:url value="${refreshUrl}"><c:param name="sort.property" value="${sortProperty}"/></c:url>"
               title="<spring:message code="ticketList.sortBy"/>">
                <spring:message code="${messageCode}" text="${messageCode}"/>
                <c:choose>
                    <c:when test="${listHolder.assets.sort.property == sortProperty}">
                        <c:choose>
                            <c:when test="${listHolder.assets.sort.ascending}">
                                <img src="<c:url value="/images/sort_asc.gif"/>" alt="" align="top"/>
                            </c:when>
                            <c:otherwise>
                                <img src="<c:url value="/images/sort_desc.gif"/>" alt="" align="top"/>
                            </c:otherwise>
                        </c:choose>
                    </c:when>
                    <c:otherwise>
                        <img src="<c:url value="/images/sort_none.gif"/>" alt="" align="top">
                    </c:otherwise>
                </c:choose>
            </a>
        </c:otherwise>
    </c:choose>
</th>