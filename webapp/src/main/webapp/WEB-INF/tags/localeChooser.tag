<%@ tag language="java" %>
<%@ attribute name="name" required="true" %>
<%@ attribute name="id" required="false" %>
<%@ attribute name="selectedLocale" required="false" %>
<%@ attribute name="onChange" required="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ehd" uri="/WEB-INF/tlds/ehelpdesk.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<select name="${name}" id="${id}" <c:if test="${not empty onChange}">onchange="${onChange}"</c:if>>

    <option value="en" <c:if test="${selectedLocale == 'en'}">selected="selected"</c:if>>
        <spring:message code="language.english"/></option>
    <option value="en_AU" <c:if test="${selectedLocale == 'en_AU'}">selected="selected"</c:if>>
        <spring:message code="language.english"/> (<spring:message code="language.english.australia"/>)</option>
    <option value="en_BZ" <c:if test="${selectedLocale == 'en_BZ'}">selected="selected"</c:if>>
        <spring:message code="language.english"/> (<spring:message code="language.english.belize"/>)</option>
    <option value="en_CA" <c:if test="${selectedLocale == 'en_CA'}">selected="selected"</c:if>>
        <spring:message code="language.english"/> (<spring:message code="language.english.canada"/>)</option>
    <option value="en_029" <c:if test="${selectedLocale == 'en_029'}">selected="selected"</c:if>>
        <spring:message code="language.english"/> (<spring:message code="language.english.caribbean"/>)</option>
    <option value="en_IE" <c:if test="${selectedLocale == 'en_IE'}">selected="selected"</c:if>>
        <spring:message code="language.english"/> (<spring:message code="language.english.ireland"/>)</option>
    <option value="en_JM" <c:if test="${selectedLocale == 'en_JM'}">selected="selected"</c:if>>
        <spring:message code="language.english"/> (<spring:message code="language.english.jamaica"/>)</option>
    <option value="en_NZ" <c:if test="${selectedLocale == 'en_NZ'}">selected="selected"</c:if>>
        <spring:message code="language.english"/> (<spring:message code="language.english.newzealand"/>)</option>
    <option value="en_PH" <c:if test="${selectedLocale == 'en_PH'}">selected="selected"</c:if>>
        <spring:message code="language.english"/> (<spring:message code="language.english.philippines"/>)</option>
    <option value="en_ZA" <c:if test="${selectedLocale == 'en_ZA'}">selected="selected"</c:if>>
        <spring:message code="language.english"/> (<spring:message code="language.english.southafrica"/>)</option>
    <option value="en_TT" <c:if test="${selectedLocale == 'en_TT'}">selected="selected"</c:if>>
        <spring:message code="language.english"/> (<spring:message code="language.english.trinidad"/>)</option>
    <option value="en_GB" <c:if test="${selectedLocale == 'en_GB'}">selected="selected"</c:if>>
        <spring:message code="language.english"/> (<spring:message code="language.english.unitedkingdom"/>)</option>
    <option value="en_US" <c:if test="${selectedLocale == 'en_US'}">selected="selected"</c:if>>
        <spring:message code="language.english"/> (<spring:message code="language.english.usa"/>)</option>
    <option value="en_ZW" <c:if test="${selectedLocale == 'en_ZW'}">selected="selected"</c:if>>
        <spring:message code="language.english"/> (<spring:message code="language.english.simbabwe"/>)</option>
    <option value="fi_FI" <c:if test="${selectedLocale == 'fi_FI'}">selected="selected"</c:if>>
        <spring:message code="language.finnish"/> (<spring:message code="language.finnish.finland"/>)</option>
    <option value="zh_CN"
            <c:if test="${selectedLocale == 'zh_CN'}">selected="selected"</c:if>>
        <spring:message code="language.chinese.simplified"/></option>
    <option value="zh_TW"
            <c:if test="${selectedLocale == 'zh_TW'}">selected="selected"</c:if>>
        <spring:message code="language.chinese.traditional"/></option>
    <option value="ja_JP"
            <c:if test="${selectedLocale == 'ja_JP'}">selected="selected"</c:if>>
        <spring:message code="language.japanese"/></option>
    <option value="nl_BE" <c:if test="${selectedLocale == 'nl_BE'}">selected="selected"</c:if>>
        <spring:message code="language.dutch"/> (<spring:message code="language.dutch.belgium"/>)</option>
    <option value="nl_NL" <c:if test="${selectedLocale == 'nl_NL'}">selected="selected"</c:if>>
        <spring:message code="language.dutch"/> (<spring:message code="language.dutch.netherlands"/>)</option>
    <option value="no" <c:if test="${selectedLocale == 'no'}">selected="selected"</c:if>>
        <spring:message code="language.norwegian"/></option>
    <option value="fr_BE" <c:if test="${selectedLocale == 'fr_BE'}">selected="selected"</c:if>>
        <spring:message code="language.french"/> (<spring:message code="language.dutch.belgium"/>)</option>
    <option value="fr_CA" <c:if test="${selectedLocale == 'fr_CA'}">selected="selected"</c:if>>
        <spring:message code="language.french"/> (<spring:message code="language.english.canada"/>)</option>
    <option value="fr_FR" <c:if test="${selectedLocale == 'fr_FR'}">selected="selected"</c:if>>
        <spring:message code="language.french"/> (<spring:message code="language.french.france"/>)</option>
    <option value="fr_LU" <c:if test="${selectedLocale == 'fr_LU'}">selected="selected"</c:if>>
        <spring:message code="language.french"/> (<spring:message code="language.french.luxembourg"/>)</option>
    <option value="fr_MC" <c:if test="${selectedLocale == 'fr_MC'}">selected="selected"</c:if>>
        <spring:message code="language.french"/> (<spring:message code="language.french.monaco"/>)</option>
    <option value="fr_CH" <c:if test="${selectedLocale == 'fr_CH'}">selected="selected"</c:if>>
        <spring:message code="language.french"/> (<spring:message code="language.french.switzerland"/>)</option>
    <option value="de_AT" <c:if test="${selectedLocale == 'de_AT'}">selected="selected"</c:if>>
        <spring:message code="language.german"/> (<spring:message code="language.german.austria"/>)</option>
    <option value="de_DE" <c:if test="${selectedLocale == 'de_DE'}">selected="selected"</c:if>>
        <spring:message code="language.german"/> (<spring:message code="language.german.germany"/>)</option>
    <option value="de_LI" <c:if test="${selectedLocale == 'de_LI'}">selected="selected"</c:if>>
        <spring:message code="language.german"/> (<spring:message code="language.german.liechtenstein"/>)</option>
    <option value="de_LU" <c:if test="${selectedLocale == 'de_LU'}">selected="selected"</c:if>>
        <spring:message code="language.german"/> (<spring:message code="language.french.luxembourg"/>)</option>
    <option value="de_CH" <c:if test="${selectedLocale == 'de_CH'}">selected="selected"</c:if>>
        <spring:message code="language.german"/> (<spring:message code="language.french.switzerland"/>)</option>
    <option value="pt_BR" <c:if test="${selectedLocale == 'pt_BR'}">selected="selected"</c:if>>
        <spring:message code="language.portuguese"/> (<spring:message code="language.portuguese.brazil"/>)</option>
    <option value="pt_PT" <c:if test="${selectedLocale == 'pt_PT'}">selected="selected"</c:if>>
        <spring:message code="language.portuguese"/> (<spring:message code="language.portuguese.portugal"/>)</option>
    <option value="es" <c:if test="${selectedLocale == 'es'}">selected="selected"</c:if>>
        <spring:message code="language.spanish"/></option>
    <option value="sv_SE" <c:if test="${selectedLocale == 'sv_SE'}">selected="selected"</c:if>>
        <spring:message code="language.swedish"/> (<spring:message code="language.swedish.sweden"/>)</option>
    <option value="sv_FI" <c:if test="${selectedLocale == 'sv_FI'}">selected="selected"</c:if>>
        <spring:message code="language.swedish"/> (<spring:message code="language.swedish.finland"/>)</option>
    <option value="tr_TR" <c:if test="${selectedLocale == 'tr_TR'}">selected="selected"</c:if>>
        <spring:message code="language.turkish"/> (<spring:message code="language.turkish.turkey"/>)</option>
    <option value="ja_JP" <c:if test="${selectedLocale == 'ja_JP'}">selected="selected"</c:if>>
        <spring:message code="language.japanese"/> (<spring:message code="language.japanese.japan"/>)</option>
</select>