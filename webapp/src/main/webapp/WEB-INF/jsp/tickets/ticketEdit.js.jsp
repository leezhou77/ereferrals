<script type="text/javascript">

//WORK TIME FUNCTIONS
function changeTime(hiddenTime, displayTime) {
    if (verifyTimeChange(hiddenTime, displayTime)) {
        saveWorkTime(displayTime, 'playButton');
    }
}

function stopTime(command, hiddenTime, displayTime) {
    stopTimerI(command, hiddenTime, displayTime);
    saveWorkTime(displayTime, 'stopButton');
}

function saveWorkTime(displayTime, buttonId) {
    var dTime;
    if (dojo.byId(buttonId) == null) return;
    dojo.byId(buttonId).src = '<c:url value='/images/circle-ball-dark-antialiased.gif'/>';
    if (document.getElementById(displayTime) != null) {
        dTime = document.getElementById(displayTime).value;
    } else {
        dTime = "00:00:00";
    }
    var saveWorkTimeUrl = '<c:url value="/tickets/legacy/saveWorkTime.glml"><c:param name="ticketId" value="${ticket.id}"/></c:url>' + "&workTime=" + dTime;

    dojo.xhrPost({
        url: saveWorkTimeUrl,
        handleAs: "text",
        content: {workTime: dTime},
        load: function() {
            if (buttonId == 'playButton') {
                dojo.byId(buttonId).src = '<c:url value='/images/play.gif'/>';
            }
            if (buttonId == 'stopButton') {
                dojo.byId(buttonId).src = '<c:url value='/images/stop.gif'/>';
            }
        }
    });
}

function timerClicked(status) {
    if (status == 'ON') {
        document.getElementById('ts').value='ON';
        timerI('hiddenWorkTime','displayWorkTime');
    } else {
        document.getElementById('ts').value='OFF';
        stopTime('stop','hiddenWorkTime','displayWorkTime');
    }
}

// END WORK TIME FUNCTIONS

function copyContactToAll(){
    var contactId = document.getElementById("contact").value;
    var ticketId = '${ticket.id}';
    TicketService.applyContactToAllSubtickets(contactId, ticketId, function(){
        $("#contactCopySuccess").css("display", "inline");
    });
}

function deleteAttachment(objId, attachId, ticketOrHistory) {
    if (confirm('<spring:message code="ticket.deleteAttachment.confirm" javaScriptEscape="true"/>')) {
        var attachDiv;
        switch(ticketOrHistory) {
            case 'ticket':
                TicketService.deleteAttachment(objId, attachId);
                attachDiv = dojo.byId('attachmentDiv_' + attachId);
                break;
            case 'history':
                TicketService.deleteTicketHistoryAttachment(objId, attachId);
                attachDiv = dojo.byId('attachmentDivTicketHistory_' + attachId);
                break;
        }
        var animation = dojo.fadeOut({ node: attachDiv});
        animation.onEnd = function() {
            dojo.destroy(attachDiv);
            if(dojo.query("#ticketHistoryAttachments a").length == 0) {
                var label = dojo.byId('ticketHistoryAttachmentsLabel');
                dojo.destroy(label);
            }
        };
        animation.play();
    }
}

function loadContactComboFromHidden() {
    var contactNode = dojo.byId("contact");
    if (contactNode) {
        var contactId = dojo.attr(contactNode, "value");
        if (contactId == "") return;

        dojo.xhrPost({
            url: '<c:url value="/stores/singleUser.json"/>',
            handleAs: "json",
            content: { contactId: contactId},
            load: function (jsonUser) {
                dijit.byId("ctctCombo").setValue(jsonUser.label);
            },
            error: function(response, ioArgs) {
                alert("XHR error HTTP status code: " + ioArgs.xhr.status);
                return response;
            }
        });
    }
}

function loadNewContact() {
    var ctctLabel = dijit.byId("ctctCombo").getValue();
    userStore.fetch({
        query: { label: ctctLabel},
        onBegin: clearCtctFields,
        onComplete: loadCtctFields,
        onError: fetchFailed,
        queryOptions: {deep:true}
    });
}

function clearCtctFields() {
    var emptyMsg = "<spring:message code="global.notSelected"/>";
    document.getElementById("contact.email").innerHTML = emptyMsg;
    document.getElementById("contact.phone").innerHTML = emptyMsg;
    document.getElementById("contact.address").innerHTML = emptyMsg;
}

function loadCtctFields(items) {
    if (items.length == 0) return;

    var contact = items[0];

    var id = userStore.getValue(contact, "userId");
    var cEmail = userStore.getValue(contact, "email");

    if (cEmail != "") {
        var emailLink = '<a href="mailto:' + cEmail + '">' + cEmail + '</a>';
        document.getElementById("contact.email").innerHTML = emailLink;
    }

    TicketService.getPrimaryPhoneForUser(id, handleGetPrimaryPhone);
    TicketService.getPrimaryAddressForUser(id, handleGetPrimaryAddress);

    if (id != "") {
        document.getElementById("contact").value = id;
        document.getElementById("contactId").value = id;
    }

    <c:if test="${ticket.id == null}">
    var locationInput = dijit.byId("location");
    locationInput.set("value", userStore.getValue(contact, "location_id"));
    locationInput.set("displayValue", userStore.getValue(contact, "location"));
    </c:if>
}

function fetchFailed(error) {
    alert("fetch failed: " + error);
}

function handleGetPrimaryPhone(phone) {
    if (phone) {
        var html = phone.number;
        if (phone.extension) {
            html += ' <spring:message code="phone.ext" /> ' + phone.extension;
        }

        html += "&nbsp;(" + phone.phoneType + ")&nbsp;";

        if (phone.bestTimeToCall) {
            html += '<br/>' + phone.bestTimeToCall;
        }

        document.getElementById("contact.phone").innerHTML = html;
    }
}

function handleGetPrimaryAddress(address) {
    if (address) {

        var html = address.line1 + '<br/>';
        if (address.line2) {
            html += address.line2 + '<br/>';
        }

        html += address.city + ',&nbsp;' + address.state + ' ' + address.postalCode;
        if (address.country) {
            html += '<br/>' + address.country;
        }

        document.getElementById("contact.address").innerHTML = html;
    }
}

/******   Cascading FilteringSelect functions ******/
function loadNextSelectForPrev(prevName, nextName, nextStore, nextStoreUrl) {
    var prevId = dojo.attr(dijit.byId(prevName), "value");
    if (prevId == "") {
        // clear successive select starting from 'next'
        clearFromPoint(nextName);
        return;
    }

    var groupId = null;
    var locationId = dojo.attr(dijit.byId("location") || {value: ""}, "value");
    if (prevName == "categoryOption") {
        groupId = dijit.byId("group").getValue();
    }

    // reset the store with prevId parameter
    resetStore(nextName, nextStore, nextStoreUrl, prevId, groupId, locationId);
}

function clearFromPoint(start) {
    switch (start) {
        case "group":
            resetStore("group", groupStore, '<c:url value="/stores/groups.json"/>', '', null, null);
        // fall through
        case "category":
            resetStore("category", categoryStore, '<c:url value="/stores/categories.json"/>', '', null, null);
        // fall through
        case "categoryOption":
            resetStore("categoryOption", catOptStore, '<c:url value="/stores/catOptions.json"/>', '', null, null);
        default: break;
    }
}

function resetStore(selectName, store, storeUrl, prevId, groupId, locationId) {

    // build the store URL
    var builtURL = storeUrl + "?incEmpty=true&prevId=" + prevId;
    if (groupId != null) builtURL += "&groupId=" + groupId;
    if (locationId != null) builtURL += "&locationId=" + locationId;

    storeNum = -1;
    store.close();
    store = new dojo.data.ItemFileReadStore({
        url:  builtURL,
        hierarchical: false
    });

    store.fetch();
    dijit.byId(selectName).store = store;
    dijit.byId(selectName).setDisplayedValue('');

    store.fetch({query: {}, onComplete:
            function(items, request) {
                if (items.length == 1) {
                    dijit.byId(selectName).attr('value', items[0].id);
                }
            }
    });
}

function resetAssignmentStore() {
    if(typeof(assignedToStore) != "undefined") {
        var locationId = dojo.attr(dijit.byId("location"), "value");
        var groupId = dojo.attr(dijit.byId("group"), "value");
        var catId = dojo.attr(dijit.byId("category"), "value");
        var catOptId = dojo.attr(dijit.byId("categoryOption"), "value");

        assignedToStore.close();
        var store = new dojo.data.ItemFileReadStore({
            url: "<c:url value="/stores/moreBetterAssignments.json"/>?incEmpty=true&locationId=" + locationId + "&groupId=" + groupId + "&catId=" + catId + "&catOptId=" + catOptId,
            hierarchical: false
        });

        store.fetch();
        var assTo = dijit.byId("assignedTo");
        assTo.store = store;
        assTo.setDisplayedValue('');

        store.fetch({
            query: {},
            onComplete: function(items, request) {
                if (items.length == 2) {
                    assTo.attr('value', items[1].id);
                }
            }
        });
    }
}

function clearCustomFields() {
//    console.log("called clearCustomFields");
    // destroy existing custom field widgets
    var cfWidgets = dijit.findWidgets(dojo.byId("cfTable_0"));
    dojo.forEach(cfWidgets, function(w){
        w.destroyRecursive(true);
    });

    cfWidgets = dijit.findWidgets(dojo.byId("cfTable_1"));
    dojo.forEach(cfWidgets, function(w){
        w.destroyRecursive(true);
    });

    // clear custom field tables
    dojo.query("#cfTable_0 tr, #cfTable_1 tr").forEach(dojo.destroy);
//    console.log("finished clearCustomFields");

}

function loadCustomFields() {
//    console.log("called loadCustomFields");

    var locationId = dijit.byId("location").getValue();
    var groupId = dojo.attr(dijit.byId("group"), "value");
    var catId = dojo.attr(dijit.byId("category"), "value");
    var categoryOptId = dojo.attr(dijit.byId("categoryOption"), "value");
    var tid;
    <c:if test="${!empty ticket.id}">
    tid = "${ticket.id}";
    </c:if>

    var yerl = "<c:url value="/stores/ticketCustomFields.json"/>?groupId=" + groupId + "&catId=" + catId + "&catOptId=" + categoryOptId + "&locId=" + locationId;
    if (tid != null) {
        yerl += "&tid=" + tid;
    }

    dojo.xhrGet({
        url: yerl,
        handleAs: "json",
        preventCache: true,
        load: function(data) {
            // clear custom fields
            clearCustomFields();
            for (var ind in data) {
                var $table = $("#cfTable_" + ind % 2);
                var cfJson = data[ind];
                var $cfHtml = getCustomFieldHTML(cfJson, tid == null ? "new" : tid);
                $table.append($cfHtml);
//                console.log("calling dojo parse");
                dojo.parser.parse($table[0]);
//                console.log("finished dojo parse");
            }
            setupDateTimePickers();
        },
        error: function(response, ioArgs) {
            alert("XHR error (ticketEdit2:loadCustomFields) HTTP status code: " + ioArgs.xhr.status);
            return response;
        }
    });
//    console.log("finished loadCustomFields");
}

function setupDateTimePickers() {
    $('.dtpicker').datetimepicker({controlType: 'select'}); // Decorate all the datetime fields with datetimepicker
}

var customFieldIdArray = new Array();

var cfVals = {};
<c:if test="${empty ticket.id}">
<c:forEach var="cfVal" items="${ticket.customeFieldValues}">
cfVals["${cfVal.customField.id}"] = "<c:out value="${cfVal.fieldValue}"/>";
</c:forEach>
</c:if>

function getCustomFieldHTML(cf, tid) {
    var html;
    var cfId = "cf_" + cf.id + "_" + tid;
    var widget = dijit.byId(cfId);
    if (widget) {
        console.log("widget with id " + cfId + " already exists");
        widget.destroyRecursive(true);
    }

    var cfVal = cf.cfValue.value;
    var required = cf.required;
    if (cfVal.trim() == "" && cfVals[cf.id]) cfVal = cfVals[cf.id];
    switch (cf.type) {
        case "text":
            var template = _.template($("#cf_text").html());
            html = $(template({cf_dom_id: cfId, cf_label: cf["name"], cf_name: cfId, cf_value: cfVal}));
            customFieldIdArray[customFieldIdArray.length] = cfId;
            break;
        case 'textarea':
            var template = _.template($("#cf_textarea").html());
            html = $(template({cf_dom_id: cfId, cf_label: cf["name"], cf_name: cfId, cf_value: cfVal}));
            customFieldIdArray[customFieldIdArray.length] = cfId;
            break;
        case 'radio':
            var template = _.template($("#cf_radio").html());
            var radioOptions = $("<div>");
            _.each(cf.options, function(cfOption) {
                var checked = false;
                var cfOptionId = cfId + '_' + cfOption.id;
                if (cfOption.displayValue != "" && cfOption.displayValue == cfVal) checked = true;
                var optionTemplate = _.template($("#cf_radio_option").html());
                radioOptions.append($(optionTemplate({cf_dom_id: cfOptionId, cf_name: cfId, cf_value: cfOption.value, cf_label: cfOption.displayValue })).attr("checked", checked));
                radioOptions.append("<br>");
                customFieldIdArray[customFieldIdArray.length] = cfOptionId;
            });
            html = $(template({cf_dom_id: cfId, cf_label: cf["name"], cf_name: cfId, cf_value: "", cf_options: radioOptions.html()}));
            break;
        case 'checkbox':
            var checked = false;
            if (cfVal == 'true') {
                checked = true;
            }
            var template = _.template($("#cf_checkbox").html());
            html = $(template({cf_dom_id: cfId, cf_label: cf["name"], cf_name: cfId}));
            html.find("input:checkbox").attr("checked", checked);
            customFieldIdArray[customFieldIdArray.length] = cfId;
            break;
        case 'select':
            var template = _.template($("#cf_select").html());
            var selectOptions = $("<div>");
            _.each(cf.options, function(cfOption) {
                var selected = false;
                if (cfOption.displayValue != "" && cfOption.displayValue == cfVal) {
                    selected = true;
                }
                var optionTemplate = _.template($("#cf_select_option").html());
                var newOption = $(optionTemplate({cf_option_value: cfOption.value, cf_option_label: cfOption.displayValue}));
                if(selected) {
                    newOption.attr("selected", "selected");
                }
                selectOptions.append(newOption);
            });
            html = $(template({cf_dom_id: cfId, cf_name: cfId, cf_label: cf["name"], cf_options: selectOptions.html()}));
            customFieldIdArray[customFieldIdArray.length] = cfId;
            break;
        case 'number':
            var template = _.template($("#cf_number").html());
            html = $(template({cf_dom_id: cfId, cf_label: cf["name"], cf_name:cfId, cf_value: cfVal}));
            customFieldIdArray[customFieldIdArray.length] = cfId;
            break;
        case 'date':
            var template = _.template($("#cf_date").html());
            html = $(template({cf_dom_id: cfId, cf_label: cf["name"], cf_name:cfId, cf_value: cfVal}));
            customFieldIdArray[customFieldIdArray.length] = cfId;
            break;
        case 'datetime':
            var template = _.template($("#cf_datetime").html());
            html = $(template({cf_dom_id: cfId, cf_label: cf["name"], cf_name:cfId, cf_value: cfVal}));
            customFieldIdArray[customFieldIdArray.length] = cfId;
            break;
        case 'time':
            var template = _.template($("#cf_time").html());
            html = $(template({cf_dom_id: cfId, cf_label: cf["name"], cf_name:cfId, cf_value: cfVal}));
            customFieldIdArray[customFieldIdArray.length] = cfId;
            break;
        default:
            html = "";
            break;
    }

    if(required) { html.find("label").not(".inline").addClass("required"); }
    return html;
}

/****** attachments *******/
var inputIndex = 1;
function fileSelected() {
    var createNew = true;
    for (var i = 0; i < inputIndex; i++) {
        var uploadFiles = document.getElementsByName('attchmnt[' + i + ']')[0];
        if (uploadFiles && uploadFiles.value.length == 0) {
            createNew = false;
        }
    }
    if (createNew) {
        addFileSelect();
    }
}

function addFileSelect() {

    var newNode = document.createElement('input');
    var breakNode = document.createElement('br');
    var name = 'attchmnt[' + inputIndex + ']';
    var fileLoader = dojo.byId('fileLoader_EditDiv');

    newNode.setAttribute('type', 'file');
    newNode.setAttribute('name', name);
    newNode.setAttribute('id', name);
    newNode.setAttribute('size', '75');
    newNode.setAttribute('onchange', 'fileSelected()');
    fileLoader.appendChild(newNode);
    fileLoader.appendChild(breakNode);
    inputIndex++;

    //for IE to work
    var node = document.getElementById(name);
    if (node.attachEvent) {
        node.attachEvent('onchange', fileSelected);
        node.size = 85;
        node.style.width = '100%';
    }
}

/******* form button actions ******/
function submitticketEdit() {
    // xhrPost didn't work for a multipart so i had to use the iframe.send instead
    // to get an ajax submit
    dojo.io.iframe.send({
        contentType: "multipart/form-data",
        handleAs: "html",
        form:     'ticketForm',
        load:     function (response) {
            dojo.byId('submitResponse').innerHTML = response.getElementsByTagName("pre")[0].innerHTML;
        },
        error:    function (error) {
            console.error('Error: ', error);
        }
    });
}

function displayAssetTrack(jsonGroup) {
    if (!jsonGroup || jsonGroup == "") return;

    var assTrackType = dijit.byId("group").store.getValue(jsonGroup[0], "assTrackType");

    var showEHD, showZEN, showLabel;
    if (assTrackType == "EHD") {
        showEHD = "block";
        showZEN = "none";
        showLabel = "block";
    } else if (assTrackType == "ZEN10") {
        showEHD = "none";
        showZEN = "block";
        showLabel = "block";
    } else {
        showEHD = "none";
        showZEN = "none";
        showLabel = "none";
    }

    document.getElementById("ehdAssetDiv").style.display = showEHD;
    document.getElementById("zenAssetDiv").style.display = showZEN;
    document.getElementById("assetLabel").style.display = showLabel;
}

// Asset Tracker Search Windows
var ehdSearchWindow, zenSearchWindow;
function assetSearch() {
    var assetNumber = document.getElementById('asset');

    // Build the url to pre-populate the search form
    var url = '<c:url value="/asset/searchAsset.glml?assetNumber=" />' + assetNumber.value;
    if (assetNumber.value == "") {
        url += "&location=" + dijit.byId("location").getDisplayedValue();
        url += "&group=" + dijit.byId("group").getDisplayedValue();
        url += "&category=" + dijit.byId("category").getDisplayedValue();
        url += "&categoryOption=" + dijit.byId("categoryOption").getDisplayedValue();
    }
    ehdSearchWindow = window.open(url, "ehdAssetSearch", "modal=yes,toolbar=no,status=yes,width=700,height=600,scrollbars=yes,resizable=yes,");
}

function choozAsset(assetId) {
    document.getElementById("asset").value = assetId;
    ehdSearchWindow.close();
}

function zenAssetSearch() {
    var contactId = document.getElementById("contactId").value;
    var url = '<c:url value="/asset/searchZenAsset.glml"/>?contactUserId=' + contactId;
    zenSearchWindow = window.open(url, "zenAssetSearch", "modal=yes,toolbar=no,status=yes,width=600,height=500,scrollbars=yes,resizable=yes,");
}

function chooseZenAsset(zenAssetId) {
    document.getElementById("zenAsset").value = zenAssetId;
    zenSearchWindow.close();
}

function openAssetDetailsWindow(assetId) {
    var url = '<c:url value="/asset/assetEdit.glml" />' + '?assetId=' + assetId;
    try {
        if (window.opener && !window.opener.closed) {
            window.opener.location = url;
        }
    } catch (err) {
        console.debug("error opening user editor on parent window: " + err);
    }
}

function openZenAssetDetailsWindow(deviceZuid, width, height) {
    var url = '${zenBaseUrl}/jsp/index.jsp?pageid=workstationDetails&uid=' + deviceZuid + '&adminid=Devices';
    openWindow(url, 'zenAssetDetails_' + deviceZuid, width, height);
}

function go(ticketId, refreshParent) {
    var url = "<c:url value="/tickets/"/>" + ticketId + "/edit";
    if (ticketId) url += "?tid=" + ticketId + "&refreshParent=" + refreshParent;
    openWindow(url, '_' + ticketId, '800px', '850px');
}

function newSubTicket() {
    var url = '<c:url value="/tickets/new"><c:param name="parentId" value="${ticket.id}"/><c:param name="refreshParent" value="true"/></c:url>';
    openWindow(url, '_blank', '800px', '850px');
}

function createHistComment(type, ticketChanges) {
    var timerStatus = document.getElementById('ts').value;
    var yurl = '<c:url value="/tickets/history/ticketHistoryEdit2.glml"/>?tid=${ticket.id}&thtype=' + type + '&ts=' + timerStatus;
    if (ticketChanges != null) {
        yurl += '&tchanges=' + ticketChanges;
    }
    openWindow(yurl, 'historyComment', '650px', '600px');
}

function saveForm() {
    var dTime;
    if (document.getElementById('displayWorkTime') == null) {
        dTime = '00:00:00';
    } else {
        dTime = document.getElementById('displayWorkTime').value;
    }
    document.getElementById('wt').value = dTime;
    document.forms[0].submit();
}

function refreshSelf(timerStatus) {
    var dTime;
    if (document.getElementById('displayWorkTime') == null) {
        dTime = '00:00:00';
    } else {
        dTime = document.getElementById('displayWorkTime').value;
    }
    var url = "<c:url value="/tickets/${ticket.id}/edit"><c:param name="smtl" value="${smtl}"/></c:url>" + "&dt=" + dTime + "&ts=" + timerStatus;
    location.href = url;
}

function init() {
    loadContactComboFromHidden();

    var workTimeInitialValue = "${workTimeInitialValue}";
    if (workTimeInitialValue) {
        $("#displayWorkTime").val(workTimeInitialValue);
        $("#hiddenWorkTime").val(workTimeInitialValue);
    }

    var timerStatus = "${timerStatus}";
    if (timerStatus == 'ON') {
        timerI('hiddenWorkTime', 'displayWorkTime');
    }

    var ticketId = $("form").data("ticketId");

    dojo.connect(dijit.byId("displayWorkTime"), "onChange", function() {
        changeTime('hiddenWorkTime', 'displayWorkTime');
    });

    dojo.connect(dijit.byId("displayWorkTime"), "onChange", function() {
        stopTimerI('stop','hiddenWorkTime','displayWorkTime');
    });

    dojo.query("#reloadTicket").connect("onclick", function() {
        refreshSelf(document.getElementById('ts').value);
    });

    dojo.query("#playButton").connect("onclick", function() {
        timerClicked('ON');
    });

    dojo.query("#stopButton").connect("onclick", function() {
        timerClicked('OFF');
    });

    dojo.connect(dijit.byId("save"), "onClick", function() {
        saveForm();
    });

    dojo.connect(dijit.byId("history_btn"), "onClick", function() {
        createHistComment(0);
    });

    dojo.connect(dijit.byId("callReceived"), "onClick", function() {
        createHistComment(3);
    });

    dojo.connect(dijit.byId("callPlaced"), "onClick", function() {
        createHistComment(4);
    });

    dojo.connect(dijit.byId("scheduleAppt"), "onClick", function() {
        createHistComment(1);
    });

    dojo.connect(dijit.byId("scheduleTask"), "onClick", function() {
        createHistComment(2);
    });

    dojo.connect(dijit.byId("exportPdf"), "onClick", function() {
        location.href = '<c:url value="/tickets/legacy/ticket.pdf"/>' + "?ticketId=" + ticketId;
    });

    dojo.connect(dijit.byId("convertToKb"), "onClick", function() {
        location.href = '<c:url value="/kbManagement/kbEdit.glml"/>' + "?ticketId=" + ticketId;
    });

    dojo.connect(dijit.byId("viewKb"), "onClick", function() {
        var kbId = dojo.attr(this, "data-kb-id");
        location.href = '<c:url value="/kb/kbView.glml"/>' + "?kbId=" + kbId;
    });

    dojo.query("#copyContact").connect("onclick", function() {
        copyContactToAll();
    });

    dojo.query("#assetDetails").connect("onclick", function() {
        var assetId = dojo.attr(this, "data-asset-id");
        openAssetDetailsWindow(assetId);
    });

    dojo.query(".deleteAttachment").connect("onclick", function() {
        var thistId = dojo.attr(this, "data-thist-id");
        var attachId = dojo.attr(this, "data-attach-id");
        deleteAttachment(thistId, attachId, 'history');
    });

    dojo.query("#deleteTicketAttachment").connect("onclick", function() {
        var attachId = dojo.attr(this, "data-attach-id");
        deleteAttachment(ticketId, attachId, 'ticket');
    });

    dojo.query("#newSubticket").connect("onclick", function() {
        newSubTicket();
    });

    dojo.query("#openParentTicket").connect("onclick", function() {
        var parentId = dojo.attr(this, "data-parent-id");
        go(parentId, false);
    });

    dojo.query(".subticketLink").connect("onclick", function() {
        var subTicketId = dojo.attr(this, "data-ticket-id");
        go(subTicketId, false);
    });

    dojo.query("#fileSelector").connect("onchange", function() {
        fileSelected();
    });


    dojo.connect(dijit.byId("group"), "onChange", function() {
        var contactId = dojo.attr(dojo.query("#contactId")[0], "value");
        <c:choose>
            <c:when test="${ticket.id eq null}">
                var url = "<c:url value="/tickets/new"/>?groupId=" + this.getValue();
            </c:when>
            <c:otherwise>
                var url = "<c:url value="/tickets/${ticket.id}/edit"/>?groupId=" + this.getValue();
            </c:otherwise>
        </c:choose>
        if(contactId) {
            url += "&contactId=" + contactId;
        }

        var parentId = dojo.attr(dojo.query("#parentId")[0], "value");
        if (parentId) {
            url += "&parentId=" + parentId;
        }

        showLoader();
        window.location = url;
    });

    <%--<c:if test="${clearCatAndCatOpt}">--%>
        <%--resetStore("category", categoryStore, '<c:url value="/stores/categories.json"/>', ${ticket.group.id}, null, null);--%>
        <%--clearFromPoint("categoryOption");--%>
    <%--</c:if>--%>

    var groupId = dojo.attr(dijit.byId("group"), "value");
    if (groupId != "") {
        var catId = dojo.attr(dijit.byId("category") || {value: ""}, "value");
        var catOptId = dojo.attr(dijit.byId("categoryOption") || {value: ""}, "value");
//        var locationId = dojo.attr(dijit.byId("location") || {value: ""}, "value");
        if(catOptId == "" && catId == "") {
            loadNextSelectForPrev("group", "category", categoryStore, '<c:url value="/stores/categories.json"/>');
        }

        loadCustomFields();

        dijit.byId("group").store.fetch({
            query: { id: groupId},
            onComplete: displayAssetTrack,
            onError: fetchFailed,
            queryOptions: {deep:true}
        });
    }

    dojo.connect(dijit.byId("category"), "onChange", function() {
        loadCustomFields();
        resetAssignmentStore();
        loadNextSelectForPrev('category', 'categoryOption', catOptStore, '<c:url value="/stores/catOptions.json"/>');
    });

    dojo.connect(dijit.byId("categoryOption"), "onChange", function() {
        loadCustomFields();
        resetAssignmentStore();
    });

    dojo.connect(dijit.byId("location"), "onChange", function() {
        loadCustomFields();
        resetAssignmentStore();
    });

    dojo.connect(dijit.byId("ctctCombo"), "onChange", function() {
        loadNewContact();
    });
}

function initPage() {
    dojo.addOnLoad(function(){
        try {
            dojo.parser.parse();
            init();
        } catch (err) {
            console.error("error initializing page: " + err);
        }
        
        hideLoader();
    });
}

var showLoader = function() {
    dojo.fadeIn({
        node:"preloader",
        duration:200,
        onEnd: function() {
            dojo.style("preloader", "display", "block");
        }
    }).play();
}

var hideLoader = function(){
    dojo.fadeOut({
        node:"preloader",
        duration:200,
        onEnd: function(){
            dojo.style("preloader", "display", "none");
        }
    }).play();
};

dojo.addOnLoad(initPage);

<c:if test="${successfulSave}">
    dojo.addOnLoad(function (){
        try {
            if (window.opener && !window.opener.closed && window.opener.forceRefresh) {
                window.opener.forceRefresh();
            }
        } catch (err) {
            console.debug("error refreshing parent window: " + err);
        }
    });
</c:if>

<c:if test="${showTicketHistory && ticket.ticketTemplateStatus != -1}">
    dojo.addOnLoad(function (){
        createHistComment(0, "${ticketChanges}");
    });
</c:if>

    dojo.addOnLoad(function() {
        setupDateTimePickers();
    });
</script>
