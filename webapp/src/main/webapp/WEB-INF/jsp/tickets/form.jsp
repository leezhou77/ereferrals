<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="smtl" type="java.lang.String"--%>
<%--@elvariable id="timerStatus" type="java.lang.String"--%>
<%--@elvariable id="ldapFieldList" type="java.util.List"--%>
<%--@elvariable id="ldapUser" type="net.grouplink.ehelpdesk.domain.LdapUser"--%>
<%--@elvariable id="ticketStatus" type="java.util.List"--%>
<%--@elvariable id="surveyItems" type="java.util.List"--%>
<%--@elvariable id="zenEhdVncEnabled" type="java.lang.Boolean"--%>
<%--@elvariable id="surveyCompleted" type="java.lang.Boolean"--%>

<head>
    <%--TODO split this out to a separate css file--%>
    <style type="text/css">
        article h1 {
            margin: 0;
        }

        #mainDiv .small-margin {
            margin-bottom: 0.5em;
        }
        
        table.form-table {
            width: 100%;
        }

        table.form-table td {
            padding: 2px;
        }

        table.form-table tr td:first-child {
            width: 143px;
        }

        table.form-table td img {
            width: 16px;
            height: 16px;
            vertical-align: middle;
        }

        table.form-table td label {
            margin-right: 2px;
        }

        #ticketHistoryComments .dijitTitlePaneTitle,
        #zenInformation .dijitTitlePaneTitle {
            background: #a3becc !important;
        }

        #notify-worktime img {
            vertical-align: middle;
        }

        #ticketForm {
            height: 100%;
        }

        #header_content {
            min-height: 18%;
        }

        #ticket_content {
            height: 78%;
            overflow-y: scroll;
        }

        #ticketInfoColumns {
            margin-bottom: 0;
        }
    </style>

    <link href="<c:url value="/css/smoothness/jquery-ui-1.10.1.custom.min.css"/>" type="text/css" rel="stylesheet">
    <link href="<c:url value="/css/jquery-ui-timepicker-addon.css"/>" type="text/css" rel="stylesheet">

    <script type="text/javascript" src="<c:url value="/js/jquery-ui-1.10.1.custom.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/jquery-ui-timepicker-addon.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/i18n/jquery.ui.datepicker-${fn:replace(rc.locale, '_', '-')}.js"/>"></script>

    <script type="text/javascript" src="<c:url value="/js/timer.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/TicketService.js"/>"></script>

    <%@ include file="/WEB-INF/jsp/tickets/ticketEdit.js.jsp"%>
</head>
<body>
<div id="preloader"></div>

<article class="white-bg form">

<div id="mainDiv">

<form:form name="ticketForm" id="ticketForm" modelAttribute="ticket" enctype="multipart/form-data" data-ticket-id="${ticket.id}">
<jsp:attribute name="action"><c:choose><c:when test="${ticket.id != null}"><c:url value="/tickets/${ticket.id}"/></c:when><c:otherwise><c:url value="/tickets"/></c:otherwise></c:choose></jsp:attribute>
<jsp:attribute name="method"><c:choose><c:when test="${ticket.id != null}">put</c:when><c:otherwise>post</c:otherwise></c:choose></jsp:attribute>
<jsp:body>
<%--HEADER--%>
<div id="header_content">
    <div class="small-margin">
        <h1 class="float-left">
            <c:choose>
                <c:when test="${ticket.ticketTemplateStatus eq -1}">
                    <spring:message code="ticket.edit"/><spring:message code="ticketTemplate.launch.status.pending"/>
                </c:when>
                <c:when test="${not empty ticket.ticketId}">
                    <spring:message code="ticket.edit"/>
                    <c:out value="${ticket.ticketId}"/>&nbsp;&nbsp;
                    <a id="reloadTicket" href="javascript://"
                        title="<spring:message code="ticket.reloadTicket" text="Reload Ticket"/>"><img
                        src="<c:url value="/images/records_reload_16.png"/>"
                        alt="<spring:message code="ticket.reloadTicket" text="Reload Ticket"/>"/>
                    </a>
                </c:when>
                <c:otherwise>
                    <spring:message code="ticket.newTicket"/>
                </c:otherwise>
            </c:choose>
        </h1>

        <c:if test="${smtl}">
            <a class="float-right" href="<c:url value="/home.glml" />" title="<spring:message code="header.myTickets"/>">
                <img src="<c:url value="/images/records_16.png"/>" alt="<spring:message code="header.myTickets"/>"/>
                <spring:message code="header.myTickets"/>
            </a>
        </c:if>


        <div class="float-right" id="notify-worktime">
            <c:if test="${not empty ticket.group}">
                <c:if test="${empty ticket.id}">
                    <label class="inline"><spring:message code="ticket.notify"/></label>&nbsp;
                    <ehd:authorizeGroup permission="ticket.view.notifyTech" group="${ticket.group}">
                        <form:checkbox path="notifyTech" dojoType="dijit.form.CheckBox" disabled="${!ehd:hasGroupPermission('ticket.change.notifyTech', ticket.group, pageContext)}"/>
                        <form:label path="notifyTech" cssClass="inline"><spring:message code="ROLE_TECHNICIAN"/></form:label>
                    </ehd:authorizeGroup>

                    <ehd:authorizeGroup permission="ticket.view.notifyUser" group="${ticket.group}">
                        <form:checkbox path="notifyUser" dojoType="dijit.form.CheckBox" disabled="${!ehd:hasGroupPermission('ticket.change.notifyUser', ticket.group, pageContext)}"/>
                        <form:label path="notifyUser" cssClass="inline"><spring:message code="ROLE_USER"/></form:label>
                    </ehd:authorizeGroup>
                </c:if>
            </c:if>

            <c:if test="${not empty ticket.id}">
                <ehd:authorizeGroup permission="ticket.view.workTime" group="${ticket.group}">
                    <label class="inline"><spring:message code="ticket.workTime"/></label>
                    <input type="text" size="8" id="displayWorkTime" name="displayWorkTime"
                           value="<c:out value="${ticket.displayWorkTime}" />"
                           dojoType="dijit.form.TextBox" style="width:80px" disabled="${!ehd:hasGroupPermission('ticket.change.workTime', ticket.group, pageContext)}"/>
                    <input type="hidden" id="hiddenWorkTime" name="hiddenWorkTime"
                           value="<c:out value="${ticket.displayWorkTime}" />"/>
                    <img id="playButton" src="<c:url value='/images/play.gif'/>" alt=""
                         title="<spring:message code="ticket.timer.play"/>"/>
                    <img id="stopButton" src="<c:url value='/images/stop.gif'/>" alt=""
                         title="<spring:message code="ticket.timer.stop"/>"/>
                </ehd:authorizeGroup>

            </c:if>
        </div>
        <div class="clearfix"></div>
    </div>
    <hr class="small-margin">

    <div class="columns small-margin">
        <div class="colx2-left">
            <div class="medium-margin">
                <form:label path="group" cssClass="required"><spring:message code="ticket.group"/></form:label>
                <div dojoType="dojo.data.ItemFileReadStore" jsId="groupStore" urlPreventCache="true" clearOnClose="true"
                     url="<c:url value="/stores/groups.json"><c:param name="incEmpty" value="true"/><c:param name="filterByPerm" value="group.createTicket"/></c:url>"></div>
                <form:input path="group" dojoType="dijit.form.FilteringSelect" store="groupStore" searchAttr="label" value="${ticket.group.id}"/>
            </div>
            <ehd:authorizeGroup permission="ticket.view.submittedBy" group="${ticket.group}">
                <label><spring:message code="ticket.submittedBy"/></label>
                <c:out value="${ticket.submittedBy.firstName} ${ticket.submittedBy.lastName}"/>
            </ehd:authorizeGroup>
        </div>

        <c:if test="${ticket.id != null}">
            <div class="colx2-right">
                <ehd:authorizeGroup permission="ticket.view.createdDate" group="${ticket.group}">
                <div class="medium-margin">
                    <label><spring:message code="ticket.creationDate"/></label>
                    <fmt:formatDate value="${ticket.createdDate}" type="both" dateStyle="MEDIUM" timeStyle="MEDIUM"/>
                </div>
                </ehd:authorizeGroup>
                <ehd:authorizeGroup permission="ticket.view.modifiedDate" group="${ticket.group}">
                <div class="medium-margin">
                    <label><spring:message code="ticket.modifiedDate"/></label>
                    <fmt:formatDate value="${ticket.modifiedDate}" type="both" dateStyle="MEDIUM" timeStyle="MEDIUM"/>
                </div>
                </ehd:authorizeGroup>
            </div>
        </c:if>
    </div>

    <div class="small-margin">
        <%-- Save Button --%>
        <button dojotype="dijit.form.Button" id="save" type="button">
            <img src="<c:url value="/images/save.gif"/>" alt="">
            <spring:message code="global.save" />
        </button>

        <%-- History Comments Combo Button --%>
        <c:if test="${not empty ticket.id}">
            <div id="history_btn" dojotype="dijit.form.ComboButton">
                <span>
                    <img src="<c:url value="/images/doc_add_16_hot.gif"/>" alt="">
                    <spring:message code="ticket.addComment"/>
                </span>
                <div dojotype="dijit.Menu" toggle="fade">
                    <div dojotype="dijit.MenuItem" id="callReceived"
                         iconclass="createPhoneRecd">
                        <spring:message code="phone.callReceived"/>
                    </div>
                    <div dojotype="dijit.MenuItem" id="callPlaced"
                         iconclass="createPhonePlaced">
                        <spring:message code="phone.callPlaced"/>
                    </div>

                    <c:if test="${applicationScope.groupWiseIntegration || applicationScope.groupWiseIntegration6x}">
                        <div dojotype="dijit.MenuItem" id="scheduleAppt"
                             iconclass="createScheduleAppt">
                            <spring:message code="collaboration.apptSchedule"/>
                        </div>
                        <div dojotype="dijit.MenuItem" id="scheduleTask"
                             iconClass="createScheduleTask">
                            <spring:message code="collaboration.taskSchedule"/>
                        </div>
                    </c:if>

                </div>
            </div>
        </c:if>

        <%-- PDF & Knowledge Base --%>
        <c:if test="${not empty ticket.id and ticket.ticketTemplateStatus ne -1}">
            <button dojotype="dijit.form.Button" type="button" id="exportPdf">
                <img src="<c:url value="/images/office_convert_tranparent.gif"/>" alt="">
                <spring:message code="ticketList.exportPDF" />
            </button>

            <c:if test="${ticket.status.id eq 6 or (fn:length(ticket.knowledgeBase) > 0)}">
                <c:if test="${fn:length(ticket.knowledgeBase) == 0}">

                    <button dojotype="dijit.form.Button" type="button" id="convertToKb">
                        <img src="<c:url value='/images/book_write_16.png'/>" alt="">
                        <spring:message code="kb.convert" />
                    </button>

                </c:if>
                <c:if test="${fn:length(ticket.knowledgeBase) > 0}">
                    <c:forEach items="${ticket.knowledgeBase}" var="kb">

                        <button dojotype="dijit.form.Button" type="button" id="viewKb" data-kb-id="${kb.id}">
                            <img src="<c:url value='/images/book_zoom_16.png'/>" alt="">
                            <spring:message code="kb.viewThisKb" />
                        </button>

                    </c:forEach>
                </c:if>
            </c:if>
        </c:if>
    </div>
</div>

<hr class="small-margin">

<div id="ticket_content">

<spring:hasBindErrors name="ticket">
    <p class="message error no-margin">
        <form:errors path="*"/>
    </p>
</spring:hasBindErrors>

<input type="hidden" id="tid" name="tid" value="${ticket.id}">
<input type="hidden" id="parentId" name="parentId" value="${ticket.parent.id}">
<input type="hidden" id="contactId" name="contactId" value="${ticket.contact.id}">
<input type="hidden" id="wt" name="wt" value="00:00:00">
<input type="hidden" id="ts" name="ts" value="${timerStatus}">
<input type="hidden" id="smtl" name="smtl" value="${smtl}"/>
<c:choose>
    <c:when test="${empty ticket.group}">
        <c:set var="grpdisplay" value="none"/>
    </c:when>
    <c:otherwise>
        <c:set var="grpdisplay" value="block"/>
    </c:otherwise>
</c:choose>
<div id="groupHideDiv" style="display:${grpdisplay}">
<%--CONTACT--%>
<ehd:authorizeGroup permission="ticket.view.contact" group="${ticket.group}">
<div dojotype="dijit.TitlePane" title="<spring:message code="ticket.contactInfo"/>">
    <div class="columns">
        <div class="colx2-left">
            <table class="form-table">
                <tr>
                    <td>
                        <form:label path="contact" cssClass="required">
                            <spring:message code="ticket.contact"/>
                        </form:label>
                    </td>
                    <td>
                        <div dojoType="dojox.data.QueryReadStore" jsId="userStore"
                            url="<c:url value="/stores/users.json"><c:param name="incEmpty" value="false"/></c:url>"></div>
                        <div dojoType="dijit.form.ComboBox" store="userStore" searchAttr="label" name="ctctCombo"
                           id="ctctCombo" autocomplete="false" pagesize="20"<ehd:notAuthorized permission="ticket.change.contact" group="${ticket.group}"> disabled="disabled"</ehd:notAuthorized>>
                        </div>
                        <form:hidden path="contact"/>
                        <c:if test="${ticket.ticketTemplateStatus eq -1}">
                            <a href="javascript://" id="copyContact">
                                <img src="<c:url value="/images/copy_down_16.gif"/>"
                                     alt="<spring:message code="ticketTemplate.copyContactToAllTickets"/>"
                                     title="<spring:message code="ticketTemplate.copyContactToAllTickets"/>">
                            </a><span id="contactCopySuccess" style="display:none"><img src="<c:url value="/images/ok_16.gif"/>" alt=""></span>
                        </c:if>
                    </td>
                </tr>
                <tr>
                    <td><label><spring:message code="phone.phone"/></label></td>
                    <td id="contact.phone">
                        <c:choose>
                            <c:when test="${empty ticket.contact.phoneList}">
                                <spring:message code="global.notSelected"/>
                            </c:when>
                            <c:otherwise>
                                <c:set var="phone" value="${ticket.contact.phoneList[0]}"/>
                                <c:out value="${ticket.contact.phoneList[0].number}"/>
                                (<c:out value="${ticket.contact.phoneList[0].phoneType}"/>)
                                <c:out value="${ticket.contact.phoneList[0].extension}"/>
                                <br/>
                                <c:out value="${ticket.contact.phoneList[0].bestTimeToCall}"/>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
                <ehd:authorizeGroup permission="ticket.view.location" group="${ticket.group}">
                    <tr>
                        <td><form:label path="location" cssClass="required"><spring:message code="ticket.location"/></form:label></td>
                        <td>
                            <form:select path="location" dojoType="dijit.form.FilteringSelect" disabled="${!ehd:hasGroupPermission('ticket.change.location', ticket.group, pageContext)}">
                                <form:option value="">&nbsp;</form:option>
                                <form:options items="${locations}" itemValue="id" itemLabel="name"/>
                            </form:select>
                        </td>
                    </tr>
                </ehd:authorizeGroup>
                <%-- Ldap fields --%>
                <c:set var="showLdapInfo" value="false"/>
                <c:forEach items="${ldapFieldList}" var="ldapField">
                    <c:if test="${!showLdapInfo && ldapField.showOnTicket}">
                        <c:set var="showLdapInfo" value="true"/>
                    </c:if>
                </c:forEach>

                <c:if test="${showLdapInfo}">
                    <c:set var="colIndex" value="0"/>
                    <c:forEach items="${ldapFieldList}" var="ldapField">
                        <c:if test="${ldapField.showOnTicket}">
                            <c:if test="${colIndex eq 0}">
                            <tr>
                            </c:if>

                            <td>
                                <label><c:out value="${ldapField.label}"/><c:if test="${empty ldapField.label}"><c:out
                                    value="${ldapField.name}"/></c:if></label>
                            </td>
                            <td>
                                <c:out value="${ldapUser.attributeMap[ldapField.name]}"/>
                            </td>

                            <c:choose>
                                <c:when test="${colIndex eq 0}">
                                    <c:set var="colIndex" value="1"/>
                                </c:when>
                                <c:otherwise>
                            </tr>
                                    <c:set var="colIndex" value="0"/>
                                </c:otherwise>
                            </c:choose>
                        </c:if>
                    </c:forEach>
                    <c:remove var="colIndex"/>
                </c:if>
                <c:remove var="showLdapInfo"/>
            </table>
        </div>
        <div class="colx2-right">
            <table class="form-table">
                <tr>
                    <td><label><spring:message code="user.email"/></label></td>
                    <td id="contact.email">
                    <c:choose>
                        <c:when test="${not empty ticket.contact.email}">
                            <a href="mailto:<c:out value="${ticket.contact.email}"/>">
                                <c:out value="${ticket.contact.email}"/>
                            </a>
                        </c:when>
                        <c:otherwise>
                            <spring:message code="global.notSelected"/>
                        </c:otherwise>
                    </c:choose>
                    </td>
                </tr>

                <tr>
                    <td><label><spring:message code="address.address"/></label></td>
                    <td id="contact.address">
                        <c:choose>
                            <c:when test="${empty ticket.contact.addressList}">
                                <spring:message code="global.notSelected"/>
                            </c:when>
                            <c:otherwise>
                                <c:out value="${ticket.contact.addressList[0].line1}"/>
                                <br>
                                <c:if test="${not empty ticket.contact.addressList[0].line2}">
                                    <c:out value="${ticket.contact.addressList[0].line2}"/>
                                    <br>
                                </c:if>
                                <c:out value="${ticket.contact.addressList[0].city}"/>,
                                <c:out value="${ticket.contact.addressList[0].state}"/>
                                <c:out value="${ticket.contact.addressList[0].postalCode}"/>
                                <br>
                                <c:out value="${ticket.contact.addressList[0].country}"/>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
</ehd:authorizeGroup>

<%--TICKET INFO--%>
<div dojotype="dijit.TitlePane" title="<spring:message code="ticket.ticketInfo"/>">
<div id="ticketInfoColumns" class="columns">
    <div class="colx2-left">
        <table id="col_0" class="form-table">
            <ehd:authorizeGroup permission="ticket.view.category" group="${ticket.group}">
            <tr>
                <td><form:label path="category"><spring:message code="ticket.category"/></form:label></td>
                <td>
                    <div dojoType="dojo.data.ItemFileReadStore" jsId="categoryStore" urlPreventCache="true" clearOnClose="true" id="category" hierarchical="false"
                         url="<c:url value="/stores/categories.json"><c:param name="incEmpty" value="true"/><c:param name="prevId" value="${ticket.group.id}"/></c:url>"></div>
                    <form:input path="category" dojoType="dijit.form.FilteringSelect" store="categoryStore" searchAttr="label" disabled="${!ehd:hasGroupPermission('ticket.change.category', ticket.group, pageContext)}"/>
                </td>
            </tr>
            </ehd:authorizeGroup>
            <ehd:authorizeGroup permission="ticket.view.categoryOption" group="${ticket.group}">
            <tr>
                <td><form:label path="categoryOption"><spring:message code="ticket.categoryOption"/></form:label></td>
                <td>
                    <div dojoType="dojo.data.ItemFileReadStore" jsId="catOptStore" urlPreventCache="true" clearOnClose="true" id="categoryOption"
                         url="<c:url value="/stores/catOptions.json"><c:param name="incEmpty" value="true"/><c:param name="prevId" value="${ticket.category.id}"/></c:url>"></div>
                    <form:input path="categoryOption" dojoType="dijit.form.FilteringSelect" store="catOptStore" searchAttr="label" disabled="${!ehd:hasGroupPermission('ticket.change.categoryOption', ticket.group, pageContext)}"/>
                </td>
            </tr>
            </ehd:authorizeGroup>
            <ehd:authorizeGroup permission="ticket.view.assignedTo" group="${ticket.group}">
            <tr>
                <td><form:label path="assignedTo" cssClass="required"><spring:message code="ticket.assignedTo"/></form:label></td>
                <td>
                    <div dojoType="dojo.data.ItemFileReadStore" jsId="assignedToStore" urlPreventCache="true" clearOnClose="true"
                         url="<c:url value="/stores/moreBetterAssignments.json"><c:param name="incEmpty" value="true"/><c:param name="catId" value="${ticket.category.id}"/><c:param name="catOptId" value="${ticket.categoryOption.id}"/><c:param name="groupId" value="${ticket.group.id}"/><c:param name="locationId" value="${ticket.location.id}"/><c:param name="ticketId" value="${ticket.id}"/></c:url>"></div>
                    <form:input path="assignedTo" dojoType="dijit.form.FilteringSelect" store="assignedToStore" searchAttr="label" disabled="${!ehd:hasGroupPermission('ticket.change.assignedTo', ticket.group, pageContext)}"/>
                </td>
            </tr>
            </ehd:authorizeGroup>
        </table>
    </div>
    <div class="colx2-right">
        <table id="col_1" class="form-table">
            <ehd:authorizeGroup permission="ticket.view.priority" group="${ticket.group}">
            <tr>
                <td><form:label path="priority" cssClass="required"><spring:message code="ticket.priority"/></form:label></td>
                <td>
                    <form:select path="priority" dojoType="dijit.form.FilteringSelect" disabled="${!ehd:hasGroupPermission('ticket.change.priority', ticket.group, pageContext)}">
                        <form:options items="${priorities}" itemLabel="name" itemValue="id"/>
                    </form:select>
                </td>
            </tr>
            </ehd:authorizeGroup>
            <ehd:authorizeGroup permission="ticket.view.status" group="${ticket.group}">
            <tr>
                <td><form:label path="status" cssClass="required"><spring:message code="ticket.status"/></form:label></td>
                <td>
                    <c:choose>
                        <c:when test="${ticket.status.id eq -1}">
                            <spring:message code="global.deleted"/>
                        </c:when>
                        <c:otherwise>
                            <form:select path="status" dojoType="dijit.form.FilteringSelect" disabled="${!ehd:hasGroupPermission('ticket.change.status', ticket.group, pageContext)}">
                                <form:options items="${ticketStatus}" itemLabel="name" itemValue="id"/>
                            </form:select>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
            </ehd:authorizeGroup>
            <ehd:authorizeGroup permission="ticket.view.estimatedDate" group="${ticket.group}">
            <tr>
                <td><form:label path="estimatedDate"><spring:message code="ticket.estimatedDate"/></form:label></td>
                <td>
                    <form:input path="estimatedDate" dojoType="dijit.form.DateTextBox" disabled="${!ehd:hasGroupPermission('ticket.change.estimatedDate', ticket.group, pageContext)}"/>
                </td>
            </tr>
            </ehd:authorizeGroup>
            <c:if test="${ticket.group.assetTrackerType eq 'EHD'}">
                <ehd:authorizeGroup permission="ticket.view.asset" group="${ticket.group}">
                <tr>
                    <td><form:label path="asset"><spring:message code="ticket.asset.name"/></form:label></td>
                    <td>
                        <form:input path="asset" dojoType="dijit.form.TextBox" value="${ticket.asset.assetNumber}" disabled="${!ehd:hasGroupPermission('ticket.change.asset', ticket.group, pageContext)}"/>
                        <a href="javascript:assetSearch();">
                            <img class="icon_16" src="<c:url value='/images/find2.gif'/>" alt="">
                        </a>
                        <c:if test="${ticket.asset != null}">
                            <br>
                            <a href="javascript://" id="assetDetails" data-asset-id="${ticket.asset.id}">
                            <c:out value="${ticket.asset.name}"/>
                            </a>

                            <c:if test="${!empty ticket.asset.hostname && !empty ticket.asset.vncPort}">
                                <br>
                                <a href="<c:url value="/jnlp/vncviewerAsset.jnlp"><c:param name="assetId"
                                                    value="${ticket.asset.id}"/><c:param name="ticketId" value="${ticket.id}"/></c:url>">
                                    <img src="<c:url value="/images/RMPolicy.png"/>" alt="<spring:message code="remoteControl.remoteControl"/>">
                                </a>
                            </c:if>
                        </c:if>
                    </td>
                </tr>
                </ehd:authorizeGroup>
            </c:if>
            <c:if test="${ticket.group.assetTrackerType eq 'ZEN10'}">
                <ehd:authorizeGroup permission="ticket.view.zenAsset" group="${ticket.group}">
                <tr>
                    <td><form:label path="zenAsset"><spring:message code="ticket.asset.name"/></form:label></td>
                    <td>
                        <c:set var="hasChangeZenPerm" value="${ehd:hasGroupPermission('ticket.change.zenAsset', ticket.group, pageContext)}"/>
                        <form:input path="zenAsset" dojoType="dijit.form.TextBox" value="${ticket.zenAsset.id}" disabled="${!hasChangeZenPerm}"/>
                        <c:if test="${hasChangeZenPerm}">
                            <a href="javascript:zenAssetSearch();">
                                <img src="<c:url value='/images/find2.gif'/>" alt="">
                            </a>
                        </c:if>
                        <c:if test="${not empty ticket.zenAsset}">
                            <br>
                            <a href="javascript:openZenAssetDetailsWindow('<c:out value="${ticket.zenAsset.deviceIdString}"/>', 800, 600);"><c:out value="${ticket.zenAsset.assetName}"/></a>

                            <c:if test="${zenEhdVncEnabled && not empty ticket.zenAsset}">
                                <a href="<c:url value="/jnlp/vncviewer.jnlp"><c:param name="zenAssetId"
                                            value="${ticket.zenAsset.id}"/><c:param name="ticketId" value="${ticket.id}"/></c:url>"><img src="<c:url value="/images/RMPolicy.png"/>" alt="<spring:message code="remoteControl.remoteControl"/>"></a>
                            </c:if>
                        </c:if>
                        <c:remove var="hasChangeZenPerm"/>
                    </td>
                </tr>
                </ehd:authorizeGroup>
            </c:if>
        </table>
    </div>
</div>
<div class="columns">
    <div class="colx2-left">
        <table id="cfTable_0" class="form-table"></table>
    </div>
    <div class="colx2-right">
        <table id="cfTable_1" class="form-table"></table>
    </div>
</div>
</div>

<%--Description--%>
<div dojotype="dijit.TitlePane" title="<spring:message code="ticket.description"/>">
    <table class="form-table">
        <ehd:authorizeGroup permission="ticket.view.subject" group="${ticket.group}">
        <tr>
            <td><form:label path="subject" cssClass="required"><spring:message code="ticket.subject"/></form:label></td>
            <td style="width:85%"><form:input path="subject" dojoType="dijit.form.ValidationTextBox" maxlength="255" cssStyle="width:100%" disabled="${!ehd:hasGroupPermission('ticket.change.subject', ticket.group, pageContext)}"/></td>
        </tr>
        </ehd:authorizeGroup>
        <ehd:authorizeGroup permission="ticket.view.cc" group="${ticket.group}">
        <tr>
            <td><form:label path="cc"><spring:message code="ticket.cc"/></form:label></td>
            <td style="width:85%">
                <form:input path="cc" dojoType="dijit.form.ValidationTextBox"
                            regExpGen="dojox.validate.regexp.emailAddressList"
                            maxlength="255"
                            cssStyle="width:100%"
                            disabled="${!ehd:hasGroupPermission('ticket.change.cc', ticket.group, pageContext)}">
                    <jsp:attribute name="invalidMessage"><spring:message code="mail.validation.address"/></jsp:attribute>
                </form:input>
            </td>
        </tr>
        </ehd:authorizeGroup>
        <ehd:authorizeGroup permission="ticket.view.bc" group="${ticket.group}">
        <tr>
            <td><form:label path="bc"><spring:message code="ticket.bcc"/></form:label></td>
            <td style="width:85%">
                <form:input path="bc" dojoType="dijit.form.ValidationTextBox"
                            regExpGen="dojox.validate.regexp.emailAddressList"
                            maxlength="255"
                            cssStyle="width:100%"
                            disabled="${!ehd:hasGroupPermission('ticket.change.bc', ticket.group, pageContext)}">
                    <jsp:attribute name="invalidMessage"><spring:message code="mail.validation.address"/></jsp:attribute>
                </form:input>
            </td>
        </tr>
        </ehd:authorizeGroup>
        <ehd:authorizeGroup permission="ticket.view.note" group="${ticket.group}">
        <tr>
            <td><form:label path="note"><spring:message code="ticket.note"/></form:label></td>
            <td style="width:85%">
                <form:textarea path="note" dojoType="dijit.form.Textarea" rows="" cols="" cssStyle="width:100%" disabled="${!ehd:hasGroupPermission('ticket.change.note', ticket.group, pageContext)}"/>
            </td>
        </tr>
        </ehd:authorizeGroup>
    </table>
</div>

<%--Attachments--%>
<ehd:authorizeGroup permission="ticket.view.attachments" group="${ticket.group}">
<div dojoType="dijit.TitlePane" title="<spring:message code="ticket.attachments"/>">
    <table class="form-table">
        <tr>
            <td>
                <c:forEach var="attach" items="${ticket.attachments}" varStatus="status">
                    <div id="attachmentDiv_${attach.id}">
                        <a href="<c:url value="/ticket/attachment/handleDownload.glml" ><c:param name="aid" value="${attach.id}" /></c:url>">
                            <c:out value="${attach.fileName}"/>
                        </a>

                        <a href="javascript://" id="deleteTicketAttachment" data-attach-id="${attach.id}" title="<spring:message code="ticket.deleteAttachment"/>">
                            <img alt="<spring:message code="ticket.deleteAttachment"/>" src="<c:url value="/images/delete.gif" />"/>
                        </a>

                    </div>
                </c:forEach>
                <div id="fileLoader_EditDiv">
                    <input type="file" name="attchmnt[0]" size="75" id="fileSelector"<ehd:notAuthorized permission="ticket.change.attachments" group="${ticket.group}"> disabled="disabled"</ehd:notAuthorized>/><br/>
                </div>
            </td>
        </tr>
    </table>
</div>
</ehd:authorizeGroup>

<%--Ticket History--%>
<ehd:authorizeGroup permission="ticket.view.ticketHistory" group="${ticket.group}">
<div dojotype="dijit.TitlePane" title="<spring:message code="global.comments"/>">
    <c:choose>
        <c:when test="${fn:length(ticket.ticketHistoryList) > 0}">
            <c:forEach items="${ticket.ticketHistoryList}" var="thist">
                <div id="ticketHistoryComments">
                    <div dojoType="dijit.TitlePane" title="
                        <c:choose>
                            <c:when test="${thist.user.id == 0}">
                                <spring:message code="global.systemUserName"/>
                            </c:when>
                            <c:otherwise>
                                <c:out value="${thist.user.firstName}"/>
                                <c:out value="${thist.user.lastName}"/>
                            </c:otherwise>
                        </c:choose>
                        -
                        <c:out value="${thist.subject}"/>



                        <fmt:formatDate value="${thist.createdDate}" type="both" timeStyle="MEDIUM"
                                        dateStyle="MEDIUM"/>
                    ">

                        <div class="commentBody">
                            <ehd:formatHistoryNote ticketHistory="${thist}"/>
                        </div>

                        <c:if test="${not empty thist.attachments}">

                            <label id="ticketHistoryAttachmentsLabel">
                                <spring:message code="ticket.attachments"/>:
                            </label>
                            <div id="ticketHistoryAttachments">
                                <c:forEach items="${thist.attachments}" var="attach" varStatus="status">
                                    <div id="attachmentDivTicketHistory_${attach.id}">
                                        <a href="<c:url value="/ticket/attachment/handleDownload.glml" ><c:param
                            name="aid" value="${attach.id}" /></c:url>"><c:out value="${attach.fileName}"/></a>

                                        <a href="javascript://" title="<spring:message code="ticket.deleteAttachment"/>" class="deleteAttachment" data-thist-id="${thist.id}" data-attach-id="${attach.id}">
                                            <img alt="<spring:message code="ticket.deleteAttachment"/>" src="<c:url value="/images/delete.gif" />"/>
                                        </a>

                                    </div>
                                </c:forEach>
                            </div>
                        </c:if>

                    </div>
                </div>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <p class="with-padding"><spring:message code="ticket.noComments"/></p>
        </c:otherwise>
    </c:choose>
</div>
</ehd:authorizeGroup>

</div>

<c:if test="${not empty ticket.id}">
<%--Sub-Ticket--%>
<ehd:authorizeGroup permission="ticket.view.subtickets" group="${ticket.group}">
<div dojoType="dijit.TitlePane" title="<spring:message code="ticket.subtickets"/>">
    <div class="block-content no-title">
        <div class="block-controls">
            <ul class="controls-buttons">
                <ehd:authorizeGroup permission="ticket.change.subtickets" group="${ticket.group}">
                <li>
                    <a href="javascript://" id="newSubticket">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>"/>
                        <spring:message code="ticket.createNewSub"/>
                    </a>
                </li>
                </ehd:authorizeGroup>
                <c:if test="${not empty ticket.parent.id}">
                    <ehd:authorizeGroup permission="ticket.view.parent" group="${ticket.group}">
                    <li>
                        <a href="javascript://" id="openParentTicket" data-parent-id="${ticket.parent.id}">
                            <spring:message code="ticket.openParent"/>
                        </a>
                    </li>
                    </ehd:authorizeGroup>
                </c:if>
            </ul>
        </div>
        <div class="no-margin">
            <c:choose>
                <c:when test="${fn:length(ticket.subtickets) > 0}">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th><spring:message code="ticket.subject"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="ticket" items="${ticket.subTicketsList}">
                            <tr class="subticketLink" data-ticket-id="${ticket.id}">
                                <td>
                                    <c:out value="${ticket.id}"/>
                                </td>
                                <td>
                                    <c:out value="${ticket.subject}"/>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <p class="with-padding">
                        <spring:message code="ticket.noSubTickets"/>
                    </p>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>
</ehd:authorizeGroup>

<%-- Zen 7 Asset Info --%>
<c:if test="${fn:length(ticket.zenInformation) > 0}">
    <ehd:authorizeGroup permission="ticket.view.zenInformation" group="${ticket.group}">
    <div dojotype="dijit.TitlePane" title="<spring:message code="ticket.zenInformation"/>" open="false">
        <c:forEach items="${ticket.zenInformation}" var="zen" varStatus="index">
            <div id="zenInformation">
                <div dojoType="dijit.TitlePane" title="<c:out escapeXml="false" value="${zen.dn}"/>">
                    <table class="form-table">
                        <c:if test="${not empty zen.dn}">
                        <tr>
                            <td><label>DN</label></td>
                            <td>
                                <c:out escapeXml="false" value="${zen.dn}"/>
                            </td>
                        </tr>
                        </c:if>
                        <c:if test="${not empty zen.assetTag}">
                        <tr>
                            <td><label>zENINVAssetTag</label></td>
                            <td>
                                <c:out escapeXml="false" value="${zen.assetTag}"/>
                            </td>
                        </tr>
                        </c:if>
                        <c:if test="${not empty zen.biosType}">
                        <tr>
                            <td><label>zENINVBIOSType</label></td>
                            <td>
                                <c:out escapeXml="false" value="${zen.biosType}"/>
                            </td>
                        </tr>
                        </c:if>
                        <c:if test="${not empty zen.computerModel}">
                        <tr>
                            <td><label>zENINVComputerModel</label></td>
                            <td>
                                <c:out escapeXml="false" value="${zen.computerModel}"/>
                            </td>
                        </tr>
                        </c:if>
                        <c:if test="${not empty zen.diskInfo}">
                        <tr>
                            <td><label>zENINVDiskInfo</label></td>
                            <td>
                                <c:out escapeXml="false" value="${zen.diskInfo}"/>
                            </td>
                        </tr>
                        </c:if>
                        <c:if test="${not empty zen.ipAddress}">
                        <tr>
                            <td><label>zENINVIPAddress</label></td>
                            <td>
                                <c:out escapeXml="false" value="${zen.ipAddress}"/>
                            </td>
                        </tr>
                        </c:if>
                        <c:if test="${not empty zen.lastUser}">
                        <tr>
                            <td><label>wMNAMEUser</label></td>
                            <td>
                                <c:out escapeXml="false" value="${zen.lastUser}"/>
                            </td>
                        </tr>
                        </c:if>
                        <c:if test="${not empty zen.loggedInWorkstation}">
                        <tr>
                            <td><label>zenwmLoggedInWorkstation</label></td>
                            <td>
                                <c:out escapeXml="false" value="${zen.loggedInWorkstation}"/>
                            </td>
                        </tr>
                        </c:if>
                        <c:if test="${not empty zen.macAddress}">
                        <tr>
                            <td><label>zENINVMACAddress</label></td>
                            <td>
                                <c:out escapeXml="false" value="${zen.macAddress}"/>
                            </td>
                        </tr>
                        </c:if>
                        <c:if test="${not empty zen.memorySize}">
                        <tr>
                            <td><label>zENINVMemorySize</label></td>
                            <td>
                                <c:out escapeXml="false" value="${zen.memorySize}"/>
                            </td>
                        </tr>
                        </c:if>
                        <c:if test="${not empty zen.modelNumber}">
                        <tr>
                            <td><label>zENINVModelNumber</label></td>
                            <td>
                                <c:out escapeXml="false" value="${zen.modelNumber}"/>
                            </td>
                        </tr>
                        </c:if>
                        <c:if test="${not empty zen.ncv}">
                        <tr>
                            <td><label>zENINVNovellClientVersion</label></td>
                            <td>
                                <c:out escapeXml="false" value="${zen.ncv}"/>
                            </td>
                        </tr>
                        </c:if>
                        <c:if test="${not empty zen.nicType}">
                        <tr>
                            <td><label>zENINVNICType</label></td>
                            <td>
                                <c:out escapeXml="false" value="${zen.nicType}"/>
                            </td>
                        </tr>
                        </c:if>
                        <c:if test="${not empty zen.operator}">
                        <tr>
                            <td><label>operator</label></td>
                            <td>
                                <c:out escapeXml="false" value="${zen.operator}"/>
                            </td>
                        </tr>
                        </c:if>
                        <c:if test="${not empty zen.osRevision}">
                        <tr>
                            <td><label>zENINVOSRevision</label></td>
                            <td>
                                <c:out escapeXml="false" value="${zen.osRevision}"/>
                            </td>
                        </tr>
                        </c:if>
                        <c:if test="${not empty zen.osType}">
                        <tr>
                            <td><label>zENINVOSType</label></td>
                            <td>
                                <c:out escapeXml="false" value="${zen.osType}"/>
                            </td>
                        </tr>
                        </c:if>
                        <c:if test="${not empty zen.processorType}">
                        <tr>
                            <td><label>zENINVProcessorType</label></td>
                            <td>
                                <c:out escapeXml="false" value="${zen.processorType}"/>
                            </td>
                        </tr>
                        </c:if>
                        <c:if test="${not empty zen.serialNumber}">
                        <tr>
                            <td><label>zENINVSerialNumber</label></td>
                            <td>
                                <c:out escapeXml="false" value="${zen.serialNumber}"/>
                            </td>
                        </tr>
                        </c:if>
                        <c:if test="${not empty zen.subnetAddress}">
                        <tr>
                            <td><label>zENINVSubNetAddress</label></td>
                            <td>
                                <c:out escapeXml="false" value="${zen.subnetAddress}"/>
                            </td>
                        </tr>
                        </c:if>
                        <c:if test="${not empty zen.userHistory}">
                        <tr>
                            <td><label>wMUserHistory</label></td>
                            <td>
                                <c:out escapeXml="false" value="${zen.userHistory}"/>
                            </td>
                        </tr>
                        </c:if>
                        <c:if test="${not empty zen.videoType}">
                        <tr>
                            <td><label>zENINVVideoType</label></td>
                            <td>
                                <c:out escapeXml="false" value="${zen.videoType}"/>
                            </td>
                        </tr>
                        </c:if>
                        <c:if test="${not empty zen.workstation}">
                        <tr>
                            <td><label>wMWorkstation</label></td>
                            <td>
                                <c:out escapeXml="false" value="${zen.workstation}"/>
                            </td>
                        </tr>
                        </c:if>
                        <c:if test="${not empty zen.computerName}">
                        <tr>
                            <td><label>wMNAMEComputer</label></td>
                            <td>
                                <c:out escapeXml="false" value="${zen.computerName}"/>
                            </td>
                        </tr>
                        </c:if>
                    </table>
                </div>
            </div>
        </c:forEach>
    </div>
    </ehd:authorizeGroup>
</c:if>

<%--Ticket Audit--%>
<ehd:authorizeGroup permission="ticket.view.ticketAudit" group="${ticket.group}">
<div dojoType="dijit.TitlePane" title="<spring:message code="ticket.auditTitle"/>" open="false">
    <table class="table">
        <thead>
        <tr>
            <th><spring:message code="ticket.auditFieldName"/></th>
            <th><spring:message code="ticket.auditFrom"/></th>
            <th><spring:message code="ticket.auditTo"/></th>
            <th><spring:message code="ticket.auditOn"/></th>
            <th><spring:message code="ticket.auditBy"/></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><spring:message code="ticket.auditCreated"/></td>
            <td></td>
            <td></td>
            <td>
                <ehd:authorizeGroup permission="ticket.view.createdDate" group="${ticket.group}">
                    <fmt:formatDate value="${ticket.createdDate}" type="both" dateStyle="MEDIUM"
                                    timeStyle="MEDIUM"/>
                </ehd:authorizeGroup>
            </td>
            <td>
                <ehd:authorizeGroup permission="ticket.view.submittedBy" group="${ticket.group}">
                    <c:out value="${ticket.submittedBy.loginId}"/>
                </ehd:authorizeGroup>
            </td>
        </tr>
        <c:forEach items="${ticket.ticketAudit}" var="audit">
            <tr>
                <td><spring:message code="${audit.property}" text="${audit.property}"/></td>
                <td><c:out value="${audit.beforeValueDescription}"/></td>
                <td><c:out value="${audit.afterValueDescription}"/></td>
                <td><fmt:formatDate value="${audit.dateStamp}" type="both" dateStyle="MEDIUM"
                                    timeStyle="MEDIUM"/></td>
                <td><c:out value="${audit.user.loginId}"/></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</ehd:authorizeGroup>

<%--Survey Results--%>
<ehd:authorizeGroup permission="ticket.view.surveyData" group="${ticket.group}">
<div dojoType="dijit.TitlePane" title="<spring:message code="ticket.surveyResults"/>" open="false">
    <c:choose>
        <c:when test="${surveyCompleted == true}">
            <c:forEach items="${surveyItems}" var="surveyItem" varStatus="index">
                <div class="small-margin">
                    <div>
                        <label><c:out value="${surveyItem.question}"/></label>
                    </div>
                    <div>
                        <c:out value="${surveyItem.response}"/>
                    </div>
                </div>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <span><spring:message code="ticket.surveyNotSubmitted"/></span>
        </c:otherwise>
    </c:choose>
</div>
</ehd:authorizeGroup>
</c:if>
</div>
</jsp:body>
</form:form>
</div>
</article>

<%@ include file="/WEB-INF/jsp/tickets/customFieldTemplates.jsp" %>

</body>
