<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ehd" uri="/WEB-INF/tlds/ehelpdesk.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="authz" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<tags:jsTemplate id="cf_text">
    <tr>
        <td><label for="{{ cf_dom_id }}">{{ cf_label }}</label></td>
        <td>
            <input type="text" id="{{ cf_dom_id }}" name="{{ cf_name }}" value="{{ cf_value }}" dojoType="dijit.form.TextBox">
        </td>
    </tr>
</tags:jsTemplate>
<tags:jsTemplate id="cf_textarea">
    <tr>
        <td><label for="{{ cf_dom_id }}">{{ cf_label }}</label></td>
        <td>
            <input type="textarea" id="{{ cf_dom_id }}" name="{{ cf_name }}" value="{{ cf_value }}" dojoType="dijit.form.Textarea" style="width:182px">
        </td>
    </tr>
</tags:jsTemplate>
<tags:jsTemplate id="cf_radio">
    <tr>
        <td><label for="{{ cf_dom_id }}">{{ cf_label }}</label></td>
        <td>
            {{ cf_options }}
            <input type="hidden" name="_{{ cf_name }}" value="{{ cf_value }}">
        </td>
    </tr>
</tags:jsTemplate>
<tags:jsTemplate id="cf_radio_option">
    <input type="radio" id="{{ cf_dom_id }}" name="{{ cf_name }}" value="{{ cf_label }}" dojoType="dijit.form.RadioButton"><label for="{{ cf_dom_id }}" class="inline">{{ cf_label }}</label>
</tags:jsTemplate>
<tags:jsTemplate id="cf_checkbox">
    <tr>
        <td><label for="{{ cf_dom_id }}">{{ cf_label }}</label></td>
        <td>
            <input type="checkbox" name="{{ cf_name }}" id="{{ cf_dom_id }}" dojoType="dijit.form.CheckBox">
            <input name="_{{ cf_name }}" type="hidden" value="false"/>
        </td>
    </tr>
</tags:jsTemplate>
<tags:jsTemplate id="cf_select">
    <tr>
        <td><label for="{{ cf_dom_id }}">{{ cf_label }}</label></td>
        <td>
            <select id="{{ cf_dom_id }}" name="{{ cf_name }}" dojoType="dijit.form.FilteringSelect" style="width:180px;">
                {{ cf_options }}
            </select>
        </td>
    </tr>
</tags:jsTemplate>
<tags:jsTemplate id="cf_select_option">
    <option value="{{ cf_option_label }}">{{ cf_option_label }}</option>
</tags:jsTemplate>

<tags:jsTemplate id="cf_number">
    <tr>
        <td><label for="{{ cf_dom_id }}">{{ cf_label }}</label></td>
        <td>
            <input type="text" id="{{ cf_dom_id }}" name="{{ cf_name }}" value="{{ cf_value }}" dojoType="dijit.form.NumberTextBox">
        </td>
    </tr>
</tags:jsTemplate>
<tags:jsTemplate id="cf_date">
    <tr>
        <td><label for="{{ cf_dom_id }}">{{ cf_label }}</label></td>
        <td>
            <input type="text" id="{{ cf_dom_id }}" name="{{ cf_name }}" value="{{ cf_value }}" dojoType="dijit.form.DateTextBox">
        </td>
    </tr>
</tags:jsTemplate>
<tags:jsTemplate id="cf_datetime">
    <tr>
        <td><label for="{{ cf_dom_id }}">{{ cf_label }}</label></td>
        <td>
            <input class="dtpicker" type="text" id="{{ cf_dom_id }}" name="{{ cf_name }}" value="{{ cf_value }}">
        </td>
    </tr>
</tags:jsTemplate>
<tags:jsTemplate id="cf_time">
    <tr>
        <td><label for="{{ cf_dom_id }}">{{ cf_label }}</label></td>
        <td>
            <input class="tpicker" type="text" id="{{ cf_dom_id }}" name="{{ cf_name }}" value="{{ cf_value }}">
        </td>
    </tr>
</tags:jsTemplate>