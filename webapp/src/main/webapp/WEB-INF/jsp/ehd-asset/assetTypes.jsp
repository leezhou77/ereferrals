<%@ include file="/WEB-INF/jsp/include.jsp" %>

<head>
    <meta name="assetSidebar" content="true">
    <title><spring:message code="asset.types"/></title>

    <script type="text/javascript">
        function viewAssetType(assetTypeId) {
            var url = '<c:url value="/asset/viewAssetType.glml"/>';
            if (assetTypeId != null)url += '?assetTypeId=' + assetTypeId;
            window.location.href = url;
        }

        var nonDelIds = {
            <c:forEach var="assetType" items="${nonDelAssetTypes}" varStatus="status">
            "${assetType.id}":''<c:if test="${not status.last}">,</c:if>
            </c:forEach>
        };

        function deleteAssetType(assetTypeId) {
            // avoid a db error if trying to delete a referenced assettype
            if (assetTypeId in nonDelIds) {
                alert('<spring:message code="asset.type.nodelete"/>');
                return;
            }
            // OK to delete
            if (confirm('<spring:message code="global.confirmdelete"/>')) {
                window.location.href = '<c:url value="/asset/deleteAssetType.glml"/>?assetTypeId=' + assetTypeId;
            }
        }

        function init() {
        }

        dojo.addOnLoad(init)

    </script>
</head>

<body>


<section class="grid_8">
    <div class="block-content">
        <h1><spring:message code="asset.types"/></h1>
        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a href="<c:url value="/asset/viewAssetType.glml"/>">
                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="">
                        <spring:message code="assetTracker.typeNew"/>
                    </a>
                </li>
            </ul>
        </div>

        <div class="no-margin">

        <table class="table">
            <thead>
            <tr>
                <th scope="col"><spring:message code="global.name"/></th>
                <th scope="col"><spring:message code="global.description"/></th>
                <th scope="col" class="table-actions"><spring:message code="global.actions"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${assetTypes}" var="assetType">
                <tr>
                    <td><c:out value="${assetType.name}"/></td>
                    <td><c:out value="${assetType.notes}"/></td>
                    <td class="table-actions">
                        <a href="javascript:viewAssetType(<c:out value="${assetType.id}" />);">
                            <img alt="" src="<c:url value="/images/theme/icons/fugue/pencil.png" />"/></a>
                        <a href="javascript:deleteAssetType(<c:out value="${assetType.id}" />);">
                            <img alt="" src="<c:url value="/images/theme/icons/fugue/cross-circle.png" />"/></a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        </div>
    </div>
</section>
</body>
