<%@ include file="/WEB-INF/jsp/include.jsp" %>

<head>

    <meta name="assetSidebar" content="true">
    <title><spring:message code="softwareLicense.edit"/></title>
    <script type="text/javascript">

        function cancelForm() {
            window.location.href = '<c:url value="/asset/softwareLicenses.glml"/>';
        }

        function init() {
        }

        dojo.addOnLoad(init);

    </script>
</head>

<body>

<section class="grid_5">
    <div class="block-content">
        <h1><spring:message code="softwareLicense.edit"/></h1>

        <form:form commandName="softwareLicense" method="post" cssClass="form">
            <spring:hasBindErrors name="vendor">
                <p class="message error no-margin">
                    <form:errors path="*"/>
                </p>
            </spring:hasBindErrors>

            <input type="hidden" name="softwareLicenseId" value="${softwareLicense.id}"/>

            <p class="required">
                <form:label path="productName"><spring:message code="softwareLicense.productName"/></form:label>
                <form:input path="productName" cssErrorClass="error" dojoType="dijit.form.TextBox" maxlength="255"/>
            </p>

            <p>
                <form:label path="productVersion"><spring:message code="softwareLicense.productVersion"/></form:label>
                <form:input path="productVersion" cssErrorClass="error" dojoType="dijit.form.TextBox" maxlength="255"/>
            </p>

            <p>
                <form:label path="vendor"><spring:message code="softwareLicense.vendor"/></form:label>
                <form:select path="vendor" cssErrorClass="error">
                    <form:option value="">&nbsp;</form:option>
                    <form:options items="${vendors}" itemValue="id" itemLabel="name"/>
                </form:select>
            </p>

            <p>
                <form:label path="numberOfLicenses"><spring:message code="softwareLicense.numberOfLicenses"/></form:label>
                <form:input path="numberOfLicenses" cssErrorClass="error" dojoType="dijit.form.TextBox" maxlength="11"/>
            </p>

            <p>
                <form:label path="purchaseDate"><spring:message code="softwareLicense.purchaseDate"/></form:label>
                <form:input path="purchaseDate" cssErrorClass="error" dojoType="dijit.form.DateTextBox"/>
            </p>

            <p>
                <form:label path="supportExpirationDate"><spring:message code="softwareLicense.supportExpirationDate"/></form:label>
                <form:input path="supportExpirationDate" cssErrorClass="error" dojoType="dijit.form.DateTextBox"/>
            </p>

            <p>
                <form:label path="note"><spring:message code="softwareLicense.notes"/></form:label>
                <form:input path="note" cssErrorClass="error" dojoType="dijit.form.TextBox"/>
            </p>

            <p>
                <span class="label"><spring:message code="softwareLicense.numberInstalled"/></span>
                ${fn:length(softwareLicense.installedOnAssets)}
            </p>

            <p class="grey-bg no-margin" id="commandButtons">
                <button type="submit"><spring:message code="global.save"/></button>
                <button type="button" onclick="cancelForm();"><spring:message
                        code="global.cancel"/></button>
            </p>
        </form:form>
    </div>
</section>
</body>
