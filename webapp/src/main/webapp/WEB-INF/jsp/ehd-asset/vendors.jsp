<%@ include file="/WEB-INF/jsp/include.jsp" %>

<head>
    <meta name="assetSidebar" content="true">
    <title><spring:message code="assetTracker.vendors"/></title>

    <script type="text/javascript">

        function viewVendor(vendorId) {
            var url = '<c:url value="/asset/editVendor.glml"/>';
            if (vendorId != null) url += '?vendorId=' + vendorId;
            window.location.href = url;
        }

        function importVendor() {
            window.location.href = '<c:url value="/asset/importVendor.glml"/>';
        }

        var nonDelIds = {
            <c:forEach var="vendor" items="${nonDelVendors}" varStatus="status">
            "${vendor.id}":''<c:if test="${not status.last}">,</c:if>
            </c:forEach>
        };

        function deleteVendor(vendorId) {
            // avoid a db error if trying to delete a referenced vendor
            if (vendorId in nonDelIds) {
                alert('<spring:message code="asset.vendor.nodelete"/>');
                return;
            }
            // OK to delete
            if (confirm('<spring:message code="global.confirmdelete"/>')) {
                window.location.href = '<c:url value="/asset/deleteVendor.glml"/>?vendorId=' + vendorId;
            }
        }

        function init() {
        }

        dojo.addOnLoad(init);

    </script>
</head>

<body>

<section class="grid_8">
    <div class="block-content">
        <h1><spring:message code="assetTracker.vendors"/></h1>
        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a href="<c:url value="/asset/editVendor.glml"/>">
                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="">
                        <spring:message code="vendor.new"/>
                    </a>
                </li>
                <li>
                    <a href="javascript:importVendor();">
                        <img src="<c:url value="/images/import.png" />" style="width:18px;height:18px;" alt="">
                        <spring:message code="vendor.import"/>
                    </a>
                </li>
            </ul>
        </div>

        <div class="no-margin">

        <table class="table">
            <thead>
            <tr>
                <th scope="col"><spring:message code="global.name"/></th>
                <th scope="col"><spring:message code="vendor.primaryPhone"/></th>
                <th scope="col"><spring:message code="vendor.email"/></th>
                <th scope="col" class="table-actions"><spring:message code="global.actions"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="vendor" items="${vendors}">
                <tr>
                    <td><c:out value="${vendor.name}"/></td>
                    <td><c:out value="${vendor.primaryPhone}"/></td>
                    <td><c:out value="${vendor.email}"/></td>
                    <td class="table-actions">
                        <a href="javascript:viewVendor(<c:out value="${vendor.id}" />);">
                            <img alt="" src="<c:url value="/images/theme/icons/fugue/pencil.png" />"/></a>
                        <a href="javascript:deleteVendor(<c:out value="${vendor.id}" />);">
                            <img alt="" src="<c:url value="/images/theme/icons/fugue/cross-circle.png" />"/></a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        </div>
    </div>
</section>
</body>
