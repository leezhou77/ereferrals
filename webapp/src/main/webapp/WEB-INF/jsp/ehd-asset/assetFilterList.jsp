<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="listHolder" type="java.util.Map"--%>
<%--@elvariable id="refreshUrl" type="java.lang.String"--%>
<%--@elvariable id="assignedTicketCount" type="java.lang.Integer"--%>
<%--@elvariable id="colOrder" type="java.lang.String[]"--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
    <%--This is required by the scrolltable.js because it doesn't allow style expressions in IE8--%>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
        
    <spring:theme code="css" var="cssfile"/>
    <c:if test="${not empty cssfile}">
        <link rel="stylesheet" href="<c:url value="${cssfile}"/>" type="text/css">
    </c:if>
    <style type="text/css">
        @import "<c:url value="/js/dojo-1.6.2/dijit/themes/tundra/tundra.css"/>";
    </style>

    <script type="text/javascript" src="<c:url value="/js/ticketTable.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/scrollabletable.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/scripts.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/dojo-1.6.2/dojo/dojo.js"/>"
            djConfig="parseOnLoad: true,
            locale: '<tags:dojoLocale locale="${rc.locale}" />'">
    </script>
    <script type="text/javascript" src="<c:url value="/js/dojo-1.6.2/grouplink/includes.js"/>"></script>

    <script type="text/javascript">

        function go(assetId) {
            var url = "<c:url value="/asset/assetEdit.glml"/>";
            if (assetId) url += "?assetId=" + assetId;
            openWindow(url, '_' + assetId, '600px', '600px');
        }

        function checkAllCbx(checkIt) {
            var cbxs = document.getElementsByName("assetCbx");
            for (var i = 0; i < cbxs.length; i++) {
                cbxs[i].checked = checkIt;
            }
        }

        function massDeleteAssets() {
            if (confirm("<spring:message code="assetFilter.delete.confirm"/>")) {
                var theForm = document.forms[0];
                theForm.action = '<c:url value="/asset/filter/massDelete.glml"/>';
                theForm.submit();
            }
        }

        function init() {
            // decorate the table
            tableruler();
            tablestripe();
        }
        dojo.addOnLoad(init);
    </script>
</head>
<body class="tundra">
<form action="" method="post" name="assetlistform">
<table width="100%">
<tr>
<td valign="top">
<div id="content">

<authz:authorize ifAnyGranted="ROLE_ADMINISTRATOR, ROLE_MANAGER, ROLE_TECHNICIAN">
    <table>
        <tr>
            <td>
                <input type="button" id="btnMassDelete" name="btnMassDelete"
                       value="<spring:message code="assetFilter.delete"/>"
                       onclick="massDeleteAssets();"/>
            </td>
        </tr>
    </table>
</authz:authorize>

<table class="stripe ruler paged">
<thead>
<tr class="title">

<authz:authorize ifAnyGranted="ROLE_ADMINISTRATOR, ROLE_MANAGER, ROLE_TECHNICIAN">
    <th>
        <input type="checkbox" id="chkMain" onclick="checkAllCbx(this.checked);"
               title="<spring:message code="assetFilter.checkAll"/>">
    </th>
</authz:authorize>

<c:forEach var="col" items="${colOrder}">
<c:choose>
<c:when test="${col == 'asset.assetNumber'}">
    <tags:plistHeader messageCode="asset.assetNumber" sortProperty="assetNumber"/>
</c:when>
<c:when test="${col == 'asset.name'}">
    <tags:plistHeader messageCode="asset.name" sortProperty="name"/>
</c:when>
<c:when test="${col == 'asset.type'}">
    <tags:plistHeader messageCode="asset.type" sortProperty="type"/>
</c:when>
<c:when test="${col == 'asset.owner'}">
    <tags:plistHeader messageCode="asset.owner" sortProperty="owner"/>
</c:when>
<c:when test="${col == 'asset.vendor'}">
    <tags:plistHeader messageCode="asset.vendor" sortProperty="vendor"/>
</c:when>
<c:when test="${col == 'asset.assetlocation'}">
    <tags:plistHeader messageCode="asset.assetlocation" sortProperty="assetLocation"/>
</c:when>
<c:when test="${col == 'asset.acquisitionDate'}">
    <tags:plistHeader messageCode="asset.acquisitionDate" sortProperty="acquisitionDate"/>
</c:when>
<c:when test="${col == 'asset.location'}">
    <tags:plistHeader messageCode="asset.location" sortProperty="location"/>
</c:when>
<c:when test="${col == 'asset.group'}">
    <tags:plistHeader messageCode="asset.group" sortProperty="group"/>
</c:when>
<c:when test="${col == 'asset.category'}">
    <tags:plistHeader messageCode="asset.category" sortProperty="category"/>
</c:when>
<c:when test="${col == 'asset.categoryoption'}">
    <tags:plistHeader messageCode="asset.categoryoption" sortProperty="categoryOption"/>
</c:when>
<c:when test="${col == 'asset.manufacturer'}">
    <tags:plistHeader messageCode="asset.manufacturer" sortProperty="manufacturer"/>
</c:when>
<c:when test="${col == 'asset.modelNumber'}">
    <tags:plistHeader messageCode="asset.modelNumber" sortProperty="modelNumber"/>
</c:when>
<c:when test="${col == 'asset.serialnumber'}">
    <tags:plistHeader messageCode="asset.serialnumber" sortProperty="serialNumber"/>
</c:when>
<c:when test="${col == 'asset.description'}">
    <tags:plistHeader messageCode="asset.description" sortProperty="description"/>
</c:when>
<c:when test="${col == 'asset.status'}">
    <tags:plistHeader messageCode="asset.status" sortProperty="status"/>
</c:when>
<c:when test="${col == 'accountinginfo.ponumber'}">
    <tags:plistHeader messageCode="accountinginfo.ponumber" sortProperty="accountingInfo.poNumber"/>
</c:when>
<c:when test="${col == 'accountinginfo.accountnumber'}">
    <tags:plistHeader messageCode="accountinginfo.accountnumber" sortProperty="accountingInfo.accountNumber"/>
</c:when>
<c:when test="${col == 'accountinginfo.accumulatednumber'}">
    <tags:plistHeader messageCode="accountinginfo.accumulatednumber" sortProperty="accountingInfo.accumulatedNumber"/>
</c:when>
<c:when test="${col == 'accountinginfo.expensenumber'}">
    <tags:plistHeader messageCode="accountinginfo.expensenumber" sortProperty="accountingInfo.expenseNumber"/>
</c:when>
<c:when test="${col == 'accountinginfo.acquisitionvalue'}">
    <tags:plistHeader messageCode="accountinginfo.acquisitionvalue" sortProperty="accountingInfo.acquisitionValue"/>
</c:when>
<c:when test="${col == 'accountinginfo.leasenumber'}">
    <tags:plistHeader messageCode="accountinginfo.leasenumber" sortProperty="accountingInfo.leaseNumber"/>
</c:when>
<c:when test="${col == 'accountinginfo.leaseduration'}">
    <tags:plistHeader messageCode="accountinginfo.leaseduration" sortProperty="accountingInfo.leaseDuration"/>
</c:when>
<c:when test="${col == 'accountinginfo.leasefrequency'}">
    <tags:plistHeader messageCode="accountinginfo.leasefrequency" sortProperty="accountingInfo.leaseFrequency"/>
</c:when>
<c:when test="${col == 'accountinginfo.leasefrequencyprice'}">
    <tags:plistHeader messageCode="accountinginfo.leasefrequencyprice"
                      sortProperty="accountingInfo.leaseFrequencyPrice"/>
</c:when>
<c:when test="${col == 'accountinginfo.disposalmethod'}">
    <tags:plistHeader messageCode="accountinginfo.disposalmethod" sortProperty="accountingInfo.disposalMethod"/>
</c:when>
<c:when test="${col == 'accountinginfo.disposaldate'}">
    <tags:plistHeader messageCode="accountinginfo.disposaldate" sortProperty="accountingInfo.disposalDate"/>
</c:when>
<c:when test="${col == 'accountinginfo.insurancecategory'}">
    <tags:plistHeader messageCode="accountinginfo.insurancecategory" sortProperty="accountingInfo.insuranceCategory"/>
</c:when>
<c:when test="${col == 'accountinginfo.replacementvaluecategory'}">
    <tags:plistHeader messageCode="accountinginfo.replacementvaluecategory"
                      sortProperty="accountingInfo.replacementValueCategory"/>
</c:when>
<c:when test="${col == 'accountinginfo.replacementvalue'}">
    <tags:plistHeader messageCode="accountinginfo.replacementvalue" sortProperty="accountingInfo.replacementValue"/>
</c:when>
<c:when test="${col == 'accountinginfo.maintenancecost'}">
    <tags:plistHeader messageCode="accountinginfo.maintenancecost" sortProperty="accountingInfo.maintenanceCost"/>
</c:when>
<c:when test="${col == 'accountinginfo.depreciationmethod'}">
    <tags:plistHeader messageCode="accountinginfo.depreciationmethod" sortProperty="accountingInfo.depreciationMethod"/>
</c:when>
<c:when test="${col == 'accountinginfo.leaseexpirationdate'}">
    <tags:plistHeader messageCode="accountinginfo.leaseexpirationdate"
                      sortProperty="accountingInfo.leaseExpirationDate"/>
</c:when>
<c:when test="${col == 'accountinginfo.warrantydate'}">
    <tags:plistHeader messageCode="accountinginfo.warrantydate" sortProperty="accountingInfo.warrantyDate"/>
</c:when>
<c:otherwise>
    <%--Must be a custom field. Innit?--%>
    <tags:plistHeader messageCode="${col}" sortProperty="nosort"/>
</c:otherwise>
</c:choose>
</c:forEach>

</tr>
</thead>
<tbody id="theBody" style="overflow: auto;">
<c:choose>
<c:when test="${fn:length(listHolder.assets.pageList)==0}">
    <tr style="height:10px">
        <td><spring:message code="assetTracker.nolist"/></td>
    </tr>
</c:when>
<c:otherwise>
<c:forEach var="asset" items="${listHolder.assets.pageList}">
<tr class="toberuled">

<authz:authorize ifAnyGranted="ROLE_ADMINISTRATOR, ROLE_MANAGER, ROLE_TECHNICIAN">
    <td>
        <input type="checkbox" name="assetCbx" value="<c:out value="${asset.id}"/>"/>
    </td>
</authz:authorize>

<c:forEach var="col" items="${colOrder}">
<c:choose>
<c:when test="${col == 'asset.assetNumber'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.assetNumber}"/>
</c:when>
<c:when test="${col == 'asset.name'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.name}"/>
</c:when>
<c:when test="${col == 'asset.type'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.assetType.name}"/>
</c:when>
<c:when test="${col == 'asset.owner'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.owner.firstName} ${asset.owner.lastName}"/>
</c:when>
<c:when test="${col == 'asset.vendor'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.vendor.name}"/>
</c:when>
<c:when test="${col == 'asset.assetlocation'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.assetLocation}"/>
</c:when>
<c:when test="${col == 'asset.acquisitionDate'}">
    <c:set var="fmtdDate">
        <fmt:formatDate value="${asset.acquisitionDate}" type="both" dateStyle="MEDIUM" timeStyle="SHORT"/>
    </c:set>
    <tags:plistRow assetId="${asset.id}" dispValue="${fmtdDate}"/>
    <c:remove var="fmtdDate"/>
</c:when>
<c:when test="${col == 'asset.location'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.location.name}"/>
</c:when>
<c:when test="${col == 'asset.group'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.group.name}"/>
</c:when>
<c:when test="${col == 'asset.category'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.category.name}"/>
</c:when>
<c:when test="${col == 'asset.categoryoption'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.categoryOption.name}"/>
</c:when>
<c:when test="${col == 'asset.manufacturer'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.manufacturer}"/>
</c:when>
<c:when test="${col == 'asset.modelNumber'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.modelNumber}"/>
</c:when>
<c:when test="${col == 'asset.serialnumber'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.serialNumber}"/>
</c:when>
<c:when test="${col == 'asset.description'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.description}"/>
</c:when>
<c:when test="${col == 'asset.status'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.status.name}"/>
</c:when>
<c:when test="${col == 'accountinginfo.ponumber'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.accountingInfo.poNumber}"/>
</c:when>
<c:when test="${col == 'accountinginfo.accountnumber'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.accountingInfo.accountNumber}"/>
</c:when>
<c:when test="${col == 'accountinginfo.accumulatednumber'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.accountingInfo.accumulatedNumber}"/>
</c:when>
<c:when test="${col == 'accountinginfo.expensenumber'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.accountingInfo.expenseNumber}"/>
</c:when>
<c:when test="${col == 'accountinginfo.acquisitionvalue'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.accountingInfo.acquisitionValue}"/>
</c:when>
<c:when test="${col == 'accountinginfo.leasenumber'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.accountingInfo.leaseNumber}"/>
</c:when>
<c:when test="${col == 'accountinginfo.leaseduration'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.accountingInfo.leaseDuration}"/>
</c:when>
<c:when test="${col == 'accountinginfo.leasefrequency'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.accountingInfo.leaseFrequency}"/>
</c:when>
<c:when test="${col == 'accountinginfo.leasefrequencyprice'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.accountingInfo.leaseFrequencyPrice}"/>
</c:when>
<c:when test="${col == 'accountinginfo.disposalmethod'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.accountingInfo.disposalMethod}"/>
</c:when>
<c:when test="${col == 'accountinginfo.disposaldate'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.accountingInfo.disposalDate}" isDate="true"/>
</c:when>
<c:when test="${col == 'accountinginfo.insurancecategory'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.accountingInfo.insuranceCategory}"/>
</c:when>
<c:when test="${col == 'accountinginfo.replacementvaluecategory'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.accountingInfo.replacementValueCategory}"/>
</c:when>
<c:when test="${col == 'accountinginfo.replacementvalue'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.accountingInfo.replacementValue}"/>
</c:when>
<c:when test="${col == 'accountinginfo.maintenancecost'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.accountingInfo.maintenanceCost}"/>
</c:when>
<c:when test="${col == 'accountinginfo.depreciationmethod'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.accountingInfo.depreciationMethod}"/>
</c:when>
<c:when test="${col == 'accountinginfo.leaseexpirationdate'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.accountingInfo.leaseExpirationDate}" isDate="true"/>
</c:when>
<c:when test="${col == 'accountinginfo.warrantydate'}">
    <tags:plistRow assetId="${asset.id}" dispValue="${asset.accountingInfo.warrantyDate}" isDate="true"/>
</c:when>
<c:otherwise>
    <%--Must be a custom field. Innit?--%>
    <%--Loop through the customfieldvalue objects looking for the right one to display--%>
    <c:forEach var="acfv" items="${asset.customFieldValues}">
        <c:if test="${acfv.customField.name eq col}">
            <c:choose>
                <c:when test="${acfv.customField.customFieldType eq 'textarea'}">
                    <c:set var="fieldValue" value="${acfv.fieldValueText}"/>
                </c:when>
                <c:otherwise>
                    <c:set var="fieldValue" value="${acfv.fieldValue}"/>
                </c:otherwise>
            </c:choose>
        </c:if>
    </c:forEach>
    <tags:plistRow assetId="${asset.id}" dispValue="${fieldValue}"/>
    <c:remove var="fieldValue"/>
</c:otherwise>
</c:choose>
</c:forEach>

</tr>
</c:forEach>
</c:otherwise>
</c:choose>
</tbody>
</table>

<hr/>
<div class="pagernav">
    <c:if test="${listHolder.assets.pageCount > 1}">
        <a href="<c:url value="${refreshUrl}"><c:param name="page" value="0"/></c:url>">1</a>
        &nbsp;...&nbsp;
        <c:forEach begin="${listHolder.assets.firstLinkedPage}"
                   end="${listHolder.assets.lastLinkedPage}" var="crtpg">
            <c:choose>
                <c:when test="${crtpg == listHolder.assets.page}">
                    <strong>
                        <c:out value="${crtpg + 1}"/>
                    </strong>
                </c:when>
                <c:otherwise>
                    <a href="<c:url value="${refreshUrl}"><c:param name="page" value="${crtpg}"/></c:url>">
                        <c:out value="${crtpg + 1}"/>
                    </a>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        &nbsp;...&nbsp;
        <a href="<c:url value="${refreshUrl}"><c:param name="page" value="${listHolder.assets.pageCount - 1}"/></c:url>">
            <c:out value="${listHolder.assets.pageCount}"/>
        </a>
    </c:if>
</div>
<div class="psize">
    <spring:message code="pagedList.pltSize" var="ps"/>
    <select name="pageSize" onChange="document.forms[0].submit();">
        <c:forTokens items="5,10,25,50,75,100" delims="," var="crtps">
            <option
                    <c:if test="${listHolder.assets.pageSize == crtps}">selected</c:if>
                    value="<c:out value="${crtps}"/>">
                <c:out value="${crtps} ${ps}"/>
            </option>
        </c:forTokens>
    </select>
</div>
<div>
    <table class="pager paged">
        <tr>
            <td align="left" width="33%">
                <spring:message code="pagedList.pltPages"
                                arguments="${listHolder.assets.page + 1},${listHolder.assets.pageCount}"/>
            </td>
            <td align="center" width="34%"/>
            <td align="right" width="33%" style="padding-right:15px">
                <spring:message code="pagedList.pltRecords"
                                arguments="${listHolder.assets.firstElementOnPage + 1},${listHolder.assets.lastElementOnPage + 1},${listHolder.assets.nrOfElements}"/>
            </td>
        </tr>
    </table>
</div>
<hr/>

</div>
</td>
</tr>
</table>
</form>
</body>
</html>