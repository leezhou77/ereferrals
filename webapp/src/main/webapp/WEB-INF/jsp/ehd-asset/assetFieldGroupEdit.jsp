<%@ include file="/WEB-INF/jsp/include.jsp" %>

<head>
    <meta name="assetSidebar" content="true">
    <title><spring:message code="assetTracker.editFieldGroup"/></title>

    <script type="text/javascript">

        function viewAsset() {
            window.location.href = '<c:url value="/asset/viewVendor.glml"/>';
        }

        function cancelForm() {
            window.location.href = '<c:url value="/asset/fieldGroups.glml"/>';
        }

        function init() {
        }

        dojo.addOnLoad(init);
    </script>
</head>

<body>

<section class="grid_5">
    <div class="block-content">
        <h1><spring:message code="assetTracker.editFieldGroup"/></h1>

        <form:form commandName="fieldGroup" method="post" cssClass="form">
            <spring:hasBindErrors name="fieldGroup">
                <p class="message error no-margin">
                    <form:errors path="*"/>
                </p>
            </spring:hasBindErrors>

            <input type="hidden" name="fieldGroupId" value="<c:out value="${fieldGroup.id}"/>"/>

            <p class="required">
                <form:label path="name"><spring:message code="global.name"/></form:label>
                <form:input path="name" cssErrorClass="error" dojoType="dijit.form.TextBox" maxlength="255"/>
            </p>

            <p>
                <form:label path="description"><spring:message code="global.description"/></form:label>
                <form:input path="description" cssErrorClass="error" dojoType="dijit.form.TextBox" maxlength="255"/>
            </p>

            <p>
                <form:label path="assetTypes"><spring:message code="assetTracker.assetTypes"/></form:label>
                <spring:message code="assetTracker.assetTypesChoose"/>:
                <br>
                <c:forEach items="${assetTypes}" var="assetType">

                    <c:set var="selectedAssetTypesChecked" value="false"/>

                    <c:forEach items="${fieldGroup.assetTypes}" var="fgAssetType">
                        <c:if test="${fgAssetType.id == assetType.id}">
                            <c:set var="selectedAssetTypesChecked" value="true"/>
                        </c:if>
                    </c:forEach>

                    <input type="checkbox" dojoType="dijit.form.CheckBox" name="selectedAssetTypes" value="<c:out value="${assetType.id}"/>"
                            <c:if test="${selectedAssetTypesChecked == true}">
                                checked="true"
                            </c:if>
                            />
                    <c:out value="${assetType.name}"/><br>

                    <c:remove var="selectedAssetTypesChecked"/>
                </c:forEach>

            </p>

            <p class="grey-bg no-margin" id="commandButtons">
                <button type="submit"><spring:message code="global.save"/></button>
                <button type="button" onclick="cancelForm();"><spring:message
                        code="global.cancel"/></button>
            </p>
        </form:form>
    </div>
</section>
</body>
