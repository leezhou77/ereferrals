f<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/include.jsp" %>

<%--@elvariable id="assetFilter" type="net.grouplink.ehelpdesk.domain.asset.AssetFilter"--%>

<head>
    <title><spring:message code="assetFilter.title"/></title>

    <meta name="assetSidebar" content="true">

    <link rel="stylesheet" href="<c:url value="/css/ticketFilterEdit.css"/>" type="text/css">

    <link rel="stylesheet" href="<c:url value="/js/chosen/chosen.css"/>" type="text/css">

    <style type="text/css">
        #field-in-filter-select_chzn {
            width: 175px !important;
            margin-right: 10px;
        }
        .dijitDialogUnderlayWrapper {
            z-index: 10000 !important;
        }
        .dijitDialog {
            z-index: 10001 !important;
        }
    </style>

    <script type="text/javascript" src="<c:url value="/js/chosen/chosen.jquery.js"/>"></script>

    <tags:jsTemplate id="filter-def-tmpl">
        <tags:filterBox title="{{customFieldName}}" filterName="customField_{{rowId}}" filterValuesBoxId="customFieldBox_{{rowId}}" columnName="customField.{{customFieldName}}">
            <jsp:attribute name="hidden">false</jsp:attribute>
            <jsp:attribute name="widgets">
                <input type="hidden" id="customFieldName_{{rowId}}" name="customFieldName_{{rowId}}" value="{{customFieldName}}">
                <input type="text" class="filterValueInput" name="customField_{{rowId}}" id="customField_{{rowId}}" trim="true"
                       dojoType="dijit.form.TextBox">
                <a href="javascript:void(0)"
                   title="<spring:message code="ticketFilter.addNew"/>"
                   class="addItem">
                    <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                </a>
            </jsp:attribute>
        </tags:filterBox>
    </tags:jsTemplate>

    <script type="text/javascript">
        dojo.addOnLoad(function() {
            filterMode = "assetFilter";
            //create and populate the drag and drop source for column ordering
            window.columnOrder = new dojo.dnd.Source("columnOrder", { creator: columnCreator, singular: true });
            <c:forEach var="col" items="${assetFilter.columnOrder}">
                <c:choose>
                    <c:when test="${fn:startsWith(col, 'asset') || fn:startsWith(col, 'accountinginfo')}">
                        <c:set var="colText">
                            <spring:message code="${col}" javaScriptEscape="true"/>
                        </c:set>
                    </c:when>
                    <c:otherwise>
                        <c:set var="colText" value="${fn:substringAfter(col, 'customField.')}"/>
                    </c:otherwise>
                </c:choose>
                window.columnOrder.insertNodes(false,
                    [{
                        name: "<c:out value="${col}"/>",
                        text: "<c:out value="${colText}"/>"
                    }]);
            </c:forEach>

            dojo.connect(dojo.byId("reportCheckbox"), "onclick", function() {
                var reportCb = dojo.byId("reportCheckbox");
                var reportDiv = dojo.byId("reportBox");
                if (reportCb.checked) {
                    reportDiv.style.display = "block";
                } else {
                    reportDiv.style.display = "none";
                }
            })
        });
    </script>

    <%@ include file="/WEB-INF/jsp/includes/ticketFilterEdit.js.jsp" %>
</head>
<body>

<div id="preloader"></div>

<c:choose>
    <c:when test="${assetFilter.id eq null}">
        <c:set var="method" value="post"/>
        <c:set var="action">
            <c:url value="/assets/filters"/>
        </c:set>
    </c:when>
    <c:otherwise>
        <c:set var="method" value="put"/>
        <c:set var="action">
            <c:url value="/assets/filters/${assetFilter.id}"/>
        </c:set>
    </c:otherwise>
</c:choose>

<form:form modelAttribute="assetFilter" id="assetFilterForm" name="assetFilterForm" method="${method}" action="${action}">
<section class="grid_7">
    <div class="block-content form">
        <h1><spring:message code="assetFilter.title"/></h1>
        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a href="javascript:void(0)" data-form-action="apply">
                        <img src="<c:url value="/images/theme/icons/fugue/arrow-curve-000-left.png"/>" alt="">
                        <spring:message code="assetFilter.applyFilter"/>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)" data-form-action="save">
                        <img src="<c:url value="/images/save.gif"/>" alt="">
                        <spring:message code="assetFilter.saveFilter"/>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)" data-form-action="reset">
                        <img src="<c:url value="/images/theme/icons/fugue/counter-reset.png"/>" alt="">
                        <spring:message code="assetFilter.resetFilter"/>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)" data-form-action="saveas">
                        <img src="<c:url value="/images/theme/icons/fugue/document.png"/>" alt="">
                        <spring:message code="assetFilter.saveAsFilter"/>
                    </a>
                </li>
            </ul>
        </div>

        <input type="hidden" name="formAction" id="formAction" value="save"/>
        <input type="hidden" name="saveAsName" id="saveAsName"/>

        <form:label path="name"><spring:message code="ticketFilter.filterName"/></form:label>
        <form:input path="name" size="40" maxlength="255"/>

        <form:checkbox path="privateFilter"/>
        <form:label path="privateFilter" cssClass="inline"><spring:message code="ticketFilter.markasprivate"/></form:label>

        <form:checkbox id="reportCheckbox" path="report"/>
        <form:label path="report" cssClass="inline"><spring:message code="assetFilter.isReport"/></form:label>

        <div id="reportBox"
                <c:if test="${not assetFilter.report}">
                    style="display: none;"
                </c:if>
                >
            <br>
            <p>
            <form:label path="format"><spring:message code="assetFilter.report.format"/></form:label>
            <form:select path="format">
                <form:option value="pdf">PDF</form:option>
                <%--<form:option value="html">HTML</form:option>--%>
                <form:option value="xls">Excel</form:option>
                <form:option value="csv">CSV</form:option>
            </form:select>
            </p>

            <p>
            <form:label path="groupBy"><spring:message code="assetFilter.report.groupBy"/></form:label>
            <form:select path="groupBy">
                <form:options items="${groupByOptions}" itemLabel="display" itemValue="value"/>
            </form:select>
            </p>
        </div>
    </div>

    <br>
    <div id="filter-block" class="block-content no-padding">
    <div class="with-padding">
            <div class="float-left margin-right">
                <select id="field-select" data-placeholder="<spring:message code="ticketFilter.chooseColumnFilter"/>">
                    <option value=""></option>
                    <optgroup label="<spring:message code="assetFilter.assetFields"/>" class="assetFields">
                        <c:forEach items="${allColumns}" var="entry">
                            <c:if test="${!ehd:mapContainsKey(criteriaCols, entry.key)}">
                                <option value="${entry.key}" id="opt_${entry.key}">
                                    <c:out value="${entry.value}"/>
                                </option>
                            </c:if>
                        </c:forEach>
                    </optgroup>
                    <c:if test="${fn:length(customFields) gt 0}">
                    <optgroup label="<spring:message code="ticketFilter.customField"/>" class="customFields">
                        <c:forEach var="cf" items="${customFields}" varStatus="row">
                            <c:set var="customFieldColumn" value="customField.${cf.name}"/>
                            <c:if test="${!ehd:mapContainsKey(criteriaCols, customFieldColumn)}">
                                <option value="customField.${cf.name}" id="opt_assetFilter.customField_${row.index}" data-custom-field-id="${cf.id}">
                                    <c:out value="${cf.name}"/>
                                </option>
                            </c:if>
                            <c:remove var="customFieldColumn"/>
                        </c:forEach>
                    </optgroup>
                    </c:if>
                </select>
            </div>
            <div class="float-left">
                <a class="button float-left" style="height:21px;padding:3px 5px 0 5px;margin-top:1px" href="javascript:void(0)" id="showFilterLi">
                    <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="" style="width:16px;height:16px">
                </a>
            </div>
            <div class="clearfix"></div>
        </div>

        <ul id="filter-list" class="extended-list no-hover">
            <tags:filterBox filterName="assetNumber" titleMessageCode="asset.assetNumber" filterValuesBoxId="assetNumbers" columnName="asset.assetNumber">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'asset.assetNumber')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <input type="text" class="filterValueInput" name="anumFld" id="anumFld" trim="true"
                           dojoType="dijit.form.TextBox"/>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="anum" items="${assetFilter.assetNumbers}" varStatus="row">
                        <tr>
                            <td>${anum}</td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="assetNumbers"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="name" titleMessageCode="asset.name" filterValuesBoxId="names" columnName="asset.name">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'asset.name')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <input type="text" class="filterValueInput" name="nameFld" id="nameFld" trim="true"
                           dojoType="dijit.form.TextBox"/>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="name" items="${assetFilter.names}" varStatus="row">
                        <tr>
                            <td>${name}</td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="names"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="type" titleMessageCode="asset.types" filterValuesBoxId="typeIds" columnName="asset.type">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'asset.type')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <select name="typeFld" id="typeFld" dojoType="dijit.form.FilteringSelect" class="filterValueInput">
                        <option value=""></option>
                        <c:forEach var="stat" items="${assetTypes}">
                            <option value="${stat.id}">${stat.name}</option>
                        </c:forEach>
                    </select>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="typeId" items="${assetFilter.typeIds}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${typeIdToNameMap[typeId]}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="typeIds"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="owner" titleMessageCode="asset.owner" filterValuesBoxId="ownerIds" columnName="asset.owner">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'asset.owner')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <div dojoType="dojo.data.ItemFileReadStore" jsId="userStore"
                         url="<c:url value="/stores/users.json"/>"></div>
                    <input dojoType="dijit.form.FilteringSelect"
                           store="userStore"
                           searchAttr="label"
                           name="ownerFld"
                           id="ownerFld"
                           autocomplete="true"
                           class="filterValueInput"/>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="ownerId" items="${assetFilter.typeIds}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${ownerIdToNameMap[ownerId]}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="ownerIds"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="vendor" titleMessageCode="asset.vendor" filterValuesBoxId="vendorIds" columnName="asset.vendor">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'asset.vendor')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <select name="vendorFld" id="vendorFld" dojoType="dijit.form.FilteringSelect" class="filterValueInput">
                        <option value=""></option>
                        <c:forEach var="vendor" items="${vendors}">
                            <option value="${vendor.id}">${vendor.name}</option>
                        </c:forEach>
                    </select>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="vendorId" items="${assetFilter.vendorIds}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${vendorIdToNameMap[vendorId]}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="vendorIds"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="assetLocation" titleMessageCode="asset.assetlocation" filterValuesBoxId="assetLocations" columnName="asset.assetlocation">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'asset.assetlocation')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <input type="text" class="filterValueInput" name="alocFld" id="alocFld" trim="true"
                           dojoType="dijit.form.TextBox"/>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="aloc" items="${assetFilter.assetLocations}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${aloc}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="assetLocations"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="acquisitionDate" titleMessageCode="asset.acquisitiondate" columnName="asset.acquisitionDate">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'asset.acquisitionDate')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <div class="small-margin">
                        <form:select path="acquisitionDateOperator" id="acquisitionDateOperator" dojoType="dijit.form.FilteringSelect">
                            <form:options items="${dateOps}" itemLabel="display" itemValue="value"/>
                        </form:select>
                    </div>

                    <div id="acqdate1" class="small-margin">
                        <form:input path="acquisitionDate" cssStyle="width:100px" dojoType="dijit.form.DateTextBox"/>
                    </div>

                    <div id="acqdate2" class="small-margin">
                        <form:input path="acquisitionDate2" cssStyle="width:100px" dojoType="dijit.form.DateTextBox"/>
                    </div>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="location" titleMessageCode="asset.location" filterValuesBoxId="locationIds" columnName="asset.location">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'asset.location')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <select name="locFld" id="locFld" dojoType="dijit.form.FilteringSelect" class="filterValueInput">
                        <option value=""></option>
                        <c:forEach var="loc" items="${locations}">
                            <option value="${loc.id}">${loc.name}</option>
                        </c:forEach>
                    </select>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="locationId" items="${assetFilter.locationIds}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${locationIdToNameMap[locationId]}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="locationIds"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="group" titleMessageCode="asset.group" filterValuesBoxId="groupIds" columnName="asset.group">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'asset.group')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <select name="grpFld" id="grpFld" dojoType="dijit.form.FilteringSelect" class="filterValueInput">
                        <option value=""></option>
                        <c:forEach var="group" items="${groups}">
                            <option value="${group.id}">${group.name}</option>
                        </c:forEach>
                    </select>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="groupId" items="${assetFilter.groupIds}" varStatus="row">
                        <tr>
                            <td>
                                <c:forEach var="gr" items="${groups}">
                                    <c:if test="${gr.id == groupId}">
                                        <c:out value="${gr.name}"/>
                                    </c:if>
                                </c:forEach>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="groupIds"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="category" titleMessageCode="asset.category" filterValuesBoxId="categoryIds" columnName="asset.category">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'asset.category')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <a id="showCategoryChooser" href="javascript:void(0)" title="<spring:message code="ticketFilter.addNew"/>">
                        <img src="<c:url value="/images/theme/icons/fugue/magnifier.png"/>" alt="<spring:message code="ticketFilter.addNew"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="catId" items="${assetFilter.categoryIds}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${catIdToNameMap[catId]}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="categoryIds"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="categoryOption" titleMessageCode="asset.categoryoption" filterValuesBoxId="categoryOptionIds" columnName="asset.categoryoption">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'asset.categoryoption')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <a id="showCatOptChooser" href="javascript:void(0)" title="<spring:message code="ticketFilter.addNew"/>">
                        <img src="<c:url value="/images/theme/icons/fugue/magnifier.png"/>" alt="<spring:message code="ticketFilter.addNew"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="catOptId" items="${assetFilter.categoryOptionIds}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${catOptIdToNameMap[catOptId]}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="categoryOptionIds"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="manufacturer" titleMessageCode="asset.manufacturer" filterValuesBoxId="manufacturers" columnName="asset.manufacturer">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'asset.manufacturer')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <input type="text" name="manuFld" id="manuFld" trim="true" dojoType="dijit.form.TextBox" class="filterValueInput"/>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="manu" items="${assetFilter.manufacturers}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${manu}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="manufacturers"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="modelNumber" titleMessageCode="asset.modelNumber" filterValuesBoxId="modelNumbers" columnName="asset.modelNumber">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'asset.modelNumber')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <input type="text" class="filterValueInput" name="mnumFld" id="mnumFld" trim="true"
                           dojoType="dijit.form.TextBox"/>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="mnum" items="${assetFilter.modelNumbers}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${mnum}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="modelNumbers"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="serialNumber" titleMessageCode="asset.serialNumber" filterValuesBoxId="serialNumbers" columnName="asset.serialnumber">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'asset.serialnumber')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <input type="text" name="snumFld" id="snumFld" trim="true" dojoType="dijit.form.TextBox" class="filterValueInput"/>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="snum" items="${assetFilter.serialNumbers}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${snum}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="serialNumbers"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="description" titleMessageCode="asset.description" filterValuesBoxId="descriptions" columnName="asset.description">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'asset.description')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <input type="text" name="descFld" id="descFld" trim="true" dojoType="dijit.form.TextBox" class="filterValueInput"/>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="desc" items="${assetFilter.descriptions}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${desc}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="descriptions"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="status" titleMessageCode="asset.status" filterValuesBoxId="statusIds" columnName="asset.status">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'asset.status')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <select name="statFld" id="statFld" dojoType="dijit.form.FilteringSelect" class="filterValueInput">
                        <option value=""></option>
                        <c:forEach var="stat" items="${assetStatuses}">
                            <option value="${stat.id}">${stat.name}</option>
                        </c:forEach>
                    </select>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="statId" items="${assetFilter.statusIds}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${assetStatusIdToNameMap[statId]}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="statusIds"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="softwarelicenses" titleMessageCode="asset.softwarelicenses" filterValuesBoxId="softwarelicenseIds" columnName="asset.softwarelicenses">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'asset.softwarelicense')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <select name="softLicField" id="softLic" dojoType="dijit.form.FilteringSelect" class="filterValueInput">
                        <option value=""></option>
                        <c:forEach var="lic" items="${softwareLicenses}">
                            <option value="${lic.id}">${lic.productName}</option>
                        </c:forEach>
                    </select>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                       <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="licId" items="${assetFilter.softLicIds}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${softLicIdToNameMap[licId]}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="softwarelicenseIds"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                   <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="ponumber" titleMessageCode="accountinginfo.ponumber" filterValuesBoxId="poNumbers" columnName="accountinginfo.ponumber">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'accountinginfo.ponumber')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <input type="text" name="pnumFld" id="pnumFld" trim="true" dojoType="dijit.form.TextBox" class="filterValueInput"/>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="pnum" items="${assetFilter.poNumbers}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${pnum}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="poNumbers"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="accountnumber" titleMessageCode="accountinginfo.accountnumber" filterValuesBoxId="accountNumbers" columnName="accountinginfo.accountnumber">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'accountinginfo.accountnumber')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <input class="filterValueInput" type="text" name="acnumFld" id="acnumFld" trim="true" dojoType="dijit.form.TextBox"/>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="anum" items="${assetFilter.accountNumbers}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${anum}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="accountNumbers"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="accumulatednumber" titleMessageCode="accountinginfo.accumulatednumber" filterValuesBoxId="accumulatedNumbers" columnName="accountinginfo.accumulatednumber">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'accountinginfo.accumulatednumber')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <input type="text" name="accnumFld" id="accnumFld" trim="true" dojoType="dijit.form.TextBox" class="filterValueInput"/>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="anum" items="${assetFilter.accumulatedNumbers}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${pnum}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="accumulatedNumbers"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="expensenumber" titleMessageCode="accountinginfo.expensenumber" filterValuesBoxId="expenseNumbers" columnName="accountinginfo.expensenumber">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'accountinginfo.expensenumber')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <input type="text" name="expFld" id="expFld" trim="true" dojoType="dijit.form.TextBox" class="filterValueInput"/>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="enum" items="${assetFilter.expenseNumbers}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${enum}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="expenseNumbers"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="acquisitionvalue" titleMessageCode="accountinginfo.acquisitionvalue" filterValuesBoxId="acquisitionValues" columnName="accountinginfo.acquisitionvalue">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'accountinginfo.acquisitionvalue')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <input type="text" name="acqFld" id="acqFld" trim="true" dojoType="dijit.form.TextBox" class="filterValueInput"/>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="anum" items="${assetFilter.acquisitionValues}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${anum}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="acquisitionValues"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="leasenumber" titleMessageCode="accountinginfo.leasenumber" filterValuesBoxId="leaseNumbers" columnName="accountinginfo.leasenumber">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'accountinginfo.leasenumber')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <input type="text" name="leaFld" id="leaFld" trim="true" dojoType="dijit.form.TextBox" class="filterValueInput"/>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="lnum" items="${assetFilter.leaseNumbers}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${lnum}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="leaseNumbers"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="leaseduration" titleMessageCode="accountinginfo.leaseduration" filterValuesBoxId="leaseDurations" columnName="accountinginfo.leaseduration">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'accountinginfo.leaseduration')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <input type="text" name="ldurFld" id="ldurFld" trim="true" dojoType="dijit.form.TextBox" class="filterValueInput"/>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="ldur" items="${assetFilter.leaseDurations}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${ldur}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="leaseDurations"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="leasefrequency" titleMessageCode="accountinginfo.leasefrequency" filterValuesBoxId="leaseFrequencies" columnName="accountinginfo.leasefrequency">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'accountinginfo.leasefrequency')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <input type="text" name="lfreFld" id="lfreFld" trim="true" dojoType="dijit.form.TextBox" class="filterValueInput"/>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <%-- Lease frequencys? dumbass --%>
                    <c:forEach var="lfre" items="${assetFilter.leaseFrequencys}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${lfre}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="leaseFrequencies"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="leasefrequencyprice" titleMessageCode="accountinginfo.leasefrequencyprice" filterValuesBoxId="leaseFrequencyPrices" columnName="accountinginfo.leasefrequencyprice">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'accountinginfo.leasefrequencyprice')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <input type="text" name="lfpFld" id="lfpFld" trim="true" dojoType="dijit.form.TextBox" class="filterValueInput"/>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="lfp" items="${assetFilter.leaseFrequencyPrices}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${lfp}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="leaseFrequencyPrices"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="disposalmethod" titleMessageCode="accountinginfo.disposalmethod" filterValuesBoxId="disposalMethods" columnName="accountinginfo.disposalmethod">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'accountinginfo.disposalmethod')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <input type="text" name="dispFld" id="dispFld" trim="true" dojoType="dijit.form.TextBox" class="filterValueInput"/>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="disp" items="${assetFilter.disposalMethods}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${disp}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="disposalMethods"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="disposaldate" titleMessageCode="accountinginfo.disposaldate" columnName="accountinginfo.disposaldate">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'accountinginfo.disposaldate')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <div class="small-margin">
                        <form:select path="disposalDateOperator" id="disposalDateOperator" dojoType="dijit.form.FilteringSelect">
                            <form:options items="${dateOps}" itemLabel="display" itemValue="value"/>
                        </form:select>
                    </div>

                    <div id="dispdate1" class="small-margin">
                        <form:input path="disposalDate" cssStyle="width:100px" dojoType="dijit.form.DateTextBox"/>
                    </div>

                    <div id="dispdate2" class="small-margin">
                        <form:input path="disposalDate2" cssStyle="width:100px" dojoType="dijit.form.DateTextBox"/>
                    </div>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="insurancecategory" titleMessageCode="accountinginfo.insurancecategory" filterValuesBoxId="insuranceCategories" columnName="accountinginfo.insurancecategory">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'accountinginfo.insurancecategory')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <input type="text" name="insFld" id="insFld" trim="true" dojoType="dijit.form.TextBox" class="filterValueInput"/>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="ins" items="${assetFilter.insuranceCategories}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${ins}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="insuranceCategories"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="replacementvaluecategory" titleMessageCode="accountinginfo.replacementvaluecategory" filterValuesBoxId="replacementValueCategories" columnName="accountinginfo.replacementvaluecategory">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'accountinginfo.replacementvaluecategory')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <input type="text" name="rvcFld" id="rvcFld" trim="true" dojoType="dijit.form.TextBox" class="filterValueInput"/>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="rvc" items="${assetFilter.replacementValuecategories}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${rvc}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="replacementValueCategories"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="replacementvalue" titleMessageCode="accountinginfo.replacementvalue" filterValuesBoxId="replacementValues" columnName="accountinginfo.replacementvalue">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'accountinginfo.replacementvalue')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <input type="text" name="rvFld" id="rvFld" trim="true" dojoType="dijit.form.TextBox" class="filterValueInput"/>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="rv" items="${assetFilter.replacementValues}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${rv}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="replacementValues"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="maintenancecost" titleMessageCode="accountinginfo.maintenancecost" filterValuesBoxId="maintenanceCosts" columnName="accountinginfo.maintenancecost">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'accountinginfo.maintenancecost')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <input type="text" name="mcFld" id="mcFld" trim="true" dojoType="dijit.form.TextBox" class="filterValueInput"/>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="mc" items="${assetFilter.maintenanceCosts}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${mc}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="maintenanceCosts"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="depreciationmethod" titleMessageCode="accountinginfo.depreciationmethod" filterValuesBoxId="depreciationMethods" columnName="accountinginfo.depreciationmethod">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'accountinginfo.depreciationmethod')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <input type="text" name="depFld" id="depFld" trim="true" dojoType="dijit.form.TextBox" class="filterValueInput"/>
                    <a href="javascript:void(0)"
                       title="<spring:message code="ticketFilter.addNew"/>"
                       class="addItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                    <c:forEach var="dep" items="${assetFilter.depreciationMethods}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${dep}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="depreciationMethods"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                               </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="leaseexpirationdate" titleMessageCode="accountinginfo.leaseexpirationdate" columnName="accountinginfo.leaseexpirationdate">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'accountinginfo.leaseexpirationdate')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <div class="small-margin">
                        <form:select path="leaseExpirationDateOperator" id="leaseExpirationDateOperator" dojoType="dijit.form.FilteringSelect">
                            <form:options items="${dateOps}" itemLabel="display" itemValue="value"/>
                        </form:select>
                    </div>

                    <div id="leadate1" class="small-margin">
                        <form:input path="leaseExpirationDate" cssStyle="width:100px" dojoType="dijit.form.DateTextBox"/>
                    </div>

                    <div id="leadate2" class="small-margin">
                        <form:input path="leaseExpirationDate2" cssStyle="width:100px" dojoType="dijit.form.DateTextBox"/>
                    </div>
                </jsp:attribute>
            </tags:filterBox>

            <tags:filterBox filterName="warrantydate" titleMessageCode="accountinginfo.warrantydate" columnName="accountinginfo.warrantydate">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsKey(criteriaCols, 'accountinginfo.warrantydate')}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <div class="small-margin">
                        <form:select path="warrantyDateOperator" id="warrantyDateOperator" dojoType="dijit.form.FilteringSelect">
                            <form:options items="${dateOps}" itemLabel="display" itemValue="value"/>
                        </form:select>
                    </div>

                    <div id="wardate1" class="small-margin">
                        <form:input path="warrantyDate" cssStyle="width:100px" dojoType="dijit.form.DateTextBox"/>
                    </div>

                    <div id="wardate2" class="small-margin">
                        <form:input path="warrantyDate2" cssStyle="width:100px" dojoType="dijit.form.DateTextBox"/>
                    </div>
                </jsp:attribute>
            </tags:filterBox>

            <!-- Custom Fields -->
            <c:forEach var="customField" items="${assetFilter.customFields}" varStatus="row">
                <tags:filterBox filterName="customField_${row.index}" title="${customFieldIdToNameMap[customField.key]}" filterValuesBoxId="customFieldDiv_${row.index}" columnName="customField.${customField.key}">
                    <jsp:attribute name="hidden">false</jsp:attribute>
                    <jsp:attribute name="filterValuesBox">
                        <table class="filterValues">
                            <c:forEach var="cfFieldVal" items="${customField.value}">
                                <tr>
                                    <td><c:out value="${cfFieldVal}"/></td>
                                    <td class="delete">
                                        <a href="javascript:void(0)"
                                           class="deleteCFItem"
                                           title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                                        </a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </table>
                    </jsp:attribute>
                </tags:filterBox>
            </c:forEach>

        </ul>
    </div>
</section>
<section class="grid_3">
    <div class="block-content">
        <h1><spring:message code="ticketFilter.colOrder"/></h1>

        <div>
            <div class="float-left">
                <select id="field-in-filter-select" class="chosen" data-placeholder="<spring:message code="ticketFilter.chooseColumn"/>">
                    <option value=""></option>
                    <%-- Asset Fields --%>
                    <optgroup class="assetFields" label="<spring:message code="assetFilter.assetFields"/>">
                        <c:forEach items="${allColumns}" var="entry">
                            <c:if test="${!ehd:arrayContains(assetFilter.columnOrder, entry.key)}">
                                <option value="${entry.key}"><c:out value="${entry.value}"/></option>
                            </c:if>
                        </c:forEach>
                    </optgroup>
                    <%-- Custom Fields --%>
                    <c:if test="${fn:length(customFields) gt 0}">
                    <optgroup class="customFields" label="<spring:message code="ticketFilter.customField"/>">
                        <c:forEach var="cf" items="${customFields}" varStatus="row">
                            <c:if test="${!ehd:arrayContains(assetFilter.columnOrder, cf.name)}">
                                <option value="customField.${cf.name}"><c:out value="${cf.name}"/></option>
                            </c:if>
                        </c:forEach>
                    </optgroup>
                    </c:if>
                </select>
            </div>
            <a class="button float-left" style="height:21px;padding:3px 5px 0 5px;margin-top:1px" href="javascript:void(0)" id="addColumnToColOrder">
                <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="" style="width:16px;height:16px">
            </a>
            <div class="clearfix"></div>
        </div>

        <hr>

        <ol id="columnOrder" class="columnOrder mini-blocks-list"></ol>
    </div>
</section>

<div id="fetcher"></div>

<div dojoType="dijit.Dialog" id="saveAsDialog" title="<spring:message code="assetFilter.saveAsFilter"/>"
     execute="dojo.attr(dojo.byId('saveAsName'), 'value', arguments[0].newName); dojo.destroy(dojo.query('#assetFilterForm input[name^=_method]')[0]); dojo.attr(dojo.byId('assetFilterForm'), 'action', '<c:url value="/assets/filters/${assetFilter.id}/copy"/>'); dojo.byId('assetFilterForm').submit();">
    <p>
        <label for="saveAsName"><spring:message code="ticketFilter.filterName"/></label>
        <input dojoType="dijit.form.ValidationTextBox" type="text" name="newName" id="newName"
            required="true" invalidmessage="<spring:message code="global.requiredInput"/>"/>
    </p>
    <button dojotype="dijit.form.Button" type="submit"
        onclick="return dijit.byId('saveAsDialog').isValid();">
    <spring:message code="global.save"/>
    </button>
    <button dojotype="dijit.form.Button" type="button" onclick="dijit.byId('saveAsDialog').hide();">
        <spring:message code="global.cancel"/>
    </button>
</div>

</form:form>

</body>
