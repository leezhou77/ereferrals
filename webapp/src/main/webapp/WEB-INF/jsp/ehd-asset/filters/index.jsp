<%--@elvariable id="myFilters" type="java.util.List<AssetFilter>"--%>
<%--@elvariable id="pubFilters" type="java.util.List<AssetFilter>"--%>
<%--@elvariable id="rc" type="org.springframework.web.servlet.support.RequestContext"--%>

<%@ include file="/WEB-INF/jsp/include.jsp" %>

<head>
    <title><spring:message code="assetFilter.title"/></title>

    <meta name="assetSidebar" content="true">

    <link rel="stylesheet" href="<c:url value="/css/ticketFilter.css"/>" type="text/css">

    <script type="text/javascript">
        dojo.addOnLoad(function() {
            dojo.query(".applyFilter").connect("onclick", function(e) {
                dojo.stopEvent(e);
                var url = dojo.attr(this, "href");
                openWindow(url, 'filterResults', 900, 600);
            });

            dojo.query(".deleteFilter").connect("onclick", function(e) {
                dojo.stopEvent(e);
                var url = dojo.attr(this, "href");
                var tr = $(this).parents("tr");
                if(confirm("<spring:message code="global.confirmdelete"/>")) {
                    dojo.xhrDelete({
                        url: url,
                        handleAs: 'text',
                        load: function(data) {
                            tr.fadeOut(function() {
                                 dojo.create("ul", {"class": "message success", innerHTML: '<li><spring:message code="assetFilter.delete.success"/></li><li class="close-bt"></li>'}, dojo.byId("flashMessage"), "only");
                            });
                        }
                    });
                }
            });
        });
    </script>

</head>

<body>
<section class="grid_7">
    <div class="block-content">
        <h1><spring:message code="ticketFilter.myFilters"/></h1>
        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a href="<c:url value="/assets/filters/new"/>">
                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>">
                        <spring:message code="global.createNew"/>
                    </a>
                </li>
            </ul>
        </div>

        <c:choose>
            <c:when test="${not empty myFilters}">
                <div class="no-margin">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col"><spring:message code="ticketFilter.filterName"/></th>
                            <th scope="col"><spring:message code="ticketFilter.visibility"/></th>
                            <th scope="col" class="table-actions"><spring:message code="ticketFilter.actions"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="filter" items="${myFilters}">
                            <tr>
                                <td><c:out value="${filter.name}"/></td>
                                <td>
                                    <c:choose>
                                        <c:when test="${filter.privateFilter}">
                                            <spring:message code="ticketFilter.private"/>
                                        </c:when>
                                        <c:otherwise>
                                            <spring:message code="ticketFilter.public"/>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="table-actions">
                                    <a href="<c:url value="/assets/filters/${filter.id}/results"/>" class="applyFilter"
                                       title="<spring:message code="ticketFilter.action.apply"/>">
                                        <img src="<c:url value="/images/theme/icons/fugue/control.png"/>" alt="">
                                    </a>
                                    <a href="<c:url value="/assets/filters/${filter.id}/edit"/>"
                                       title="<spring:message code="ticketFilter.action.edit"/>">
                                        <img src="<c:url value="/images/theme/icons/fugue/pencil.png"/>" alt="">
                                    </a>
                                    <a href="<c:url value="/assets/filters/${filter.id}"/>" class="deleteFilter"
                                       title="<spring:message code="ticketFilter.action.delete"/>">
                                        <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt="">
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </c:when>
            <c:otherwise>
                <p><spring:message code="ticketFilter.noneFound"/></p>
            </c:otherwise>
        </c:choose>
    </div>
</section>

<section class="grid_7">
    <div class="block-content">
        <h1><spring:message code="ticketFilter.publicFilters"/></h1>

        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a href="<c:url value="/assets/filters/new"/>">
                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>">
                        <spring:message code="global.createNew"/>
                    </a>
                </li>
            </ul>
        </div>

        <c:choose>
            <c:when test="${not empty pubFilters}">
                <div class="no-margin">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col"><spring:message code="ticketFilter.filterName"/></th>
                            <th scope="col"><spring:message code="ticketFilter.owner"/></th>
                            <th class="table-actions" scope="col"><spring:message code="ticketFilter.actions"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="filter" items="${pubFilters}">
                            <tr>
                                <td><c:out value="${filter.name}"/></td>
                                <td>${filter.user.firstName} ${filter.user.lastName}</td>
                                <td class="table-actions">
                                    <a href="javascript://" onclick="ticketFilterListAction('apply', '${filter.id}');"
                                       title="<spring:message code="ticketFilter.action.apply"/>">
                                        <img src="<c:url value="/images/theme/icons/fugue/control.png"/>" alt="">
                                    </a>
                                    <a href="<c:url value="/assets/filters/${filter.id}/edit"/>"
                                       title="<spring:message code="ticketFilter.action.edit"/>">
                                        <img src="<c:url value="/images/theme/icons/fugue/pencil.png"/>" alt="">
                                    </a>
                                    <a href="javascript://" onclick="ticketFilterListAction('delete', '${filter.id}');"
                                       title="<spring:message code="ticketFilter.action.delete"/>">
                                        <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt="">
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </c:when>
            <c:otherwise>
                <p><spring:message code="ticketFilter.noneFound"/></p>
            </c:otherwise>
        </c:choose>
    </div>
</section>
</body>
