<%@ include file="/WEB-INF/jsp/include.jsp" %>

<head>

    <meta name="assetSidebar" content="true">
    <title><spring:message code="softwareLicense.title"/></title>
    <script type="text/javascript">

        function viewSL(slId) {
            var url = '<c:url value="/asset/editSoftwareLicense.glml"/>';
            if (slId != null) url += '?softwareLicenseId=' + slId;
            window.location.href = url;
        }

        function deleteSL(slId) {
            if (confirm('<spring:message code="global.confirmdelete"/>')) {
                window.location.href = '<c:url value="/asset/deleteSoftwareLicense.glml"/>?slId=' + slId;
            }
        }

        function init() {
        }

        dojo.addOnLoad(init);

    </script>
</head>

<body>

<section class="grid_8">
    <div class="block-content">
        <h1><spring:message code="softwareLicense.title"/></h1>
        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a href="<c:url value="/asset/editSoftwareLicense.glml"/>">
                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="">
                        <spring:message code="softwareLicense.new"/>
                    </a>
                </li>
            </ul>
        </div>

        <div class="no-margin">

        <table class="table">
            <thead>
            <tr>
                <th scope="col"><spring:message code="softwareLicense.productName"/></th>
                <th scope="col"><spring:message code="softwareLicense.productVersion"/></th>
                <th scope="col"><spring:message code="softwareLicense.vendor"/></th>
                <th scope="col"><spring:message code="softwareLicense.numberOfLicenses"/></th>
                <th scope="col"><spring:message code="softwareLicense.numberInstalled"/></th>
                <th scope="col"><spring:message code="softwareLicense.supportExpirationDate"/></th>
                <th scope="col" class="table-actions"><spring:message code="global.actions"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${softwareLicenses}" var="sl">
                <tr>
                    <td><c:out value="${sl.productName}"/></td>
                    <td><c:out value="${sl.productVersion}"/></td>
                    <td><c:out value="${sl.vendor.name}"/></td>
                    <td><c:out value="${sl.numberOfLicenses}"/></td>
                    <td><c:out value="${fn:length(sl.installedOnAssets)}"/></td>
                    <td><fmt:formatDate value="${sl.supportExpirationDate}" type="date" dateStyle="MEDIUM"/></td>
                    <td class="table-actions">
                        <a href="javascript:viewSL(<c:out value="${sl.id}" />);">
                            <img alt="" src="<c:url value="/images/theme/icons/fugue/pencil.png" />"/></a>
                        <a href="javascript:deleteSL(<c:out value="${sl.id}" />);">
                            <img alt="" src="<c:url value="/images/theme/icons/fugue/cross-circle.png" />"/></a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        </div>
    </div>
</section>
</body>
