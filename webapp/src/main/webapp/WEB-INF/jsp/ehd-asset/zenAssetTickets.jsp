<%@include file="/WEB-INF/jsp/include.jsp" %>

<%--@elvariable id="workstation" type="net.grouplink.ehelpdesk.domain.zen.Workstation"--%>
<%--@elvariable id="tickets" type="java.util.List<Ticket>"--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
    <title>Tickets</title>
    <spring:theme code="css" var="cssfile"/>
    <c:if test="${not empty cssfile}">
        <link rel="stylesheet" href="<c:url value="${cssfile}"/>" type="text/css">
    </c:if>

    <style type="text/css">
        @import "<c:url value="/js/dojo-1.6.2/dijit/themes/tundra/tundra.css"/>";
    </style>

    <script type="text/javascript" src="<c:url value="/js/dojo-1.6.2/dojo/dojo.js"/>"
            djConfig="parseOnLoad: true"></script>
    <script type="text/javascript" src="<c:url value="/js/dojo-1.6.2/grouplink/includes.js"/>"></script>

    <script type="text/javascript" src="<c:url value="/js/ticketTable.js" />"></script>
    <script type="text/javascript" src="<c:url value="/js/scripts.js" />"></script>
    <script type="text/javascript" src="<c:url value="/js/ticketTable.js" />"></script>
    <script type="text/javascript" src="<c:url value="/js/scripts.js" />"></script>

    <script type="text/javascript">
//        dojo.require("dojo.parser");
//        dojo.require("dijit.Tooltip");

        function go(ticketId) {
            var url = "<c:url value="/tickets"/>";
            if (ticketId) url += "/" + ticketId + "/edit";
            openWindow(url, '_' + ticketId, 600, 600);
        }

        function init() {
            // decorate the ticket table
            tableruler();
            tablestripe();
        }

        dojo.addOnLoad(init);

    </script>
</head>

<body class="tundra">

<div style="margin:10px">
    <span style="font-weight:bold;"><spring:message code="assetTracker.ticketsForZenAsset"
                                                    arguments="${workstation.machineName}"/></span>

    <div style="border:1px solid lightgray;">
        <table class="stripe ruler paged" style="margin: 0 0 20px 0" width="100%">
            <thead>
            <tr class="title">
                <td>
                    <strong><spring:message code="ticket.number"/></strong>
                </td>
                <td>
                    <strong><spring:message code="ticket.group"/></strong>
                </td>
                <td>
                    <strong><spring:message code="ticket.subject"/></strong>
                </td>
                <td>
                    <strong><spring:message code="ticket.status"/></strong>
                </td>
            </tr>
            </thead>
            <tbody>

            <c:forEach items="${tickets}" var="ticket">
                <tr class="toberuled" onclick="go('<c:out value="${ticket.id}"/>');">
                    <td>
                        <c:out value="${ticket.id}"/>
                    </td>
                    <td>
                        <c:out value="${ticket.group.name}"/>
                    </td>
                    <td id="col_<c:out value="${ticket.id}"/>">
                <span dojoType="dijit.Tooltip" connectId="col_<c:out value="${ticket.id}"/>"
                      toggle="explode">
                    <table width="200" cellspacing="0" cellpadding="0">
                            <%--<tr>--%>
                            <%--<td height="30" style="background-image:url(<c:url value='/images/bubble_top.gif'/>);"/>--%>
                            <%--</tr>--%>
                        <tr>
                                <%--<td style="background-image:url(<c:url value='/images/bubble_middle.gif'/>); padding: 0 5px 0 5px;">--%>
                            <td style="padding: 0 5px 0 5px;">
                                <fmt:formatDate value="${ticket.createdDate}" type="date"
                                                dateStyle="medium"/>
                                - ${ticket.contact.firstName} ${ticket.contact.lastName}
                                <hr class="vsep">
                                <strong>
                                    <spring:message code="ticket.subject"/>
                                    :</strong>&nbsp;&nbsp;<c:out value="${ticket.subject}"/>
                                <hr class="vsep">
                                <strong>
                                    <spring:message code="ticket.note"/>
                                    :</strong>&nbsp;&nbsp;<c:out value="${ticket.note}"/>
                            </td>
                        </tr>
                            <%--<tr>--%>
                            <%--<td height="12" style="background-image:url(<c:url value='/images/bubble_bottom.gif'/>)"/>--%>
                            <%--</tr>--%>
                    </table>
                            </span>
                        <ehd:trimString value="${ticket.subject}" length="25"/>
                    </td>
                    <td>
                        <c:out value="${ticket.status.name}"/>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>

    </div>
</div>
</body>
</html>
