<%@ include file="/WEB-INF/jsp/include.jsp" %>

<head>
    <meta name="assetSidebar" content="true">
    <title><spring:message code="assetTracker.editAssetType"/></title>
    <script type="text/javascript">

        function cancelForm() {
            window.location.href = '<c:url value="/asset/assetTypes.glml"/>';
        }

        function init() {
        }

        dojo.addOnLoad(init);
    </script>
</head>

<body>

<section class="grid_5">
    <div class="block-content">
        <h1><spring:message code="assetTracker.editAssetType"/></h1>

        <form:form commandName="assetType" method="post" cssClass="form">
            <spring:hasBindErrors name="assetType">
                <p class="message error no-margin">
                    <form:errors path="*"/>
                </p>
            </spring:hasBindErrors>
            <input type="hidden" name="assetTypeId" value="<c:out value="${assetType.id}"/>"/>

            <p class="required">
                <form:label path="name"><spring:message code="global.name"/></form:label>
                <form:input path="name" cssErrorClass="error" dojoType="dijit.form.TextBox" maxlength="255"/>
            </p>

            <p>
                <form:label path="notes"><spring:message code="global.description"/></form:label>
                <form:input path="notes" cssErrorClass="error" dojoType="dijit.form.TextBox" maxlength="255"/>
            </p>

            <p class="grey-bg no-margin" id="commandButtons">
                <button type="submit"><spring:message code="global.save"/></button>
                <button type="button" onclick="cancelForm();"><spring:message
                        code="global.cancel"/></button>
            </p>
        </form:form>
    </div>
</section>
</body>
