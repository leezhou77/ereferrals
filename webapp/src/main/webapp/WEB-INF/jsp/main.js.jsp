<script type="text/javascript">
    function quickTicketSearch(){
        var query = document.getElementById("searchQuery").value;
        window.location = "<c:url value="/tickets/search/fullTextSearch.glml"/>?query=" + encodeURIComponent(query);
    }

    $(function() {
        $("#searchQuery").keyup(function(event) {
            if (wasEnterKeyPressed(event))
                quickTicketSearch();
        });
    });

    function wasEnterKeyPressed(e) {
        var keynum;
        if(window.event) { //IE
            keynum = e.keyCode;
        }
        else if(e.which) { //The rest of the world
            keynum = e.which;
        }
        return keynum == 13 || keynum == 3; //enter key and numpad enter key
    }

    function openTicketWindow(ticketId) {
        var url = "<c:url value="/tickets/"/>" + ticketId + "/edit";
        openWindow(url, '_' + ticketId, '800px', '850px');
    }
</script>
