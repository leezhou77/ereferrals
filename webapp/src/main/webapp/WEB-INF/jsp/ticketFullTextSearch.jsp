<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>
    <title>
        <spring:message code="header.quickTicketSearch"/>
    </title>

    <style type="text/css">
        #searchResults li > * {
            margin-bottom: 5px;
        }

        .searchResult {
            cursor: pointer;
        }

        .tid {
            color: #0E774A;
            font-weight: bold;
        }

        .pagernav {
            padding-bottom: 30px;
            margin-right: 20px;
        }

        #pagination {
            width: 100%;
            margin: auto;
        }

    </style>

    <script type="text/javascript">
        function setPageCookie(fld) {
            var value = fld.value;
            setCookie("ticketFullTextSearchPageSize", value);
        }

        dojo.addOnLoad(function() {
            dojo.query(".searchResult").connect("onclick", function() {
                var ticketId = dojo.attr(this, "data-ticket-id");
                openWindow('<c:url value="/tickets/"/>' + ticketId + '/edit', '_' + ticketId, '800px', '850px');
            });
        });
    </script>
</head>

<body>
<article class="container_12">
    <section class="grid_8">
        <div class="block-content">
            <h1>
                <c:choose>
                    <c:when test="${ticketCount == 0}">
                        <spring:message code="search.noResultsFor"/> <em><c:out value="${query}"/></em>
                    </c:when>
                    <c:otherwise>
                        <spring:message code="search.resultsFor"/> <em><c:out value="${query}"/></em>
                    </c:otherwise>
                </c:choose>
            </h1>

            <c:if test="${!empty error}">
                <h2><spring:message code="error.error"/></h2>
                <p>${error}</p>
            </c:if>

            <div>
                <ol id="searchResults" class="extended-list no margin">
                <c:forEach items="${tickets}" var="ticket">
                    <li class="searchResult" data-ticket-id="${ticket.id}">
                        <h3>
                            <c:out value="${ticket.subject}" />
                        </h3>
                        <div>
                            <c:choose>
                                <c:when test="${fn:length(ticket.note) > 149}">
                                    <c:out value="${fn:substring(ticket.note, 0, 148)}"/> ...
                                </c:when>
                                <c:otherwise>
                                    <c:out value="${ticket.note}"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <div class="tid">
                            <span><spring:message code="ticket.number"/><c:out value="${ticket.id}"/></span>
                        </div>
                    </li>
                </c:forEach>
                </ol>

                <c:if test="${pageCount > 1}">
                <div id="pagination">
                    <div class="pagernav">
                        <c:forEach begin="${currentPage - 6 < 1 ? 1 : currentPage - 6}" end="${currentPage + 6 > pageCount - 1 ? pageCount - 1 : currentPage + 6}" var="crtpg">
                            <c:choose>
                                <c:when test="${crtpg == currentPage}">
                                    <strong>
                                        <c:out value="${crtpg}"/>
                                    </strong>
                                </c:when>
                                <c:otherwise>
                                    <a href="<c:url value="/tickets/search/fullTextSearch.glml">
                                                <c:param name="query" value="${query}"/>
                                                <c:param name="page" value="${crtpg}"/>
                                             </c:url>">
                                        <c:out value="${crtpg}"/>
                                    </a>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                        &nbsp;...&nbsp;
                        <a href="<c:url value="/tickets/search/fullTextSearch.glml">
                                     <c:param name="query" value="${query}"/>
                                     <c:param name="page" value="${pageCount}"/>
                                 </c:url>">
                            <c:out value="${pageCount}"/>
                        </a>
                        <c:if test="${currentPage < pageCount}">
                            <a href="<c:url value="/tickets/search/fullTextSearch.glml">
                                        <c:param name="query" value="${query}"/>
                                        <c:param name="page" value="${currentPage + 1}"/>
                                     </c:url>">
                                <spring:message code="global.next"/>&gt;
                            </a>
                        </c:if>
                    </div>
                    <div class="pageSize">
                        <spring:message code="search.recordsPerPage" var="ps"/>
                        <select name="pageSize" onChange="setPageCookie(this); location.reload(true);">
                            <c:forTokens items="10,20,30,50,100" delims="," var="crtps">
                                <option <c:if test="${pageSize == crtps}">selected</c:if> value="<c:out value="${crtps}"/>">
                                    <c:out value="${crtps} ${ps}"/>
                                </option>
                            </c:forTokens>
                        </select>
                    </div>
                </div>
                </c:if>
            </div>
        </div>
    </section>
</article>
</body>