<%@ include file="/WEB-INF/jsp/include.jsp" %>
<jsp:useBean id="license" scope="request" class="net.grouplink.ehelpdesk.web.domain.License"/>
<head>
    <meta name="configSidebar" content="true">
    <title>
        <spring:message code="licensing.title"/>
    </title>

    <script type="text/javascript">
        dojo.addOnLoad(function() {
            dojo.connect(dojo.byId("licenseFile"), "onchange", function() {
                dojo.attr(dojo.byId("loadFile"), "disabled", this.value.trim() == '');
            });
        });
    </script>
</head>
<body>
<form:form commandName="license" enctype="multipart/form-data" cssClass="form">
<section class="grid_3">
    <div class="block-content">
        <h1><spring:message code="licensing.title"/></h1>
        <c:if test="${!applicationScope['licenseExists']}">
            <p class="error message no-margin">
                <spring:message code="licensing.noFileFound"/>
            </p>
        </c:if>
        <c:if test="${applicationScope['licenseExists']}">
            <p>
                <form:label path="description"><spring:message code="licensing.licensee"/></form:label>
                <c:out value="${license.description}"/>
            </p>
            <p>
                <form:label path="edition"><spring:message code="licensing.edition"/></form:label>
                <c:out value="${license.edition}"/>
            </p>
            <p>
                <form:label path="groupCount"><spring:message code="licensing.groupNumber"/></form:label>
                <c:out value="${license.groupCount}"/>
            </p>
            <p>
                <form:label path="technicianCount"><spring:message code="licensing.tekNumber"/></form:label>
                <c:out value="${usedTechs}"/> / <c:out value="${license.technicianCount}"/>
            </p>
            <c:if test="${license.wfParticipantCount > 0 || usedWfps > 0}">
            <p>
                <form:label path="wfParticipantCount"><spring:message code="licensing.wfpNumber"/></form:label>
                <c:out value="${usedWfps}"/> / <c:out value="${license.wfParticipantCount}"/>
            </p>
            </c:if>
            <p>
                <form:label path="upgradeDate"><spring:message code="licensing.upgradeDate"/></form:label>
                <fmt:formatDate value="${license.upgradeDate}" type="date" var="upgDate"/>
                <c:out value="${upgDate}"/>
            </p>
            <p>
                <form:label path="expirationDate"><spring:message code="licensing.expirationDate"/></form:label>
                <fmt:formatDate value="${license.expirationDate}" type="date" var="expDate"/>
                <c:out value="${expDate}"/>
            </p>
            <p>
                <form:label path="dashboards"><spring:message code="licensing.dashboards"/></form:label>
                <span class="keyword">
                    <c:choose>
                        <c:when test="${license.dashboards eq true}">
                            <spring:message code="global.enabled"/>
                        </c:when>
                        <c:otherwise>
                            <spring:message code="global.disabled"/>
                        </c:otherwise>
                    </c:choose>
                </span>
            </p>
        </c:if>
    </div>
</section>
<section class="grid_7">
    <div class="block-content">
        <h1><spring:message code="licensing.uploadLicense"/></h1>
        <spring:hasBindErrors name="license">
            <p class="message error no-margin">
                <form:errors path="*"/>
            </p>
        </spring:hasBindErrors>
        <p>
            <spring:bind path="license.fileData">
                <input type="file" name="${status.expression}" id="licenseFile">
            </spring:bind>
            <input type="submit" name="loadFile" id="loadFile" value="<spring:message code="licensing.load" />" disabled="disabled">
        </p>
    </div>
</section>
</form:form>
</body>
