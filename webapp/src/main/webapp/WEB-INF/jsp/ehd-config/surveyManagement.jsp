<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>
    <meta name="configSidebar" content="true">

<script type="text/javascript">
    function toggleGroupHasChanged() {
        document.getElementById("groupHasChanged").value = true;
    }

    function toggleCategoryHasChanged(idx) {
        document.getElementById("categoryHasChanged_"+idx).value = true;
    }

    function toggleCategoryOptionHasChanged(idx) {
        document.getElementById("categoryOptionHasChanged_"+idx).value = true;
    }

    function onTabDownloadEnd(form, groupId) {
        dojo.connect(form, "onsubmit", function(evt) {
            dojo.stopEvent(evt);
            dojo.xhrPost({
                form: form,
                load: function() {
                    var contentPane = dijit.byId("contentPane_" + groupId);
                    contentPane.refresh();
                    dojo.place('<p class="message success no-margin"><spring:message code="surveys.changes.saved"/></p>', "successMessage", "only");
                }
            });
        });
    }

    dojo.addOnLoad(function() {
        var tabContainer = new dijit.layout.TabContainer({
            style: "width: 100%; height: 100%; margin-top:10px",
            persist: true,
            doLayout: false
        }, 'mainTabContainer');
        <c:forEach var="grp" items="${groups}">
            <c:if test="${grp.active}">
                tabContainer.addChild(new dijit.layout.ContentPane({
                    id: "contentPane_<c:out value="${grp.id}"/>",
                    title: "<c:out value="${grp.name}"/>",
                    href: "<c:url value="/config/surveys/management/surveyActivationList.glml"><c:param name="sgid" value="${grp.id}"/></c:url>",
                    onDownloadEnd: function() {
                        var form = dojo.byId("surveyActivate_<c:out value="${grp.id}"/>");
                        onTabDownloadEnd(form, "<c:out value="${grp.id}"/>");
                    }
                }));
            </c:if>
        </c:forEach>
        tabContainer.addChild(new dijit.layout.ContentPane({
            id: "contentPane_allGroups",
            title: "<spring:message code='surveys.all.groups'/>",
            href: "<c:url value="/config/surveys/management/surveyActivationList.glml"/>",
            onDownloadEnd: function() {
                var form = dojo.byId("surveyActivate_-9999");
                onTabDownloadEnd(form, "allGroups");
            }
        }));
        dojo.connect(tabContainer, "selectChild", function() {
            var successMsg = dojo.byId("successMessage");
            if(successMsg.innerHTML != "") {
                successMsg.innerHTML = "";
            }
        });
        tabContainer.startup();
    });
</script>

</head>
<body>
<section class="grid_10">
    <div id="successMessage"></div>
    <div class="block-content">
        <h1><spring:message code="surveys.management"/></h1>
        <div id="mainTabContainer"></div>
    </div>
</section>
</body>
