<%@ include file="/WEB-INF/jsp/include.jsp" %>
<jsp:useBean id="field" class="net.grouplink.ehelpdesk.domain.LdapField" />
<table>
    <tr style="font-weight:bold">
        <td><spring:message code="ldapconfig.ldapField" /></td><td><spring:message code="ldapconfig.ldapField.label" /></td><td><spring:message code="ldapconfig.ldapField.showOnTicket" /></td><td></td>
    </tr>
    <c:forEach items="${ldapFieldList}" var="field" varStatus="index">
        <c:set value="even" var="classVar" />
        <c:if test="${index.index%2 == 0}" >
            <c:set value="odd" var="classVar" />
        </c:if>
        <tr class="${classVar}">
            <td align="right">
                <c:out value="${field.name}" />
            </td>
            <td>
                <input type="text" name="label" size="30" value="<c:out value="${field.label}" />" onchange="modifyLdapLabel('<c:out value="${field.id}" />', this.value)" />
            </td>
            <td align="center">
                <input type="checkbox" name="showOnTicket" onclick="modifyShowOnTicket('<c:out value="${field.id}" />', this.checked)" <c:if test="${field.showOnTicket}" >checked="checked"</c:if>/>
            </td>
            <td style="padding: 0px 5px 0px 5px">
                <a href="javascript://" onclick="removeLdapField('<c:out value="${field.id}" />')" ><img src="<c:url value="/images/trash.gif" />" /></a>
            </td>
        </tr>
    </c:forEach>
</table>