<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="treeNode" type="net.grouplink.ehelpdesk.common.utils.TreeNode"--%>
<%--@elvariable id="jsonString" type="java.lang.String"--%>

<head>
<meta name="configSidebar" content="true">
<title>
    <spring:message code="ldapconfig.title"/>
</title>

<script type="text/javascript">
dojo.addOnLoad(function() {
    dojo.require("dojo.NodeList-traverse");

    dojo.connect(dojo.byId("addSearchDn"), "onclick", function() {
        var searchInput = dijit.byId("searchDn");
        var value = dojo.attr(searchInput, "value");
        var nextId = 0;
        if($('.searchDn').length > 0) {
            nextId = parseInt($('#baseSearchList li:last-child input').attr('id').replace('baseSearchList-', '')) + 1;
        }
        dojo.create("li", {innerHTML: value + '<input id="baseSearchList-' + nextId + '" class="searchDn" type="hidden" name="baseSearchList[' + nextId + ']" value="' + value + '"><span class="close-bt-no-js"></span>'}, "baseSearchList", "last");
        searchInput.set("value", "");
        dojo.style("baseSearchList", "display", "block");
    });

    var ldapCbx = dijit.byId("ldapIntegration1");
    dojo.connect(ldapCbx.domNode, "onclick", function() {
        toggleLdapFields(ldapCbx, "ldapFields");
    });
    toggleLdapFields(ldapCbx, "ldapFields");

    function toggleLdapFields(cbx, elId) {
        var display = "none";
        if(dojo.attr(cbx, "checked")) {
            display = "block";
        }
        dojo.style(dojo.byId(elId), "display", display);
    }

    var fetchBtn = dijit.byId("fetchDns");
    var origFetchBtnLbl = fetchBtn.get("label");
    dojo.connect(dojo.byId("fetchDns"), "onclick", function() {
        fetchBtn.set("disabled", true);
        fetchBtn.set("label", "<spring:message code="global.working" javaScriptEscape="true"/>");
        showBaseDn(false);
    });
    showBaseDn(true);

    function showBaseDn(onLoad) {
        var url = dojo.attr(dojo.byId("url"), "value");
        if(url != "") {
            var store = new dojo.data.ItemFileReadStore({
                url: "<c:url value="/config/ldap/fetchDns.glml"/>?ipAddress=" + url,
                urlPreventCache: true
            });
            store.fetch({
                query: {},
                onComplete: function(items, request) {
                    if(items.length) {
                        dijit.byId("base").set("store", store);
                        dijit.byId("base").set("searchAttr", "dn");
                        toggleLdapFields(ldapCbx, "ldapConfigFields");
                    } else if(!onLoad) {
                        Ehd.Utils.flash("error", "<spring:message code="ldapconfig.noDnsFound" javaScriptEscape="true"/>", true);
                        dojo.style(dojo.byId("ldapConfigFields"), "display", "none");
                    }
                    dijit.byId("fetchDns").set("disabled", false);
                    fetchBtn.set("label", origFetchBtnLbl);
                }
            });
        } else if(!onLoad) {
            Ehd.Utils.flash("error", "<spring:message code="ldapconfig.urlRequired" javaScriptEscape="true"/>", true);
            dijit.byId("fetchDns").set("disabled", false);
            fetchBtn.set("label", origFetchBtnLbl);
            dojo.style(dojo.byId("ldapConfigFields"), "display", "none");
        }

        $(".close-bt-no-js").live("click", function() {
            var stringVal = dojo.query(this).prev("input[type=hidden]").attr("value");
            var node = dojo.query(this).parent("li")[0];
            dojo.fadeOut({
                node: node,
                onEnd: function() {
                    dojo.create("input", {"type": "hidden", "name": "inclDnsToDelete", "value": stringVal}, "ldap", "first");
                    var lis = dojo.query("#baseSearchList li");
                    if(lis.length == 0) {
                        dojo.style(dojo.byId("baseSearchList"), "display", "none");
                    }
                    dojo.destroy(node);
                }
            }).play();
        });
    }
});
</script>
</head>

<body>
<section class="grid_6">
    <div class="block-content">
        <h1><spring:message code="ldapconfig.title"/></h1>

        <c:if test="${ldap.ldapIntegration}">
        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a id="fieldSetup" href="<c:url value="/config/ldap/ldapFields"/>">
                        <img src="<c:url value="/images/theme/icons/fugue/tags-label.png"/>">
                        <spring:message code="ldapconfig.fieldSetup"/>
                    </a>
                </li>
            </ul>
        </div>
        </c:if>

        <form:form commandName="ldap" action="" method="post" cssClass="form">
            <spring:hasBindErrors name="ldap">
                <p class="message error no-margin">
                    <form:errors path="*"/>
                </p>
            </spring:hasBindErrors>
            <p>
                <form:checkbox path="ldapIntegration" dojoType="dijit.form.CheckBox"/><form:label cssClass="inline" path="ldapIntegration"><spring:message code="ldapconfig.ldapIntegration"/></form:label>
            </p>
            <div id="ldapFields">
                <p>
                    <form:label path="url"><spring:message code="ldapconfig.ldapUrl"/><tags:helpIcon id="urlHelp" messageCode="ldapconfig.ldapUrl.desc"/></form:label>
                    <form:input path="url" dojoType="dijit.form.TextBox" cssStyle="width:350px"/><button id="fetchDns" type="button" dojoType="dijit.form.Button"><spring:message code="ldap.fetchDns"/></button>
                </p>
                <div id="ldapConfigFields" style="display:none">
                    <p>
                        <form:label path="base"><spring:message code="ldapconfig.ldapBase"/><tags:helpIcon id="baseHelp" messageCode="ldapconfig.ldapBase.desc"/></form:label>
                        <form:input path="base" dojoType="dijit.form.ComboBox" cssStyle="width:350px"/>
                    </p>
                    <p>
                        <form:label path="login"><spring:message code="ldapconfig.ldapLogin"/><tags:helpIcon id="loginHelp" messageCode="ldapconfig.ldapLogin.desc"/></form:label>
                        <form:input path="login" dojoType="dijit.form.TextBox" cssStyle="width:350px"/>
                    </p>
                    <p>
                        <form:label path="changePassword"><spring:message code="ldapconfig.ldapPassword"/><tags:helpIcon id="passwordHelp" messageCode="ldapconfig.ldapPassword.desc"/></form:label>
                        <form:password path="changePassword" dojoType="dijit.form.TextBox" cssStyle="width:350px" showPassword="true"/>
                    </p>
                    <p>
                        <form:label path="usernameAttribute"><spring:message code="ldapconfig.usernameAttribute"/><tags:helpIcon id="usernameAttHelp" messageCode="ldapconfig.usernameAttribute.desc"/></form:label>
                        <form:input path="usernameAttribute" dojoType="dijit.form.TextBox" cssStyle="width:350px"/>
                    </p>
                    <p>
                        <form:label path="filter"><spring:message code="ldapconfig.ldapFilter"/><tags:helpIcon id="filterHelp" messageCode="ldapconfig.ldapFilter.desc"/></form:label>
                        <form:input path="filter" dojoType="dijit.form.TextBox" cssStyle="width:350px"/>
                    </p>
                    <p>
                        <form:checkbox path="ehdFallback" dojoType="dijit.form.CheckBox"/>
                        <form:label path="ehdFallback" cssClass="inline"><spring:message code="ldapconfig.ehdFallback"/><tags:helpIcon id="fallbackHelp" messageCode="ldapconfig.ehdFallback.desc"/></form:label>
                    </p>
                    <p>
                        <form:label path="locationAttribute"><spring:message code="ldapconfig.locationAttribute"/><tags:helpIcon id="locHelp" messageCode="ldapconfig.locationAttribute.desc"/></form:label>
                        <form:input path="locationAttribute" dojoType="dijit.form.TextBox" cssStyle="width:350px"/>
                    </p>
                    <p>
                        <form:checkbox path="createLocation" dojoType="dijit.form.CheckBox"/>
                        <form:label path="createLocation" cssClass="inline"><spring:message code="ldapconfig.createLocation"/><tags:helpIcon id="createLocHelp" messageCode="ldapconfig.createLocation.desc"/></form:label>
                    </p>
                    <div class="with-margin">
                        <form:label path="baseSearchList"><spring:message code="ldapconfig.ldapBaseSearch"/><tags:helpIcon id="baseSearchListHelp" messageCode="ldapconfig.ldapBaseSearch.desc"/></form:label>
                        <div class="with-margin"><input id="searchDn" dojoType="dijit.form.TextBox" style="width:350px"><button id="addSearchDn" type="button" dojoType="dijit.form.Button"><spring:message code="global.add"/></button></div>
                        <ul id="baseSearchList" class="mini-blocks-list box" <c:if test="${empty ldap.baseSearchList}">style="display:none"</c:if>>
                            <c:forEach items="${ldap.baseSearchList}" var="item" varStatus="row">
                                <li><c:out value="${item}"/><form:hidden id="baseSearchList-${row.index}" path="baseSearchList[${row.index}]" cssClass="searchDn"/><span class="close-bt-no-js"></span></li>
                            </c:forEach>
                        </ul>

                    </div>
                </div>
            </div>
            <p class="no-margin grey-bg">
                <button type="submit"><spring:message code="global.save"/></button><tags:helpIcon id="submitHelp" messageCode="ldapconfig.ldapField.saveFirst"/>
            </p>
        </form:form>
    </div>
</section>
</body>