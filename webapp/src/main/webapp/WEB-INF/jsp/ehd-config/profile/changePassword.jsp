<%@ include file="/WEB-INF/jsp/include.jsp" %>

<c:set var="action"><c:url value="/config/profile/${user.id}/changePass"><c:param name="dialog" value="true"/></c:url></c:set>

<form:form id="cpwdForm" modelAttribute="changePassword" cssClass="form" method="put" action="${action}">
    <div id="messages" class="no-margin">
        <spring:hasBindErrors name="changePassword">
            <p id="bindErrors" class="message error no-margin">
                <form:errors path="*"/>
            </p>
        </spring:hasBindErrors>
    </div>
    <br/>
    <fieldset>
        <p>
            <form:label path="password1"><spring:message code="login.password"/></form:label>
            <form:password path="password1" dojoType="dijit.form.TextBox" size="50"/>
        </p>

        <p>
            <form:label path="password2"><spring:message code="login.retypepassword"/></form:label>
            <form:password path="password2" dojoType="dijit.form.TextBox" size="50"/>
        </p>

    </fieldset>
    <p>
        <button type="submit" id="cpwdSaveBtn"><spring:message code="global.save"/></button>
        <button type="button" id="cpwdCancelBtn"><spring:message code="global.cancel"/></button>
    </p>
</form:form>
