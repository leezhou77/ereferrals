<%@ include file="/WEB-INF/jsp/include.jsp"%>
<!DOCTYPE html>
<html>
<head></head>
<body>
<form id="surveyActivate_<c:out value="${sgid}"/>" action="<c:url value='/config/surveys/management/surveyActivate.glml'/>" method="post" >
    <input type="hidden" name="sgid" value="<c:out value='${sgid}'/>"/>

    <c:choose>
        <c:when test="${not empty group.categories or sgid == -9999}">
            <c:choose>
                <c:when test="${sgid == -9999}">
                    <p>
                        <spring:message code="surveys.enable.all1"/>
                        <input type="radio" name="rbEnableSurveyForAllGroups" value="true"
                               <c:if test="${surveyActivatedForAll == true}"> checked</c:if>
                               ><spring:message code="global.yes"/>
                        <input type="radio" name="rbEnableSurveyForAllGroups" value="false"
                               <c:if test="${surveyActivatedForAll == false}"> checked</c:if>
                               ><spring:message code="global.no"/>
                    </p>
                </c:when>
                <c:otherwise>
                    <p>
                        <input type="hidden" id="groupHasChanged" name="groupHasChanged" value="false"/>
                        <spring:message code="surveys.enable.all2"/>
                        <input type="radio" name="rbEnableSurveyForCatOptsInGroup" value="true"
                               onclick="toggleGroupHasChanged();"
                               <c:if test="${surveyActivatedForGroup == true}"> checked</c:if>
                               <c:if test="${surveyActivatedForAll == true}"> disabled</c:if>
                               ><spring:message code="global.yes"/>
                        <input type="radio" name="rbEnableSurveyForCatOptsInGroup" value="false"
                               onclick="toggleGroupHasChanged();"
                               <c:if test="${surveyActivatedForGroup == false}"> checked</c:if>
                               <c:if test="${surveyActivatedForAll == true}"> disabled</c:if>
                               ><spring:message code="global.no"/>
                    </p>


                    <table class="table">
                        <thead>
                            <tr>
                                <th>
                                    <spring:message code="surveys.category"/>
                                </th>
                                <th>
                                    <spring:message code="surveys.category.option"/>
                                </th>
                                <th>
                                    <spring:message code="surveys.enabled"/>
                                </th>
                            </tr>
                        </thead>


                        <tbody>

                            <c:forEach var="item" items="${surveyItems}" varStatus="idx">
                                <tr>
                                    <c:if test="${item.type == 2}">
                                        <td>
                                            <c:out value="${item.objectName}"/>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <input type="hidden" id="categoryHasChanged_<c:out value="${item.objectId}"/>" name="categoryHasChanged_<c:out value="${item.objectId}"/>" value="false"/>
                                            <input type="radio" name="rbEnableSurveyForAllOptsInCat_<c:out value="${item.objectId}"/>" value="true"
                                                   <c:if test="${item.surveyEnabled == true}">checked</c:if>
                                                   onclick="toggleCategoryHasChanged(<c:out value="${item.objectId}"/>);"
                                                   <c:if test="${surveyActivatedForGroup == true}">disabled</c:if>
                                                   ><spring:message code="global.yes"/>
                                            <input type="radio" name="rbEnableSurveyForAllOptsInCat_<c:out value="${item.objectId}"/>" value="false"
                                                   <c:if test="${item.surveyEnabled == false}">checked</c:if>
                                                   onclick="toggleCategoryHasChanged(<c:out value="${item.objectId}"/>);"
                                                   <c:if test="${surveyActivatedForGroup == true}">disabled</c:if>
                                                   ><spring:message code="global.no"/>
                                        </td>
                                    </c:if>

                                    <c:if test="${item.type == 3}">
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                             <c:out value="${item.objectName}"/>
                                        </td>
                                        <td>
                                            <input type="hidden" id="categoryOptionHasChanged_<c:out value="${item.objectId}"/>" name="categoryOptionHasChanged_<c:out value="${item.objectId}"/>" value="false"/>
                                            <input type="radio" name="rbEnableSurveyForCatOpt_<c:out value="${item.objectId}"/>" value="true"
                                                   <c:if test="${item.surveyEnabled ==  true}"> checked</c:if>
                                                   onclick="toggleCategoryOptionHasChanged(<c:out value="${item.objectId}"/>);"
                                                   <c:if test="${item.controlEnabled == false}"> disabled</c:if>
                                                   ><spring:message code="global.yes"/>
                                            <input type="radio" name="rbEnableSurveyForCatOpt_<c:out value="${item.objectId}"/>" value="false"
                                                   <c:if test="${item.surveyEnabled == false}"> checked</c:if>
                                                   onclick="toggleCategoryOptionHasChanged(<c:out value="${item.objectId}"/>);"
                                                   <c:if test="${item.controlEnabled == false}"> disabled</c:if>
                                                   ><spring:message code="global.no"/>
                                        </td>
                                    </c:if>
                                </tr>
                            </c:forEach>

                        </tbody>
                    </table>

                </c:otherwise>
            </c:choose>
            <p class="grey-bg">
                <button type="submit"><spring:message code="global.save"/></button>
            </p>
        </c:when>
        <c:otherwise>
            <p><spring:message code="surveys.noCatsOrOpts"/></p>
        </c:otherwise>
    </c:choose>

</form>
</body>
</html>