<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="group" type="net.grouplink.ehelpdesk.domain.Group"--%>
<%--@elvariable id="permissionSchemeList" type="java.util.List"--%>
<%--@elvariable id="urgList" type="java.util.List"--%>
<%--@elvariable id="urgTicketPool" type="net.grouplink.ehelpdesk.domain.UserRoleGroup"--%>
<c:choose>
    <c:when test="${group.id eq null}">
        <c:set var="method" value="post"/>
        <c:set var="action"> <c:url value="/config/groups"/></c:set>
        <c:set var="formTitle"><spring:message code="group.new"/></c:set>
    </c:when>
    <c:otherwise>
        <c:set var="method" value="put"/>
        <c:set var="action"> <c:url value="/config/groups/${group.id}"/> </c:set>
        <c:set var="formTitle"><spring:message code="group.edit.title" arguments="${group.name}"/></c:set>
    </c:otherwise>
</c:choose>
<html>
<head>
<meta name="configSidebar" content="true"/>
<title><c:out value="${formTitle}"/></title>

<link rel="stylesheet" type="text/css" href="<c:url value="/css/groups.css"/>"/>
<link rel="stylesheet" href="<c:url value="/js/chosen/chosen.css"/>" type="text/css">
<script type="text/javascript" src="<c:url value="/js/chosen/chosen.jquery.js"/>"></script>

<script type="text/javascript">

dojo.addOnLoad(function() {

    dojo.connect(dojo.byId("groupCancel"), "onclick", function() {
        location.href = '<c:url value="/config/groups"/>';
    });

    <c:if test="${not empty group.id}">

    // Setup the Tree for displaying the category and category options for this group
    var catCatOptStore = new dojo.data.ItemFileReadStore({
        url: "<c:url value="/config/groups/${group.id}/categoryjson"/>",
        urlPreventCache: true
    });

    var catTreeModel = new dijit.tree.ForestStoreModel({
        store: catCatOptStore,
        query: { "type": "category" },
        rootId: "root",
        rootLabel: "<c:out value="${group.name}"/>",
        childrenAttrs: ["children"]
    });

    var catTree = new dijit.Tree({
        model: catTreeModel,
        getIconClass: catCatOptIcons
    }, "categoryTree");

    function catCatOptIcons(item, opened) {
        if (item == catTreeModel.root) {
            return "groupIcon";
        } else {
            return catCatOptStore.getValue(item, "type") + "Icon";
        }
    }

    // Setup Category and Category Option Tree operations

    // Category dialog
    function dlgCategoryDownloadEnd() {
        var form = dojo.byId("category");
        dojo.connect(form, "onsubmit", function(evt) {
            dojo.stopEvent(evt);
            dojo.xhrPost({
                form: form,
                load: function(response) {
                    // check for validation errors
                    var tempDiv = document.createElement("div");
                    tempDiv.innerHTML = response;
                    var errors = dojo.query("[id=bindErrors]", tempDiv);
                    if (errors.length) {
                        categoryDlg.set("content", response);
                        dlgCategoryDownloadEnd();
                    } else {
                        categoryDlg.hide();
                        reloadTree(catTree);
                    }
                }
            });
        });

        dojo.connect(dojo.byId("catCancelBtn"), "onclick", function() {
            categoryDlg.hide();
        });

        // Apply the theme js to the form in the dialog
        $("#category").applyTemplateSetup();
    }

    var categoryDlg = new dijit.Dialog({
        title: "<spring:message code="ticketSettings.addCategory"/>",
        onDownloadEnd: dlgCategoryDownloadEnd
    });

    dojo.connect(dojo.byId("addCategoryBtn"), "onclick", function(evt) {
        evt.preventDefault();
        categoryDlg.set("title", "<spring:message code="ticketSettings.addCategory"/>");
        categoryDlg.set("href", "<c:url value="/config/groups/${group.id}/categories/new"><c:param name="dialog" value="true"/></c:url>");
        categoryDlg.show();
    });

    // Category Option dialog
    var categoryOptionDlg = new dijit.Dialog({
        title: "<spring:message code="ticketSettings.addNewCategoryOption"/>",
        onDownloadEnd: dlgCatOptDownloadEnd
    });

    function dlgCatOptDownloadEnd() {
        var form = dojo.byId("categoryOption");
        dojo.connect(form, "onsubmit", function(evt) {
            dojo.stopEvent(evt);
            dojo.xhrPost({
                form: form,
                load: function(response) {
                    // check for validation errors
                    var tempDiv = document.createElement("div");
                    tempDiv.innerHTML = response;
                    var errors = dojo.query("[id=bindErrors]", tempDiv);
                    if (errors.length) {
                        categoryOptionDlg.set("content", response);
                        dlgCatOptDownloadEnd();
                    } else {
                        if (isEdit) {
                            isEdit = false;
                        } 
                        categoryOptionDlg.hide();
                        reloadTree(catTree);
                    }
                }
            });
        });

        new dijit.form.FilteringSelect({
                    id: "catFilterSelectBox",
                    name: "category",
                    value: catSelectedValue,
                    store: categoryByGroupStore,
                    searchAttr: "label"
                },
                "catFilterSelectBox");

        dojo.connect(dojo.byId("catOptCancelBtn"), "onclick", function() {
            categoryOptionDlg.hide();
        });

        $("#categoryOption").applyTemplateSetup();
    }

    var catSelectedValue = "";
    var yerl = null;
    var isEdit = false;
    dojo.connect(dojo.byId("addCatOptBtn"), "onclick", function(evt) {
        evt.preventDefault();
        var item = catTree.attr("selectedItem");
        yerl = "<c:url value="/config/groups/${group.id}/categoryOptions/new"/>?dialog=true";
        if (item != null && item.id != 'root') {
            var itemType = catCatOptStore.getValue(item, "type");
            var itemId = catCatOptStore.getValue(item, "id");
            if (itemType == 'category') {
                // itemId is the categoryId
                yerl += "&catId=" + itemId;
            }
            catSelectedValue = itemId;
        }
        categoryOptionDlg.set("title", "<spring:message code="ticketSettings.addNewCategoryOption"/>");
        categoryOptionDlg.set("href", yerl);
        categoryOptionDlg.show();
    });

    // Edit selected item
    dojo.connect(dojo.byId("editSlctdBtn"), "onclick", function(evt) {
        evt.preventDefault();
        var item = catTree.attr("selectedItem");
        if (item != null && item != catTreeModel.root) {
            console.debug("selectedNode name: " + catCatOptStore.getValue(item, "name"));
            console.debug("selectedNode type: " + catCatOptStore.getValue(item, "type"));
            console.debug("selectedNode id: " + catCatOptStore.getValue(item, "id"));

            var itemType = catCatOptStore.getValue(item, "type");
            var itemId = catCatOptStore.getValue(item, "id");
            if (itemType == 'category') {
                categoryDlg.set("title", "<spring:message code="category.edit"/>");
                categoryDlg.set("href", "<c:url value="/config/groups/${group.id}/categories/"/>" + itemId + "/edit?dialog=true");
                categoryDlg.show();
            } else if (itemType == 'categoryOption') {
                // strip the 'copt' from the itemId
                var coptId = itemId.replace("copt", "");
                catSelectedValue = catCatOptStore.getValue(item, "catId");
                isEdit = true;
                categoryOptionDlg.set("title", "<spring:message code="categoryOption.edit"/>");
                categoryOptionDlg.set("href", "<c:url value="/config/groups/${group.id}/categoryOptions/"/>" + coptId + "/edit?dialog=true");
                categoryOptionDlg.show();
            }
        } else {
            alert("<spring:message code="category.edit.selectFirst"/>");
        }
    });

    // Delete selected item
    dojo.connect(dojo.byId("deleteSlctdBtn"), "onclick", function(evt) {
        evt.preventDefault();
        var item = catTree.attr("selectedItem");
        if (item != null && item != catTreeModel.root) {
            if (confirm('<spring:message code="global.confirmdelete" javaScriptEscape="true"/>')) {
                console.debug("deleting node - name: " + catCatOptStore.getValue(item, "name"));
                console.debug("deleting node - type: " + catCatOptStore.getValue(item, "type"));
                console.debug("deleting node - id: " + catCatOptStore.getValue(item, "id"));

                var itemType = catCatOptStore.getValue(item, "type");
                var itemId = catCatOptStore.getValue(item, "id");
                if (itemType == 'category') {
                    dojo.xhrDelete({
                        url: "<c:url value="/config/groups/${group.id}/category/"/>" + itemId,
                        handle: function(response, ioargs) {
                            if (ioargs.xhr.status == 417) {
                                dojo.place('<ul class="message warning grid_12">\
                                     <li><spring:message code="category.inuse" javaScriptEscape="true"/></li>\
                                  </ul>', "flashMessage", "only");
                            } else {
                                reloadTree(catTree);
                            }
                        }
                    });
                } else if (itemType == 'categoryOption') {
                    // strip the 'copt' from the itemId
                    var coptId = itemId.replace("copt", "");
                    dojo.xhrDelete({
                        url: "<c:url value="/config/groups/${group.id}/categoryOptions/"/>" + coptId,
                        handle: function(response, ioargs) {
                            if (ioargs.xhr.status == 417) {
                                dojo.place('<ul class="message warning grid_12">\
                                     <li><spring:message code="categoryOption.inuse" javaScriptEscape="true"/></li>\
                                  </ul>', "flashMessage", "only");
                            } else {
                                reloadTree(catTree);
                            }
                        }
                    });
                }
            }
        } else {
            alert("<spring:message code="category.edit.deleteSelectFirst" javaScriptEscape="true"/>");
        }
    });

    // Destroys all nodes of a tree and loads it again based on it's model
    function reloadTree(theTree) {
        location.reload();
        // The following shows the changes to the tree nodes after an edit was made but didn't allow selection of nodes
        // afterwards so just reload the page to fix this and think long and hard about finding another fix.


        // Credit to this discussion: http://mail.dojotoolkit.org/pipermail/dojo-interest/2010-April/045180.html
        // Close the store (So that the store will do a new fetch()).
//        theTree.model.store.clearOnClose = true;
//        theTree.model.store.close();
//
//        // Completely delete every node from the dijit.Tree
//        theTree._itemNodesMap = {};
//        theTree.rootNode.state = "UNCHECKED";
//        theTree.model.root.children = null;
//
//        // Destroy the widget
//        theTree.rootNode.destroyRecursive();
//
//        // Recreate the model, (with the model again)
//        theTree.model.constructor(theTree.model);
//
//        // Rebuild the tree
//        theTree.postMixInProperties();
//        theTree._load();
    }

    var categoryByGroupStore = new dojo.data.ItemFileReadStore({
        url: "<c:url value="/stores/categories.json"><c:param name="prevId" value="${group.id}"/></c:url>",
        hierarchical: false
    });


    // Group Roles
    dojo.connect(dojo.byId("addRoleBtn"), "onclick", function(evt) {
        evt.preventDefault();
        groupRoleDlg.show();
    });

    var groupRoleDlg = new dijit.Dialog({
        title: "<spring:message code="group.addNewRole"/>",
        href: "<c:url value="/config/groups/${group.id}/groupRoles/new"><c:param name="dialog" value="true"/></c:url>",
        onDownloadEnd: dlgGroupRoleDownloadEnd
    });

    function dlgGroupRoleDownloadEnd() {
        var form = dojo.byId("userRoleGroup");
        dojo.connect(form, "onsubmit", function(evt) {
            dojo.stopEvent(evt);
            dojo.xhrPost({
                form: form,
                load: function(response) {
                    // check for validation errors
                    var tempDiv = document.createElement("div");
                    tempDiv.innerHTML = response;
                    var errors = dojo.query("[id=bindErrors]", tempDiv);
                    if (errors.length) {
                        groupRoleDlg.set("content", response);
                        dlgGroupRoleDownloadEnd();
                    } else {
                        groupRoleDlg.hide();
                        location.reload();
                    }
                }
            });
        });

        new dijit.form.FilteringSelect({
                    id: "groupRoleUserSelectBox",
                    name: "userRole.user",
                    store: userNoRoleForGroupStore,
                    searchAttr: "name"
                },
                "groupRoleUserSelectBox");

        new dijit.form.FilteringSelect({
            id: "groupRoleRoleSelectBox",
            name: "userRole.role",
            style: "width:15em;",
            store: groupRoleStore
        }, "groupRoleRoleSelectBox");

        dojo.connect(dojo.byId("groupRoleCancelBtn"), "onclick", function() {
            groupRoleDlg.hide();
        });

        $("#userRoleGroup").applyTemplateSetup();
    }

    var userNoRoleForGroupStore = new dojo.data.ItemFileReadStore({
        url: "<c:url value="/config/groups/${group.id}/groupRoles/userNoRolesJson"/>",
        urlPreventCache: true
    });

    var groupRoleStore = new dojo.data.ItemFileReadStore({
        url: "<c:url value="/config/groups/${group.id}/groupRoles/roles"/>",
        urlPreventCache: true
    });

    //handle button clicks for deleting a group role
    dojo.query('a[id^="del_urg_"]').connect("onclick", function(evt) {
        evt.preventDefault();
        if (confirm('<spring:message code="global.confirmdelete" javaScriptEscape="true"/>')) {
            var urgId = this.id.replace("del_urg_", "");
            var elementToRemove = this.parentNode.parentNode;
            console.debug("deleting urgId: " + urgId);
            dojo.xhrDelete({
                url: "<c:url value="/config/groups/${group.id}/groupRoles/"/>" + urgId,
                handle: function(response, ioargs) {
                    if (ioargs.xhr.status == 417) {
                        dojo.place('<ul class="message warning grid_12">\
                                     <li><spring:message code="group.role.inuse" javaScriptEscape="true"/></li>\
                                  </ul>', "flashMessage", "only");
                    } else {
                        dojo.fadeOut({
                            node: elementToRemove,
                            onEnd: function() {
                                dojo.destroy(elementToRemove);
                            }
                        }).play();
                        location.reload();
                    }
                }
            });
        }
    });

    //handle button clicks for modifying notification assistant for the particular UserRoleGroup
    dojo.query('a[id^="noteAss_"]').connect("onclick", function(evt) {
        evt.preventDefault();
        var urgId = this.id.replace("noteAss_", "");
        noteAssDlg.set("href", "<c:url value="/config/groups/${group.id}/groupRoles/"/>" + urgId + "/notificationAsst/edit?dialog=true");
        noteAssDlg.show();
    });

    var noteAssDlg = new dijit.Dialog({
        title: "<spring:message code="notAssist.title"/>",
        style: "width:400px",
        onDownloadEnd: dlgNotAssistDownloadEnd
    });

    function dlgNotAssistDownloadEnd() {
        $(".chosen").chosen();

        var form = dojo.byId("notificationAssistantForm");
        dojo.connect(form, "onsubmit", function(evt) {
            dojo.stopEvent(evt);
            dojo.xhrPost({
                form: form,
                load: function(response) {
                    // check for validation errors
                    var tempDiv = document.createElement("div");
                    tempDiv.innerHTML = response;
                    var errors = dojo.query("[id=bindErrors]", tempDiv);
                    if (errors.length) {
                        noteAssDlg.set("content", response);
                        dlgNotAssistDownloadEnd();
                    } else {
                        noteAssDlg.hide();
                    }
                }
            });
        });

        dojo.connect(dojo.byId("notAssCancelBtn"), "onclick", function() {
            noteAssDlg.hide();
        });
    }

    </c:if>


});
</script>

</head>
<body>

<content tag="breadCrumb">
    <ul id="breadcrumb">
        <li><a href="<c:url value="/config/groups"/>"><spring:message code="group.title"/></a></li>
        <li><a href="javascript:location.reload();"><c:out value="${formTitle}"/></a></li>
    </ul>
</content>
<section class="grid_10">
    <div class="block-content">
        <h1><c:out value="${formTitle}"/></h1>
        <form:form modelAttribute="group" cssClass="form" method="${method}" action="${action}">
            <spring:hasBindErrors name="group">
                <p id="bindErrors" class="message error no-margin"><form:errors path="*"/></p>
            </spring:hasBindErrors>
            <p class="required">
                <c:set var="nameError"><form:errors path="name"/></c:set>
                <form:label path="name"><spring:message code="group.name"/></form:label>
                <form:input path="name" size="50" maxlength="255">
                            <jsp:attribute name="cssClass">
                                <form:errors path="name">
                                    error
                                </form:errors>
                            </jsp:attribute>
                </form:input>
            </p>

            <p>
                <form:label path="comments"><spring:message code="group.comments"/></form:label>
                <form:input path="comments" size="50" maxlength="255"/>
            </p>

            <p>
                <form:label path="active" cssClass="float-left" cssStyle="padding-right: 15px;">
                    <spring:message code="global.active"/>
                </form:label>
                <form:checkbox path="active" cssClass="mini-switch with-tip">
                    <jsp:attribute name="title"><spring:message code="global.active.toggle"/></jsp:attribute>
                </form:checkbox>
            </p>

            <%--<p>--%>
                <%--<form:label path="assetTrackerType">--%>
                    <%--<spring:message code="assetTracker.groupManageAssets" arguments="${group.name}"/>--%>
                <%--</form:label>--%>
                <%--<form:radiobutton path="assetTrackerType" value="EHD"/>--%>
                <%--<spring:message code="assetTracker.ehdInternalAssetTracker"/>--%>
                <%--<form:radiobutton path="assetTrackerType" value="ZEN10"/>--%>
                <%--<spring:message code="assetTracker.novellZen10AssetTracker"/>--%>
                <%--<form:radiobutton path="assetTrackerType" value="NONE"/>--%>
                <%--<spring:message code="assetTracker.noneAssetTracker"/>--%>
            <%--</p>--%>

            <p>
                <form:label path="permissionScheme"><spring:message code="group.permissionModel"/></form:label>
                <form:select path="permissionScheme" dojoType="dijit.form.Select">
                    <form:options items="${permissionSchemeList}" itemValue="id" itemLabel="name"/>
                </form:select>
            </p>

            <p class="grey-bg no-margin">
                <button type="submit" id="groupSave" name="save"><spring:message code="global.save"/></button>
                <button type="button" id="groupCancel" name="cancel">
                    <spring:message code="global.cancel"/>
                </button>
            </p>
        </form:form>
    </div>
</section>

<c:if test="${group.id ne null}">
    <section class="grid_5">
        <div class="block-content">
            <h1><spring:message code="group.catsetup.title"/></h1>

            <div class="block-controls">
                <ul class="controls-buttons">
                    <li>
                        <a id="addCategoryBtn" href="javascript://"
                           title="<spring:message code="ticketSettings.addCategory"/>">
                            <img src="<c:url value="/images/ticketSettings/category.gif"/>" alt="">
                        </a>
                    </li>
                    <li>
                        <a id="addCatOptBtn" href="javascript://"
                           title="<spring:message code="ticketSettings.addNewCategoryOption"/>">
                            <img src="<c:url value="/images/ticketSettings/categoryOption.gif"/>" alt="">
                        </a>
                    </li>
                    <li>
                        <a id="editSlctdBtn" href="javascript://"
                           title="<spring:message code="global.editSelected"/>">
                            <img src="<c:url value="/images/theme/icons/fugue/pencil.png"/>" alt=""/>
                        </a>
                    </li>
                    <li>
                        <a id="deleteSlctdBtn" href="javascript://"
                           title="<spring:message code="global.deleteSelected"/>">
                            <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                        </a>
                    </li>
                </ul>
            </div>
            <div id="categoryTree"></div>
        </div>
    </section>
    <section class="grid_5">
        <div class="block-content no-padding">
            <h1><spring:message code="group.roles"/></h1>

            <div class="block-controls">
                <ul class="controls-buttons">
                    <li>
                        <a id="addRoleBtn" href="javascript://">
                            <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="">
                            <spring:message code="global.createNew"/>
                        </a>
                    </li>
                </ul>
            </div>
            <table class="table full-width">
                <thead>
                <tr>
                    <th scope="col"><spring:message code="user.user"/></th>
                    <th scope="col"><spring:message code="user.role"/></th>
                    <th scope="col" class="table-actions"><spring:message code="global.actions"/></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <spring:message code="ticketList.ticketPool"/>
                    </td>
                    <td>
                    </td>
                    <td class="table-actions">
                        <a id="noteAss_${urgTicketPool.id}" title="<spring:message code="notAssist.title"/>"
                           href="javascript://">
                            <img alt="" src="<c:url value="/images/wireless.png"/>" style="padding-left: 20px;"/>
                        </a>
                    </td>
                </tr>
                <c:forEach items="${urgList}" var="urg" varStatus="index">
                    <tr>
                        <td>
                            <c:out value="${urg.userRole.user.lastName}"/>,
                            <c:out value="${urg.userRole.user.firstName}"/>
                            (<c:out value="${urg.userRole.user.loginId}"/>)
                        </td>
                        <td>
                            <spring:message code="${urg.userRole.role.name}"/>
                        </td>
                        <td class="table-actions">
                            <a id="del_urg_${urg.id}" title="<spring:message code="global.delete"/>"
                               href="javascript://">
                                <img alt="remove"
                                     src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>"/>
                            </a>
                            <a id="noteAss_${urg.id}" title="<spring:message code="notAssist.title"/>"
                               href="javascript://">
                                <img alt="" src="<c:url value="/images/wireless.png"/>"/>
                            </a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </section>
</c:if>
</body>
</html>
