<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>
    <meta name="configSidebar" content="true">
    <title><spring:message code="acl.globalPermissionsTitle"/></title>

</head>

<body>

<section class="grid_10">
    <div class="block-content">
        <h1><spring:message code="acl.globalPermissionsTitle"/></h1>
        <table class="table">
            <thead>
            <tr>
                <th scope="col"><spring:message code="acl.permission"/></th>
                <th scope="col"><spring:message code="acl.users"/></th>
                <th scope="col"><spring:message code="acl.roles"/></th>
                <th scope="col" class="table-actions"><spring:message code="acl.actions"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="gpHolder" items="${globalPermissions}">
                <tr>
                    <td><spring:message code="${gpHolder.permission.key}.name" text="Translation missing for ${gpHolder.permission.key}"/></td>
                    <td>
                        <ul class="keywords">
                            <c:forEach var="user" items="${gpHolder.globalPermissionEntry.permissionActors.users}" varStatus="varStatus">
                                <li><c:out value="${user.loginId}"/></li>
                            </c:forEach>
                        </ul>
                    </td>
                    <td>
                        <ul class="keywords">
                            <c:forEach var="role" items="${gpHolder.globalPermissionEntry.permissionActors.roles}" varStatus="varStatus">
                                <li><spring:message code="${role.name}"/></li>
                            </c:forEach>
                        </ul>
                    </td>
                    <td>
                        <spring:url value="/config/acl/globalPermissions/{id}/edit" var="editUrl">
                            <spring:param name="id" value="${gpHolder.permission.id}"/>
                        </spring:url>

                        <a href="${fn:escapeXml(editUrl)}"
                           title="<spring:message code="global.edit"/>">
                            <img src="<c:url value="/images/theme/icons/fugue/pencil.png"/>" alt="">
                        </a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</section>
</body>
