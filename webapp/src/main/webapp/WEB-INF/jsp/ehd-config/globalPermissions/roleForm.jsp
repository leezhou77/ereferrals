<%@ include file="/WEB-INF/jsp/include.jsp" %>
<body>

<form:form modelAttribute="permissionActorsForm" cssClass="form" method="post">
    <jsp:attribute name="action"><c:url value="/config/acl/globalPermissions/${permission.id}/roles"><c:param name="dialog" value="true"/></c:url></jsp:attribute>
    <jsp:body>
        <div id="messages" class="no-margin">
            <spring:hasBindErrors name="permissionActorsForm">
                <p id="rolePermErrors" class="message error no-margin">
                    <form:errors path="*"/>
                </p>
            </spring:hasBindErrors>
        </div>
        <br>


        <fieldset>
            <legend><spring:message code="acl.addRolePermissions"/></legend>
            <p>
                <form:label path="users"><spring:message code ="acl.roles"/></form:label>
                <form:select path="roles" multiple="multiple" cssClass="chosen full-width" id="role-select">
                    <c:forEach var="role" items="${roles}">
                        <form:option value="${role.id}"><spring:message code ="${role.name}"/></form:option>
                    </c:forEach>
                </form:select>
            </p>
        </fieldset>


        <p id="buttons">
            <button type="submit" name="save"><spring:message code="global.save"/></button>
            <button class="cancelBtn" type="button" name="cancel"><spring:message code="global.cancel"/></button>
        </p>
    </jsp:body>
</form:form>
</body>
