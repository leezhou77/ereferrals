<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>
    <meta name="configSidebar" content="true">
    <title><spring:message code="acl.permissionModel"/></title>

    <link rel="stylesheet" type="text/css" href="<c:url value="/css/permissions.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/js/chosen/chosen.css"/>" type="text/css">

    <script type="text/javascript" src="<c:url value="/js/chosen/chosen.jquery.js"/>"></script>

    <script type="text/javascript">
        dojo.addOnLoad(function() {
            var permStore = new dojo.data.ItemFileReadStore({
                url: "<c:url value="/config/acl/permissionSchemes/${permissionScheme.id}/entries.json"/>",
                urlPreventCache: true
            });

            var permTreeModel = new dijit.tree.ForestStoreModel({
                store: permStore,
                query: { type: "*PermissionSchemeEntry" },
                rootId: "root",
                rootLabel: "Group Permissions",
                childrenAttrs: ["children"]
            });

            var permTree = new dijit.Tree({
                model: permTreeModel,
                getIconClass: getIcons,
                onClick: handleNodeClick
            }, "permTree");

            function getIcons(item, opened) {
                if (item == permTreeModel.root || item.type == "userContainer" || item.type == "roleContainer") {
                    return opened ? "dijitFolderOpened" : "dijitFolderClosed";
                } else {
                    return permStore.getValue(item, "type") + "Icon";
                }
            }

            function createTreeNode(args) {
                var tnode = new dijit._TreeNode(args);

                if(args.item != permTreeModel.root && args.item.type == "userContainer" || args.item.type == "roleContainer") {
                    div.append(link);
                    tnode.labelNode.innerHTML = args.label + div.html();
                }
                return tnode;

            }
            function handleNodeClick(item, node, evt) {
                if (item != permTreeModel.root && (item.type == "userContainer" || item.type == "roleContainer")) {
                    var itemType = "";
                    if(item.type == "userContainer") {
                        itemType = "user";
                    } else {
                        itemType = "role";
                    }

                    var href = "<c:url value="/config/acl/permissionSchemes/${permissionScheme.id}/permissions/"/>" + item.permId + "/" + itemType + "s/edit?dialog=true";
                    addActorDlg.set("href", href);
                    addActorDlg.show();
                }
            }

            var addActorDlg = new dijit.Dialog({
                style: "overflow:visible",
                onDownloadEnd: actorDlgDownloadEnd
            });

            function actorDlgDownloadEnd() {
                $(addActorDlg.domNode).find(".chosen").chosen();
                dojo.query("#permissionActorsForm").connect("submit", function(e) {
                    dojo.stopEvent(e);
                    var form = dojo.byId("permissionActorsForm");
                    dojo.xhrPost({
                        form: form,
                        load: function(response) {
                            //create a tmp div so we can use dojo.query on the response
                            //to check for errors and either hide the form or leave it up
                            var tempDiv = document.createElement("div");
                            tempDiv.innerHTML = response;
                            var errors = dojo.query("[id=userPermErrors]", tempDiv);
                            if (errors.length) {
                                addActorDlg.set("content", response);
                                actorDlgDownloadEnd();
                            } else {
                                addActorDlg.hide();
                                location.reload(true);
                            }
                        }
                    });
                });
            }

            $(".cancelBtn").live("click", function() {
                addActorDlg.hide();
            });
        });
    </script>
</head>

<body>

<content tag="breadCrumb">
    <ul id="breadcrumb">
        <li><a href="<c:url value="/config/acl/permissionSchemes"/>"><spring:message code="acl.permissionModels"/></a></li>
        <li><a href="javascript:location.reload();"><spring:message code="acl.editModel"/></a></li>
    </ul>
</content>

<section class="grid_8">
    <div class="block-content">
        <h1><spring:message code="acl.modelEdit"/></h1>


        <form:form modelAttribute="permissionScheme" method="put" cssClass="form">
            <jsp:attribute name="action"><c:url value="/config/acl/permissionSchemes/${permissionScheme.id}"/></jsp:attribute>
            <jsp:body>
                <form:label path="name"><spring:message code="global.name"/></form:label>
                <form:input path="name" size="60">
                    <jsp:attribute name="cssClass">
                        <form:errors path="name">error</form:errors>
                    </jsp:attribute>
                </form:input>
                &nbsp;
                <button type="submit" class="small"><spring:message code="acl.permissionModel.changeName"/></button>
            </jsp:body>
        </form:form>
        <br><br>
        <div id="permTree"></div>
    </div>
</section>
</body>