<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>
    <meta name="configSidebar" content="true">
    <title><spring:message code="userManagement.title"/></title>

    <link rel="stylesheet" href="<c:url value="/js/dojo-1.6.2/dojox/grid/resources/Grid.css"/>" type="text/css">
    <link rel="stylesheet" href="<c:url value="/js/dojo-1.6.2/dojox/grid/resources/tundraGrid.css"/>" type="text/css">

    <script type="text/javascript" src="<c:url value="/js/jquery.watermark.min.js"/>"></script>

    <script type="text/javascript">

        dojo.addOnLoad(function() {
            var userStore = new dojox.data.QueryReadStore({
                url: "<c:url value="/stores/users.json"><c:param name="incInactive" value="true"/></c:url>"
            });

            function formatActions(value, rowIndex, cell) {
                var userId = value.i.userId;
                return '<a title="<spring:message code="global.edit"/>" class="editLink" id="edit_user_' + userId + '" href="javascript://"> <img style="width:12px;height:12px;padding-right:5px;" src="<c:url value="/images/theme/icons/fugue/pencil.png"/>"/></a>' +
                        '<a title="<spring:message code="global.delete"/>" class="deleteLink" id="del_user_' + userId + '" href="javascript://"><img style="width:12px;height:12px;" src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>"></a>';
            }

            var grid = new dojox.grid.DataGrid({
                store: userStore,
                structure: [
                    {name:"<spring:message code="user.lastName"/>", field: "lastName", width: 'auto'},
                    {name:"<spring:message code="user.firstName"/>", field: "firstName", width: 'auto'},
                    {name:"<spring:message code="user.email"/>", field: "email", width: 'auto'},
                    {name:"<spring:message code="user.userName"/>", field: "loginId", width: 'auto'},
                    {name:"<spring:message code="user.status"/>", field: "active", width: 'auto'},
                    {name:"<spring:message code="user.roles"/>", field: "roles", width: 'auto'},
                    {name:"<spring:message code="user.groups"/>", field: "groups", width: 'auto'},
                    {name:"<spring:message code="global.actions"/>", field: "_item", width: "50px", formatter: formatActions}
                ],
                editable: false,
                autoHeight: 20,
                noDataMessage: "<spring:message code="userManagement.noUsers" javaScriptEscape="true"/>"
            }, "usersGrid");

            //disallow sorting on roles or groups
            grid.canSort = function(col) {
                return !(Math.abs(col) == 6 || Math.abs(col) == 7 || Math.abs(col) == 8);
            };

            <%--dojo.connect(grid, "onRowDblClick", function(e) {--%>
            <%--var userId = e.grid.getItem(e.rowIndex).i.userId;--%>
            <%--openWindow('<c:url value="/user/userEdit.glml"/>' + "?uid=" + userId, 'userEdit', 800, 650);--%>
            <%--});--%>

            grid.startup();

            dojo.connect(dojo.byId("searchUser"), "onkeyup", function(e) {
                var searchQuery = this.value;
                delay(function() {
                    grid.setQuery({label: searchQuery});
                }, 500);
            });

            $(".editLink").live("click", function(e) {
                e.preventDefault();
                var userId = $(this).attr("id").replace("edit_user_", "");
                location.href = '<c:url value="/config/users/"/>' + userId + '/edit';
                <%--openWindow('<c:url value="/user/userEdit.glml"/>' + "?uid=" + userId, 'userEdit', 800, 650);--%>
            });

            $(".deleteLink").live("click", function(e) {
                e.preventDefault();
                if (confirm('<spring:message code="global.confirmdelete" javaScriptEscape="true"/>')) {
                    var userId = $(this).attr("id").replace("del_user_", "");
                    var elementToRemove = this.parentNode.parentNode;
                    dojo.xhrDelete({
                        url: "<c:url value="/config/users/"/>" + userId,
                        handle: function(response, ioargs) {
                            if (ioargs.xhr.status == 417) {
                                dojo.place('<ul class="message warning grid_12">\
                                     <li><spring:message code="user.inuse" javaScriptEscape="true"/></li>\
                                  </ul>', "flashMessage", "only");
                            } else {
                                dojo.fadeOut({
                                    node: elementToRemove,
                                    onEnd: function() {
                                        dojo.destroy(elementToRemove);
                                    }
                                }).play();
                            }
                        }
                    });
                }
            });


        });


        $(function() {
            $("#searchUser").watermark("<spring:message code="global.search"/>");
        });
    </script>

</head>
<body>
<section class="grid_10">
    <div class="block-content">
        <h1><spring:message code="userManagement.title"/></h1>

        <div class="block-controls">
            <ul class="controls-buttons">
                <li class="form">
                    <label for="searchUser"></label><input type="text" id="searchUser" name="searchUser">
                </li>
                <li class="sep"></li>
                <li>
                    <a href="<c:url value="/config/users/new"/>">
                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="">
                        <spring:message code="userManagement.createnew"/>
                    </a>
                </li>
                <c:if test="${applicationScope.ldapIntegration}">
                    <li>
                        <a href="<c:url value="/config/users/import" />">
                        <img src="<c:url value="/images/theme/icons/fugue/arrow-270.png"/>" alt="">
                            <spring:message code="global.importLdapUser"/>
                        </a>
                    </li>
                </c:if>
            </ul>
        </div>
        <div class="no-margin">
            <div id="usersGrid"></div>
        </div>
    </div>
</section>
</body>
