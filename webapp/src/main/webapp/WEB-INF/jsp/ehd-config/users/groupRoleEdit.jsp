<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="availableGroups" type="java.util.List"--%>
<%--@elvariable id="roles" type="java.util.List"--%>

<c:set var="action">
    <c:url value="/config/users/${user.id}/groupRoles/${urgSetter.userRoleGroup.id}"><c:param name="dialog" value="true"/></c:url>
</c:set>

<form:form modelAttribute="urgSetter" cssClass="form" method="put" action="${action}">
    <div id="messages" class="no-margin">
        <spring:hasBindErrors name="urgSetter">
            <p id="bindErrors" class="message error no-margin">
                <form:errors path="*"/>
            </p>
        </spring:hasBindErrors>
    </div>
    <br/>
    <fieldset>
        <p>
            <form:label path="group"><spring:message code="global.group"/></form:label>
            <c:out value="${urgSetter.group.name}"/>
        </p>

        <p>
            <form:label path="role"><spring:message code="user.role"/></form:label>
            <form:select path="role" dojoType="dijit.form.Select" cssStyle="width:15em;">
                <c:forEach items="${roles}" var="role">
                    <form:option value="${role.id}" >
                        <jsp:attribute name="label"><spring:message code="${role.name}"/></jsp:attribute>
                    </form:option>
                </c:forEach>
            </form:select>
        </p>


    </fieldset>
    <p>
        <button type="submit" id="urgSaveBtn"><spring:message code="global.save"/></button>
        <button type="button" id="urgCancelBtn"><spring:message code="global.cancel"/></button>
    </p>
</form:form>
