<%@ include file="/WEB-INF/jsp/include.jsp" %>

<c:choose>
    <c:when test="${empty address.id}">
        <c:set var="method" value="post"/>
        <c:set var="action">
            <c:url value="/config/users/${user.id}/addresses"><c:param name="dialog" value="true"/></c:url>
        </c:set>
    </c:when>
    <c:otherwise>
        <c:set var="method" value="put"/>
        <c:set var="action">
            <c:url value="/config/users/${user.id}/addresses/${address.id}">
                <c:param name="dialog" value="true"/>
            </c:url>
        </c:set>
    </c:otherwise>
</c:choose>

<form:form modelAttribute="address" cssClass="form" method="${method}" action="${action}">
    <div id="messages" class="no-margin">
        <spring:hasBindErrors name="address">
            <p id="bindErrors" class="message error no-margin">
                <form:errors path="*"/>
            </p>
        </spring:hasBindErrors>
    </div>
    <br/>
    <fieldset>
        <p>
            <form:label path="addressType"><spring:message code="address.type"/></form:label>
            <form:select path="addressType" dojoType="dijit.form.Select" cssStyle="width:15em;">
                <form:option value="Office">
                    <jsp:attribute name="label"> <spring:message code="phone.office"/> </jsp:attribute>
                </form:option>
                <form:option value="Home">
                    <jsp:attribute name="label"> <spring:message code="phone.home"/> </jsp:attribute>
                </form:option>
            </form:select>
        </p>

        <p>
            <form:label path="country"><spring:message code="address.country"/></form:label>
            <ehd:countryCombo name="country" selectedValue="${address.country}"/>
        </p>

        <p>
            <form:label path="line1"><spring:message code="address.street"/></form:label>
            <form:input path="line1" dojoType="dijit.form.TextBox" size="50" maxlength="110"/>
        </p>

        <p>
            <form:label path="city"><spring:message code="address.city"/></form:label>
            <form:input path="city" dojoType="dijit.form.TextBox" size="50" maxlength="50"/>
        </p>

        <p>
            <form:label path="state"><spring:message code="address.state"/></form:label>
            <form:input path="state" dojoType="dijit.form.TextBox" size="25" maxlength="40"/>
        </p>

        <p>
            <form:label path="postalCode"><spring:message code="address.postalCode"/></form:label>
            <form:input path="postalCode" dojoType="dijit.form.TextBox" size="25" maxlength="40"/>
        </p>

    </fieldset>
    <p>
        <button type="submit" id="addressSaveBtn"><spring:message code="global.save"/></button>
        <button type="button" id="addressCancelBtn"><spring:message code="global.cancel"/></button>
    </p>
</form:form>
