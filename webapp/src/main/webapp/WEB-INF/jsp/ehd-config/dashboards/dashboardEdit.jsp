<%@ include file="/WEB-INF/jsp/include.jsp" %>

<head>
    <title><spring:message code ="dashboard.title"/></title>

    <script type="text/javascript">

        function cancelForm() {
            location.href = '<c:url value="/config/dashboardList.glml"/>';
        }

        function viewWidget(widgetId) {
            var url = '<c:url value="/config/widgetEdit.glml"/>?dashId=${dashboard.id}';
            if (widgetId != null) url += '&widgetId=' + widgetId;
            window.location.href = url;
        }

        function deleteWidget(widgetId) {
            if (confirm('<spring:message code="global.confirmdelete"/>')) {
                window.location.href = '<c:url value="/config/widgetDelete.glml"/>?widgetId=' + widgetId + '&dashId=${dashboard.id}';
            }
        }
    </script>

</head>

<body>

<content tag="breadCrumb">
    <ul id="breadcrumb">
        <li><a href="<c:url value="/config/dashboardList.glml"/>"><spring:message code="dashboard.title"/></a></li>
        <li><a href="javascript:location.reload();"><spring:message code="dashboard.edit.title"/></a></li>
    </ul>
</content>

<section class="grid_10">
    <div class="block-content">
        <h1><spring:message code="dashboard.edit.title"/></h1>

        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a href="javascript:viewWidget(null);">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>">
                        <spring:message code="dashboard.widget.new"/>
                    </a>
                </li>
            </ul>
        </div>

        <form:form modelAttribute="dashboard" action="" method="post" cssClass="form">

            <spring:hasBindErrors name="dashboard">
                <p class="message error no-margin">
                    <form:errors path="*"/>
                </p>
            </spring:hasBindErrors>

            <c:if test="${not empty dashboard.id}">
                <input type="hidden" name="dashId" value="${dashboard.id}"/>
            </c:if>

            <form:label path="name" cssClass="required"><spring:message code="dashboard.name"/></form:label>
            <form:input path="name" size="60" maxlength="255">
                <jsp:attribute name="cssClass">
                        <form:errors path="name">error</form:errors>
                    </jsp:attribute>
            </form:input>

            <br/><br/>

            <%--Only show the widget table if the dashboard has been saved--%>
            <c:choose>
                <c:when test="${not empty dashboard.id and not empty dashboard.sortedWidgets}">
                    <div id="widgetsDiv">

                        <table class="table">
                            <thead>
                            <tr class="title">
                                <th scope="col"><spring:message code="dashboard.widget.name"/></th>
                                <th scope="col"><spring:message code="dashboard.widget.row"/></th>
                                <th scope="col"><spring:message code="dashboard.widget.col"/></th>
                                <th scope="col"><spring:message code="dashboard.widget.colspan"/></th>
                                <th scope="col"><spring:message code="dashboard.widget.rowspan"/></th>
                                <th scope="col" class="table-actions"><spring:message code="dashboard.widget.action"/></th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach var="widget" items="${dashboard.sortedWidgets}">
                                <tr class="toberuled">
                                    <td ondblclick="viewWidget(${widget.id});"><c:out value="${widget.name}"/></td>
                                    <td ondblclick="viewWidget(${widget.id});"><c:out value="${widget.row}"/></td>
                                    <td ondblclick="viewWidget(${widget.id});"><c:out value="${widget.column}"/></td>
                                    <td ondblclick="viewWidget(${widget.id});"><c:out value="${widget.colspan}"/></td>
                                    <td ondblclick="viewWidget(${widget.id});"><c:out value="${widget.rowspan}"/></td>
                                    <td align="right" nowrap="nowrap">
                                        <a href="javascript://" onclick="viewWidget(${widget.id}); return false;">
                                            <img alt="" src="<c:url value="/images/edit.gif" />"/>
                                        </a>
                                        <a href="javascript://" onclick="deleteWidget(${widget.id}); return false;">
                                            <img alt="" src="<c:url value="/images/trash.gif" />"/>
                                        </a>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </c:when>
                <c:otherwise>
                    <p><spring:message code="dashboard.widget.noneFound"/></p>
                </c:otherwise>
            </c:choose>
            <br/><br/>
            <c:if test="${empty dashboard.id}">
                <input type="hidden" name="nameOnly" value="true"/>
            </c:if>
            <p class="grey-bg no-margin" id="commandButtons">
                <button type="submit"><spring:message code="global.save"/></button>
                <button type="button" onclick="cancelForm();"><spring:message code="global.cancel"/></button>
            </p>
        </form:form>
    </div>
</section>
</body>
