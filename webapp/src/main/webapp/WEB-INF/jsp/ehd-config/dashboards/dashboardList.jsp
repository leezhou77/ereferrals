<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="dashboards" type="java.util.List"--%>
<%--@elvariable id="showHeader" type="java.lang.Boolean"--%>
<head>
    <title>
        <spring:message code="dashboard.title"/>
    </title>

    <script type="text/javascript">

        function launchDashboard(dashId) {
            var url = '<c:url value="/jnlp/dashboard.jnlp"/>';
            url += '?dashboardId=' + dashId;
            window.location.href = url;
        }

        function viewDashboard(dashId) {
            var url = '<c:url value="/config/dashboardEdit.glml"/>';
            if (dashId != null) url += '?dashId=' + dashId;
            window.location.href = url;
        }

        function copyDashboard(dashId) {
            if (confirm('<spring:message code="global.confirmcopy"/>')) {
                window.location.href = '<c:url value="/config/dashboardCopy.glml"/>?dashId=' + dashId;
            }
        }

        function deleteDashboard(dashId) {
            if (confirm('<spring:message code="global.confirmdelete"/>')) {
                window.location.href = '<c:url value="/config/dashboardDelete.glml"/>?dashId=' + dashId;
            }
        }

    </script>
</head>

<body>
<section class="grid_8">
    <div class="block-content no-padding">
        <h1><spring:message code="dashboard.title"/></h1>
        <ehd:authorizeGlobal permission="global.dashboardManagement">
            <div class="block-controls">
                <ul class="controls-buttons">
                    <li>
                        <a href="javascript://" onclick="viewDashboard(null); return false;">
                            <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>">
                            <spring:message code="dashboard.new"/>
                        </a>
                    </li>
                </ul>
            </div>
        </ehd:authorizeGlobal>

        <c:choose>
            <c:when test="${not empty dashboards}">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col"><spring:message code="dashboard.name"/></th>
                        <th scope="col" class="table-actions"><spring:message code="dashboard.action"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="dash" items="${dashboards}">
                        <tr>
                            <td><c:out value="${dash.name}"/></td>
                            <td class="table-actions">
                                <a href="javascript://" title="<spring:message code="dashboard.launch"/>" onclick="launchDashboard(${dash.id}); return false;">
                                    <img alt="" src="<c:url value="/images/theme/icons/fugue/arrow-curve-000-left.png"/>"/>
                                </a>
                                <ehd:authorizeGlobal permission="global.dashboardManagement">
                                    <a href="javascript://" title="<spring:message code="dashboard.edit"/>" onclick="viewDashboard(${dash.id}); return false;">
                                        <img alt="" src="<c:url value="/images/theme/icons/fugue/pencil.png"/>"/>
                                    </a>
                                    <a href="javascript://" title="<spring:message code="dashboard.copy"/>" onclick="copyDashboard(${dash.id}); return false;">
                                        <img alt="" src="<c:url value="/images/copy_16.png"/>"/>
                                    </a>
                                    <a href="javascript://" title="<spring:message code="dashboard.delete"/>" onclick="deleteDashboard(${dash.id}); return false;">
                                        <img alt="" src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>"/>
                                    </a>
                                </ehd:authorizeGlobal>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:when>
            <c:otherwise>
                <p class="with-padding"><spring:message code="dashboard.noneFound"/></p>
            </c:otherwise>
        </c:choose>
    </div>
</section>
</body>
