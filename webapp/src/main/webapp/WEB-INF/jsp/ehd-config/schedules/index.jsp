<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>
    <title><spring:message code="scheduler.title"/></title>
</head>
<body>
<div id="group_${group.id}" class="block-content no-title">
    <div class="block-controls">
        <ul class="controls-buttons">
            <li>
                <a class="addSchedule" href="<c:url value="/config/groups/${group.id}/schedules/new"/>"
                   data-form-title="<spring:message code="scheduler.editTask" javaScriptEscape="true"/> - <c:out value="${group.name}"/>">
                    <spring:message code="scheduler.newTask"/>
                    <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>">
                </a>
            </li>
        </ul>
    </div>

    <c:choose>
        <c:when test="${not empty scheduleList}">
            <div class="no-margin">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col"><spring:message code="scheduler.taskName"/></th>
                        <th style="width:28px" scope="col"><spring:message code="scheduler.action"/></th>
                        <th style="width:28px" scope="col"><spring:message code="scheduler.status"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${scheduleList}" var="schedule">
                        <tr>
                            <td>
                                <c:out value="${schedule.name}"/>
                            </td>
                            <td class="table-actions">
                                <a class="editSchedule" href="<c:url value="/config/groups/${group.id}/schedules/${schedule.id}/edit"/>"
                                   data-form-title="<spring:message code="scheduler.editTask" javaScriptEscape="true"/> - <c:out value="${group.name}"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/pencil.png"/>">
                                </a>
                                <a class="delSchedule" href="<c:url value="/config/groups/schedules/${schedule.id}"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>">
                                </a>
                            </td>
                            <td class="table-actions">
                                <span class="statusButton">
                                    <c:choose>
                                        <c:when test="${schedule.running}">
                                            <a class="toggleRunning" href="<c:url value="/config/groups/schedules/${schedule.id}/toggleRunning"/>">
                                                <img src="<c:url value='/images/theme/icons/fugue/control-stop-square.png'/>" alt=""
                                                     title="<spring:message code="scheduler.stopProcess"/>">
                                            </a>
                                        </c:when>
                                        <c:otherwise>
                                            <a class="toggleRunning" href="<c:url value="/config/groups/schedules/${schedule.id}/toggleRunning"/>">
                                                <img src="<c:url value='/images/theme/icons/fugue/control.png'/>" alt=""
                                                     title="<spring:message code="scheduler.startProcess"/>">
                                            </a>
                                        </c:otherwise>
                                    </c:choose>
                                </span>
                                <span class="statusIcon">
                                    <c:choose>
                                        <c:when test="${schedule.running}">
                                            <img src="<c:url value="/images/circle-ball-dark-antialiased.gif"/>" alt=""/>
                                        </c:when>
                                        <c:otherwise>
                                            <img src="<c:url value="/images/circle-ball-dark-noanim.gif"/>" alt=""/>
                                        </c:otherwise>
                                    </c:choose>
                                </span>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </c:when>
        <c:otherwise>
            <p class="with-margin">
                <spring:message code="scheduler.noneFound"/>
            </p>
        </c:otherwise>
    </c:choose>
</div>
</body>