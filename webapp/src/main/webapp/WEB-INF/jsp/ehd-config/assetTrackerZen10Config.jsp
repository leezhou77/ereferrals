<%--@elvariable id="zen10Config" type="net.grouplink.ehelpdesk.common.utils.AssetTrackerZen10Config"--%>

<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>
    <meta name="configSidebar" content="true">
    <title><spring:message code="assetTracker.configurationPage" text="Asset Tracker Configuration Page"/></title>

    <script type="text/javascript">
        function driverChanged(selectBox) {
            console.debug("called driverChanged()");
            console.debug("driverSelect.value: " + selectBox.value);

            if (dojo.attr(selectBox, "value") == 'Sybase ODBC') {
                dojo.query(".hideme").forEach(function(node) {
                    dojo.style(node, "display", "none");
                    dojo.attr(node, "value", "");
                });
                dojo.query(".showme").forEach(function(node) {
                    dojo.style(node, "display", "block");
                });
            } else {
                dojo.query(".hideme").forEach(function(node) {
                    dojo.style(node, "display", "block");
                });
                dojo.query(".showme").forEach(function(node) {
                    dojo.style(node, "display", "none");
                    dojo.attr(node, "value", "");
                })
            }
        }

        function toggleFields(checkBox) {
            if(dojo.attr(checkBox, "checked")) {
                dojo.query("#zen10Fields input, #zen10Fields select, #testButton").attr("disabled", false);
            } else {
                dojo.query("#zen10Fields input, #zen10Fields select, #testButton").attr("disabled", true);
            }
        }

        function init() {
            dojo.connect(dojo.byId("driverSelect"), "onchange", function() {
                driverChanged(this);
            });
            driverChanged(dojo.byId("driverSelect"));

            dojo.connect(dojo.byId("enableZen101"), "onclick", function() {
                toggleFields(this);
            });
            toggleFields(dojo.byId("enableZen101"));

            dojo.connect(dojo.byId("testButton"), "onclick", function() {
                dojo.query("#buttons button").attr("disabled", true);
                dojo.xhrGet({
                    url: "<c:url value="/config/asset/testZen10Config.glml"/>",
                    content: dojo.formToObject("zen10Config"),
                    handleAs: "json",
                    load: function(response) {
                        dojo.query("input.error").removeClass("error");
                        dojo.create("p",
                                    {"class": response["type"] + " message no-margin", innerHTML: response["msg"]},
                                    dojo.byId("messages"),
                                    "only");
                        dojo.query("#buttons button").attr("disabled", false);
                    }
                });
            });
        }

        dojo.addOnLoad(init);

    </script>

</head>

<body>
<section class="grid_10">
    <div class="block-content">
        <h1><spring:message code="assetTracker.zenworksConfig"/></h1>
        <form:form modelAttribute="zen10Config" cssClass="form">
            <div id="messages">
                <spring:hasBindErrors name="zen10Config">
                    <p class="error message no-margin">
                        <form:errors path="*"/>
                    </p>
                </spring:hasBindErrors>
            </div>
            <p>
                <form:checkbox path="enableZen10"/>
                    <form:label path="enableZen10" cssClass="inline"><spring:message code="assetTracker.enableNovellZen10Integration"/></form:label>
            </p>
            <div id="zen10Fields">
                <p>
                    <form:label path="zenBaseUrl"><spring:message code="assetTracker.zenBaseUrl"/></form:label>
                    <form:input path="zenBaseUrl" size="60">
                        <jsp:attribute name="cssClass"><spring:bind path="zen10Config.zenBaseUrl"><c:if test="${not empty status.errorMessage}">error</c:if></spring:bind></jsp:attribute>
                    </form:input>
                    <br>
                    <span class="small">(e.g. http://&lt;servername&gt;/&lt;zenworks&gt;)</span>
                </p>
                <p>
                    <form:checkbox path="enableEhdVnc">
                        <jsp:attribute name="label"><spring:message code="assetTracker.enableEhdVnc"/></jsp:attribute>
                    </form:checkbox>
                </p>
                <p>
                    <form:label path="zenDatabaseConfig.dbType"><spring:message code="databaseConfig.driver"/></form:label>
                    <form:select path="zenDatabaseConfig.dbType" id="driverSelect">
                        <form:option value="Oracle8i"><spring:message code="databaseConfig.oracle8i"/></form:option>
                        <form:option value="Oracle9i"><spring:message code="databaseConfig.oracle9i"/></form:option>
                        <form:option value="Oracle10g"><spring:message code="databaseConfig.oracle10g"/></form:option>
                        <form:option value="Oracle11g"><spring:message code="databaseConfig.oracle11g"/></form:option>
                        <form:option value="SQLServer"><spring:message code="databaseConfig.mssql"/></form:option>
                        <form:option value="Sybase"><spring:message code="databaseConfig.sybase"/></form:option>
                        <form:option value="Sybase ODBC"><spring:message code="databaseConfig.sybaseODBC"/></form:option>
                    </form:select>
                </p>
                <p class="hideme">
                    <form:label path="zenDatabaseConfig.serverName"><spring:message code="databaseConfig.serverName"/></form:label>
                    <form:input path="zenDatabaseConfig.serverName" size="60">
                        <jsp:attribute name="cssClass"><spring:bind path="zen10Config.zenDatabaseConfig.serverName"><c:if test="${not empty status.errorMessage}">error</c:if></spring:bind></jsp:attribute>
                    </form:input>
                </p>
                <p class="hideme">
                    <form:label path="zenDatabaseConfig.databaseName"><spring:message code="databaseConfig.databaseName"/></form:label>
                    <form:input path="zenDatabaseConfig.databaseName" size="60">
                        <jsp:attribute name="cssClass"><spring:bind path="zen10Config.zenDatabaseConfig.databaseName"><c:if test="${not empty status.errorMessage}">error</c:if></spring:bind></jsp:attribute>
                    </form:input>
                </p>
                <p class="hideme">
                    <form:label path="zenDatabaseConfig.username"><spring:message code="databaseConfig.userName"/></form:label>
                    <form:input path="zenDatabaseConfig.username" size="60">
                        <jsp:attribute name="cssClass"><spring:bind path="zen10Config.zenDatabaseConfig.username"><c:if test="${not empty status.errorMessage}">error</c:if></spring:bind></jsp:attribute>
                    </form:input>
                </p>
                <p class="showme">
                    <form:label path="zenDatabaseConfig.dsn"><spring:message code="databaseConfig.odbcDsn"/></form:label>
                    <form:input path="zenDatabaseConfig.dsn" size="60">
                        <jsp:attribute name="cssClass"><spring:bind path="zen10Config.zenDatabaseConfig.dsn"><c:if test="${not empty status.errorMessage}">error</c:if></spring:bind></jsp:attribute>
                    </form:input>
                </p>
                <p class="hideme">
                    <form:label path="zenDatabaseConfig.password"><spring:message code="databaseConfig.password"/></form:label>
                    <form:password path="zenDatabaseConfig.password" showPassword="true" size="60">
                        <jsp:attribute name="cssClass"><spring:bind path="zen10Config.zenDatabaseConfig.password"><c:if test="${not empty status.errorMessage}">error</c:if></spring:bind></jsp:attribute>
                    </form:password>
                </p>
            </div>
            <br>
            <p id="buttons" class="grey-bg no-margin">
                <button type="submit" name="saveButton"><spring:message code="global.save"/></button>
                <button type="button" id="testButton"><spring:message code="global.test"/></button>
            </p>
        </form:form>
    </div>
</section>
</body>
