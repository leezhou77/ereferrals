<%--@elvariable id="ldapUser" type="net.grouplink.ehelpdesk.domain.LdapUser"--%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>
    <meta name="configSidebar" content="true">

    <tags:jsTemplate id="ldap-field-tmpl">
        <tr>
            <td>
                <input type="hidden" name="ldapFields[{{ index }}].name" value="{{ attributeName }}">
                {{ attributeName }}
            </td>
            <td>
                <input name="ldapFields[{{ index }}].label" dojoType="dijit.form.TextBox" style="width:100%" value="{{ attributeName }}">
            </td>
            <td>
                <input type="checkbox" name="ldapFields[{{ index }}].showOnTicket" dojoType="dijit.form.CheckBox" checked="checked">
                <input type="hidden" name="_ldapFields[{{ index }}].showOnTicket" value="on">
            </td>
            <td>
                <a class="delField" href="javascript:void(0)"><img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>"></a>
            </td>
        </tr>
    </tags:jsTemplate>

    <script type="text/javascript">
        function getNextIndex() {
            var lastRow = dojo.query('#ldapFieldTable tbody tr:last-child')[0];
            if(dojo.query("input", lastRow).length > 0) {
                var nameOfLastRowInputs = dojo.attr(dojo.query('#ldapFieldTable tbody tr:last-child input[name$=".name"]')[0], "name");
                return parseInt(parseInt(/[(^\d+$)]/.exec(nameOfLastRowInputs)[0]) + 1);
            } else {
                return 0;
            }
        }

        dojo.addOnLoad(function() {
            dojo.require("dojo.NodeList-traverse");

            $(".addField").live("click", function() {
                var template = _.template($("#ldap-field-tmpl").html());
                var attrName = dojo.attr(this, "data-attribute-name");
                var index = getNextIndex();
                var html = dojo.trim(template({
                    index: index,
                    attributeName: attrName
                }));
                var table = dojo.byId("ldapFieldTable");
                dojo.style(table, "display", "table");
                var tableHeader = dojo.query("thead", table)[0];
                dojo.fadeIn({
                    node: tableHeader,
                    onEnd: function() {
                        dojo.style(tableHeader, "display", "table-header-group");
                    }
                }).play();
                var position = "last";
                if(index == 0) {
                    position = "only";
                }
                dojo.place(html, dojo.query("tbody", table)[0], position);
                dojo.parser.parse(table);
            });

            $(".delField").live("click", function() {
                var elToRemove = dojo.query(this).parents("tr")[0];
                dojo.fadeOut({
                    node: elToRemove,
                    onEnd: function() {
                        var hideTableHeader = dojo.query("#ldapFieldTable tbody tr").length == 1;
                        var idInput = dojo.query('input[name$=".id"]', elToRemove);
                        if(idInput.length) {
                            dojo.style(elToRemove, "display", "none");
                            var idsToDeleteInput = dojo.query('input[name="idsToDelete"]')[0];
                            var id = dojo.attr(idInput[0], "value");
                            var currentIds = dojo.attr(idsToDeleteInput, "value");
                            dojo.attr(idsToDeleteInput, "value", currentIds + (currentIds.length ? "," + id : id));
                        } else {
                            dojo.destroy(elToRemove);
                        }
                        if(hideTableHeader) {
                            var node = dojo.query("#ldapFieldTable thead")[0];
                            dojo.fadeOut({
                                node: node,
                                onEnd: function() {
                                    dojo.style(node, "display", "none");
                                }
                            }).play();
                            dojo.create("tr",
                                    {innerHTML: "<td><spring:message code="ldapconfig.fieldSetup.noneFound" javaScriptEscape="true"/></td>"},
                                    dojo.query("#ldapFieldTable tbody")[0],
                                    "only");
                        }
                    }
                }).play();
            });
        });
    </script>
</head>
<body>
<section class="grid_10">
    <div class="block-content form">
        <h1><spring:message code="ldapconfig.fieldSetup"/></h1>

        <form id="myForm" action="" method="post" class="with-margin">
            <p>
                <label for="dn"><spring:message code="ldap.ldapUser"/><tags:helpIcon id="ldapUserHelp" messageCode="ldapconfig.ldapField.ldapUserHelp"/></label>
                <input id="dn" name="dn" value="${ldapUser.dn}" dojoType="dijit.form.TextBox" style="width:400px"/>
                <button type="submit" dojoType="dijit.form.Button"><spring:message code="global.submit"/></button>
            </p>
        </form>

        <div class="with-margin">
            <c:if test="${not empty ldapUser.attributeMap}">
                <label><spring:message code="ldapconfig.ldapField.availableFields"/></label>
                <div class="full-width" style="max-height: 200px;overflow: auto; overflow-x: hidden;">
                    <table class="table">
                        <c:forEach items="${ldapUser.attributeMap}" var="attribute">
                            <tr>
                                <td>
                                    <a class="addField" href="javascript:void(0)" data-attribute-name="${attribute.key}">
                                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="<c:out value="${attribute.key}"/>">
                                    </a>
                                </td>
                                <td>
                                    <c:out value="${attribute.key}"/>
                                </td>
                                <td>
                                    <c:out escapeXml="false" value="${attribute.value}"/>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </c:if>
        </div>

        <form:form modelAttribute="ldapFieldForm">
            <jsp:attribute name="action"><c:url value="/config/ldap/ldapFields/createMultiple"/></jsp:attribute>
            <jsp:body>
                <input type="hidden" name="idsToDelete"/>
                <div class="with-margin">
                    <label><spring:message code="ldapconfig.ldapField.configuredFields"/></label>
                    <table id="ldapFieldTable" class="table full-width">
                        <thead<c:if test="${empty ldapFieldForm.ldapFields}"> style="display:none"</c:if>>
                            <tr>
                                <th><spring:message code="ldapconfig.ldapField" /></th>
                                <th><spring:message code="ldapconfig.ldapField.label" /></th>
                                <th style="width:150px"><spring:message code="ldapconfig.ldapField.showOnTicket" /></th>
                                <th style="width:15px"></th>
                            </tr>
                        </thead>
                        <c:choose>
                            <c:when test="${not empty ldapFieldForm.ldapFields}">
                                <tbody>
                                    <c:forEach items="${ldapFieldForm.ldapFields}" var="field" varStatus="row">
                                        <tr>
                                            <td>
                                                <c:if test="${field.id != null}"><form:hidden path="ldapFields[${row.index}].id"/></c:if>
                                                <form:hidden path="ldapFields[${row.index}].name"/>
                                                <c:out value="${field.name}"/>
                                            </td>
                                            <td>
                                                <form:input path="ldapFields[${row.index}].label" dojoType="dijit.form.TextBox" cssStyle="width:100%"/>
                                            </td>
                                            <td>
                                                <form:checkbox path="ldapFields[${row.index}].showOnTicket" dojoType="dijit.form.CheckBox"/>
                                            </td>
                                            <td>
                                                <a class="delField" href="javascript:void(0)"><img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>"></a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </c:when>
                            <c:otherwise>
                                <tbody>
                                    <tr>
                                        <td>
                                            <spring:message code="ldapconfig.fieldSetup.noneFound"/>
                                        </td>
                                    </tr>
                                </tbody>
                            </c:otherwise>
                        </c:choose>
                    </table>
                </div>
                <p class="no-margin grey-bg">
                    <button type="submit"><spring:message code="global.save"/></button>
                </p>
            </jsp:body>
        </form:form>
    </div>
</section>
</body>
