<%@ include file="/WEB-INF/jsp/include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>

    <spring:theme code="css" var="cssfile"/>
    <c:if test="${not empty cssfile}">
        <link rel="stylesheet" href="<c:url value="${cssfile}"/>"
              type="text/css"/>
    </c:if>

    <script type="text/javascript" src="<c:url value="/js/dtree.js" />"></script>

    <script type="text/javascript">
        function showLocList(loc) {
            window.parent.showLocList(loc);
        }

        function modifyCategory(cat) {
            window.parent.modifyCategory(cat);
        }

        function modifyCategoryOption(opt) {
            window.parent.modifyCategoryOption(opt);
        }
    </script>
</head>
<body>
<table>
    <tr>
        <td>
            <script type="text/javascript">
                <!--
                cat = new dTree('cat', '<c:url value="/images/dtree"/>');
                cat.config.folderLinks = true;
                cat.config.useSelection = true;
                cat.config.useCookies = true;
                <c:forEach items="${catList}" var="node" >
                cat.add(<c:out value="${node.index}" />, <c:out value="${node.parent}" />, '<c:out value="${node.name}" />', '', '<c:out value="${node.text}" />', '', '', '', '<c:url value="${node.iconOpen}" />', '<c:url value="${node.iconOpen}" />');
                </c:forEach>
                //-->
                document.write(cat);
            </script>
        </td>
    </tr>
</table>
</body>
</html>