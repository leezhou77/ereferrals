<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="customFields" type="java.util.List"--%>

<head>
<meta name="configSidebar" content="true">
<title><spring:message code="customField.customFields"/></title>

<style type="text/css">
    .page-controls {
        padding: 15px;
    }

    .page-controls::after, .table-controls::after {
        clear: both;
        content: ' ';
        display: block;
        font-size: 0;
        line-height: 0;
        visibility: hidden;
        width: 0;
        height: 0;
    }

    #cfTable_wrapper .table-message {
        margin-bottom: 0;
    }

    .table-controls {
        padding: 15px;
    }
</style>

<!-- DataTables -->
<script type="text/javascript" charset="utf8" src="<c:url value="/js/jquery.dataTables.min.js"/>"></script>

<script type="text/javascript">

dojo.addOnLoad(function () {

    <c:if test="${not empty customFields}">
    $.fn.dataTableExt.oStdClasses.sWrapper = 'no-margin';
    $.fn.dataTableExt.oStdClasses.sInfo = 'message no-margin table-message';
    $.fn.dataTableExt.oStdClasses.sLength = 'float-left';
    $.fn.dataTableExt.oStdClasses.sFilter = 'float-right';
    $.fn.dataTableExt.oStdClasses.sPaging = 'sub-hover paging_';
    $.fn.dataTableExt.oStdClasses.sPagePrevEnabled = 'control-prev';
    $.fn.dataTableExt.oStdClasses.sPagePrevDisabled = 'control-prev disabled';
    $.fn.dataTableExt.oStdClasses.sPageNextEnabled = 'control-next';
    $.fn.dataTableExt.oStdClasses.sPageNextDisabled = 'control-next disabled';
    $.fn.dataTableExt.oStdClasses.sPageFirst = 'control-first';
    $.fn.dataTableExt.oStdClasses.sPagePrevious = 'control-prev';
    $.fn.dataTableExt.oStdClasses.sPageNext = 'control-next';
    $.fn.dataTableExt.oStdClasses.sPageLast = 'control-last';

    $('.sortable').each(function (i) {
        // DataTable config
        var table = $(this),
                oTable = table.dataTable({
                    /*
                     * We set specific options for each columns here. Some columns contain raw data to enable correct sorting, so we convert it for display
                     * @url http://www.datatables.net/usage/columns
                     */
                    aoColumns:[
                        { bSortable:false },
                        { sType:'string' },
                        { sType:'string' },
                        { sType:'numeric' },
                        { sType:'string' },
                        { sType:'string' },
                        { sType:'string' },
                        { sType:'string' },
                        { sType:'string' },
                        { bSortable:false }
                    ],

                    /*
                     * Set DOM structure for table controls
                     * @url http://www.datatables.net/examples/basic_init/dom.html
                     */
                    sDom:'<"page-controls grey-bg"<"controls-buttons"p>>rti<"table-controls grey-bg"lf>',

                    /*
                     * Callback to apply template setup
                     */
                    fnDrawCallback:function () {
                        this.parent().applyTemplateSetup();
                    },
                    fnInitComplete:function () {
                        this.parent().applyTemplateSetup();
                    }
                });

        // Sorting arrows behaviour
        table.find('thead .sort-up').click(function (event) {
            // Stop link behaviour
            event.preventDefault();

            // Find column index
            var column = $(this).closest('th'),
                    columnIndex = column.parent().children().index(column.get(0));

            // Send command
            oTable.fnSort([
                [columnIndex, 'asc']
            ]);

            // Prevent bubbling
            return false;
        });
        table.find('thead .sort-down').click(function (event) {
            // Stop link behaviour
            event.preventDefault();

            // Find column index
            var column = $(this).closest('th'),
                    columnIndex = column.parent().children().index(column.get(0));

            // Send command
            oTable.fnSort([
                [columnIndex, 'desc']
            ]);

            // Prevent bubbling
            return false;
        });
    });
    </c:if>

    var custFieldDlg = new dijit.Dialog({
        style:"width:700px",
        onDownloadEnd:dlgCustFieldDownloadEnd
    });

    function dlgCustFieldDownloadEnd() {
        toggleCustomFieldOptions(dojo.byId("fieldType"));
        var form = dojo.byId("customField");

        dojo.connect(form, "onsubmit", function (evt) {
            //we'll handle form submission on our own, so prevent the submit from happening
            dojo.stopEvent(evt);
            dojo.xhrPost({
                form:form,
                load:function (response) {
                    // Check for validation errors
                    var tempDiv = document.createElement("div");
                    tempDiv.innerHTML = response;
                    var errors = dojo.query("[id=bindErrors]", tempDiv);
                    if (errors.length) {
                        custFieldDlg.set("content", response);
                        dlgCustFieldDownloadEnd();
                    } else {
                        custFieldDlg.hide();
                        location.reload();
                    }
                }
            });
        });

        dojo.connect(dojo.byId("cfCancel"), "onclick", function () {
            custFieldDlg.hide();
        });

        dojo.connect(dijit.byId("group"), "onChange", function() {
            loadNextSelectForPrev('group', 'category', categoryStore, '<c:url value="/stores/categories.json"/>');
        });

        dojo.connect(dijit.byId("category"), "onChange", function() {
            loadNextSelectForPrev('category', 'categoryOption', catOptStore, '<c:url value="/stores/catOptions.json"/>');
        });

        var cforder = dojo.attr(dojo.byId("order"), "value");
        new dijit.form.NumberSpinner({
            id:"order",
            name:"order",
            style:"width:60px;",
            value:cforder,
            constraints:{min:0, max:1000, places:0 }
        }, "order");

        $("#customField").applyTemplateSetup();
    }

    function toggleCustomFieldOptions(selectBox) {
//        var selectedOption = dojo.attr(dojo.query("option:checked", selectBox)[0], "id");
        var selectedOption = dojo.byId("fieldType").value;
        var display = (selectedOption == "checkbox" || selectedOption == "radio") ? "none" : "block";
        dojo.style("required", "display", display);
        dojo.style("divCustFieldOptions", "display", (selectedOption == "select" || selectedOption == "radio") ? "block" : "none");
    }

    $("#fieldType").live("change", function () {
        toggleCustomFieldOptions(this);
    });

    $("#addNewCustomFieldOption").live("click", function (e) {
        e.stopPropagation();
        var nextId = 0;
        if (dojo.query(".customFieldOption").length > 0) {
            nextId = parseInt(dojo.attr(dojo.query("#customFieldOptions input").slice(-1)[0], "id")) + 1;
        }

        $('<div class="customFieldOption small-margin" id="customFieldOption_new">\
                       <input type="text" id="' + nextId + '" name="options[' + nextId + '].displayValue" size="30">\
                           <a href="javascript:void(0)" class="deleteCustomFieldOption">\
                               <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt="">\
                           </a>\
                   </div>').appendTo("#customFieldOptions");
    });

    $(".deleteCustomFieldOption").live("click", function (e) {
        e.stopPropagation();
        var cfoId = $(this).parent("div.customFieldOption").attr("id").replace("customFieldOption_", "");
        $('<input type="hidden" name="optionsToDestroy" value="' + cfoId + '">').appendTo($(this).parent("div.customFieldOption"));
        $(this).parent("div.customFieldOption").fadeOut(function () {
            $(this).addClass("deleted");
            if (/new$/.test($(this).attr("id"))) {
                $(this).remove();
            }
        });
    });

    dojo.connect(dojo.byId("addCustomField"), "onclick", function (evt) {
        evt.preventDefault();
        custFieldDlg.set('href', "<c:url value="/config/customFields/new"><c:param name="dialog" value="true"/></c:url>");
        custFieldDlg.set('title', "<spring:message code="customField.setupTitle"/>");
        custFieldDlg.show();
    });

    //handle button clicks for editing a custom field
    $('a[id^="edit_cf_"]').live("click", function (evt) {
        var cfId = this.id.replace("edit_cf_", "");
        custFieldDlg.set('href', "<c:url value="/config/customFields/"/>" + cfId + "/edit?dialog=true");
        custFieldDlg.set('title', "<spring:message code="customField.edit"/>");
        custFieldDlg.show();
    });

    //handle button clicks for deleting a custom field
    $('a[id^="del_cf_"]').live("click", function (evt) {
        if (confirm('<spring:message code="global.confirmdelete" javaScriptEscape="true"/>')) {
            var cfId = this.id.replace("del_cf_", "");
            dojo.xhrDelete({
                url:"<c:url value="/config/customFields/"/>" + cfId,
                handle:function (response, ioargs) {
                    location.replace("<c:url value="/config/customFields"/>");
                }
            });
        }
    });

    dojo.connect(dojo.byId("selectAllBtn"), "onclick", function(){
        var cbxs = dojo.query('[name=\"cfListCbx\"]');
        dojo.forEach(cbxs, function(cbx){
            dijit.getEnclosingWidget(cbx).set("checked", true);
        });
    });

    dojo.connect(dojo.byId("deselectAllBtn"), "onclick", function(){
        var cbxs = dojo.query('[name=\"cfListCbx\"]');
        dojo.forEach(cbxs, function(cbx){
            dijit.getEnclosingWidget(cbx).set("checked", false);
        });
    });

    dojo.connect(dojo.byId("cf_table_form"), "onsubmit", function(evt){
        dojo.stopEvent(evt);
        if (confirm('<spring:message code="customField.deletePrompt" javaScriptEscape="true"/>')) {
            var form = dojo.byId("cf_table_form");
            dojo.xhrDelete({
                form: form,
                url: "<c:url value="/config/customFields/deleteMultiple"/>",
                handle: function() {
                    location.replace("<c:url value="/config/customFields"/>");
                }
            });
        }
    });
});

function loadNextSelectForPrev(prevName, nextName, nextStore, nextStoreUrl) {
    var prevId = dijit.byId(prevName).getValue();
    if (prevId == "") {
        // clear successive select starting from 'next'
        clearFromPoint(nextName);
        return;
    }

    // if prevName is categoryOption then set the groupId parameter for assignments
    // and load the customFields
    var groupId = null;
    var locationId = dijit.byId("location").getValue();

    // reset the store with prevId parameter
    resetStore(nextName, nextStore, nextStoreUrl, prevId, groupId, locationId);
}

function clearFromPoint(start) {
    switch (start) {
        case "group":
            resetStore("group", groupStore, '<c:url value="/stores/groups.json"/>', '', null, null);
        // fall through
        case "category":
            resetStore("category", categoryStore, '<c:url value="/stores/categories.json"/>', '', null, null);
        // fall through
        case "categoryOption":
            resetStore("categoryOption", catOptStore, '<c:url value="/stores/catOptions.json"/>', '', null, null);
            break;
        default:
            break;
    }
}

function resetStore(selectName, store, storeUrl, prevId, groupId, locationId) {
    // build the store URL
    var builtURL = storeUrl + "?incEmpty=true&prevId=" + prevId;
    if (groupId != null) builtURL += "&groupId=" + groupId;
    if (locationId != null) builtURL += "&locationId=" + locationId;

    store.close();
    store = new dojo.data.ItemFileReadStore({
        url:builtURL,
        hierarchical: false
    });

    store.fetch();
    dijit.byId(selectName).store = store;
    dijit.byId(selectName).setDisplayedValue('');

    store.fetch({query:{}, onComplete:function (items, request) {
        if (items.length == 1) {
            dijit.byId(selectName).attr('value', items[0].id);
        }
    }
    });
}
</script>
</head>
<body>
<section class="grid_10">
    <form class="form" id="cf_table_form" method="post" action="">
        <div class="block-content">
            <h1><spring:message code="customField.customFields"/></h1>

                <div class="block-controls">
                    <ul class="controls-buttons">
                        <li>
                            <a id="addCustomField" href="javascript://">
                                <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="">
                                <spring:message code="customField.create"/>
                            </a>
                        </li>
                    </ul>
                </div>
                <c:choose>
                    <c:when test="${empty customFields}">
                        <p>
                            <spring:message code="customField.noneFound"/>
                        </p>
                    </c:when>
                    <c:otherwise>
                        <table id="cfTable" class="table sortable">
                            <thead>
                            <tr>
                                <th></th>
                                <th scope="col">
                                    <span class="column-sort">
                                        <a href="#" title="<spring:message code="global.sortup"/>" class="sort-up"></a>
                                        <a href="#" title="<spring:message code="global.sortdown"/>" class="sort-down"></a>
                                    </span>
                                    <spring:message code="customField.name"/>
                                </th>
                                <th scope="col">
                                    <span class="column-sort">
                                        <a href="#" title="<spring:message code="global.sortup"/>" class="sort-up"></a>
                                        <a href="#" title="<spring:message code="global.sortdown"/>" class="sort-down"></a>
                                    </span>
                                    <spring:message code="customField.fieldType"/>
                                </th>
                                <th scope="col">
                                    <span class="column-sort">
                                        <a href="#" title="<spring:message code="global.sortup"/>" class="sort-up"></a>
                                        <a href="#" title="<spring:message code="global.sortdown"/>" class="sort-down"></a>
                                    </span>
                                    <spring:message code="global.order"/>
                                </th>
                                <th scope="col">
                                    <span class="column-sort">
                                        <a href="#" title="<spring:message code="global.sortup"/>" class="sort-up"></a>
                                        <a href="#" title="<spring:message code="global.sortdown"/>" class="sort-down"></a>
                                    </span>
                                    <spring:message code="customField.required"/>
                                </th>
                                <th scope="col">
                                    <span class="column-sort">
                                        <a href="#" title="<spring:message code="global.sortup"/>" class="sort-up"></a>
                                        <a href="#" title="<spring:message code="global.sortdown"/>" class="sort-down"></a>
                                    </span>
                                    <spring:message code="ticket.location"/>
                                </th>
                                <th scope="col">
                                    <span class="column-sort">
                                        <a href="#" title="<spring:message code="global.sortup"/>" class="sort-up"></a>
                                        <a href="#" title="<spring:message code="global.sortdown"/>" class="sort-down"></a>
                                    </span>
                                    <spring:message code="ticket.group"/>
                                </th>
                                <th scope="col">
                                    <span class="column-sort">
                                        <a href="#" title="<spring:message code="global.sortup"/>" class="sort-up"></a>
                                        <a href="#" title="<spring:message code="global.sortdown"/>" class="sort-down"></a>
                                    </span>
                                    <spring:message code="ticket.category"/>
                                </th>
                                <th scope="col">
                                    <span class="column-sort">
                                        <a href="#" title="<spring:message code="global.sortup"/>" class="sort-up"></a>
                                        <a href="#" title="<spring:message code="global.sortdown"/>" class="sort-down"></a>
                                    </span>
                                    <spring:message code="ticket.categoryOption"/>
                                </th>
                                <th scope="col" class="table-actions sorting_disabled" style="width:100px;">
                                    <spring:message code="global.actions"/>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${customFields}" var="cf" varStatus="status">
                                <tr class="${status.index%2 ==0? 'odd':'even'}">
                                    <td class="th table-check-cell">
                                        <input type="checkbox" name="cfListCbx" dojotype="dijit.form.CheckBox"
                                               value="${cf.id}">
                                    </td>
                                    <td><c:out value="${cf.name}"/></td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${cf.fieldType eq 'text'}">
                                                <spring:message code="customField.textField"/>
                                            </c:when>
                                            <c:when test="${cf.fieldType eq 'textarea'}">
                                                <spring:message code="customField.textArea"/>
                                            </c:when>
                                            <c:when test="${cf.fieldType eq 'select'}">
                                                <spring:message code="customField.dropDown"/>
                                            </c:when>
                                            <c:when test="${cf.fieldType eq 'checkbox'}">
                                                <spring:message code="customField.checkbox"/>
                                            </c:when>
                                            <c:when test="${cf.fieldType eq 'radio'}">
                                                <spring:message code="customField.radio"/>
                                            </c:when>
                                            <c:when test="${cf.fieldType eq 'date'}">
                                                <spring:message code="customField.date"/>
                                            </c:when>
                                            <c:when test="${cf.fieldType eq 'datetime'}">
                                                <spring:message code="customField.datetime"/>
                                            </c:when>
                                            <c:when test="${cf.fieldType eq 'number'}">
                                                <spring:message code="customField.number"/>
                                            </c:when>
                                            <c:otherwise>
                                                <c:out value="${cf.fieldType}"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td><c:out value="${cf.order}"/></td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${cf.required}">
                                                <spring:message code="global.true" text="True"/>
                                            </c:when>
                                            <c:otherwise>
                                                <spring:message code="global.false" text="False"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td><c:out value="${cf.location.name}"/></td>
                                    <td><c:out value="${cf.group.name}"/></td>
                                    <td><c:out value="${cf.category.name}"/></td>
                                    <td><c:out value="${cf.categoryOption.name}"/></td>
                                    <td>
                                        <a id="edit_cf_${cf.id}" href="javascript://"
                                           title="<spring:message code="global.edit"/>" class="with-tip">
                                            <img src="<c:url value="/images/theme/icons/fugue/pencil.png"/>"/>
                                        </a>
                                        <a id="del_cf_${cf.id}" href="javascript://"
                                           title="<spring:message code="global.delete"/>" class="with-tip">
                                            <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>"/>
                                        </a>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </c:otherwise>
                </c:choose>
            <div class="block-footer">
                <img src="<c:url value="/images/theme/icons/fugue/arrow-curve-000-left.png"/>" width="16" height="16" class="picto">
                <a id="selectAllBtn" href="#" class="button"><spring:message code="userManagement.import.selectAll"/></a>
                <a id="deselectAllBtn" href="#" class="button"><spring:message code="userManagement.import.deselectAll"/></a>
                <span class="sep"></span>
                <button type="submit" class="red small">
                    <spring:message code="global.deleteSelected"/>
                </button>
            </div>
        </div>
    </form>
</section>
</body>