<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>
    <title><spring:message code="statusSetup.title"/></title>
    <meta name="configSidebar" content="true"/>

    <tags:jsTemplate id="status-tmpl">
        <tr class="dojoDndItem">
            <td style="width:15px">
                <img class="dojoDndHandle" src="<c:url value="/images/theme/icons/fugue/arrow-move.png"/>">
                <input type="hidden" name="statuses[{{ index }}].order" value="{{ order }}">
            </td>
            <td style="width:20%">&nbsp;</td>
            <td>
                <input type="text" name="statuses[{{ index }}].name" dojoType="dijit.form.TextBox" maxlength="50" style="width:100%">
            </td>
            <td class="table-actions" style="width:15px">
                <a class="delStatus" href="javascript:void(0)">
                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>">
                </a>
            </td>
        </tr>
    </tags:jsTemplate>

    <script type="text/javascript">
        function getNextStatusInfo() {
            var lastStatus = dojo.query("#statusTable tr:last-child")[0];
            var orderInput = dojo.query('input[name$=".order"]', lastStatus)[0];
            var rowName = dojo.attr(orderInput, "name");
            var order = parseInt(dojo.attr(orderInput, "value")) + 1;
            var lastIndexRegexMatch = /\[(\d+)\]/.exec(rowName);
            var lastIndex = lastIndexRegexMatch[1];
            var index = parseInt(lastIndex) + 1;
            return {"index": index, "order": order};
        }

        function listReordered() {
            dojo.query("#statusTable tr").forEach(function(item, index) {
                var orderInput = dojo.query('input[name$=".order"]', item)[0];
                dojo.attr(orderInput, "value", index);
            });

            dojo.addClass("addNewStatus", "disabled")
        }

        dojo.addOnLoad(function() {
            dojo.query("#addNewStatus").connect("onclick", function(e) {
                if (dojo.hasClass("addNewStatus", "disabled")) return;
                var template = _.template(dojo.attr(dojo.byId("status-tmpl"), "innerHTML"));

                var refnode = dojo.query("#statusTable tbody")[0];
                dojo.place(dojo.trim(template(getNextStatusInfo())), refnode);
                dojo.parser.parse(refnode);
            });

            $(".delStatus").live("click", function(e) {
                e.stopPropagation();
                var elmToRemove = this.parentNode.parentNode;
                dojo.fadeOut({
                    node: elmToRemove,
                    onEnd: function() {
                        var thisId = dojo.attr(elmToRemove, "id");
                        if(thisId != null && thisId != "status_") {
                            var statusId = thisId.replace("status_", "");
                            var idsToDeleteInput = dojo.query('input[name="idsToDelete"]')[0];
                            var ids = dojo.attr(idsToDeleteInput, "value");
                            dojo.attr(idsToDeleteInput, "value", ids + (ids.length ? "," + statusId : statusId));
                            dojo.style(elmToRemove, "display", "none");
                        } else {
                            dojo.destroy(elmToRemove);
                        }
                    }
                }).play();
            });

            dojo.connect(dojo.byId("saveStatuses"), "onclick", function() {
                dojo.byId("statusForm").submit();
            });
        });
    </script>
</head>
<body>
<section class="grid_7">
    <form:form modelAttribute="statusForm">
        <input type="hidden" name="idsToDelete"/>
        <div class="block-content">
            <h1><spring:message code="statusSetup.title"/></h1>

            <div class="block-controls">
                <ul class="controls-buttons">
                    <li>
                        <a id="addNewStatus" href="javascript:void(0)">
                            <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>">
                            <spring:message code="statusSetup.add"/>
                        </a>
                    </li>
                    <li>
                        <a id="saveStatuses" href="javascript:void(0)">
                            <img src="<c:url value="/images/save.gif"/>">
                            <spring:message code="global.short.save"/>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="no-margin">
                <spring:hasBindErrors name="statusForm">
                    <p class="message error no-margin">
                        <form:errors path="*"/>
                    </p>
                </spring:hasBindErrors>
                <table id="statusTable" class="table no-margin" dojoType="dojo.dnd.Source" withHandles="true" autoSync="true">
                    <script type="dojo/connect" event="onDrop" args="source, nodes, copy">
                        listReordered();
                    </script>
                    <thead></thead>
                    <tbody>
                        <c:forEach items="${statusForm.statuses}" var="status" varStatus="counter">
                            <tr id="status_${status.id}" class="dojoDndItem">
                                <td style="width:15px">
                                    <img class="dojoDndHandle" src="<c:url value="/images/theme/icons/fugue/arrow-move.png"/>">
                                    <c:if test="${status.id != null}"><form:hidden path="statuses[${counter.index}].id"/></c:if>
                                    <form:hidden path="statuses[${counter.index}].order"/>
                                </td>
                                <td style="width:20%">
                                    <c:choose>
                                        <c:when test="${status.id == 1}">
                                            <ul class="keywords"><li><spring:message code="statusSetup.ticketPoolInitial"/></li></ul>
                                        </c:when>
                                        <c:when test="${status.id == 2}">
                                            <ul class="keywords"><li><spring:message code="statusSetup.initial"/></li></ul>
                                        </c:when>
                                        <c:when test="${status.id == 6}">
                                            <ul class="keywords"><li><spring:message code="statusSetup.closed"/></li></ul>
                                        </c:when>
                                        <c:otherwise>&nbsp;</c:otherwise>
                                    </c:choose>
                                </td>
                                <td><form:input path="statuses[${counter.index}].name" dojoType="dijit.form.TextBox" maxlength="50" cssStyle="width:100%" cssErrorClass="error"/></td>
                                <td class="table-actions" style="width:15px">
                                    <c:set var="undeletables">1,2,6</c:set>
                                    <c:choose>
                                        <c:when test="${not empty status.id && fn:contains(undeletables, status.id)}">
                                            &nbsp;
                                        </c:when>
                                        <c:otherwise>
                                            <a class="delStatus" href="javascript:void(0)">
                                                <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>">
                                            </a>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </form:form>
</section>
</body>