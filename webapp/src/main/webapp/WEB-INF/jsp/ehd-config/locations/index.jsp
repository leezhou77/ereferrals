<%--@elvariable id="locationList" type="java.util.List"--%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>
    <meta name="configSidebar" content="true"/>
    <title>
        <spring:message code="locationSetup.title"/>
    </title>
    <script type="text/javascript">

        function showLocDialog(url, title) {
            var dlg = dijit.byId("addNewLocDlg");
            dlg.set('href', url);
            dlg.set('title', title);
            dlg.show();
        }

        dojo.addOnLoad(function() {

            function dlgDownloadEnd() {
                var form = dojo.byId("location");
                dojo.connect(form, "onsubmit", function(evt) {
                    //we'll handle form submission on our own, so prevent the submit from happening
                    dojo.stopEvent(evt);
                    dojo.xhrPost({
                        form: form,
                        load: function(response) {
                            // Check for validation errors
                            var tempDiv = document.createElement("div");
                            tempDiv.innerHTML = response;
                            var errors = dojo.query("[id=bindErrors]", tempDiv);
                            if (errors.length) {
                                dijit.byId("addNewLocDlg").set("content", response);
                                dlgDownloadEnd();
                            } else {
                                dijit.byId("addNewLocDlg").hide();
                                location.reload();
                            }
                        }
                    });
                });

                dojo.connect(dojo.byId("locCancel"), "onclick", function() {
                    dijit.byId("addNewLocDlg").hide();
                });

                $("#location").applyTemplateSetup();
            }

            new dijit.Dialog({
                onDownloadEnd: dlgDownloadEnd
            }, "addNewLocDlg");

            //handle button clicks for the add new location button
            dojo.connect(dojo.byId("addNewLoc"), "onclick", function() {
                showLocDialog("<c:url value="/config/locations/new"><c:param name="dialog" value="true"/></c:url>",
                        "<spring:message code="location.new"/>");
            });

            //handle button clicks for editing a location
            dojo.query('a[id^="edit_loc_"]').connect("onclick", function(evt) {
                var locId = this.id.replace("edit_loc_", "");
                showLocDialog("<c:url value="/config/locations/"/>" + locId + "/edit?dialog=true",
                        "<spring:message code="location.edit"/>");
            });

            //handle button clicks for deleting a location
            dojo.query('a[id^="del_loc_"]').connect("onclick", function(evt) {
                if (confirm('<spring:message code="global.confirmdelete" javaScriptEscape="true"/>')) {
                    var locId = this.id.replace("del_loc_", "");
                    var elementToRemove = this.parentNode.parentNode;
                    dojo.xhrDelete({
                        url: "<c:url value="/config/locations/"/>" + locId,
                        handle: function(response, ioargs) {
                            if (ioargs.xhr.status == 417) {
                                dojo.place('<ul class="message warning grid_12">\
                                     <li><spring:message code="location.inuse" javaScriptEscape="true"/></li>\
                                  </ul>', "flashMessage", "only");
                            } else {
                                dojo.fadeOut({
                                    node: elementToRemove,
                                    onEnd: function() {
                                        dojo.destroy(elementToRemove);
                                    }
                                }).play();
                            }
                        }
                    });
                }
            });
        });
    </script>
</head>
<body>

<section class="grid_10">
    <div class="block-content no-padding">
        <h1><spring:message code="locationSetup.title"/></h1>

        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a id="addNewLoc" href="javascript://">
                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="">
                        <spring:message code="unitSetup.addLocation"/>
                    </a>
                </li>
            </ul>
        </div>
        <table class="table full-width">
            <thead>
            <tr>
                <th scope="col"><spring:message code="location.name"/></th>
                <th scope="col"><spring:message code="location.comments"/></th>
                <th scope="col"><spring:message code="global.active"/></th>
                <th scope="col" class="table-actions"><spring:message code="global.actions"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${locationList}" var="loc" varStatus="index">
                <tr>
                    <td>
                        <c:out value="${loc.name}"/>
                    </td>
                    <td>
                        <c:out value="${loc.comments}"/>
                    </td>
                    <td>
                        <c:out value="${loc.active}"/>
                    </td>
                    <td class="table-actions">
                        <a id="edit_loc_${loc.id}" href="javascript://">
                            <img alt="edit" src="<c:url value="/images/theme/icons/fugue/pencil.png"/>"/>
                        </a>
                        <a id="del_loc_${loc.id}" href="javascript://">
                            <img alt="remove" src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>"/>
                        </a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</section>

<div id="addNewLocDlg"></div>

</body>