<%--@elvariable id="schedule" type="net.grouplink.ehelpdesk.domain.RecurrenceSchedule"--%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>

<div id="templateMasterRecurrenceEdit" style="padding: 20px">
<form:form modelAttribute="schedule" id="recurrenceScheduleForm" action="" method="post" dojotype="dijit.form.Form" class="form">
<spring:hasBindErrors name="schedule">
    <p class="message error no-margin">
        <form:errors path="*"/>
    </p>
</spring:hasBindErrors>

<input type="hidden" name="ticketTemplateMasterId" value="${schedule.ticketTemplateMaster.id}"/>
<input type="hidden" name="recurrenceScheduleId" value="${schedule.id}"/>

<fieldset>
    <legend><spring:message code="recurrence.recurrencePattern"/></legend>

<table>
<tr>
<td>
    <table>
        <spring:bind path="schedule.pattern">
            <tr>
                <td>
                    <input type="radio" name="${status.expression}" id="patternDaily"
                           onclick="showDailyOptions();" value="1" dojotype="dijit.form.RadioButton"
                            <c:if test="${status.value eq 1}">
                                checked="checked"
                            </c:if>
                            />
                    <label for="patternDaily" class="inline"><spring:message code="recurrence.daily"/></label>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="radio" name="${status.expression}" id="patternWeekly"
                           onclick="showWeeklyOptions();" value="2" dojotype="dijit.form.RadioButton"
                            <c:if test="${status.value eq 2}">
                                checked="checked"
                            </c:if>
                            />
                    <label for="patternWeekly" class="inline"><spring:message code="recurrence.weekly"/></label>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="radio" name="${status.expression}" id="patternMonthly"
                           onclick="showMonthlyOptions();" value="3" dojotype="dijit.form.RadioButton"
                            <c:if test="${status.value eq 3}">
                                checked="checked"
                            </c:if>
                            />
                    <label for="patternMonthly" class="inline"><spring:message code="recurrence.monthly"/></label>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="radio" name="${status.expression}" id="patternYearly"
                           onclick="showYearlyOptions();" value="4" dojotype="dijit.form.RadioButton"
                            <c:if test="${status.value eq 4}">
                                checked="checked"
                            </c:if>
                            />
                    <label for="patternYearly" class="inline"><spring:message code="recurrence.yearly"/></label>
                </td>
            </tr>
        </spring:bind>

    </table>
</td>
<td valign="top" style="border: 1px solid lightgray; padding:5px; height:100%">
<div >
<div id="dailyOptions" <c:if test="${schedule.pattern != 1}">style="display:none;"</c:if>>
    <spring:bind path="schedule.dailyInterval">
        <spring:message code="recurrence.recurEvery"/>: <input name="${status.expression}" value="${status.value}"
                           dojotype="dijit.form.NumberSpinner"
                           constraints="{min:1,max:365,places:0}"
                           style="width:70px"/>
        <spring:message code="recurrence.days"/>
    </spring:bind>
</div>

<div id="weeklyOptions" <c:if test="${schedule.pattern != 2}">style="display:none;"</c:if>>
    <spring:bind path="schedule.weeklyInterval">
        <spring:message code="recurrence.recurEvery"/>: <input name="${status.expression}" value="${status.value}"
                            dojotype="dijit.form.NumberSpinner"
                            constraints="{min:1,max:365,places:0}"
                            style="width:70px"/>
        <spring:message code="recurrence.weekson"/>
    </spring:bind>
    <table>
        <tr>
            <td>
                <form:checkbox id="onSunday" path="onSunday"/>
                <form:label path="onSunday" cssClass="inline"><spring:message code="recurrence.sunday"/></form:label>
            </td>
            <td>
                <form:checkbox path="onMonday" id="onMonday"/>
                <form:label path="onMonday" cssClass="inline"><spring:message code="recurrence.monday"/></form:label>
            </td>
            <td>
                <form:checkbox path="onTuesday" id="onTuesday"/>
                <form:label path="onTuesday" cssClass="inline"><spring:message code="recurrence.tuesday"/></form:label>
            </td>
            <td>
                <form:checkbox path="onWednesday" id="onWednesday"/>
                <form:label path="onWednesday" cssClass="inline"><spring:message code="recurrence.wednesday"/></form:label>
            </td>
        </tr>
        <tr>
            <td>
                <form:checkbox path="onThursday" id="onThursday"/>
                <form:label path="onThursday" cssClass="inline"><spring:message code="recurrence.thursday"/></form:label>
            </td>
            <td>
                <form:checkbox path="onFriday" id="onFriday"/>
                <form:label path="onFriday" cssClass="inline"><spring:message code="recurrence.friday"/></form:label>
            </td>
            <td>
                <form:checkbox path="onSaturday" id="onSaturday"/>
                <form:label path="onSaturday" cssClass="inline"><spring:message code="recurrence.saturday"/></form:label>
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
</div>

<div id="monthlyOptions" <c:if test="${schedule.pattern != 3}">style="display:none;"</c:if>>
    <table>
        <tr>
            <td>
                <spring:bind path="schedule.monthlySchedule">
                    <input type="radio" id="monthlyScheduleDayOfMonth" name="${status.expression}" value="1"
                           dojotype="dijit.form.RadioButton"
                            <c:if test="${status.value eq 1}">
                                checked="checked"
                            </c:if>
                            />
                    <label for="monthlyScheduleDayOfMonth" class="inline"><spring:message code="recurrence.day"/></label>
                </spring:bind>

                <spring:bind path="schedule.monthlyDayOfMonth">
                    <input name="${status.expression}" value="${status.value}"
                           dojotype="dijit.form.NumberSpinner"
                           constraints="{min:1,max:31,places:0}"
                           style="width:70px"/>
                </spring:bind>

                <spring:message code="recurrence.ofEvery"/>
                <spring:bind path="schedule.monthlyDayOfMonthOccurrence">
                    <input name="${status.expression}" value="${status.value}"
                           dojotype="dijit.form.NumberSpinner"
                           constraints="{min:1,max:50,places:0}"
                           style="width:70px"/>
                </spring:bind>
                <spring:message code="recurrence.months"/>
            </td>
        </tr>
        <tr>
            <td>
                <spring:bind path="schedule.monthlySchedule">
                    <input type="radio" id="monthlyScheduleDayOfWeek" name="${status.expression}" value="2"
                           dojotype="dijit.form.RadioButton"
                            <c:if test="${status.value eq 2}">
                                checked="checked"
                            </c:if>
                            />
                    <label for="monthlyScheduleDayOfWeek" class="inline"><spring:message code="recurrence.the"/></label>
                </spring:bind>

                <spring:bind path="schedule.monthlyDayOfWeekInterval">
                    <select name="${status.expression}" value="${status.value}" dojotype="dijit.form.FilteringSelect">
                        <option value="1"><spring:message code="recurrence.first"/></option>
                        <option value="2"><spring:message code="recurrence.second"/></option>
                        <option value="3"><spring:message code="recurrence.third"/></option>
                        <option value="4"><spring:message code="recurrence.fourth"/></option>
                        <option value="5"><spring:message code="recurrence.fifth"/></option>
                    </select>
                </spring:bind>

                <spring:bind path="schedule.monthlyDayOfWeekDay">
                    <select name="${status.expression}" value="${status.value}" dojotype="dijit.form.FilteringSelect">
                        <option value="1"><spring:message code="recurrence.sunday"/></option>
                        <option value="2"><spring:message code="recurrence.monday"/></option>
                        <option value="3"><spring:message code="recurrence.tuesday"/></option>
                        <option value="4"><spring:message code="recurrence.wednesday"/></option>
                        <option value="5"><spring:message code="recurrence.thursday"/></option>
                        <option value="6"><spring:message code="recurrence.friday"/></option>
                        <option value="7"><spring:message code="recurrence.saturday"/></option>
                    </select>
                </spring:bind>

                <spring:message code="recurrence.ofEvery"/>
                <spring:bind path="schedule.monthlyDayOfWeekOccurrence">
                    <input name="${status.expression}" value="${status.value}"
                           dojotype="dijit.form.NumberSpinner"
                           constraints="{min:1,max:50,places:0}"
                           style="width:70px"/>
                </spring:bind>
                <spring:message code="recurrence.months"/>
            </td>
        </tr>
    </table>
</div>

<div id="yearlyOptions" <c:if test="${schedule.pattern != 4}">style="display:none;"</c:if>>
    <table>
        <tr>
            <td>

                <spring:bind path="schedule.yearlySchedule">
                    <input type="radio" id="yearlyScheduleDayOfYear" name="${status.expression}" value="1"
                           dojotype="dijit.form.RadioButton"
                            <c:if test="${status.value eq 1}">
                                checked="checked"
                            </c:if>
                            />
                    <label for="yearlyScheduleDayOfYear" class="inline"><spring:message code="recurrence.day"/></label>
                </spring:bind>

                <spring:bind path="schedule.yearlyDayOfYearDay">
                    <input name="${status.expression}" value="${status.value}"
                           dojotype="dijit.form.NumberSpinner"
                           constraints="{min:1,max:31,places:0}"
                           style="width:70px"/>
                </spring:bind>

                <spring:message code="recurrence.of"/>
                <spring:bind path="schedule.yearlyDayOfYearMonth">
                    <select name="${status.expression}" value="${status.value}" dojotype="dijit.form.FilteringSelect">
                        <option value="1"><spring:message code="recurrence.january"/></option>
                        <option value="2"><spring:message code="recurrence.february"/></option>
                        <option value="3"><spring:message code="recurrence.march"/></option>
                        <option value="4"><spring:message code="recurrence.april"/></option>
                        <option value="5"><spring:message code="recurrence.may"/></option>
                        <option value="6"><spring:message code="recurrence.june"/></option>
                        <option value="7"><spring:message code="recurrence.july"/></option>
                        <option value="8"><spring:message code="recurrence.august"/></option>
                        <option value="9"><spring:message code="recurrence.september"/></option>
                        <option value="10"><spring:message code="recurrence.october"/></option>
                        <option value="11"><spring:message code="recurrence.november"/></option>
                        <option value="12"><spring:message code="recurrence.december"/></option>
                    </select>
                </spring:bind>
            </td>
        </tr>
        <tr>
            <td>
                <spring:bind path="schedule.yearlySchedule">
                    <input type="radio" id="yearlyScheduleDayOfMonth" name="${status.expression}" value="2"
                           dojotype="dijit.form.RadioButton"
                            <c:if test="${status.value eq 2}">
                                checked="checked"
                            </c:if>
                            />
                    <label for="yearlyScheduleDayOfMonth" class="inline"><spring:message code="recurrence.the"/></label>
                </spring:bind>

                <spring:bind path="schedule.yearlyDayOfMonthInterval">
                    <select name="${status.expression}" value="${status.value}" dojotype="dijit.form.FilteringSelect">
                        <option value="1"><spring:message code="recurrence.first"/></option>
                        <option value="2"><spring:message code="recurrence.second"/></option>
                        <option value="3"><spring:message code="recurrence.third"/></option>
                        <option value="4"><spring:message code="recurrence.fourth"/></option>
                        <option value="5"><spring:message code="recurrence.fifth"/></option>
                    </select>
                </spring:bind>

                <spring:bind path="schedule.yearlyDayOfMonthDay">
                    <select name="${status.expression}" value="${status.value}" dojotype="dijit.form.FilteringSelect">
                        <option value="1"><spring:message code="recurrence.sunday"/></option>
                        <option value="2"><spring:message code="recurrence.monday"/></option>
                        <option value="3"><spring:message code="recurrence.tuesday"/></option>
                        <option value="4"><spring:message code="recurrence.wednesday"/></option>
                        <option value="5"><spring:message code="recurrence.thursday"/></option>
                        <option value="6"><spring:message code="recurrence.friday"/></option>
                        <option value="7"><spring:message code="recurrence.saturday"/></option>
                    </select>
                </spring:bind>

                <spring:message code="recurrence.of"/>
                <spring:bind path="schedule.yearlyDayOfMonthMonth">
                    <select name="${status.expression}" value="${status.value}" dojotype="dijit.form.FilteringSelect">
                        <option value="1"><spring:message code="recurrence.january"/></option>
                        <option value="2"><spring:message code="recurrence.february"/></option>
                        <option value="3"><spring:message code="recurrence.march"/></option>
                        <option value="4"><spring:message code="recurrence.april"/></option>
                        <option value="5"><spring:message code="recurrence.may"/></option>
                        <option value="6"><spring:message code="recurrence.june"/></option>
                        <option value="7"><spring:message code="recurrence.july"/></option>
                        <option value="8"><spring:message code="recurrence.august"/></option>
                        <option value="9"><spring:message code="recurrence.september"/></option>
                        <option value="10"><spring:message code="recurrence.october"/></option>
                        <option value="11"><spring:message code="recurrence.november"/></option>
                        <option value="12"><spring:message code="recurrence.december"/></option>
                    </select>
                </spring:bind>
            </td>
        </tr>
    </table>

</div>

</div>

</td>
</tr>
</table>
</fieldset>

<br>
<fieldset>
    <legend><spring:message code="recurrence.rangeOfRecurrence"/></legend>
<table>
    <tr>
        <td>
            <spring:bind path="schedule.rangeStartDate">
                <spring:message code="recurrence.start"/>: <input type="text" dojotype="dijit.form.DateTextBox" 
                       name="${status.expression}" value="${status.value}"/>
            </spring:bind>
        </td>
        <td>
            <spring:bind path="schedule.rangeEnd">
                <input type="radio" id="rangeEndAfter" name="${status.expression}" value="1" dojotype="dijit.form.RadioButton"
                        <c:if test="${status.value eq 1}">
                            checked="checked"
                        </c:if>
                        /> <label for="rangeEndAfter" class="inline"><spring:message code="recurrence.endAfter"/>:</label>
            </spring:bind>
        </td>
        <td>
            <spring:bind path="schedule.rangeEndAfterOccurence">
                <input name="${status.expression}" value="${status.value}"
                       dojotype="dijit.form.NumberSpinner"
                       constraints="{min:1,max:999,places:0}"
                       style="width:70px"/> <spring:message code="recurrence.occurrences"/>
            </spring:bind>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>
            <spring:bind path="schedule.rangeEnd">
                <input type="radio" id="rangeEndBy" name="${status.expression}" value="2" dojotype="dijit.form.RadioButton"
                        <c:if test="${status.value eq 2}">
                            checked="checked"
                        </c:if>
                        /> <label for="rangeEndBy" class="inline"><spring:message code="recurrence.endBy"/>:</label>
            </spring:bind>
        </td>
        <td>
            <spring:bind path="schedule.rangeEndByDate">
                <input type="text" dojotype="dijit.form.DateTextBox" name="${status.expression}" value="${status.value}"/>
            </spring:bind>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td colspan="2">
            <spring:bind path="schedule.rangeEnd">
                <input type="radio" id="rangeEndNever" name="${status.expression}" value="3" dojotype="dijit.form.RadioButton"
                        <c:if test="${status.value eq 3}">
                            checked="checked"
                        </c:if>
                        /> <label for="rangeEndNever" class="inline"><spring:message code="recurrence.endNever"/></label>
            </spring:bind>
        </td>
    </tr>
</table>
</fieldset>

<button type="submit" id="saveRecurrenceScheduleButton" onclick="saveRecurrenceScheduleForm();"><spring:message code="global.save"/></button>

<div id="response" style="color:red;"></div>
</form:form>

</div>
