<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="dashboards" type="java.util.List"--%>
<%--@elvariable id="showHeader" type="java.lang.Boolean"--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
    <title>
        <spring:message code="ticketTemplate.title"/>
    </title>
    <spring:theme code="css" var="cssfile"/>
    <c:if test="${not empty cssfile}">
        <link rel="stylesheet" href="<c:url value="${cssfile}"/>" type="text/css">
    </c:if>

</head>
<body>

<div id="bodyContent" style="padding: 10px;">
    <strong><spring:message code="ticketTemplate.title"/></strong>
    <br/><br/>

    <p>
        Ticket Templates are not available in the free edition of <i>eReferrals</i>&reg;. To upgrade to eReferrals&trade;'s
        Enterprise edition, or for more information, please contact a Sales Representative at
        <a href="mailto:sales@grouplink.net">sales@grouplink.net</a> or 801-335-0702.
    </p>

    <p>
        Ticket Templates are made to help you stay one step ahead of your business processes and routine tasks. Use the
        schedule calendar utility to schedule routine tasks or business processes to run daily, weekly, monthly, or
        yearly. The utility can also be used to schedule tasks to run on a specific date as well as reoccur for a
        specific number of times and during an identified time range. Each of these tasks, which can be assigned to a
        predetermined technician as well as span multiple departments, is a separate ticket with step by step
        instructions for completing business processes. Plus, eReferrals gives you the ability to tie assets to these tickets,
        track any needed repairs or comments in the history record of the ticket and attach images, pictures or files
        directly to the ticket. These features make eReferrals the ideal service desk solution for business
        process automation and to use as preventative maintenance software or facilities management software.
    </p>

    <p>To view the capability of eReferrals' Ticket Templates to automate your business processes, watch the video below.</p>

    <p>
        <object width="500" height="405">
            <param name="movie"
                   value="http://www.youtube.com/v/XYcegKWqi8Q&amp;hl=en_US&amp;fs=1?rel=0&amp;border=1"/>
            <param name="allowFullScreen" value="true"/>
            <param name="allowscriptaccess" value="always"/>
            <embed src="http://www.youtube.com/v/XYcegKWqi8Q&amp;hl=en_US&amp;fs=1?rel=0&amp;border=1"
                   type="application/x-shockwave-flash" allowscriptaccess="always"
                   allowfullscreen="true" width="500" height="405"></embed>
        </object>
    </p>

    <p>To view the capability of eReferrals' Ticket Templates to automate your preventative maintenance program, watch the
        video below.</p>

    <p>

        <object width="500" height="405">
            <param name="movie"
                   value="http://www.youtube.com/v/rM0ybPDHDxY&amp;hl=en_US&amp;fs=1?rel=0&amp;border=1"/>
            <param name="allowFullScreen" value="true"/>
            <param name="allowscriptaccess" value="always"/>
            <embed src="http://www.youtube.com/v/rM0ybPDHDxY&amp;hl=en_US&amp;fs=1?rel=0&amp;border=1"
                   type="application/x-shockwave-flash" allowscriptaccess="always"
                   allowfullscreen="true" width="500" height="405"></embed>
        </object>
    </p>

</div>
</body>
</html>
