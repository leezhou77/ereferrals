<%--@elvariable id="user" type="net.grouplink.ehelpdesk.domain.User"--%>
<%--@elvariable id="userOnly" type="java.lang.Boolean"--%>
<%--@elvariable id="countMap" type="java.util.HashMap<Integer, Integer>"--%>
<%--@elvariable id="myTicketTabs" type="java.util.List<MyTicketTab>"--%>
<%--@elvariable id="rc" type="org.springframework.web.servlet.support.RequestContext"--%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>

<head>

<title>
    <spring:message code="header.myTickets"/>
</title>

<link rel="stylesheet" href="<c:url value="/css/myTickets2.css"/>" type="text/css">
<script type="text/javascript" src="<c:url value="/js/cookies.js"/>"></script>
<script type="text/javascript" src="<c:url value="/dwr/interface/MyTicketTabsService.js"/>"></script>
<script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>

<%@ include file="/WEB-INF/jsp/ehd-tf/ticketListFragment.jsp" %>

<script type="text/javascript">

    function go(ticketType) {
        var url = "<c:url value="/tickets/new"/>";
        url += "?ticketType=" + ticketType;
    <authz:authorize ifNotGranted="ROLE_MANAGER, ROLE_TECHNICIAN" >
        url += '&contactId=<c:out value="${user.id}"/>';
    </authz:authorize>
        url += "&nt=true";
        url += "&uo=" + '<c:out value="${userOnly}"/>';
        openWindow(url, '_blank', '800px', '650px');
    }

    //This function is only user by ticketList.jsp forceRefresh() when browser is IE only
    function refreshGroup(winName, url) {
        frames[winName].location.href = url;
    }

    function updateTicketCounter(urgId, tabName, count) {
        var name = 'divTab_' + urgId;
        if (urgId == 0) {
            name = 'divTab_owned';
        }

        var tab = document.getElementById(name);
        tab.innerHTML = tabName + ' <strong>(' + count + ')</strong>';
    }

    //Function used only new tickets gets created from create new ticket button
    function forceRefresh() {
        refreshTickets();
    }

    function exportList(format, selectedOnly) {
        // get the ticketfilterid
        var selectedTabId = dijit.byId('maintc').selectedChildWidget.id;
        var mtTabId = selectedTabId.substring("4");

        var selectedTidsFromGrid;
        if (selectedOnly) {
            selectedTidsFromGrid = getSelectedTidsFromGrid(selectedTabId + "_grid");
        }

        var callMetaData = {
            callback: exportCallBack,
            arg: {format: format, selectedOnly: selectedOnly, selectedTids: selectedTidsFromGrid},
            errorHandler: outputError
        };

        MyTicketTabsService.getTicketFilterIdFromTabId(mtTabId, callMetaData);
    }

    function outputError(e, ex) {
        console.debug("Error: " + ex);
    }

    function exportCallBack(tfId, args) {
        var yerl;
        var format = args.format;
        var selectedOnly = args.selectedOnly;
        var selectedTids = args.selectedTids;
        if (format == 'pdf') {
            yerl = "<c:url value="/tf/ticketFilterList.pdf"/>?tfId=" + tfId;
        } else if (format == 'excel') {
            yerl = "<c:url value="/tf/ticketFilterList.xls"/>?tfId=" + tfId;
        }

        if (selectedOnly) {
            yerl += "&selectedTids=";
            for (var i=0; i<selectedTids.length; i++) {
                if (i > 0) yerl += ",";
                yerl += selectedTids[i];
            }
        }

        window.open(yerl);
    }

    function goToTT() {
        location.href = '<c:url value="/config/templateMasterList.glml"/>';
    }

    function launchTT(tmId) {
        location.href = '<c:url value="/ticketTemplates/ticketTemplateLaunch.glml"/>?tmId=' + tmId;
    }

    function refreshTickets() {
        // only refresh the currently selected tab
        var selectedTab = dijit.byId('maintc').selectedChildWidget;
        var tabId = selectedTab.id;
        var gridId = tabId + "_grid";
        var grid = dijit.byId(gridId);
        // calling setStore on the grid causes the grid to refresh
        grid.setStore(grid.store);
    }

    function turnOffOutOfOffice() {
        dojo.xhrGet({
            url: '<c:url value="/config/notification/turnOffOutOfOffice.glml" ><c:param name="userId" value="${sessionScope.User.id}" /></c:url>&random=' + Math.random(),
            handleAs: "text",
            preventCache: true,
            load: function() {
                document.getElementById('outOfOfficeAssignment').style.display = 'none';
            },
            error: function(response, ioArgs) {
                alert("XHR error (myTickets:turnOffOutOfOffice) HTTP status code: " + ioArgs.xhr.status);
                return response;
            }
        });
    }
</script>


<script type="text/javascript">
    var viewedTabs = {};
    <c:forEach var="mtt" items="${myTicketTabs}">
    viewedTabs['tab_<c:out value="${mtt.id}"/>'] = 0;
    </c:forEach>

    function initTabs() {
        var selectChildCookie = getSelectChildCookieValue();

        var tc = new dijit.layout.TabContainer({
            id: "maintc",
            style: "width: 100%; height: 100%;"
        });

        // add check tickets

        var firstTabId;
        var firstTicketFilterId;
        var selectedInitialTab = false;
    <c:forEach var="myTicketTab" items="${myTicketTabs}" varStatus="varStatus">
        var tabId = "tab_${myTicketTab.id}";
    <c:if test="${varStatus.first}">
        firstTabId = tabId;
        firstTicketFilterId = "${myTicketTab.ticketFilter.id}";
    </c:if>

        var editTFHtml = '<a href="<c:url value="/tf/ticketFilterEdit.glml"><c:param name="tfId" value="${myTicketTab.ticketFilter.id}"/></c:url>"';
        editTFHtml += 'title="<spring:message code="ticketFilter.action.editFilter"/>">';
        editTFHtml += '<img src="<c:url value="/images/edit.gif"/>" alt="" style="vertical-align:baseline;"> </a>';

        var mttCp = new dijit.layout.ContentPane({
            id: tabId,
            title: '<c:out value="${myTicketTab.name}"/> <span id="' + tabId + '_badge" class="badge"><c:out value="${countMap[myTicketTab.ticketFilter.id]}"/></span> ' + editTFHtml,
            ticketFilterId: "${myTicketTab.ticketFilter.id}",
            style: "width:100%;height:100%"
        });

        if (selectChildCookie == tabId) {
            mttCp.attr('selected', true);
            createGrid(tabId, ${myTicketTab.ticketFilter.id});
            viewedTabs[tabId] = 1;
            selectedInitialTab = true;
        }
        tc.addChild(mttCp);
    </c:forEach>

        if (!selectedInitialTab) {
            createGrid(firstTabId, firstTicketFilterId);
            viewedTabs[firstTabId] = 1;
        }

        dojo.connect(tc, 'selectChild', handleSelectChild);

        dojo.byId("mainTabContainer").appendChild(tc.domNode);
        tc.startup();
        tc.resize();
    }

    function handleSelectChild(selectedTab) {
        if (selectedTab) {
            if (viewedTabs[selectedTab.id] == 0) {
                createGrid(selectedTab.id, selectedTab.ticketFilterId);
                viewedTabs[selectedTab.id] = 1;
            }

            setCookie('selectChild', selectedTab.id);
        }
    }

    function getSelectChildCookieValue() {
        return getCookie('selectChild');
    }

    function resizeGrids() {
        var windowHeight = $(window).height();
        var tabc = dijit.byId("maintc");
        $("#mainTabContainer").height(windowHeight - 370);
        tabc.resize();
    }

    function initPage() {
        dojo.addOnLoad(function(){
            try {
                initTabs();
                resizeGrids();
                dojo.connect(window, "onresize", resizeGrids);
            } catch (err) {
                console.error("error initializing page: " + err);
            }

            hideLoader();
        });
    }

    dojo.addOnLoad(initPage);

</script>

</head>

<body>

<div id="preloader"></div>

<section class="grid_12">
    <div class="block-content">
        <div class="h1 with-menu">
            <h1><spring:message code="header.myTickets"/></h1>
            <div class="menu">
                <img src="<c:url value="/images/theme/menu-open-arrow.png"/>">
                <ul>
                    <li class="icon_export">
                        <a href="javascript://"><spring:message code="ticketList.export"/></a>
                        <ul>
                            <li>
                                <a href="javascript://"><spring:message code="global.all" text="All"/></a>
                                <ul>
                                    <li class="icon_doc_pdf">
                                        <a href="javascript://" onclick="exportList('pdf');"><spring:message code="ticketList.exportPDF"/></a>
                                    </li>
                                    <li class="icon_doc_excel">
                                        <a href="javascript://" onclick="exportList('excel');"><spring:message code="ticketList.exportExcel"/></a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript://"><spring:message code="global.selected" text="Selected"/></a>
                                <ul>
                                    <li class="icon_doc_pdf">
                                        <a href="javascript://" onclick="exportList('pdf', true);"><spring:message code="ticketList.exportPDF"/></a>
                                    </li>
                                    <li class="icon_doc_excel">
                                        <a href="javascript://" onclick="exportList('excel', true);"><spring:message code="ticketList.exportExcel"/></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <authz:authorize ifAnyGranted="ROLE_ADMINISTRATOR, ROLE_MANAGER, ROLE_TECHNICIAN">
                    <li class="icon_user_silhouette_q">
                        <a href="<c:url value="/findUser"/>"><spring:message code="user.find"/></a>
                    </li>
                    </authz:authorize>
                    <li class="icon_doc_code">
                        <a href="javascript://"><spring:message code="ticketTemplate.title"/></a>
                        <ul>
                            <c:forEach var="entry" items="${groupTicketTemplateMap}">
                                <c:set var="group" value="${entry.key}"/>
                                <c:set var="tmList" value="${entry.value}"/>

                                <c:if test="${fn:length(tmList)>0}">
                                    <li>
                                        <a href="javascript://"><c:out value="${group.name}"/></a>
                                        <ul>
                                            <c:forEach var="master" items="${tmList}">
                                                <li>
                                                    <a href="javascript://" onclick="launchTT(${master.id});"><c:out value="${master.name}"/></a>
                                                </li>
                                            </c:forEach>
                                        </ul>
                                    </li>
                                </c:if>
                            </c:forEach>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a href="javascript://" onclick="refreshTickets();">
                        <img src="<c:url value="/images/theme/icons/fugue/arrow-circle.png"/>" alt="" title="<spring:message code="ticket.check"/>">
                    </a>
                </li>
                <li class="sep"></li>
                <li>
                    <a href="<c:url value="/mytickets-config.glml"/>">
                        <img src="<c:url value="/images/theme/icons/fugue/pencil.png"/>" alt="" title="<spring:message code="myTicketTabs.customize"/>">
                    </a>
                </li>
                </li>
                <c:if test="${sessionScope.hasBackUp}">
                    <li id="out_of_office">
                        <span id="outOfOfficeAssignment">
                            <img alt="" align="middle" style="cursor: pointer;"
                                 src="<c:url value="/images/info_16.png" />"/>
                            <spring:message code="ticketSettings.outOfOfficeOn"
                                            arguments="<a href=\"javascript://\" style=\"color:red; text-decoration:underline\" onclick=\"turnOffOutOfOffice()\" >, </a>"/>
                        </span>
                    </li>
                </c:if>
            </ul>
        </div>

        <div id="mainTabContainer"></div>

    </div>
</section>
</body>
