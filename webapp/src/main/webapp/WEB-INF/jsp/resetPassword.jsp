<%@ include file="/WEB-INF/jsp/include.jsp"%>
<head>
    <title><spring:message code="resetPassword.title" />
    </title>

    <script type="text/javascript">
    dojo.ready(function() {
        var email = dojo.byId('email');
        if(email != null) {
            email.focus();
        }

        var returnBtn = dojo.byId('return');
        if(returnBtn != null) {
            dojo.connect(returnBtn, "onclick", function(evt) {
                window.location = "<c:url value="/"/>";
            });
        }
    });

    function disableSubmit() {
        dojo.byId('submit').disabled = true;
        return true;
    }
</script>
</head>
<body>
<c:if test="${messageSent != null or adminReset != null}">
    <section id="message">
        <div class="block-content no-title dark-bg">
            <c:choose>
                <c:when test="${messageSent}">
                    <p class="mini-infos">
                        <spring:message code="resetPassword.confirm"/>
                    </p>
                </c:when>
            </c:choose>
        </div>
    </section>
</c:if>

<section class="login-block">
    <div class="block-content no-title">
        <img class="logo" src="<c:url value='/images/logo.png'/>" alt="" />
        <div class="message no-margin"><spring:message code="resetPassword.instructions"/></div>
        <form class="form" action="" method="post" onsubmit="disableSubmit();">
            <spring:bind path="resetPassword.email">
                <c:if test="${not empty status.errorMessage}">
                    <p class="message error no-margin">
                        ${status.errorMessage}
                    </p>
                </c:if>
            </spring:bind>
            <p class="inline-small-label">
                <label for="email"><span class="big"><spring:message code="user.email"/></span></label>
                <spring:bind path="resetPassword.email">
                    <input class="full-width" type="text" name="${status.expression}" id="email" value=""/>
                </spring:bind>
            </p>
            <c:choose>
                <c:when test="${messageSent or adminReset}">
                    <button class="float-right" id="return" type="button"><spring:message code="resetPassword.returntologin" /></button>
                </c:when>
                <c:otherwise>
                        <button class="float-right" type="submit" name="submit" id="submit">
                            <spring:message code="resetPassword.resetPassword"/>
                        </button>
                </c:otherwise>
            </c:choose>
            <div class="clearfix"></div>
        </form>
        </div>
    </section>
</body>