<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>
    <title>
        <spring:message code="kb.knowledgeBaseArticle" />
    </title>
</head>

<body>
<section class="grid_11">
    <div class="block-content form">
        <h1><spring:message code="kb.knowledgeBaseArticle"/> <c:out value="${kb.id}"/></h1>

        <ehd:authorizeGroup permission="group.kbManage" group="${kb.group}">
            <div class="block-controls">
                <ul class="controls-buttons">
                    <li>
                        <a href="<c:url value="/kbManagement/kbEdit.glml" ><c:param name="kbId" value="${kb.id}" /></c:url>" >
                            <img src="<c:url value="/images/theme/icons/fugue/pencil.png"/>">
                            <spring:message code="kb.editKb" />
                        </a>
                    </li>
                    <c:choose>
                        <c:when test="${kb.isPrivate}">
                        <li>
                            <a href="<c:url value="/kbManagement/kbMakePrivate.glml" ><c:param name="kbId" value="${kb.id}" /><c:param name="type" value="public" /></c:url>" >
                                <img src="<c:url value="/images/theme/icons/fugue/globe-network.png"/>">
                                <spring:message code="kb.makePublic" />
                            </a>
                        </li>
                        </c:when>
                        <c:otherwise>
                        <li>
                            <a href="<c:url value="/kbManagement/kbMakePrivate.glml" ><c:param name="kbId" value="${kb.id}" /><c:param name="type" value="private" /></c:url>" >
                                <img src="<c:url value="/images/theme/icons/fugue/lock--arrow.png"/>">
                                <spring:message code="kb.makePrivate" />
                            </a>
                        </li>
                        </c:otherwise>
                    </c:choose>
                    <li>
                        <a href="<c:url value="/kbManagement/kbResetCounter.glml" ><c:param name="kbId" value="${kb.id}" /></c:url>">
                            <img src="<c:url value="/images/theme/icons/fugue/counter-reset.png"/>">
                            <spring:message code="kb.resetCounter" />
                        </a>
                    </li>
                    <li>
                        <a href="<c:url value="/kbManagement/kbRemove.glml" ><c:param name="kbId" value="${kb.id}" /><c:param name="showHeader" value="${sessionScope.showHeader}" /></c:url>" >
                            <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>">
                            <spring:message code="kb.removeKb" />
                        </a>
                    </li>
                </ul>
            </div>
        </ehd:authorizeGroup>

        <div class="with-margin">
            <h3><c:out value="${kb.subject}" /></h3>
            <hr>
            <div><c:out escapeXml="false" value="${kb.problem}" /></div>
            <hr>
            <div><c:out escapeXml="false" value="${kb.resolution}" /></div>
        </div>
    </div>
    <br>
    <div class="block-content">
        <div class="columns">
            <div class="colx2-left">
                <form action="<c:url value="/kb/kbSend.glml" />">
                    <fieldset>
                        <legend><spring:message code="user.email"/></legend>
                        <p class="small-margin">
                            <select id="emailType" name="emailType" >
                                <option value="1" ><spring:message code="kb.wholePage"/></option>
                                <option value="2" ><spring:message code="kb.articleLink"/></option>
                            </select>
                            <spring:message code="collaboration.appointment.to"/>
                            <input size="40" type="text" name="email" />
                            <input type="hidden" name="kbId" value="<c:out value="${kb.id}" />" />
                        </p>
                        <p>
                            <input class="button" type="submit" name="submit" value="<spring:message code="kb.button.send" />" />
                        </p>
                    </fieldset>
                </form>
            </div>
            <div class="colx2-right">
                <c:if test="${not empty kb.attachments}">
                    <label><spring:message code="ticket.attachments"/></label>
                    <ul class="mini-blocks-list">
                    <c:forEach items="${kb.attachments}" var="attach">
                        <li>
                            <authz:authorize ifAnyGranted="ROLE_ADMINISTRATOR, ROLE_MANAGER, ROLE_TECHNICIAN">
                                <a href="<c:url value="/ticket/attachment/handleDownload.glml" ><c:param name="aid" value="${attach.id}" /></c:url>" ><c:out value="${attach.fileName}" /></a>
                            </authz:authorize>
                            <authz:authorize ifNotGranted="ROLE_ADMINISTRATOR, ROLE_MANAGER, ROLE_TECHNICIAN">
                                <a href="<c:url value="/ticket/attachment/nonPrivateHandleDownload.glml" ><c:param name="aid" value="${attach.id}" /></c:url>" ><c:out value="${attach.fileName}" /></a>
                            </authz:authorize>
                        </li>
                    </c:forEach>
                    </ul>
                </c:if>
            </div>
        </div>

        <div>
            <ul class="simple-list horiz">
                <li><label><spring:message code="global.group" /></label> <c:out value="${kb.group.name}" /></li>
                <li><label><spring:message code="kb.lastModified" /></label> <fmt:formatDate value="${kb.date}" dateStyle="MEDIUM"/></li>
                <li><label><spring:message code="kb.type" /></label> <c:if test="${kb.isPrivate}" ><spring:message code="kb.private" /></c:if><c:if test="${not kb.isPrivate}" ><spring:message code="kb.public" /></c:if></li>
                <li><label><spring:message code="kb.timesViewed" /></label> <c:out value="${kb.timesViewed}" /></li>
            </ul>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
</body>

