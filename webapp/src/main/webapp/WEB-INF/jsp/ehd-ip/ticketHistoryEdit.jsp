<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="apptTypes" type="java.util.List<net.grouplink.ehelpdesk.domain.AppointmentType>"--%>
<%--@elvariable id="ticketHistory" type="net.grouplink.ehelpdesk.domain.TicketHistory"--%>
<%--@elvariable id="mttabId" type="java.lang.Integer"--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>

    <title><spring:message code="global.systemName"/></title>

    <meta content="yes" name="apple-mobile-web-app-capable"/>
    <meta content="index,follow" name="robots"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <link href="<c:url value="/images/eReferrals_logo.png"/>" rel="apple-touch-icon"/>
    <meta name="viewport" content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no"/>

    <link href="<c:url value="/css/iwk_style.css"/>" rel="stylesheet" type="text/css"/>
    <script src="<c:url value="/js/iwk_functions.js"/>" type="text/javascript"></script>

    <link rel="stylesheet" href="<c:url value="/js/spinningwheel/spinningwheel.css"/>" type="text/css" media="all"/>

    <script type="text/javascript" src="<c:url value="/js/spinningwheel/spinningwheel-min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/scripts.js"/>"></script>

    <%--Spinning Wheel JS--%>
    <script type="text/javascript">
        function iwkSelectHack(fld) {
            // hack for mucked up iwebkit library that was stealing the onchange event. I love web programming!
            var e = fld.getElementsByTagName("option");
            for (var f = 0; f < e.length; f++) {
                if (e[f].selected) {
                    document.getElementById("select" + fld.name).childNodes[0].nodeValue = e[f].childNodes[0].nodeValue;
                }
            }
        }
    </script>

    <script type="text/javascript">
        function openApptStartDate() {
            var now = new Date();
            var days = { };
            var years = { };
            var months = { 0: 'Jan', 1: 'Feb', 2: 'Mar', 3: 'Apr', 4: 'May', 5: 'Jun', 6: 'Jul', 7: 'Aug', 8: 'Sep', 9: 'Oct', 10: 'Nov', 11: 'Dec' };

            for (var i = 1; i < 32; i += 1) {
                days[i] = i;
            }

            for (i = now.getFullYear(); i < now.getFullYear() + 50; i++) {
                years[i] = i;
            }

            SpinningWheel.addSlot(years, 'right', now.getFullYear());
            SpinningWheel.addSlot(months, '', now.getMonth());
            SpinningWheel.addSlot(days, 'right', now.getDate());

            SpinningWheel.setCancelAction(function () {
                document.getElementById('thApptStartDate').innerHTML = "";
                document.getElementById('apptStartDate').value = "";
            });
            SpinningWheel.setDoneAction(function () {
                var results = SpinningWheel.getSelectedValues();
                var selDate = new Date();
                selDate.setFullYear(results.keys[0], results.keys[1], results.keys[2]);
                document.getElementById('thApptStartDate').innerHTML = results.values.join(' ');
                document.getElementById('apptStartDate').value = selDate.getFullYear()+ "-" + (selDate.getMonth() + 1) + "-" + selDate.getDate();
            });

            SpinningWheel.open();
        }


        function openApptStartTime() {
            var hours = {}; // 1-12
            var minutes = {0:'00', 1:"01", 2:"02", 3:"03", 4:"04", 5:"05", 6:"06", 7:"07", 8:"08", 9:"09"}; // 00-59
            var ampm = { 0:'am', 1:'pm' };

            for (var i = 1; i < 13; i += 1) {
                hours[i] = i;
            }

            for (i = 10; i < 60; i++) {
                minutes[i] = i;
            }

            var now = new Date();
            var hourValue = now.getHours(); // 0-23
            var ampmval = 0;

            if (hourValue == 0) {
                hourValue = 12;
            } else if (hourValue > 12) {
                hourValue = hourValue - 12;
                ampmval = 1;
            }

            SpinningWheel.addSlot(hours, 'right shrink', hourValue);
            SpinningWheel.addSlot(minutes, 'shrink', now.getMinutes());
            SpinningWheel.addSlot(ampm, 'right shrink', ampmval);

            SpinningWheel.setCancelAction(function () {
                document.getElementById('thApptStartTime').innerHTML = "";
                document.getElementById('apptStartTime').value = "";
            });
            SpinningWheel.setDoneAction(function () {
                var results = SpinningWheel.getSelectedValues();
                var resValue = results.values[0] + ":" + results.values[1] + " " + results.values[2];
                document.getElementById('thApptStartTime').innerHTML = resValue;
                var hour = results.values[0];
                // Convert to 24 hour time based on am pm
                if(results.values[2] == "pm") hour = hour+12;
                document.getElementById('apptStartTime').value = hour + ":" + results.values[1] + ":00";
            });

            SpinningWheel.open();
        }

        function openApptDurationAmount() {
            var durationAmount = {}; // 1-100
            var durationUnit = {0:'Minutes', 1:'Hours', 2:'Days'};

            for (var i = 1; i < 101; i ++) {
                durationAmount[i] = i;
            }

            SpinningWheel.addSlot(durationAmount, 'right shrink', 1);
            SpinningWheel.addSlot(durationUnit, 'shrink', 1);

            SpinningWheel.setCancelAction(function () {
                document.getElementById('durationDisplay').innerHTML = "";
                document.getElementById('durationAmount').value = "";
                document.getElementById('durationUnit').value = "";
            });
            SpinningWheel.setDoneAction(function () {
                var results = SpinningWheel.getSelectedValues();
                document.getElementById('durationDisplay').innerHTML = results.values.join(' ');
                document.getElementById('durationAmount').value = results.keys[0];
                document.getElementById('durationUnit').value = results.keys[1];
            });

            SpinningWheel.open();
        }
    </script>

</head>

<body>

<div id="topbar">
    <div id="leftnav">
        <a href="<c:url value="/ip/home.glml"/>">
            <img alt="home" src="<c:url value="/images/home.png"/>"/>
        </a>
        <a href="<c:url value="/ip/ticketEdit.glml"><c:param name="tid" value="${ticketHistory.ticket.id}"/><c:param name="mttabId" value="${mttabId}"/></c:url>">
            <spring:message code="ticket.number"/>: ${ticketHistory.ticket.id}
        </a>
    </div>
    <div id="rightnav">
        <a href="<c:url value="/logout.glml"/>">
            <spring:message code="header.signout"/>
        </a>
    </div>
</div>
<div id="content">

    <spring:bind path="ticketHistory.*">
        <c:if test="${not empty status.errorMessages}">
            <ul class="pageitem">
                <li class="textbox">
                    <c:forEach items="${status.errorMessages}" var="error">
                        <c:if test="${not empty error}">
                            <span style="color: red">* <c:out value="${error}"/></span><br/>
                        </c:if>
                    </c:forEach>
                </li>
            </ul>
        </c:if>
    </spring:bind>

    <span class="graytitle">
        <spring:message code="ticket.addCommentTitle" arguments="${ticketHistory.ticket.id}"/>
    </span>

    <form action="" method="post" name="listForm">

        <ul class="pageitem">
            <c:if test="${ticketHistory.commentType eq 1 or ticketHistory.commentType eq 2}">
                <li class="menu">
                    <span class="name1"><spring:message code="collaboration.appointment.from"/> :</span>
                    <span class="name2">
                        <c:out value="${sessionScope['User'].firstName}"/>
                        <c:out value="${sessionScope['User'].lastName}"/>
                    </span>
                </li>

                <li class="form">
                    <spring:bind path="ticketHistory.recipients">
                        <input placeholder="<spring:message code="collaboration.appointment.to"/>" type="text"
                               name="${status.expression}" id="${status.expression}"
                               value="${sessionScope['User'].email}"/>
                    </spring:bind>
                </li>
                <li class="form">
                    <spring:bind path="ticketHistory.cc">
                        <input placeholder="<spring:message code="mail.cc"/>" type="text"
                               name="${status.expression}" id="${status.expression}" value="${status.value}"/>
                    </spring:bind>
                </li>
                <li class="form">
                    <spring:bind path="ticketHistory.bc">
                        <input placeholder="<spring:message code="mail.bcc"/>" type="text"
                               name="${status.expression}" id="${status.expression}" value="${status.value}"/>
                    </spring:bind>
                </li>
                <li class="form">
                    <spring:bind path="ticketHistory.place">
                        <input placeholder="<spring:message code="collaboration.appointment.place"/>" type="text"
                               name="${status.expression}" id="${status.expression}" value="${status.value}"/>
                    </spring:bind>
                </li>

                <%--start date--%>
                <li class="menu">
                    <a href="javascript:openApptStartDate();">
                        <span class="name1"><spring:message code="collaboration.appointment.startDate"/>:&nbsp;</span>
                        <span class="name2" id="thApptStartDate">
                            <fmt:formatDate pattern="yyyy MM dd" value="${ticketHistory.startDate}"/>
                        </span>
                        <input type="hidden" name="apptStartDate" id="apptStartDate"
                               value="<fmt:formatDate pattern="yyyy-MM-dd" value="${ticketHistory.startDate}"/>"/>
                        <span class="arrow"></span>
                    </a>
                </li>
                <c:if test="${ticketHistory.commentType eq 1}">
                <%--start time--%>
                <li class="menu">
                    <a href="javascript:openApptStartTime();">
                        <span class="name1"><spring:message code="collaboration.appointment.startTime"/>:&nbsp;</span>
                        <span class="name2" id="thApptStartTime">
                            <fmt:formatDate pattern="HH:mm:ss" value="${ticketHistory.startDate}"/>
                        </span>
                        <input type="hidden" name="apptStartTime" id="apptStartTime"
                               value="<fmt:formatDate pattern="HH:mm:00" value="${ticketHistory.startDate}"/>"/>
                        <span class="arrow"></span>
                    </a>
                </li>
                </c:if>
                <%--Duration--%>
                <li class="menu">
                    <a href="javascript:openApptDurationAmount();">
                        <span class="name1"><spring:message code="collaboration.appointment.duration"/>:&nbsp;</span>
                        <span class="name2" id="durationDisplay">
                            ${ticketHistory.durationAmount} ${ticketHistory.durationUnit}
                        </span>
                        <spring:bind path="ticketHistory.durationAmount">
                            <input type="hidden" name="${status.expression}" id="${status.expression}"
                                   value="${status.value}"/>
                        </spring:bind>
                        <spring:bind path="ticketHistory.durationUnit">
                            <input type="hidden" name="${status.expression}" id="${status.expression}"
                                   value="${status.value}"/>
                        </spring:bind>
                        <span class="arrow"></span>
                    </a>
                </li>
                <%--Appointment Type--%>
                <li class="form">
                    <label for="apptType" class="selectlabel"><spring:message code="collaboration.appointment.markAs"/>:&nbsp;</label>
                    <spring:bind path="ticketHistory.apptType">
                        <select name="${status.expression}" id="apptType" onchange="iwkSelectHack(this);">
                            <c:forEach var="at" items="${apptTypes}">
                                <option value="${at.id}"><spring:message code="${at.name}"/></option>
                            </c:forEach>
                        </select>
                    </spring:bind>
                    <span class="arrow"></span>
                </li>
            </c:if>

            <li class="form">
                <spring:bind path="ticketHistory.subject">
                    <input placeholder="<spring:message code="ticket.subject"/>" type="text" id="${status.expression}"
                           name="${status.expression}" value="${status.value}">
                </spring:bind>
            </li>
            <li class="textbox">
                <span class="name"><spring:message code="ticket.note"/></span>
                <spring:bind path="ticketHistory.note">
                    <textarea id="${status.expression}" name="${status.expression}" rows="5"><c:out
                            value="${status.value}"/></textarea>
                </spring:bind>
            </li>
            <li class="form">
                <span class="check">
                    <span class="name">
                        <spring:message code="ticket.hist.notifyTech"/>
                    </span>
                    <spring:bind path="ticketHistory.notifyTech">
                        <input type="checkbox" name="${status.expression}" id="${status.expression}"
                               <c:if test="${ticketHistory.notifyTech}">checked="checked"</c:if>/>
                        <input type="hidden" name="_${status.expression}" id="_${status.expression}"/>
                    </spring:bind>
                </span>
            </li>
            <li class="form">
                <span class="check">
                    <span class="name">
                        <spring:message code="ticket.hist.notifyUser"/>
                    </span>
                    <spring:bind path="ticketHistory.notifyUser">
                        <input id="${status.expression}" name="${status.expression}" type="checkbox"
                               <c:if test="${ticketHistory.notifyUser}">checked="checked"</c:if> />
                        <input type="hidden" name="_${status.expression}" id="_${status.expression}">
                    </spring:bind>
                </span>
            </li>

            <c:if test="${ticketHistory.commentType != 1 and ticketHistory.commentType != 2}">
                <li class="form">
                    <spring:bind path="ticketHistory.cc">
                        <input type="text" name="${status.expression}" id="${status.expression}"
                               value="<c:out value="${status.value}"/>" placeholder="<spring:message code="mail.cc"/>"/>
                    </spring:bind>
                </li>
            </c:if>

        </ul>

        <ul class="pageitem">
            <li class="form"><input type="submit" value="<spring:message code="global.save"/>"/></li>
        </ul>

        <input type="hidden" name="mttabId" value="${mttabId}"/>
    </form>

    <div id="footer"><a href="http://www.grouplink.net">Powered by GroupLink</a></div>
</div>

</body>
</html>