<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="tid" type="java.lang.Integer"--%>
<%--@elvariable id="mttabId" type="java.lang.Integer"--%>
<%--@elvariable id="redMessage" type="java.lang.String"--%>
<%--@elvariable id="message" type="java.lang.String"--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>

    <title><spring:message code="global.systemName"/></title>

    <meta content="yes" name="apple-mobile-web-app-capable"/>
    <meta content="index,follow" name="robots"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <link href="<c:url value="/images/eReferrals_logo.png"/>" rel="apple-touch-icon"/>
    <meta name="viewport" content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no"/>

    <link href="<c:url value="/css/iwk_style.css"/>" rel="stylesheet" type="text/css"/>
    <script src="<c:url value="/js/iwk_functions.js"/>" type="text/javascript"></script>

</head>
<body>
<div id="topbar">
    <div id="leftnav">
        <a href="<c:url value="/ip/home.glml"/>">
            <img alt="home" src="<c:url value="/images/home.png"/>"/>
        </a>
        <a href="<c:url value="/ip/ticketEdit.glml"><c:param name="tid" value="${tid}"/><c:param name="mttabId" value="${mttabId}"/></c:url>">
            <spring:message code="ticket.number"/>: ${tid}
        </a>
    </div>
    <div id="rightnav">
        <a href="<c:url value="/logout.glml"/>">
            <spring:message code="header.signout"/>
        </a>
    </div>
</div>


<div id="content">
    <ul class="pageitem">
        <li class="textbox">
            <p style="margin: 20px; font-family:Verdana, Arial, Helvetica, sans-serif; color: red; font-size: 22px; font-weight: bold">
                <spring:message code="${redMessage}"/>
            </p>

            <p style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;">
                <spring:message code="${message}"/>
            </p>
        </li>
    </ul>
</div>
</body>
</html>