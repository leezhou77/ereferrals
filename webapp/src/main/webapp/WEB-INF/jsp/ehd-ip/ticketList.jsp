<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="sort" type="java.lang.String"--%>
<%--@elvariable id="tabName" type="java.lang.String"--%>
<%--@elvariable id="mttabId" type="java.lang.Integer"--%>
<%--@elvariable id="tickets" type="java.util.List<net.grouplink.ehelpdesk.domain.Ticket>"--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
    <title><spring:message code="global.systemName"/></title>
    <meta content="yes" name="apple-mobile-web-app-capable"/>
    <meta content="index,follow" name="robots"/>
    <meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type"/>
    <link href="<c:url value="/images/eReferrals_logo.png"/>" rel="apple-touch-icon"/>
    <meta content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no"
          name="viewport"/>
    <link href="<c:url value="/css/iwk_style.css"/>" rel="stylesheet" type="text/css"/>
    <script src="<c:url value="/js/iwk_functions.js"/>" type="text/javascript"></script>
    <script type="text/javascript" src="<c:url value="/js/theme/jquery-1.7.0.min.js"/>"></script>

    <script type="text/javascript">

        $(function() {
            $("#sort").on("change", function(e) {
                document.forms[0].submit();
            });
        });

    </script>
</head>
<body>

<div id="topbar">
    <div id="leftnav">
        <a href="<c:url value="/ip/home.glml"/>">
            <img alt="home" src="<c:url value="/images/home.png"/>"/>
        </a>
        <a href="<c:url value="/ip/ticketEdit.glml"><c:param name="mttabId" value="${mttabId}"/></c:url> ">
            <%--<spring:message code="ticket.new"/>--%>
            <img src="<c:url value="/images/header/records_add_48.png"/>" alt="" style="height:20px;width:20px;"/>
        </a>
    </div>
    <div id="rightnav">
        <a href="<c:url value="/logout.glml"/>">
            <spring:message code="header.signout"/>
        </a>
    </div>
</div>

<form action="" method="post" name="listForm">
    <%--<input type="hidden" name="urgid" value="${urgId}">--%>
    <%--<input type="hidden" name="obm" value="${obm}">--%>

    <div id="content">
        <span class="graytitle"> <c:out value="${tabName}"/></span>

        <ul class="pageitem">

            <li class="form">
                <select id="sort" name="sort">
                    <option value="priority" <c:if test="${sort == 'priority'}">selected</c:if>>
                        <spring:message code="iplist.sort.priority"/>
                    </option>
                    <option value="id" <c:if test="${sort == 'id'}">selected</c:if>>
                        <spring:message code="iplist.sort.id"/>
                    </option>
                    <option value="contact" <c:if test="${sort == 'contact'}">selected</c:if>>
                        <spring:message code="iplist.sort.contact"/>
                    </option>
                    <option value="created" <c:if test="${sort == 'created'}">selected</c:if>>
                        <spring:message code="iplist.sort.created"/>
                    </option>
                    <option value="modified" <c:if test="${sort == 'modified'}">selected</c:if>>
                        <spring:message code="iplist.sort.modified"/>
                    </option>
                    <option value="location" <c:if test="${sort == 'location'}">selected</c:if>>
                        <spring:message code="iplist.sort.location"/>
                    </option>
                </select>
                <span class="arrow"></span>
            </li>

            <%--<c:if test="${not obm}">--%>
                <%--<li class="form">--%>
                    <%--<select name="grouping" onchange="ddChanged(this);">--%>
                        <%--<option value="assignedToMe" <c:if test="${grouping == 'assignedToMe'}">selected</c:if>>--%>
                            <%--<spring:message code="ticketList.assignedToMe"/>--%>
                        <%--</option>--%>
                        <%--<option value="ticketPool" <c:if test="${grouping == 'ticketPool'}">selected</c:if>>--%>
                            <%--<spring:message code="ticketList.ticketPool"/>--%>
                        <%--</option>--%>
                        <%--<c:if test="${showAllGroupTickets}">--%>
                            <%--<option value="allGroupTickets"--%>
                                    <%--<c:if test="${grouping == 'allGroupTickets'}">selected</c:if>>--%>
                                <%--<spring:message code="ticketList.allGroupTickets"/>--%>
                            <%--</option>--%>
                        <%--</c:if>--%>
                    <%--</select>--%>
                    <%--<span class="arrow"></span>--%>
                <%--</li>--%>
            <%--</c:if>--%>
        </ul>

        <span class="graytitle">
            <spring:message code="ticket.header"/>
            <c:if test="${fn:length(tickets) > 0}">(${fn:length(tickets)})</c:if>
        </span>
        <ul class="pageitem autolist">
            <c:choose>
                <c:when test="${fn:length(tickets)==0}">
                    <li class="textbox">
                        <spring:message code="ticketList.nolist"/>
                    </li>
                </c:when>
                <c:otherwise>
                    <c:forEach items="${tickets}" var="tckt">
                        <li class="store">
                            <a href="<c:url value="/ip/ticketEdit.glml"><c:param name="tid" value="${tckt.id}"/><c:param name="mttabId" value="${mttabId}"/></c:url>">
                                <div class="topper">
                                    <table class="topper" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="cellleft">
                                                <span class="tid">#${tckt.id}</span>
                                                <c:choose>
                                                    <c:when test="${tckt.priority.icon == 'text'}">
                                                        <c:out value="${tckt.priority.name}"/>
                                                    </c:when>
                                                    <c:when test="${tckt.priority.icon == 'blank'}">
                                                    </c:when>
                                                    <c:otherwise>
                                                        <img src="<c:url value="/images/${tckt.priority.icon}ball_12.png"/>"
                                                             title="<c:out value="${tckt.priority.name}"/>" alt="">
                                                    </c:otherwise>
                                                </c:choose>
                                            </td>
                                            <td class="cellleft">
                                                <%--<c:if test="${not empty tckt.ticketTemplate}">--%>
                                                    <%--<img alt="" src="<c:url value="/images/form_16.png"/>"--%>
                                                         <%--title="<spring:message code="ticketList.hasTicketTemplate" />"/>--%>
                                                <%--</c:if>--%>
                                                <%--<c:if test="${fn:length(tckt.subtickets) > 0}">--%>
                                                    <%--<img alt="" src="<c:url value="/images/form_down_16.png"/>"--%>
                                                         <%--title="<spring:message code="ticketList.hasSubTickets" />"/>--%>
                                                <%--</c:if>--%>
                                                <%--<c:if test="${not empty tckt.parent}">--%>
                                                    <%--<img alt="" src="<c:url value="/images/form_up_16.png"/>"--%>
                                                         <%--title="<spring:message code="ticketList.hasParentTicket" />"/>--%>
                                                <%--</c:if>--%>
                                                <%--<c:set var="attchCount" value="${fn:length(tckt.attachments)}"/>--%>
                                                <%--<c:forEach items="${tckt.ticketHistory}" var="tHist">--%>
                                                    <%--<c:set var="attchCount"--%>
                                                           <%--value="${attchCount + fn:length(tHist.attachments)}"/>--%>
                                                <%--</c:forEach>--%>
                                                <%--<c:if test="${attchCount > 0}">--%>
                                                    <%--<img src="<c:url value="/images/clip_14.png"/>"--%>
                                                         <%--alt="" title="<spring:message code="ticket.attachments" />"/>--%>
                                                <%--</c:if>--%>
                                            </td>
                                            <td class="cellright" align="right"><c:out value="${tckt.status.name}"/></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="midder">
                                    <table class="midder" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="cellleft">
                                                <c:out value="${tckt.contact.firstName}"/> <c:out
                                                    value="${tckt.contact.lastName}"/>
                                            </td>
                                            <td class="cellright" align="right">
                                                <spring:message code="ticket.creationDate"/>:
                                                <fmt:formatDate value="${tckt.createdDate}" type="both"
                                                                dateStyle="SHORT"
                                                                timeStyle="SHORT"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="cellleft">
                                                <c:out value="${tckt.location.name}"/>
                                            </td>
                                            <td class="cellright" align="right">
                                                <spring:message code="ticket.modifiedDate"/>:
                                                <fmt:formatDate value="${tckt.modifiedDate}" type="both"
                                                                dateStyle="SHORT"
                                                                timeStyle="SHORT"/>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="bottomer">
                                    <table>
                                        <tr>
                                            <td class="cellleft">
                                                <spring:message code="ticket.subject"/>:
                                                <span style="color:#4c4c4c"><c:out value="${tckt.subject}"/></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="cellleft">
                                                <spring:message code="ticket.note"/>:
                                                <span style="color:#4c4c4c"><c:out value="${tckt.note}"/></span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </a>
                        </li>
                    </c:forEach>
                </c:otherwise>
            </c:choose>

            <li class="hidden autolisttext">
                <a class="noeffect" href="#"><spring:message code="iplist.moreitems" arguments="10"/></a>
            </li>
        </ul>
    </div>

</form>
</body>

</html>