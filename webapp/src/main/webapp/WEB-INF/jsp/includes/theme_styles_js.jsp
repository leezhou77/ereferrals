<link rel="shortcut icon" href="<c:url value="/images/favicon.ico"/>" type="image/x-icon"/>
<link rel="stylesheet" href="<c:url value="/css/theme/reset.css"/>" type="text/css">
<link rel="stylesheet" href="<c:url value="/css/theme/common.css"/>" type="text/css">
<link rel="stylesheet" href="<c:url value="/css/theme/standard.css"/>" type="text/css">
<link rel="stylesheet" href="<c:url value="/css/theme/960.gs.fluid.css"/>" type="text/css">
<link rel="stylesheet" href="<c:url value="/css/theme/simple-lists.css"/>" type="text/css">
<link rel="stylesheet" href="<c:url value="/css/theme/block-lists.css"/>" type="text/css">
<link rel="stylesheet" href="<c:url value="/css/theme/form.css"/>" type="text/css">
<link rel="stylesheet" href="<c:url value="/css/theme/table.css"/>" type="text/css">

<!-- Generic libs -->
<script type="text/javascript" src="<c:url value="/js/theme/html5.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/theme/jquery-1.7.0.min.js"/>"></script>

<!-- Template libs -->
<script type="text/javascript" src="<c:url value="/js/theme/jquery.accessibleList.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/theme/common.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/theme/standard.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/theme/list.js"/>"></script>

<!--[if lte IE 8]><script type="text/javascript" src="<c:url value="/js/theme/standard.ie.js"/>"></script><![endif]-->
<script type="text/javascript" src="<c:url value="/js/theme/jquery.tip.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/theme/jquery.hashchange.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/theme/jquery.contextMenu.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/theme/jquery.modal.js"/>"></script>