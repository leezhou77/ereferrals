<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="ticketDataJSON" type="java.lang.String"--%>
<%--@elvariable id="master" type="net.grouplink.ehelpdesk.domain.TicketTemplateMaster"--%>
<%--@elvariable id="launch" type="net.grouplink.ehelpdesk.domain.TicketTemplateLaunch"--%>
<%--@elvariable id="launchError" type="java.lang.Boolean"--%>
<%--@elvariable id="launchSuccessful" type="java.lang.Boolean"--%>
<%--@elvariable id="firstId" type="java.lang.Integer"--%>

<head>
    <title><spring:message code="ticketTemplate.title"/></title>
    <script type="text/javascript">
        var ticketData = ${ticketDataJSON};

        function launchTemplate(){
            location.href = '<c:url value="/ticketTemplates/activateTTLaunch.glml"><c:param name="ttLaunchId" value="${launch.id}"/></c:url>';
        }

        function abortLaunch(){
            if(confirm("<spring:message code="ticketTemplate.abortConfirm"/>")){
                location.href = '<c:url value="/ticketTemplates/abortTTLaunch.glml"><c:param name="ttLaunchId" value="${launch.id}"/></c:url>';
            }
        }

        function createTree() {
            // load up the tree model with json store
            var ticketStore = new dojo.data.ItemFileWriteStore({data:ticketData});

            var ttModel = new dijit.tree.ForestStoreModel({
                store: ticketStore,
                rootId: "root",
                rootLabel: "<spring:message code="ticketTemplate.tickets"/>",
                childrenAttrs: ["children"]
            });

            new dijit.Tree({
                model: ttModel,
                onClick: loadEdit
            }, "ticketTree");
        }

        function loadEdit(item){          
            var url;
            if(item.id == "root"){
                return;
            } else {
                url = '<c:url value="/tickets/"/>' + item.id + '/edit';
                document.getElementById("ticketPaneFrame").src = url;
            }
        }

        function clickFirstTreeNode(){
        <c:if test="${firstId != -1}">

            <spring:url value="/tickets/{ticketId}/edit" var="ticketEditUrl">
                <spring:param name="ticketId" value="${firstId}"/>
            </spring:url>

            <%--var yerl = '<c:url value="/ticket/edit2.glml"><c:param name="tid" value="${firstId}"/></c:url>';--%>
            var yerl = '${ticketEditUrl}';
            document.getElementById("ticketPaneFrame").src = yerl;
        </c:if>
        }

        dojo.addOnLoad(function(){
            createTree();
            clickFirstTreeNode();
        });

        </script>

</head>

<body>

<section class="grid_12">
    <div class="block-content">
        <h1><spring:message code="ticketTemplate.launch"/></h1>

        <c:if test="${launch.status eq 1}">
            <div class="block-controls">
                <ul class="controls-buttons">
                    <li>
                        <a href="javascript:abortLaunch();" id="abortButton">
                            <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt="">
                            <spring:message code="ticketTemplate.abort"/>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:launchTemplate();" id="launchButton">
                            <img src="<c:url value="/images/theme/icons/fugue/tick-circle.png"/>" alt="">
                            <spring:message code="ticketTemplate.launch"/>
                        </a>
                    </li>
                </ul>
            </div>
        </c:if>

        <div class="columns">
            <div class="colx3-left">
                <spring:message code="ticketTemplate.name"/>: <strong>${master.name}</strong>
            </div>
            <div class="colx3-center">
                <spring:message code="ticketTemplate.launch.id"/>: ${launch.id}
            </div>
            <div class="colx3-right">
                <spring:message code="ticketTemplate.launch.status"/>:
                <span style="color:red; font-weight:bold;">
                    <c:choose>
                        <c:when test="${launch.status eq 1}"><spring:message code="ticketTemplate.launch.status.pending"/></c:when>
                        <c:when test="${launch.status eq 2}"><spring:message code="ticketTemplate.launch.status.active"/></c:when>
                    </c:choose>
                </span>
            </div>

        </div>

        <c:if test="${launch.status eq 1 && !launchError && !launchSuccessful}">
            <p class="message warning">
                <spring:message code="ticketTemplate.launch.message"/>
            </p>
        </c:if>
        <c:if test="${launchError}">
            <p class="message error">
                <spring:message code="ticketTemplate.launch.launchError"/>
            </p>
        </c:if>

        <c:if test="${launchSuccessful}">
            <p class="message success">
                <spring:message code="ticketTemplate.launch.launchSuccessful"/>
            </p>
        </c:if>

        <div class="content-columns left30">
            <!-- Vertical separator -->
            <div class="content-columns-sep"></div>

            <!-- Left column -->
            <div class="content-left"><div class="dark-grey-gradient with-padding" style="height:900px;">
                <div id="ticketTree"> </div>
            </div></div>

            <!-- Right column -->
            <div class="content-right"><div class="dark-grey-gradient with-padding" style="height:900px;">
                <iframe id="ticketPaneFrame" frameborder="0" style="height: 100%; width: 100%;"></iframe>
            </div></div>
        </div>
    </div>
</section>
</body>
