<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/include.jsp" %>

<%--@elvariable id="ticketFilter" type="net.grouplink.ehelpdesk.domain.ticket.TicketFilter"--%>
<%--@elvariable id="opNameMap" type="java.util.Map<java.lang.Integer,java.lang.String>"--%>
<%--@elvariable id="priorities" type="java.util.List<net.grouplink.ehelpdesk.domain.TicketPriority>"--%>
<%--@elvariable id="status" type="java.util.List<net.grouplink.ehelpdesk.domain.Status>"--%>
<%--@elvariable id="groups" type="java.util.List<net.grouplink.ehelpdesk.domain.Group>"--%>
<%--@elvariable id="rc" type="org.springframework.web.servlet.support.RequestContext"--%>
<%--@elvariable id="dateOps" type="java.util.List<net.grouplink.ehelpdesk.domain.ticket.TicketFilterOperators>"--%>
<%--@elvariable id="surveyQuestions" type="java.util.List<java.lang.String>"--%>
<%--@elvariable id="customFields" type="java.util.List<java.lang.String>"--%>
<%--@elvariable id="surveysOn" type="java.lang.Boolean"--%>
<%--@elvariable id="hasCustomFieldFilterEnabled" type="java.lang.Boolean"--%>
<%--@elvariable id="hasSurveyFieldFilterEnabled" type="java.lang.Boolean"--%>
<%--@elvariable id="visibleCustomFields" type="java.util.List<java.lang.String>"--%>

<head>

    <title><spring:message code="ticketSearch.title"/></title>

    <link rel="stylesheet" href="<c:url value="/js/dojo-1.6.2/dojox/grid/resources/Grid.css"/>" type="text/css">
    <link rel="stylesheet" href="<c:url value="/js/dojo-1.6.2/dojox/grid/resources/tundraGrid.css"/>" type="text/css">

    <link rel="stylesheet" href="<c:url value="/css/ticketFilterEdit.css"/>" type="text/css">

    <tags:jsTemplate id="filter-tr-tmpl">
        <tr>
            <td>{{ operator }}</td>
            <td>{{ item }}</td>
            <td class="delete">
                <a href="javascript:void(0)"
                   class="{{ deleteClass }}"
                   data-div-id="{{ divId }}"
                   data-row-id="{{ rowId }}"
                   title="<spring:message code="global.delete"/>">
                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                </a>
            </td>
        </tr>
    </tags:jsTemplate>

    <tags:jsTemplate id="fetcher-li-tmpl">
        <li class="small-margin">
            <input type="checkbox" name="cbx_{{ rowIndex }}" id="cbx_{{ rowIndex }}_{{ objId }}" dojoType="dijit.form.CheckBox" value="{{ objId }}">
            <select style="width:100px" dojoType="dijit.form.FilteringSelect" id="cbx_{{ rowIndex }}_{{ objId }}_dd">
                <option value="9"><spring:message code="ticketFilter.op.equals"/></option>
                <option value="10"><spring:message code="ticketFilter.op.notEquals"/></option>
            </select>
            <label for="cbx_{{ rowIndex }}_{{ objId }}">{{ objName }}</label>
        </li>
    </tags:jsTemplate>

    <tags:jsTemplate id="filter-def-tmpl">
        <tags:filterBox title="{{customFieldName}}" filterName="customField_{{rowId}}" filterValuesBoxId="customFieldBox_{{rowId}}" columnName="customField.{{customFieldName}}">
            <jsp:attribute name="hidden">false</jsp:attribute>
            <jsp:attribute name="widgets">
                <select dojoType="dijit.form.FilteringSelect" id="op_customField_{{rowId}}">
                    <option value="9"><spring:message code="ticketFilter.op.equals"/></option>
                    <option value="10"><spring:message code="ticketFilter.op.notEquals"/></option>
                    <option value="13"><spring:message code="ticketFilter.op.contains"/></option>
                    <option value="14"><spring:message code="ticketFilter.op.notContains"/></option>
                    <option value="15"><spring:message code="ticketFilter.op.startsWith"/></option>
                    <option value="16"><spring:message code="ticketFilter.op.endsWith"/></option>
                </select>
                <input type="text" class="filterValueInput" name="customField_{{rowId}}" id="customField_{{rowId}}" trim="true"
                       dojoType="dijit.form.TextBox">
                <input type="hidden" id="customFieldName_{{rowId}}" name="customFieldName_{{rowId}}" value="{{customFieldName}}">
                <a href="javascript:void(0)"
                   title="<spring:message code="ticketFilter.addNew"/>"
                   class="addCFItem">
                    <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                </a>
            </jsp:attribute>
        </tags:filterBox>
    </tags:jsTemplate>

    <script type="text/javascript" src="<c:url value="/dwr/interface/TicketFilterService.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>

    <link rel="stylesheet" href="<c:url value="/js/chosen/chosen.css"/>" type="text/css">
    <script type="text/javascript" src="<c:url value="/js/chosen/chosen.jquery.js"/>"></script>

    <script type="text/javascript">
        dojo.addOnLoad(function() {
            filterMode = "ticketFilter";
            //create and populate the drag and drop source for column ordering
            window.columnOrder = new dojo.dnd.Source("columnOrder", { "creator": columnCreator, "singular": true });
            <c:forEach var="col" items="${ticketFilter.columnOrder}">
                <c:choose>
                    <c:when test="${fn:startsWith(col, 'ticketFilter')}">
                        <c:set var="colText">
                            <spring:message code="${col}" javaScriptEscape="true"/>
                        </c:set>
                    </c:when>
                    <c:when test="${fn:startsWith(col, 'survey.')}">
                        <c:set var="colText" value="${fn:substringAfter(col, 'survey.')}"/>
                    </c:when>
                    <c:otherwise>
                        <c:set var="colText" value="${fn:substringAfter(col, 'customField.')}"/>
                    </c:otherwise>
                </c:choose>
                window.columnOrder.insertNodes(false,
                    [{
                        name: "<c:out value="${col}"/>",
                        text: "<c:out value="${colText}"/>"
                    }]);
            </c:forEach>
        });
    </script>

    <%@ include file="/WEB-INF/jsp/includes/ticketFilterEdit.js.jsp" %>

</head>

<body>

<div id="preloader"></div>

<content tag="breadCrumb">
    <ul id="breadcrumb">
        <li><a href="<c:url value="/tf/ticketFilterList.glml"/>"><spring:message code="tabs.filters"/></a></li>
        <li><a href="javascript:location.reload();"><spring:message code="ticketFilter.action.editFilter"/></a></li>
    </ul>
</content>

<form:form commandName="ticketFilter" id="ticketFilterForm" name="ticketFilterForm">
<section class="grid_8">
    <div class="block-content form">
        <h1><spring:message code="ticketFilter.title"/></h1>
        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a href="#" data-form-action="apply">
                        <img src="<c:url value="/images/theme/icons/fugue/arrow-curve-000-left.png"/>" alt="">
                        <spring:message code="assetFilter.applyFilter"/>
                    </a>
                </li>

                <%-- check permission if filter is public and owned by another user --%>
                <c:if test="${showSaveButton}">
                    <li>
                        <a href="#" data-form-action="save">
                            <img src="<c:url value="/images/save.gif"/>" alt="" style="width:16px;height:16px">
                            <spring:message code="assetFilter.saveFilter"/>
                        </a>
                    </li>
                </c:if>
                <li>
                    <a href="#" data-form-action="reset">
                        <img src="<c:url value="/images/theme/icons/fugue/counter-reset.png"/>" alt="">
                        <spring:message code="assetFilter.resetFilter"/>
                    </a>
                </li>
                <li>
                    <a href="#" data-form-action="saveas">
                        <img src="<c:url value="/images/theme/icons/fugue/document.png"/>" alt="">
                        <spring:message code="assetFilter.saveAsFilter"/>
                    </a>
                </li>
            </ul>
        </div>

        <input type="hidden" name="formAction" id="formAction" value="save"/>
        <input type="hidden" name="saveAsName" id="saveAsName"/>

        <spring:bind path="ticketFilter.name">
            <label for="${status.expression}"><spring:message code="ticketFilter.filterName"/></label>
            <input type="text" id="${status.expression}" name="${status.expression}"
                   value="<ehd:stringEscape value="${status.value}" type="html"/>" size="40" maxlength="255"/>
        </spring:bind>
        <spring:bind path="ticketFilter.privateFilter">
            <input type="checkbox" id="${status.expression}" name="${status.expression}"
                   value="TRUE" <c:if test="${status.value eq true}">checked="checked"</c:if>/>
            <label for="${status.expression}" class="inline"><spring:message code="ticketFilter.markasprivate"/></label>
            <input type="hidden" name="_${status.expression}">
        </spring:bind>

    </div>
    <br>
    <div id="filter-block" class="block-content no-padding">
        <div class="with-padding">
            <div class="float-left margin-right">
                <select id="field-select" data-placeholder="<spring:message code="ticketFilter.chooseColumnFilter"/>">
                    <option value=""></option>
                    <%-- Ticket Fields --%>
                    <optgroup class="ticketFields" label="<spring:message code="ticketFilter.ticketFields"/>">
                        <c:forEach items="${allColumns}" var="entry">
                            <c:if test="${!ehd:mapContainsValue(criteriaCols, entry.value)}">
                                <option value="${entry.value}" id="opt_${entry.value}">
                                    <c:out value="${entry.key}"/>
                                </option>
                            </c:if>
                        </c:forEach>
                    </optgroup>
                    <%-- Custom Fields --%>
                    <c:if test="${fn:length(customFields) gt 0}">
                    <optgroup class="customFields" label="<spring:message code="ticketFilter.customField"/>">
                        <c:forEach var="cfname" items="${customFields}" varStatus="row">
                            <c:set var="customFieldColumn" value="customField.${cfname}"/>
                            <c:if test="${!ehd:mapContainsValue(criteriaCols, customFieldColumn)}">
                                <option value="customField.${cfname}" id="opt_ticketFilter.customField_${row.index}">
                                    <c:out value="${cfname}"/>
                                </option>
                            </c:if>
                            <c:remove var="customFieldColumn"/>
                        </c:forEach>
                    </optgroup>
                    </c:if>
                    <%-- Survey Questions --%>
                    <c:if test="${fn:length(surveyQuestions) gt 0}">
                    <optgroup class="surveyQuestions" label="<spring:message code="ticketFilter.surveys"/>">
                        <c:forEach var="question" items="${surveyQuestions}" varStatus="row">
                            <c:set var="surveyColumn" value="survey.${question}"/>
                            <c:if test="${!ehd:mapContainsValue(criteriaCols, surveyColumn)}">
                                <option value="survey.${question}" id="opt_ticketFilter.survey_${row.index}">
                                    <c:out value="${ehd:truncateString(question, 27)}"/>
                                </option>
                            </c:if>
                            <c:remove var="surveyColumn"/>
                        </c:forEach>
                    </optgroup>
                    </c:if>
                </select>
            </div>
            <div class="float-left">
                <a class="button float-left" style="height:21px;padding:3px 5px 0 5px;margin-top:1px" href="javascript:void(0)" id="showFilterLi">
                    <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="" style="width:16px;height:16px">
                </a>
            </div>
            <div class="clearfix"></div>
        </div>

        <ul id="filter-list" class="extended-list no-hover">
        <%--Ticket Numbers--%>
        <tags:filterBox titleMessageCode="ticketFilter.ticketNumber" filterName="ticketNumber" filterValuesBoxId="ticketNumbers" columnName="ticketFilter.ticketNumber">
            <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsValue(criteriaCols, 'ticketFilter.ticketNumber')}">false</c:if></jsp:attribute>
            <jsp:attribute name="widgets">
                <select dojoType="dijit.form.FilteringSelect" id="op_ticketNumber">
                    <option value="9"><spring:message code="ticketFilter.op.equals"/></option>
                    <option value="10"><spring:message code="ticketFilter.op.notEquals"/></option>
                    <option value="11"><spring:message code="ticketFilter.op.greaterThan"/></option>
                    <option value="12"><spring:message code="ticketFilter.op.lessThan"/></option>
                </select>
                <input type="text" class="filterValueInput" name="tnumFld" id="tnumFld" trim="true"
                       invalidmessage="<spring:message code="ticketSearch.validate.ticketnumber.parse"/>"
                       dojoType="dijit.form.NumberTextBox" constraints="{min:1,places:0}"/>
                <a href="javascript:void(0)"
                   title="<spring:message code="ticketFilter.addNew"/>"
                   class="addItem">
                    <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                </a>
            </jsp:attribute>
            <jsp:attribute name="filterValuesBox">
                <table class="filterValues">
                <c:forEach var="tnum" items="${ticketFilter.ticketNumbers}" varStatus="row">
                    <tr>
                        <td>
                            <c:set var="opNumber" value="${ticketFilter.ticketNumbersOps[row.index]}"/>
                            <c:out value="${opNameMap[opNumber]}"/>
                            <c:remove var="opNumber"/>
                        </td>
                        <td>${tnum}</td>
                        <td class="delete">
                            <a href="javascript:void(0)"
                               class="deleteItem"
                               data-div-id="ticketNumbers"
                               data-row-id="${row.index}"
                               title="<spring:message code="global.delete"/>">
                                <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                            </a>
                        </td>
                    </tr>
                </c:forEach>
                </table>
            </jsp:attribute>
        </tags:filterBox>

        <%--Priorities--%>
        <tags:filterBox titleMessageCode="ticketFilter.priority" filterName="priority" filterValuesBoxId="priorityIds" columnName="ticketFilter.priority">
            <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsValue(criteriaCols, 'ticketFilter.priority')}">false</c:if></jsp:attribute>
            <jsp:attribute name="widgets">
                <select dojoType="dijit.form.FilteringSelect" id="op_priority">
                    <option value="9"><spring:message code="ticketFilter.op.equals"/></option>
                    <option value="10"><spring:message code="ticketFilter.op.notEquals"/></option>
                </select>
                <select name="priorFld" class="filterValueInput" id="priorFld" dojoType="dijit.form.FilteringSelect">
                    <option value=""></option>
                    <c:forEach var="prior" items="${priorities}">
                        <option value="${prior.id}">${prior.name}</option>
                    </c:forEach>
                </select>
                <a href="javascript:void(0)" title="<spring:message code="ticketFilter.addNew"/>"
                   class="addItem">
                    <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                </a>
            </jsp:attribute>
            <jsp:attribute name="filterValuesBox">
                <table class="filterValues">
                <c:forEach var="priority" items="${ticketFilter.priorityIds}" varStatus="row">
                    <tr>
                        <td>
                            <c:set var="opPriority" value="${ticketFilter.priorityIdsOps[row.index]}"/>
                            <c:out value="${opNameMap[opPriority]}"/>
                            <c:remove var="opPriority"/>
                        </td>
                        <td>
                            <c:forEach var="prior" items="${priorities}">
                                <c:if test="${prior.id == priority}">
                                    <c:out value="${prior.name}"/>
                                </c:if>
                            </c:forEach>
                        </td>
                        <td class="delete">
                            <a href="javascript:void(0)"
                               class="deleteItem"
                               data-div-id="priorityIds"
                               data-row-id="${row.index}"
                               title="<spring:message code="global.delete"/>">
                                <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                            </a>
                        </td>
                    </tr>
                </c:forEach>
                </table>
            </jsp:attribute>
        </tags:filterBox>

        <%--Status--%>
        <tags:filterBox titleMessageCode="ticketFilter.status" filterName="status" filterValuesBoxId="statusIds" columnName="ticketFilter.status">
            <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsValue(criteriaCols, 'ticketFilter.status')}">false</c:if></jsp:attribute>
            <jsp:attribute name="widgets">
                <select dojoType="dijit.form.FilteringSelect" id="op_status">
                    <option value="9"><spring:message code="ticketFilter.op.equals"/></option>
                    <option value="10"><spring:message code="ticketFilter.op.notEquals"/></option>
                </select>
                <select name="statusFld" class="filterValueInput" id="statusFld" dojoType="dijit.form.FilteringSelect">
                    <option value=""></option>
                    <c:forEach var="stat" items="${status}">
                        <option value="${stat.id}">${stat.name}</option>
                    </c:forEach>
                </select>
                <a href="javascript:void(0)" title="<spring:message code="ticketFilter.addNew"/>"
                   class="addItem">
                    <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                </a>
            </jsp:attribute>
            <jsp:attribute name="filterValuesBox">
                <table class="filterValues">
                <c:forEach var="stat" items="${ticketFilter.statusIds}" varStatus="row">
                    <tr>
                        <td>
                            <c:set var="opStatus" value="${ticketFilter.statusIdsOps[row.index]}"/>
                            <c:out value="${opNameMap[opStatus]}"/>
                            <c:remove var="opStatus"/>
                        </td>
                        <td>
                            <c:forEach var="st" items="${status}">
                                <c:if test="${st.id == stat}">
                                    <c:out value="${st.name}"/>
                                </c:if>
                            </c:forEach>
                        </td>
                        <td class="delete">
                            <a href="javascript:void(0)"
                               class="deleteItem"
                               data-div-id="statusIds"
                               data-row-id="${row.index}"
                               title="<spring:message code="global.delete"/>">
                                <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                            </a>
                        </td>
                    </tr>
                </c:forEach>
                </table>
            </jsp:attribute>
        </tags:filterBox>

        <%--TODO make sure the problem in 18863 is still fixed --%>
        <%--Assigned To--%>
        <tags:filterBox titleMessageCode="ticketFilter.assigned" filterName="assigned" filterValuesBoxId="assignedToIds" columnName="ticketFilter.assigned">
            <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsValue(criteriaCols, 'ticketFilter.assigned')}">false</c:if></jsp:attribute>
            <jsp:attribute name="widgets">
                <select dojoType="dijit.form.FilteringSelect" id="op_assigned">
                    <option value="9"><spring:message code="ticketFilter.op.equals"/></option>
                    <option value="10"><spring:message code="ticketFilter.op.notEquals"/></option>
                </select>

                <span dojoType="dojox.data.QueryReadStore" jsId="techStore"
                     url="<c:url value="/stores/ticketSearchAssignments.json"/>"></span>

                <input dojoType="dijit.form.FilteringSelect" class="filterValueInput" store="techStore" pagesize="20" searchAttr="label"
                       name="assFld" id="assFld" autocomplete="true" searchDelay="500"/>

                <a href="javascript:void(0)" title="<spring:message code="ticketFilter.addNew"/>"
                   class="addItem">
                    <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                </a>
            </jsp:attribute>
            <jsp:attribute name="filterValuesBox">
                <table class="filterValues">
                    <c:forEach var="assToId" items="${ticketFilter.assignedToIds}" varStatus="row">
                        <tr>
                            <td>
                                <c:set var="opNumber" value="${ticketFilter.assignedToIdsOps[row.index]}"/>
                                <c:out value="${opNameMap[opNumber]}"/>
                                <c:remove var="opNumber"/>
                            </td>
                            <td>
                                <c:out value="${assignedToIdToNameMap[assToId]}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)" title="<spring:message code="global.delete"/>"
                                   class="deleteItem"
                                   data-div-id="assignedToIds"
                                   data-row-id="${row.index}"
                                   data-type="assignedToIds">
                                   <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                    <c:forEach var="assToUserId" items="${ticketFilter.assignedToUserIds}" varStatus="row2">
                        <tr>
                            <td>
                                <c:set var="opNumber" value="${ticketFilter.assignedToUserIdsOps[row2.index]}"/>
                                <c:out value="${opNameMap[opNumber]}"/>
                                <c:remove var="opNumber"/>
                            </td>
                            <td>
                                <c:out value="${assignedToUserIdToNameMap[assToUserId]}"/>
                            </td>
                            <td align="right">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="assignedToIds"
                                   data-row-id="${row2.index}"
                                   data-type="assignedToUserIds"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </jsp:attribute>
        </tags:filterBox>


        <%--Subject--%>
        <tags:filterBox titleMessageCode="ticketFilter.subject" filterName="subject" filterValuesBoxId="subject" columnName="ticketFilter.subject">
            <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsValue(criteriaCols, 'ticketFilter.subject')}">false</c:if></jsp:attribute>
            <jsp:attribute name="widgets">
                <select dojoType="dijit.form.FilteringSelect" id="op_subject">
                    <option value="9"><spring:message code="ticketFilter.op.equals"/></option>
                    <option value="10"><spring:message code="ticketFilter.op.notEquals"/></option>
                    <option value="13"><spring:message code="ticketFilter.op.contains"/></option>
                    <option value="14"><spring:message code="ticketFilter.op.notContains"/></option>
                    <option value="15"><spring:message code="ticketFilter.op.startsWith"/></option>
                    <option value="16"><spring:message code="ticketFilter.op.endsWith"/></option>
                </select>

                <input type="text" class="filterValueInput" name="subjFld" id="subjFld" dojoType="dijit.form.TextBox" trim="true"/>

                <a href="javascript:void(0)" title="<spring:message code="ticketFilter.addNew"/>"
                   class="addItem">
                    <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                </a>
            </jsp:attribute>
            <jsp:attribute name="filterValuesBox">
                <table class="filterValues">
                    <c:forEach var="subj" items="${ticketFilter.subject}" varStatus="row">
                        <tr>
                            <td>
                                <c:set var="opSubject"
                                       value="${ticketFilter.subjectOps[row.index]}"/>
                                <c:out value="${opNameMap[opSubject]}"/>
                                <c:remove var="opSubject"/>
                            </td>
                            <td><c:out value="${subj}"/></td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="subject"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </jsp:attribute>
        </tags:filterBox>

        <%--Note--%>
        <tags:filterBox titleMessageCode="ticketFilter.note" filterName="note" filterValuesBoxId="note" columnName="ticketFilter.note">
            <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsValue(criteriaCols, 'ticketFilter.note')}">false</c:if></jsp:attribute>
            <jsp:attribute name="widgets">
                <select dojoType="dijit.form.FilteringSelect" id="op_note">
                    <option value="9"><spring:message code="ticketFilter.op.equals"/></option>
                    <option value="10"><spring:message code="ticketFilter.op.notEquals"/></option>
                    <option value="13"><spring:message code="ticketFilter.op.contains"/></option>
                    <option value="14"><spring:message code="ticketFilter.op.notContains"/></option>
                    <option value="15"><spring:message code="ticketFilter.op.startsWith"/></option>
                    <option value="16"><spring:message code="ticketFilter.op.endsWith"/></option>
                </select>

                <input type="text" class="filterValueInput" name="noteFld" id="noteFld" dojoType="dijit.form.TextBox" trim="true"/>

                <a href="javascript:void(0)" title="<spring:message code="ticketFilter.addNew"/>"
                   class="addItem">
                    <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                </a>
            </jsp:attribute>
            <jsp:attribute name="filterValuesBox">
                <table class="filterValues">
                    <c:forEach var="note" items="${ticketFilter.note}" varStatus="row">
                        <tr>
                            <td>
                                <c:set var="opNote" value="${ticketFilter.noteOps[row.index]}"/>
                                <c:out value="${opNameMap[opNote]}"/>
                                <c:remove var="opNote"/>
                            </td>
                            <td><c:out value="${note}"/></td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="note"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </jsp:attribute>
        </tags:filterBox>

        <%--History Subject--%>
        <tags:filterBox titleMessageCode="ticketFilter.historySubject" filterName="historySubject" filterValuesBoxId="historySubject" columnName="ticketFilter.historySubject">
            <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsValue(criteriaCols, 'ticketFilter.historySubject')}">false</c:if></jsp:attribute>
            <jsp:attribute name="widgets">
                <select dojoType="dijit.form.FilteringSelect" id="op_historySubject">
                    <option value="9"><spring:message code="ticketFilter.op.equals"/></option>
                    <option value="10"><spring:message code="ticketFilter.op.notEquals"/></option>
                    <option value="13"><spring:message code="ticketFilter.op.contains"/></option>
                    <option value="14"><spring:message code="ticketFilter.op.notContains"/></option>
                    <option value="15"><spring:message code="ticketFilter.op.startsWith"/></option>
                    <option value="16"><spring:message code="ticketFilter.op.endsWith"/></option>
                </select>

                <input type="text" class="filterValueInput" name="histSubjFld" id="histSubjFld" dojoType="dijit.form.TextBox" trim="true"/>

                <a href="javascript:void(0)" title="<spring:message code="ticketFilter.addNew"/>"
                   class="addItem">
                    <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                </a>
            </jsp:attribute>
            <jsp:attribute name="filterValuesBox">
                <table class="filterValues">
                    <c:forEach var="hSubj" items="${ticketFilter.historySubject}" varStatus="row">
                        <tr>
                            <td>
                                <c:set var="opHSubj" value="${ticketFilter.historySubjectOps[row.index]}"/>
                                <c:out value="${opNameMap[opHSubj]}"/>
                                <c:remove var="opHSubj"/>
                            </td>
                            <td><c:out value="${hSubj}"/></td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="historySubject"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </jsp:attribute>
        </tags:filterBox>

        <%--History Note--%>
        <tags:filterBox titleMessageCode="ticketFilter.historyNote" filterName="historyNote" filterValuesBoxId="historyNote" columnName="ticketFilter.historyNote">
            <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsValue(criteriaCols, 'ticketFilter.historyNote')}">false</c:if></jsp:attribute>
            <jsp:attribute name="widgets">
                <select dojoType="dijit.form.FilteringSelect" id="op_historyNote">
                    <option value="9"><spring:message code="ticketFilter.op.equals"/></option>
                    <option value="10"><spring:message code="ticketFilter.op.notEquals"/></option>
                    <option value="13"><spring:message code="ticketFilter.op.contains"/></option>
                    <option value="14"><spring:message code="ticketFilter.op.notContains"/></option>
                    <option value="15"><spring:message code="ticketFilter.op.startsWith"/></option>
                    <option value="16"><spring:message code="ticketFilter.op.endsWith"/></option>
                </select>

                <input type="text" class="filterValueInput" name="histNoteFld" id="histNoteFld" dojoType="dijit.form.TextBox" trim="true"/>

                <a href="javascript:void(0)" title="<spring:message code="ticketFilter.addNew"/>"
                   class="addItem">
                    <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                </a>
            </jsp:attribute>
            <jsp:attribute name="filterValuesBox">
                <table class="filterValues">
                    <c:forEach var="hNote" items="${ticketFilter.historyNote}" varStatus="row">
                        <tr>
                            <td>
                                <c:set var="opHNote" value="${ticketFilter.historyNoteOps[row.index]}"/>
                                <c:out value="${opNameMap[opHNote]}"/>
                                <c:remove var="opHNote"/>
                            </td>
                            <td><c:out value="${hNote}"/></td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="historyNote"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </jsp:attribute>
        </tags:filterBox>

        <%--Location--%>
        <tags:filterBox titleMessageCode="ticketFilter.location" filterName="location" filterValuesBoxId="locationIds"  columnName="ticketFilter.location">
            <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsValue(criteriaCols, 'ticketFilter.location')}">false</c:if></jsp:attribute>
            <jsp:attribute name="widgets">
                <select dojoType="dijit.form.FilteringSelect" id="op_location">
                    <option value="9"><spring:message code="ticketFilter.op.equals"/></option>
                    <option value="10"><spring:message code="ticketFilter.op.notEquals"/></option>
                </select>

                <span dojoType="dojox.data.QueryReadStore" jsId="locationStore"
                     url="<c:url value="/stores/locations.json"><c:param name="tech" value="true"/></c:url>"></span>

                <input dojoType="dijit.form.FilteringSelect" class="filterValueInput" store="locationStore" pagesize="20"
                       searchAttr="label" name="locFld" id="locFld" autocomplete="true" searchDelay="500"/>

                <a href="javascript:void(0)" title="<spring:message code="ticketFilter.addNew"/>"
                   class="addItem">
                    <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                </a>
            </jsp:attribute>
            <jsp:attribute name="filterValuesBox">
                <table class="filterValues">
                    <c:forEach var="locId" items="${ticketFilter.locationIds}" varStatus="row">
                        <tr>
                            <td>
                                <c:set var="opLocation" value="${ticketFilter.priorityIdsOps[row.index]}"/>
                                <c:out value="${opNameMap[opLocation]}"/>
                                <c:remove var="opLocation"/>
                            </td>
                            <td>
                                <c:out value="${locationIdToNameMap[locId]}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="locationIds"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </jsp:attribute>
        </tags:filterBox>

        <%--Group--%>
        <tags:filterBox titleMessageCode="ticketFilter.group" filterName="group" filterValuesBoxId="groupIds" columnName="ticketFilter.group">
            <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsValue(criteriaCols, 'ticketFilter.group')}">false</c:if></jsp:attribute>
            <jsp:attribute name="widgets">
                <select dojoType="dijit.form.FilteringSelect" id="op_group">
                    <option value="9"><spring:message code="ticketFilter.op.equals"/></option>
                    <option value="10"><spring:message code="ticketFilter.op.notEquals"/></option>
                </select>

                <select name="groupFld" class="filterValueInput" id="groupFld" dojoType="dijit.form.FilteringSelect">
                    <option value=""></option>
                    <c:forEach var="group" items="${groups}">
                        <option value="${group.id}">${group.name}</option>
                    </c:forEach>
                </select>

                <a href="javascript:void(0)" title="<spring:message code="ticketFilter.addNew"/>"
                   class="addItem">
                    <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                </a>
            </jsp:attribute>
            <jsp:attribute name="filterValuesBox">
                <table class="filterValues">
                    <c:forEach var="groupId" items="${ticketFilter.groupIds}" varStatus="row">
                        <tr>
                            <td>
                                <c:set var="opGroup" value="${ticketFilter.groupIdsOps[row.index]}"/>
                                <c:out value="${opNameMap[opGroup]}"/>
                                <c:remove var="opGroup"/>
                            </td>
                            <td>
                                <c:forEach var="gr" items="${groups}">
                                    <c:if test="${gr.id == groupId}">
                                        <c:out value="${gr.name}"/>
                                    </c:if>
                                </c:forEach>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="groupIds"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </jsp:attribute>
        </tags:filterBox>

        <%--Category--%>
        <tags:filterBox titleMessageCode="ticketFilter.category" filterName="category" filterValuesBoxId="categoryIds" columnName="ticketFilter.category">
            <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsValue(criteriaCols, 'ticketFilter.category')}">false</c:if></jsp:attribute>
            <jsp:attribute name="widgets">
                <a id="showCategoryChooser" href="javascript:void(0)" title="<spring:message code="ticketFilter.addNew"/>">
                    <img src="<c:url value="/images/theme/icons/fugue/magnifier.png"/>" alt="<spring:message code="ticketFilter.addNew"/>"/>
                </a>
            </jsp:attribute>
            <jsp:attribute name="filterValuesBox">
                <table class="filterValues">
                    <c:forEach var="catId" items="${ticketFilter.categoryIds}" varStatus="row">
                        <tr>
                            <td>
                                <c:set var="opCategory" value="${ticketFilter.categoryIdsOps[row.index]}"/>
                                <c:out value="${opNameMap[opCategory]}"/>
                                <c:remove var="opCategory"/>
                            </td>
                            <td>
                                <c:out value="${catIdToNameMap[catId]}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="categoryIds"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </jsp:attribute>
        </tags:filterBox>

        <%--Category Options--%>
        <tags:filterBox titleMessageCode="ticketFilter.catopt" filterName="catopt" filterValuesBoxId="categoryOptionIds" columnName="ticketFilter.catopt">
            <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsValue(criteriaCols, 'ticketFilter.catopt')}">false</c:if></jsp:attribute>
            <jsp:attribute name="widgets">
                <a id="showCatOptChooser" href="javascript:void(0)" title="<spring:message code="ticketFilter.addNew"/>">
                    <img src="<c:url value="/images/theme/icons/fugue/magnifier.png"/>" alt="<spring:message code="ticketFilter.addNew"/>"/>
                </a>
            </jsp:attribute>
            <jsp:attribute name="filterValuesBox">
                <table class="filterValues">
                    <c:forEach var="catOptId" items="${ticketFilter.categoryOptionIds}" varStatus="row">
                        <tr>
                            <td>
                                <c:set var="opCatOpt" value="${ticketFilter.categoryOptionIdsOps[row.index]}"/>
                                <c:out value="${opNameMap[opCatOpt]}"/>
                                <c:remove var="opCatOpt"/>
                            </td>
                            <td>
                                <c:out value="${catOptIdToNameMap[catOptId]}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="categoryOptionIds"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </jsp:attribute>
        </tags:filterBox>

        <%--Contact--%>
        <tags:filterBox titleMessageCode="ticketFilter.contact" filterName="contact" filterValuesBoxId="contactIds" columnName="ticketFilter.contact">
            <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsValue(criteriaCols, 'ticketFilter.contact')}">false</c:if></jsp:attribute>
            <jsp:attribute name="widgets">
                <div dojoType="dojox.data.QueryReadStore" jsId="userStore" url="<c:url value="/stores/users.json"/>"></div>

                <select dojoType="dijit.form.FilteringSelect" id="op_contact">
                    <option value="9"><spring:message code="ticketFilter.op.equals"/></option>
                    <option value="10"><spring:message code="ticketFilter.op.notEquals"/></option>
                </select>

                <input dojoType="dijit.form.FilteringSelect" class="filterValueInput" store="userStore" pagesize="20"
                       searchAttr="label" name="ctctFld" id="ctctFld" autocomplete="true" searchDelay="500"/>

                <a href="javascript:void(0)" title="<spring:message code="ticketFilter.addNew"/>"
                   class="addItem">
                    <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                </a>
            </jsp:attribute>
            <jsp:attribute name="filterValuesBox">
                <table class="filterValues">
                    <c:forEach var="contactId" items="${ticketFilter.contactIds}" varStatus="row">
                        <tr>
                            <td>
                                <c:set var="opContact" value="${ticketFilter.contactIdsOps[row.index]}"/>
                                <c:out value="${opNameMap[opContact]}"/>
                                <c:remove var="opContact"/>
                            </td>
                            <td>
                                <c:out value="${contactIdToNameMap[contactId]}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="contactIds"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </jsp:attribute>
        </tags:filterBox>

        <%--Submitted By--%>
        <tags:filterBox titleMessageCode="ticketFilter.submitted" filterName="submitted" filterValuesBoxId="submittedByIds" columnName="ticketFilter.submitted">
            <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsValue(criteriaCols, 'ticketFilter.submitted')}">false</c:if></jsp:attribute>
            <jsp:attribute name="widgets">
                <select dojoType="dijit.form.FilteringSelect" id="op_submitted">
                    <option value="9"><spring:message code="ticketFilter.op.equals"/></option>
                    <option value="10"><spring:message code="ticketFilter.op.notEquals"/></option>
                </select>

                <input dojoType="dijit.form.FilteringSelect" class="filterValueInput" store="userStore" pagesize="20"
                       searchAttr="label" name="subByFld" id="subByFld" autocomplete="true" searchDelay="500"/>

                <a href="javascript:void(0)" title="<spring:message code="ticketFilter.addNew"/>"
                   class="addItem">
                    <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                </a>
            </jsp:attribute>
            <jsp:attribute name="filterValuesBox">
                <table class="filterValues">
                    <c:forEach var="subId" items="${ticketFilter.submittedByIds}" varStatus="row">
                        <tr>
                            <td>
                                <c:set var="opSub" value="${ticketFilter.submittedByIdsOps[row.index]}"/>
                                <c:out value="${opNameMap[opSub]}"/>
                                <c:remove var="opSub"/>
                            </td>
                            <td>
                                <c:out value="${submittedIdToNameMap[subId]}"/>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="submittedByIds"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </jsp:attribute>
        </tags:filterBox>

        <%--Created Date--%>
        <tags:filterBox titleMessageCode="ticketFilter.created" filterName="created" columnName="ticketFilter.created">
            <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsValue(criteriaCols, 'ticketFilter.created')}">false</c:if></jsp:attribute>
            <jsp:attribute name="widgets">
                <div class="small-margin align-right">
                    <form:select id="createdDateOperator" path="createdDateOperator" onchange="showDate(this, 'cdate');">
                        <form:options items="${dateOps}" itemLabel="display" itemValue="value"/>
                    </form:select>
                </div>

                <div class="small-margin" id="cdate1" style="display:none">
                    <spring:bind path="ticketFilter.createdDate">
                        <input type="text" name="${status.expression}" id="${status.expression}" value="${status.value}"
                               dojoType="dijit.form.DateTextBox"/>
                        <span style="color:red"><c:out value="${status.errorMessage}"/></span>
                    </spring:bind>
                </div>

                <div class="small-margin" id="cdate2" style="display:none">
                    <spring:bind path="ticketFilter.createdDate2">
                        <input type="text" name="${status.expression}" id="${status.expression}" value="${status.value}"
                               dojoType="dijit.form.DateTextBox"/>
                        <span style="color:red"><c:out value="${status.errorMessage}"/></span>
                    </spring:bind>
                </div>

                <div class="small-margin" id="cdateLastXDiv" style="display:none">
                    <spring:bind path="ticketFilter.createdDateLastX">
                        <input dojoType="dijit.form.NumberSpinner" constraints="{min:1,max:100,places:0}"
                               value="${status.value}" id="${status.expression}" name="${status.expression}"/>
                    </spring:bind>
                </div>
            </jsp:attribute>
        </tags:filterBox>

        <%--Modified Date--%>
        <tags:filterBox titleMessageCode="ticketFilter.modified" filterName="modified" columnName="ticketFilter.modified">
            <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsValue(criteriaCols, 'ticketFilter.modified')}">false</c:if></jsp:attribute>
            <jsp:attribute name="widgets">
                <div class="small-margin align-right">
                    <form:select id="modifiedDateOperator" path="modifiedDateOperator" onchange="showDate(this, 'mdate');">
                        <form:options items="${dateOps}" itemLabel="display" itemValue="value"/>
                    </form:select>
                </div>

                <div class="small-margin" id="mdate1" style="display:none">
                    <spring:bind path="ticketFilter.modifiedDate">
                        <input type="text" name="${status.expression}" id="${status.expression}" value="${status.value}"
                               dojoType="dijit.form.DateTextBox"/>
                        <span style="color:red"><c:out value="${status.errorMessage}"/></span>
                    </spring:bind>
                </div>

                <div class="small-margin" id="mdate2" style="display:none">
                    <spring:bind path="ticketFilter.modifiedDate2">
                        <input type="text" name="${status.expression}" id="${status.expression}" value="${status.value}"
                               dojoType="dijit.form.DateTextBox"/>
                        <span style="color:red"><c:out value="${status.errorMessage}"/></span>
                    </spring:bind>
                </div>

                <div class="small-margin" id="mdateLastXDiv" style="display:none">
                    <spring:bind path="ticketFilter.modifiedDateLastX">
                        <input dojoType="dijit.form.NumberSpinner" constraints="{min:1,max:100,places:0}"
                               value="${status.value}" id="${status.expression}" name="${status.expression}"/>
                    </spring:bind>
                </div>
            </jsp:attribute>
        </tags:filterBox>

        <%--Est. Completed Date--%>
        <tags:filterBox titleMessageCode="ticketFilter.estimated" filterName="estimated" columnName="ticketFilter.estimated">
            <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsValue(criteriaCols, 'ticketFilter.estimated')}">false</c:if></jsp:attribute>
            <jsp:attribute name="widgets">
                <div class="small-margin align-right">
                    <form:select id="estimatedDateOperator" path="estimatedDateOperator"
                                 onchange="showDate(this, 'estdate');">
                        <form:options items="${dateOps}" itemLabel="display" itemValue="value"/>
                    </form:select>
                </div>

                <div class="small-margin" id="estdate1" style="display:none">
                    <spring:bind path="ticketFilter.estimatedDate">
                        <input type="text" name="${status.expression}" id="${status.expression}" value="${status.value}"
                               dojoType="dijit.form.DateTextBox"/>
                        <span style="color:red"><c:out value="${status.errorMessage}"/></span>
                    </spring:bind>
                </div>

                <div class="small-margin" id="estdate2" style="display:none">
                    <spring:bind path="ticketFilter.estimatedDate2">
                        <input type="text" name="${status.expression}" id="${status.expression}" value="${status.value}"
                               dojoType="dijit.form.DateTextBox"/>
                        <span style="color:red"><c:out value="${status.errorMessage}"/></span>
                    </spring:bind>
                </div>

                <div class="small-margin" id="estdateLastXDiv" style="display:none">
                    <spring:bind path="ticketFilter.estimatedDateLastX">
                        <input dojoType="dijit.form.NumberSpinner" constraints="{min:1,max:100,places:0}"
                               value="${status.value}" id="${status.expression}" name="${status.expression}"/>
                    </spring:bind>
                </div>
            </jsp:attribute>
        </tags:filterBox>

        <%--Worktime--%>
        <tags:filterBox titleMessageCode="ticketFilter.worktime" filterName="worktime" columnName="ticketFilter.worktime">
            <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsValue(criteriaCols, 'ticketFilter.worktime')}">false</c:if></jsp:attribute>
            <jsp:attribute name="widgets">
                <spring:bind path="ticketFilter.workTimeOperator">
                    <div class="small-margin align-right">
                        <select name="${status.expression}" id="${status.expression}" onchange="showWorktime(this);">
                            <option value="">-- <spring:message code="ticketSearch.any"/> --</option>
                            <option value="more"
                                    <c:if test="${status.value eq 'more'}">selected="selected"</c:if> >
                                <spring:message code="ticketSearch.worktime.more"/>
                            </option>
                            <option value="less"
                                    <c:if test="${status.value eq 'less'}">selected="selected"</c:if> >
                                <spring:message code="ticketSearch.worktime.less"/>
                            </option>
                        </select>
                    </div>
                </spring:bind>

                <div class="small-margin" id="workTime1" style="display:none">
                    <spring:bind path="ticketFilter.workTimeHours">
                        <input type="text" name="${status.expression}" id="${status.expression}" value="${status.value}"
                               dojoType="dijit.form.NumberTextBox"/>
                        <label for="${status.expression}"><spring:message code="scheduler.time.hour"/></label>
                        <span style="color:red"><c:out value="${status.errorMessage}"/></span>
                    </spring:bind>
                </div>

                <div class="small-margin" id="workTime2" style="display:none">
                    <spring:bind path="ticketFilter.workTimeMinutes">
                        <input type="text" name="${status.expression}" id="${status.expression}" value="${status.value}"
                               dojoType="dijit.form.NumberTextBox"/>
                        <span style="color:red"><c:out value="${status.errorMessage}"/></span>
                        <label for="${status.expression}"><spring:message code="scheduler.time.mins"/></label>
                    </spring:bind>
                </div>
            </jsp:attribute>
        </tags:filterBox>

        <%--Zen10 Assets--%>
        <tags:filterBox titleMessageCode="ticketFilter.zenworks10Asset" filterName="zenworks10Asset" filterValuesBoxId="zenAssetIds" columnName="ticketFilter.zenworks10Asset">
            <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsValue(criteriaCols, 'ticketFilter.zenworks10Asset')}">false</c:if></jsp:attribute>
            <jsp:attribute name="widgets">
                <a id="showZenAssetChooser" href="javascript:void(0)" title="<spring:message code="ticketFilter.addNew"/>">
                    <img src="<c:url value="/images/theme/icons/fugue/magnifier.png"/>" alt="<spring:message code="ticketFilter.addNew"/>"/>
                </a>
            </jsp:attribute>
            <jsp:attribute name="filterValuesBox">
                <table class="filterValues">
                    <c:forEach var="zenAssetId" items="${ticketFilter.zenAssetIds}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${zenAssetIdToNameMap[zenAssetId]}"/>
                            </td>
                            <td class="delete">
                                <form:hidden path="zenAssetIds[${row.index}]"/>
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="zenAssetIds"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </jsp:attribute>
        </tags:filterBox>

        <%--Internal Assets--%>
        <tags:filterBox titleMessageCode="ticketFilter.internalAsset" filterName="internalAsset" filterValuesBoxId="internalAssetIds" columnName="ticketFilter.internalAsset">
            <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsValue(criteriaCols, 'ticketFilter.internalAsset')}">false</c:if></jsp:attribute>
            <jsp:attribute name="widgets">
                <a id="showInternalAssetChooser" href="javascript:void(0)" title="<spring:message code="ticketFilter.addNew"/>">
                    <img src="<c:url value="/images/theme/icons/fugue/magnifier.png"/>" alt="<spring:message code="ticketFilter.addNew"/>"/>
                </a>
            </jsp:attribute>
            <jsp:attribute name="filterValuesBox">
                <table width="100%">
                    <c:forEach var="internalAssetId" items="${ticketFilter.internalAssetIds}" varStatus="row">
                        <tr>
                            <td>
                                <c:out value="${assetIdToNameMap[internalAssetId]}"/>
                            </td>
                            <td class="delete">
                                <form:hidden path="internalAssetIds[${row.index}]"/>
                                <a href="javascript:void(0)"
                                   class="deleteItem"
                                   data-div-id="internalAssetIds"
                                   data-row-id="${row.index}"
                                   title="<spring:message code="global.delete"/>">
                                <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </jsp:attribute>
        </tags:filterBox>

        <%--Custom Fields --%>
        <c:forEach var="customField" items="${ticketFilter.customFields}" varStatus="row">
            <c:set var="colName" value="custom_field_${row.index}"/>
            <tags:filterBox title="${customField.key}" filterName="customField_${row.index}" filterValuesBoxId="customFieldDiv_${row.index}" columnName="customField.${customField.key}">
                <jsp:attribute name="hidden">false</jsp:attribute>
                <jsp:attribute name="widgets">
                    <select dojoType="dijit.form.FilteringSelect" id="op_customField_${row.index}">
                        <option value="9"><spring:message code="ticketFilter.op.equals"/></option>
                        <option value="10"><spring:message code="ticketFilter.op.notEquals"/></option>
                        <option value="13"><spring:message code="ticketFilter.op.contains"/></option>
                        <option value="14"><spring:message code="ticketFilter.op.notContains"/></option>
                        <option value="15"><spring:message code="ticketFilter.op.startsWith"/></option>
                        <option value="16"><spring:message code="ticketFilter.op.endsWith"/></option>
                    </select>

                    <input type="text" name="customField_${row.index}" id="customField_${row.index}"
                           dojoType="dijit.form.TextBox" trim="true"/>
                    <input type="hidden" name="customFieldName_${row.index}" id="customFieldName_${row.index}"
                           dojoType="dijit.form.TextBox" trim="true" value="<c:out value="${customField.key}"/>"/>

                    <a href="javascript:void(0)" title="<spring:message code="ticketFilter.addNew"/>"
                       class="addCFItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                        <c:forEach var="tfCFHelper" items="${customField.value}">
                            <tr>
                                <td>
                                    <c:out value="${opNameMap[tfCFHelper.operatorId]}"/>
                                </td>
                                <td><c:out value="${tfCFHelper.value}"/></td>
                                <td class="delete">
                                    <a href="javascript:void(0)"
                                       class="deleteCFItem"
                                       title="<spring:message code="global.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>
            <c:remove var="colName"/>
        </c:forEach>

        <%--Surveys--%>
        <%--TODO there's a bug here: survey columns never make it into criteriaCols. Bug exists at least since 9.3.2 but probably since ticket filters were introduced --%>
        <c:forEach var="question" items="${surveyQuestions}" varStatus="row">
            <c:set var="colName" value="survey_${row.index}"/>
            <tags:filterBox title="${question}" filterName="survey_${row.index}" filterValuesBoxId="surveyQuestionDiv_${row.index}" columnName="survey.${question}">
                <jsp:attribute name="hidden"><c:if test="${ehd:mapContainsValue(criteriaCols, colName)}">false</c:if></jsp:attribute>
                <jsp:attribute name="widgets">
                    <select dojoType="dijit.form.FilteringSelect" id="op_survey_${row.index}">
                        <option value="9"><spring:message code="ticketFilter.op.equals"/></option>
                        <option value="10"><spring:message code="ticketFilter.op.notEquals"/></option>
                        <option value="13"><spring:message code="ticketFilter.op.contains"/></option>
                        <option value="14"><spring:message code="ticketFilter.op.notContains"/></option>
                        <option value="15"><spring:message code="ticketFilter.op.startsWith"/></option>
                        <option value="16"><spring:message code="ticketFilter.op.endsWith"/></option>
                    </select>

                    <input type="text" name="survey_${row.index}" id="survey_${row.index}"
                           dojoType="dijit.form.TextBox" trim="true"/>
                    <input type="hidden" name="surveyQuestionText_${row.index}"
                           id="surveyQuestionText_${row.index}"
                           dojoType="dijit.form.TextBox" trim="true" value="${question}"/>

                    <a href="javascript:void(0)" title="<spring:message code="ticketFilter.addNew"/>"
                       class="addSurveyItem">
                        <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle-blue.png"/>"/>
                    </a>
                </jsp:attribute>
                <jsp:attribute name="filterValuesBox">
                    <table class="filterValues">
                        <c:forEach var="surveyEntry" items="${ticketFilter.surveys}" varStatus="filterValRow">
                            <c:if test="${surveyEntry.key == question}">
                                <c:forEach var="tfcHelper" items="${surveyEntry.value}">
                                    <tr>
                                        <td>
                                            <c:out value="${opNameMap[tfcHelper.operatorId]}"/>
                                        </td>
                                        <td><c:out value="${tfcHelper.value}"/></td>
                                        <td class="delete">
                                            <a href="javascript:void(0)"
                                               title="<spring:message code="global.delete"/>"
                                               class="delSurveyItem">
                                                <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt=""/>
                                            </a>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </c:if>
                        </c:forEach>
                    </table>
                </jsp:attribute>
            </tags:filterBox>
            <c:remove var="colName"/>
        </c:forEach>
    </ul>

</div>

</section>
<section class="grid_3">
    <div class="block-content">
        <h1><spring:message code="ticketFilter.colOrder"/></h1>

        <div class="float-left">
            <select id="field-in-filter-select" class="chosen" data-placeholder="<spring:message code="ticketFilter.chooseColumn"/>">
                <option value=""></option>
                <%-- Ticket Fields --%>
                <optgroup class="ticketFields" label="<spring:message code="ticketFilter.ticketFields"/>">
                    <c:forEach items="${allColumns}" var="entry">
                        <c:if test="${!ehd:arrayContains(ticketFilter.columnOrder, entry.value)}">
                            <option value="${entry.value}"><c:out value="${entry.key}"/></option>
                        </c:if>
                    </c:forEach>
                </optgroup>
                <%-- Custom Fields --%>
                <c:if test="${fn:length(customFields) gt 0}">
                <optgroup class="customFields" label="<spring:message code="ticketFilter.customField"/>">
                    <c:forEach var="cfname" items="${customFields}" varStatus="row">
                        <c:if test="${!ehd:arrayContains(ticketFilter.columnOrder, cfname)}">
                            <option value="customField.${cfname}"><c:out value="${cfname}"/></option>
                        </c:if>
                    </c:forEach>
                </optgroup>
                </c:if>
                <%-- Survey Questions --%>
                <c:if test="${fn:length(surveyQuestions) gt 0}">
                <optgroup class="surveyQuestions" label="<spring:message code="ticketFilter.surveys"/>">
                    <c:forEach var="question" items="${surveyQuestions}" varStatus="row">
                        <c:if test="${!ehd:arrayContains(ticketFilter.columnOrder, question)}">
                            <option value="survey.${question}"><c:out value="${ehd:truncateString(question, 27)}"/></option>
                        </c:if>
                    </c:forEach>
                </optgroup>
                </c:if>
            </select>
        </div>
        <div class="float-right">
            <a class="button float-left" style="height:21px;padding:3px 5px 0 5px;margin-top:1px" href="javascript:void(0)" id="addColumnToColOrder">
                <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="" style="width:16px;height:16px">
            </a>
        </div>
        <div class="clearfix"></div>

        <hr>

        <ol id="columnOrder" class="columnOrder mini-blocks-list"></ol>
    </div>
</section>

<%--Category and Category Option Fetcher dialog--%>
<div id="fetcher"></div>

<div dojoType="dijit.Dialog" id="saveAsDialog" title="<spring:message code="assetFilter.saveAsFilter"/>"
     execute="dojo.attr(dojo.byId('saveAsName'), 'value', arguments[0].newName); dojo.byId('ticketFilterForm').submit();">
    <p>
        <label for="saveAsName"><spring:message code="ticketFilter.filterName"/></label>
        <input dojoType="dijit.form.ValidationTextBox" type="text" name="newName" id="newName"
            required="true" invalidmessage="<spring:message code="global.requiredInput"/>"/>
    </p>
    <button dojotype="dijit.form.Button" type="submit"
        onclick="return dijit.byId('saveAsDialog').isValid();">
    <spring:message code="global.save"/>
    </button>
    <button dojotype="dijit.form.Button" type="button" onclick="dijit.byId('saveAsDialog').hide();">
        <spring:message code="global.cancel"/>
    </button>
</div>

<div id="assChooser" dojoType="dijit.Dialog">
    <div id="assGridCP" style="width:850px;height:475px;margin-bottom:20px" class="form">
        <p class="small-margin">
            <label for="assSearch"><spring:message code="global.search"/></label>
            <input type="text" id="assSearch">
        </p>
        <div id="assGrid"></div>
    </div>
    <p>
        <button id="intAssApplyBtn"><spring:message code="global.applySelected"/></button>
        <button id="intAssCancelBtn"><spring:message code="global.cancel"/></button>
    </p>
</div>
</form:form>
</body>
