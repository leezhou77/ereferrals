<%--@elvariable id="userFilters" type="java.util.List<TicketFilter>"--%>
<%--@elvariable id="publicFilters" type="java.util.List<TicketFilter"--%>
<%--@elvariable id="rc" type="org.springframework.web.servlet.support.RequestContext"--%>
<%--@elvariable id="filterUsage" type="java.util.Map<TicketFilter, Integer>"--%>

<%@ include file="/WEB-INF/jsp/include.jsp" %>

<head>

    <title>
        <spring:message code="header.ticketFilter"/>
    </title>

    <link rel="stylesheet" href="<c:url value="/css/ticketFilter.css"/>" type="text/css">

    <script type="text/javascript" src="<c:url value="/js/scripts.js"/>"></script>

    <%-- dwr js --%>
    <script type="text/javascript" src="<c:url value="/dwr/interface/TicketFilterService.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>

    <script type="text/javascript">
        function ticketFilterListAction(action, tfId) {
            if (action == 'apply') {
                var url = '<c:url value="/tf/ticketFilterResults.glml"/>?tfId=' + tfId;
                openWindow(url, 'ticketFilterResults', 900, 600);
            } else if (action == 'delete') {
                var callMetaData = {
                    callback: deleteTicketFilterCallbackHandler,
                    arg: tfId
                };

                TicketFilterService.getUsageCount(tfId, callMetaData);
            }
        }

        function deleteTicketFilterCallbackHandler(usageCount, tfId) {
            if (usageCount == 0) {
                if (confirm("<spring:message code="global.confirmdelete"/>")) {
                    // no usages, ok to delete
                    location.href = '<c:url value="/tf/ticketFilterDelete.glml"/>?tfId=' + tfId;
                }
            } else {
                // tf is in use, show usages
                TicketFilterService.getTicketFilterUsage(tfId, showTicketFilterUsageCallbackHandler);
            }
        }

        function showTicketFilterUsageCallbackHandler(ticketFilterDto) {
            var usageDialog = new dijit.Dialog({
                title: "<spring:message code="ticketFilter.usage" javaScriptEscape="true" />"
            });

            var contentDiv = document.createElement("div");
            contentDiv.innerHTML = "<spring:message code="ticketFilter.action.delete.inUse" javaScriptEscape="true"/>";

            contentDiv.appendChild(document.createElement("br"));
            contentDiv.appendChild(document.createElement("br"));

            if (ticketFilterDto.tabs.length > 0) {
                var tabHeading = document.createElement("span");
                tabHeading.innerHTML = "<spring:message code="myTicketTabs" javaScriptEscape="true"/>";
                tabHeading.style.fontWeight = "bold";
                contentDiv.appendChild(tabHeading);
                var tabUsageTable = document.createElement("table");
                tabUsageTable.style.border = "1px solid lightgray";
                tabUsageTable.setAttribute("cellspacing", 5);
                var tabHeaderRow = tabUsageTable.insertRow(-1);
                var tabNameCell = tabHeaderRow.insertCell(-1);
                tabNameCell.innerHTML = "<spring:message code="myTicketTabs.tabName" javaScriptEscape="true" />";
                tabNameCell.style.fontWeight = "bold";
                var tabUserCell = tabHeaderRow.insertCell(-1);
                tabUserCell.innerHTML = "<spring:message code="user.user" javaScriptEscape="true" />";
                tabUserCell.style.fontWeight = "bold";
                for (var i = 0; i<ticketFilterDto.tabs.length; i++) {
                    var tab = ticketFilterDto.tabs[i];
                    var tabRow = tabUsageTable.insertRow(-1);
                    tabRow.insertCell(-1).innerHTML = tab.name;
                    tabRow.insertCell(-1).innerHTML = tab.user.firstName + " " + tab.user.lastName;
                }


                contentDiv.appendChild(tabUsageTable);
                contentDiv.appendChild(document.createElement("br"));
            }

            if (ticketFilterDto.reports.length > 0) {

                var reportHeading = document.createElement("span");
                reportHeading.innerHTML = "<spring:message code="report.title" javaScriptEscape="true" />";
                reportHeading.style.fontWeight = "bold";
                contentDiv.appendChild(reportHeading);
                var reportUsageTable = document.createElement("table");
                reportUsageTable.setAttribute("cellspacing", 5);
                reportUsageTable.style.border = "1px solid lightgray";
                var reportHeaderRow = reportUsageTable.insertRow(-1);
                var reportNameCell = reportHeaderRow.insertCell(-1);
                reportNameCell.innerHTML = "<spring:message code="report.name" javaScriptEscape="true" />";
                reportNameCell.style.fontWeight = "bold";
                var reportUserCell = reportHeaderRow.insertCell(-1);
                reportUserCell.innerHTML = "<spring:message code="report.owner" javaScriptEscape="true" />";
                reportUserCell.style.fontWeight = "bold";
                for (var j = 0; j<ticketFilterDto.reports.length; j++) {
                    var report = ticketFilterDto.reports[j];
                    var reportRow = reportUsageTable.insertRow(-1);
                    reportRow.insertCell(-1).innerHTML = report.name;
                    reportRow.insertCell(-1).innerHTML = report.user.firstName + " " + report.user.lastName;
                }

                contentDiv.appendChild(reportUsageTable);
                contentDiv.appendChild(document.createElement("br"));
            }

            var okBtn = new dijit.form.Button({
                label: "<spring:message code="global.ok" javaScriptEscape="true" />",
                onClick: function() {
                    usageDialog.hide();
                    usageDialog.destroy();
                }
            });

            contentDiv.appendChild(okBtn.domNode);

            usageDialog.set("content", contentDiv);
            usageDialog.show();
        }
    </script>

</head>

<body>
<section class="grid_12">
    <div class="block-content">
        <h1><spring:message code="ticketFilter.myFilters"/></h1>
        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a href="<c:url value="/tf/ticketFilterEdit.glml"><c:param name="clearSessionAttr" value="true"/></c:url>">
                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>">
                        <spring:message code="global.createNew"/>
                    </a>
                </li>
            </ul>
        </div>

        <c:choose>
            <c:when test="${not empty userFilters}">
                <div class="no-margin">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col"><spring:message code="ticketFilter.filterName"/></th>
                            <th scope="col"><spring:message code="ticketFilter.visibility"/></th>
                            <th scope="col" class="table-actions"><spring:message code="ticketFilter.actions"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="filter" items="${userFilters}">
                            <tr>
                                <td><c:out value="${filter.name}"/></td>
                                <td>
                                    <c:choose>
                                        <c:when test="${filter.privateFilter}">
                                            <spring:message code="ticketFilter.private"/>
                                        </c:when>
                                        <c:otherwise>
                                            <spring:message code="ticketFilter.public"/>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="table-actions">
                                    <a href="javascript://" onclick="ticketFilterListAction('apply', '${filter.id}');"
                                       title="<spring:message code="ticketFilter.action.apply"/>">
                                        <img src="<c:url value="/images/theme/icons/fugue/control.png"/>" alt="">
                                    </a>
                                    <a href="<c:url value="/tf/ticketFilterEdit.glml"><c:param name="tfId" value="${filter.id}"/></c:url>"
                                       title="<spring:message code="ticketFilter.action.edit"/>">
                                        <img src="<c:url value="/images/theme/icons/fugue/pencil.png"/>" alt="">
                                    </a>
                                    <a href="javascript://" onclick="ticketFilterListAction('delete', '${filter.id}');"
                                       title="<spring:message code="ticketFilter.action.delete"/>">
                                        <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt="">
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </c:when>
            <c:otherwise>
                <p><spring:message code="ticketFilter.noneFound"/></p>
            </c:otherwise>
        </c:choose>
    </div>
</section>

<section class="grid_12">
    <div class="block-content">
        <h1><spring:message code="ticketFilter.publicFilters"/></h1>

        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a href="<c:url value="/tf/ticketFilterEdit.glml"><c:param name="clearSessionAttr" value="true"/></c:url>">
                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>">
                        <spring:message code="global.createNew"/>
                    </a>
                </li>
            </ul>
        </div>

        <c:choose>
            <c:when test="${not empty publicFilters}">
                <div class="no-margin">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col"><spring:message code="ticketFilter.filterName"/></th>
                            <th scope="col"><spring:message code="ticketFilter.owner"/></th>
                            <th class="table-actions" scope="col"><spring:message code="ticketFilter.actions"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="filter" items="${publicFilters}">
                            <tr>
                                <td><c:out value="${filter.name}"/></td>
                                <td>${filter.user.firstName} ${filter.user.lastName}</td>
                                <td class="table-actions">
                                    <a href="javascript://" onclick="ticketFilterListAction('apply', '${filter.id}');"
                                       title="<spring:message code="ticketFilter.action.apply"/>">
                                        <img src="<c:url value="/images/theme/icons/fugue/control.png"/>" alt="">
                                    </a>
                                    <a href="<c:url value="/tf/ticketFilterEdit.glml"><c:param name="tfId" value="${filter.id}"/></c:url>"
                                       title="<spring:message code="ticketFilter.action.edit"/>">
                                        <img src="<c:url value="/images/theme/icons/fugue/pencil.png"/>" alt="">
                                    </a>
                                    <ehd:authorizeGlobal permission="global.publicTicketFilterManagement">
                                    <a href="javascript://" onclick="ticketFilterListAction('delete', '${filter.id}');"
                                       title="<spring:message code="ticketFilter.action.delete"/>">
                                        <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt="">
                                    </a>
                                    </ehd:authorizeGlobal>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </c:when>
            <c:otherwise>
                <p><spring:message code="ticketFilter.noneFound"/></p>
            </c:otherwise>
        </c:choose>
    </div>
</section>
</body>
