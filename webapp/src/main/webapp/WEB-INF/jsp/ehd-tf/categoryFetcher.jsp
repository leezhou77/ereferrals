<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="groups" type="java.util.List<net.grouplink.ehelpdesk.domain.Group>"--%>

<div style="width:300px;max-height:500px;overflow:auto">
    <c:forEach var="group" items="${groups}" varStatus="row">
        <c:set var="openDiv" value="${row.first}"/>
        <div dojoType="dijit.TitlePane" title="${group.name}" id="titlePane_${group.id}" open="${openDiv}" onClick="catFetcherShowTitlePane('${group.id}', '${row.index}');">
            <div id="catFetcherGroup${group.id}">
                <input type="checkbox" dojoType="dijit.form.CheckBox" id="sall_${row.index}"/>
                <label for="sall_${row.index}">
                    <spring:message code="ticketFilter.selectAll"/>
                </label>
                <hr>
                <c:if test="${openDiv}">
                    <div id="catFetcher_group_${group.id}" style="display: none;"></div>

                    <ul>
                        <c:forEach var="category" items="${group.categories}">
                            <c:if test="${category.active}">
                                <li class="small-margin">
                                    <input type="checkbox" name="cbx_${row.index}" id="cbx_${row.index}_${category.id}"
                                           dojoType="dijit.form.CheckBox" value="${category.id}"/>
                                    <select dojoType="dijit.form.FilteringSelect" id="cbx_${row.index}_${category.id}_dd"
                                            style="width:100px">
                                        <option value="9"><spring:message code="ticketFilter.op.equals"/></option>
                                        <option value="10"><spring:message code="ticketFilter.op.notEquals"/></option>
                                    </select>
                                    <label for="cbx_${row.index}_${category.id}">${category.name}</label>
                                </li>
                            </c:if>
                        </c:forEach>
                    </ul>
                </c:if>
            </div>
        </div>
    </c:forEach>
    <c:remove var="openDiv"/>
</div>

<div>
    <button dojoType="dijit.form.Button" type="submit">
        <spring:message code="ticketFilter.apply"/>
    </button>
</div>
