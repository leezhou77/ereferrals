<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/include.jsp" %>

<%--@elvariable id="report" type="net.grouplink.ehelpdesk.domain.Report"--%>
<%--@elvariable id="myFilters" type="java.util.List<net.grouplink.ehelpdesk.domain.ticket.TicketFilter>"--%>
<%--@elvariable id="pubFilters" type="java.util.List<net.grouplink.ehelpdesk.domain.ticket.TicketFilter>"--%>
<%--@elvariable id="saved" type="java.lang.String"--%>
<%--@elvariable id="scheduledReports" type="java.lang.Boolean"--%>

<head>

    <%@ include file="/WEB-INF/jsp/ehd-config/ticketTemplates/templateMasterRecurrenceEdit-js.jsp" %>

    <script type="text/javascript">
        function hideRegroup(value) {
            if (value) {
                document.getElementById('groupByBy').style.display = '';

            }
            else {
                document.getElementById('groupByBy').style.display = 'none';
            }
            regroupByBy();
        }

        function regroupByBy() {

            var value = document.getElementById('regroupDrilldown').checked;
            var groupBy = document.getElementById('groupBy');
            var catOptText = "<spring:message code="report.categoryOpt"/>";

            if(value) {
                if(optionExists(groupBy, catOptText)) {
                    var selectedIndex = groupBy.options[groupBy.selectedIndex].value;
                    if(selectedIndex == 3) {
                        groupBy.selectedIndex = 0;
                    }
                    removeOption(groupBy, catOptText);
                }
            }

            var groupByBy = document.getElementById('groupByBy');

            if (!value) {
                groupByBy.selectedIndex = groupBy.selectedIndex;
            }
            else {
                var selectedGroupBy = groupBy.options[groupBy.selectedIndex].value;
                if(selectedGroupBy == 3) {
                    if(!optionExists(groupByBy, catOptText)) {
                       addOption(groupByBy, catOptText, '4', 3);
                    }
                }
                else {
                    if(optionExists(groupByBy, catOptText)) {
                        removeOption(groupByBy, catOptText);
                    }
                }
            }
        }

        function regroupByByInit() {

            var value = document.getElementById('regroupDrilldown').checked;
            var groupBy = document.getElementById('groupBy');
            var catOptText = "<spring:message code="report.categoryOpt"/>";
            var selectedGroupBy = groupBy.options[groupBy.selectedIndex].value;
            var groupByBy = document.getElementById('groupByBy');
            var selectedGroupByBy = groupByBy.options[groupByBy.selectedIndex].value;
            if(value) {
                if(selectedGroupBy == 3) {

                    if(!optionExists(groupByBy, catOptText)) {
                       addOption(groupByBy, catOptText, '4', 3);
                    }
                }
                else {
                    if(optionExists(groupByBy, catOptText)) {
                        removeOption(groupByBy, catOptText);
                    }
                }
            }
        }

        function optionExists(select, text) {
            var optionFound = false;
            var i;
            for(i = select.options.length - 1; i >= 0; i--) {
                if(select.options[i].text == text) {
                    optionFound = true;
                    break;
                }
            }
            return optionFound;
        }

        function removeOption(select, text) {
            for(i = select.options.length - 1; i >= 0; i--) {
                if(select.options[i].text == text) {
                    select.remove(i);
                }
            }
        }

        function addOption(select, text, value, i) {
            var optionToAdd = document.createElement("OPTION");
            optionToAdd.text = text;
            optionToAdd.value = value;
            select.options.add(optionToAdd, i);
        }

        function togglePieBar(cbx) {
            document.getElementById('showPieBar').style.display = cbx.checked ? "block" : "none";
        }

        function toggleScheduler(cbx) {
            document.getElementById('scheduler').style.display = cbx.checked ? "block" : "none";
        }

        function changeReportGroupOrder() {
            var groupByOrderSelect = document.getElementById('groupByOrder');
            if (groupByOrderSelect.options[groupByOrderSelect.selectedIndex].value == '2') {
                document.getElementById('reportShowTopSpan').style.display = '';
            } else {
                document.getElementById('reportShowTopSpan').style.display = 'none';
            }
        }

        function showGroupByReport(value) {
            if (value) {
                dojo.style(dojo.byId('groupByReport'), "display", "none");
                dojo.style(dojo.byId('dateBy'), "display", "");
            }
            else {
                dojo.style(dojo.byId('groupByReport'), "display", "");
                dojo.style(dojo.byId('dateBy'), "display", "none");
            }
        }

        function init() {
            dojo.addOnLoad(function(){
                try {
                        if (document.getElementById('regroupDrilldown').checked) {
                            document.getElementById('groupByBy').style.display = '';
                            regroupByByInit();
                        }
                        else {
                            document.getElementById('groupByBy').style.display = 'none';
                        }

                        var groupByOrderSelect = document.getElementById('groupByOrder');
                        if (groupByOrderSelect.options[groupByOrderSelect.selectedIndex].value == '2') {
                            document.getElementById('reportShowTopSpan').style.display = '';
                        } else {
                            document.getElementById('reportShowTopSpan').style.display = 'none';
                        }
                } catch (err) {
                    console.error("error initializing page: " + err);
                }
                hideLoader();
            });
        }

        dojo.addOnLoad(init);
    </script>

</head>

<body>

<content tag="breadCrumb">
    <ul id="breadcrumb">
        <li><a href="<c:url value="/tf/reportList.glml"/>"><spring:message code="tabs.reports"/></a></li>
        <li><a href="javascript:location.reload();"><spring:message code="report.edit"/></a></li>
    </ul>
</content>

<section class="grid_9">
<div class="block-content">
    <c:choose>
        <c:when test="${empty report.id}">
            <h1><spring:message code="report.newreport"/></h1>
        </c:when>
        <c:otherwise>
            <h1><spring:message code="report.edit"/></h1>
        </c:otherwise>
    </c:choose>

    <form:form commandName="report" id="reportForm" name="reportForm" cssClass="form">
        <spring:hasBindErrors name="report">
            <p class="message error no-margin">
                <form:errors path="*"/>
            </p>
        </spring:hasBindErrors>

        <div class="columns">
            <div class="colx2-left">
                <div class="columns">
                    <div class="colx2-left">
                        <spring:bind path="report.name">
                            <form:label path="${status.expression}"><spring:message code="report.name"/></form:label>
                            <form:input path="${status.expression}" cssErrorClass="error" maxlength="255"/>
                        </spring:bind>
                    </div>

                    <div class="colx2-right">
                        <form:label path="ticketFilter"><spring:message code="ticketFilter.title"/></form:label>
                        <form:select path="ticketFilter" cssErrorClass="error">
                            <option value=""></option>
                            <c:if test="${fn:length(myFilters)>0}">
                                <optgroup label="<spring:message code="ticketFilter.myFilters"/>">
                                    <form:options items="${myFilters}" itemValue="id" itemLabel="name"/>
                                </optgroup>
                            </c:if>
                            <c:if test="${fn:length(pubFilters)>0}">
                                <optgroup label="<spring:message code="ticketFilter.publicFilters"/>">
                                    <form:options items="${pubFilters}" itemValue="id" itemLabel="name"/>
                                </optgroup>
                            </c:if>
                        </form:select>
                    </div>
                </div>
            </div>
            <div class="colx2-right">
                <form:checkbox path="privateReport"/>
                <spring:message code="report.markasprivate"/><br />

                <form:checkbox path="showDetail"/>
                <spring:message code="report.showDetail"/><br />

                <form:checkbox path="showReportByDate" onchange="showGroupByReport(this.checked)"/>
                <spring:message code="report.reportByDate"/>
            </div>
        </div>

        <fieldset id="groupByReport"<c:if test="${report.showReportByDate}">style="display:none"</c:if>>
            <legend><spring:message code="report.grouping"/></legend>
            <div class="columns">
                <div class="colx2-left">
                    <form:label path="groupBy"><spring:message code="report.groupBy"/></form:label>
                    <form:select path="groupBy" id="groupBy" onchange="regroupByBy()">
                        <form:option value="1"><spring:message code="report.group"/></form:option>
                        <form:option value="2"><spring:message code="report.location"/></form:option>
                        <form:option value="3"><spring:message code="report.category"/></form:option>
                        <form:option value="5"><spring:message code="report.technician"/></form:option>
                        <form:option value="6"><spring:message code="report.priority"/></form:option>
                        <form:option value="7"><spring:message code="report.status"/></form:option>
                        <%--<form:option value="8"><spring:message code="report.zenworks10Asset"/></form:option>--%>
                    </form:select>
                </div>
                <div class="colx2-right">
                    <form:checkbox path="reGroup" id="regroupDrilldown" onclick="hideRegroup(this.checked)"/>
                    <spring:message code="report.regroup"/>

                    <div id="groupByBy">
                        <form:select path="groupByBy">
                            <form:option value="1"><spring:message code="report.group"/></form:option>
                            <form:option value="2"><spring:message code="report.location"/></form:option>
                            <form:option value="3"><spring:message code="report.category"/></form:option>
                            <form:option value="4"><spring:message code="report.categoryOpt"/></form:option>
                            <form:option value="5"><spring:message code="report.technician"/></form:option>
                            <form:option value="6"><spring:message code="report.priority"/></form:option>
                            <form:option value="7"><spring:message code="report.status"/></form:option>
                            <%--<form:option value="8"><spring:message code="report.zenworks10Asset"/></form:option>--%>
                        </form:select>
                    </div>
                </div>
            </div>
        </fieldset>

        <fieldset id="dateBy"<c:if test="${not report.showReportByDate}">style="display:none"</c:if>>
            <legend><spring:message code="report.byDate"/></legend>
            <form:label path="byDate"><spring:message code="report.orderBy"/></form:label>
            <form:select path="byDate">
                <form:option value="0"><spring:message code="report.day"/></form:option>
                <form:option value="1"><spring:message code="report.week"/></form:option>
                <form:option value="2"><spring:message code="report.month"/></form:option>
                <form:option value="3"><spring:message code="report.year"/></form:option>
            </form:select>
        </fieldset>

        <fieldset>
            <legend><spring:message code="report.ordering"/></legend>
            <div class="columns">
                <div class="colx2-left">
                    <form:label path="groupByOrder"><spring:message code="report.orderBy"/></form:label>
                    <form:select path="groupByOrder" id="groupByOrder" onchange="changeReportGroupOrder();">
                        <form:option value="1"><spring:message code="report.alphabetically"/></form:option>
                        <form:option value="2"><spring:message code="report.greatestNumberOfTickts"/></form:option>
                    </form:select>
                </div>
                <div class="colx2-right">
                    <span id="reportShowTopSpan">
                        <form:label path="showTop"><spring:message code="report.showTop"/></form:label>
                        <form:select path="showTop">
                            <form:option value="">&nbsp;</form:option>
                            <form:option value="10">10</form:option>
                            <form:option value="25">25</form:option>
                            <form:option value="50">50</form:option>
                            <form:option value="75">75</form:option>
                            <form:option value="100">100</form:option>
                        </form:select>
                    </span>
                </div>
            </div>
        </fieldset>

        <fieldset>
            <legend><spring:message code="report.format"/></legend>
            <div class="columns">
                <div class="colx2-left">
                    <form:label path="format"><spring:message code="report.reportFormat"/></form:label>
                    <form:select path="format">
                        <form:option value="pdf"><spring:message code="ticketList.exportPDF"/></form:option>
                        <form:option value="html"><spring:message code="report.html"/></form:option>
                        <form:option value="xls"><spring:message code="ticketList.exportExcel"/></form:option>
                        <form:option value="csv"><spring:message code="assetTracker.csv"/></form:option>
                    </form:select>
                </div>
                <div class="colx2-right">
                    <form:checkbox path="showGraph" onclick="togglePieBar(this);"/>
                    <spring:message code="report.showGraph"/><br />

                    <div id="showPieBar" <c:if test="${not report.showGraph}">style="display:none"</c:if>>
                        <form:radiobutton path="showPieChart" value="true" id="pi"/>
                        <label for="pi" class="inline"><spring:message code="report.pie"/></label>
                        <form:radiobutton path="showPieChart" value="false" id="bar"/>
                        <label for="bar" class="inline"><spring:message code="report.bar"/></label>
                    </div>
                </div>
            </div>
        </fieldset>

        <c:choose>
            <c:when test="${scheduledReports}">
                <form:checkbox path="showRecurrenceSchedule" id="showRecurr" onchange="toggleScheduler(this);"/>
                <spring:message code="report.enableScheduler"/><br/><br/>
                <fieldset id="scheduler"<c:if test="${not report.showRecurrenceSchedule}">style="display:none"</c:if>>
                    <legend><spring:message code="report.scheduling"/></legend>
                    <div class="columns">
                        <div class="colx3-left">
                            <spring:bind path="schedule.recurrenceSchedule.pattern">
                                <form:label path="schedule.recurrenceSchedule.pattern"><spring:message code="recurrence.recurrencePattern"/></form:label>
                                <ul class="checkable-list">
                                    <li>
                                        <input type="radio" name="${status.expression}" id="patternDaily" onclick="showDailyOptions();"
                                               value="1" <c:if test="${status.value eq 1}"> checked="checked" </c:if> />
                                        <label for="patternDaily"><spring:message code="recurrence.daily"/></label>
                                    </li>
                                    <li>
                                        <input type="radio" name="${status.expression}" id="patternWeekly"
                                               onclick="showWeeklyOptions();" value="2"
                                                <c:if test="${status.value eq 2}"> checked="checked" </c:if> />
                                        <label for="patternWeekly"><spring:message code="recurrence.weekly"/></label>
                                    </li>
                                    <li>
                                        <input type="radio" name="${status.expression}" id="patternMonthly"
                                               onclick="showMonthlyOptions();" value="3"
                                                <c:if test="${status.value eq 3}"> checked="checked" </c:if> />
                                        <label for="patternMonthly"><spring:message code="recurrence.monthly"/></label>
                                    </li>
                                    <li>
                                        <input type="radio" name="${status.expression}" id="patternYearly"
                                               onclick="showYearlyOptions();" value="4"
                                                <c:if test="${status.value eq 4}">
                                                    checked="checked"
                                                </c:if>
                                                />
                                        <label for="patternYearly"><spring:message code="recurrence.yearly"/></label>
                                    </li>
                                </ul>
                            </spring:bind>
                        </div>
                        <div class="colx3-right-double">
                            <div id="dailyOptions" <c:if test="${report.schedule.recurrenceSchedule.pattern != 1}">style="display:none;"</c:if>>
                                <spring:bind path="schedule.recurrenceSchedule.dailyInterval">
                                    <spring:message code="recurrence.recurEvery"/>:
                                    <input name="${status.expression}" value="${status.value}" dojotype="dijit.form.NumberSpinner"
                                           constraints="{min:1,max:365,places:0}" style="width:50px"/>
                                    <spring:message code="recurrence.days"/>
                                </spring:bind>
                            </div>

                            <div id="weeklyOptions" <c:if test="${report.schedule.recurrenceSchedule.pattern != 2}">style="display:none;"</c:if>>
                                <spring:bind path="schedule.recurrenceSchedule.weeklyInterval">
                                    <spring:message code="recurrence.recurEvery"/>:
                                    <input name="${status.expression}" value="${status.value}" dojotype="dijit.form.NumberSpinner"
                                           constraints="{min:1,max:365,places:0}" style="width:50px"/>
                                    <spring:message code="recurrence.weekson"/>
                                </spring:bind>
                                <div class="columns">
                                    <div class="colx2-left">
                                        <ul class="checkable-list">
                                            <li>
                                                <spring:bind path="schedule.recurrenceSchedule.onSunday">
                                                    <input type="checkbox" name="${status.expression}" value="true"
                                                            <c:if test="${status.value eq 'true'}">
                                                                checked="checked"
                                                            </c:if>
                                                            />
                                                    <input type="hidden" name="_${status.expression}"/>
                                                    <spring:message code="recurrence.sunday"/>
                                                </spring:bind>
                                            </li>
                                            <li>
                                                <spring:bind path="schedule.recurrenceSchedule.onMonday">
                                                    <input type="checkbox" name="${status.expression}" value="true"
                                                            <c:if test="${status.value eq 'true'}">
                                                                checked="checked"
                                                            </c:if>
                                                            />
                                                    <input type="hidden" name="_${status.expression}"/>
                                                    <spring:message code="recurrence.monday"/>
                                                </spring:bind>
                                            </li>
                                            <li>
                                                <spring:bind path="schedule.recurrenceSchedule.onTuesday">
                                                    <input type="checkbox" name="${status.expression}" value="true"
                                                            <c:if test="${status.value eq 'true'}">
                                                                checked="checked"
                                                            </c:if>
                                                            />
                                                    <input type="hidden" name="_${status.expression}"/>
                                                    <spring:message code="recurrence.tuesday"/>
                                                </spring:bind>
                                            </li>
                                            <li>
                                                <spring:bind path="schedule.recurrenceSchedule.onWednesday">
                                                    <input type="checkbox" name="${status.expression}" value="true"
                                                            <c:if test="${status.value eq 'true'}">
                                                                checked="checked"
                                                            </c:if>
                                                            />
                                                    <input type="hidden" name="_${status.expression}"/>
                                                    <spring:message code="recurrence.wednesday"/>
                                                </spring:bind>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="colx2-right">
                                        <ul class="checkable-list">
                                            <li>
                                                <spring:bind path="schedule.recurrenceSchedule.onThursday">
                                                    <input type="checkbox" name="${status.expression}" value="true"
                                                            <c:if test="${status.value eq 'true'}">
                                                                checked="checked"
                                                            </c:if>
                                                            />
                                                    <input type="hidden" name="_${status.expression}"/>
                                                    <spring:message code="recurrence.thursday"/>
                                                </spring:bind>
                                            </li>
                                            <li>
                                                <spring:bind path="schedule.recurrenceSchedule.onFriday">
                                                    <input type="checkbox" name="${status.expression}" value="true"
                                                            <c:if test="${status.value eq 'true'}">
                                                                checked="checked"
                                                            </c:if>
                                                            />
                                                    <input type="hidden" name="_${status.expression}"/>
                                                    <spring:message code="recurrence.friday"/>
                                                </spring:bind>
                                            </li>
                                            <li>
                                                <spring:bind path="schedule.recurrenceSchedule.onSaturday">
                                                    <input type="checkbox" name="${status.expression}" value="true"
                                                            <c:if test="${status.value eq 'true'}">
                                                                checked="checked"
                                                            </c:if>
                                                            />
                                                    <input type="hidden" name="_${status.expression}"/>
                                                    <spring:message code="recurrence.saturday"/>
                                                </spring:bind>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div id="monthlyOptions" <c:if test="${report.schedule.recurrenceSchedule.pattern != 3}">style="display:none;"</c:if>>
                                <p>
                                    <spring:bind path="schedule.recurrenceSchedule.monthlySchedule">
                                        <input type="radio" id="monthlyScheduleDayOfMonth" name="${status.expression}" value="1"
                                                <c:if test="${status.value eq 1}">
                                                    checked="checked"
                                                </c:if>
                                                />
                                        <label for="monthlyScheduleDayOfMonth"><spring:message code="recurrence.day"/></label>
                                    </spring:bind>

                                    <spring:bind path="schedule.recurrenceSchedule.monthlyDayOfMonth">
                                        <input name="${status.expression}" value="${status.value}"
                                               dojotype="dijit.form.NumberSpinner"
                                               constraints="{min:1,max:31,places:0}"
                                               style="width:50px"/>
                                    </spring:bind>

                                    <spring:message code="recurrence.ofEvery"/>
                                    <spring:bind path="schedule.recurrenceSchedule.monthlyDayOfMonthOccurrence">
                                        <input name="${status.expression}" value="${status.value}"
                                               dojotype="dijit.form.NumberSpinner"
                                               constraints="{min:1,max:50,places:0}"
                                               style="width:50px"/>
                                    </spring:bind>
                                    <spring:message code="recurrence.months"/>
                                </p>
                                <p>
                                    <spring:bind path="schedule.recurrenceSchedule.monthlySchedule">
                                        <input type="radio" id="monthlyScheduleDayOfWeek" name="${status.expression}" value="2"
                                                <c:if test="${status.value eq 2}">
                                                    checked="checked"
                                                </c:if>
                                                />
                                        <label for="monthlyScheduleDayOfWeek"><spring:message code="recurrence.the"/></label>
                                    </spring:bind>

                                    <spring:bind path="schedule.recurrenceSchedule.monthlyDayOfWeekInterval">
                                        <select name="${status.expression}" value="${status.value}">
                                            <option value="1"><spring:message code="recurrence.first"/></option>
                                            <option value="2"><spring:message code="recurrence.second"/></option>
                                            <option value="3"><spring:message code="recurrence.third"/></option>
                                            <option value="4"><spring:message code="recurrence.fourth"/></option>
                                            <option value="5"><spring:message code="recurrence.fifth"/></option>
                                        </select>
                                    </spring:bind>

                                    <spring:bind path="schedule.recurrenceSchedule.monthlyDayOfWeekDay">
                                        <select name="${status.expression}" value="${status.value}">
                                            <option value="1"><spring:message code="recurrence.sunday"/></option>
                                            <option value="2"><spring:message code="recurrence.monday"/></option>
                                            <option value="3"><spring:message code="recurrence.tuesday"/></option>
                                            <option value="4"><spring:message code="recurrence.wednesday"/></option>
                                            <option value="5"><spring:message code="recurrence.thursday"/></option>
                                            <option value="6"><spring:message code="recurrence.friday"/></option>
                                            <option value="7"><spring:message code="recurrence.saturday"/></option>
                                        </select>
                                    </spring:bind>

                                    <spring:message code="recurrence.ofEvery"/>
                                    <spring:bind path="schedule.recurrenceSchedule.monthlyDayOfWeekOccurrence">
                                        <input name="${status.expression}" value="${status.value}"
                                               dojotype="dijit.form.NumberSpinner"
                                               constraints="{min:1,max:50,places:0}"
                                               style="width:50px"/>
                                    </spring:bind>
                                    <spring:message code="recurrence.months"/>
                                </p>
                            </div>

                            <div id="yearlyOptions" <c:if test="${report.schedule.recurrenceSchedule.pattern != 4}">style="display:none;"</c:if>>
                                <p>
                                    <spring:bind path="schedule.recurrenceSchedule.yearlySchedule">
                                        <input type="radio" id="yearlyScheduleDayOfYear" name="${status.expression}" value="1"
                                                <c:if test="${status.value eq 1}">
                                                    checked="checked"
                                                </c:if>
                                                />
                                        <label for="yearlyScheduleDayOfYear"><spring:message code="recurrence.day"/></label>
                                    </spring:bind>

                                    <spring:bind path="schedule.recurrenceSchedule.yearlyDayOfYearDay">
                                        <input name="${status.expression}" value="${status.value}"
                                               dojotype="dijit.form.NumberSpinner"
                                               constraints="{min:1,max:31,places:0}"
                                               style="width:50px"/>
                                    </spring:bind>

                                    <spring:message code="recurrence.of"/>
                                    <spring:bind path="schedule.recurrenceSchedule.yearlyDayOfYearMonth">
                                        <select name="${status.expression}" value="${status.value}">
                                            <option value="1"><spring:message code="recurrence.january"/></option>
                                            <option value="2"><spring:message code="recurrence.february"/></option>
                                            <option value="3"><spring:message code="recurrence.march"/></option>
                                            <option value="4"><spring:message code="recurrence.april"/></option>
                                            <option value="5"><spring:message code="recurrence.may"/></option>
                                            <option value="6"><spring:message code="recurrence.june"/></option>
                                            <option value="7"><spring:message code="recurrence.july"/></option>
                                            <option value="8"><spring:message code="recurrence.august"/></option>
                                            <option value="9"><spring:message code="recurrence.september"/></option>
                                            <option value="10"><spring:message code="recurrence.october"/></option>
                                            <option value="11"><spring:message code="recurrence.november"/></option>
                                            <option value="12"><spring:message code="recurrence.december"/></option>
                                        </select>
                                    </spring:bind>
                                </p>
                                <p>
                                    <spring:bind path="schedule.recurrenceSchedule.yearlySchedule">
                                        <input type="radio" id="yearlyScheduleDayOfMonth" name="${status.expression}" value="2"
                                                <c:if test="${status.value eq 2}">
                                                    checked="checked"
                                                </c:if>
                                                />
                                        <label for="yearlyScheduleDayOfMonth"><spring:message code="recurrence.the"/></label>
                                    </spring:bind>

                                    <spring:bind path="schedule.recurrenceSchedule.yearlyDayOfMonthInterval">
                                        <select name="${status.expression}" value="${status.value}">
                                            <option value="1"><spring:message code="recurrence.first"/></option>
                                            <option value="2"><spring:message code="recurrence.second"/></option>
                                            <option value="3"><spring:message code="recurrence.third"/></option>
                                            <option value="4"><spring:message code="recurrence.fourth"/></option>
                                            <option value="5"><spring:message code="recurrence.fifth"/></option>
                                        </select>
                                    </spring:bind>

                                    <spring:bind path="schedule.recurrenceSchedule.yearlyDayOfMonthDay">
                                        <select name="${status.expression}" value="${status.value}">
                                            <option value="1"><spring:message code="recurrence.sunday"/></option>
                                            <option value="2"><spring:message code="recurrence.monday"/></option>
                                            <option value="3"><spring:message code="recurrence.tuesday"/></option>
                                            <option value="4"><spring:message code="recurrence.wednesday"/></option>
                                            <option value="5"><spring:message code="recurrence.thursday"/></option>
                                            <option value="6"><spring:message code="recurrence.friday"/></option>
                                            <option value="7"><spring:message code="recurrence.saturday"/></option>
                                        </select>
                                    </spring:bind>

                                    <spring:message code="recurrence.of"/>
                                    <spring:bind path="schedule.recurrenceSchedule.yearlyDayOfMonthMonth">
                                        <select name="${status.expression}" value="${status.value}">
                                            <option value="1"><spring:message code="recurrence.january"/></option>
                                            <option value="2"><spring:message code="recurrence.february"/></option>
                                            <option value="3"><spring:message code="recurrence.march"/></option>
                                            <option value="4"><spring:message code="recurrence.april"/></option>
                                            <option value="5"><spring:message code="recurrence.may"/></option>
                                            <option value="6"><spring:message code="recurrence.june"/></option>
                                            <option value="7"><spring:message code="recurrence.july"/></option>
                                            <option value="8"><spring:message code="recurrence.august"/></option>
                                            <option value="9"><spring:message code="recurrence.september"/></option>
                                            <option value="10"><spring:message code="recurrence.october"/></option>
                                            <option value="11"><spring:message code="recurrence.november"/></option>
                                            <option value="12"><spring:message code="recurrence.december"/></option>
                                        </select>
                                    </spring:bind>
                                </p>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="columns">
                        <div class="colx2-left">
                            <label><spring:message code="recurrence.rangeOfRecurrence"/></label>
                            <spring:bind path="schedule.recurrenceSchedule.rangeStartDate">
                                <spring:message code="recurrence.start"/>:
                                <input type="text" dojotype="dijit.form.DateTextBox" name="${status.expression}"
                                       value="${status.value}"/>
                            </spring:bind>
                        </div>
                        <div class="colx2-right">
                            <p>
                                <spring:bind path="schedule.recurrenceSchedule.rangeEnd">
                                    <input type="radio" name="${status.expression}" value="1"
                                            <c:if test="${status.value eq 1}"> checked="checked" </c:if> />
                                    <spring:message code="recurrence.endAfter"/>:
                                </spring:bind>

                                <spring:bind path="schedule.recurrenceSchedule.rangeEndAfterOccurence">
                                    <input name="${status.expression}" value="${status.value}" dojotype="dijit.form.NumberSpinner"
                                           constraints="{min:1,max:999,places:0}" style="width:50px"/>
                                    <spring:message code="recurrence.occurrences"/>
                                </spring:bind>
                            </p>
                            <p>
                                <spring:bind path="schedule.recurrenceSchedule.rangeEnd">
                                    <input type="radio" name="${status.expression}" value="2"
                                            <c:if test="${status.value eq 2}"> checked="checked" </c:if>/>
                                    <spring:message code="recurrence.endBy"/>:
                                </spring:bind>

                                <spring:bind path="schedule.recurrenceSchedule.rangeEndByDate">
                                    <input type="text" dojotype="dijit.form.DateTextBox" name="${status.expression}"
                                           value="${status.value}"/>
                                </spring:bind>
                            </p>
                            <p>
                                <spring:bind path="schedule.recurrenceSchedule.rangeEnd">
                                    <input type="radio" name="${status.expression}" value="3"
                                            <c:if test="${status.value eq 3}"> checked="checked" </c:if> />
                                    <spring:message code="recurrence.endNever"/>
                                </spring:bind>
                            </p>
                        </div>
                    </div>
                <hr />
                <div>
                    <p>
                        <label><spring:message code="recurrence.emailtosend"/></label>
                        <form:label path="schedule.emailMessageTemplate.to"><spring:message code="mail.to"/></form:label>
                        <spring:bind path="schedule.emailMessageTemplate.to">
                            <form:input path="schedule.emailMessageTemplate.to" cssErrorClass="full-width error" cssClass="full-width"/>
                        </spring:bind>
                    </p>

                    <p>
                        <form:label path="schedule.emailMessageTemplate.subject"><spring:message code="mail.subject"/></form:label>
                        <spring:bind path="schedule.emailMessageTemplate.subject">
                            <input type="text" name="${status.expression}" id="${status.expression}"
                                   value="<c:out value="${status.value}"/>" class="full-width"/>
                        </spring:bind>
                    </p>

                    <p>
                        <form:label path="schedule.emailMessageTemplate.body"><spring:message code="mail.body"/></form:label>
                        <spring:bind path="schedule.emailMessageTemplate.body">
                            <textarea class="full-width" rows="5" name="${status.expression}"
                                      id="${status.expression}"><c:out value="${status.value}"/></textarea>
                        </spring:bind>
                    </p>
                </div>
                </fieldset>
            </c:when>
            <c:otherwise>
                <strong>SCHEDULED AUTOMATIC REPORTS</strong>
                <p>
                    Scheduled Reports are not available in the free edition of <i>eReferrals</i>&trade;. To upgrade
                    to eReferrals&reg;'s
                    Enterprise
                    edition, or for more information, please contact a Sales Representative at <a
                        href="mailto:sales@grouplink.net">sales@grouplink.net</a> or
                    801-335-0702.
                </p>

                <p>
                    Have your custom reports set on a scheduled recurrence pattern to automatically run and email your boss,
                    and any
                    other individuals you desire, in time for your weekly meetings. Reports now come to you, with the
                    correct
                    numbers and on time, as if your administrative assistant slaved all day to produce them. No more number
                    crunching right before meetings!
                </p>

                <p>To view the capability of eReferrals' Scheduled Automatic Reports, watch the video below.</p>

                <p>
                    <object width="500" height="405">
                        <param name="movie"
                               value="http://www.youtube.com/v/KIYYX1bPm8o&amp;hl=en_US&amp;fs=1?rel=0&amp;border=1"/>
                        <param name="allowFullScreen" value="true"/>
                        <param name="allowscriptaccess" value="always"/>
                        <embed src="http://www.youtube.com/v/KIYYX1bPm8o&amp;hl=en_US&amp;fs=1?rel=0&amp;border=1"
                               type="application/x-shockwave-flash" allowscriptaccess="always"
                               allowfullscreen="true" width="500" height="405"></embed>
                    </object>

                </p>
            </c:otherwise>
        </c:choose>
        <p class="grey-bg no-margin">
            <button type="submit" href="#"><spring:message code="report.saveReport"/></button>
        </p>
    </form:form>
</div>
</section>
</body>
