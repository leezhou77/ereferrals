<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="groups" type="java.util.List<net.grouplink.ehelpdesk.domain.Group>"--%>

<div style="max-height:500px;width:400px;overflow:auto;">
    <c:set var="rowIndex" value="0"/>
    <c:forEach var="group" items="${groups}">
        <c:forEach var="cat" items="${group.categories}">
            <c:if test="${cat.active}">
                <div dojoType="dijit.TitlePane" title="${group.name} - ${cat.name}" id="div_${rowIndex}" open="${rowIndex eq 0}" onclick="catOptFetcherShowTitlePane('${group.id}', '${cat.id}', '${rowIndex}');">
                    <div id="catOptFetcherDiv_${cat.id}">
                        <input type="checkbox" dojoType="dijit.form.CheckBox" id="sall_${rowIndex}"/>
                        <label for="sall_${rowIndex}">
                            <spring:message code="ticketFilter.selectAll"/>
                        </label>
                        <hr>
                        <c:if test="${rowIndex eq 0}">
                            <div id="catOptFetcher_cat_${cat.id}" style="display: none"></div>

                            <ul>
                                <c:forEach var="catOpt" items="${cat.categoryOptions}" varStatus="innerRow">
                                    <c:if test="${catOpt.active}">
                                        <li class="small-margin">
                                            <input type="checkbox" name="cbx_${rowIndex}" id="cbx_${rowIndex}_${catOpt.id}"
                                                   dojoType="dijit.form.CheckBox" value="${catOpt.id}"/>
                                            <select dojoType="dijit.form.FilteringSelect" id="cbx_${rowIndex}_${catOpt.id}_dd"
                                                    style="width:100px">
                                                <option value="9"><spring:message code="ticketFilter.op.equals"/></option>
                                                <option value="10"><spring:message code="ticketFilter.op.notEquals"/></option>
                                            </select>
                                            <label for="cbx_${rowIndex}_${catOpt.id}"><c:out value="${catOpt.name}"/></label>
                                        </li>
                                    </c:if>
                                </c:forEach>
                            </ul>
                        </c:if>
                    </div>
                </div>
            </c:if>
            <c:set var="rowIndex" value="${rowIndex + 1}"/>
        </c:forEach>
    </c:forEach>

    <c:remove var="rowIndex"/>

</div>


<div>
    <button dojoType="dijit.form.Button" type="submit">
        <spring:message code="ticketFilter.apply"/>
    </button>
</div>

