<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="ticketFilter" type="net.grouplink.ehelpdesk.domain.ticket.TicketFilter"--%>
<%--@elvariable id="tfId" type="java.lang.Integer"--%>
<%--@elvariable id="status" type="java.util.List<net.grouplink.ehelpdesk.domain.Status>"--%>
<%--@elvariable id="priorities" type="java.util.List<net.grouplink.ehelpdesk.domain.TicketPriority>"--%>
<%--@elvariable id="rc" type="org.springframework.web.servlet.support.RequestContext"--%>
<%--@elvariable id="ticketCount" type="java.lang.Integer"--%>

<html>
<head>
    <title><spring:message code="ticketFilter.searchResults"/></title>

    <%@ include file="/WEB-INF/jsp/ehd-tf/ticketListFragment.jsp" %>

    <script type="text/javascript">

        function exportList(format, selectedOnly) {
            var selectedTids;
            if (selectedOnly) {
                selectedTids = getSelectedTidsFromGrid("gridContainer_grid");
            }

            var yerl;
            if (format == 'pdf') {
                yerl = "<c:url value="/tf/ticketFilterList.pdf"/>?tfId=";
            } else if (format == 'excel') {
                yerl = "<c:url value="/tf/ticketFilterList.xls"/>?tfId=";
            }

            <c:if test="${not empty tfId}">
                yerl += "${tfId}";
            </c:if>

            if (selectedOnly) {
                yerl += "&selectedTids=";
                for (var i=0; i<selectedTids.length; i++) {
                    if (i > 0) yerl += ",";
                    yerl += selectedTids[i];
                }
            }

            window.open(yerl);
        }

        function ticketFilterResultsInit() {
        <c:choose>
        <c:when test="${not empty tfId}">
            createGrid("gridContainer", '${tfId}');
        </c:when>
        <c:otherwise>
            createGrid("gridContainer");
        </c:otherwise>
        </c:choose>
            dojo.parser.parse();
        }
        dojo.addOnLoad(ticketFilterResultsInit);
    </script>
</head>
<body>
<article class="container_12">
<br>
<section class="grid_12">
    <div class="block-content">
        <h1><c:out value="${ticketFilter.name}"/> (<c:out value="${ticketCount}" />)</h1>

        <div class="block-controls">
            <ul class="controls-buttons">
                <authz:authorize ifAnyGranted="ROLE_ADMINISTRATOR, ROLE_MANAGER, ROLE_TECHNICIAN">
                <li>
                    <button dojotype="dijit.form.ComboButton" onclick="showMUDialog('gridContainer');">
                        <span>
                            <spring:message code="ticketList.massUpdate" text="Mass Update"/>
                        </span>

                        <div dojotype="dijit.Menu" toggle="fade">
                            <div dojotype="dijit.MenuItem"
                                 onclick="deleteSelected('gridContainer', '<c:out value="${tfId}" default="-1"/>');">
                                <spring:message code="global.delete"/>
                            </div>
                        </div>
                    </button>
                </li>
                </authz:authorize>
                <li>
                    <div dojotype="dijit.form.ComboButton" onclick="exportList('pdf');">
                        <div>
                            <img src="<c:url value="/images/export_db_16.png"/>" alt="">
                            <spring:message code="ticketList.export"/>
                        </div>

                        <div dojotype="dijit.Menu" toggle="fade" class="tenpix">
                            <div dojotype="dijit.PopupMenuItem" class="tenpix">
                                <span><spring:message code="global.all" text="All"/></span>
                                <div dojotype="dijit.Menu" toggle="fade">
                                    <div dojotype="dijit.MenuItem" onclick="exportList('pdf');" class="tenpix"
                                         iconClass="exportPdfIcon">
                                        <spring:message code="ticketList.exportPDF"/>
                                    </div>
                                    <div dojotype="dijit.MenuItem" onclick="exportList('excel');" class="tenpix"
                                         iconClass="exportXcelIcon">
                                        <spring:message code="ticketList.exportExcel"/>
                                    </div>
                                </div>
                            </div>
                            <div dojotype="dijit.PopupMenuItem">
                                <span><spring:message code="global.selected" text="Selected"/></span>
                                <div dojotype="dijit.Menu" toggle="fade">
                                    <div dojotype="dijit.MenuItem" onclick="exportList('pdf', true);" class="tenpix"
                                         iconClass="exportPdfIcon">
                                        <spring:message code="ticketList.exportPDF"/>
                                    </div>
                                    <div dojotype="dijit.MenuItem" onclick="exportList('excel', true);" class="tenpix"
                                         iconClass="exportXcelIcon">
                                        <spring:message code="ticketList.exportExcel"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <tags:ticketListGrid gridContainerId="gridContainer" massUpdate="true" ticketFilterId="${tfId}"
             priorities="${priorities}" status="${status}" containerStyle="height:550px"/>
    </div>
</section>
</article>
</body>
</html>
