<%@ include file="/WEB-INF/jsp/include.jsp" %>
<jsp:useBean id="ticket" scope="request" type="net.grouplink.ehelpdesk.domain.Ticket"/>
<jsp:useBean id="ticketPriorities" scope="request" type="java.util.List"/>
<jsp:useBean id="ticketHistories" scope="request" type="java.util.List"/>
<jsp:useBean id="locations" scope="request" type="java.util.List"/>
<jsp:useBean id="ticketStatus" scope="request" type="java.util.List"/>
<jsp:useBean id="initGroups" scope="request" type="java.util.List"/>
<jsp:useBean id="initCats" scope="request" type="java.util.List"/>
<jsp:useBean id="initCatOpts" scope="request" type="java.util.List"/>
<jsp:useBean id="initAssignedTo" scope="request" type="java.util.List"/>
<jsp:useBean id="userRoleGroupList" scope="request" type="java.util.List"/>
<jsp:useBean id="customFields" scope="request" type="java.util.Map"/>
<jsp:useBean id="cFields" scope="request" type="java.util.List"/>
<jsp:useBean id="ldapUserList" scope="request" type="java.util.List"/>
<jsp:useBean id="ldapFieldList" scope="request" type="java.util.List"/>
<jsp:useBean id="userList" scope="request" type="java.util.List"/>
<jsp:useBean id="tinfo_details_state" scope="request" type="java.lang.String"/>
<jsp:useBean id="tdescription_state" scope="request" type="java.lang.String"/>
<jsp:useBean id="hist_details_state" scope="request" type="java.lang.String"/>
<jsp:useBean id="showSearchPage" scope="request" type="java.lang.String"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
    <title><spring:message code="global.systemName"/></title>
    <style type="text/css">
        .bar { width: 100%; color: #878787; font-size: 10px; font-weight: bold; background: #D6D6D6; vertical-align: middle; padding: 4px 3px 2px 5px; margin: 3px 0 3px 0; border: 1px solid #878787; text-decoration:none; }
        .barnob { width: 100%; color: #878787; font-size: 10px; font-weight: bold; background: #D6D6D6; vertical-align: middle; text-decoration:none; }
        .bar a {text-decoration:none}
        img { border:0; }
        .commentHeader { width: 100%; color: #666666; background: #E3E3E3; vertical-align: middle; padding: 4px 5px 2px 5px; margin: 3px 0 3px 0; font-size:10px;}
        .commentBody{ color: #000000; background: white; padding: 4px 5px 2px 5px; font-size:10px;}
        .box{ border: 1px solid #D6D6D6; font-size:10px;}

        table .paged { border: 0; margin-left: 5px; margin-right: 5px; border-collapse: collapse; }
        .even { background: #eeeeff; color: #666666; }
        .odd { background: white; color: #666666 }
    </style>
    <script type="text/javascript">

        function toggleExpansion(imageId, detailDiv){
            var image = document.getElementById(imageId);
            var theDiv = document.getElementById(detailDiv);
            if(image.src.indexOf("minus.gif") > -1){
                hideExpansion(theDiv, image);
                document.getElementById(detailDiv+"_state").value = "none";
            } else {
                showExpansion(theDiv, image);
                document.getElementById(detailDiv+"_state").value = "block";
            }
        }

        function showExpansion(exp, img){
            img.src = '<c:url value="/images/minus.gif"/>';
            exp.style.display = "block";
        }

        function hideExpansion(exp, img){
            img.src = '<c:url value="/images/plus.gif"/>';
            exp.style.display = "none";
        }

        function findUser(){
            document.getElementById('ticketEditPage').style.display = 'none';
            document.getElementById('findUserPage').style.display = 'block';
        }

        function chooseUser(dn){
            document.getElementById('searchForUser').value = '';
            document.getElementById('selectedUser').value = dn;
            document.getElementById('contact').value = dn;
            document.forms[0].submit();
        }

        function startSearch(){
            document.getElementById('searchForUser').value = 'search';
            document.forms[0].submit();
        }

        function cancelSearch(){
            document.getElementById('searchForUser').value = '';
            document.getElementById('selectedUser').value = '';
            document.getElementById('findUserPage').style.display = 'none';
            document.getElementById('ticketEditPage').style.display = 'block';
        }

        function addComment(){
            document.getElementById('ticketEditPage').style.display = 'none';
            document.getElementById('addCommentPage').style.display = 'block';
        }

        function saveTHist(){
            var thsubj = document.getElementById('thist_subject').value.replace(/^\s+|\s+$/g, '') ;

            var errs = '';
            if(isEmpty(thsubj)) errs += '<spring:message code="ticket.subject"/>\n';

            if(!isEmpty(errs)){
                alert('<spring:message code="global.requiredFields"/>:\n\n' + errs);
            } else {
                document.getElementById('addingNewComment').value = 'yes';
                document.forms[0].submit();
            }
        }

        function cancelTHist(){
            document.getElementById('addCommentPage').style.display = 'none';
            document.getElementById('ticketEditPage').style.display = 'block';
            // clear the inputs
            document.getElementById('thist_subject').value = '';
            document.getElementById('thist_note').value = '';
        }

        function init(){
            if('${tinfo_details_state}' == 'block')
                showExpansion(document.getElementById('tinfo_details'), document.getElementById('tinfoImg'));
            else if('${tinfo_details_state}' == 'none')
                hideExpansion(document.getElementById('tinfo_details'), document.getElementById('tinfoImg'));

            if('${tdescription_state}' == 'block')
                showExpansion(document.getElementById('tdescription'), document.getElementById('descImg'));
            else if('${tdescription_state}' == 'none')
                hideExpansion(document.getElementById('tdescription'), document.getElementById('descImg'));

            if('${hist_details_state}' == 'block')
                showExpansion(document.getElementById('hist_details'), document.getElementById('thistImg'));
            else if('${hist_details_state}' == 'none')
                hideExpansion(document.getElementById('hist_details'), document.getElementById('thistImg'));
        }

        function ddChanged(theDD) {
            document.getElementById('ddSubmit').value = theDD;
            document.forms[0].submit();
        }

        function isEmpty(s){
            return ((s == null) || (s.length == 0))
        }

        function saveForm(){
            // Check for an incomplete form
            var contact = document.getElementById('contactUser').value.replace(/^\s+|\s+$/g, '') ;
            var status = document.getElementById('statusSelection').value;
            var location = document.getElementById('location').value;
            var group = document.getElementById('group').value;
            var category = document.getElementById('category').value;
            var categoryOpt = document.getElementById('categoryOption').value;
            var assignee = document.getElementById('assignedTo').value;
            var subject = document.getElementById('subject').value.replace(/^\s+|\s+$/g, '') ;

            var errs = '';
            if(isEmpty(contact)) errs += '<spring:message code="ticket.contact"/>\n';
            if(isEmpty(status)) errs += '<spring:message code="ticket.status"/>\n';
            if(isEmpty(location)) errs += '<spring:message code="ticket.location"/>\n';
            if(isEmpty(group)) errs += '<spring:message code="ticket.group"/>\n';
            if(isEmpty(category)) errs += '<spring:message code="ticket.category"/>\n';
            if(isEmpty(categoryOpt)) errs += '<spring:message code="ticket.categoryOption"/>\n';
            if(isEmpty(assignee)) errs += '<spring:message code="ticket.assignedTo"/>\n';
            if(isEmpty(subject)) errs += '<spring:message code="ticket.subject"/>\n';

            // Check for required custom fields
            var custFieldName;
            <c:forEach var="cfield" items="${cFields}"><c:if test="${cfield.required}">
            custFieldName = 'cf_${cfield.id}_' + '<c:choose><c:when test="${not empty ticket.id}"><c:out value="${ticket.id}"/></c:when><c:otherwise>new</c:otherwise></c:choose>';
            if(isEmpty(document.getElementById(custFieldName).value.replace(/^\s+|\s+$/g, ''))) errs += '${cfield.name}\n';</c:if></c:forEach>

            if(!isEmpty(errs)){
                alert('<spring:message code="global.requiredFields"/>:\n\n' + errs);
            } else {
                document.forms[0].submit();
            }
        }

    </script>
</head>
<body onload="init();">
<form action="<c:url value="/pda/ticketEdit.glml"/> " method="POST">

<input type="hidden" name="tid" id="tid" value="${ticket.id}"/>
<input type="hidden" name="selectedUser" id="selectedUser" value=""/>
<input type="hidden" name="searchForUser" id="searchForUser" value=""/>
<input type="hidden" name="addingNewComment" id="addingNewComment" value=""/>
<input type="hidden" name="ddSubmit" id="ddSubmit" value=""/>
<input type="hidden" name="tinfo_details_state" id="tinfo_details_state" value="${tinfo_details_state}"/>
<input type="hidden" name="tdescription_state" id="tdescription_state" value="${tdescription_state}"/>
<input type="hidden" name="hist_details_state" id="hist_details_state" value="${hist_details_state}"/>

<table width="100%" style="font-size:10px">
    <tr>
        <th align="left">
            <c:out value="${sessionScope['User'].firstName}"/>
            <c:out value="${sessionScope['User'].lastName}"/>
        </th>
        <th>
            <c:choose>
                <c:when test="${not empty ticket.id}">
                    #<c:out value="${ticket.id}"/>
                </c:when>
                <c:otherwise>
                    <spring:message code="ticket.newTicket"/>
                </c:otherwise>
            </c:choose>
        </th>
        <th align="right">
            <a href="<c:url value="/logout.glml"/>">
                <spring:message code="header.signout"/>
            </a>
        </th>
    </tr>
    <tr>
        <th colspan="3" style="text-align:left;font-weight:normal;">
            <a href="<c:url value="/pda/ticketList.glml"/>"><spring:message code="header.myTickets"/></a>
        </th>
    </tr>
</table>

<div id="findUserPage" style="display:<c:choose><c:when test="${showSearchPage == 'true'}">block</c:when><c:otherwise>none</c:otherwise></c:choose>">
    <table style="font-size:10px">
        <tr>
            <td colspan="2"><hr></td>
        </tr>
        <tr>
            <td><strong> <spring:message code="user.lastName"/>:</strong></td>
            <td><input type="text" name="usearch_lastName" id="usearch_lastName"/></td>
        </tr>
        <tr>
            <td><strong> <spring:message code="user.firstName"/>:</strong></td>
            <td><input type="text" name="usearch_firstName" id="usearch_firstName"/></td>
        </tr>
        <tr>
            <td><strong> <spring:message code="user.email"/>:</strong></td>
            <td><input type="text" name="usearch_email" id="usearch_email" size="40"/></td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <input type="button" name="searchForUserBtn" onclick="startSearch();" value="<spring:message code="global.search" />"/>
                <input type="button" name="cancelSearchBtn" onclick="cancelSearch();" value="<spring:message code="global.cancel" />"/>
            </td>
        </tr>
    </table>

    <table class="paged">
        <tr>
            <td></td>
            <td><strong> <spring:message code="user.lastName"/> </strong></td>
            <td><strong> <spring:message code="user.firstName"/> </strong></td>
            <td><strong> <spring:message code="user.email"/> </strong></td>
        </tr>
    <c:forEach items="${userList}" var="user" varStatus="vstat">
        <c:choose>
            <c:when test='${(vstat.index)%2 eq 0}'>
                <c:set var="row" value="even" scope="page"/>
            </c:when>
            <c:otherwise>
                <c:set var="row" value="odd" scope="page"/>
            </c:otherwise>
        </c:choose>
        <tr class="${row}">
            <td nowrap="nowrap">
                <a href="javascript:chooseUser('<c:out value="${user.id}"/>')">
                    <img src="<c:url value="/images/add.gif" />" alt=""/>
                </a>
            </td>
            <td nowrap="nowrap" >
                <c:out value="${user.lastName}" />
            </td>
            <td nowrap="nowrap" >
                <c:out escapeXml="false" value="${user.firstName}" />
            </td>
            <td nowrap="nowrap" >
                <c:out escapeXml="false" value="${user.email}" />
            </td>
        </tr>
   </c:forEach>
</table>

    <c:if test="${applicationScope.ldapIntegration}">
        <b><spring:message code="ldapconfig.title"/></b>
        <table class="paged">
            <tr>
                <td></td>
                <td nowrap="nowrap"><strong><spring:message code="user.lastName" /></strong></td>
                <td nowrap="nowrap"><strong><spring:message code="user.firstName" /></strong></td>
                <td nowrap="nowrap"><strong><spring:message code="user.userName" /></strong></td>
                <td nowrap="nowrap"><strong><spring:message code="user.email" /></strong></td>
                <c:forEach items="${ldapFieldList}" var="field">
                    <td nowrap="nowrap"><strong>
                        <c:out value="${field.label}"/><c:if test="${empty field.label}" ><c:out value="${field.name}"/></c:if>
                    </strong></td>
                </c:forEach>
            </tr>
			<c:forEach items="${ldapUserList}" var="user" varStatus="vstat">
                <c:choose>
                    <c:when test='${(vstat.index)%2 eq 0}'>
                        <c:set var="row" value="even" scope="page"/>
                    </c:when>
                    <c:otherwise>
                        <c:set var="row" value="odd" scope="page"/>
                    </c:otherwise>
                </c:choose>
                <tr class="${row}">
                    <td nowrap="nowrap">
                        <a href="javascript:chooseUser('<c:out value="${user.dn}"/>')">
                            <img src="<c:url value="/images/add.gif" />" alt=""/>
                        </a>
                    </td>
                    <td nowrap="nowrap"><c:out value="${user.sn}"/> </td>
                    <td nowrap="nowrap"> <c:out escapeXml="false" value="${user.givenName}"/> </td>
                    <td nowrap="nowrap"> <c:out escapeXml="false" value="${user.cn}"/> </td>
                    <td nowrap="nowrap"> <c:out escapeXml="false" value="${user.mail}"/> </td>
                    <c:forEach items="${user.attributeMap}" var="map">
                        <td nowrap="nowrap"> <c:out escapeXml="false" value="${map.value}"/> </td>
                    </c:forEach>
                </tr>
            </c:forEach>
    	</table>
	</c:if>

</div>

<div id="ticketEditPage" style="display:<c:choose><c:when test="${showSearchPage == 'true'}">none</c:when><c:otherwise>block</c:otherwise></c:choose>">
<!--TICKET INFO-->
<div class="bar" style="width:98%">
    <a href="javascript:toggleExpansion('tinfoImg', 'tinfo_details')"><img id="tinfoImg" src="<c:url value="/images/minus.gif"/>" alt=""></a>
    <spring:message code="ticket.ticketInfo"/>
</div>
<div id="tinfo_details">
<table style="font-size:10px">
    <tr>
        <spring:bind path="ticket.contact">
        <td nowrap="nowrap">
            <c:choose>
                <c:when test="${not empty status.errorMessage}">
                <span style="color: red">
                * <spring:message code="ticket.contact"/>:
                </span>
                </c:when>
                <c:otherwise>
                    <spring:message code="ticket.contact"/>:
                </c:otherwise>
            </c:choose>
        </td>
        <td>
            <input type="text" id="contactUser" name="contactUser" size="10" disabled
                   value="<c:out value="${ticket.contact.firstName} ${ticket.contact.lastName}"/>">
            <a href="javascript:findUser();">
                <img src="<c:url value='/images/find2.gif'/>" alt="" align="bottom" >
            </a>
            <input type="hidden" name="${status.expression}" id="${status.expression}" value="${status.value}">
        </td>
        </spring:bind>
    </tr>
    <tr>
        <spring:bind path="ticket.priority">
        <td>
            <c:choose>
                <c:when test="${not empty status.errorMessage}">
                <span style="color: red">
                    *<spring:message code="ticket.priority"/>:
                </span>
                </c:when>
                <c:otherwise>
                    <spring:message code="ticket.priority"/>:
                </c:otherwise>
            </c:choose>
        </td>
        <td>
            <select name="${status.expression}">
                <c:forEach items="${ticketPriorities}" var="priority">
                    <option value="${priority.id}" <c:if test="${status.value == priority.id}">selected="selected"</c:if>>
                        <c:out value="${priority.name}"/>
                    </option>
                </c:forEach>
            </select>
        </td>
        </spring:bind>
    </tr>
    <tr>
        <spring:bind path="ticket.status">
        <td>
            <c:choose>
                <c:when test="${not empty status.errorMessage}">
                <span style="color: red">
                    *<spring:message code="ticket.status"/>:
                </span>
                </c:when>
                <c:otherwise>
                    <spring:message code="ticket.status"/>:
                </c:otherwise>
            </c:choose>
        </td>
        <td>
            <select id="statusSelection" name="${status.expression}">
                <option value="">&nbsp;</option>
                <c:forEach items="${ticketStatus}" var="tstatus">
                <option value="${tstatus.id}" <c:if test="${status.value == tstatus.id}">selected="selected"</c:if>>
                    <c:out value="${tstatus.name}"/>
                </option>
                </c:forEach>
            </select>
        </td>
        </spring:bind>
    </tr>
    <tr>
        <spring:bind path="ticket.location">
        <td>
            <c:choose>
                <c:when test="${not empty status.errorMessage}">
                <span style="color: red">
                    *<spring:message code="ticket.location"/>:
                </span>
                </c:when>
                <c:otherwise>
                    <spring:message code="ticket.location"/>:
                </c:otherwise>
            </c:choose>
        </td>
        <td>
            <select name="${status.expression}" id="${status.expression}" onchange="ddChanged('location');">
                <option value="">&nbsp;</option>
                <c:forEach items="${locations}" var="loc">
                    <option value="${loc.id}" <c:if test="${not empty status.value && status.value == loc.id}">selected="selected"</c:if>>
                        <c:out value="${loc.name}"/>
                    </option>
                </c:forEach>
            </select>
        </td>
        </spring:bind>
    </tr>
    <tr>
        <spring:bind path="ticket.group">
        <td  id="tgroup">
            <c:choose>
                <c:when test="${not empty status.errorMessage}">
                <span style="color: red">
                    *<spring:message code="ticket.group"/>:
                </span>
                </c:when>
                <c:otherwise>
                    <spring:message code="ticket.group"/>:
                </c:otherwise>
            </c:choose>
        </td>
        <td>
            <select name="${status.expression}" id="${status.expression}" onchange="ddChanged('group');">
                <option value="">&nbsp;</option>
                <c:forEach items="${initGroups}" var="group">
                    <option value="${group.id}" <c:if test="${not empty status.value && status.value == group.id && ddVal >1}"> selected="selected"</c:if>>
                        <c:out value="${group.name}"/>
                    </option>
                </c:forEach>
            </select>
        </td>
        </spring:bind>
    </tr>
    <tr>
        <spring:bind path="ticket.category">
        <td  id="tcategory">
            <c:choose>
                <c:when test="${not empty status.errorMessage}">
                <span style="color: red">
                    * <spring:message code="ticket.category"/>:
                </span>
                </c:when>
                <c:otherwise>
                    <spring:message code="ticket.category"/>:
                </c:otherwise>
            </c:choose>
        </td>
        <td>
            <select name="${status.expression}" id="${status.expression}" onchange="ddChanged('category');">
                <option value="">&nbsp;</option>
                <c:forEach items="${initCats}" var="category">
                    <option value="${category.id}" <c:if test="${status.value == category.id && ddVal>2}">selected="selected"</c:if>>
                        <c:out value="${category.name}"/>
                    </option>
                </c:forEach>
            </select>
        </td>
        </spring:bind>
    </tr>
    <tr>
        <spring:bind path="ticket.categoryOption">
        <td  id="tcatoption">
            <c:choose>
                <c:when test="${not empty status.errorMessage}">
                <span style="color: red">
                    *<spring:message code="ticket.categoryOption"/>:
                </span>
                </c:when>
                <c:otherwise>
                    <spring:message code="ticket.categoryOption"/>:
                </c:otherwise>
            </c:choose>
        </td>
        <td>
            <select name="${status.expression}" id="${status.expression}" onchange="ddChanged('categoryoption');">
                <option value="">&nbsp;</option>
                <c:forEach items="${initCatOpts}" var="catOpt">
                    <option value="${catOpt.id}" <c:if test="${status.value == catOpt.id && ddVal>3}">selected="selected"</c:if>>
                        <c:out value="${catOpt.name}"/>
                    </option>
                </c:forEach>
            </select>
        </td>
        </spring:bind>
    </tr>
    <tr>
        <spring:bind path="ticket.assignedTo">
        <td id="tassignedto">
            <c:choose>
                <c:when test="${not empty status.errorMessage}">
                <span style="color: red">
                    *<spring:message code="ticket.assignedTo"/>:
                </span>
                </c:when>
                <c:otherwise>
                    <spring:message code="ticket.assignedTo"/>:
                </c:otherwise>
            </c:choose>
        </td>
        <td>
            <select name="${status.expression}" id="${status.expression}">
                <option value="">&nbsp;</option>
                <c:forEach items="${initAssignedTo}" var="urg">
                    <option <c:if test="${status.value == urg.id && ddVal >=4}">selected="selected"</c:if> value="${urg.id}">
                        <c:choose>
                            <c:when test="${not empty urg.userRole}">
                                <c:out value="${urg.userRole.user.lastName}"/>, <c:out value="${urg.userRole.user.firstName}"/>
                            </c:when>
                            <c:otherwise>
                                <spring:message code="ticketList.ticketPool"/>
                            </c:otherwise>
                        </c:choose>
                    </option>
                </c:forEach>
            </select>
        </td>
        </spring:bind>
    </tr>
    <c:forEach var="cfield" items="${customFields}">
    <tr>
        <td>${cfield.key}</td>
        <td>${cfield.value}</td>
    </tr>
    </c:forEach>
</table>
</div>

<!--DESCRIPTION-->
<div class="bar" style="width:98%">
    <a href="javascript:toggleExpansion('descImg', 'tdescription')"><img id="descImg" src="<c:url value="/images/minus.gif"/>" alt=""></a>
    <spring:message code="ticket.description"/>
</div>
<div id="tdescription">
<table style="font-size:10px">
    <tr>
       <spring:bind path="ticket.subject">
       <td id="tsubject">
           <c:choose>
               <c:when test="${not empty status.errorMessage}">
                <span style="color: red">
                    *<spring:message code="ticket.subject"/>:
                </span>
               </c:when>
               <c:otherwise>
                   <spring:message code="ticket.subject"/>:
               </c:otherwise>
           </c:choose>
       </td>
       <td>
          <input type="text" id="subject" name="${status.expression}" value="<c:out value="${status.value}"/>"/>
       </td>
       </spring:bind>
    </tr>
    <tr>
        <td colspan="2"><spring:message code="ticket.note"/>:</td>
    </tr>
    <tr>
        <td colspan="2"  align="center" width="100%">
            <spring:bind path="ticket.note">
                <textarea id="note" rows="3" cols="30" name="${status.expression}"><c:out value="${status.value}"/></textarea>
            </spring:bind>
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center" width="100%">
            <input type="button" value="<spring:message code="global.save"/>" onclick="saveForm();"/>
        </td>
    </tr>
</table>
</div>

<!--TICKET HISTORY-->
<c:if test="${not empty ticket.id}">
<div class="bar" style="width:98%">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="barnob">
                <a href="javascript:toggleExpansion('thistImg', 'hist_details')">
                    <img id="thistImg" src="<c:url value="/images/minus.gif"/>" alt="">
                </a>
                <spring:message code="global.comments"/>
            </td>
            <td align="right">
                <a href="javascript://" onclick="addComment();">
                    <img src="<c:url value="/images/doc_add_16_hot.gif"/>" alt="">
                </a>
            </td>
        </tr>
    </table>
</div>
<div id="hist_details">
    <table width="99%" cellpadding="0" cellspacing="0" border="0" class="box" align="center">
        <tr>
            <td>
                <div id="tiHistContainer">
                    <c:if test="${fn:length(ticketHistories) > 0}" >
                    <table width="100%" align="center">
                        <c:forEach items="${ticketHistories}" var="thist">
                            <tr>
                                <td class="commentHeader">
                                    <fmt:formatDate value="${thist.createdDate}" type="both" timeStyle="medium" dateStyle="long"/>
                                    - <c:out value="${thist.user.firstName}"/> <c:out value="${thist.user.lastName}"/>
                                    - <c:out value="${thist.subject}"/>
                                </td>
                            </tr>
                            <c:choose>
                                <c:when test="${not empty thist.note}">
                                    <tr>
                                        <td class="commentBody" style="padding-bottom:3px">
                                            <c:out value="${thist.note}" escapeXml="false"/>
                                        </td>
                                    </tr>
                                </c:when>
                                <c:otherwise>
                                    <tr>
                                        <td style="background:white;">&nbsp;</td>
                                    </tr>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </table>
                    </c:if>
                    <c:if test="${fn:length(ticketHistories) == 0}" >
                        <i><spring:message code="ticket.noComments" /></i>
                    </c:if>
                </div>
            </td>
        </tr>
    </table>
</div>
</c:if>
</div>

<div id="addCommentPage" style="display:none">
    <table width="100%" style="font-size:10px">
        <tr>
            <td colspan="2"><hr></td>
        </tr>
        <tr>
            <td><spring:message code="ticket.subject"/>:</td>
            <td width="100%">
                <input type="text" name="thist_subject" id="thist_subject" style="width:100%;"/>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center" width="100%">
                <textarea id="thist_note" rows="4" cols="50" name="thist_note"></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center" width="100%">
                <input type="button" name="saveTHistoryBtn" onclick="saveTHist();" value="<spring:message code="global.save"/>"/>
                <input type="button" name="cancelSearchBtn" onclick="cancelTHist();" value="<spring:message code="global.cancel" />"/>
            </td>
        </tr>
    </table>
</div>
</form>
</body>
</html>