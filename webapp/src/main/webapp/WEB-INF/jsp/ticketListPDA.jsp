<%@ include file="/WEB-INF/jsp/include.jsp" %>

<jsp:useBean id="userRoleGroups" scope="request" type="java.util.List"/>
<jsp:useBean id="listHolder" scope="request" type="java.util.Map"/>
<jsp:useBean id="showAllGroupTickets" scope="request" type="java.lang.Boolean"/>
<jsp:useBean id="urgId" scope="request" type="java.lang.Integer"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
    <title><spring:message code="global.systemName"/></title>
    <link rel="stylesheet" href="<c:url value="/css/ticketListPDA.css"/>" type="text/css">
</head>
<body>
<form action="<c:url value="/pda/ticketList.glml"/> " method="post">
    <div class="section">
        <div class="column">
            <div>
                <span><c:out value="${sessionScope['User'].firstName}"/>
                <c:out value="${sessionScope['User'].lastName}"/></span>
            </div>

            <div>
                <select name="urgId" onchange="document.forms[0].submit()">
                <c:forEach var="urg" items="${userRoleGroups}">
                    <c:if test="${urg.active}">
                        <option value="${urg.id}" <c:if test="${urg.id == urgId}">selected</c:if>><c:out value=" ${urg.group.name}"/></option>
                    </c:if>
                </c:forEach>
                </select>
            </div>

            <div>
                <a href="<c:url value="/pda/ticketEdit.glml"/>">
                <spring:message code="ticket.new"/>
                </a>
            </div>
        </div>

        <div class="column right">
            <div>
                <a href="<c:url value="/logout.glml"/>">
                    <spring:message code="header.signout"/>
                </a>
            </div>

            <div>
                <select name="filter.grouping" onchange="document.forms[0].submit()">
                    <option value="assignedToMe" <c:if test="${listHolder.tickets.filter.grouping == 'assignedToMe'}">selected</c:if>>
                        <spring:message code="ticketList.assignedToMe"/>
                    </option>
                    <option value="ticketPool" <c:if test="${listHolder.tickets.filter.grouping == 'ticketPool'}">selected</c:if>>
                        <spring:message code="ticketList.ticketPool"/>
                    </option>
                    <c:if test="${showAllGroupTickets}">
                        <option value="allGroupTickets" <c:if test="${listHolder.tickets.filter.grouping == 'allGroupTickets'}">selected</c:if>>
                            <spring:message code="ticketList.allGroupTickets"/>
                        </option>
                    </c:if>
                </select>
            </div>
        </div>
    </div>

<div class="section">
    <table class="paged">
        <thead>
            <tr class="title">
                <th>
                    <a href="<c:url value="/pda/ticketList.glml"><c:param name="urgId" value="${urgId}"/><c:param name="sort.property" value="priority"/></c:url>">
                        <img src="<c:url value="/images/redball.gif"/>" alt=""/>
                    </a>
                </th>
                <th>
                    <a href="<c:url value="/pda/ticketList.glml"><c:param name="urgId" value="${urgId}"/><c:param name="sort.property" value="id"/></c:url>">#</a>
                </th>
                <th>
                    <a href="<c:url value="/pda/ticketList.glml"><c:param name="urgId" value="${urgId}"/><c:param name="sort.property" value="location"/></c:url>">
                        <spring:message code="ticket.location"/>
                    </a>
                </th>
                <th>
                    <a href="<c:url value="/pda/ticketList.glml"><c:param name="urgId" value="${urgId}"/><c:param name="sort.property" value="subject"/></c:url>">
                        <spring:message code="ticket.subject"/>
                    </a>
                </th>
                <th>
                    <a href="<c:url value="/pda/ticketList.glml"><c:param name="urgId" value="${urgId}"/><c:param name="sort.property" value="assignedTo"/></c:url>">
                        <spring:message code="ticket.assignedTo"/>
                    </a>
                </th>
                <th>
                    <a href="<c:url value="/pda/ticketList.glml"><c:param name="urgId" value="${urgId}"/><c:param name="sort.property" value="status"/></c:url>">
                        <spring:message code="ticket.status"/>
                    </a>
                </th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${listHolder.tickets.pageList}" var="tckt" varStatus="vstat">
                <c:choose>
                    <c:when test='${(vstat.index)%2 eq 0}'>
                        <c:set var="row" value="even" scope="page"/>
                    </c:when>
                    <c:otherwise>
                        <c:set var="row" value="odd" scope="page"/>
                    </c:otherwise>
                </c:choose>
                <tr class="${row}">
                    <td>
                        <c:choose>
                            <c:when test="${tckt.priority.id == -3}">
                                <img src="<c:url value="/images/redball.gif"/>"
                                     alt="<spring:message code="ticket.priority.high"/>">
                            </c:when>
                            <c:when test="${tckt.priority.id == -2}">
                                <img src="<c:url value="/images/orangeball.gif"/>"
                                     alt="<spring:message code="ticket.priority.medium"/>">
                            </c:when>
                        </c:choose>
                    </td>
                    <td>
                        <a href="<c:url value="/pda/ticketEdit.glml"><c:param name="tid" value="${tckt.id}"/></c:url>">
                            <c:out value="${tckt.ticketId}"/>
                        </a>
                    </td>
                    <td>
                        <c:out value="${tckt.location.name}"/>
                    </td>
                    <td>
                        <ehd:trimString value="${tckt.subject}" length="25"/>
                    </td>
                    <td>
                        <c:choose>
                            <c:when test="${not empty tckt.assignedTo.userRole}">
                                <c:out value="${tckt.assignedTo.userRole.user.firstName}"/>
                                <c:out value="${tckt.assignedTo.userRole.user.lastName}"/>
                            </c:when>
                            <c:otherwise>
                                <spring:message code="ticketList.ticketPool"/>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td>
                        <c:out value="${tckt.status.name}"/>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>

<div class="section end pager">
    <div class="column">
        <div>
            <c:if test="${listHolder.tickets.pageCount > 1}">
                <a href="<c:url value="/pda/ticketList.glml"><c:param name="urgId" value="${urgId}"/><c:param name="page" value="0"/></c:url>">1</a>
                &nbsp;...&nbsp;
                <c:forEach begin="${listHolder.tickets.firstLinkedPage}"
                           end="${listHolder.tickets.lastLinkedPage}" var="crtpg">
                    <c:choose>
                        <c:when test="${crtpg == listHolder.tickets.page}">
                            <strong>
                                <c:out value="${crtpg + 1}"/>
                            </strong>
                        </c:when>
                        <c:otherwise>
                            <a href="<c:url value="/pda/ticketList.glml"><c:param name="urgId" value="${urgId}"/><c:param name="page" value="${crtpg}"/></c:url>">
                                <c:out value="${crtpg + 1}"/>
                            </a>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
                &nbsp;...&nbsp;
                <a href="<c:url value="/pda/ticketList.glml"><c:param name="urgId" value="${urgId}"/><c:param name="page" value="${listHolder.tickets.pageCount - 1}"/></c:url>">
                    <c:out value="${listHolder.tickets.pageCount}"/>
                </a>
            </c:if>
            &nbsp;
        </div>

        <div>
            <spring:message code="pagedList.pltPages"
                            arguments="${listHolder.tickets.page + 1},${listHolder.tickets.pageCount}"/>
        </div>
    </div>

    <div class="column right">
        <div>
            <spring:message code="pagedList.pltSize" var="ps"/>
            <select name="pageSize" onChange="document.forms[0].submit()">
                <c:forTokens items="5,10,25,50,75,100" delims="," var="crtps">
                    <option
                            <c:if test="${listHolder.tickets.pageSize == crtps}">selected</c:if>
                            value="<c:out value="${crtps}"/>">
                        <c:out value="${crtps} ${ps}"/>
                    </option>
                </c:forTokens>
            </select>
        </div>

        <div>
            <spring:message code="pagedList.pltRecords"
                            arguments="${listHolder.tickets.firstElementOnPage + 1},${listHolder.tickets.lastElementOnPage + 1},${listHolder.tickets.nrOfElements}"/>
        </div>
    </div>
</div>
</form>
</body>
</html>