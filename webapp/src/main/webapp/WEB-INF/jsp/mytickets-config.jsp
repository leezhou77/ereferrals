<%--@elvariable id="myTicketsConfig" type="net.grouplink.ehelpdesk.web.domain.MyTicketsConfig"--%>
<%--@elvariable id="maxOrder" type="java.lang.Integer"--%>
<%--@elvariable id="myFilters" type="java.util.List<net.grouplink.ehelpdesk.domain.ticket.TicketFilter>"--%>
<%--@elvariable id="pubFilters" type="java.util.List<net.grouplink.ehelpdesk.domain.ticket.TicketFilter>"--%>

<%@ include file="/WEB-INF/jsp/include.jsp" %>

<head>

    <title><spring:message code="myTicketTabs" text="My Ticket Tabs"/></title>

    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/MyTicketTabsService.js"/>"></script>

    <script type="text/javascript">

        function addTab() {
            var addTabDialog = dijit.byId("addTabDialog");
            addTabDialog.show();
        }

        function deleteTab(mttId) {
            if (confirm("<spring:message code="global.confirmdelete" text="Are you sure you want to delete this Item?"/>")) {
                console.debug("called deleteTab");
                var callMetaData = {
                    callback: refreshWindow
                };

                MyTicketTabsService.deleteTab(mttId, callMetaData);
            }
        }

        function saveNewTab(dialogForm) {
            console.debug("called saveNewTab");
            var callMetaData = {
                callback: refreshWindow
            };

            MyTicketTabsService.saveNewTab(dialogForm.newTabName, dialogForm.newTicketFilter, dialogForm.newOrder, callMetaData);
        }

        function refreshWindow() {
            window.location = '<c:url value="/mytickets-config.glml"/>';
        }

        function saveForm() {
            document.getElementById("myTicketConfigForm").submit();
        }

        function applyChanges() {
            var form = document.getElementById("myTicketConfigForm");
            var submitType = document.createElement("input");
            submitType.setAttribute("type", "hidden");
            submitType.setAttribute("name", "submitType");
            submitType.setAttribute("value", "apply");
            form.appendChild(submitType);
            form.submit();
        }

        function cancel() {
            window.location = '<c:url value="/mytickets.glml"/>';
        }

        function addDialogFilterChanged() {
            var filter = document.getElementById("newTicketFilterSelect");
            var newTicketFilter = dijit.byId("newTicketFilter");
            newTicketFilter.attr("value", filter.options[filter.selectedIndex].value);
            var tabName = dijit.byId("newTabName");
            tabName.attr("value", filter.options[filter.selectedIndex].text);
        }

        function newTabIsValid() {
            var newTicketFilter = dijit.byId("newTicketFilter");
            return newTicketFilter.attr("value") != "";
        }

        function hideAddDialog() {
            dijit.byId('addTabDialog').hide();
            var tabName = dijit.byId("newTabName");
            tabName.attr("value", "");
            var filter = dijit.byId("newTicketFilter");
            filter.attr("value", -1);
            var order = dijit.byId("newOrder");
            order.attr("value", "${maxOrder + 1}");
        }

        function init() {
        }

        dojo.addOnLoad(init);
    </script>

</head>

<body>

<section class="grid_10">
    <div class="block-content no-padding">
        <h1><spring:message code="myTicketTabs" text="My Ticket Tabs"/></h1>

        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a href="javascript:addTab();">
                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="">
                        <spring:message code="myTicketTabs.addTab" text="Add Tab"/>
                    </a>
                </li>
            </ul>
        </div>


        <form action="<c:url value="/mytickets-config.glml"/>" method="post" id="myTicketConfigForm">

            <spring:bind path="myTicketsConfig.*">
                <c:if test="${not empty status.errorMessages}">
                    <div id="errors">
                        <ul>
                            <c:forEach var="error" items="${status.errorMessages}">
                                <c:if test="${not empty error}">
                                    <li>${error}</li>
                                </c:if>
                            </c:forEach>
                        </ul>
                    </div>
                </c:if>
            </spring:bind>

            <div class="no-margin">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col"><spring:message code="myTicketTabs.order" text="Order"/></th>
                        <th scope="col"><spring:message code="myTicketTabs.name" text="Name"/></th>
                        <th scope="col"><spring:message code="myTicketTabs.ticketFilter" text="Ticket Filter"/></th>
                        <th scope="col"><spring:message code="myTicketTabs.actions" text="Actions"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="myTicketTab" items="${myTicketsConfig.myTicketTabs}" varStatus="varStatus">
                        <tr class="toberuled">
                            <spring:bind path="myTicketsConfig.myTicketTabs[${varStatus.index}].order">
                                <td><input class="short" dojoType="dijit.form.NumberTextBox" type="text" name="${status.expression}" value="${status.value}"/></td>
                            </spring:bind>
                            <spring:bind path="myTicketsConfig.myTicketTabs[${varStatus.index}].name">
                                <td><input dojoType="dijit.form.TextBox" type="text" name="${status.expression}" value="${status.value}"/></td>
                            </spring:bind>

                            <td>
                                <form:select path="myTicketsConfig.myTicketTabs[${varStatus.index}].ticketFilter" id="mtt_${varStatus.index}_tf">
                                    <option value=""></option>

                                    <c:if test="${fn:length(myFilters)>0}">
                                        <optgroup label="<spring:message code="ticketFilter.myFilters"/>">
                                            <form:options items="${myFilters}" itemValue="id" itemLabel="name"/>
                                        </optgroup>
                                    </c:if>
                                    <c:if test="${fn:length(pubFilters)>0}">
                                        <optgroup label="<spring:message code="ticketFilter.publicFilters"/>">
                                            <form:options items="${pubFilters}" itemValue="id" itemLabel="name"/>
                                        </optgroup>
                                    </c:if>
                                </form:select>
                            </td>

                            <td class="nowrap">
                                <a href="javascript://" title="<spring:message code="myTicketTabs.delete" text="Delete Tab"/>" onclick="deleteTab('${myTicketTab.id}');return false;">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt="">
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>

            <p id="submit_buttons" class="grey-bg">
                <button type="button" onclick="applyChanges();">
                    <spring:message code="global.apply" text="Apply" />
                </button>

                <button type="button" onclick="saveForm();">
                    <spring:message code="global.save" />
                </button>

                <button type="button" onclick="cancel();">
                    <spring:message code="global.cancel" />
                </button>
            </p>

        </form>
    </div>
</section>


<div id="addTabDialog" dojotype="dijit.Dialog" title="<spring:message code="myTicketTabs.addNewTab" text="Add New Tab"/>"
        execute="saveNewTab(arguments[0]);">
    <div id="newTicketFilter" name="newTicketFilter" dojotype="dijit.form.TextBox" style="display:none;" required="true"></div>
    <div class="form">
        <p>
            <label><spring:message code="ticketFilter.title" text="Ticket Filter"/></label>
            <select name="newTicketFilterSelect" id="newTicketFilterSelect" onchange="addDialogFilterChanged();" >
                    <option></option>
                    <c:if test="${fn:length(myFilters)>0}">
                        <optgroup label="<spring:message code="ticketFilter.myFilters"/>">
                            <c:forEach var="ticketFilter" items="${myFilters}">
                                <option value="${ticketFilter.id}"><c:out value="${ticketFilter.name}"/></option>
                            </c:forEach>
                        </optgroup>
                    </c:if>
                    <c:if test="${fn:length(pubFilters)>0}">
                        <optgroup label="<spring:message code="ticketFilter.publicFilters"/>">
                            <c:forEach var="ticketFilter" items="${pubFilters}">
                                <option value="${ticketFilter.id}"><c:out value="${ticketFilter.name}"/></option>
                            </c:forEach>
                        </optgroup>
                    </c:if>
                </select>
        </p>
        <p>
            <label><spring:message code="myTicketTabs.tabName" text="Tab Name"/></label>
            <input type="text" dojotype="dijit.form.TextBox" name="newTabName" id="newTabName" required="true"/>
        </p>
        <p>
            <label><spring:message code="myTicketTabs.order" text="Order"/> </label>
            <input type="text" dojotype="dijit.form.NumberTextBox" name="newOrder" id="newOrder" value="${maxOrder + 1}" required="true"/>
        </p>

        <button dojoType="dijit.form.Button" type="submit" onClick="return dijit.byId('addTabDialog').isValid() && newTabIsValid();">
            <spring:message code="global.save"/>
        </button>
        <button dojoType="dijit.form.Button" type="button" onClick="hideAddDialog();">
            <spring:message code="global.cancel"/>
        </button>
        
        </div>
</div>

</body>
