<%@ include file="/WEB-INF/jsp/include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<title>
    <spring:message code="surveys.submitted" />
</title>
<spring:theme code="css" var="cssfile"/>

<c:if test="${not empty cssfile}">
    <link rel="stylesheet" href="<c:url value="${cssfile}"/>" type="text/css">
</c:if>

<link rel="stylesheet" href="<c:url value="/css/surveys.css"/>" type="text/css">
</head>

<body>
<div id="complete">
    <p>
        <spring:message code="surveys.thankYou" />
    </p>
</div>
</body>
</html>