var timeID;
var count = 0;
var h = 0;
var hh = 0;
var m = 0;
var mm = 0;
var s = 0;
var ss = 0;

function timer(id,divId) {
    try {
        clearTimeout(timeID);
        f = document.getElementById(id);
        d = document.getElementById(divId);
        tmpTime = f.value.split(":");
        h = getTime(tmpTime[0]);
        m = getTime(tmpTime[1]);
        s = getTime(tmpTime[2]);
        count += 1;
        s += 1;
        ss = s;
        if (s > 59) {
            s -= 60; m += 1; ss -= 60;
        }
        mm = m;
        if (m > 59) {
            m -= 60; h += 1; mm -= 60;
        }
        hh = h;
        if (ss < 10) {
            ss = "0" + ss;
        }
        if (mm < 10) {
            mm = "0" + mm;
        }
        if (h < 10) {
            hh = "0" + hh;
        }
        msg = hh + ":" + mm + ":" + ss ;
        f.value = msg;
        d.innerHTML = msg;
        timeID = setTimeout("timer('" + id + "','" + divId + "')", 1000);
    } catch (e) {
        alert(e.message);
    }
}

function timerI(id,inpId, formId) {
    try {
        clearTimeout(timeID);

        // browser detection is a bad idea.  Check for capabilities directly instead.
/*        var browserVer=parseInt(navigator.appVersion);
        if (navigator.appName.match("Internet Explorer") && browserVer == 4) {
            f = document.form.elements[id];
            d = document.form.elements[inpId];
        } else {
            f = document.getElementById(id);
            d = document.getElementById(inpId);
        }
*/
		if (document.getElementById) {
			f = document.getElementById(id);
			d = document.getElementById(inpId);
		} else if (document.forms && formId && document.forms[formId]) {
			f = document.forms[formId].elements[id];
			d = document.forms[formId].elements[inpId];
		} else if (document.forms && document.forms[0]) {
			f = document.forms[0].elements[id];
			d = document.forms[0].elements[inpId];
		}
        try {
            tmpTime = f.value.split(":");
            h = getTime(tmpTime[0]);
            m = getTime(tmpTime[1]);
            s = getTime(tmpTime[2]);
        } catch (exceptionObject) {
            alert(exceptionObject.message);
        }
        count += 1;
        s += 1;
        ss = s;
        if (s > 59) {
            s -= 60; m += 1; ss -= 60;
        }
        mm = m;
        if (m > 59) {
            m -= 60; h += 1; mm -= 60;
        }
        hh = h;
        if (ss < 10) {
            ss = "0" + ss;
        }
        if (mm < 10) {
            mm = "0" + mm;
        }
        if (h < 10) {
            hh = "0" + hh;
        }
        msg = hh + ":" + mm + ":" + ss ;
        f.value = msg;
        d.value = msg;
        timeID = setTimeout("timerI('" + id + "','" + inpId + "')", 1000);
    } catch (e) {
        alert(e.message);
    }
}

function stopTimer(c,id,divId) {
    try {
        if (c == 'stop') {
            clearTimeout(timeID);
        }
        if (c == 'clear') {
            clearTimeout(timeID);
            ss = 0; mm = 0; m = 0; s = -1;
            count = 0;
            timer(id,divId);
            stopTimer('stop',id,divId);
        }
    } catch (e) {
        alert(e.message);
    }
}

function stopTimerI(c,id,inpId) {
    try {
        if (c == 'stop') {
            clearTimeout(timeID);
        }
        if (c == 'clear') {
            clearTimeout(timeID);
            ss = 0; mm = 0; m = 0; s = -1;
            count = 0;
            timerI(id,inpId);
            stopTimer('stop',id,inpId);
        }
    } catch (e) {
        alert(e.message);
    }
}

function getTime(time) {
    try {
        var iReturn = 0;
        try {
            tmpStr = new String(time);
            if (tmpStr.indexOf("0") == 0) {
                tmpStr = tmpStr.substr(1,tmpStr.length);
            }
            iReturn = parseInt(tmpStr);
        } catch (exceptionObject) {
            alert(time + ":" + exceptionObject.message);
        }
        return iReturn;
    } catch (e) {
        alert(e.message);
    }
}

function resetTimer(id, divId) {
    try {
        f = document.getElementById(id);
        d = document.getElementById(divId);
        f.value = "00:00:00";
        d.innerHTML = "00:00:00";
    } catch (e) {
        alert(e.message);
    }
}

function verifyTimeChange(id, inpId) {
    f = document.getElementById(id);
    d = document.getElementById(inpId);
    if (confirm("Are you sure you want to modify the Work Time?\nIf yes, click OK.")) {
        f.value = d.value;
        return true;
    } else {
        d.value = f.value;
    }
    return false;
}
