function openWindow(url, windowName, width, height) {
    var features = "modal=yes,toolbar=no,status=yes,width=" + width + ",height=" + height + ",scrollbars=yes,resizable=yes,";
    var popup = window.open(url, windowName, features);
    if (popup != null) {
        popup.focus();
    }
}

function vToggle(id) {
    var el = document.getElementById(id).style;
    if (el.display == "none") {
        el.display = "";
    } else {
        if (el.display == "") {
            el.display = "none";
        }
    }
}

function GetXmlHttpObject()
{
    var xmlHttp = null;
    try
    {
        // Firefox, Opera 8.0+, Safari
        xmlHttp = new XMLHttpRequest();
    }
    catch (e)
    {
        // Internet Explorer
        try
        {
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e)
        {
            xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
    }
    return xmlHttp;
}

function addElement() {
    var ni = document.getElementById('formTarget');
    var ex = document.getElementById('newForm');
    var numi = document.getElementById('theValue');
    var num = (document.getElementById('theValue').value - 1) + 2;
    numi.value = num;
    var newdiv = document.createElement('div');
    var divIdName = 'div_' + num;
    newdiv.setAttribute('id', divIdName);
    newdiv.innerHTML = replaceSubstring(ex.innerHTML, 'myForm', divIdName);
    ni.appendChild(newdiv);
}

function removeElement(divNum) {
    var d = document.getElementById('formTarget');
    var olddiv = document.getElementById(divNum);
    d.removeChild(olddiv);
}

function replaceSubstring(inputString, fromString, toString) {
    // Goes through the inputString and replaces every occurrence of fromString with toString
    var temp = inputString;
    if (fromString == "") {
        return inputString;
    }
    if (toString.indexOf(fromString) == -1) { // If the string being replaced is not a part of the replacement string (normal situation)
        while (temp.indexOf(fromString) != -1) {
            var toTheLeft = temp.substring(0, temp.indexOf(fromString));
            var toTheRight = temp.substring(temp.indexOf(fromString) + fromString.length, temp.length);
            temp = toTheLeft + toString + toTheRight;
        }
    } else { // String being replaced is part of replacement string (like "+" being replaced with "++") - prevent an infinite loop
        var midStrings = new Array("~", "`", "_", "^", "#");
        var midStringLen = 1;
        var midString = "";
        // Find a string that doesn't exist in the inputString to be used
        // as an "inbetween" string
        while (midString == "") {
            for (var i = 0; i < midStrings.length; i++) {
                var tempMidString = "";
                for (var j = 0; j < midStringLen; j++) {
                    tempMidString += midStrings[i];
                }
                if (fromString.indexOf(tempMidString) == -1) {
                    midString = tempMidString;
                    i = midStrings.length + 1;
                }
            }
        }
        // Keep on going until we build an "inbetween" string that doesn't exist
        // Now go through and do two replaces - first, replace the "fromString" with the "inbetween" string
        while (temp.indexOf(fromString) != -1) {
            toTheLeft = temp.substring(0, temp.indexOf(fromString));
            toTheRight = temp.substring(temp.indexOf(fromString) + fromString.length, temp.length);
            temp = toTheLeft + midString + toTheRight;
        }
        // Next, replace the "inbetween" string with the "toString"
        while (temp.indexOf(midString) != -1) {
            toTheLeft = temp.substring(0, temp.indexOf(midString));
            toTheRight = temp.substring(temp.indexOf(midString) + midString.length, temp.length);
            temp = toTheLeft + toString + toTheRight;
        }
    }
    // Ends the check to see if the string being replaced is part of the replacement string or not
    return temp;
    // Send the updated string back to the user
}
// Ends the "replaceSubstring" function

// Finds the x coordinate of an html element (obj)
function findPosX(obj)
{
    var curleft = 0;
    if (obj.offsetParent)
        while (1)
        {
            curleft += obj.offsetLeft;
            if (!obj.offsetParent)
                break;
            obj = obj.offsetParent;
        }
    else if (obj.x)
        curleft += obj.x;
    return curleft;
}

// Finds the y coordinate of an html element (obj)
function findPosY(obj)
{
    var curtop = 0;
    if (obj.offsetParent)
        while (1)
        {
            curtop += obj.offsetTop;
            if (!obj.offsetParent)
                break;
            obj = obj.offsetParent;
        }
    else if (obj.y)
        curtop += obj.y;
    return curtop;
}

function setEditMode(editable) {
    if (editable) setCookie("editableForm", editable);
    else deleteCookie("editableForm");
}

function isEditModeOn() {
    return (getCookie("editableForm") == "true");
}

function toggleEditMode() {
    if (isEditModeOn()) setEditMode(false);
    else setEditMode(true);
}

// puts a java like trim function to strings
String.prototype.trim = function() {
    // skip leading and trailing whitespace
    // and return everything in between
    var x = this;
    x = x.replace(/^\s*(.*)/, "$1");
    x = x.replace(/(.*?)\s*$/, "$1");
    return x;
};

function hideLoader() {
    dojo.fadeOut({
        node:"preloader",
        duration:200,
        onEnd: function(){
            dojo.style("preloader", "display", "none");
        }
    }).play();
}

var delay = (function() {
    var timer = 0;
    return function(callback, milliseconds) {
        clearTimeout(timer);
        timer = setTimeout(callback, milliseconds);
    };
})();

var Ehd = Ehd || {};
Ehd.Utils = {};

Ehd.Utils.flash = function(flashType, message, replace) {
    var html = dojo.create("p", {"class": "no-margin message " + flashType, innerHTML: '<span>' + message + '</span><span class="close-bt"></span>'});
    var pos = "last";
    if(replace) {
        pos = "only";
    }
    dojo.place(html, dojo.byId("flashMessage"), pos);
};


