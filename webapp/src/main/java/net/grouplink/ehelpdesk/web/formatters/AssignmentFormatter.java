package net.grouplink.ehelpdesk.web.formatters;

import net.grouplink.ehelpdesk.domain.Assignment;
import net.grouplink.ehelpdesk.service.AssignmentService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class AssignmentFormatter implements Formatter<Assignment> {
    @Autowired
    private AssignmentService addressService;

    public Assignment parse(String text, Locale locale) throws ParseException {
        if (StringUtils.isNotBlank(text)) {
            return addressService.getById(Integer.valueOf(text));
        } else {
            return null;
        }
    }

    public String print(Assignment object, Locale locale) {
        return object == null ? "" : object.getId().toString();
    }
}
