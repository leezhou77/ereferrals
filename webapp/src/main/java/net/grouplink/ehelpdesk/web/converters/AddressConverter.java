package net.grouplink.ehelpdesk.web.converters;

import net.grouplink.ehelpdesk.domain.Address;
import net.grouplink.ehelpdesk.service.AddressService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: 11/8/11
 * Time: 1:11 PM
 */
public class AddressConverter implements Converter<String, Address> {
    @Autowired
    private AddressService addressService;

    public Address convert(String id){
        if(StringUtils.isBlank(id)) return null;

        try{
            int addressId = Integer.parseInt(id);
            return addressService.getById(addressId);
        } catch (Exception e){
            return null;
        }
    }
}
