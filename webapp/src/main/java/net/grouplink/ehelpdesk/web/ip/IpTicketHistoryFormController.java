package net.grouplink.ehelpdesk.web.ip;

import net.grouplink.ehelpdesk.common.collaboration.Appointment;
import net.grouplink.ehelpdesk.common.collaboration.NameAndEmail;
import net.grouplink.ehelpdesk.common.collaboration.Session;
import net.grouplink.ehelpdesk.common.collaboration.Task;
import net.grouplink.ehelpdesk.common.utils.Constants;
import net.grouplink.ehelpdesk.common.utils.GLTest;
import net.grouplink.ehelpdesk.common.utils.MailConfig;
import net.grouplink.ehelpdesk.domain.AppointmentType;
import net.grouplink.ehelpdesk.domain.Attachment;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketHistory;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRole;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.service.AppointmentTypeService;
import net.grouplink.ehelpdesk.service.MailService;
import net.grouplink.ehelpdesk.service.TicketHistoryService;
import net.grouplink.ehelpdesk.service.TicketService;
import net.grouplink.ehelpdesk.service.collaboration.CollaborationService;
import net.grouplink.ehelpdesk.web.propertyeditors.AppointmentTypeEditor;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: Dec 29, 2009
 * Time: 3:07:43 PM
 */
public class IpTicketHistoryFormController extends SimpleFormController {

    private TicketHistoryService ticketHistoryService;
    private TicketService ticketService;
    private MailService mailService;
    private AppointmentTypeService appointmentTypeService;
    private CollaborationService collaborationService;

    public IpTicketHistoryFormController() {
        super();
        setCommandClass(TicketHistory.class);
        setCommandName("ticketHistory");
        setFormView("ticketHistoryEdit");
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        Integer thid = ServletRequestUtils.getIntParameter(request, "thid");
        TicketHistory ticketHistory = null;
        if (thid != null) {
            ticketHistory = ticketHistoryService.getTicketHistoryById(thid);
        }

        if (ticketHistory == null) {
            ticketHistory = initializeTicketHistory(request);
        }

        return ticketHistory;
    }

    private TicketHistory initializeTicketHistory(HttpServletRequest request) throws ServletRequestBindingException {

        Integer ticketId = ServletRequestUtils.getIntParameter(request, "tid");
        Integer thtype = ServletRequestUtils.getIntParameter(request, "thtype", Constants.THISTTYPE_COMMENT);
        String ticketChanges = ServletRequestUtils.getStringParameter(request, "tchanges", "");

        Ticket ticket = ticketService.getTicketById(ticketId);
        User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);

        TicketHistory th = new TicketHistory();
        th.setUser(user);
        th.setTicket(ticket);
        th.setCommentType(thtype);
        th.setNote(ticketChanges);
        th.setCreatedDate(new Date());
        th.setActive(true);
        th.setTextFormat(true);

        // Get notification defaults from mailconfig
        MailConfig mc = mailService.getMailConfig();
        Boolean checkNotifyUser = mc.getNotifyUser();
        Boolean checkNotifyTech = mc.getNotifyTechnician();

        // check user against ticket contact
        if (user.getId().equals(ticket.getContact().getId())) {
            checkNotifyUser = Boolean.FALSE;
        }

        // check user against ticket assignment
        UserRoleGroup urg = ticket.getAssignedTo();
        if (urg != null) {
            UserRole ur = urg.getUserRole();
            if (ur != null) {
                User atu = ur.getUser();
                if (atu != null) {
                    Integer atid = atu.getId();
                    if (user.getId().equals(atid)) {
                        checkNotifyTech = Boolean.FALSE;
                    }
                }
            }
        }

        th.setNotifyTech(checkNotifyTech);
        th.setNotifyUser(checkNotifyUser);

        // set ticket history type specific attributes
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT, RequestContextUtils.getLocale(request));
        String now = dateFormat.format(new Date());
        switch (thtype) {
            case Constants.THISTTYPE_PHONEMADE:
                th.setSubject(now + " - " +
                        getMessageSourceAccessor().getMessage("phone.callPlaced", "Placed a phone call"));
                break;
            case Constants.THISTTYPE_PHONERECD:
                th.setSubject(now + " - " +
                        getMessageSourceAccessor().getMessage("phone.callReceived", "Received a phone call"));
                break;
            case Constants.THISTTYPE_APPOINTMENT:
                th.setStartDate(new Date());
                th.setDurationAmount(1);
                th.setDurationUnit("collaboration.duration.hours");
                break;
            default:
                break;
        }

        return th;
    }

    @Override
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("apptTypes", appointmentTypeService.getApptTypes());

        String mttabIdStr = ServletRequestUtils.getStringParameter(request, "mttabId");
        Integer mttabId = StringUtils.isBlank(mttabIdStr) ? null : Integer.valueOf(mttabIdStr);
        model.put("mttabId", mttabId);

        return model;
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {

        TicketHistory ticketHistory = (TicketHistory) command;

        if (ticketHistory.getCommentType() == Constants.THISTTYPE_APPOINTMENT ||
                ticketHistory.getCommentType() == Constants.THISTTYPE_TASK) {
            // Determine the start and end dates from some unbound params from the form
            String apptStartDate = ServletRequestUtils.getStringParameter(request, "apptStartDate");
            String apptStartTime = ServletRequestUtils.getStringParameter(request, "apptStartTime", "00:00:00");
            if(StringUtils.isBlank(apptStartTime)) apptStartTime = "00:00:00";

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            // default start to today if it comes in blank
            Date start = new Date();
            if(StringUtils.isNotBlank(apptStartDate)){
                start = df.parse(apptStartDate + " " + apptStartTime.replace("T", ""));
            }
            ticketHistory.setStartDate(start);

            Calendar endcal = Calendar.getInstance();
            endcal.setTime(start);
            int durUnit = Calendar.MINUTE;
            if (ticketHistory.getDurationUnit().equals("collaboration.duration.hours")) {
                durUnit = Calendar.HOUR;
            } else if (ticketHistory.getDurationUnit().equals("collaboration.duration.days")) {
                durUnit = Calendar.DAY_OF_MONTH;
            }
            int durAmount = ticketHistory.getDurationAmount()==null?0:ticketHistory.getDurationAmount();
            endcal.add(durUnit, durAmount);
            ticketHistory.setDueDate(endcal.getTime());
        }

        User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);

        // Check to see if ticketHistory has cc recipient.  If so, add annotation.
        String cc = ticketHistory.getCc();
        if ((cc != null) && (!cc.trim().equals(""))) {
            String note = ticketHistory.getNoteText();
            note += "\r\n\r\n\t[cc: " + cc + "]";
            ticketHistory.setNote(note);
        }

        //Saving ticket history
        ticketHistoryService.saveTicketHistory(ticketHistory, user);

        //Sending email notification
        ticketHistoryService.sendEmailNotification(ticketHistory);

        if (ticketHistory.getCommentType() == Constants.THISTTYPE_APPOINTMENT ||
                ticketHistory.getCommentType() == Constants.THISTTYPE_TASK) {
            String encPwd = (String)request.getSession().getAttribute(SessionConstants.ATTR_USER_PWD);
            Session collSession = collaborationService.getCollaborationSession(user, GLTest.decryptPassword(encPwd));
//            Session collSession = (Session) request.getSession().getAttribute(SessionConstants.COLLABORATION_SESSION);
            sendCalendarItem(user, ticketHistory, collSession);
        }

//        Map<String, Object> model = new HashMap<String, Object>();
//        model.put("successfulSave", true);
//        model.put("timerStatus", ServletRequestUtils.getStringParameter(request, "ts"));
//
//        return showForm(request, response, errors, model);

        // on successful save, return to ticketHistoryList
        String thListUrl = "/ip/ticketHistoryList.glml?tid=" + ticketHistory.getTicket().getId() + "&urgId=" +
                ServletRequestUtils.getIntParameter(request, "urgId");
        return new ModelAndView(new RedirectView(thListUrl, true));
    }

    /**
     * Sends an appointment or task to the users' collaboration calendar.
     *
     * @param user          the user
     * @param ticketHistory - Object containing appointment and task information
     * @param session       the session
     */
    private void sendCalendarItem(User user, TicketHistory ticketHistory, Session session) {
        if (session != null) {
            // send appointments and tasks to the collaborating calendar system
            switch (ticketHistory.getCommentType()) {
                case Constants.THISTTYPE_APPOINTMENT: {
                    Appointment appt = new Appointment();

                    // Message is alwasy html
                    appt.setMessage(ticketHistory.getNote());
                    appt.setContentType("text/html");
                    appt.setSubject(ticketHistory.getSubject());
                    appt.setPlace(ticketHistory.getPlace());

                    // attachments
                    Set<Attachment> attachments = ticketHistory.getAttachments();
                    if (attachments != null) {
                        for (Attachment attachment : attachments) {
                            appt.addAttachment(attachment);
                        }
                    }

                    // recipients
                    String recipients = ticketHistory.getRecipients();
                    if (StringUtils.isNotBlank(recipients)) {
                        String[] recipientList = recipients.split(";");
                        for (String aRecipientList : recipientList) {
                            appt.addRecipient(null, aRecipientList);
                        }
                        appt.addRecipient(null, user.getEmail());
                    }

                    // from
                    appt.setFrom(new NameAndEmail(null, user.getEmail()));

                    // Start and end dates
                    if (ticketHistory.getStartDate() != null) {
                        Calendar startDate = Calendar.getInstance();
                        startDate.setTime(ticketHistory.getStartDate());
                        appt.setStartDate(startDate);
                    }

                    if (ticketHistory.getDueDate() != null) {
                        Calendar endDate = Calendar.getInstance();
                        endDate.setTime(ticketHistory.getDueDate());
                        appt.setEndDate(endDate);
                    }

                    collaborationService.scheduleCalendarItem(appt, session);
                    break;
                }
                case Constants.THISTTYPE_TASK: {
                    Task task = new Task();
                    // Message is alwasy html
                    task.setMessage(ticketHistory.getNote());
                    task.setContentType("text/html");
                    task.setSubject(ticketHistory.getSubject());

                    // attachments
                    Set<Attachment> attachments = ticketHistory.getAttachments();
                    if (attachments != null) {
                        for (Attachment attachment : attachments) {
                            task.addAttachment(attachment);
                        }
                    }

                    // recipients
                    String recipients = ticketHistory.getRecipients();
                    if (StringUtils.isNotBlank(recipients)) {
                        String[] recipientList = recipients.split(";");
                        for (String aRecipientList : recipientList) {
                            task.addRecipient(null, aRecipientList);
                        }
                        // assigned tasks do not need to go in my mailbox
                        //task.addRecipient(null, user.getEmail());
                    }

                    // from
                    task.setFrom(new NameAndEmail(null, user.getEmail()));

                    // Start and end dates
                    if (ticketHistory.getStartDate() != null) {
                        Calendar startDate = Calendar.getInstance();
                        startDate.setTime(ticketHistory.getStartDate());
                        task.setStartDate(startDate);
                    }

                    if (ticketHistory.getDueDate() != null) {
                        Calendar dueDate = Calendar.getInstance();
                        dueDate.setTime(ticketHistory.getDueDate());
                        task.setDueDate(dueDate);
                    }

                    // assigned as of right now
                    task.setAssignedDate(Calendar.getInstance());

                    task.setPriority(ticketHistory.getTaskPriority());

                    collaborationService.scheduleCalendarItem(task, session);
                    break;
                }
                default:
                    break;
            }
        }
    }

    @Override
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
        super.initBinder(request, binder);
        binder.registerCustomEditor(AppointmentType.class, new AppointmentTypeEditor(appointmentTypeService));
    }

    public void setTicketHistoryService(TicketHistoryService ticketHistoryService) {
        this.ticketHistoryService = ticketHistoryService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }

    public void setAppointmentTypeService(AppointmentTypeService appointmentTypeService) {
        this.appointmentTypeService = appointmentTypeService;
    }

    public void setCollaborationService(CollaborationService collaborationService) {
        this.collaborationService = collaborationService;
    }
}
