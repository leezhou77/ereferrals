package net.grouplink.ehelpdesk.web.pagedlistproviders;

import net.grouplink.ehelpdesk.common.beans.support.GLPagedListSourceProvider;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.asset.AssetFilter;
import net.grouplink.ehelpdesk.service.asset.AssetFilterService;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: Sep 17, 2008
 * Time: 12:27:48 PM
 */
public class AssetFilterProvider implements GLPagedListSourceProvider, Serializable {

    private User user;
    private AssetFilterService assetFilterService;

    /**
     * Constructs a new AssetFilterProvider with the associated User and AssetFilterService
     *
     * @param user               the {@link User} that is using this provider.
     * @param assetFilterService the service that provides the Asset List
     */
    public AssetFilterProvider(User user, AssetFilterService assetFilterService) {
        this.user = user;
        this.assetFilterService = assetFilterService;
    }

    /**
     * Load the List for the given Locale and filter settings.
     * The filter object can be of any custom class, preferably a bean
     * for easy data binding from a request. An instance will simply
     * get passed through to this callback method.
     *
     * @param locale Locale that the List should be loaded for,
     *               or <code>null</code> if not locale-specific
     * @param filter object representing filter settings,
     *               or <code>null</code> if no filter options are used
     * @return the loaded List
     * @see net.grouplink.ehelpdesk.common.beans.support.GLRefreshablePagedListHolder#setLocale
     * @see net.grouplink.ehelpdesk.common.beans.support.GLRefreshablePagedListHolder#setFilter
     */
    public List loadList(Locale locale, Object filter) {
        AssetFilter assetFilter = (AssetFilter) filter;
        return assetFilterService.getAssets(assetFilter, user);
    }
}
