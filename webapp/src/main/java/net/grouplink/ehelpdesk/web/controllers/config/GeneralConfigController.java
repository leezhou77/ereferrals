package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.common.utils.PropertiesConstants;
import net.grouplink.ehelpdesk.domain.GeneralConfig;
import net.grouplink.ehelpdesk.service.PropertiesService;
import net.grouplink.ehelpdesk.web.beans.ApplicationInitializer;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * User: jaymehafen
 * Date: Feb 13, 2008
 * Time: 3:59:06 PM
 */
public class GeneralConfigController extends SimpleFormController {
    private PropertiesService propertiesService;
    private ApplicationInitializer applicationInitializer;

    public GeneralConfigController() {
        super();
        setCommandClass(GeneralConfig.class);
        setCommandName("generalConfig");
        setFormView("generalConfig");
    }

    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        GeneralConfig gc = new GeneralConfig();

        String baseUrl = propertiesService.getPropertiesValueByName(PropertiesConstants.BASE_URL);
        gc.setBaseUrl(baseUrl);

        String maxVal = propertiesService.getPropertiesValueByName(PropertiesConstants.MAX_UPLOAD_SIZE);
        Integer maxUpload;
        if (StringUtils.isBlank(maxVal)) {
            maxUpload = 1;
            propertiesService.saveProperties(PropertiesConstants.MAX_UPLOAD_SIZE, String.valueOf(maxUpload));
        } else {
            maxUpload = Integer.valueOf(maxVal);
        }

        gc.setMaxUploadSize(maxUpload);

        // if baseurl is blank, guess value and save
        if (StringUtils.isBlank(gc.getBaseUrl())) {
            StringBuilder sb = guessBaseUrl(request);
            gc.setBaseUrl(sb.toString());
            propertiesService.saveProperties(PropertiesConstants.BASE_URL, gc.getBaseUrl());
            propertiesService.flush();
        }

        gc.setMarqueeText(propertiesService.getPropertiesValueByName(PropertiesConstants.APP_CUSTOMIZATION_SYSTEM_MESSAGE));
        String timeoutLength = propertiesService.getPropertiesValueByName(PropertiesConstants.SESSION_TIMEOUT_LENGTH);
        if ((timeoutLength != null) && (timeoutLength.length() > 0)) {
            try {
                gc.setSessionTimeoutLength(new Integer(timeoutLength));
            } catch (NumberFormatException nfe) {
                gc.setSessionTimeoutLength(30);
            }
        } else {
            gc.setSessionTimeoutLength(30);
        }

        gc.setAllowUserRegistration(propertiesService.getBoolValueByName(PropertiesConstants.ALLOW_USER_REGISTRATION, true));

        return gc;
    }

    private StringBuilder guessBaseUrl(HttpServletRequest request) {
        StringBuilder sb = new StringBuilder()
                .append(request.getScheme())
                .append("://")
                .append(request.getServerName());
        if (request.getServerPort() != 80)
            sb.append(":").append(request.getServerPort());

        sb.append(request.getContextPath());
        return sb;
    }

    protected Map referenceData(HttpServletRequest request) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        m.put("exampleBaseUrl", guessBaseUrl(request));
        return m;
    }

    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response,
                                    Object command, BindException errors) throws Exception {
        GeneralConfig gc = (GeneralConfig) command;
        if (StringUtils.isNotBlank(gc.getBaseUrl()))
            propertiesService.saveProperties(PropertiesConstants.BASE_URL, gc.getBaseUrl());

        propertiesService.saveProperties(PropertiesConstants.MAX_UPLOAD_SIZE, String.valueOf(gc.getMaxUploadSize()));

        propertiesService.saveProperties(PropertiesConstants.APP_CUSTOMIZATION_SYSTEM_MESSAGE, gc.getMarqueeText());
        propertiesService.saveProperties(PropertiesConstants.SESSION_TIMEOUT_LENGTH, gc.getSessionTimeoutLength().toString());
        propertiesService.saveProperties(PropertiesConstants.ALLOW_USER_REGISTRATION, gc.getAllowUserRegistration().toString());
        applicationInitializer.applicationCustomizationInit();

        request.setAttribute("flash.success", getMessageSourceAccessor().getMessage("generalConfig.saved"));

        return new ModelAndView(new RedirectView("generalConfig.glml"));
    }

    public void setPropertiesService(PropertiesService propertiesService) {
        this.propertiesService = propertiesService;
    }

    public void setApplicationInitializer(ApplicationInitializer applicationInitializer) {
        this.applicationInitializer = applicationInitializer;
    }
}
