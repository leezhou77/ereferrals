package net.grouplink.ehelpdesk.web.controllers;

import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.web.tags.Functions;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

public class SystemDispatcherController extends MultiActionController {

    public ModelAndView systemDispatcher(HttpServletRequest request, HttpServletResponse response)
            throws ServletException {
        assert response != null;
        
        HttpSession session = request.getSession(true);
        User user = (User) session.getAttribute(SessionConstants.ATTR_USER);

        // intercept PDA users, send them to the PDA screen
        if (Functions.isPDA(request.getHeader("User-Agent"))) {
            // Redirect iPhone, iPod users to the ip pages
            if(Functions.isIPodPhone(request.getHeader("User-Agent"))){
                return new ModelAndView(new RedirectView("/ip/home.glml", true));
            }

            return new ModelAndView(new RedirectView("/pda/ticketList.glml", true));
        }

        if (user.getLoginId().equalsIgnoreCase(User.ADMIN_LOGIN)) {
            return new ModelAndView(new RedirectView("/config/general/generalConfig.glml", true));
        } else {
            return new ModelAndView(new RedirectView("/home.glml", true));
        }
    }

    public ModelAndView handleSessionTimeout(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        assert request != null;

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("Session Updated - Server Time:" + new Date().toString());
        return null;
    }
}
