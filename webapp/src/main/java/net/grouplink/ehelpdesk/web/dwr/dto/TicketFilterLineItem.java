package net.grouplink.ehelpdesk.web.dwr.dto;

public class TicketFilterLineItem {
    private String operatorName;
    private String displayName;
    private String propertyName;
    private Object nameOrId;
    private Integer index;
    private String cfName;
    private String deleteMethodName;


    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public Object getNameOrId() {
        return nameOrId;
    }

    public void setNameOrId(Object nameOrId) {
        this.nameOrId = nameOrId;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getCfName() {
        return cfName;
    }

    public void setCfName(String cfName) {
        this.cfName = cfName;
    }

    public String getDeleteMethodName() {
        return deleteMethodName;
    }

    public void setDeleteMethodName(String deleteMethodName) {
        this.deleteMethodName = deleteMethodName;
    }
}
