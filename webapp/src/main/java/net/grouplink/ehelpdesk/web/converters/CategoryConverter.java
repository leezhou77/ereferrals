package net.grouplink.ehelpdesk.web.converters;

import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: 11/8/11
 * Time: 1:11 PM
 */
public class CategoryConverter implements Converter<String, Category> {
    @Autowired
    private CategoryService categoryService;

    public Category convert(String id){
        try{
            int categoryId = Integer.parseInt(id);
            return categoryService.getCategoryById(categoryId);
        } catch (Exception e){
            return null;
        }
    }
}
