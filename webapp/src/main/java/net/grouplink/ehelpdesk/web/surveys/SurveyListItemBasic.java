package net.grouplink.ehelpdesk.web.surveys;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Apr 1, 2008
 * Time: 12:57:26 AM
 */
public class SurveyListItemBasic {

    private int type;
    private boolean surveyEnabled;
    private int objectId;
    private String objectName;
    private boolean controlEnabled;

    public static final int GROUP = 1;
    public static final int CATEGORY = 2;
    public static final int OPTION = 3;

    public SurveyListItemBasic() {
        type = GROUP;
        objectId = -9999;
        surveyEnabled = false;
        objectName = "";
        controlEnabled = true;
    }

    public SurveyListItemBasic(int type, int objectId, boolean surveyEnabled, String objectName) {
        this.type = type;
        this.objectId = objectId;
        this.surveyEnabled = surveyEnabled;
        this.objectName = objectName;
        this.controlEnabled = true;
    }

    public SurveyListItemBasic(int type, int objectId, boolean surveyEnabled, String objectName, boolean controlEnabled) {
        this.type = type;
        this.objectId = objectId;
        this.surveyEnabled = surveyEnabled;
        this.objectName = objectName;
        this.controlEnabled = controlEnabled;
    }

    public boolean getSurveyEnabled() {
        return surveyEnabled;
    }

    public int getObjectId() {
        return objectId;
    }

    public int getType() {
        return type;
    }

    public String getObjectName() {
        return objectName;
    }

    public boolean getControlEnabled() {
        return controlEnabled;
    }

    public void setSurveyEnabled(boolean surveyEnabled) {
        this.surveyEnabled = surveyEnabled;
    }

    public void setObjectId(int objectId) {
        this.objectId = objectId;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public void setControlEnabled(boolean controlEnabled) {
        this.controlEnabled = controlEnabled;
    }
}
