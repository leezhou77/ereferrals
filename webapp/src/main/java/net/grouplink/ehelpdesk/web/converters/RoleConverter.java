package net.grouplink.ehelpdesk.web.converters;

import net.grouplink.ehelpdesk.domain.Role;
import net.grouplink.ehelpdesk.service.RoleService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;


public class RoleConverter implements Converter<String, Role> {
    @Autowired
    private RoleService roleService;

    public Role convert(String id) {
        if (StringUtils.isBlank(id)) return null;

        return roleService.getRoleById(Integer.valueOf(id));
    }
}
