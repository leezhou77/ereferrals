package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Survey;
import net.grouplink.ehelpdesk.domain.SurveyFrequency;
import net.grouplink.ehelpdesk.domain.SurveyQuestion;
import net.grouplink.ehelpdesk.domain.SurveyQuestionResponse;
import net.grouplink.ehelpdesk.service.CategoryOptionService;
import net.grouplink.ehelpdesk.service.CategoryService;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.SurveyFrequencyService;
import net.grouplink.ehelpdesk.service.SurveyQuestionResponseService;
import net.grouplink.ehelpdesk.service.SurveyQuestionService;
import net.grouplink.ehelpdesk.service.SurveyService;
import net.grouplink.ehelpdesk.web.surveys.SurveyListItemBasic;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Feb 13, 2008
 * Time: 1:48:00 PM
 */
public class SurveyManagementController extends MultiActionController {

    private CategoryService categoryService;
    private CategoryOptionService categoryOptionService;
    private GroupService groupService;
    private SurveyService surveyService;
    private SurveyFrequencyService surveyFrequencyService;
    private SurveyQuestionResponseService surveyQuestionResponseService;


    public SurveyQuestionService getSurveyQuestionService() {
        return surveyQuestionService;
    }

    public void setSurveyQuestionService(SurveyQuestionService surveyQuestionService) {
        this.surveyQuestionService = surveyQuestionService;
    }

    private SurveyQuestionService surveyQuestionService;

    public ModelAndView surveyManage(HttpServletRequest request, HttpServletResponse response) throws Exception {
	    Map model = new HashMap();

        List groups = groupService.getGroups();
        model.put("groups", groups);

        List surveys = surveyService.getActiveSurveys();
        if(surveys.size() == 0) {
            initializeSurveyTables();
        }

        List _surveys = getNoDupeSurveyNameList(surveys);
        model.put("surveys", _surveys);

        surveys = surveyService.getActiveSurveys();
        Survey fSurvey = (Survey) surveys.get(0);
        model.put("firstSurvey", fSurvey);

        return new ModelAndView("surveyManagement", model);
    }


    public ModelAndView surveyActivationList(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Map model = new HashMap();
        int sgid = ServletRequestUtils.getIntParameter(request, "sgid", -9999);
        model.put("sgid", new Integer(sgid));

        if (sgid < 0) {
            model.put("surveyActivatedForAll", Boolean.valueOf(surveyService.getDefaultSurveyIsActivatedForAllGroups()));
        } else {
            model.put("surveyActivatedForAll", Boolean.valueOf(surveyService.getDefaultSurveyIsActivatedForAllGroups()));
            model.put("surveyActivatedForGroup", Boolean.valueOf(surveyService.getDefaultSurveyIsActivatedForGroup(sgid)));
            model.put("surveyItems", getSurveyItemsForGroup(sgid));
            model.put("group", groupService.getGroupById(sgid));
        }
        return new ModelAndView("surveyList", model);
    }

    

    private List getSurveyItemsForGroup(int sgid) {
       List surveyItems = new ArrayList();
       List categoriesList = categoryService.getCategoryByGroupId(new Integer(sgid));
       Iterator categoriesListIterator = categoriesList.iterator();
       while (categoriesListIterator.hasNext()) {
            Category category = (Category) categoriesListIterator.next();
            int catId = category.getId().intValue();
            boolean catSurveyActivated = surveyService.getDefaultSurveyIsActivatedForCategory(catId);
            SurveyListItemBasic item = new SurveyListItemBasic(SurveyListItemBasic.CATEGORY, catId, catSurveyActivated, category.getName());
            surveyItems.add(item);
            List categoryOptionsList = categoryOptionService.getCategoryOptionByCategoryId(new Integer(catId));
            Iterator categoryOptionsListIterator = categoryOptionsList.iterator();
            while (categoryOptionsListIterator.hasNext()) {
                CategoryOption categoryOption = (CategoryOption) categoryOptionsListIterator.next();
                int catOptId = categoryOption.getId().intValue();
                Survey survey = categoryOption.getSurvey();
                boolean surveyActivated = false;
                if ((survey != null) && (survey.getId().intValue() == 1))
                    surveyActivated = true;
                item = new SurveyListItemBasic(SurveyListItemBasic.OPTION, catOptId, surveyActivated, categoryOption.getName(), !catSurveyActivated);
                surveyItems.add(item);
            }
        }
        return surveyItems;
    }

    private void initializeSurveyTables(){
        // db has not been initialized for surveytables
        SurveyFrequency surveyFrequency = new SurveyFrequency();
        surveyFrequency.setValue("Always");
        surveyFrequencyService.save(surveyFrequency);
        surveyFrequency = new SurveyFrequency();
        surveyFrequency.setValue("Random");
        surveyFrequencyService.save(surveyFrequency);
        surveyFrequency = new SurveyFrequency();
        surveyFrequency.setValue("Date Range");
        surveyFrequencyService.save(surveyFrequency);
        surveyFrequencyService.flush();

        Survey survey = new Survey();
        survey.setName("Default Survey");
        survey.setSurveyFrequency(surveyFrequencyService.getDefaultSurveyFrequency());
        survey.setActive(Boolean.TRUE);
        surveyService.saveSurvey(survey);
        surveyService.flush();

        SurveyQuestion surveyQuestion1 = new SurveyQuestion();
        surveyQuestion1.setSurvey(survey);
        surveyQuestion1.setSurveyQuestionTypeId(3);
        surveyQuestion1.setSurveyQuestionText("Was your referral answered?");
        surveyQuestion1.setDisplayOrder(1);
        surveyQuestionService.saveSurveyQuestion(surveyQuestion1);

        SurveyQuestion surveyQuestion2 = new SurveyQuestion();
        surveyQuestion2.setSurvey(survey);
        surveyQuestion2.setSurveyQuestionTypeId(3);
        surveyQuestion2.setSurveyQuestionText("Was your referral answered in a timely manner?");
        surveyQuestion2.setDisplayOrder(2);
        surveyQuestionService.saveSurveyQuestion(surveyQuestion2);

        SurveyQuestion surveyQuestion3 = new SurveyQuestion();
        surveyQuestion3.setSurvey(survey);
        surveyQuestion3.setSurveyQuestionTypeId(3);
        surveyQuestion3.setSurveyQuestionText("Please rate your experience on a scale of 1 to 5, 1 being unsatisfactory, 5 being excellent:");
        surveyQuestion3.setDisplayOrder(3);
        surveyQuestionService.saveSurveyQuestion(surveyQuestion3);
        surveyQuestionService.flush();

        SurveyQuestionResponse surveyQuestionResponse = new SurveyQuestionResponse();
        surveyQuestionResponse.setSurveyQuestion(surveyQuestion1);
        surveyQuestionResponse.setSurveyQuestionResponseText("Yes");
        surveyQuestionResponse.setSelected(Boolean.TRUE);
        surveyQuestionResponseService.saveSurveyQuestionResponse(surveyQuestionResponse);

        surveyQuestionResponse = new SurveyQuestionResponse();
        surveyQuestionResponse.setSurveyQuestion(surveyQuestion1);
        surveyQuestionResponse.setSurveyQuestionResponseText("No");
        surveyQuestionResponse.setSelected(Boolean.FALSE);
        surveyQuestionResponseService.saveSurveyQuestionResponse(surveyQuestionResponse);

        surveyQuestionResponse = new SurveyQuestionResponse();
        surveyQuestionResponse.setSurveyQuestion(surveyQuestion2);
        surveyQuestionResponse.setSurveyQuestionResponseText("Yes");
        surveyQuestionResponse.setSelected(Boolean.TRUE);
        surveyQuestionResponseService.saveSurveyQuestionResponse(surveyQuestionResponse);

        surveyQuestionResponse = new SurveyQuestionResponse();
        surveyQuestionResponse.setSurveyQuestion(surveyQuestion2);
        surveyQuestionResponse.setSurveyQuestionResponseText("No");
        surveyQuestionResponse.setSelected(Boolean.FALSE);
        surveyQuestionResponseService.saveSurveyQuestionResponse(surveyQuestionResponse);

        surveyQuestionResponse = new SurveyQuestionResponse();
        surveyQuestionResponse.setSurveyQuestion(surveyQuestion3);
        surveyQuestionResponse.setSurveyQuestionResponseText("1");
        surveyQuestionResponse.setSelected(Boolean.TRUE);
        surveyQuestionResponseService.saveSurveyQuestionResponse(surveyQuestionResponse);

        surveyQuestionResponse = new SurveyQuestionResponse();
        surveyQuestionResponse.setSurveyQuestion(surveyQuestion3);
        surveyQuestionResponse.setSurveyQuestionResponseText("2");
        surveyQuestionResponse.setSelected(Boolean.FALSE);
        surveyQuestionResponseService.saveSurveyQuestionResponse(surveyQuestionResponse);

        surveyQuestionResponse = new SurveyQuestionResponse();
        surveyQuestionResponse.setSurveyQuestion(surveyQuestion3);
        surveyQuestionResponse.setSurveyQuestionResponseText("3");
        surveyQuestionResponse.setSelected(Boolean.FALSE);
        surveyQuestionResponseService.saveSurveyQuestionResponse(surveyQuestionResponse);

        surveyQuestionResponse = new SurveyQuestionResponse();
        surveyQuestionResponse.setSurveyQuestion(surveyQuestion3);
        surveyQuestionResponse.setSurveyQuestionResponseText("4");
        surveyQuestionResponse.setSelected(Boolean.FALSE);
        surveyQuestionResponseService.saveSurveyQuestionResponse(surveyQuestionResponse);

        surveyQuestionResponse = new SurveyQuestionResponse();
        surveyQuestionResponse.setSurveyQuestion(surveyQuestion3);
        surveyQuestionResponse.setSurveyQuestionResponseText("5");
        surveyQuestionResponse.setSelected(Boolean.FALSE);
        surveyQuestionResponseService.saveSurveyQuestionResponse(surveyQuestionResponse);

        surveyQuestionResponseService.flush();

    }

    public ModelAndView surveyActivate(HttpServletRequest request, HttpServletResponse response) throws Exception {

        int sgid = ServletRequestUtils.getIntParameter(request, "sgid", -9999);

        if (sgid < 0) {
            // All groups tab was selected
            boolean enableSurveyForAllGroups = ServletRequestUtils.getBooleanParameter(request, "rbEnableSurveyForAllGroups", false);
            if (enableSurveyForAllGroups)
                surveyService.activateSurveyForAllGroups(1);
            else
                surveyService.deactivateSurveysForAllGroups();
        } else {

            boolean groupHasChanged = ServletRequestUtils.getBooleanParameter(request, "groupHasChanged", false);
            if (groupHasChanged) {
                boolean enableSurveyForAllCatOptsInGroup = ServletRequestUtils.getBooleanParameter(request, "rbEnableSurveyForCatOptsInGroup", false);
                if (enableSurveyForAllCatOptsInGroup)
                    surveyService.activateSurveyForAllCatOptsInGroup(1, sgid);
                else
                    surveyService.deactivateSurveysForAllCatOptsInGroup(sgid);
            } else {
                // Step One -- Loop through all categories in the selected group and activate or
                // deactivate all surveys in each based on radio button selection
                List categoriesList = categoryService.getCategoryByGroupId(new Integer(sgid));
                Iterator categoriesListIterator = categoriesList.iterator();
                while (categoriesListIterator.hasNext()) {
                    Category category = (Category) categoriesListIterator.next();
                    int catid = category.getId().intValue();
                    boolean categoryHasChanged = ServletRequestUtils.getBooleanParameter(request, "categoryHasChanged_"+catid, false);

                    if (categoryHasChanged) {
                        boolean enableSurveyForAllOptsInCat = ServletRequestUtils.getBooleanParameter(request, "rbEnableSurveyForAllOptsInCat_"+catid, false);
                        if (enableSurveyForAllOptsInCat)
                            surveyService.activateSurveyForCategory(1, catid);
                        else
                            surveyService.deactivateSurveysForAllOptsInCategory(catid);
                    }
                }

                // Step Two -- Loop through each of the category options in the selected group and see if they
                // belong to a category that has already been activated or not.
                List catOptsList = categoryOptionService.getCategoryOptionByGroupId(new Integer(sgid));
                Iterator catOptsListIterator = catOptsList.iterator();
                while (catOptsListIterator.hasNext()) {
                    CategoryOption catOpt = (CategoryOption) catOptsListIterator.next();
                    int catoptid = catOpt.getId().intValue();
                    boolean catOptHasChanged = ServletRequestUtils.getBooleanParameter(request, "categoryOptionHasChanged_"+catoptid, false);
                    if (catOptHasChanged) {
                        boolean enableSurveyForCatOpt = ServletRequestUtils.getBooleanParameter(request, "rbEnableSurveyForCatOpt_"+catoptid, false);
                        if (enableSurveyForCatOpt)
                            surveyService.activateSurveyForCategoryOption(1, catoptid);
                        else
                            surveyService.deactivateSurveyForCategoryOption(catoptid);
                    }
                }
            }
        }
        response.setStatus(HttpServletResponse.SC_OK);
        return null;
    }


    private Integer getSurveyIdFromCatOptsList(List catOptsList) {
        Integer sid = null;
        Iterator it = catOptsList.iterator();
        while (it.hasNext()) {
            CategoryOption cOpt = (CategoryOption) it.next();
            Survey s = cOpt.getSurvey();
            if (s != null) {
                if (sid != null) {
                    if (!sid.equals(s.getId()))
                        return new Integer(-9999);
                }
                sid = s.getId();
            }
        }
        return sid;
    }


    private List getNoDupeSurveyNameList(List surveys) {
        List noDupes = new ArrayList();
        List noDupeNames = new ArrayList();
        for (int i=0; i<surveys.size(); i++) {
            Survey survey = (Survey) surveys.get(i);
            if (!noDupeNames.contains(survey.getName())) {
                noDupeNames.add(survey.getName());
                noDupes.add(survey);
            }
        }
        return noDupes;
    }



    // Spring Injection

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setCategoryOptionService(CategoryOptionService categoryOptionService) {
        this.categoryOptionService = categoryOptionService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setSurveyService(SurveyService surveyService) {
        this.surveyService = surveyService;
    }

    public void setSurveyFrequencyService(SurveyFrequencyService surveyFrequencyService) {
        this.surveyFrequencyService = surveyFrequencyService;
    }

    public void setSurveyQuestionResponseService(SurveyQuestionResponseService surveyQuestionResponseService) {
        this.surveyQuestionResponseService = surveyQuestionResponseService;
    }
}
