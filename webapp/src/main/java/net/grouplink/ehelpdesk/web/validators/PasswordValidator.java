package net.grouplink.ehelpdesk.web.validators;

import net.grouplink.ehelpdesk.domain.Address;
import net.grouplink.ehelpdesk.web.domain.ChangePassword;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: 1/9/12
 * Time: 4:39 PM
 */
@Component
public class PasswordValidator implements Validator {

    public boolean supports(Class<?> aClass) {
        return aClass.equals(ChangePassword.class);
    }

    public void validate(Object o, Errors errors) {
        ChangePassword cpwd = (ChangePassword) o;

        if (!StringUtils.equals(cpwd.getPassword1(), cpwd.getPassword2())) {
            errors.rejectValue("password2", "global.passwordMatchError", "Passwords don't match");
        }
    }
}
