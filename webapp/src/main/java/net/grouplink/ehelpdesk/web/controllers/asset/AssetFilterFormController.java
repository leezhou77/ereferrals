package net.grouplink.ehelpdesk.web.controllers.asset;

import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.asset.AssetCustomField;
import net.grouplink.ehelpdesk.domain.asset.AssetFieldGroup;
import net.grouplink.ehelpdesk.domain.asset.AssetFilter;
import net.grouplink.ehelpdesk.domain.asset.AssetReportGroupByOptions;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilterOperators;
import net.grouplink.ehelpdesk.service.CategoryOptionService;
import net.grouplink.ehelpdesk.service.CategoryService;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LocationService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.asset.AssetFieldGroupService;
import net.grouplink.ehelpdesk.service.asset.AssetFilterService;
import net.grouplink.ehelpdesk.service.asset.AssetStatusService;
import net.grouplink.ehelpdesk.service.asset.AssetTypeService;
import net.grouplink.ehelpdesk.service.asset.VendorService;
import net.grouplink.ehelpdesk.web.controllers.AbstractEHelpDeskFormController;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * User: mark
 * Date: Sep 9, 2008
 * Time: 3:10:51 PM
 */
public class AssetFilterFormController extends AbstractEHelpDeskFormController {

    public static final String AFC_FORM_ASSETFILTER = "AFC.FORM.ASSETFILTER";

    private AssetFilterService assetFilterService;
    private AssetStatusService assetStatusService;
    private UserService userService;
    private LocationService locationService;
    private GroupService groupService;
    private CategoryService categoryService;
    private CategoryOptionService categoryOptionService;
    private AssetFieldGroupService assetFieldGroupService;
    private AssetTypeService assetTypeService;
    private VendorService vendorService;

    public AssetFilterFormController() {
        super();
        setCommandClass(AssetFilter.class);
        setCommandName("assetFilter");
        setFormView("assetFilterEdit");
        setSuccessView("assetFilterEdit");
        setSessionForm(true);
    }

    protected String getFormSessionAttributeName(HttpServletRequest request) {
        return AFC_FORM_ASSETFILTER;
    }

    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        Integer afId = ServletRequestUtils.getIntParameter(request, "afId");

        // Check for the session fbo first
        AssetFilter assetFilter = (AssetFilter) request.getSession().getAttribute(AFC_FORM_ASSETFILTER);
        if (assetFilter != null) {
            if (afId == null && assetFilter.getId() == null) {
                return assetFilter;
            } else if (assetFilter.getId() != null && assetFilter.getId().equals(afId)) {
                return assetFilter;
            }
        }

        // This is either brand new or the id has changed.
        if (afId == null) {
            assetFilter = getNewAssetFilter();
        } else {
            assetFilter = assetFilterService.getById(afId);
        }
        return assetFilter;
    }

    // sets up a new AssetFilter setting some defaults 
    private AssetFilter getNewAssetFilter() {
        AssetFilter assetFilter = new AssetFilter();
        assetFilter.setPrivateFilter(Boolean.TRUE);

        // load some default columOrders
        String[] defCols = {
                "asset.assetNumber",
                "asset.name",
                "asset.location",
                "asset.assetlocation",
                "asset.group",
                "asset.category",
                "asset.categoryoption"
        };
        assetFilter.setColumnOrder(defCols);
        assetFilter.setName(getMessageSourceAccessor().getMessage("assetFilter.defaultName"));
        return assetFilter;
    }

    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        AssetFilter assetFilter = (AssetFilter) command;
        User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);
        assetFilter.setUser(user);

        // always save this ticketfilter on the session
        request.getSession().setAttribute(AssetFilterFormController.AFC_FORM_ASSETFILTER, assetFilter);

        // determine whether to persist to db (save), just show results (apply),  delete(delete)
        // or just reload the filter to the filter edit page (reload)
        String formAction = ServletRequestUtils.getStringParameter(request, "formAction", "save");
        if ("apply".equals(formAction)) {
            request.setAttribute("apply", "true");

            // set the url for either showing the search results as a list or as a report
            String applyURL = "/asset/filter/results.glml";
            if (assetFilter.getReport()) {
                applyURL = "/asset/report/dynamicAssetReport.glml";
            }
            
            request.setAttribute("applyURL", applyURL);
        } else if ("delete".equals(formAction)) {
            assetFilterService.delete(assetFilter);
            request.setAttribute("deleted", "true");
            request.getSession().setAttribute(AssetFilterFormController.AFC_FORM_ASSETFILTER, getNewAssetFilter());
        } else { // save
            assetFilterService.save(assetFilter);
            request.setAttribute("saved", "true");
        }
        
        return showForm(request, response, errors);
    }

    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
        Map<String, Object> refData = new HashMap<String, Object>();
        refData.put("assetStatus", assetStatusService.getAllAssetStatuses());
        refData.put("dateOps", getDateOpOptions());
        refData.put("groupByOptions", getReportGroupByOptions());
        refData.put("users", userService.getUsers());
        refData.put("locations", locationService.getLocations());
        refData.put("groups", groupService.getGroups());
        refData.put("categories", categoryService.getCategories());
        refData.put("catOptions", categoryOptionService.getCategoryOptions());
        refData.put("fieldGroups", assetFieldGroupService.getAllAssetFieldGroups());
        refData.put("assetTypes", assetTypeService.getAllAssetTypes());
        refData.put("vendors", vendorService.getAllVendors());
        refData.put("availableCols", getAvailableColumns((AssetFilter) command));

        return refData;
    }

    // returns a list of all available columns with any in the colorder field
    // of the AssetFilter removed
    private List getAvailableColumns(AssetFilter assetFilter) {
        List<String> list = new ArrayList<String>();

        // load up asset fields
        list.add("asset.assetNumber");
        list.add("asset.name");
        list.add("asset.type");
        list.add("asset.owner");
        list.add("asset.vendor");
        list.add("asset.assetlocation");
        list.add("asset.acquisitionDate");
        list.add("asset.location");
        list.add("asset.group");
        list.add("asset.category");
        list.add("asset.categoryoption");
        list.add("asset.manufacturer");
        list.add("asset.modelNumber");
        list.add("asset.serialnumber");
        list.add("asset.description");
        list.add("asset.status");
        // load up accounting info fields
        list.add("accountinginfo.ponumber");
        list.add("accountinginfo.accountnumber");
        list.add("accountinginfo.accumulatednumber");
        list.add("accountinginfo.expensenumber");
        list.add("accountinginfo.acquisitionvalue");
        list.add("accountinginfo.leasenumber");
        list.add("accountinginfo.leaseduration");
        list.add("accountinginfo.leasefrequency");
        list.add("accountinginfo.leasefrequencyprice");
        list.add("accountinginfo.disposalmethod");
        list.add("accountinginfo.disposaldate");
        list.add("accountinginfo.insurancecategory");
        list.add("accountinginfo.replacementvaluecategory");
        list.add("accountinginfo.replacementvalue");
        list.add("accountinginfo.maintenancecost");
        list.add("accountinginfo.depreciationmethod");
        list.add("accountinginfo.leaseexpirationdate");
        list.add("accountinginfo.warrantydate");
        // load up custom fields by field group
        List<AssetFieldGroup> fieldGroups = assetFieldGroupService.getAllAssetFieldGroups();
        for (AssetFieldGroup assetFieldGroup : fieldGroups) {
            Set<AssetCustomField> customFields = assetFieldGroup.getCustomFields();
            for (AssetCustomField assetCustomField : customFields) {
                list.add(assetCustomField.getName());
            }
        }

        // now remove any items that may be in the assetfilter.colorder
        if (assetFilter.getColumnOrder() != null) {
            List colOrder = Arrays.asList(assetFilter.getColumnOrder());
            list.removeAll(colOrder);
        }

        return list;
    }

    // creates a list of the group by options for asset reports
    private List getReportGroupByOptions() {
        List<AssetReportGroupByOptions> list = new ArrayList<AssetReportGroupByOptions>();
        list.add(new AssetReportGroupByOptions(AssetReportGroupByOptions.TYPE, getMessageSourceAccessor().getMessage("asset.type")));
        list.add(new AssetReportGroupByOptions(AssetReportGroupByOptions.STATUS, getMessageSourceAccessor().getMessage("asset.status")));
        list.add(new AssetReportGroupByOptions(AssetReportGroupByOptions.LOCATION, getMessageSourceAccessor().getMessage("asset.location")));
        list.add(new AssetReportGroupByOptions(AssetReportGroupByOptions.GROUP, getMessageSourceAccessor().getMessage("asset.group")));
        list.add(new AssetReportGroupByOptions(AssetReportGroupByOptions.CATEGORY, getMessageSourceAccessor().getMessage("asset.category")));
        list.add(new AssetReportGroupByOptions(AssetReportGroupByOptions.CATEGORY_OPTION, getMessageSourceAccessor().getMessage("asset.categoryoption")));
        list.add(new AssetReportGroupByOptions(AssetReportGroupByOptions.OWNER, getMessageSourceAccessor().getMessage("asset.owner")));
        return list;
    }

    // creates a list of the system date operator options. Used to create a drop-down
    // list for the asset search filters on dates
    private List getDateOpOptions() {
        List<TicketFilterOperators> list = new ArrayList<TicketFilterOperators>();
        // Any
        TicketFilterOperators ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_ANY);
        ticketFilterOperators.setDisplay("-- " + getMessageSourceAccessor().getMessage("ticketSearch.any") + " --");
        list.add(ticketFilterOperators);
        // Today
        ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_TODAY);
        ticketFilterOperators.setDisplay(getMessageSourceAccessor().getMessage("date.today"));
        list.add(ticketFilterOperators);
        // Week
        ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_WEEK);
        ticketFilterOperators.setDisplay(getMessageSourceAccessor().getMessage("date.thisweek"));
        list.add(ticketFilterOperators);
        // Month
        ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_MONTH);
        ticketFilterOperators.setDisplay(getMessageSourceAccessor().getMessage("date.thismonth"));
        list.add(ticketFilterOperators);
        // On
        ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_ON);
        ticketFilterOperators.setDisplay(getMessageSourceAccessor().getMessage("date.on"));
        list.add(ticketFilterOperators);
        // Before
        ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_BEFORE);
        ticketFilterOperators.setDisplay(getMessageSourceAccessor().getMessage("date.before"));
        list.add(ticketFilterOperators);
        // After
        ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_AFTER);
        ticketFilterOperators.setDisplay(getMessageSourceAccessor().getMessage("date.after"));
        list.add(ticketFilterOperators);
        // Range
        ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_RANGE);
        ticketFilterOperators.setDisplay(getMessageSourceAccessor().getMessage("date.range"));
        list.add(ticketFilterOperators);

        return list;
    }

    public void afterPropertiesSet() throws Exception {
        if (assetFilterService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: assetFilterService");
    }

    public void setAssetFilterService(AssetFilterService assetFilterService) {
        this.assetFilterService = assetFilterService;
    }

    public void setAssetStatusService(AssetStatusService assetStatusService) {
        this.assetStatusService = assetStatusService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setCategoryOptionService(CategoryOptionService categoryOptionService) {
        this.categoryOptionService = categoryOptionService;
    }

    public void setAssetFieldGroupService(AssetFieldGroupService assetFieldGroupService) {
        this.assetFieldGroupService = assetFieldGroupService;
    }

    public void setAssetTypeService(AssetTypeService assetTypeService) {
        this.assetTypeService = assetTypeService;
    }

    public void setVendorService(VendorService vendorService) {
        this.vendorService = vendorService;
    }
}
