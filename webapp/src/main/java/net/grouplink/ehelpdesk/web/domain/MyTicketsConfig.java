package net.grouplink.ehelpdesk.web.domain;

import net.grouplink.ehelpdesk.domain.MyTicketTab;

import java.io.Serializable;
import java.util.List;

public class MyTicketsConfig implements Serializable {
    private List<MyTicketTab> myTicketTabs;

    public List<MyTicketTab> getMyTicketTabs() {
        return myTicketTabs;
    }

    public void setMyTicketTabs(List<MyTicketTab> myTicketTabs) {
        this.myTicketTabs = myTicketTabs;
    }
}
