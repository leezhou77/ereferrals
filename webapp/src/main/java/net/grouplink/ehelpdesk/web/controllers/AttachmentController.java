package net.grouplink.ehelpdesk.web.controllers;

import net.grouplink.ehelpdesk.domain.Attachment;
import net.grouplink.ehelpdesk.service.AttachmentService;
import net.grouplink.ehelpdesk.service.KnowledgeBaseService;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author mrollins
 * @version 1.0
 */
public class AttachmentController extends MultiActionController {
    private KnowledgeBaseService knowledgeBaseService;
    private AttachmentService attachmentService;

    public ModelAndView handleDownload(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        String attchId = ServletRequestUtils.getStringParameter(req, "aid");

        if (StringUtils.isNotBlank(attchId)) {
            Attachment attachment = attachmentService.getById(Integer.valueOf(attchId));

            if (attachment != null) {
                resp.setContentType(attachment.getContentType());
                String fileName = attachment.getFileName();
                resp.addHeader("Content-disposition", "attachment; filename=\"" + fileName + "\"");
                resp.getOutputStream().write(attachment.getFileData());
            }
        }

        return null;
    }

    public ModelAndView nonPrivateHandleDownload(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        String attchId = ServletRequestUtils.getStringParameter(req, "aid");

        if (StringUtils.isNotBlank(attchId)) {
            Attachment attachment = knowledgeBaseService.getNonPrivateAttachment(Integer.valueOf(attchId));

            if (attachment != null) {
                resp.setContentType(attachment.getContentType());
                resp.addHeader("Content-disposition", "attachment; filename=\"" + attachment.getFileName() + "\"");
                resp.getOutputStream().write(attachment.getFileData());
            }
        }

        return null;
    }

    public void setKnowledgeBaseService(KnowledgeBaseService knowledgeBaseService) {
        this.knowledgeBaseService = knowledgeBaseService;
    }

    public void setAttachmentService(AttachmentService attachmentService) {
        this.attachmentService = attachmentService;
    }
}
