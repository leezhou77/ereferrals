package net.grouplink.ehelpdesk.web.formatters;

import net.grouplink.ehelpdesk.domain.Role;
import net.grouplink.ehelpdesk.service.RoleService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class RoleFormatter implements Formatter<Role> {
    @Autowired
    private RoleService roleService;

    public Role parse(String text, Locale locale) throws ParseException {
        if (StringUtils.isNotBlank(text)) {
            return roleService.getRoleById(Integer.valueOf(text));
        } else {
            return null;
        }
    }

    public String print(Role object, Locale locale) {
        return object == null ? "" : object.getId().toString();
    }
}
