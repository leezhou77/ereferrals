package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.TicketTemplate;
import net.grouplink.ehelpdesk.domain.TicketTemplateMaster;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.TemplateMasterService;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TemplateMasterFormController extends SimpleFormController {

    private TemplateMasterService templateMasterService;
    private GroupService groupService;
    private PermissionService permissionService;

    public TemplateMasterFormController() {
        setCommandClass(TicketTemplateMaster.class);
        setCommandName("templateMaster");
        setFormView("ticketTemplates/templateMasterEdit");
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        Integer id = ServletRequestUtils.getIntParameter(request, "tmId");
        Integer groupId = ServletRequestUtils.getIntParameter(request, "groupId");
        Group group = groupService.getGroupById(groupId);

        // check if user has ticketTemplateManagement permission
        if (!permissionService.hasGroupPermission("group.ticketTemplateManagement", group)) {
            throw new AccessDeniedException("Access is denied");
        }

        TicketTemplateMaster master;
        if (id == null) {
            master = new TicketTemplateMaster();

            master.setGroup(group);
            master.setPrivified(true);
        } else {
            master = templateMasterService.getById(id);
        }

        return master;
    }

    @Override
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {

        List<TicketTemplate> tts = new ArrayList<TicketTemplate>(((TicketTemplateMaster)command).getTicketTemplates());
        Collections.sort(tts);
        Integer firstId = -1;
        if(tts.size() > 0) firstId = tts.get(0).getId();

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("selectWorkflowTab", ServletRequestUtils.getBooleanParameter(request, "wfTab", false));
        model.put("firstId", firstId);
        return model;
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors)
            throws Exception {
        TicketTemplateMaster master = (TicketTemplateMaster) command;
        templateMasterService.save(master);
        templateMasterService.flush();

        request.setAttribute("flash.success", getMessageSourceAccessor().getMessage("ticketTemplate.saveSuccesssful"));
        return new ModelAndView(new RedirectView("/config/templateMasterEdit.glml?groupId=" + master.getGroup().getId() + "&tmId=" + master.getId(), true));
    }

    public void setTemplateMasterService(TemplateMasterService templateMasterService) {
        this.templateMasterService = templateMasterService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }
}
