package net.grouplink.ehelpdesk.web.controllers.config.acl;

import net.grouplink.ehelpdesk.common.utils.MultiComparator;
import net.grouplink.ehelpdesk.domain.Role;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.acl.*;
import net.grouplink.ehelpdesk.domain.acl.PermissionActors;
import net.grouplink.ehelpdesk.service.RoleService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.acl.PermissionAvailabilityService;
import net.grouplink.ehelpdesk.service.acl.PermissionInitializerService;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.*;

@Controller
@RequestMapping(value = "/acl/permissionSchemes")
public class PermissionSchemesController extends ApplicationObjectSupport {

    @Autowired
    private PermissionService permissionService;
    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private PermissionInitializerService permissionInitializerService;
    @Autowired
    private PermissionAvailabilityService permissionAvailabilityService;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        List<PermissionScheme> schemes = permissionService.getPermissionSchemes();
        model.addAttribute("schemes", schemes);
        return "permissionSchemes/index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}/edit")
    public String show(@PathVariable("id") PermissionScheme permissionScheme, Model model) {
        model.addAttribute("permissionScheme", permissionScheme);

        List<Permission> groupPermissions = permissionService.getGroupPermissions();
        List<PermissionSchemeEntryHolder> groupPermissionHolders = getPermissionHolders(groupPermissions, permissionScheme);
        model.addAttribute("groupPermissionEntryHolders", groupPermissionHolders);

        List<Permission> ticketPermissions = permissionService.getFieldPermissionsByModelName("ticket");
        List<PermissionSchemeEntryHolder> ticketPermissionHolders = getPermissionHolders(ticketPermissions, permissionScheme);
        model.addAttribute("ticketPermissionEntryHolders", ticketPermissionHolders);

        return "permissionSchemes/edit";
    }

    @RequestMapping(method = RequestMethod.GET, value="/new")
    public String newForm(Model model) {
        model.addAttribute(new PermissionScheme());
        return "permissionSchemes/form";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String create(@Valid @ModelAttribute PermissionScheme permissionScheme, BindingResult result) {
        if (!result.hasErrors()) {
            permissionInitializerService.loadDefaultPermissionScheme(permissionScheme);
            permissionService.savePermissionScheme(permissionScheme);
        }

        return "permissionSchemes/form";
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{permissionScheme}")
    public String update(@Valid @ModelAttribute("permissionScheme") PermissionScheme permissionScheme, BindingResult result,  Model model, HttpServletRequest request) {
        if (!result.hasErrors()) {
            permissionService.savePermissionScheme(permissionScheme);
            request.setAttribute("flash.success", getMessageSourceAccessor().getMessage("acl.permissionModel.save.success"));
            return "redirect:/config/acl/permissionSchemes/" + permissionScheme.getId() + "/edit";
        }

        List<Permission> groupPermissions = permissionService.getGroupPermissions();
        List<PermissionSchemeEntryHolder> groupPermissionHolders = getPermissionHolders(groupPermissions, permissionScheme);
        model.addAttribute("permissionSchemeEntryHolders", groupPermissionHolders);

        return "permissionSchemes/edit";
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{schemeId}")
    public void delete(@PathVariable("schemeId") PermissionScheme scheme, HttpServletResponse response) {
        permissionService.deletePermissionScheme(scheme);
        response.setStatus(HttpServletResponse.SC_OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{schemeId}/permissions/{permId}/users/edit")
    public String editUsers(@PathVariable("schemeId") PermissionScheme scheme, @PathVariable("permId") Permission permission, Model model) {
        PermissionActorsForm form = new PermissionActorsForm();
        List<User> userList = new ArrayList<User>(scheme.getPermissionSchemeEntry(permission).getPermissionActors().getUsers());
        form.setUsers(userList);
        model.addAttribute("permissionActorsForm", form);
        List<User> users = permissionAvailabilityService.getAvailableUsersForPermission(permission);
        model.addAttribute("users", users);
        model.addAttribute("permissionScheme", scheme);
        model.addAttribute("permission", permission);
        return "permissionSchemes/userForm";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{schemeId}/permissions/{permId}/users")
    public String updateUsers(PermissionActorsForm form, BindingResult result, @PathVariable("schemeId") PermissionScheme scheme, @PathVariable("permId") Permission permission) {
        if (!result.hasErrors()) {
            if (form.getUsers() == null) form.setUsers(new ArrayList<User>(0));
            PermissionSchemeEntry entry = scheme.getPermissionSchemeEntry(permission);
            Set<User> userSet = new HashSet<User>(form.getUsers());
            entry.getPermissionActors().setUsers(userSet);
            permissionService.savePermissionSchemeEntry(entry);
        }
        return "permissionSchemes/userForm";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{schemeId}/permissions/{permId}/roles/edit")
    public String editRoles(@PathVariable("schemeId") PermissionScheme scheme, @PathVariable("permId") Permission permission, Model model) {
        PermissionActorsForm form = new PermissionActorsForm();
        List<Role> roleList = new ArrayList<Role>(scheme.getPermissionSchemeEntry(permission).getPermissionActors().getRoles());
        form.setRoles(roleList);
        model.addAttribute("permissionActorsForm", form);
        List<Role> roles = permissionAvailabilityService.getAvailableRolesForPermission(permission);
        model.addAttribute("roles", roles);
        model.addAttribute("permission", permission);
        model.addAttribute("permissionScheme", scheme);
        return "permissionSchemes/roleForm";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{schemeId}/permissions/{permId}/roles")
    public String updateRoles(PermissionActorsForm form, BindingResult result, @PathVariable("schemeId") PermissionScheme scheme, @PathVariable("permId") Permission permission) {
        if (!result.hasErrors()) {
            if (form.getRoles() == null) form.setRoles(new ArrayList<Role>(0));
            PermissionSchemeEntry entry = scheme.getPermissionSchemeEntry(permission);
            Set<Role> roleSet = new HashSet<Role>(form.getRoles());
            entry.getPermissionActors().setRoles(roleSet);
            permissionService.savePermissionSchemeEntry(entry);
        }
        return "permissionSchemes/roleForm";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{schemeId}/entries.json")
    public void getPermissionSchemeEntries(@PathVariable("schemeId") PermissionScheme permissionScheme, HttpServletResponse response) throws JSONException, IOException {
        List<PermissionSchemeEntry> permissionSchemeEntries = new ArrayList<PermissionSchemeEntry>(permissionScheme.getPermissionSchemeEntries());

        MultiComparator mc = new MultiComparator();
        mc.addComparator(new Comparator<PermissionSchemeEntry>() {
            public int compare(PermissionSchemeEntry me, PermissionSchemeEntry other) {
                return me.getPermission().getPermissionType().compareTo(other.getPermission().getPermissionType());
            }
        });
        mc.addComparator(new Comparator<PermissionSchemeEntry>() {
            public int compare(PermissionSchemeEntry me, PermissionSchemeEntry other) {
                return me.getPermission().getId().compareTo(other.getPermission().getId());
            }
        });

        Collections.sort(permissionSchemeEntries, mc);

        List<JSONObject> jsonObjects = new ArrayList<JSONObject>();

        for(Role role : roleService.getRoles()) {
            JSONObject roleItem = new JSONObject();
            roleItem.put("id", "role" + role.getId());
            roleItem.put("name", role.getName());
            roleItem.put("type", "role");
            jsonObjects.add(roleItem);
        }

        // keep track of user objects we've already added to jsonObjects so we don't get collisions
        // do this with domain User objects since JSONObject doesn't have a full equals method.
        List<User> userObjectsAdded = new ArrayList<User>();

        for (PermissionSchemeEntry pse : permissionSchemeEntries) {
            JSONObject pseItem = new JSONObject();
            pseItem.put("id", pse.getId());
            pseItem.put("name", getMessageSourceAccessor().getMessage(pse.getPermission().getKey() + ".name", "Translation missing for " + pse.getPermission().getKey()));
            String namePrefix = "";
            if(pse.getPermission().getPermissionType().equals(Permission.PERMISSIONTYPE_SCHEME)) {
                namePrefix = "group";
            } else {
                namePrefix = "field";
            }
            pseItem.put("type", namePrefix + "PermissionSchemeEntry");

            List<JSONObject> pseChildren = new ArrayList<JSONObject>();
            JSONObject pseChildUserRef = new JSONObject();
            pseChildUserRef.put("_reference", "users" + pse.getId());
            pseChildren.add(pseChildUserRef);

            JSONObject pseChildRoleRef = new JSONObject();
            pseChildRoleRef.put("_reference", "roles" + pse.getId());
            pseChildren.add(pseChildRoleRef);

            JSONObject usersItem = new JSONObject();
            usersItem.put("id", "users" + pse.getId());
            usersItem.put("name", "Users");
            usersItem.put("type", "userContainer");
            usersItem.put("permId", pse.getPermission().getId());
            jsonObjects.add(usersItem);

            List<JSONObject> usersChildren = new ArrayList<JSONObject>();

            for(User user : pse.getPermissionActors().getUsers()) {
                JSONObject userItem = new JSONObject();
                userItem.put("id", user.getLoginId());
                userItem.put("name", user.getDisplayName());
                userItem.put("type", "user");
                if(!userObjectsAdded.contains(user)) {
                    jsonObjects.add(userItem);
                    userObjectsAdded.add(user);
                }

                JSONObject usersChildUserRef = new JSONObject();
                usersChildUserRef.put("_reference", user.getLoginId());
                usersChildren.add(usersChildUserRef);
            }
            usersItem.put("children", usersChildren);

            JSONObject rolesItem = new JSONObject();
            rolesItem.put("id", "roles" + pse.getId());
            rolesItem.put("name", "Roles");
            rolesItem.put("type", "roleContainer");
            rolesItem.put("permId", pse.getPermission().getId());
            jsonObjects.add(rolesItem);

            List<JSONObject> rolesChildren = new ArrayList<JSONObject>();
            for(Role role : pse.getPermissionActors().getRoles()) {
                JSONObject rolesChildRoleRef = new JSONObject();
                rolesChildRoleRef.put("_reference", "role" + role.getId());
                rolesChildren.add(rolesChildRoleRef);
            }


            rolesItem.put("children", rolesChildren);

            pseItem.put("children", pseChildren);
            jsonObjects.add(pseItem);
        }

        JSONObject store = new JSONObject();
        store.put("identifier", "id");
        store.put("label", "name");
        store.put("items", jsonObjects);

        response.setContentType("application/json");
        response.getWriter().print(store.toString());
    }

    private List<PermissionSchemeEntryHolder> getPermissionHolders(List<Permission> permissions, PermissionScheme permissionScheme) {
        List<PermissionSchemeEntryHolder> holders = new ArrayList<PermissionSchemeEntryHolder>();
        for (Permission groupPermission : permissions) {
            PermissionSchemeEntryHolder holder = new PermissionSchemeEntryHolder();
            holder.setPermission(groupPermission);
            holder.setEntry(getPermissionSchemeEntry(permissionScheme, groupPermission));
            holders.add(holder);
        }
        return holders;
    }

    private PermissionSchemeEntry getPermissionSchemeEntry(PermissionScheme scheme, Permission permission) {
        PermissionSchemeEntry entry = permissionService.getPermissionSchemeEntry(scheme, permission);
        if (entry == null) {
            entry = new PermissionSchemeEntry();
            entry.setPermission(permission);
            entry.setPermissionScheme(scheme);
            entry.setPermissionActors(new PermissionActors());
        }
        return entry;
    }
}
