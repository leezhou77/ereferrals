package net.grouplink.ehelpdesk.web.controllers.asset;

import net.grouplink.ehelpdesk.domain.asset.AssetCustomField;
import net.grouplink.ehelpdesk.domain.asset.AssetCustomFieldOption;
import net.grouplink.ehelpdesk.domain.asset.AssetFieldGroup;
import net.grouplink.ehelpdesk.service.asset.AssetCustomFieldService;
import net.grouplink.ehelpdesk.service.asset.AssetFieldGroupService;
import org.apache.commons.lang.StringUtils;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * User: jaymehafen
 * Date: Aug 7, 2008
 * Time: 4:27:41 PM
 */
public class AssetCustomFieldFormController extends SimpleFormController {

    private AssetCustomFieldService assetCustomFieldService;
    private AssetFieldGroupService assetFieldGroupService;

    public AssetCustomFieldFormController() {
        setCommandClass(AssetCustomField.class);
        setCommandName("customField");
        setFormView("assetCustomFieldEdit");
    }

    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
        super.initBinder(request, binder);
    }

    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        AssetCustomField assetCustomField;
        String customFieldId = ServletRequestUtils.getStringParameter(request, "customFieldId");
        if (StringUtils.isNotBlank(customFieldId)) {
            assetCustomField = assetCustomFieldService.getById(new Integer(customFieldId));
        } else {
            assetCustomField = new AssetCustomField();
            String fieldGroupId = ServletRequestUtils.getStringParameter(request, "fieldGroupId");
            AssetFieldGroup afg = assetFieldGroupService.getById(new Integer(fieldGroupId));
            assetCustomField.setFieldGroup(afg);
            assetCustomField.setRequired(Boolean.FALSE);
            assetCustomField.setFieldOrder(0);
        }

        return assetCustomField;
    }


    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
        @SuppressWarnings("unchecked")
        Map<String, Object> model = new ModelMap();
        Map<String, String> customFieldTypes = new LinkedHashMap<String, String>();
        customFieldTypes.put(AssetCustomField.VALUETYPE_CHECKBOX, getMessageSourceAccessor().getMessage("customField.checkbox"));
        customFieldTypes.put(AssetCustomField.VALUETYPE_DATE, getMessageSourceAccessor().getMessage("customField.date"));
        customFieldTypes.put(AssetCustomField.VALUETYPE_RADIO, getMessageSourceAccessor().getMessage("customField.radio"));
        customFieldTypes.put(AssetCustomField.VALUETYPE_SELECT, getMessageSourceAccessor().getMessage("customField.dropDown"));
        customFieldTypes.put(AssetCustomField.VALUETYPE_TEXT, getMessageSourceAccessor().getMessage("customField.textField"));
        customFieldTypes.put(AssetCustomField.VALUETYPE_TEXTAREA, getMessageSourceAccessor().getMessage("customField.textArea"));

        model.put("customFieldTypes", customFieldTypes);
        String fieldGroupId = ServletRequestUtils.getStringParameter(request, "fieldGroupId");
        if (StringUtils.isNotBlank(fieldGroupId))
            model.put("fieldGroupId", fieldGroupId);
        return model;
    }

    /**
     * This implementation calls
     * {@link #showForm(javax.servlet.http.HttpServletRequest , javax.servlet.http.HttpServletResponse , org.springframework.validation.BindException)}
     * in case of errors, and delegates to the full
     * {@link #onSubmit(javax.servlet.http.HttpServletRequest , javax.servlet.http.HttpServletResponse , Object, org.springframework.validation.BindException)}'s
     * variant else.
     * <p>This can only be overridden to check for an action that should be executed
     * without respect to binding errors, like a cancel action. To just handle successful
     * submissions without binding errors, override one of the <code>onSubmit</code>
     * methods or {@link #doSubmitAction}.
     *
     * @see #showForm(javax.servlet.http.HttpServletRequest , javax.servlet.http.HttpServletResponse , org.springframework.validation.BindException)
     * @see #onSubmit(javax.servlet.http.HttpServletRequest , javax.servlet.http.HttpServletResponse , Object, org.springframework.validation.BindException)
     * @see #onSubmit(Object, org.springframework.validation.BindException)
     * @see #onSubmit(Object)
     * @see #doSubmitAction(Object)
     */
    @SuppressWarnings("unchecked")
    protected ModelAndView processFormSubmission(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        AssetCustomField acf = (AssetCustomField) command;

        // Create a CustomFieldOption list to add to the Custom field if any
        String[] newcfopts = ServletRequestUtils.getStringParameters(request, "customFieldOptions.value");
        List<AssetCustomFieldOption> optList = new ArrayList<AssetCustomFieldOption>();
        for (String newcfopt : newcfopts) {
            AssetCustomFieldOption assCFO = new AssetCustomFieldOption();
            assCFO.setCustomField(acf);
            assCFO.setValue(newcfopt);
            optList.add(assCFO);
        }

        // clear out any existing customfieldoptions and add the incoming ones
        acf.getCustomFieldOptions().clear();
        acf.getCustomFieldOptions().addAll(optList);

        if (errors.hasErrors()) {
            return showForm(request, response, errors);
        } else if (isFormChangeRequest(request, command)) {
            onFormChange(request, response, command, errors);
            return showForm(request, response, errors);
        } else {
            return onSubmit(request, response, command, errors);
        }
    }

    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        AssetCustomField acf = (AssetCustomField) command;
        assetCustomFieldService.saveAssetCustomField(acf);
        return new ModelAndView(new RedirectView("customFields.glml?fieldGroupId=" + acf.getFieldGroup().getId()));
    }

    public void setAssetCustomFieldService(AssetCustomFieldService assetCustomFieldService) {
        this.assetCustomFieldService = assetCustomFieldService;
    }

    public void setAssetFieldGroupService(AssetFieldGroupService assetFieldGroupService) {
        this.assetFieldGroupService = assetFieldGroupService;
    }
}
