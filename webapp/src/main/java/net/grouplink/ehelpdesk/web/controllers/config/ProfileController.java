package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.common.utils.GLTest;
import net.grouplink.ehelpdesk.domain.Address;
import net.grouplink.ehelpdesk.domain.LdapField;
import net.grouplink.ehelpdesk.domain.LdapUser;
import net.grouplink.ehelpdesk.domain.Phone;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.service.AddressService;
import net.grouplink.ehelpdesk.service.LdapFieldService;
import net.grouplink.ehelpdesk.service.LdapService;
import net.grouplink.ehelpdesk.service.LocationService;
import net.grouplink.ehelpdesk.service.PhoneService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.web.domain.ChangePassword;
import net.grouplink.ehelpdesk.web.utils.ApplicationConstants;
import net.grouplink.ehelpdesk.web.validators.AddressValidator;
import net.grouplink.ehelpdesk.web.validators.PasswordValidator;
import net.grouplink.ehelpdesk.web.validators.PhoneValidator;
import net.grouplink.ehelpdesk.web.validators.UserRegistrationValidator;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: 3/9/12
 * Time: 4:58 PM
 */
@Controller
@RequestMapping(value = "/profile")
public class ProfileController extends ApplicationObjectSupport {
    @Autowired
    private UserService userService;
    @Autowired
    private LocationService locationService;
    @Autowired
    private LdapService ldapService;
    @Autowired
    private LdapFieldService ldapFieldService;
    @Autowired
    private UserRegistrationValidator userRegistrationValidator;
    @Autowired
    private PhoneValidator phoneValidator;
    @Autowired
    private PhoneService phoneService;
    @Autowired
    private AddressValidator addressValidator;
    @Autowired
    private AddressService addressService;
    @Autowired
    private PasswordValidator passwordValidator;

    @RequestMapping(method = RequestMethod.GET, value = "/{id}/edit")
    public String edit(Model model, @PathVariable("id") User user, HttpServletRequest request) {
        if(!userService.getLoggedInUser().equals(user)){
            throw new AccessDeniedException("R U Denial?");
        }
        model.addAttribute(user);
        model.addAttribute("locations", locationService.getLocations());
        model.addAttribute("ldapFieldList", getLdapFieldList(request));
        model.addAttribute("ldapUser", getLdapUserForUser(request, user));
        return "profile/form";
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{user}")
    public String udpate(Model model, @ModelAttribute("user") User user, BindingResult bindingResult, HttpServletRequest request) {
        if(!userService.getLoggedInUser().equals(user)){
            throw new AccessDeniedException("R U Denial?");
        }
        userRegistrationValidator.validate(user, bindingResult);
        if (!bindingResult.hasErrors()) {
            userService.saveUser(user);
            request.setAttribute("flash.success", getMessageSourceAccessor().getMessage("user.saved", "User saved successfully"));
            return "redirect:/config/profile/" + user.getId() + "/edit";
        }
        model.addAttribute("locations", locationService.getLocations());
        return "profile/form";
    }


    @RequestMapping(method = RequestMethod.GET, value = "/{id}/changePass")
    public String changePassword(Model model, @PathVariable("id") User user) {
        if(!userService.getLoggedInUser().equals(user)){
            throw new AccessDeniedException("R U Denial?");
        }
        model.addAttribute("changePassword", new ChangePassword());
        model.addAttribute(user);
        return "profile/changePassword";
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{user}/changePass")
    public String savePassword(Model model, @ModelAttribute("changePassword") ChangePassword changePassword,
                               BindingResult bindingResult, @PathVariable("user") User user) {
        if(!userService.getLoggedInUser().equals(user)){
            throw new AccessDeniedException("R U Denial?");
        }
        passwordValidator.validate(changePassword, bindingResult);
        if (!bindingResult.hasErrors()) {
            user.setPassword(GLTest.getMd5Password(changePassword.getPassword1()));
            userService.saveUser(user);
        }
        model.addAttribute(user);
        return "profile/changePassword";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{userId}/phones/new")
    public String newPhone(Model model, @PathVariable("userId") User user) {
        if(!userService.getLoggedInUser().equals(user)){
            throw new AccessDeniedException("R U Denial?");
        }
        model.addAttribute("phone", new Phone());
        model.addAttribute("user", user);
        return "profile/phone";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{userId}/phones")
    public String createPhone(@ModelAttribute Phone phone, BindingResult bindingResult,
                              @PathVariable("userId") User user) {
        if(!userService.getLoggedInUser().equals(user)){
            throw new AccessDeniedException("R U Denial?");
        }
        user.addPhone(phone);

        phoneValidator.validate(phone, bindingResult);
        if (!bindingResult.hasErrors()) {
            userService.saveUser(user);
        }
        return "profile/phone";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{userId}/phones/{phoneId}/edit")
    public String editPhone(Model model, @PathVariable("userId") User user, @PathVariable("phoneId") Phone phone) {
        if(!userService.getLoggedInUser().equals(user)){
            throw new AccessDeniedException("R U Denial?");
        }
        model.addAttribute("phone", phone);
        model.addAttribute("user", user);
        return "profile/phone";
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{userId}/phones/{phone}")
    public String updatePhone(@ModelAttribute("phone") Phone phone, BindingResult bindingResult, @PathVariable("userId") User user) {
        if(!userService.getLoggedInUser().equals(user)){
            throw new AccessDeniedException("R U Denial?");
        }
        phoneValidator.validate(phone, bindingResult);
        if (!bindingResult.hasErrors()) {
            phoneService.savePhone(phone);
        }
        return "profile/phone";
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{userId}/phones/{phoneId}")
    public void deletePhone(@PathVariable("phoneId") Phone phone, HttpServletResponse response, @PathVariable("userId") User user) {
        if(!userService.getLoggedInUser().equals(user)){
            throw new AccessDeniedException("R U Denial?");
        }
        boolean deleted = phoneService.deletePhone(phone);
        if (deleted) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{userId}/addresses/new")
    public String newAddress(Model model, @PathVariable("userId") User user) {
        if(!userService.getLoggedInUser().equals(user)){
            throw new AccessDeniedException("R U Denial?");
        }
        Address address = new Address();
        address.setCountry("US");
        model.addAttribute("address", address);
        model.addAttribute("user", user);
        return "profile/address";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{userId}/addresses")
    public String createAddress(@ModelAttribute Address address, BindingResult bindingResult,
                                @PathVariable("userId") User user) {
        if(!userService.getLoggedInUser().equals(user)){
            throw new AccessDeniedException("R U Denial?");
        }
        user.addAddress(address);

        addressValidator.validate(address, bindingResult);
        if (!bindingResult.hasErrors()) {
            userService.saveUser(user);
        }
        return "profile/address";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{userId}/addresses/{addressId}/edit")
    public String editAddress(Model model, @PathVariable("userId") User user, @PathVariable("addressId") Address address) {
        if(!userService.getLoggedInUser().equals(user)){
            throw new AccessDeniedException("R U Denial?");
        }
        model.addAttribute("address", address);
        model.addAttribute("user", user);
        return "profile/address";
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{userId}/addresses/{address}")
    public String updateAddress(@ModelAttribute("address") Address address, BindingResult bindingResult, @PathVariable("userId") User user) {
        if(!userService.getLoggedInUser().equals(user)){
            throw new AccessDeniedException("R U Denial?");
        }
        addressValidator.validate(address, bindingResult);
        if (!bindingResult.hasErrors()) {
            addressService.saveAddress(address);
        }
        return "profile/address";
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{userId}/addresses/{addressId}")
    public void deleteAddress(@PathVariable("addressId") Address address, HttpServletResponse response, @PathVariable("userId") User user) {
        if(!userService.getLoggedInUser().equals(user)){
            throw new AccessDeniedException("R U Denial?");
        }
        boolean deleted = addressService.deleteAddress(address);
        if (deleted) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
        }
    }


    private List getLdapFieldList(HttpServletRequest request) {
        List<LdapField> ldapFieldList = new ArrayList<LdapField>();

        ServletContext application = request.getSession(true).getServletContext();
        Boolean ldapIntegration = (Boolean) application.getAttribute(ApplicationConstants.LDAP_INTEGRATION);
        if (ldapIntegration != null && ldapIntegration) {
            ldapFieldList = this.ldapFieldService.getLdapFields();
        }

        return ldapFieldList;
    }

    private LdapUser getLdapUserForUser(HttpServletRequest request, User user) {
        LdapUser ldapUser = new LdapUser();

        ServletContext application = request.getSession(true).getServletContext();
        Boolean ldapIntegration = (Boolean) application.getAttribute(ApplicationConstants.LDAP_INTEGRATION);

        if (ldapIntegration != null && ldapIntegration && user != null) {
            if (StringUtils.isNotBlank(user.getLdapDN())) {
                ldapUser = this.ldapService.getLdapUser(user.getLdapDN());
            }
        }
        return ldapUser;
    }
}
