package net.grouplink.ehelpdesk.web.validators;

import net.grouplink.ehelpdesk.common.collaboration.FreeBusySearch;
import org.springframework.validation.Errors;

public class FreeBusySearchValidator extends BaseValidator {

	public boolean supports(Class clazz) {
		return FreeBusySearch.class.equals(clazz);
	}

	public void validate(Object obj, Errors errors) {
		FreeBusySearch freeBusySearch = (FreeBusySearch)obj;

        if(!isValidEmailString(freeBusySearch.getRecipients())){
            errors.rejectValue("recipients", "mail.validation.address", "Invalid email address listed.");
        }
	}
}
