package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.domain.Report;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.dashboard.Widget;
import net.grouplink.ehelpdesk.service.DashboardService;
import net.grouplink.ehelpdesk.service.ReportService;
import net.grouplink.ehelpdesk.service.WidgetService;
import net.grouplink.ehelpdesk.web.propertyeditors.ReportEditor;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WidgetFormController extends SimpleFormController {

    private WidgetService widgetService;
    private DashboardService dashboardService;
    private ReportService reportService;

    public WidgetFormController() {
        setCommandClass(Widget.class);
        setCommandName("widget");
        setFormView("dashboards/widgetEdit");
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        Integer widgetId = ServletRequestUtils.getIntParameter(request, "widgetId");
        Integer dashId = ServletRequestUtils.getIntParameter(request, "dashId");

        Widget widget;
        if (widgetId == null){
            widget = new Widget();
            widget.setDashboard(dashboardService.getById(dashId));
        } else {
            widget = widgetService.getById(widgetId);
        }
        return widget;
    }

    @Override
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
        Map<String, Object> refData = new HashMap<String, Object>();

        User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);

        List<Report> myReports = reportService.getByUser(user);
        List<Report> pubReports = reportService.getPublicMinusUser(user);

        // Create a map of report ids and whether the report has group and regroup
        // denoted by 0=not a report filter 1=single group 2=group and regroup
        Map<Integer, Integer> reportGroupBys = new HashMap<Integer, Integer>();
        for (Report report : myReports) {
            Integer grouping = 1;
            if(report.getReGroup()) grouping = 2;

            reportGroupBys.put(report.getId(), grouping);
        }

        for (Report report : pubReports) {
            Integer grouping = 1;
            if (report.getReGroup()) grouping = 2;

            reportGroupBys.put(report.getId(), grouping);
        }

        refData.put("myReports", myReports);
        refData.put("pubReports", pubReports);
        refData.put("reportGroupBys", reportGroupBys);

        return refData;
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {

        Widget widget = (Widget) command;
        widgetService.save(widget);

        request.setAttribute("flash.success", "Widget saved successfully");
        Integer dashId = widget.getDashboard().getId();
        return new ModelAndView(new RedirectView("/config/widgetEdit.glml?widgetId=" + widget.getId(), true));
    }



    @Override
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
        super.initBinder(request, binder);
        binder.registerCustomEditor(Report.class, new ReportEditor(reportService));
    }

    public void setWidgetService(WidgetService widgetService) {
        this.widgetService = widgetService;
    }

    public void setDashboardService(DashboardService dashboardService) {
        this.dashboardService = dashboardService;
    }

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }
}
