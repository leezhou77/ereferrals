package net.grouplink.ehelpdesk.web.formatters;

import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.service.GroupService;
import org.codehaus.plexus.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class GroupFormatter implements Formatter<Group> {
    @Autowired
    private GroupService groupService;

    public Group parse(String id, Locale locale) throws ParseException {
        if (StringUtils.isNotBlank(id)) {
            return groupService.getGroupById(Integer.parseInt(id));
        } else {
            return null;
        }
    }

    public String print(Group g, Locale locale) {
        return g == null ? "" : g.getId().toString();
    }
}
