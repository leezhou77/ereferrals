package net.grouplink.ehelpdesk.web.ip;

import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketPriority;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.MyTicketTab;
import net.grouplink.ehelpdesk.service.TicketService;
import net.grouplink.ehelpdesk.service.TicketFilterService;
import net.grouplink.ehelpdesk.service.MyTicketTabService;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.apache.commons.collections.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: Nov 12, 2009
 * Time: 12:41:21 PM
 */
public class IpActionController extends MultiActionController {

//    private UserRoleGroupService userRoleGroupService;
    private TicketService ticketService;
    private TicketFilterService ticketFilterService;
    private MyTicketTabService myTicketTabService;

    public ModelAndView displayHome(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        assert response != null;

        HttpSession session = request.getSession(true);
        User user = (User) session.getAttribute(SessionConstants.ATTR_USER);

//        List<UserRoleGroup> urgList = userRoleGroupService.getUserRoleGroupByUserId(user.getId());

        Map<String, Object> map = new HashMap<String, Object>();
        List<MyTicketTab> myTicketTabs = myTicketTabService.getByUser(user);
        if (CollectionUtils.isEmpty(myTicketTabs)) {
            myTicketTabs = myTicketTabService.initializeTabs(user);
        }
        map.put("myTicketTabs", myTicketTabs);
//        map.put("userRoleGroups", urgList);
        return new ModelAndView("home", map);
    }

    public ModelAndView displayTicketList(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        assert response != null;

        HttpSession session = request.getSession(true);
        User user = (User) session.getAttribute(SessionConstants.ATTR_USER);
//        Locale locale = RequestContextUtils.getLocale(request);

//        Integer urgId = ServletRequestUtils.getIntParameter(request, "urgid", -1);
//        Boolean isOwnedByMe = ServletRequestUtils.getBooleanParameter(request, "obm", false);
//        String grouping = ServletRequestUtils.getStringParameter(request, "grouping", "assignedToMe");

        String mttabIdStr = ServletRequestUtils.getStringParameter(request, "mttabId");
        Integer mttabId = StringUtils.isBlank(mttabIdStr) ? null : Integer.valueOf(mttabIdStr);
        String sort = ServletRequestUtils.getStringParameter(request, "sort", "priority");

        MyTicketTab myTicketTab = myTicketTabService.getById(mttabId);
        if(myTicketTab == null) return new ModelAndView(new RedirectView("/ip/home.glml", true));

        List<Ticket> ticketList;
        ticketList = ticketFilterService.getTickets(myTicketTab.getTicketFilter(), user);

//        UserRoleGroup urg = null;

//        if (isOwnedByMe) {
//            ticketList = ticketService.getTicketsByOwner(user, false);
//        } else {
//            urg = userRoleGroupService.getUserRoleGroupById(urgId);
//            ticketList = ticketService.getFilteredTickets(user, urg, locale, grouping, false);
//        }

//        String groupName = getMessageSourceAccessor().getMessage("ticketList.ownedByMe", locale);
//        Boolean showAllGroupTickets = false;

//        if (!isOwnedByMe) {
//            groupName = urg.getGroup().getName();
//            showAllGroupTickets = urg.getGroup().isShowAllGroupTickets();
//        }

        // Sort the list of tickets
        Collections.sort(ticketList, getComparator(sort));

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("tickets", ticketList);
        map.put("tabName", myTicketTab.getName());
        map.put("mttabId", myTicketTab.getId());
//        map.put("urgId", urgId);
//        map.put("groupName", groupName);
//        map.put("obm", isOwnedByMe);
//        map.put("grouping", grouping);
        map.put("sort", sort);
//        map.put("showAllGroupTickets", showAllGroupTickets);

        return new ModelAndView("ticketList", map);
    }

    public ModelAndView displayTicketHistoryList(HttpServletRequest request, HttpServletResponse response)
        throws Exception {
        assert response != null;

        Integer tid = ServletRequestUtils.getIntParameter(request, "tid");
        String mttabIdStr = ServletRequestUtils.getStringParameter(request, "mttabId");

        Integer mttabId = StringUtils.isBlank(mttabIdStr) ? null : Integer.valueOf(mttabIdStr);
//        Integer urgId = ServletRequestUtils.getIntParameter(request, "urgId");

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("ticket", ticketService.getTicketById(tid));
        model.put("mttabId", mttabId);
//        model.put("urgId", urgId);

        return new ModelAndView("ticketHistoryList", model);
    }

    private Comparator getComparator(String sort) {
        if("priority".equalsIgnoreCase(sort)){
            return new PriorityComparator();
        } else if("id".equalsIgnoreCase(sort)){
            return new IdComparator();
        } else if("contact".equalsIgnoreCase(sort)){
            return new ContactComparator();
        } else if("created".equalsIgnoreCase(sort)){
            return new CreatedComparator();
        } else if("modified".equalsIgnoreCase(sort)){
            return new ModifiedComparator();
        } else if("location".equalsIgnoreCase(sort)){
            return new LocationComparator();
        } else {
            return new PriorityComparator();
        }
    }

    public void setTicketFilterService(TicketFilterService ticketFilterService) {
        this.ticketFilterService = ticketFilterService;
    }

    public void setMyTicketTabService(MyTicketTabService myTicketTabService) {
        this.myTicketTabService = myTicketTabService;
    }

    class PriorityComparator implements Comparator{
        public int compare(Object o1, Object o2) {
            TicketPriority tp1 = ((Ticket)o1).getPriority();
            TicketPriority tp2 = ((Ticket)o2).getPriority();
            return tp1.compareTo(tp2);
        }
    }

    class IdComparator implements Comparator{
        public int compare(Object o1, Object o2) {
            Integer tp1 = ((Ticket)o1).getId();
            Integer tp2 = ((Ticket)o2).getId();
            return tp1.compareTo(tp2);
        }
    }

    private class ContactComparator implements Comparator {
        public int compare(Object o1, Object o2) {
            User tp1 = ((Ticket)o1).getContact();
            User tp2 = ((Ticket)o2).getContact();
            return tp1.compareTo(tp2);
        }
    }

    private class CreatedComparator implements Comparator {
        public int compare(Object o1, Object o2) {
            Date tp1 = ((Ticket)o1).getCreatedDate();
            Date tp2 = ((Ticket)o2).getCreatedDate();
            return tp1.compareTo(tp2);
        }
    }

    private class ModifiedComparator implements Comparator {
        public int compare(Object o1, Object o2) {
            Date tp1 = ((Ticket)o1).getModifiedDate();
            Date tp2 = ((Ticket)o2).getModifiedDate();
            return tp1.compareTo(tp2);
        }
    }

    private class LocationComparator implements Comparator {
        public int compare(Object o1, Object o2) {
            Location tp1 = ((Ticket)o1).getLocation();
            Location tp2 = ((Ticket)o2).getLocation();
            return tp1.compareTo(tp2);
        }
    }


//    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
//        this.userRoleGroupService = userRoleGroupService;
//    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

}
