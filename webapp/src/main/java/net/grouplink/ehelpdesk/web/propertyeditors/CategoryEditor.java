package net.grouplink.ehelpdesk.web.propertyeditors;

import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.service.CategoryService;
import org.apache.commons.lang.StringUtils;

import java.beans.PropertyEditorSupport;


public class CategoryEditor extends PropertyEditorSupport {

    private CategoryService categoryService;

    /**
     * Constructor for use by derived PropertyEditor classes.
     */
    public CategoryEditor() {
    }

    /**
     * Constructor for use by derived PropertyEditor classes.
     * @param categoryService the category service
     */
    public CategoryEditor(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    /**
     * Gets the property value as a string suitable for presentation
     * to a human to edit.
     *
     * @return The property value as a string suitable for presentation
     *         to a human to edit.
     *         <p>   Returns "null" is the value can't be expressed as a string.
     *         <p>   If a non-null value is returned, then the PropertyEditor should
     *         be prepared to parse that string back in setAsText().
     */
    public String getAsText() {
        Category category = (Category)getValue();
        return (category !=  null)?category.getId().toString():"";
    }

    /**
     * Sets the property value by parsing a given String.  May raise
     * java.lang.IllegalArgumentException if either the String is
     * badly formatted or if this kind of property can't be expressed
     * as text.
     *
     * @param text The string to be parsed.
     */
    public void setAsText(String text) throws IllegalArgumentException {
        if (StringUtils.isNotBlank(text)){
            setValue(categoryService.getCategoryById(new Integer(text)));
        } else {
            setValue(null);
        }
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

}