package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.common.utils.MailConfig;
import net.grouplink.ehelpdesk.common.utils.MailMessage;
import net.grouplink.ehelpdesk.domain.EmailToTicketConfig;
import net.grouplink.ehelpdesk.service.EmailToTicketConfigService;
import net.grouplink.ehelpdesk.service.MailService;
import net.grouplink.ehelpdesk.web.beans.ApplicationInitializer;
import net.grouplink.ehelpdesk.web.utils.ApplicationConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

/**
 * author: zpearce
 * Date: 9/22/11
 * Time: 11:24 AM
 */
@Controller
@RequestMapping(value = "/mail")
public class MailConfigController extends ApplicationObjectSupport {
    @Autowired private EmailToTicketConfigService emailToTicketConfigService;
    @Autowired private MailService mailService;
    @Autowired private ApplicationInitializer applicationInitializer;

    @ModelAttribute("e2ts")
    public List<EmailToTicketConfig> e2tConfigs() {
        return emailToTicketConfigService.getAll();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/mailConfig.glml")
    public String getConfigForm(Model model) {
        MailConfig mailConfig = mailService.getMailConfig();
        model.addAttribute(mailConfig);
        return "mailConfig";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/mailConfig.glml")
    public String updateConfig(@Valid MailConfig mailConfig, BindingResult bindingResult, HttpServletRequest request) {
        if(bindingResult.hasErrors()) {
            return "mailConfig";
        }
        mailService.saveMailConfig(mailConfig);
        applicationInitializer.mailInit();
        String msg = getMessageSourceAccessor().getMessage("mailconfig.update.success");
        request.setAttribute("flash.success", msg);
        return "redirect:/config/mail/mailConfig.glml";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/mailConfig.glml", params = "reset")
    public String resetConfig(HttpServletRequest request) {
        mailService.saveMailConfig(new MailConfig());
        applicationInitializer.mailInit();
        String msg = getMessageSourceAccessor().getMessage("mailconfig.reset.success");
        request.setAttribute("flash.success", msg);
        return "redirect:/config/mail/mailConfig.glml";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/mailConfig.glml", params = "sendTestEmail")
    public String sendTestEmail(@Valid MailConfig mailConfig, BindingResult bindingResult, HttpServletRequest request) {
        ValidationUtils.rejectIfEmpty(bindingResult, "email", "mailconfig.sendtest.toFieldEmpty");
        if(bindingResult.hasErrors()) {
            return "mailConfig";
        }

        try {
            sendTestEmail(mailConfig);
        } catch (Exception e) {
            String msg = getMessageSourceAccessor().getMessage("mailconfig.sendtest.error", new Object[]{e.getMessage()});
            request.setAttribute("flash.error", msg);
            logger.error("Error sending email to: " + mailConfig.getEmail());
            logger.error(e.getMessage());
        }
        String msg = getMessageSourceAccessor().getMessage("mailconfig.sendtest.success");
        request.setAttribute("flash.success", msg);
        return "redirect:/config/mail/mailConfig.glml";
    }

    private void sendTestEmail(MailConfig mailConfig) throws Exception {

        String displayName = mailConfig.getEmailDisplayName();
        String fromAddress = mailConfig.getEmailFromAddress();
        String testEmail = mailConfig.getEmail();

        MailMessage message = this.mailService.newMessage();
            message.setTo(testEmail.split(";"));
            message.setFrom(fromAddress, displayName);
            message.setSubject("eHelpDesk SMTP Configuration Test");
            message
                    .setText(
                            "Congratulations your SMTP configuration is complete.",
                            "<html><body>Congratulations your SMTP configuration is complete.</body></html>");
            mailService.sendMail(message);
            logger.info("Test email sent to: " + testEmail);

    }
}
