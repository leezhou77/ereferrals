package net.grouplink.ehelpdesk.web.controllers;

import net.grouplink.ehelpdesk.common.utils.GLTest;
import net.grouplink.ehelpdesk.common.utils.MailMessage;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.service.MailService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.web.domain.ResetPassword;
import net.grouplink.ehelpdesk.web.utils.ApplicationConstants;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public class ResetPasswordFormController extends
		AbstractEHelpDeskFormController {

	private UserService userService;
	private MailService mailService;
	
	public ResetPasswordFormController() {
		super();
		setCommandClass(ResetPassword.class);
		setCommandName("resetPassword");
		setFormView("resetPassword");
		setSuccessView("resetPassword");
	}
	
	protected ModelAndView onSubmit(HttpServletRequest request, 
			HttpServletResponse response, Object command, BindException errors) throws Exception {
		ResetPassword resetPassword = (ResetPassword)command;
		String email = resetPassword.getEmail();
		Map model = new HashMap();

        User user = userService.getUserByEmail(email);
        String newPassword = userService.resetUserPassword(user);

        ServletContext application = request.getSession().getServletContext();
        String emailFromAddress = (String) application.getAttribute(ApplicationConstants.MAIL_EMAIL_FROM_ADDRESS);
        String emailDisplayName = (String) application.getAttribute(ApplicationConstants.MAIL_EMAIL_DISPLAY_NAME);

        if(StringUtils.isBlank(emailFromAddress)){
            emailFromAddress = "eHelpDesk@" + request.getServerName() + ".com";
        }
        if(StringUtils.isBlank(emailDisplayName)){
            emailDisplayName = "eHelpDesk";
        }

        MailMessage message = mailService.newMessage();
        message.setTo(user.getEmail());
        message.setFrom(emailFromAddress, emailDisplayName);
        message.setSubject(getMessageSourceAccessor().getMessage("resetPassword.subject"));
        message.setText(getMessageSourceAccessor().getMessage("resetPassword.body", new Object[]{newPassword}));
			
        mailService.sendMail(message);
			
        model.put("messageSent", Boolean.TRUE);

        model.put("resetPassword", resetPassword);
		return new ModelAndView(getSuccessView(), model);
	}

	public void afterPropertiesSet() throws Exception {
		if (userService == null) {
			throw new Exception("User Service not set");
		}
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public void setMailService(MailService mailService) {
		this.mailService = mailService;
	}
}
