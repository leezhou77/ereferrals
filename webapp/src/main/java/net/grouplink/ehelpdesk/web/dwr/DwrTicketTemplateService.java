package net.grouplink.ehelpdesk.web.dwr;

import net.grouplink.ehelpdesk.domain.Attachment;
import net.grouplink.ehelpdesk.domain.TicketTemplate;
import net.grouplink.ehelpdesk.service.AttachmentService;
import net.grouplink.ehelpdesk.service.TicketTemplateService;

import java.util.Iterator;
import java.util.Set;

public class DwrTicketTemplateService {

    private TicketTemplateService ticketTemplateService;
    private AttachmentService attachmentService;

    public void deleteAttachment(Integer ticketTemplateId, Integer attachmentId) {
        TicketTemplate ticketTemplate = ticketTemplateService.getById(ticketTemplateId);
        Set<Attachment> attachments = ticketTemplate.getAttachments();
        for (Iterator<Attachment> iterator = attachments.iterator(); iterator.hasNext();) {
            Attachment attachment = iterator.next();
            if (attachment.getId().equals(attachmentId)) {
                iterator.remove();
                attachmentService.delete(attachment);
            }
        }

        ticketTemplateService.save(ticketTemplate);
    }

    public void setTicketTemplateService(TicketTemplateService ticketTemplateService) {
        this.ticketTemplateService = ticketTemplateService;
    }

    public void setAttachmentService(AttachmentService attachmentService) {
        this.attachmentService = attachmentService;
    }
}
