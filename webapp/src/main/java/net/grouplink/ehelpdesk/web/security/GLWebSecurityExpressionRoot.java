package net.grouplink.ehelpdesk.web.security;

import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.expression.WebSecurityExpressionRoot;

public class GLWebSecurityExpressionRoot extends WebSecurityExpressionRoot {
    private PermissionService permissionService;
    private UserService userService;

    public GLWebSecurityExpressionRoot(Authentication a, FilterInvocation fi) {
        super(a, fi);
    }

    public boolean hasGlobalPermission(String permission) {
        User user = userService.getLoggedInUser();
        return permissionService.hasGlobalPermission(user, permissionService.getPermissionByKey(permission));
    }

    public boolean hasGroupPermission(String permission, Group group) {
        return permissionService.hasGroupPermission(userService.getLoggedInUser(), permissionService.getPermissionByKey(permission), group);
    }

    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

}
