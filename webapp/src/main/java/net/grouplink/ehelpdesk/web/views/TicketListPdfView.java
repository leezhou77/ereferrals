package net.grouplink.ehelpdesk.web.views;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;
import net.grouplink.ehelpdesk.common.beans.support.GLRefreshablePagedListHolder;
import net.grouplink.ehelpdesk.domain.AdvancedTicketsFilter;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.CustomFieldValues;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.domain.Status;
import net.grouplink.ehelpdesk.domain.SurveyData;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketHistory;
import net.grouplink.ehelpdesk.domain.TicketPriority;
import net.grouplink.ehelpdesk.domain.TicketsFilter;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.domain.asset.ZenAsset;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilterOperators;
import net.grouplink.ehelpdesk.service.CategoryOptionService;
import net.grouplink.ehelpdesk.service.CategoryService;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LocationService;
import net.grouplink.ehelpdesk.service.StatusService;
import net.grouplink.ehelpdesk.service.TicketPriorityService;
import net.grouplink.ehelpdesk.service.TicketService;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.asset.ZenAssetService;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.support.SortDefinition;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.Color;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author mrollins
 * @version 1.0
 * @deprecated replaced by TicketFilterListPdfView
 */
public class TicketListPdfView extends AbstractPdfView {

    private static final Font TITLE_FONT_ITALIC = new Font(Font.HELVETICA, 18, Font.ITALIC, new Color(153, 153, 153));
    private static final Font TITLE_FONT_BOLD = new Font(Font.HELVETICA, 18, Font.BOLD, new Color(204, 51, 0));

    private static final Font HEADING_FONT = new Font(Font.HELVETICA, 12, Font.ITALIC, Color.black);
    private static final Font HEADING_DATA_FONT = new Font(Font.HELVETICA, 12, Font.ITALIC, Color.blue);
    private static final Font DATA_HEAD_FONT = new Font(Font.HELVETICA, 10, Font.ITALIC, Color.black);
    private static final Font TEXT_FONT = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL, Color.black);
    private static final Font BOLD_FONT = new Font(Font.TIMES_ROMAN, 10, Font.BOLD, Color.black);
    private static final int MARGIN = 32;

    private final Log log = LogFactory.getLog(TicketListPdfView.class);
    private final SimpleDateFormat filterDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private TicketPriorityService ticketPriorityService;
    private UserRoleGroupService userRoleGroupService;
    private CategoryService categoryService;
    private CategoryOptionService categoryOptionService;
    private UserService userService;
    private GroupService groupService;
    private LocationService locationService;
    private StatusService statusService;
    private ZenAssetService zenAssetService;
    private TicketService ticketService;

    protected void buildPdfMetadata(Map map, Document document, HttpServletRequest httpServletRequest) {
        document.addTitle(getMessageSourceAccessor().getMessage("ticketList.title", "Ticket List"));
        document.addCreator(getMessageSourceAccessor().getMessage("global.systemName", "everything HelpDesk\u2122"));
        document.addCreationDate();
    }

    /**
     * Subclasses must implement this method to build an iText PDF document,
     * given the model. Called between <code>Document.open()</code> and
     * <code>Document.close()</code> calls.
     * <p>Note that the passed-in HTTP response is just supposed to be used
     * for setting cookies or other HTTP headers. The built PDF document itself
     * will automatically get written to the response after this method returns.
     *
     * @param model    the model Map
     * @param document the iText Document to add elements to
     * @param writer   the PdfWriter to use
     * @param request  in case we need locale etc. Shouldn't look at attributes.
     * @param response in case we need to set cookies. Shouldn't write to it.
     * @throws Exception any exception that occured during document building
     * @see com.lowagie.text.Document#open()
     * @see com.lowagie.text.Document#close()
     */
    protected void buildPdfDocument(Map model, Document document, PdfWriter writer, HttpServletRequest request,
                                    HttpServletResponse response) throws Exception {
        // Get the ticket data.
        Map underModel = (Map) model.get("data");
        String groupName = (String) underModel.get("groupName");
        GLRefreshablePagedListHolder pgHolder = (GLRefreshablePagedListHolder) underModel.get("tickets");

        DateFormat df = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT, pgHolder.getLocale());
        SortDefinition sort = pgHolder.getSort();

        // Create and add the event handler.
        // This is to ensure that only entire cells are printed at end of pages
        MyPageEvents events = new MyPageEvents(getMessageSourceAccessor());
        writer.setPageEvent(events);
        events.onOpenDocument(writer, document);

        String everything = "everything ";
        String helpDesk = "HelpDesk\u2122";

        Paragraph title = new Paragraph();
        title.add(new Chunk(everything, TITLE_FONT_ITALIC));
        title.add(new Chunk(helpDesk, TITLE_FONT_BOLD));
        document.add(title);
        document.add(new Paragraph(" "));

        // Ticket Summary table
        buildSummaryTable(document, groupName, pgHolder, df, sort);

        // Filters Table
        buildFiltersTable(document, pgHolder, df);

        // Ticket table
        buildTicketsTable(document, request, pgHolder, df);
    }

    /**
     * Builds the table that contains the ticket data.
     * @param document the document
     * @param request the servlet request
     * @param pgHolder the list holder
     * @param df the date format
     * @throws DocumentException if an error occurs
     * @throws IOException if an error occurs
     */
    private void buildTicketsTable(Document document, HttpServletRequest request, GLRefreshablePagedListHolder pgHolder,
                                   DateFormat df) throws DocumentException, IOException {
        if (pgHolder.getFilter() != null && pgHolder.getFilter() instanceof TicketFilter) {
            buildTicketsTableDynamic(document, pgHolder, df);
        } else {
            buildTicketsTableStatic(document, request, pgHolder, df);
        }
    }

    private void buildTicketsTableDynamic(Document document, GLRefreshablePagedListHolder pgHolder, DateFormat df)
            throws DocumentException {
        String[] colHeads = getColumnHeadings(pgHolder);
        int indexOfColNote = indexOfNoteColumn(colHeads);
        int tableLength = indexOfColNote >= 0 ? colHeads.length -1 : colHeads.length;
        PdfPTable table = new PdfPTable(tableLength);
        table.setWidthPercentage(100);
        table.getDefaultCell().setBorderWidth(1);
        table.getDefaultCell().setBorderColor(Color.black);
        table.getDefaultCell().setGrayFill(0.75f);
        table.getDefaultCell().setPadding(3);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

        // header row
        for (int i = 0; i < tableLength; i++) {
            if(indexOfColNote < 0) {
                table.addCell(new Phrase(colHeads[i], DATA_HEAD_FONT));
            }
            else {
                if(i < indexOfColNote) {
                     table.addCell(new Phrase(colHeads[i], DATA_HEAD_FONT));
                }
                else {
                     table.addCell(new Phrase(colHeads[i+1], DATA_HEAD_FONT));
                }
            }
        }

        table.setHeaderRows(1);
        table.getDefaultCell().setBorderWidth(1);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        table.getDefaultCell().setVerticalAlignment(Element.ALIGN_TOP);

        TicketFilter tf = (TicketFilter) pgHolder.getFilter();
        String[] columns = tf.getColumnOrder();

        // Iterate on the tickets list
        PdfPCell cell;
        PdfPCell noteCell;
        boolean even = false;

        @SuppressWarnings("unchecked")
        List<Ticket> tickets = pgHolder.getSource();

        for (Ticket ticket : tickets) {
            table.getDefaultCell().setColspan(1);
            table.getDefaultCell().setPaddingLeft(3);
            table.getDefaultCell().setGrayFill(even ? 0.90f : 1.00f);
//            cell = null;

            boolean showNote = false;

            for (String column : columns) {
                cell = null;
                if (column.equals(TicketFilter.COLUMN_PRIORITY)) {
                    cell = new PdfPCell(new Phrase(ticket.getPriority().getName()));

                } else if (column.equals(TicketFilter.COLUMN_TICKETNUMBER)) {
                    cell = new PdfPCell(new Phrase(ticket.getTicketId().toString()));
                } else if (column.equals(TicketFilter.COLUMN_LOCATION)) {
                    cell = new PdfPCell(new Phrase(ticket.getLocation().getName()));
                } else if (column.equals(TicketFilter.COLUMN_SUBJECT)) {
                    cell = new PdfPCell(new Phrase(ticket.getSubject()));
                } else if (column.equals(TicketFilter.COLUMN_NOTE)) {
                    //cell = new PdfPCell(new Phrase(ticket.getNote()));
                    showNote = true;
                } else if (column.equals(TicketFilter.COLUMN_CATOPT)) {
                    cell = new PdfPCell(new Phrase(ticket.getCategoryOption() == null ? "" :
                            ticket.getCategoryOption().getName()));
                } else if (column.equals(TicketFilter.COLUMN_ASSIGNED)) {
                    UserRoleGroup assignedToUrg = ticket.getAssignedTo();
                    cell = new PdfPCell(new Phrase(assignedToUrg.getUserRole() == null ?
                            assignedToUrg.getGroup().getName() : assignedToUrg.getUserRole().getUser().getFirstName() +
                            " " + ticket.getAssignedTo().getUserRole().getUser().getLastName()));
                } else if (column.equals(TicketFilter.COLUMN_STATUS)) {
                    cell = new PdfPCell(new Phrase(ticket.getStatus().getName()));
                } else if (column.equals(TicketFilter.COLUMN_CREATED)) {
                    cell = new PdfPCell(new Phrase(df.format(ticket.getCreatedDate())));
                } else if (column.equals(TicketFilter.COLUMN_MODIFIED)) {
                    cell = new PdfPCell(new Phrase(df.format(ticket.getModifiedDate())));
                } else if (column.equals(TicketFilter.COLUMN_ESTIMATED)) {
                    cell = new PdfPCell(new Phrase(ticket.getEstimatedDate() == null ?
                            "" : df.format(ticket.getEstimatedDate())));
                } else if (column.equals(TicketFilter.COLUMN_CONTACT)) {
                    cell = new PdfPCell(new Phrase(ticket.getContact().getFirstName() + " " +
                            ticket.getContact().getLastName()));
                } else if (column.equals(TicketFilter.COLUMN_SUBMITTED)) {
                    cell = new PdfPCell(new Phrase(ticket.getSubmittedBy().getFirstName() + " " +
                            ticket.getSubmittedBy().getLastName()));
                } else if (column.equals(TicketFilter.COLUMN_GROUP)) {
                    cell = new PdfPCell(new Phrase(ticket.getGroup().getName()));
                } else if (column.equals(TicketFilter.COLUMN_CATEGORY)) {
                    cell = new PdfPCell(new Phrase(ticket.getCategory() == null ? "" : ticket.getCategory().getName()));
                } else if (column.equals(TicketFilter.COLUMN_WORKTIME)) {
                    cell = new PdfPCell(new Phrase(ticket.getWorkTime() == null ? "" : ticket.getWorkTime().toString()));
                } else if (column.endsWith(TicketFilter.COLUMN_ZENWORKS10ASSET)) {
                    cell = new PdfPCell(new Phrase(ticket.getZenAsset() == null ? "" : ticket.getZenAsset().getAssetName()));
                } else if (column.equals(TicketFilter.COLUMN_HISTORYSUBJECT)) {
                    TicketHistory ticketHistory = ticketService.getMostRecentTicketHistory(ticket);
                    if (ticketHistory == null) {
                        cell = new PdfPCell(new Phrase(""));
                    } else {
                        cell = new PdfPCell(new Phrase(ticketHistory.getSubject() == null ? "" : ticketHistory.getSubject()));
                    }
                } else if (column.equals(TicketFilter.COLUMN_HISTORYNOTE)) {
                    TicketHistory ticketHistory = ticketService.getMostRecentTicketHistory(ticket);
                    if (ticketHistory == null) {
                        cell = new PdfPCell(new Phrase(""));
                    } else {
                        cell = new PdfPCell(new Phrase(ticketHistory.getNote() == null ? "" : ticketHistory.getNote()));
                    }
                } else if (column.startsWith(TicketFilter.COLUMN_CUSTOMFIELDPREFIX)) {
                    // column is a custom field
                    // check the ticket for a custom field value name that matches the column name
                    CustomFieldValues cfVal = ticketService.getCustomFieldValue(ticket, StringUtils.removeStart(column, TicketFilter.COLUMN_CUSTOMFIELDPREFIX));
                    if (cfVal == null) {
                        cell = new PdfPCell(new Phrase(""));
                    } else {
                        cell = new PdfPCell(new Phrase(cfVal.getFieldValue() == null ? "" : cfVal.getFieldValue()));
                    }
                } else if (column.startsWith(TicketFilter.COLUMN_SURVEYPREFIX)) {
                    SurveyData surveyData = ticketService.getSurveyData(ticket, StringUtils.removeStart(column, TicketFilter.COLUMN_SURVEYPREFIX));
                    if (surveyData == null) {
                        cell = new PdfPCell(new Phrase(""));
                    } else {
                        cell = new PdfPCell(new Phrase(surveyData.getValue() == null ? "" : surveyData.getValue()));
                    }
                } else {
                    log.error("unknown column value: " + column);
                }

                if (cell != null) {
                    cell.setGrayFill(even ? 0.9f : 1.0f);
                    table.addCell(cell);
                }
            }
            if (showNote) {
                even = !even;
                String noteLabel = getMessageSourceAccessor().getMessage("ticket.note") + ":";
                String note = StringUtils.isBlank(ticket.getNote()) ? noteLabel : ticket.getNote();
                noteCell = new PdfPCell(new Phrase(note));
                noteCell.setColspan(columns.length - 1);
                noteCell.setHorizontalAlignment(Element.ALIGN_LEFT);
                noteCell.setGrayFill(even ? 0.9f : 1.0f);
                table.addCell(noteCell);
            }
            even = !even;
        }
        document.add(table);
    }

    private int indexOfNoteColumn(String[] colHeads) {
        int indexOfColNote = -999;
        for (int i = 0; i < colHeads.length; i++) {
            if(colHeads[i].equalsIgnoreCase("Note")) {
                indexOfColNote = i;
                break;
            }
        }
        return indexOfColNote;
    }

    private void buildTicketsTableStatic(Document document, HttpServletRequest request,
                                         GLRefreshablePagedListHolder pgHolder, DateFormat df)
            throws DocumentException, IOException {
        PdfPTable table;
        PdfPCell cell;
        table = new PdfPTable(9);
        int headerwidths[] = {5, 5, 5, 10, 20, 10, 10, 10, 12};
        table.setWidths(headerwidths);
        table.setWidthPercentage(100);
        table.getDefaultCell().setBorderWidth(1);
        table.getDefaultCell().setBorderColor(Color.black);
        table.getDefaultCell().setGrayFill(0.75f);
        table.getDefaultCell().setPadding(3);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

        String[] colHeads = getColumnHeadings(pgHolder);
        for (String colHead : colHeads) {
            table.addCell(new Phrase(colHead, DATA_HEAD_FONT));
        }

        // We set the above row as remaining title
        // and adjust properties for normal cells
        table.setHeaderRows(1);
        table.getDefaultCell().setBorderWidth(1);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        table.getDefaultCell().setVerticalAlignment(Element.ALIGN_TOP);

        // Iterate on the tickets list
        boolean even = false;
        @SuppressWarnings("unchecked")
        List<Ticket> tickets = pgHolder.getSource();
        for (Ticket ticket : tickets) {

            // reset colspan and padding left for second and successive iterations
            table.getDefaultCell().setColspan(1);
            table.getDefaultCell().setPaddingLeft(3);

            table.getDefaultCell().setGrayFill(even ? 0.90f : 1.00f);

//            if (even) {
//                table.getDefaultCell().setGrayFill(0.90f);
//                even = false;
//            } else {
//                table.getDefaultCell().setGrayFill(1.00f);
//                even = true;
//            }


            if (ticket.getPriority().getIcon().equals("text")) {
                cell = new PdfPCell(new Phrase(ticket.getPriority().getName()));
            } else {
                String url = request.getRequestURL().toString();
                url = url.substring(0, url.indexOf("/tickets"));
                cell = new PdfPCell(Image.getInstance(new URL(url + "/images/" + ticket.getPriority().getIcon() +
                        "ball_12.png")));
            }


            cell.setPadding(3);
            cell.setGrayFill(even ? 0.90f : 1.00f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            table.getDefaultCell().setImage(null);
            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(new Phrase(ticket.getTicketId().toString(), TEXT_FONT));

            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            String contactName = ticket.getContact().getFirstName() + " " + ticket.getContact().getLastName();
            table.addCell(new Phrase(contactName, TEXT_FONT));

            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(new Phrase(ticket.getLocation().getName(), BOLD_FONT));

            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(new Phrase(ticket.getSubject(), TEXT_FONT));

            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(new Phrase(ticket.getCategoryOption() == null ? "" :
                    ticket.getCategoryOption().getName(), BOLD_FONT));

            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            String assTo = ticket.getAssignedTo().getUserRole() == null ?
                    ticket.getAssignedTo().getGroup().getName() :
                    ticket.getAssignedTo().getUserRole().getUser().getFirstName() + " " +
                            ticket.getAssignedTo().getUserRole().getUser().getLastName();
            table.addCell(new Phrase(assTo, TEXT_FONT));

            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(new Phrase(ticket.getStatus().getName(), BOLD_FONT));

            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(new Phrase(df.format(ticket.getCreatedDate()), TEXT_FONT));

            if (StringUtils.isNotBlank(ticket.getNote())) {
                table.getDefaultCell().setPaddingLeft(15);
                table.getDefaultCell().setColspan(9);
                table.addCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.note", "Note") + ": " + ticket.getNote(), TEXT_FONT));
            }

            // switch even for odd/even row coloring
            even = !even;
        }
        document.add(table);
    }

    /**
     * Builds the table that describes which filters were applied.
     * @param document the document
     * @param pgHolder the list holder
     * @param df the date format instance
     * @throws DocumentException if an error occurs
     */
    private void buildFiltersTable(Document document, GLRefreshablePagedListHolder pgHolder, DateFormat df)
            throws DocumentException {
        if (pgHolder.getFilter() != null && pgHolder.getFilter() instanceof TicketFilter) {
            buildFilterTableDynamic(document, pgHolder, df);
        } else {
            buildFiltersTableStatic(document, pgHolder);
        }
    }

    private void buildFilterTableDynamic(Document document, GLRefreshablePagedListHolder pgHolder, DateFormat df)
            throws DocumentException {
        PdfPTable table = new PdfPTable(2);
        PdfPCell cell;

        table.setWidthPercentage(100);
        table.setWidths(new float[]{30f, 70f});
        table.getDefaultCell().setBorderWidth(1);
        table.getDefaultCell().setBorderColor(Color.black);
        table.getDefaultCell().setPadding(4);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);


        cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticketList.filters"), HEADING_FONT));
        cell.setColspan(2);
        cell.setGrayFill(0.75f);
        table.addCell(cell);
        table.setHeaderRows(1);
        
        TicketFilter tf = (TicketFilter) pgHolder.getFilter();

        // assignedToIds
        if (!ArrayUtils.isEmpty(tf.getAssignedToIds())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_ASSIGNED)));
            table.addCell(cell);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getAssignedToIds().length; i++) {
                Integer integer = tf.getAssignedToIds()[i];
                UserRoleGroup urg = userRoleGroupService.getUserRoleGroupById(integer);
                if (sb.length() > 0) sb.append(", ");
                if (urg.getUserRole() == null) {
                    sb.append(urg.getGroup().getName());
                } else {
                    sb.append(urg.getUserRole().getUser().getFirstName()).append(" ")
                            .append(urg.getUserRole().getUser().getLastName());
                }
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

        // categoryIds
        if (!ArrayUtils.isEmpty(tf.getCategoryIds())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_CATEGORY)));
            table.addCell(cell);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getCategoryIds().length; i++) {
                Integer integer = tf.getCategoryIds()[i];
                Category cat = categoryService.getCategoryById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(cat.getName());
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

        // categoryOptionIds
        if (!ArrayUtils.isEmpty(tf.getCategoryOptionIds())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_CATOPT)));
            table.addCell(cell);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getCategoryOptionIds().length; i++) {
                Integer integer = tf.getCategoryOptionIds()[i];
                CategoryOption catOpt = categoryOptionService.getCategoryOptionById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(catOpt.getName());
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

        // contactIds
        if (!ArrayUtils.isEmpty(tf.getContactIds())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_CONTACT)));
            table.addCell(cell);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getContactIds().length; i++) {
                Integer integer = tf.getContactIds()[i];
                User u = userService.getUserById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(u.getFirstName()).append(" ").append(u.getLastName());
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

//        tf.getCreatedDateOperator();
        if (tf.getCreatedDateOperator() != null && !tf.getCreatedDateOperator().equals(TicketFilterOperators.DATE_ANY)) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_CREATED)));
            table.addCell(cell);
            String s = getDateFilterDescription(df, tf.getCreatedDateOperator(), tf.getCreatedDate(), tf.getCreatedDate2());
            cell = new PdfPCell(new Phrase(s));
            table.addCell(cell);
        }

//        tf.getCustomFieldName();
        if (!ArrayUtils.isEmpty(tf.getCustomFieldName()) && !ArrayUtils.isEmpty(tf.getCustomFieldValue())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_CUSTOMFIELD)));
            table.addCell(cell);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getCustomFieldName().length; i++) {
                String cfn = tf.getCustomFieldName()[i];
                String cfv = tf.getCustomFieldValue()[i];
                if (StringUtils.isNotBlank(cfn) && StringUtils.isNotBlank(cfv)) {
                    if (sb.length() > 0) sb.append(", ");
                    sb.append(cfn).append(" = ").append(cfv);
                }
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

//        tf.getEstimatedDateOperator();
        if (tf.getEstimatedDateOperator() != null && !tf.getEstimatedDateOperator().equals(TicketFilterOperators.DATE_ANY)) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_ESTIMATED)));
            table.addCell(cell);
            String s = getDateFilterDescription(df, tf.getEstimatedDateOperator(), tf.getEstimatedDate(),
                    tf.getEstimatedDate2());
            cell = new PdfPCell(new Phrase(s));
            table.addCell(cell);
        }

//        tf.getGroupIds();
        if (!ArrayUtils.isEmpty(tf.getGroupIds())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_GROUP)));
            table.addCell(cell);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getGroupIds().length; i++) {
                Integer integer = tf.getGroupIds()[i];
                Group g = groupService.getGroupById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(g.getName());
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

//        tf.getLocationIds();
        if (!ArrayUtils.isEmpty(tf.getLocationIds())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_LOCATION)));
            table.addCell(cell);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getLocationIds().length; i++) {
                Integer integer = tf.getLocationIds()[i];
                Location loc = locationService.getLocationById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(loc.getName());
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

//        tf.getModifiedDateOperator();
        if (tf.getModifiedDateOperator() != null && !tf.getModifiedDateOperator().equals(TicketFilterOperators.DATE_ANY)) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_MODIFIED)));
            table.addCell(cell);
            String s = getDateFilterDescription(df, tf.getModifiedDateOperator(), tf.getModifiedDate(),
                    tf.getModifiedDate2());
            cell = new PdfPCell(new Phrase(s));
            table.addCell(cell);
        }

//        tf.getNote();
        if (!ArrayUtils.isEmpty(tf.getNote())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_NOTE)));
            table.addCell(cell);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getNote().length; i++) {
                String s = tf.getNote()[i];
                if (sb.length() > 0) sb.append(", ");
                sb.append(s);
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

//        tf.getPriorityIds();
        if (!ArrayUtils.isEmpty(tf.getPriorityIds())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_PRIORITY)));
            table.addCell(cell);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getPriorityIds().length; i++) {
                Integer integer = tf.getPriorityIds()[i];
                TicketPriority p = ticketPriorityService.getPriorityById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(p.getName());
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

//        tf.getStatusIds();
        if (!ArrayUtils.isEmpty(tf.getStatusIds())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_STATUS)));
            table.addCell(cell);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getStatusIds().length; i++) {
                Integer integer = tf.getStatusIds()[i];
                Status status = statusService.getStatusById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(status.getName());
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

//        tf.getSubject();
        if (!ArrayUtils.isEmpty(tf.getSubject())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_SUBJECT)));
            table.addCell(cell);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getSubject().length; i++) {
                String s = tf.getSubject()[i];
                if (sb.length() > 0) sb.append(", ");
                sb.append(s);
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

//        tf.getSubmittedByIds();
        if (!ArrayUtils.isEmpty(tf.getSubmittedByIds())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_SUBMITTED)));
            table.addCell(cell);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getSubmittedByIds().length; i++) {
                Integer integer = tf.getSubmittedByIds()[i];
                User u = userService.getUserById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(u.getFirstName()).append(" ").append(u.getLastName());
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

//        tf.getTicketNumbers();
        if (!ArrayUtils.isEmpty(tf.getTicketNumbers())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_TICKETNUMBER)));
            table.addCell(cell);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getTicketNumbers().length; i++) {
                Integer integer = tf.getTicketNumbers()[i];
                if (sb.length() > 0) sb.append(", ");
                sb.append(integer);
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

//        tf.getWorkTimeOperator();
        if (StringUtils.isNotBlank(tf.getWorkTimeOperator())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_WORKTIME)));
            table.addCell(cell);
            StringBuilder sb = new StringBuilder();
            if (tf.getWorkTimeOperator().equals("more")) {
                sb.append(getMessageSourceAccessor().getMessage("ticketSearch.worktime.more")).append(" ");
            } else if (tf.getWorkTimeOperator().equals("less")) {
                sb.append(getMessageSourceAccessor().getMessage("ticketSearch.worktime.less")).append(" ");
            } else {
                sb.append("unknown work time operator! ");
            }

            if (StringUtils.isNotBlank(tf.getWorkTimeHours())) {
                sb.append(tf.getWorkTimeHours()).append(" ")
                        .append(getMessageSourceAccessor().getMessage("scheduler.time.hour")).append(" ");
            }

            if (StringUtils.isNotBlank(tf.getWorkTimeMinutes())) {
                sb.append(tf.getWorkTimeMinutes()).append(" ")
                        .append(getMessageSourceAccessor().getMessage("scheduler.time.mins"));
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

        // zenAssetIds
        if (!ArrayUtils.isEmpty(tf.getZenAssetIds())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_ZENWORKS10ASSET)));
            table.addCell(cell);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getZenAssetIds().length; i++) {
                Integer integer = tf.getZenAssetIds()[i];
                ZenAsset za = zenAssetService.getById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(za.getAssetName());
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

        document.add(table);
        document.add(new Paragraph(" "));
        document.add(new Paragraph(" "));
    }

    private String getDateFilterDescription(DateFormat df, Integer dateOperator, String date1, String date2) {
        StringBuilder sb = new StringBuilder();
        if (dateOperator.equals(TicketFilterOperators.DATE_TODAY)) {
            sb.append(getMessageSourceAccessor().getMessage("date.today"));
        } else if (dateOperator.equals(TicketFilterOperators.DATE_WEEK)) {
            sb.append(getMessageSourceAccessor().getMessage("date.thisweek"));
        } else if (dateOperator.equals(TicketFilterOperators.DATE_MONTH)) {
            sb.append(getMessageSourceAccessor().getMessage("date.thismonth"));
        } else if (dateOperator.equals(TicketFilterOperators.DATE_ON)) {
            Date d1 = parseDate(date1);
            sb.append(getMessageSourceAccessor().getMessage("date.on")).append(" ").append(df.format(d1));
        } else if (dateOperator.equals(TicketFilterOperators.DATE_BEFORE)) {
            Date d1 = parseDate(date1);
            sb.append(getMessageSourceAccessor().getMessage("date.before")).append(" ").append(df.format(d1));
        } else if (dateOperator.equals(TicketFilterOperators.DATE_AFTER)) {
            Date d1 = parseDate(date1);
            sb.append(getMessageSourceAccessor().getMessage("date.after")).append(" ").append(df.format(d1));
        } else if (dateOperator.equals(TicketFilterOperators.DATE_RANGE)) {
            Date d1 = parseDate(date1);
            Date d2 = parseDate(date2);
            sb.append(getMessageSourceAccessor().getMessage("date.range")).append(" ").append(df.format(d1))
                    .append(" - ").append(df.format(d2));
        }

        return sb.toString();
    }

    private Date parseDate(String date1) {
        Date selectedDate;
        try {
            selectedDate = filterDateFormat.parse(date1);
        } catch (ParseException e) {
            log.error("error parsing date: " + date1, e);
            throw new RuntimeException(e);
        }

        return selectedDate;
    }

    private void buildFiltersTableStatic(Document document, GLRefreshablePagedListHolder pgHolder)
            throws DocumentException {
        PdfPTable table;
        PdfPCell cell;
        table = new PdfPTable(9);
        table.setWidthPercentage(100);
        table.getDefaultCell().setBorderWidth(1);
        table.getDefaultCell().setBorderColor(Color.black);
        table.getDefaultCell().setPadding(4);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

        cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticketList.filters"), HEADING_FONT));
        cell.setColspan(9);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setBorder(0);
        cell.setPaddingBottom(4);
        table.addCell(cell);

        String[] colHeads = getColumnHeadings(pgHolder);

        for (String colHead : colHeads) {
            cell = new PdfPCell(new Phrase(colHead, HEADING_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setGrayFill(0.9f);
            table.addCell(cell);
        }

        TicketsFilter filter = new TicketsFilter();
        if(pgHolder.getFilter() instanceof TicketsFilter){
            filter = (TicketsFilter) pgHolder.getFilter();
        }
        if(pgHolder.getFilter() instanceof AdvancedTicketsFilter){
            AdvancedTicketsFilter advFilter = (AdvancedTicketsFilter) pgHolder.getFilter();
            filter.setTicketNumber(StringUtils.isNotBlank(advFilter.getTicketNumber()) ? advFilter.getTicketNumber() : "");
            filter.setLocation(advFilter.getLocation() != null ? advFilter.getLocation().getName() : "");
            filter.setSubject(StringUtils.isNotBlank(advFilter.getSubject()) ? advFilter.getSubject() : "");
            filter.setCategoryOption(advFilter.getCategoryOption() != null ? advFilter.getCategoryOption().getName() : "");
            if(advFilter.getAssignedTo() != null){
                if(Boolean.valueOf(advFilter.getAssignedToTicketPool()) == Boolean.TRUE){
                    filter.setAssignedTo(advFilter.getAssignedToName());
                }
                else{
                    filter.setAssignedTo(advFilter.getAssignedToName());
                }
            }
            filter.setStatus(advFilter.getTicketStatus() != null ? advFilter.getTicketStatus().getName() : "");
            filter.setCreationDate(StringUtils.isNotBlank(advFilter.getCreationDate()) ? advFilter.getCreationDate() : "");
        }

        // get actual priority from id
        TicketPriority priority;
        String priorityName = "";
        if (StringUtils.isNotBlank(filter.getPriority())) {
            priority = ticketPriorityService.getPriorityById(Integer.valueOf(filter.getPriority()));
            priorityName = priority.getName();
        }
        table.addCell(new PdfPCell(new Phrase(priorityName, HEADING_DATA_FONT)));
        table.addCell(new PdfPCell(new Phrase(StringUtils.isBlank(filter.getTicketNumber()) ? " " : filter.getTicketNumber(), HEADING_DATA_FONT)));
        table.addCell(new PdfPCell(new Phrase(StringUtils.isBlank(filter.getContact()) ? " " : filter.getContact(), HEADING_DATA_FONT)));
        table.addCell(new PdfPCell(new Phrase(StringUtils.isBlank(filter.getLocation()) ? " " : filter.getLocation(), HEADING_DATA_FONT)));
        table.addCell(new PdfPCell(new Phrase(StringUtils.isBlank(filter.getSubject()) ? " " : filter.getSubject(), HEADING_DATA_FONT)));
        table.addCell(new PdfPCell(new Phrase(StringUtils.isBlank(filter.getCategoryOption()) ? " " : filter.getCategoryOption(), HEADING_DATA_FONT)));
        table.addCell(new PdfPCell(new Phrase(StringUtils.isBlank(filter.getAssignedTo()) ? " " : filter.getAssignedTo(), HEADING_DATA_FONT)));
        table.addCell(new PdfPCell(new Phrase(StringUtils.isBlank(filter.getStatus()) ? " " : filter.getStatus(), HEADING_DATA_FONT)));
        table.addCell(new PdfPCell(new Phrase(StringUtils.isBlank(filter.getCreationDate()) ? " " : filter.getCreationDate(), HEADING_DATA_FONT)));

        document.add(table);
//        document.newPage();
        document.add(new Paragraph(" "));
        document.add(new Paragraph(" "));
    }

    private String[] getColumnHeadings(GLRefreshablePagedListHolder pgHolder) {
        String[] colHeads;

        if (pgHolder.getFilter() != null && pgHolder.getFilter() instanceof TicketFilter) {
            TicketFilter tf = (TicketFilter) pgHolder.getFilter();
            String[] columns = tf.getColumnOrder();
            colHeads = new String[columns.length];
            for (int i = 0; i < columns.length; i++) {
                String column = columns[i];
                colHeads[i] = getMessageSourceAccessor().getMessage(column);
            }
        } else {
            colHeads = new String[] {
                    getMessageSourceAccessor().getMessage("ticket.priority"),
                    getMessageSourceAccessor().getMessage("ticket.number"),
                    getMessageSourceAccessor().getMessage("ticket.contact"),             
                    getMessageSourceAccessor().getMessage("ticket.location"),
                    getMessageSourceAccessor().getMessage("ticket.subject"),
                    getMessageSourceAccessor().getMessage("ticket.categoryOption"),
                    getMessageSourceAccessor().getMessage("ticket.assignedTo"),
                    getMessageSourceAccessor().getMessage("ticket.status"),
                    getMessageSourceAccessor().getMessage("ticket.creationDate")
            };
        }

        return colHeads;
    }

    private void buildSummaryTable(Document document, String groupName, GLRefreshablePagedListHolder pgHolder,
                                   DateFormat df, SortDefinition sort) throws DocumentException {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(50);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.getDefaultCell().setBorderWidth(1);
        table.getDefaultCell().setBorderColor(Color.black);
        table.getDefaultCell().setPadding(4);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticketList.summary"), HEADING_FONT));
        cell.setColspan(2);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setGrayFill(0.7f);
        table.addCell(cell);

        // We put the used criteria and extracting information
        cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticketList.dateExported"), HEADING_FONT));
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(df.format(pgHolder.getRefreshDate()), HEADING_DATA_FONT));
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticketList.nbTickets"), HEADING_FONT));
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(String.valueOf(pgHolder.getNrOfElements()), HEADING_DATA_FONT));
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.group"), HEADING_FONT));
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(groupName, HEADING_DATA_FONT));
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticketList.sorting"), HEADING_FONT));
        cell.setPaddingBottom(4);
        table.addCell(cell);
        // Get the name of the sorted column
        String colNameKey = "";
        if (sort.getProperty().equals("priority")) colNameKey = "ticket.priority";
        else if (sort.getProperty().equals("ticketId")) colNameKey = "ticket.number";
        else if (sort.getProperty().equals("contact")) colNameKey = "ticket.contact";
        else if (sort.getProperty().equals("location")) colNameKey = "ticket.location";
        else if (sort.getProperty().equals("subject")) colNameKey = "ticket.subject";
        else if (sort.getProperty().equals("categoryOption")) colNameKey = "ticket.categoryOption";
        else if (sort.getProperty().equals("assignedTo")) colNameKey = "ticket.assignedTo";
        else if (sort.getProperty().equals("status")) colNameKey = "ticket.status";
        else if (sort.getProperty().equals("createdDate")) colNameKey = "ticket.creationDate";

        String colName = getMessageSourceAccessor().getMessage(colNameKey, "None") + " - " +
                (sort.isAscending() ? getMessageSourceAccessor().getMessage("pagedList.pltAscending") :
                        getMessageSourceAccessor().getMessage("pagedList.pltDescending"));
        cell = new PdfPCell(new Phrase(colName, HEADING_DATA_FONT));
        cell.setPaddingBottom(4);
        table.addCell(cell);
        document.add(table);

        document.add(new Paragraph(" "));
    }


    public void setTicketPriorityService(TicketPriorityService ticketPriorityService) {
        this.ticketPriorityService = ticketPriorityService;
    }

    /**
     * Create a new document to hold the PDF contents.
     * <p>By default returns an A4 document, but the subclass can specify any
     * Document, possibly parameterized via bean properties defined on the View.
     *
     * @return the newly created iText Document instance
     * @see com.lowagie.text.Document#Document(com.lowagie.text.Rectangle)
     */
    protected Document newDocument() {
        return new Document(PageSize.A4.rotate());    //To change body of overridden methods use File | Settings | File Templates.
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setCategoryOptionService(CategoryOptionService categoryOptionService) {
        this.categoryOptionService = categoryOptionService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

    public void setStatusService(StatusService statusService) {
        this.statusService = statusService;
    }

    public void setZenAssetService(ZenAssetService zenAssetService) {
        this.zenAssetService = zenAssetService;
    }

    public TicketService getTicketService() {
        return ticketService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    private static class MyPageEvents extends PdfPageEventHelper {

        private MessageSourceAccessor messageSourceAccessor;

        // This is the PdfContentByte object of the writer
        private PdfContentByte cb;

        // We will put the final number of pages in a template
        private PdfTemplate template;

        // This is the BaseFont we are going to use for the header / footer
        private BaseFont bf = null;

        public MyPageEvents(MessageSourceAccessor messageSourceAccessor) {
            this.messageSourceAccessor = messageSourceAccessor;
        }

        // we override the onOpenDocument method
        public void onOpenDocument(PdfWriter writer, Document document) {
            try {
                bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb = writer.getDirectContent();
                template = cb.createTemplate(50, 50);
            } catch (DocumentException de) {
                de.printStackTrace();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }

        // we override the onEndPage method
        public void onEndPage(PdfWriter writer, Document document) {
            int pageN = writer.getPageNumber();
            String text = messageSourceAccessor.getMessage("page", "page") + " " + pageN + " " +
                    messageSourceAccessor.getMessage("of", "of") + " ";

//            String text = messageSourceAccessor.getMessage("pagedList.pageNumOfNum", new Object[]{""+pageN });

            float len = bf.getWidthPoint(text, 8);
            cb.beginText();
            cb.setFontAndSize(bf, 8);

            cb.setTextMatrix(MARGIN, 16);
            cb.showText(text);
            cb.endText();

            cb.addTemplate(template, MARGIN + len, 16);
            cb.beginText();
            cb.setFontAndSize(bf, 8);

            cb.endText();
        }

        // we override the onCloseDocument method
        public void onCloseDocument(PdfWriter writer, Document document) {
            template.beginText();
            template.setFontAndSize(bf, 8);
            template.showText(String.valueOf(writer.getPageNumber() - 1));
            template.endText();
        }
    }
}
