package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LicenseService;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import net.grouplink.ehelpdesk.web.domain.License;
import net.grouplink.ehelpdesk.web.utils.ApplicationConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

/**
 * author: zpearce
 * Date: 9/26/11
 * Time: 11:16 AM
 */
@Controller
@RequestMapping(value = "/licensing")
public class LicenseConfigController extends ApplicationObjectSupport {
    @Autowired private LicenseService licenseService;
    @Autowired private UserRoleGroupService userRoleGroupService;
    @Autowired private GroupService groupService;

    @RequestMapping(method = RequestMethod.GET, value = "/licensing.glml")
    public String showLicense(Model model, HttpServletRequest request) {
        License license = new License();
        refreshLicense(request, license);
        model.addAttribute(license);
        model.addAttribute("usedTechs", userRoleGroupService.getTechnicianCount());
        model.addAttribute("usedWfps", userRoleGroupService.getWFParticipantCount());
        return "licensing";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/licensing.glml")
    public String updateLicense(License license, BindingResult bindingResult, HttpServletRequest request, Model model) throws Exception {
        String licensePath = request.getSession().getServletContext().getRealPath(LicenseService.LICENSE_FILE);
        String tmpFilePath = licensePath + ".tmp";
        File tmpFile = writeTmpFile(license.getFileData(), tmpFilePath);

        // Check if the uploaded file is valid then reset application context with license data
        if (licenseService.isValidEHDLicense(tmpFilePath)) {
            writeTmpFile(license.getFileData(), licensePath);

            ServletContext application = request.getSession().getServletContext();

            licenseService.init(application.getRealPath(LicenseService.LICENSE_FILE));
            application.setAttribute(ApplicationConstants.LICENSE_EXISTS, Boolean.TRUE);

            Integer teksInLic = licenseService.getTechnicianCount();
            Integer teksInDB = userRoleGroupService.getTechnicianCount();
            application.setAttribute(ApplicationConstants.TEKS_EXCEEDED, teksInDB > teksInLic);

            Integer wfpInLic = licenseService.getWFParticpantCount();
            Integer wfpInDB = userRoleGroupService.getWFParticipantCount();
            application.setAttribute(ApplicationConstants.WFPARTICIPANTS_EXCEEDED, wfpInDB > wfpInLic);

            Integer groupsInLic = licenseService.getGroupCount();
            Integer groupsInDB = groupService.getGroupCount();
            application.setAttribute(ApplicationConstants.GROUPS_EXCEEDED, groupsInDB > groupsInLic);

            Date now = new Date();
            Date hBomb = licenseService.getGlobalBombDate();
            if (hBomb != null) {
                application.setAttribute(ApplicationConstants.GLOBAL_BOMB, now.getTime() > hBomb.getTime());
            }
            application.setAttribute(ApplicationConstants.ENTERPRISE, licenseService.isEnterprise());
            application.setAttribute(ApplicationConstants.DASHBOARDS_ALLOWED, licenseService.isDashboard());

        } else {
            bindingResult.reject("licensing.error.invalid");
            model.addAttribute(refreshLicense(request, license));
        }

        tmpFile.delete();

        if(bindingResult.hasErrors()) {
            return "licensing";
        }

        String msg = getMessageSourceAccessor().getMessage("licensing.success");
        request.setAttribute("flash.success", msg);
        return "redirect:/config/licensing/licensing.glml";
    }

    @InitBinder
    public void initBinder(final WebDataBinder binder) {
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
    }

    private License refreshLicense(HttpServletRequest request, License license) {
        licenseService.init(request.getSession().getServletContext().getRealPath(LicenseService.LICENSE_FILE));
        license.setDescription(licenseService.getDescription());
        license.setExpirationDate(licenseService.getGlobalBombDate());
        license.setUpgradeDate(licenseService.getUPDate());
        license.setTechnicianCount(licenseService.getTechnicianCount());
        license.setWfParticipantCount(licenseService.getWFParticpantCount());
        if (licenseService.isEnterprise()) {
            license.setGroupCount(licenseService.getGroupCount());
        } else {
            license.setGroupCount(2);
        }

        license.setEdition(licenseService.isEnterprise() ?
            getMessageSourceAccessor().getMessage("licensing.enterprise") :
            getMessageSourceAccessor().getMessage("licensing.standard"));

        license.setDashboards(licenseService.isDashboard());

        return license;
    }

    private File writeTmpFile(byte[] fileData, String path) throws IOException {
        FileOutputStream fos;
        DataOutputStream dos;

        File file = new File(path);
        fos = new FileOutputStream(file);
        dos = new DataOutputStream(fos);
        dos.write(fileData, 0, fileData.length);
        return file;
    }
}
