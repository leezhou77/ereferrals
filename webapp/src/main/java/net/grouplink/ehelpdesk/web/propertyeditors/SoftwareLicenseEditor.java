package net.grouplink.ehelpdesk.web.propertyeditors;

import net.grouplink.ehelpdesk.domain.asset.SoftwareLicense;
import net.grouplink.ehelpdesk.service.asset.SoftwareLicenseService;
import org.apache.commons.lang.StringUtils;

import java.beans.PropertyEditorSupport;

/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: Oct 9, 2008
 * Time: 5:55:56 PM
 */
public class SoftwareLicenseEditor extends PropertyEditorSupport {
    private SoftwareLicenseService softwareLicenseService;

    public SoftwareLicenseEditor(SoftwareLicenseService softwareLicenseService) {
        this.softwareLicenseService = softwareLicenseService;
    }

    public String getAsText() {
        SoftwareLicense lic = (SoftwareLicense) getValue();
        return (lic == null) ? "" : lic.getId().toString();
    }

    public void setAsText(String text) throws IllegalArgumentException {
        if (StringUtils.isNotBlank(text)) {
            setValue(softwareLicenseService.getById(new Integer(text)));
        } else {
            setValue(null);
        }
    }
}
