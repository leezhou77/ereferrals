package net.grouplink.ehelpdesk.web.validators;

import net.grouplink.ehelpdesk.domain.SurveyQuestion;
import net.grouplink.ehelpdesk.service.SurveyQuestionService;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Mar 25, 2008
 * Time: 6:58:41 PM
 */
public class SurveyQuestionValidator implements Validator {

	private SurveyQuestionService surveyQuestionService;

    public boolean supports(Class clazz) {
		return clazz.equals(SurveyQuestion.class);
	}

    public void validate(Object target, Errors errors) {
		SurveyQuestion surveyQuestion = (SurveyQuestion) target;
        if (surveyQuestionService.hasDuplicateSurveyQuestionText(surveyQuestion)) {
			errors.rejectValue("surveyQuestionText", "surveys.validate.question.text.notunique", "Survey question text already exists");
		}
        if(StringUtils.isBlank(surveyQuestion.getSurveyQuestionText())){
            errors.rejectValue("surveyQuestionText", "surveys.validate.question.text.empty", "Survey question text can not be empty");
        }
    }

    public void setSurveyQuestionService(SurveyQuestionService surveyQuestionService) {
        this.surveyQuestionService = surveyQuestionService;
    }
}
