package net.grouplink.ehelpdesk.web.tags;

import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;

public class AuthorizeGlobal implements Tag {

    private PageContext pc;
    private Tag parent;
    private String permission;

    public void setPageContext(PageContext pc) {
        this.pc = pc;
    }

    public void setParent(Tag t) {
        this.parent = t;
    }

    public Tag getParent() {
        return parent;
    }

    public int doStartTag() throws JspException {
        User user = (User) pc.getSession().getAttribute(SessionConstants.ATTR_USER);
        PermissionService permissionService = getPermissionService();
        boolean hasPermission = permissionService.hasGlobalPermission(user, permission);
        return hasPermission ? Tag.EVAL_BODY_INCLUDE : Tag.SKIP_BODY;
    }

    public int doEndTag() throws JspException {
        return Tag.EVAL_PAGE;
    }

    public void release() {
        pc = null;
        parent = null;
        permission = null;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    private PermissionService getPermissionService() {
        return (PermissionService) WebApplicationContextUtils.getWebApplicationContext(pc.getServletContext()).getBean("permissionService");
    }
}
