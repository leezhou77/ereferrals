package net.grouplink.ehelpdesk.web.formatters;

import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.service.LocationService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class LocationFormatter implements Formatter<Location> {
    @Autowired
    private LocationService locationService;

    public Location parse(String text, Locale locale) throws ParseException {
        if (StringUtils.isNotBlank(text)) {
            return locationService.getLocationById(Integer.valueOf(text));
        } else {
            return null;
        }
    }

    public String print(Location object, Locale locale) {
        return object == null ? "" : object.getId().toString();
    }
}
