package net.grouplink.ehelpdesk.web.converters;

import net.grouplink.ehelpdesk.domain.asset.AssetFilter;
import net.grouplink.ehelpdesk.service.asset.AssetFilterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

/**
 * author: zpearce
 * Date: 1/5/12
 * Time: 5:39 PM
 */
public class AssetFilterConverter implements Converter<String, AssetFilter> {
    @Autowired private AssetFilterService assetFilterService;
    
    public AssetFilter convert(String id) {
        try {
            int afid = Integer.parseInt(id);
            return assetFilterService.getById(afid);
        } catch (Exception e) {
            return null;
        }
    }
}
