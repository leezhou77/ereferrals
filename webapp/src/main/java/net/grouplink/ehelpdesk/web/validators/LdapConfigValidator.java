package net.grouplink.ehelpdesk.web.validators;

import net.grouplink.ehelpdesk.common.utils.ldap.LdapConfig;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.lang.StringUtils;
import org.springframework.ldap.core.DistinguishedName;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import javax.naming.directory.DirContext;

public class LdapConfigValidator extends BaseValidator {

	protected final Log logger = LogFactory.getLog(getClass());

	public boolean supports(Class clazz) {
		return clazz.equals(LdapConfig.class);
	}

	public void validate(Object target, Errors errors) {
		LdapConfig ldapConfig = (LdapConfig) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "url", "ldap.required.ldapUrl", "Ldap url required");

		/*ValidationUtils.rejectIfEmpty(errors, "login", "ldap.required.ldapLogin",
				"Login required ");
		ValidationUtils.rejectIfEmpty(errors, "password", "ldap.required.ldapPassword",
				"Password required");*/

        DirContext dirContext = null;
		try {

            LdapContextSource ldapContextSource = new LdapContextSource();

            ldapContextSource.setUrl(ldapConfig.getUrl());
            ldapContextSource.setBase(ldapConfig.getBase());

            if (StringUtils.isBlank(ldapConfig.getLogin())) {
                ldapContextSource.setAnonymousReadOnly(true);    
            } else {
                DistinguishedName dn = new DistinguishedName(ldapConfig.getLogin());
                ldapContextSource.setUserDn(dn.encode());
                String pwd = ldapConfig.getChangePassword();
                if ((pwd != null) && (pwd.length() > 0) && (! pwd.equals("********")))
                    ldapContextSource.setPassword(ldapConfig.getChangePassword());
                else
                    ldapContextSource.setPassword(ldapConfig.getPassword());
            }

            ldapContextSource.afterPropertiesSet();
			dirContext = ldapContextSource.getReadOnlyContext();
		} catch (Exception e) {
			errors.reject("ldap.ldapError", e.getLocalizedMessage());
			logger.error(e.getMessage());
		} finally {
            if (dirContext != null) {
                try {
                    dirContext.close();
                } catch (Exception ignore) {}
            }
        }
    }
}