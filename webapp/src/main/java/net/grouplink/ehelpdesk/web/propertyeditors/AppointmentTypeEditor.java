package net.grouplink.ehelpdesk.web.propertyeditors;

import net.grouplink.ehelpdesk.domain.AppointmentType;
import net.grouplink.ehelpdesk.service.AppointmentTypeService;

import java.beans.PropertyEditorSupport;

/**
 * @author mrollins
 * @version 1.0
 */
public class AppointmentTypeEditor extends PropertyEditorSupport {

    private AppointmentTypeService appointmentTypeService;

    /**
     * Constructor for use by derived PropertyEditor classes.
     */
    public AppointmentTypeEditor() {
    }

    /**
     * Constructor for use by derived PropertyEditor classes.
     * @param appointmentTypeService the service for getting appointment types
     */
    public AppointmentTypeEditor(AppointmentTypeService appointmentTypeService) {
        this.appointmentTypeService = appointmentTypeService;
    }

    /**
     * Gets the property value as a string suitable for presentation
     * to a human to edit.
     *
     * @return The property value as a string suitable for presentation
     *         to a human to edit.
     *         <p>   Returns "null" is the value can't be expressed as a string.
     *         <p>   If a non-null value is returned, then the PropertyEditor should
     *         be prepared to parse that string back in setAsText().
     */
    public String getAsText() {
        AppointmentType type = (AppointmentType)getValue();
        return (type != null) ? type.getId().toString() : "";
    }

    /**
     * Sets the property value by parsing a given String.  May raise
     * java.lang.IllegalArgumentException if either the String is
     * badly formatted or if this kind of property can't be expressed
     * as text.
     *
     * @param text The string to be parsed.
     */
    public void setAsText(String text) throws IllegalArgumentException {
        setValue(appointmentTypeService.getApptTypeById(new Integer(text)));
    }
}
