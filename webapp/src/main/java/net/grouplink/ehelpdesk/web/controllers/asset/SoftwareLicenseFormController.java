package net.grouplink.ehelpdesk.web.controllers.asset;

import net.grouplink.ehelpdesk.domain.asset.SoftwareLicense;
import net.grouplink.ehelpdesk.domain.asset.Vendor;
import net.grouplink.ehelpdesk.service.asset.SoftwareLicenseService;
import net.grouplink.ehelpdesk.service.asset.VendorService;
import net.grouplink.ehelpdesk.web.propertyeditors.VendorEditor;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: jaymehafen
 * Date: Sep 1, 2008
 * Time: 12:32:16 PM
 */
public class SoftwareLicenseFormController extends SimpleFormController {

    protected final Log logger = LogFactory.getLog(getClass());

    private SoftwareLicenseService softwareLicenseService;
    private VendorService vendorService;

    public SoftwareLicenseFormController() {
        setCommandClass(SoftwareLicense.class);
        setCommandName("softwareLicense");
        setFormView("softwareLicenseEdit");
    }

    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        SoftwareLicense sl = null;
        String slId = ServletRequestUtils.getStringParameter(request, "softwareLicenseId");
        if (StringUtils.isNotBlank(slId)) {
            sl = softwareLicenseService.getById(new Integer(slId));
        }

        if (sl == null) {
            sl = new SoftwareLicense();
        }

        return sl;
    }

    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
        Map<String, Object> model = new HashMap<String, Object>();
        List vendors = vendorService.getAllVendors();
        model.put("vendors", vendors);
        return model;
    }

    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
        super.initBinder(request, binder);

        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
        binder.registerCustomEditor(Vendor.class, new VendorEditor(vendorService));
    }

    protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
        logger.debug("errors.getErrorCount() = " + errors.getErrorCount());
    }

    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        SoftwareLicense sl = (SoftwareLicense) command;
        softwareLicenseService.saveSoftwareLicense(sl);

        return new ModelAndView(new RedirectView("softwareLicenses.glml"));
    }

    public void setSoftwareLicenseService(SoftwareLicenseService softwareLicenseService) {
        this.softwareLicenseService = softwareLicenseService;
    }

    public void setVendorService(VendorService vendorService) {
        this.vendorService = vendorService;
    }


}
