package net.grouplink.ehelpdesk.web.controllers.config.acl;

import net.grouplink.ehelpdesk.domain.Role;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.acl.GlobalPermissionEntry;
import net.grouplink.ehelpdesk.domain.acl.Permission;
import net.grouplink.ehelpdesk.domain.acl.PermissionActors;
import net.grouplink.ehelpdesk.service.acl.PermissionAvailabilityService;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/acl/globalPermissions")
public class GlobalPermissionsController {

    @Autowired
    private PermissionService permissionService;
    @Autowired
    private PermissionAvailabilityService permissionAvailabilityService;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("globalPermissions", getGlobalPermissions());
        return "globalPermissions/index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}/edit")
    public String edit(@PathVariable("id") Permission permission, Model model) {
        model.addAttribute("permission", permission);
        model.addAttribute("globalPermissionEntry", getGlobalPermissionEntry(permission));
        return "globalPermissions/edit";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{permId}/users/new")
    public String newUserForm(@PathVariable("permId") Permission permission, Model model) {
        PermissionActorsForm form = new PermissionActorsForm();
        model.addAttribute("permissionActorsForm", form);
        List<User> users = permissionAvailabilityService.getAvailableUsersForPermission(permission);
        model.addAttribute("users", users);
        model.addAttribute("permission", permission);
        return "globalPermissions/userForm";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{permId}/users")
    public String createUser(PermissionActorsForm form, BindingResult result, @PathVariable("permId") Permission permission) {
        if (!result.hasErrors()) {
            if (form.getUsers() != null) {
                GlobalPermissionEntry entry = getGlobalPermissionEntry(permission);
                for (User user : form.getUsers()) {
                    if (!entry.getPermissionActors().getUsers().contains(user)) {
                        entry.getPermissionActors().getUsers().add(user);
                    }
                }
                permissionService.saveGlobalPermissionEntry(entry);
            }

        }

        return "globalPermissions/userForm";
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{permId}/users/{userId}")
    public void deleteUser(@PathVariable("permId") Permission permission, @PathVariable("userId") User user, HttpServletResponse response) {
        GlobalPermissionEntry entry = getGlobalPermissionEntry(permission);
        entry.getPermissionActors().getUsers().remove(user);
        permissionService.saveGlobalPermissionEntry(entry);
        response.setStatus(HttpServletResponse.SC_OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{permId}/roles/new")
    public String newRoleForm(@PathVariable("permId") Permission permission, Model model) {
        PermissionActorsForm form = new PermissionActorsForm();
        model.addAttribute("permissionActorsForm", form);
        List<Role> roles = permissionAvailabilityService.getAvailableRolesForPermission(permission);
        model.addAttribute("roles", roles);
        model.addAttribute("permission", permission);
        return "globalPermissions/roleForm";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{permId}/roles")
    public String createRole(PermissionActorsForm form, BindingResult result, @PathVariable("permId") Permission permission) {
        if (!result.hasErrors()) {
            if (form.getRoles() != null) {
                GlobalPermissionEntry entry = getGlobalPermissionEntry(permission);
                for (Role role : form.getRoles()) {
                    if (!entry.getPermissionActors().getRoles().contains(role)) {
                        entry.getPermissionActors().getRoles().add(role);
                    }
                }
                permissionService.saveGlobalPermissionEntry(entry);
            }

        }

        return "globalPermissions/roleForm";
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{permId}/roles/{roleId}")
    public void deleteRole(@PathVariable("permId") Permission permission, @PathVariable("roleId") Role role, HttpServletResponse response) {
        GlobalPermissionEntry entry = getGlobalPermissionEntry(permission);
        entry.getPermissionActors().getRoles().remove(role);
        permissionService.saveGlobalPermissionEntry(entry);
        response.setStatus(HttpServletResponse.SC_OK);
    }

    private List<GlobalPermissionEntryHolder> getGlobalPermissions() {
        List<GlobalPermissionEntryHolder> globalPermissionHolders = new ArrayList<GlobalPermissionEntryHolder>();
        List<Permission> globalPerms = permissionService.getGlobalPermissions();
        List<GlobalPermissionEntry> globalPermEntries = permissionService.getGlobalPermissionEntries();
        for (Permission globalPerm : globalPerms) {
            GlobalPermissionEntryHolder gpHolder = new GlobalPermissionEntryHolder();
            gpHolder.setPermission(globalPerm);
            GlobalPermissionEntry entry = getEntryForPermission(globalPerm, globalPermEntries);
            gpHolder.setGlobalPermissionEntry(entry);
            globalPermissionHolders.add(gpHolder);
        }

        return globalPermissionHolders;
    }

    private GlobalPermissionEntry getEntryForPermission(Permission globalPerm, List<GlobalPermissionEntry> globalPermEntries) {
        for (GlobalPermissionEntry globalPermEntry : globalPermEntries) {
            if (globalPermEntry.getPermission().equals(globalPerm)) {
                return globalPermEntry;
            }
        }

        return null;
    }

    private GlobalPermissionEntry getGlobalPermissionEntry(Permission permission) {
        GlobalPermissionEntry globalPermissionEntry = permissionService.getGlobalPermissionEntry(permission);
        if (globalPermissionEntry == null) {
            globalPermissionEntry = new GlobalPermissionEntry();
            globalPermissionEntry.setPermission(permission);
            globalPermissionEntry.setPermissionActors(new PermissionActors());
        }

        return globalPermissionEntry;
    }
}
