package net.grouplink.ehelpdesk.web.formatters;

import net.grouplink.ehelpdesk.domain.Status;
import net.grouplink.ehelpdesk.service.StatusService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class StatusFormatter implements Formatter<Status> {
    @Autowired
    private StatusService statusService;

    public Status parse(String text, Locale locale) throws ParseException {
        if (StringUtils.isNotBlank(text)) {
            return statusService.getStatusById(Integer.valueOf(text));
        } else {
            return null;
        }
    }

    public String print(Status object, Locale locale) {
        return object == null ? "" : object.getId().toString();
    }
}
