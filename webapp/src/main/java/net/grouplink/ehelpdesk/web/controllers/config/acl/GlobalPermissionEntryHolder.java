package net.grouplink.ehelpdesk.web.controllers.config.acl;

import net.grouplink.ehelpdesk.domain.acl.GlobalPermissionEntry;
import net.grouplink.ehelpdesk.domain.acl.Permission;

public class GlobalPermissionEntryHolder {
    private Permission permission;
    private GlobalPermissionEntry globalPermissionEntry;

    public Permission getPermission() {
        return permission;
    }

    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    public GlobalPermissionEntry getGlobalPermissionEntry() {
        return globalPermissionEntry;
    }

    public void setGlobalPermissionEntry(GlobalPermissionEntry globalPermissionEntry) {
        this.globalPermissionEntry = globalPermissionEntry;
    }
}
