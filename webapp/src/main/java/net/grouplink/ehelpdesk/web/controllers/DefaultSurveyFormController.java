package net.grouplink.ehelpdesk.web.controllers;

import net.grouplink.ehelpdesk.domain.Survey;
import net.grouplink.ehelpdesk.domain.SurveyData;
import net.grouplink.ehelpdesk.domain.SurveyDataCollection;
import net.grouplink.ehelpdesk.domain.SurveyQuestion;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.service.SurveyDataService;
import net.grouplink.ehelpdesk.service.SurveyQuestionService;
import net.grouplink.ehelpdesk.service.SurveyService;
import net.grouplink.ehelpdesk.service.TicketService;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Date: Jul 22, 2008
 * Time: 7:00:04 PM
 */
public class DefaultSurveyFormController extends SimpleFormController {

    private SurveyDataService surveyDataService;
    private SurveyQuestionService surveyQuestionService;
    private SurveyService surveyService;
    private TicketService ticketService;

    public DefaultSurveyFormController() {
        super();
        setCommandClass(SurveyDataCollection.class);
        setCommandName("surveyDataCollection");
        setFormView("surveyDefault");
        setSuccessView("surveySubmitted.glml");
    }

    protected ModelAndView showForm(HttpServletRequest request, HttpServletResponse response, BindException errors, Map controlModel)
            throws Exception {
        Integer tid = ServletRequestUtils.getIntParameter(request, "stid");
        if (surveyDataService.getCountByTicketId(tid) == 0)
            return showForm(request, errors, getFormView(), controlModel);
        else {
            Map<String, Object> model = new HashMap<String, Object>();
            model.put("redMessage", "surveys.alreadySubmitted");
            return new ModelAndView("surveyAlreadyCompleted", model);
        }

    }
    
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        Integer sid = ServletRequestUtils.getIntParameter(request, "ssid");
        Integer tid = ServletRequestUtils.getIntParameter(request, "stid");

        SurveyDataCollection surveyDataCollection = new SurveyDataCollection();
        Survey survey = surveyService.getSurveyById(sid);
        Ticket ticket = ticketService.getTicketById(tid);
        for (int i = 1; i < 4; i++) {
            SurveyData surveyData = new SurveyData();
            surveyData.setId(i);
            surveyData.setSurvey(survey);
            surveyData.setTicket(ticket);
            SurveyQuestion surveyQuestion = surveyQuestionService.getSurveyQuestionById(i);
            surveyData.setSurveyQuestion(surveyQuestion);
            surveyDataCollection.addSurveyData(surveyData);
        }

        boolean surveySubmitted = ServletRequestUtils.getBooleanParameter(request, "surveySubmitted", false);
        if (!surveySubmitted) {
            for (int i = 0; i < 3; i++) {
                String val = ServletRequestUtils.getStringParameter(request, "grpQuestion" + (i + 1));
                surveyDataCollection.getSurveyData(i).setValue(val);
            }
        }

        return surveyDataCollection;
    }

    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("surveySubmitted", false);
        return model;
    }

    protected ModelAndView onSubmit(Object command) throws Exception {
        doSubmitAction(command);
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("surveySubmitted", true);
        return new ModelAndView("surveySubmitted", model);
    }

    protected void doSubmitAction(Object command) throws Exception {
        SurveyDataCollection surveyDataCollection = (SurveyDataCollection) command;
        boolean doIt = false;
        for (int i = 0; i < 3; i++) {
            SurveyData surveyData = surveyDataCollection.getSurveyData(i);
            // Get default locale
            Locale locale = LocaleContextHolder.getLocale();
            if ((surveyData.getId() == 1) && (surveyData.getValue().equals(getMessageSourceAccessor().getMessage("global.no", locale))))
                doIt = true;
            surveyDataService.saveSurveyData(surveyData);
        }

        if (doIt) {
            SurveyData sd = surveyDataCollection.getSurveyData(0);
            surveyService.sendSurveyManagementNotificationEmail(sd.getSurvey(), sd.getTicket());
        }
    }

    public void setSurveyDataService(SurveyDataService surveyDataService) {
        this.surveyDataService = surveyDataService;
    }

    public void setSurveyQuestionService(SurveyQuestionService surveyQuestionService) {
        this.surveyQuestionService = surveyQuestionService;
    }

    public void setSurveyService(SurveyService surveyService) {
        this.surveyService = surveyService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }
}
