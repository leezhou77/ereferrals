package net.grouplink.ehelpdesk.web.formatters;

import net.grouplink.ehelpdesk.domain.CustomFieldType;
import net.grouplink.ehelpdesk.service.CustomFieldTypesService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class CustomFieldTypeFormatter implements Formatter<CustomFieldType> {
    @Autowired
    private CustomFieldTypesService customFieldTypeService;

    public CustomFieldType parse(String text, Locale locale) throws ParseException {
        if (StringUtils.isNotBlank(text)) {
            return customFieldTypeService.getCustomFieldType(Integer.valueOf(text));
        } else {
            return null;
        }
    }

    public String print(CustomFieldType object, Locale locale) {
        return object == null ? "" : object.getId().toString();
    }
}
