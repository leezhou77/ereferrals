package net.grouplink.ehelpdesk.web.propertyeditors;

import net.grouplink.ehelpdesk.domain.TicketPriority;
import net.grouplink.ehelpdesk.service.TicketPriorityService;
import org.apache.commons.lang.StringUtils;

import java.beans.PropertyEditorSupport;

/**
 * @author mrollins
 * @version 1.0
 */
public class TicketPriorityEditor extends PropertyEditorSupport {

    private TicketPriorityService ticketPriorityService;

    /**
     * Constructor for use by derived PropertyEditor classes.
     */
    public TicketPriorityEditor() {
    }

    /**
     * Constructor for use by derived PropertyEditor classes.
     * @param ticketPriorityService the service
     */
    public TicketPriorityEditor(TicketPriorityService ticketPriorityService) {
        this.ticketPriorityService = ticketPriorityService;
    }

    /**
     * Gets the property value as a string suitable for presentation
     * to a human to edit.
     *
     * @return The property value as a string suitable for presentation
     *         to a human to edit.
     *         <p>   Returns "null" is the value can't be expressed as a string.
     *         <p>   If a non-null value is returned, then the PropertyEditor should
     *         be prepared to parse that string back in setAsText().
     */
    public String getAsText() {
        TicketPriority ticketPriority = (TicketPriority) getValue();
        return (ticketPriority != null) ? ticketPriority.getId().toString() : "";
    }

    /**
     * Sets the property value by parsing a given String.  May raise
     * java.lang.IllegalArgumentException if either the String is
     * badly formatted or if this kind of property can't be expressed
     * as text.
     *
     * @param text The string to be parsed.
     */
    public void setAsText(String text) throws IllegalArgumentException {
        if (StringUtils.isNotBlank(text)){
            setValue(ticketPriorityService.getPriorityById(new Integer(text)));
        } else {
            setValue(null);
        }
    }

    public void setTicketPriorityService(TicketPriorityService ticketPriorityService) {
        this.ticketPriorityService = ticketPriorityService;
    }
}
