package net.grouplink.ehelpdesk.web.propertyeditors;

import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import net.grouplink.ehelpdesk.service.TicketFilterService;
import org.apache.commons.lang.StringUtils;

import java.beans.PropertyEditorSupport;

public class TicketFilterEditor extends PropertyEditorSupport {
    private TicketFilterService ticketFilterService;

    public TicketFilterEditor(TicketFilterService ticketFilterService) {
        this.ticketFilterService = ticketFilterService;
    }

    @Override
    public String getAsText() {
        TicketFilter tf = (TicketFilter) getValue();
        return (tf==null)?"":tf.getId().toString();
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        if (StringUtils.isNotBlank(text)){
            setValue(ticketFilterService.getById(new Integer(text)));
        } else {
            setValue(null);
        }
    }
}
