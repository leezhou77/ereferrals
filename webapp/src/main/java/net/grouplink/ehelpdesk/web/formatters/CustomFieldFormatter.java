package net.grouplink.ehelpdesk.web.formatters;

import net.grouplink.ehelpdesk.domain.CustomField;
import net.grouplink.ehelpdesk.service.CustomFieldsService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class CustomFieldFormatter implements Formatter<CustomField> {
    @Autowired
    private CustomFieldsService customFieldService;

    public CustomField parse(String text, Locale locale) throws ParseException {
        if (StringUtils.isNotBlank(text)) {
            return customFieldService.getCustomFieldById(Integer.valueOf(text));
        } else {
            return null;
        }
    }

    public String print(CustomField object, Locale locale) {
        return object == null ? "" : object.getId().toString();
    }
}
