package net.grouplink.ehelpdesk.web.propertyeditors;

import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import org.apache.commons.lang.StringUtils;

import java.beans.PropertyEditorSupport;

/**
 * @author david garcia
 * @version 1.0
 */
public class UserRoleGroupEditor extends PropertyEditorSupport {

    private UserRoleGroupService userRoleGroupService;

    /**
     * Constructor for use by derived PropertyEditor classes.
     */
    public UserRoleGroupEditor() {
    }

    /**
     * Constructor for use by derived PropertyEditor classes.
     *
     * @param userRoleGroupService the service
     */
    public UserRoleGroupEditor(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    /**
     * Gets the property value as a string suitable for presentation
     * to a human to edit.
     *
     * @return The property value as a string suitable for presentation
     *         to a human to edit.
     *         <p>   Returns "null" is the value can't be expressed as a string.
     *         <p>   If a non-null value is returned, then the PropertyEditor should
     *         be prepared to parse that string back in setAsText().
     */
    public String getAsText() {
        UserRoleGroup userRoleGroup = (UserRoleGroup) getValue();
        return (userRoleGroup != null) ? userRoleGroup.getId().toString() : "";
    }

    /**
     * Sets the property value by parsing a given String.  May raise
     * java.lang.IllegalArgumentException if either the String is
     * badly formatted or if this kind of property can't be expressed
     * as text.
     *
     * @param text The string to be parsed.
     */
    public void setAsText(String text) throws IllegalArgumentException {
        if (StringUtils.isNotBlank(text)){
            setValue(userRoleGroupService.getUserRoleGroupById(new Integer(text)));
        } else {
            setValue(null);
        }
    }

    public void setCategoryOptionService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }
}
