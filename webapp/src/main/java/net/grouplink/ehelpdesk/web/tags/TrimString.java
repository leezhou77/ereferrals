package net.grouplink.ehelpdesk.web.tags;

import org.apache.commons.lang.StringEscapeUtils;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;
import java.io.IOException;
import java.io.Serializable;

/**
 * @author mrollins
 * @version 1.0
 */
public class TrimString implements Tag, Serializable {

    private PageContext pageContext;
    private Tag parentTag;

    private String value;
    private String length;

    public int doStartTag() throws JspException {
        String output;

        value = StringEscapeUtils.escapeHtml(value);

        int valueLength = value.length();
        int desiredLength = Integer.parseInt(length);

        if(valueLength <= desiredLength){
            output = value;
        } else {
            String sub = value.substring(0, desiredLength);
            int lastspace = sub.lastIndexOf(' ');

            if(lastspace != -1) sub = sub.substring(0, lastspace);
            output = sub + " ...";
        }

        try {
            pageContext.getOut().write(output);
        } catch (IOException e) {
            throw new JspException(e);
        }

        return SKIP_BODY;
    }

    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }

    public void release() {
        pageContext = null;
        parentTag = null;
        value = null;
    }

    public void setPageContext(PageContext pageContext) {
        this.pageContext = pageContext;
    }

    public void setParent(Tag tag) {
        this.parentTag = tag;
    }

    public Tag getParent() {
        return parentTag;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setLength(String length) {
        this.length = length;
    }
}
