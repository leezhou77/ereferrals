package net.grouplink.ehelpdesk.web.propertyeditors;

import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.service.asset.AssetService;
import org.apache.commons.lang.StringUtils;

import java.beans.PropertyEditorSupport;

public class AssetEditor extends PropertyEditorSupport {

    private AssetService assetService;

    /**
     * Constructor for use by derived PropertyEditor classes.
     */
    public AssetEditor() {
    }

    /**
     * Constructor for use by derived PropertyEditor classes.
     * @param assetService the category service
     */
    public AssetEditor(AssetService assetService) {
        this.assetService = assetService;
    }

    /**
     * Gets the property value as a string suitable for presentation
     * to a human to edit.
     *
     * @return The property value as a string suitable for presentation
     *         to a human to edit.
     *         <p>   Returns "null" is the value can't be expressed as a string.
     *         <p>   If a non-null value is returned, then the PropertyEditor should
     *         be prepared to parse that string back in setAsText().
     */
    public String getAsText() {
        Asset asset = (Asset)getValue();
        return (asset == null) ? "" : asset.getName();
    }

    /**
     * Sets the property value by parsing a given String.  May raise
     * java.lang.IllegalArgumentException if either the String is
     * badly formatted or if this kind of property can't be expressed
     * as text.
     *
     * @param text The string to be parsed.
     */
    public void setAsText(String text) throws IllegalArgumentException {
        if(StringUtils.isNotBlank(text)){
            setValue(assetService.getAssetByAssetNumber(text));
        }
        else{
            setValue(null);
        }
    }

    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }

}
