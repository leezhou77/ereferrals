package net.grouplink.ehelpdesk.web.converters;

import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;


public class UserConverter implements Converter<String, User> {
    @Autowired
    private UserService userService;

    public User convert(String id) {
        if (StringUtils.isBlank(id)) return null;

        return userService.getUserById(Integer.valueOf(id));
    }
}
