package net.grouplink.ehelpdesk.web.validators.asset;

import net.grouplink.ehelpdesk.domain.asset.AssetType;
import net.grouplink.ehelpdesk.service.asset.AssetTypeService;
import net.grouplink.ehelpdesk.web.validators.BaseValidator;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

/**
 * User: jaymehafen
 * Date: Sep 24, 2008
 * Time: 6:18:35 PM
 */
public class AssetTypeValidator extends BaseValidator {
    private AssetTypeService assetTypeService;

    public boolean supports(Class clazz) {
        return clazz.equals(AssetType.class);
    }

    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "name", "global.fieldRequired", "This field cannot be empty");
        AssetType at = (AssetType) target;
        if (StringUtils.isNotBlank(at.getName())) {
            AssetType a = assetTypeService.getByName(at.getName());
            if (a != null && (at.getId() == null || !at.getId().equals(a.getId()))) {
                errors.rejectValue("name", "asset.type.notUnique", "The Asset Type Name already exists");
            }
        }
    }

    public void setAssetTypeService(AssetTypeService assetTypeService) {
        this.assetTypeService = assetTypeService;
    }
}