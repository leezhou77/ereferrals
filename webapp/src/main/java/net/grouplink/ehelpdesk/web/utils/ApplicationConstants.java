package net.grouplink.ehelpdesk.web.utils;

public class ApplicationConstants {
	
    public static final String LDAP_INTEGRATION = "ldapIntegration";
    public final static String GROUP_WISE_TRUSTED_APP = "groupWiseTrustedApp";
    public final static String GROUP_WISE_INTEGRATION = "groupWiseIntegration";
    public final static String GROUP_WISE_INTEGRATION_6X = "groupWiseIntegration6x";
    public final static String OUTLOOK_INTEGRATION = "outlookIntegration";

    public final static String MAIL_CONFIGURED = "mailConfigured";
    public final static String MAIL_NOTIFY_USER = "notifyUser";
    public final static String MAIL_NOTIFY_TECHNIAN = "notifyTechnician";
    public final static String MAIL_SEND_LINK_ON_NOTIFICATION = "sendLinkOnNotification";
    public final static String MAIL_EMAIL_DISPLAY_NAME = "emailDisplayName";
    public final static String MAIL_EMAIL_FROM_ADDRESS = "emailFromAddress";

    // application customization
	public final static String APP_CUSTOMIZATION_PORTAL_URL = "appCustomizationPortalUrl";
	public final static String APP_CUSTOMIZATION_SYSTEM_MESSAGE = "appCustomizationMarquee";
	public final static String APP_CUSTOMIZATION_ENABLE_ASSET_TRACKER = "appCustomizationEnableAssetTracker";
    public final static String SHOW_EHD_ASSETTRACKER = "showEhdAssetTracker";
    public static final String ALLOW_USER_REGISTRATION = "allowUserRegistration";

    // Licensing
    /**
     * @deprecated Use LicenseService instead.
     */
    public final static String LICENSE_EXISTS = "licenseExists";
    /**
     * @deprecated Use LicenseService instead.
     */
    public final static String TEKS_EXCEEDED = "doody1";
    /**
     * @deprecated Use LicenseService instead.
     */
    public final static String GROUPS_EXCEEDED = "pupu";
    /**
     * @deprecated Use LicenseService instead.
     */
    public static final String GLOBAL_BOMB = "doody3";
    /**
     * @deprecated Use LicenseService instead.
     */
    public static final String UPGRADE_DATE_BOMB = "balut PINOY!";
    /**
     * @deprecated Use LicenseService instead.
     */
    public static final String ENTERPRISE = "goody2shoes";
    /**
     * @deprecated Use LicenseService instead.
     */
    public static final String MAX_GROUPS_ALLOWED = "wannabegroupie";
    /**
     * @deprecated Use LicenseService instead.
     */
    public static final String DASHBOARDS_ALLOWED = "feeeetch";
    /**
     * @deprecated Use LicenseService instead.
     */
    public static final String TICKETTEMPLATES = "mga";
    /**
     * @deprecated Use LicenseService instead.
     */
    public static final String SCHEDULEDREPORTS = "kababayan";
    /**
     * @deprecated Use LicenseService instead.
     */
    public static final String ASSETTRACKER = "ko";
    /**
     * @deprecated Use LicenseService instead.
     */
    public static final String ZEN10INTEGRATION = "pinko";

    public static final String WFPARTICIPANTS_EXCEEDED = "wfparticipants_exceeded";
}
