package net.grouplink.ehelpdesk.web.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.Tag;
import java.io.IOException;

/**
 * author: zpearce
 * Date: 8/10/11
 * Time: 5:14 PM
 */
public class Tab extends BodyTagSupport {
    private String name, title, htmlClass, url;

    @Override
    public int doStartTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();
            Tabs parent = (Tabs)findAncestorWithClass(this, Tabs.class);
            out.print("<li id=\"" + name + "\" class=\"");
            if (htmlClass != null) {
                out.print(htmlClass);
            }
            if (parent != null && name != null && name.equals(parent.getCurrentTab())) {
                out.print(" current");
            }
            out.print("\">");
            out.print("<a href=\"" + url + "\">" + title + "</a>");
        } catch (IOException ioe) {
            throw new JspException("Error: IOException while writing to client.");
        }
        return EVAL_BODY_INCLUDE;
    }

    public int doEndTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();
            out.print("</li>");
        } catch (IOException ioe) {
            throw new JspException("Error: IOException while writing to client.");
        }
        return EVAL_PAGE;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setHtmlClass(String htmlClass) {
        this.htmlClass = htmlClass;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
