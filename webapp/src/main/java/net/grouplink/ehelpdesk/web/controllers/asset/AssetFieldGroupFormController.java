package net.grouplink.ehelpdesk.web.controllers.asset;

import net.grouplink.ehelpdesk.domain.asset.AssetFieldGroup;
import net.grouplink.ehelpdesk.domain.asset.AssetType;
import net.grouplink.ehelpdesk.service.asset.AssetFieldGroupService;
import net.grouplink.ehelpdesk.service.asset.AssetTypeService;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * User: jaymehafen
 * Date: Aug 6, 2008
 * Time: 3:04:08 PM
 */
public class AssetFieldGroupFormController extends SimpleFormController {
    private AssetFieldGroupService assetFieldGroupService;
    private AssetTypeService assetTypeService;

    public AssetFieldGroupFormController() {
        setFormView("assetFieldGroupEdit");
        setCommandClass(AssetFieldGroup.class);
        setCommandName("fieldGroup");
    }

    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        AssetFieldGroup assetFieldGroup;
        String fieldGroupId = ServletRequestUtils.getStringParameter(request, "fieldGroupId");
        if (StringUtils.isNotBlank(fieldGroupId)) {
            assetFieldGroup = assetFieldGroupService.getById(new Integer(fieldGroupId));
        } else {
            assetFieldGroup = new AssetFieldGroup();
        }

        return assetFieldGroup;
    }

    protected void onBind(HttpServletRequest request, Object command) throws Exception {
        AssetFieldGroup afg = (AssetFieldGroup) command;
        String[] selectedAssetTypes = ServletRequestUtils.getStringParameters(request, "selectedAssetTypes");

        if (ArrayUtils.isEmpty(selectedAssetTypes)) {
            // remove any asset types from field group
            for (Iterator i = afg.getAssetTypes().iterator(); i.hasNext();) {
                i.next();
                i.remove();
            }
        } else {
            // some asset types were selected

            // add any selected asset types that aren't already present
            for (String assetTypeId : selectedAssetTypes) {
                AssetType assetType = assetTypeService.getById(new Integer(assetTypeId));
                if (!afg.getAssetTypes().contains(assetType)) {
                    afg.getAssetTypes().add(assetType);
                }
            }

            // remove any asset types that weren't selected
            for (Iterator i = afg.getAssetTypes().iterator(); i.hasNext();) {
                AssetType assetType = (AssetType) i.next();
                if (!ArrayUtils.contains(selectedAssetTypes, assetType.getId().toString())) {
                    i.remove();
                }
            }
        }
    }

    protected Map referenceData(HttpServletRequest request) throws Exception {
        Map<String, Object> model = new HashMap<String, Object>();
        List assetTypes = assetTypeService.getAllAssetTypes();
        model.put("assetTypes", assetTypes);
        return model;
    }

    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        AssetFieldGroup afg = (AssetFieldGroup) command;
        assetFieldGroupService.saveAssetFieldGroup(afg);
        return new ModelAndView(new RedirectView("fieldGroups.glml"));
    }

    public void setAssetFieldGroupService(AssetFieldGroupService assetFieldGroupService) {
        this.assetFieldGroupService = assetFieldGroupService;
    }

    public void setAssetTypeService(AssetTypeService assetTypeService) {
        this.assetTypeService = assetTypeService;
    }
}
