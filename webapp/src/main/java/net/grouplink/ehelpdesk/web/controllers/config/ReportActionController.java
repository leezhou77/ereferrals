package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.RecurrenceSchedule;
import net.grouplink.ehelpdesk.domain.Role;
import net.grouplink.ehelpdesk.domain.TicketTemplate;
import net.grouplink.ehelpdesk.domain.TicketTemplateMaster;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.RecurrenceScheduleService;
import net.grouplink.ehelpdesk.service.TemplateMasterService;
import net.grouplink.ehelpdesk.service.TicketTemplateService;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReportActionController extends MultiActionController {

    private RecurrenceScheduleService recurrenceScheduleService;


    public ModelAndView toggleScheduler(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;

        String action = ServletRequestUtils.getStringParameter(request, "action");
        Integer recSchedId = ServletRequestUtils.getIntParameter(request, "recSchedId");

        RecurrenceSchedule rs = recurrenceScheduleService.getById(recSchedId);
        if (action.equals("start")) {
            rs.setRunning(true);
        } else {
            rs.setRunning(false);
        }

        recurrenceScheduleService.save(rs);

        return null;
    }

    public void setRecurrenceScheduleService(RecurrenceScheduleService recurrenceScheduleService) {
        this.recurrenceScheduleService = recurrenceScheduleService;
    }

}