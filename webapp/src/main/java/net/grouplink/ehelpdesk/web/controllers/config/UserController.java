package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.common.utils.GLTest;
import net.grouplink.ehelpdesk.domain.Address;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.LdapField;
import net.grouplink.ehelpdesk.domain.LdapUser;
import net.grouplink.ehelpdesk.domain.Phone;
import net.grouplink.ehelpdesk.domain.Role;
import net.grouplink.ehelpdesk.domain.UrgSetter;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRole;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.service.AddressService;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LdapFieldService;
import net.grouplink.ehelpdesk.service.LdapService;
import net.grouplink.ehelpdesk.service.LicenseService;
import net.grouplink.ehelpdesk.service.LocationService;
import net.grouplink.ehelpdesk.service.PhoneService;
import net.grouplink.ehelpdesk.service.RoleService;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.web.domain.ChangePassword;
import net.grouplink.ehelpdesk.web.domain.NotificationAssistantForm;
import net.grouplink.ehelpdesk.web.utils.ApplicationConstants;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import net.grouplink.ehelpdesk.web.validators.AddressValidator;
import net.grouplink.ehelpdesk.web.validators.PasswordValidator;
import net.grouplink.ehelpdesk.web.validators.PhoneValidator;
import net.grouplink.ehelpdesk.web.validators.UserRegistrationValidator;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: 12/5/11
 * Time: 10:15 AM
 */

@Controller
@RequestMapping(value = "/users")
public class UserController extends ApplicationObjectSupport {
    @Autowired
    private UserService userService;
    @Autowired
    private LocationService locationService;
    @Autowired
    private UserRegistrationValidator userRegistrationValidator;
    @Autowired
    private PhoneValidator phoneValidator;
    @Autowired
    private PhoneService phoneService;
    @Autowired
    private AddressValidator addressValidator;
    @Autowired
    private AddressService addressService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private PasswordValidator passwordValidator;
    @Autowired
    private LdapService ldapService;
    @Autowired
    private LdapFieldService ldapFieldService;
    @Autowired
    private UserRoleGroupService userRoleGroupService;
    @Autowired
    private GroupService groupService;
    @Autowired
    private LicenseService licenseService;

    @RequestMapping(method = RequestMethod.GET)
    public String index() {
        return "users/index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/new")
    public String newForm(Model model) {
        model.addAttribute("user", new User());
        model.addAttribute("locations", locationService.getLocations());
        return "users/form";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String create(Model model, @ModelAttribute User user, BindingResult bindingResult, HttpServletRequest request)
            throws Exception {

        userRegistrationValidator.validate(user, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("locations", locationService.getLocations());
            return "users/form";
        }

        // Encrypt and Set the password and the collaboration password
        if (StringUtils.isNotBlank(user.getChangePassword())) {
            user.setPassword(GLTest.getMd5Password(user.getChangePassword()));
        }
        String colPasword = ServletRequestUtils.getStringParameter(request, "tmpColPassword");
        if (StringUtils.isNotBlank(colPasword)) {
            user.setColPassword(GLTest.encryptPassword(colPasword));
        }

        // Get session User to set as the creator on the new User being created
        User seshunUser = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);
        seshunUser = userService.getUserById(seshunUser.getId());

        // Set the session User and creation Date
        user.setCreator(seshunUser.getFirstName() + " " + seshunUser.getLastName());
        user.setCreationDate(new Date());
        user.setActive(true);

        // Give the new User the ROLE_USER
        UserRole userRole = new UserRole();
        userRole.setUser(user);
        userRole.setRole(roleService.getRoleById(Role.ROLE_USER_ID));
        user.getUserRoles().add(userRole);

        if (!bindingResult.hasErrors()) {
            userService.saveUser(user);
            request.setAttribute("flash.success", getMessageSourceAccessor().getMessage("user.saved", "User saved successfully"));
            return "redirect:/config/users/" + user.getId() + "/edit";
        }
        model.addAttribute("locations", locationService.getLocations());
        return "users/form";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}/edit")
    public String edit(Model model, @PathVariable("id") User user, HttpServletRequest request) {
        if(user.isAdmin() && !userService.getLoggedInUser().isAdmin()){
            throw new AccessDeniedException("R U Denial?");
        }
        model.addAttribute(user);
        model.addAttribute("locations", locationService.getLocations());
        model.addAttribute("ldapFieldList", getLdapFieldList(request));
        model.addAttribute("ldapUser", getLdapUserForUser(request, user));
        return "users/form";
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{user}")
    public String udpate(Model model, @ModelAttribute("user") User user, BindingResult bindingResult, HttpServletRequest request) {
        if(user.isAdmin() && !userService.getLoggedInUser().isAdmin()){
            throw new AccessDeniedException("R U Denial?");
        }
        userRegistrationValidator.validate(user, bindingResult);
        if (!bindingResult.hasErrors()) {
            userService.saveUser(user);
            request.setAttribute("flash.success", getMessageSourceAccessor().getMessage("user.saved", "User saved successfully"));
            return "redirect:/config/users/" + user.getId() + "/edit";
        }
        model.addAttribute("locations", locationService.getLocations());
        return "users/form";
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void delete(@PathVariable("id") User user, HttpServletResponse response) {
        if(user.isAdmin()){
            throw new AccessDeniedException("R U Denial?");
        }
        boolean deleted = userService.deleteUser(user);
        if (deleted) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}/changePass")
    public String changePassword(Model model, @PathVariable("id") User user) {
        if(user.isAdmin() && !userService.getLoggedInUser().isAdmin()){
            throw new AccessDeniedException("R U Denial?");
        }
        model.addAttribute("changePassword", new ChangePassword());
        model.addAttribute(user);
        return "users/changePassword";
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{user}/changePass")
    public String savePassword(Model model, @ModelAttribute("changePassword") ChangePassword changePassword,
                               BindingResult bindingResult, @PathVariable("user") User user) {
        if(user.isAdmin() && !userService.getLoggedInUser().isAdmin()){
            throw new AccessDeniedException("R U Denial?");
        }
        passwordValidator.validate(changePassword, bindingResult);
        if (!bindingResult.hasErrors()) {
            user.setPassword(GLTest.getMd5Password(changePassword.getPassword1()));
            userService.saveUser(user);
        }
        model.addAttribute(user);
        return "users/changePassword";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{userId}/phones/new")
    public String newPhone(Model model, @PathVariable("userId") User user) {
        if(user.isAdmin() && !userService.getLoggedInUser().isAdmin()){
            throw new AccessDeniedException("R U Denial?");
        }
        model.addAttribute("phone", new Phone());
        model.addAttribute("user", user);
        return "users/phone";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{userId}/phones")
    public String createPhone(@ModelAttribute Phone phone, BindingResult bindingResult,
                              @PathVariable("userId") User user) {
        user.addPhone(phone);

        phoneValidator.validate(phone, bindingResult);
        if (!bindingResult.hasErrors()) {
            userService.saveUser(user);
        }
        return "users/phone";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{userId}/phones/{phoneId}/edit")
    public String editPhone(Model model, @PathVariable("userId") User user, @PathVariable("phoneId") Phone phone) {
        model.addAttribute("phone", phone);
        model.addAttribute("user", user);
        return "users/phone";
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{userId}/phones/{phone}")
    public String updatePhone(@ModelAttribute("phone") Phone phone, BindingResult bindingResult) {
        phoneValidator.validate(phone, bindingResult);
        if (!bindingResult.hasErrors()) {
            phoneService.savePhone(phone);
        }
        return "users/phone";
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{userId}/phones/{phoneId}")
    public void deletePhone(@PathVariable("phoneId") Phone phone, HttpServletResponse response) {
        boolean deleted = phoneService.deletePhone(phone);
        if (deleted) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{userId}/addresses/new")
    public String newAddress(Model model, @PathVariable("userId") User user) {
        Address address = new Address();
        address.setCountry("US");
        model.addAttribute("address", address);
        model.addAttribute("user", user);
        return "users/address";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{userId}/addresses")
    public String createAddress(@ModelAttribute Address address, BindingResult bindingResult,
                                @PathVariable("userId") User user) {
        user.addAddress(address);

        addressValidator.validate(address, bindingResult);
        if (!bindingResult.hasErrors()) {
            userService.saveUser(user);
        }
        return "users/address";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{userId}/addresses/{addressId}/edit")
    public String editAddress(Model model, @PathVariable("userId") User user, @PathVariable("addressId") Address address) {
        model.addAttribute("address", address);
        model.addAttribute("user", user);
        return "users/address";
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{userId}/addresses/{address}")
    public String updateAddress(@ModelAttribute("address") Address address, BindingResult bindingResult) {
        addressValidator.validate(address, bindingResult);
        if (!bindingResult.hasErrors()) {
            addressService.saveAddress(address);
        }
        return "users/address";
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{userId}/addresses/{addressId}")
    public void deleteAddress(@PathVariable("addressId") Address address, HttpServletResponse response) {
        boolean deleted = addressService.deleteAddress(address);
        if (deleted) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{userId}/groupRoles/new")
    public String newGroupRole(Model model, @PathVariable("userId") User user, HttpServletRequest request) {
        model.addAttribute("urgSetter", new UrgSetter());
        model.addAttribute("user", user);
        List<Role> roles = new ArrayList<Role>();
        roles.add(roleService.getRoleById(Role.ROLE_MANAGER_ID));
        roles.add(roleService.getRoleById(Role.ROLE_TECHNICIAN_ID));
        if (licenseService.getWFParticpantCount() > 0) {
            roles.add(roleService.getRoleById(Role.ROLE_WFPARTICIPANT_ID));
        }
        roles.add(roleService.getRoleById(Role.ROLE_MEMBER_ID));
        model.addAttribute("roles", roles);
        model.addAttribute("availableGroups", getAvailableGroups(request, user));
        return "users/groupRoleNew";
    }


    @RequestMapping(method = RequestMethod.POST, value = "/{userId}/groupRoles")
    public String createGroupRole(Model model, @ModelAttribute UrgSetter urgSetter, BindingResult bindingResult,
                                  @PathVariable("userId") User user, HttpServletRequest request) {

        UserRole userRole = new UserRole();
        userRole.setUser(user);
        userRole.setRole(urgSetter.getRole());

        UserRoleGroup urg = new UserRoleGroup();
        urg.setGroup(urgSetter.getGroup());
        urg.setUserRole(userRole);

        checkLicCountCompliance(urgSetter, bindingResult, user);


        if (!bindingResult.hasErrors()) {
            userRoleGroupService.saveUserRoleGroup(urg);
        }

        model.addAttribute("user", user);
        model.addAttribute("roles", roleService.getGroupRoles());
        model.addAttribute("availableGroups", getAvailableGroups(request, user));
        return "users/groupRoleNew";
    }

    private void checkLicCountCompliance(UrgSetter urgSetter, BindingResult bindingResult, User user) {
        // check if user has existing tech or manager roles (for licensing check later)
        boolean hasTechOrManagerRole = false;
        List<UserRoleGroup> userRoleGroupList = userRoleGroupService.getAllUserRoleGroupByUserId(user.getId());
        for (UserRoleGroup u : userRoleGroupList) {
            if (u.getUserRole().getRole() != null) {
                Integer roleId = u.getUserRole().getRole().getId();
                if (roleId.equals(Role.ROLE_ADMINISTRATOR_ID) ||
                        roleId.equals(Role.ROLE_MANAGER_ID) ||
                        roleId.equals(Role.ROLE_TECHNICIAN_ID)) {
                    hasTechOrManagerRole = true;
                    break;
                }
            }
        }

        boolean adminManagerOrTechRole = false;
        Integer roleId = urgSetter.getRole().getId();
        if (roleId.equals(Role.ROLE_ADMINISTRATOR_ID) || roleId.equals(Role.ROLE_MANAGER_ID) || roleId.equals(Role.ROLE_TECHNICIAN_ID)) {
            adminManagerOrTechRole = true;
        }

        // check license. if user has existing tech or manager role, no need to check
        if (!hasTechOrManagerRole && adminManagerOrTechRole) {
            Integer allowedTechs = licenseService.getTechnicianCount();
            Integer techCount = userRoleGroupService.getTechnicianCount();
            if (techCount >= allowedTechs) {
                bindingResult.addError(new ObjectError("LIC_ERROR_TECH_EXCEEDED", getMessageSourceAccessor().getMessage("licensing.tech.limit")));
            }
        }

        // check workflow participant count in license
        if(!hasTechOrManagerRole && urgSetter.getRole().getId().equals(Role.ROLE_WFPARTICIPANT_ID)){
            Integer allowedWFPs = licenseService.getWFParticpantCount();
            Integer wfpCount = userRoleGroupService.getWFParticipantCount();
            if(wfpCount >= allowedWFPs){
                bindingResult.addError(new ObjectError("LIC_ERROR_WFP_EXCEEDED", getMessageSourceAccessor().getMessage("licensing.wfp.limit")));
            }
        }
    }


    @RequestMapping(method = RequestMethod.GET, value = "/{userId}/groupRoles/{urgId}/edit")
    public String editGroupRole(Model model, @PathVariable("userId") User user, @PathVariable("urgId") UserRoleGroup urg) {
        UrgSetter urgSetter = new UrgSetter();
        urgSetter.setUserRoleGroup(urg);
        urgSetter.setUser(user);
        urgSetter.setGroup(urg.getGroup());
        urgSetter.setRole(urg.getUserRole().getRole());

        model.addAttribute("urgSetter", urgSetter);
        model.addAttribute("user", user);
        model.addAttribute("roles", roleService.getGroupRoles());
        return "users/groupRoleEdit";
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{userId}/groupRoles/{urgId}")
    public String updateGroupRole(Model model, @ModelAttribute UrgSetter urgSetter, BindingResult bindingResult,
                                  @PathVariable("userId") User user, @PathVariable("urgId") UserRoleGroup userRoleGroup) {

        UserRole userRole = userRoleGroup.getUserRole();

        // check if other urgs are using this userrole
        if (userRole.getUserRoleGroup().size() > 1) {
            // this userRoleGroup's userRole is used by another userRoleGroup, create a new userRole to unlink them
            userRole = new UserRole();
            userRole.setUser(user);
            userRole.getUserRoleGroup().add(userRoleGroup);
        }

        userRole.setRole(urgSetter.getRole());
        userRoleGroup.setUserRole(userRole);

        if (!bindingResult.hasErrors()) {
            userRoleGroupService.saveUserRoleGroup(userRoleGroup);
        }

        model.addAttribute("user", user);
        model.addAttribute("roles", roleService.getGroupRoles());
        return "users/groupRoleEdit";
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{userId}/groupRoles/{urgId}")
    public void deleteAddress(@PathVariable("urgId") UserRoleGroup userRoleGroup, HttpServletResponse response) {
        boolean deleted = userRoleGroupService.delete(userRoleGroup);
        if (deleted) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{userId}/groupRoles/{urgId}/notificationAsst/edit")
    public String editNotificationAssistant(Model model, @PathVariable("userId") User user,
                                            @PathVariable("urgId") UserRoleGroup userRoleGroup) {


        NotificationAssistantForm notAssForm = new NotificationAssistantForm();
        notAssForm.setUsers(new ArrayList<User>(userRoleGroup.getNotificationUsers()));
        notAssForm.setBackupTech(userRoleGroup.getBackUpTech());
        model.addAttribute("notificationAssistantForm", notAssForm);
        model.addAttribute("urg", userRoleGroup);
        model.addAttribute("user", user);

        String assignmentForLabel = getMessageSourceAccessor().getMessage("ticketList.ticketPool");
        if (userRoleGroup.getUserRole() != null) {
            assignmentForLabel = userRoleGroup.getUserRole().getUser().getFirstName() + " " +
                    userRoleGroup.getUserRole().getUser().getLastName();
        }
        model.addAttribute("assignmentLabel", assignmentForLabel);

        List<UserRoleGroup> backupList = userRoleGroupService.getUserRoleGroupByGroupId(userRoleGroup.getGroup().getId());
        backupList.remove(userRoleGroup);
        Collections.sort(backupList);
        model.addAttribute("backupList", backupList);

        List<User> techniciansByGroupId = userRoleGroupService.getTechnicianByGroupId(userRoleGroup.getGroup().getId());

        if (userRoleGroup.getUserRole() != null) {
            techniciansByGroupId.remove(userRoleGroup.getUserRole().getUser());
        }

        model.addAttribute("urgUserList", techniciansByGroupId);
        return "users/notificationAssistantForm";
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{userId}/groupRoles/{urgId}/notificationAsst")
    public String updateURGNotificationUsers(Model model, @ModelAttribute NotificationAssistantForm notAssForm,
                                             BindingResult bindingResult, @PathVariable("userId") User user,
                                             @PathVariable("urgId") UserRoleGroup userRoleGroup) {
        model.addAttribute("notificationAssistantForm", notAssForm);
        model.addAttribute("urg", userRoleGroup);
        model.addAttribute("user", user);

        if (!bindingResult.hasErrors()) {
            if (notAssForm.getUsers() != null) {
                userRoleGroup.setNotificationUsers(new HashSet<User>(notAssForm.getUsers()));
            } else {
                userRoleGroup.setNotificationUsers(new HashSet<User>());
            }
            userRoleGroup.setBackUpTech(notAssForm.getBackupTech());
            userRoleGroupService.saveUserRoleGroup(userRoleGroup);
        }
        return "users/notificationAssistantForm";
    }

    private List<Group> getAvailableGroups(HttpServletRequest request, User grantee) {
        // this is the user granting the new role
        User granter = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);

        // get the groups that the granter can grant to the receiver
        List<Group> availableGroups = null;
        if (granter != null) {
            if (granter.getId() == 0) {
                // admin can change all groups
                availableGroups = groupService.getGroups();
            } else {
                // I can only changes roles in groups that I manage
                availableGroups = groupService.getGroupsByManagerId(granter.getId());
            }
        }

        // Get the groups that the grantee already has and exclude them
        List excludedGroups = userRoleGroupService.getUserRoleGroupByUserId(grantee.getId());
        if (availableGroups != null && excludedGroups != null) {
            for (Object excludedGroup : excludedGroups) {
                availableGroups.remove(((UserRoleGroup) excludedGroup).getGroup());
            }
        }
        return availableGroups;
    }

    private List getLdapFieldList(HttpServletRequest request) {
        List<LdapField> ldapFieldList = new ArrayList<LdapField>();

        ServletContext application = request.getSession(true).getServletContext();
        Boolean ldapIntegration = (Boolean) application.getAttribute(ApplicationConstants.LDAP_INTEGRATION);
        if (ldapIntegration != null && ldapIntegration) {
            ldapFieldList = this.ldapFieldService.getLdapFields();
        }

        return ldapFieldList;
    }

    private LdapUser getLdapUserForUser(HttpServletRequest request, User user) {
        LdapUser ldapUser = new LdapUser();

        ServletContext application = request.getSession(true).getServletContext();
        Boolean ldapIntegration = (Boolean) application.getAttribute(ApplicationConstants.LDAP_INTEGRATION);

        if (ldapIntegration != null && ldapIntegration && user != null) {
            if (StringUtils.isNotBlank(user.getLdapDN())) {
                ldapUser = this.ldapService.getLdapUser(user.getLdapDN());
            }
        }
        return ldapUser;
    }
}
