package net.grouplink.ehelpdesk.web.views;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;
import net.grouplink.ehelpdesk.domain.Address;
import net.grouplink.ehelpdesk.domain.Attachment;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.CustomField;
import net.grouplink.ehelpdesk.domain.CustomFieldValues;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.LdapField;
import net.grouplink.ehelpdesk.domain.LdapUser;
import net.grouplink.ehelpdesk.domain.Phone;
import net.grouplink.ehelpdesk.domain.Status;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketAudit;
import net.grouplink.ehelpdesk.domain.TicketHistory;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRole;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.domain.ZenInformation;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.service.CustomFieldsService;
import net.grouplink.ehelpdesk.service.LdapFieldService;
import net.grouplink.ehelpdesk.service.LdapService;
import net.grouplink.ehelpdesk.service.TicketService;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import net.grouplink.ehelpdesk.web.surveys.SurveyItem;
import net.grouplink.ehelpdesk.web.utils.ApplicationConstants;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.Color;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class TicketPdfView extends AbstractPdfView {

    private static final Color WHT = Color.white;
    private static final Color BLK = Color.black;
    private static final Color DGR = Color.darkGray;

    private static final Font HDR_BOLD_FONT = new Font(Font.HELVETICA, 12, Font.BOLD, BLK);
    private static final Font SECTION_FONT = new Font(Font.HELVETICA, 12, Font.BOLD, BLK);
    private static final Font TEXT_FONT = new Font(Font.HELVETICA, 10, Font.NORMAL, BLK);
    private static final Font BOLD_FONT = new Font(Font.HELVETICA, 10, Font.BOLD, BLK);
    private static final Font ITAL_FONT = new Font(Font.ITALIC, 10, Font.ITALIC, BLK);
    private static final Font GRAY_FONT = new Font(Font.HELVETICA, 10, Font.NORMAL, DGR);

    private static final Font TITLE_FONT_ITALIC = new Font(Font.HELVETICA, 18, Font.ITALIC, new Color(153, 153, 153));
    private static final Font TITLE_FONT_BOLD = new Font(Font.HELVETICA, 18, Font.BOLD, new Color(204, 51, 0));

    private static final int MARGIN = 32;

    private LdapService ldapService;
    private LdapFieldService ldapFieldService;
    private CustomFieldsService customFieldsService;
    private TicketService ticketService;
    private PermissionService permissionService;

    protected void buildPdfDocument(Map map, Document document, PdfWriter writer, HttpServletRequest request,
                                    HttpServletResponse response) throws Exception {

        // Get the ticket data.
        Map data = (Map) map.get("data");
        Ticket ticket = (Ticket) data.get("ticket");

        if (ticket == null) {
            throw new RuntimeException("Ticket cannot be null for TicketPdfView");
        }

        Locale locale = (Locale) data.get("locale");

         DateFormat sdf = DateFormat.getDateInstance(DateFormat.MEDIUM,  locale);
         DateFormat dtf = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, locale);

        // Create and add the event handler.
        // This is to ensure that only entire cells are printed at end of pages
        MyPageEvents events = new MyPageEvents(getMessageSourceAccessor());
        writer.setPageEvent(events);
        events.onOpenDocument(writer, document);

        document.setMargins(45, 40, 40, 40);

        // <-- Form Header -->
        PdfContentByte cb = writer.getDirectContentUnder();
        cb.setColorFill(WHT);
        cb.setGrayStroke(.05F);
        cb.roundRectangle(32, 695, 520, 83, 8);
        cb.fillStroke();

        Paragraph title = new Paragraph();

        String everything = "";
        String helpDesk = "eReferrals\u2122";

        title.add(new Chunk(everything, TITLE_FONT_ITALIC));
        title.add(new Chunk(helpDesk, TITLE_FONT_BOLD));
        document.add(title);
        document.add(new Paragraph(" "));

        Paragraph topBubble = new Paragraph();

        Integer tid = ticket.getTicketId();
        String ticketid = "" + tid;
        User subbyUsr = ticket.getSubmittedBy();
        String subby = subbyUsr.getFirstName() + " " + subbyUsr.getLastName();
        Date createdDate = ticket.getCreatedDate();
        String createdate = createdDate != null ? dtf.format(createdDate) : " ";
        Date modDate = ticket.getModifiedDate();
        String moddate = modDate != null ? dtf.format(modDate) : " ";

        Phrase ph1 = new Phrase(getMessageSourceAccessor().getMessage("ticket.number") + ": " + ticketid, HDR_BOLD_FONT);
        Paragraph p1 = new Paragraph(ph1);
        Phrase ph2 = new Phrase(getMessageSourceAccessor().getMessage("ticket.submittedBy") + ": " + subby, HDR_BOLD_FONT);
        Paragraph p2 = new Paragraph(ph2);
        Phrase ph3 = new Phrase(getMessageSourceAccessor().getMessage("ticket.creationDate") + ": " + createdate, HDR_BOLD_FONT);
        Paragraph p3 = new Paragraph(ph3);
        Phrase ph4 = new Phrase(getMessageSourceAccessor().getMessage("ticket.modifiedDate") + ": " + moddate, HDR_BOLD_FONT);
        Paragraph p4 = new Paragraph(ph4);

        topBubble.add(p1);
        topBubble.add(p2);
        topBubble.add(p3);
        topBubble.add(p4);

        document.add(topBubble);
        document.add(new Paragraph(" "));

        //<-- Work time -->
        if (hasFieldPermisssion(ticket, "ticket.view.workTime", "ticket.change.workTime")) {
            Phrase pTest = new Phrase(getMessageSourceAccessor().getMessage("ticket.workTime") + ": " + ticket.getDisplayWorkTime(), BOLD_FONT);
            document.add(pTest);
            document.add(new Paragraph(" "));
        }

        PdfPTable table;
        PdfPCell cell;

        //<-- Contact Information -->
        if (hasFieldPermisssion(ticket, "ticket.view.contact", "ticket.change.contact")) {
            table = new PdfPTable(8);
            table.setWidthPercentage(100);
            table.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.getDefaultCell().setBorderWidth(0);
            table.getDefaultCell().setPadding(4);
            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.contactInfo"), SECTION_FONT));
            cell.setColspan(8);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setGrayFill(0.7f);
            table.addCell(cell);

            User contact = null;
            contact = ticket.getContact();

            String cname = contact != null ? contact.getFirstName() + " " + contact.getLastName() : " ";
            String cemail = contact != null ? contact.getEmail() : " ";

            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.contact"), BOLD_FONT));
            cell.setColspan(1);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(cname, TEXT_FONT));
            cell.setColspan(3);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("user.email"), BOLD_FONT));
            cell.setColspan(1);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(cemail, TEXT_FONT));
            cell.setColspan(3);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);

            String cphonenum = "";
            Phone phone = contact != null ? contact.getPrimaryPhone() : null;
            if (phone != null) {
                if (StringUtils.isNotBlank(phone.getNumber()))
                    cphonenum = phone.getNumber();
                if (StringUtils.isNotBlank(phone.getExtension()))
                    cphonenum = cphonenum + " (x" + phone.getExtension() + ")";
            }

            List addrList = contact != null ? contact.getAddressList() : null;
            Address caddr = null;
            if ((addrList != null) && (addrList.size() > 0)) {
                caddr = (Address) addrList.get(0);
            }
            String addr;
            if (caddr != null) {
                StringBuilder addrB = new StringBuilder();
                addrB.append(caddr.getLine1()).append(" ");
                if ((caddr.getLine2() != null) && (!caddr.getLine2().equals("")))
                    addrB.append(caddr.getLine2()).append(" ");
                addrB.append(caddr.getCity()).append(", ");
                addrB.append(caddr.getState()).append("  ");
                addrB.append(caddr.getPostalCode());
                addr = addrB.toString();
            } else {
                addr = " ";
            }

            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("phone.phone"), BOLD_FONT));
            cell.setColspan(1);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(cphonenum, TEXT_FONT));
            cell.setColspan(3);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("address.address"), BOLD_FONT));
            cell.setColspan(1);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(addr, TEXT_FONT));
            cell.setColspan(3);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);

            document.add(table);

//        document.add(new Paragraph(" "));

            //LDAP Custom Fields
            table = new PdfPTable(8);
            table.setWidthPercentage(100);
            table.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.getDefaultCell().setBorderWidth(0);
            table.getDefaultCell().setPadding(4);
            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

            ServletContext application = request.getSession(true).getServletContext();
            Boolean ldapIntegration = (Boolean) application.getAttribute(ApplicationConstants.LDAP_INTEGRATION);
            LdapUser ldapUser = new LdapUser();
            if (ldapIntegration != null && ldapIntegration && ticket.getContact() != null) {
                if (StringUtils.isNotBlank(ticket.getContact().getLdapDN())) {
                    ldapUser = ldapService.getLdapUser(ticket.getContact().getLdapDN());
                }
            }

            List<LdapField> ldapFieldList = ldapFieldService.getLdapFields();
            int count = 0;
            for (LdapField ldapField : ldapFieldList) {
                if (ldapField.getShowOnTicket()) {
                    count++;
                    String label = StringUtils.isNotEmpty(ldapField.getLabel()) ? ldapField.getLabel() : ldapField.getName();
                    cell = new PdfPCell(new Phrase(label, BOLD_FONT));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell.setColspan(1);
                    table.addCell(cell);
                    Map attributeMap = ldapUser.getAttributeMap();
                    if (attributeMap == null || attributeMap.size() <= 0) {
                        cell = new PdfPCell(new Phrase("", TEXT_FONT));
                        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                        cell.setColspan(3);
                        table.addCell(cell);
                    } else {
                        String value = ldapUser.getAttributeMap().get(ldapField.getName());
                        cell = new PdfPCell(new Phrase(value, TEXT_FONT));
                        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                        cell.setColspan(3);
                        table.addCell(cell);
                    }
                }
            }

            if (count > 0 && count % 2 > 0) {
                cell = new PdfPCell(new Phrase("", BOLD_FONT));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell.setColspan(1);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase("", TEXT_FONT));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setColspan(3);
                table.addCell(cell);
            }

            document.add(table);
            document.add(new Paragraph(" "));
        }

        //<-- Ticket Information -->
        table = new PdfPTable(4);
        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.getDefaultCell().setBorderWidth(0);
        table.getDefaultCell().setPadding(4);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

        cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.ticketInfo"), SECTION_FONT));
        cell.setColspan(4);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setGrayFill(0.7f);
        table.addCell(cell);

        if (hasFieldPermisssion(ticket, "ticket.view.location", "ticket.change.location")) {
            String tloc = ticket.getLocation().getName();
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.location"), BOLD_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(tloc, TEXT_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);
        } else {
            table.addCell(new PdfPCell(new Phrase(" ")));
            table.addCell(new PdfPCell(new Phrase(" ")));
        }

        if (hasFieldPermisssion(ticket, "ticket.view.priority", "ticket.change.priority")) {
            String tpri = ticket.getPriority().getName();
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.priority"), BOLD_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(tpri, TEXT_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);
        } else {
            table.addCell(new PdfPCell(new Phrase(" ")));
            table.addCell(new PdfPCell(new Phrase(" ")));
        }

        if (hasFieldPermisssion(ticket, "ticket.view.group", "ticket.change.group")) {
            String group = ticket.getGroup().getName();
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.group"), BOLD_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(group, TEXT_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);
        } else {
            table.addCell(new PdfPCell(new Phrase(" ")));
            table.addCell(new PdfPCell(new Phrase(" ")));
        }

        if (hasFieldPermisssion(ticket, "ticket.view.status", "ticket.change.status")) {
            String status = ticket.getStatus().getName();
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.status"), BOLD_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(status, TEXT_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);
        } else {
            table.addCell(new PdfPCell(new Phrase(" ")));
            table.addCell(new PdfPCell(new Phrase(" ")));
        }

        if (hasFieldPermisssion(ticket, "ticket.view.category", "ticket.change.category")) {
            String category;
            if (ticket.getCategory() != null)
                category = ticket.getCategory().getName();
            else
                category = "";

            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.category"), BOLD_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(category, TEXT_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);
        } else {
            table.addCell(new PdfPCell(new Phrase(" ")));
            table.addCell(new PdfPCell(new Phrase(" ")));
        }

        if (hasFieldPermisssion(ticket, "ticket.view.submittedBy")) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.submittedBy"), BOLD_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(subby, TEXT_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);
        } else {
            table.addCell(new PdfPCell(new Phrase(" ")));
            table.addCell(new PdfPCell(new Phrase(" ")));
        }

        if (hasFieldPermisssion(ticket, "ticket.view.categoryOption", "ticket.change.categoryOption")) {
            CategoryOption categoryOpt = ticket.getCategoryOption();
            String catOpt = categoryOpt != null ? categoryOpt.getName() : " ";
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.categoryOption"), BOLD_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(catOpt, TEXT_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);
        } else {
            table.addCell(new PdfPCell(new Phrase(" ")));
            table.addCell(new PdfPCell(new Phrase(" ")));
        }

        if (hasFieldPermisssion(ticket, "ticket.view.estimatedDate", "ticket.change.estimatedDate")) {
            Date estDate = ticket.getEstimatedDate();
            String estDat = estDate != null ? sdf.format(estDate) : " ";
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.estimatedDate"), BOLD_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(estDat, TEXT_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);
        } else {
            table.addCell(new PdfPCell(new Phrase(" ")));
            table.addCell(new PdfPCell(new Phrase(" ")));
        }

        if (hasFieldPermisssion(ticket, "ticket.view.assignedTo", "ticket.change.assignedTo")) {
            UserRoleGroup urg = ticket.getAssignedTo();
            UserRole ur = urg != null ? urg.getUserRole() : null;
            User user = ur != null ? ur.getUser() : null;
            String assTo = user != null ? user.getFirstName() + " " + user.getLastName() : " ";
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.assignedTo"), BOLD_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(assTo, TEXT_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);
        } else {
            table.addCell(new PdfPCell(new Phrase(" ")));
            table.addCell(new PdfPCell(new Phrase(" ")));
        }

        if (hasFieldPermisssion(ticket, "ticket.view.asset", "ticket.change.asset")) {
            Asset assEt = ticket.getAsset();
            String assName = assEt != null ? assEt.getName() : " ";
            String assNum = assEt != null ? assEt.getAssetNumber() : " ";
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.asset.name"), BOLD_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(assName, TEXT_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase());
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase());
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.asset.number"), BOLD_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(assNum, TEXT_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);
        } else {
            table.addCell(new PdfPCell(new Phrase(" ")));
            table.addCell(new PdfPCell(new Phrase(" ")));
        }

        // Custom Fields
        List<CustomField> customFields = customFieldsService.getCustomFields(ticket.getLocation(), ticket.getGroup(), ticket.getCategory(), ticket.getCategoryOption());
        if (CollectionUtils.isNotEmpty(customFields)) {
            Collections.sort(customFields);
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.customFields"), BOLD_FONT));
            cell.setColspan(4);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setGrayFill(0.9f);
            table.addCell(cell);
            for (CustomField customField : customFields) {
                // get the custom field values for this template, if any
                CustomFieldValues cfv = getCustomFieldValue(ticket, customField);
                cell = new PdfPCell(new Phrase(customField.getName(), BOLD_FONT));
                cell.setColspan(1);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase((cfv != null && cfv.getFieldValue() != null ?
                        cfv.getFieldValue() : ""), TEXT_FONT));
                cell.setColspan(1);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
            }

            if (customFields.size() % 2 == 1) {
                cell = new PdfPCell(new Phrase("", BOLD_FONT));
                cell.setColspan(1);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase("", TEXT_FONT));
                cell.setColspan(1);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
            }
        }


        document.add(table);
        document.add(new Paragraph(" "));

        //<-- Description -->
        table = new PdfPTable(4);
        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.getDefaultCell().setBorderWidth(0);
        table.getDefaultCell().setPadding(4);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

        cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.description"), SECTION_FONT));
        cell.setColspan(4);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setGrayFill(0.7f);
        table.addCell(cell);

        if (hasFieldPermisssion(ticket, "ticket.view.subject", "ticket.change.subject")) {
            String tickSubj = ticket.getSubject();
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("scheduler.action.mail.subject"), BOLD_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(tickSubj, TEXT_FONT));
            cell.setColspan(3);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);
        }

        if (hasFieldPermisssion(ticket, "ticket.view.cc", "ticket.change.cc")) {
            String tickCC = ticket.getCc();
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.cc"), BOLD_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(tickCC, TEXT_FONT));
            cell.setColspan(3);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);
        }

        if (hasFieldPermisssion(ticket, "ticket.view.note", "ticket.change.note")) {
            String tickNote = StringUtils.isNotBlank(ticket.getNote()) ? ticket.getNote() : " ";
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.note"), BOLD_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(tickNote, TEXT_FONT));
            cell.setColspan(3);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);
        }

        if (hasFieldPermisssion(ticket, "ticket.view.attachments", "ticket.change.attachments")) {
            Set<Attachment> attachmentsSet = null;
            List<Attachment> attachmentsList = new ArrayList<Attachment>();

            attachmentsSet = ticket.getAttachments();

            if (attachmentsSet != null)
                attachmentsList.addAll(attachmentsSet);
        
            String attList = getMessageSourceAccessor().getMessage("ticket.noattachments");
            if (attachmentsList.size() > 0) {
                StringBuilder _attList = new StringBuilder();
                Object[] arrAttachments = attachmentsList.toArray();
                for (int z = 0; z < attachmentsList.size(); z++) {
                    Attachment att = (Attachment) arrAttachments[z];
                    _attList.append(att.getFileName()).append(" ");
                }

                attList = _attList.toString();
            }

            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.attachments"), BOLD_FONT));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(attList, TEXT_FONT));
            cell.setColspan(3);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);
        }

        document.add(table);
        document.add(new Paragraph(" "));

        //<-- Sub-Tickets -->
        if (hasFieldPermisssion(ticket, "ticket.view.subtickets", "ticket.change.subtickets")) {
            table = new PdfPTable(4);
            table.setWidthPercentage(100);
            table.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.getDefaultCell().setBorderWidth(0);
            table.getDefaultCell().setPadding(4);
            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.subtickets"), SECTION_FONT));
            cell.setColspan(4);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setGrayFill(0.7f);
            table.addCell(cell);

            List<Ticket> subTicketList = ticket.getSubTicketsList();
            if ((subTicketList != null) && (subTicketList.size() > 0)) {
                PdfPTable subTable = new PdfPTable(5);
                subTable.setWidthPercentage(100);
                subTable.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.getDefaultCell().setBorderWidth(0);
                table.getDefaultCell().setPadding(4);
                table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
                PdfPCell subCell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.assignedTo"), BOLD_FONT));
                subCell.setGrayFill(0.5f);
                subTable.addCell(subCell);
                subCell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.category"), BOLD_FONT));
                subCell.setGrayFill(0.5f);
                subTable.addCell(subCell);
                subCell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.categoryOption"), BOLD_FONT));
                subCell.setGrayFill(0.5f);
                subTable.addCell(subCell);
                subCell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.status"), BOLD_FONT));
                subCell.setGrayFill(0.5f);
                subTable.addCell(subCell);
                subCell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.estimatedDate"), BOLD_FONT));
                subCell.setGrayFill(0.5f);
                subTable.addCell(subCell);

                for (Ticket subticket : subTicketList) {
                    UserRoleGroup suburg = subticket != null ? subticket.getAssignedTo() : null;
                    UserRole subur = suburg != null ? suburg.getUserRole() : null;
                    User subuser = subur != null ? subur.getUser() : null;
                    String assname = subuser != null ? subuser.getFirstName() + " " + subuser.getLastName() : " ";
                    subCell = new PdfPCell(new Phrase(assname, TEXT_FONT));
                    subTable.addCell(subCell);

                    Category subCat = subticket != null ? subticket.getCategory() : null;
                    String subCatName = subCat != null ? subCat.getName() : " ";
                    subCell = new PdfPCell(new Phrase(subCatName, TEXT_FONT));
                    subTable.addCell(subCell);

                    CategoryOption subCatOpt = subticket != null ? subticket.getCategoryOption() : null;
                    String subCatOptName = subCatOpt != null ? subCatOpt.getName() : null;
                    subCell = new PdfPCell(new Phrase(subCatOptName, TEXT_FONT));
                    subTable.addCell(subCell);

                    Status substatus = subticket != null ? subticket.getStatus() : null;
                    String strsubstatus = substatus != null ? substatus.getName() : " ";
                    subCell = new PdfPCell(new Phrase(strsubstatus, TEXT_FONT));
                    subTable.addCell(subCell);

                    Date subEstDate = subticket != null ? subticket.getEstimatedDate() : null;
                    String strSubEstDate = subEstDate != null ? sdf.format(subEstDate) : " ";
                    subCell = new PdfPCell(new Phrase(strSubEstDate, TEXT_FONT));
                    subTable.addCell(subCell);
                }
                cell = new PdfPCell(subTable);
                cell.setColspan(4);
                table.addCell(cell);
            } else {
                cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.noSubTickets"), ITAL_FONT));
                cell.setColspan(4);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
            }

            document.add(table);
            document.add(new Paragraph(" "));
        }

        //<-- History Comments -->
        if (hasFieldPermisssion(ticket, "ticket.view.ticketHistory", "ticket.change.ticketHistory")) {
            table = new PdfPTable(4);
            table.setWidthPercentage(100);
            table.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.getDefaultCell().setBorderWidth(0);
            table.getDefaultCell().setPadding(4);
            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("global.comments"), SECTION_FONT));
            cell.setColspan(4);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setGrayFill(0.7f);
            table.addCell(cell);

            List<TicketHistory> tHistList = ticket.getTicketHistoryList();
            Phrase histPhrase;
            if ((tHistList != null) && (tHistList.size() > 0)) {
                for (TicketHistory tHist : tHistList) {
                    Date histDate = tHist.getCreatedDate();
                    User histUser = tHist.getUser();
                    String userName = histUser.getFirstName() + " " + histUser.getLastName();
                    String head = dtf.format(histDate) + " - " + userName + " - " + tHist.getSubject();
                    Paragraph testP = new Paragraph();
                    histPhrase = new Phrase(head, GRAY_FONT);
                    testP.add(histPhrase);
                    histPhrase = new Phrase("\n\n");
                    testP.add(histPhrase);
                    histPhrase = new Phrase(tHist.getNoteText(), TEXT_FONT);
                    testP.add(histPhrase);
                    cell = new PdfPCell(testP);
                    cell.setColspan(4);
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    table.addCell(cell);
                }
            } else {
                histPhrase = new Phrase(getMessageSourceAccessor().getMessage("ticket.noComments"), ITAL_FONT);
                cell = new PdfPCell(histPhrase);
                cell.setColspan(4);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
            }

            document.add(table);
            document.add(new Paragraph(" "));
        }

        //<-- ZEN Information -->
        if (hasFieldPermisssion(ticket, "ticket.view.zenAsset", "ticket.change.zenAsset")) {
            table = new PdfPTable(4);
            table.setWidthPercentage(100);
            table.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.getDefaultCell().setBorderWidth(0);
            table.getDefaultCell().setPadding(4);
            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.zenInformation"), SECTION_FONT));
            cell.setColspan(4);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setGrayFill(0.7f);
            table.addCell(cell);

            Set<ZenInformation> ziSet = ticket.getZenInformation();
            if (ziSet != null) {
                for (ZenInformation zinfo : ziSet) {
                    cell = new PdfPCell(new Phrase(zinfo.getDn(), TEXT_FONT));
                    cell.setColspan(4);
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    table.addCell(cell);
                }
            }

            document.add(table);
            document.add(new Paragraph(" "));
        }

        //<-- Ticket Survey Results -->
        if (hasFieldPermisssion(ticket, "ticket.view.surveyData")) {
            table = new PdfPTable(4);
            table.setWidthPercentage(100);
            table.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.getDefaultCell().setBorderWidth(0);
            table.getDefaultCell().setPadding(4);
            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.surveyResults"), SECTION_FONT));
            cell.setColspan(4);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setGrayFill(0.7f);
            table.addCell(cell);

            // Check to see if this ticket has a completed survey or not
            Boolean surveyCompleted = (Boolean) data.get("surveyCompleted");
            if (surveyCompleted) {
                List surveyDataList = (List) data.get("surveyItems");
                for (Object aSurveyDataList : surveyDataList) {
                    SurveyItem surveyItem = (SurveyItem) aSurveyDataList;
                    String value = surveyItem.getResponse();
                    String question = surveyItem.getQuestion();

                    cell = new PdfPCell(new Phrase(question, BOLD_FONT));
                    cell.setColspan(3);
                    table.addCell(cell);
                    cell = new PdfPCell(new Phrase(value, TEXT_FONT));
                    table.addCell(cell);

                }
            } else {
                cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.surveyNotSubmitted"), ITAL_FONT));
                cell.setColspan(4);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
            }

            document.add(table);
            document.add(new Paragraph(" "));
        }

        // Ticket Audit
        if (hasFieldPermisssion(ticket, "ticket.view.ticketAudit")) {
            table = new PdfPTable(5);
            table.setWidthPercentage(100);
            table.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.getDefaultCell().setBorderWidth(0);
            table.getDefaultCell().setPadding(4);
            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.auditTitle"), SECTION_FONT));
            cell.setColspan(5);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setGrayFill(0.7f);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.auditFieldName"), BOLD_FONT));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.auditFrom"), BOLD_FONT));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.auditTo"), BOLD_FONT));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.auditOn"), BOLD_FONT));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.auditBy"), BOLD_FONT));
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticket.auditCreated"), TEXT_FONT));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("", TEXT_FONT));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("", TEXT_FONT));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(createdate, TEXT_FONT));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase((ticket.getSubmittedBy() != null) ?  ticket.getSubmittedBy().getLoginId() : "", TEXT_FONT));
            table.addCell(cell);

            ticketService.initializeAuditList(ticket);
            List<TicketAudit> audits = ticket.getTicketAudit();
            for (TicketAudit audit : audits) {
                cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(audit.getProperty(), audit.getProperty()), TEXT_FONT));
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(audit.getBeforeValueDescription(), TEXT_FONT));
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(audit.getAfterValueDescription(), TEXT_FONT));
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(dtf.format(audit.getDateStamp()), TEXT_FONT));
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(audit.getUser().getLoginId(), TEXT_FONT));
                table.addCell(cell);
            }

            document.add(table);
            document.add(new Paragraph(" "));
        }
    }

    private boolean hasFieldPermisssion(Ticket ticket, String... permissionKeys) {
        Group group = ticket.getGroup();
        for (String permissionKey : permissionKeys) {
            if (permissionService.hasGroupPermission(permissionKey, group)) {
                return true;
            }
        }

        return false;
    }

    private CustomFieldValues getCustomFieldValue(Ticket ticket, CustomField cf) {
        CustomFieldValues cfv = null;
        if (ticket != null) {
            for (CustomFieldValues cfvFound : ticket.getCustomeFieldValues()) {
                if (cfvFound.getCustomField().getId().equals(cf.getId())) {
                    cfv = cfvFound;
                    break;
                }
            }
        }

        return cfv;
    }

    public void setLdapService(LdapService ldapService) {
        this.ldapService = ldapService;
    }

    public void setLdapFieldService(LdapFieldService ldapFieldService) {
        this.ldapFieldService = ldapFieldService;
    }

    public void setCustomFieldsService(CustomFieldsService customFieldsService) {
        this.customFieldsService = customFieldsService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    private static class MyPageEvents extends PdfPageEventHelper {

        private MessageSourceAccessor messageSourceAccessor;

        // This is the PdfContentByte object of the writer
        private PdfContentByte cb;

        // We will put the final number of pages in a template
        private PdfTemplate template;

        // This is the BaseFont we are going to use for the header / footer
        private BaseFont bf = null;

        public MyPageEvents(MessageSourceAccessor messageSourceAccessor) {
            this.messageSourceAccessor = messageSourceAccessor;
        }

        // we override the onOpenDocument method
        public void onOpenDocument(PdfWriter writer, Document document) {
            try {
                bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb = writer.getDirectContent();
                template = cb.createTemplate(50, 50);
            } catch (DocumentException de) {
                de.printStackTrace();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }

        // we override the onEndPage method
        public void onEndPage(PdfWriter writer, Document document) {
            int pageN = writer.getPageNumber();
            String text = messageSourceAccessor.getMessage("page", "page") + " " + pageN + " " +
                    messageSourceAccessor.getMessage("of", "of") + " ";

//            String text = messageSourceAccessor.getMessage("pagedList.pageNumOfNum", new Object[]{""+pageN });

            float len = bf.getWidthPoint(text, 8);
            cb.beginText();
            cb.setFontAndSize(bf, 8);

            cb.setTextMatrix(MARGIN, 16);
            cb.showText(text);
            cb.endText();

            cb.addTemplate(template, MARGIN + len, 16);
            cb.beginText();
            cb.setFontAndSize(bf, 8);

            cb.endText();
        }

        // we override the onCloseDocument method
        public void onCloseDocument(PdfWriter writer, Document document) {
            template.beginText();
            template.setFontAndSize(bf, 8);
            template.showText(String.valueOf(writer.getPageNumber() - 1));
            template.endText();
        }
    }
}
