package net.grouplink.ehelpdesk.web.propertyeditors;

import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.service.GroupService;
import org.apache.commons.lang.StringUtils;

import java.beans.PropertyEditorSupport;

public class GroupEditor extends PropertyEditorSupport {

    private GroupService groupService;

    /**
     * Constructor for use by derived PropertyEditor classes.
     */
    public GroupEditor() {
    }

    /**
     * Constructor for use by derived PropertyEditor classes.
     * @param groupService the category service
     */
    public GroupEditor(GroupService groupService) {
        this.groupService = groupService;
    }

    /**
     * Gets the property value as a string suitable for presentation
     * to a human to edit.
     *
     * @return The property value as a string suitable for presentation
     *         to a human to edit.
     *         <p>   Returns "null" is the value can't be expressed as a string.
     *         <p>   If a non-null value is returned, then the PropertyEditor should
     *         be prepared to parse that string back in setAsText().
     */
    public String getAsText() {
        Group group = (Group)getValue();
        return (group !=  null)?group.getId().toString():"";
    }

    /**
     * Sets the property value by parsing a given String.  May raise
     * java.lang.IllegalArgumentException if either the String is
     * badly formatted or if this kind of property can't be expressed
     * as text.
     *
     * @param text The string to be parsed.
     */
    public void setAsText(String text) throws IllegalArgumentException {
        if (StringUtils.isNotBlank(text)){
            setValue(groupService.getGroupById(new Integer(text)));
        } else {
            setValue(null);
        }
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

}
