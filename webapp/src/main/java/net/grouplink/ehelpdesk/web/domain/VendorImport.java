package net.grouplink.ehelpdesk.web.domain;

import org.apache.commons.collections.FactoryUtils;
import org.apache.commons.collections.map.LazyMap;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class VendorImport implements Serializable {
    private byte[] fileData;
    private String csvType;
    private boolean firstLineHeader;
    private Map importMap = LazyMap.decorate(new HashMap(), FactoryUtils.instantiateFactory(String.class));

    public byte[] getFileData() {
        return fileData;
    }

    public void setFileData(byte[] fileData) {
        this.fileData = fileData;
    }

    public String getCsvType() {
        return csvType;
    }

    public void setCsvType(String csvType) {
        this.csvType = csvType;
    }

    public boolean isFirstLineHeader() {
        return firstLineHeader;
    }

    public void setFirstLineHeader(boolean firstLineHeader) {
        this.firstLineHeader = firstLineHeader;
    }

    public Map getImportMap() {
        return importMap;
    }

    public void setImportMap(Map importMap) {
        this.importMap = importMap;
    }
}
