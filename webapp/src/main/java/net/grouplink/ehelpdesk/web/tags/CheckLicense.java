package net.grouplink.ehelpdesk.web.tags;

import net.grouplink.ehelpdesk.service.LicenseService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;
import java.util.Locale;

public class CheckLicense implements Tag {
    private final Log log = LogFactory.getLog(getClass());
    private PageContext pageContext;
    private Tag parentTag;

    public void setPageContext(PageContext pc) {
        this.pageContext = pc;
    }

    public void setParent(Tag t) {
        this.parentTag = t;
    }

    public Tag getParent() {
        return parentTag;
    }

    public int doStartTag() throws JspException {
        try {
            LicenseService licenseService = getLicenseService();
            if (licenseService.isMaintenanceExpired()) {
                MessageSource messageSource = getMessageSource();
                Locale locale = LocaleContextHolder.getLocale();
                StringBuilder sb = new StringBuilder();

                sb.append("<br><p class=\"message warning\">")
                        .append(messageSource.getMessage("licensing.maintenanceExpired", null, locale));
                pageContext.getOut().write(sb.toString());

            }
        } catch (Exception e) {
            log.error("error checking license", e);
        }
        return Tag.SKIP_BODY;
    }

    private LicenseService getLicenseService() {
        return (LicenseService) WebApplicationContextUtils.getWebApplicationContext(pageContext.getServletContext()).getBean("licenseService");
    }

    public MessageSource getMessageSource() {
        return (MessageSource) WebApplicationContextUtils.getWebApplicationContext(pageContext.getServletContext()).getBean("messageSource");
    }

    public int doEndTag() throws JspException {
        return Tag.EVAL_PAGE;
    }

    public void release() {
        this.pageContext = null;
        this.parentTag = null;
    }
}
