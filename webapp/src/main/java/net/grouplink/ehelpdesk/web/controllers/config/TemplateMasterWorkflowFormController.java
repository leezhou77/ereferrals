package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.domain.Status;
import net.grouplink.ehelpdesk.domain.TicketTemplate;
import net.grouplink.ehelpdesk.domain.TicketTemplateMaster;
import net.grouplink.ehelpdesk.domain.WorkflowStep;
import net.grouplink.ehelpdesk.service.StatusService;
import net.grouplink.ehelpdesk.service.TemplateMasterService;
import net.grouplink.ehelpdesk.service.TicketTemplateService;
import net.grouplink.ehelpdesk.service.WorkflowStepService;
import net.grouplink.ehelpdesk.service.WorkflowStepStatusService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TemplateMasterWorkflowFormController extends SimpleFormController {

    private TemplateMasterService templateMasterService;
    private StatusService statusService;
    private WorkflowStepService workflowStepService;
    private TicketTemplateService ticketTemplateService;
    private WorkflowStepStatusService workflowStepStatusService;

    public TemplateMasterWorkflowFormController() {
        setFormView("ticketTemplates/templateMasterWorkflowEdit");
        setCommandClass(TicketTemplateMaster.class);
        setCommandName("master");
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        Integer templateMasterId = ServletRequestUtils.getIntParameter(request, "tmId");
        TicketTemplateMaster master = templateMasterService.getById(templateMasterId);
        if (CollectionUtils.isEmpty(master.getWorkflowSteps())) {
            WorkflowStep ws = new WorkflowStep();
            ws.setStepNumber(1);
            master.addWorkflowStep(ws);
        }
        
        return master;
    }

    @Override
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
        TicketTemplateMaster master = (TicketTemplateMaster) command;
        Map<String, Object> model = new HashMap<String, Object>();
        List<Status> statuses = statusService.getStatus();
        model.put("statuses", statuses);

        Set<TicketTemplate> allTTs = master.getTicketTemplates();
        List<TicketTemplate> unusedTTs = new ArrayList<TicketTemplate>();
        for (TicketTemplate ticketTemplate : allTTs) {
            if (!ticketTemplateUsed(ticketTemplate, master.getWorkflowSteps())) {
                unusedTTs.add(ticketTemplate);
            }
        }

        Collections.sort(unusedTTs);

        Map<WorkflowStep, Boolean> launchedWorkflowSteps = new HashMap<WorkflowStep, Boolean>();
        for (WorkflowStep workflowStep : master.getWorkflowSteps()) {
            if (workflowStep.getId() != null && hasBeenLaunched(workflowStep)) {
                launchedWorkflowSteps.put(workflowStep, true);
            }
        }

        model.put("unusedTTs", unusedTTs);
        model.put("launchedWorkflowSteps", launchedWorkflowSteps);

        return model;
    }

    private boolean hasBeenLaunched(WorkflowStep workflowStep) {
        return workflowStepStatusService.getCountByWorkflowStep(workflowStep) > 0;
    }

    @Override
    protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
        TicketTemplateMaster master = (TicketTemplateMaster) command;

        @SuppressWarnings("unchecked")
        Map<String, String[]> paramMap = request.getParameterMap();
        
        // remove ticket templates for any workflow steps that were submitted
        List<WorkflowStep> workflowSteps = master.getWorkflowSteps();
        for (WorkflowStep step : workflowSteps) {
            if (!containsRequestParamForStep(paramMap, step)) {
                clearTicketTemplatesFromStep(step);
            }
        }

        // bind submitted ticket tempaltes to workflow steps
        for (Map.Entry<String, String[]> entry : paramMap.entrySet()) {
            String key = entry.getKey();
            if (key.startsWith("ws_")) {
                Integer wfsId = Integer.valueOf(key.split("_")[1]);
                WorkflowStep ws = workflowStepService.getById(wfsId);
                String[] values = entry.getValue();
                List<TicketTemplate> tts = new ArrayList<TicketTemplate>();
                for (String value : values) {
                    TicketTemplate tt = ticketTemplateService.getById(Integer.valueOf(value));
                    tts.add(tt);
                }

                // add any tts that are present in the list but not in the ws tt set
                for (TicketTemplate tt : tts) {
                    if (!ws.getTicketTemplates().contains(tt)) {
                        ws.addTicketTemplate(tt);
                    }
                }

                // remove any tts that are in the ws tt set but not in the list
                for (Iterator<TicketTemplate> iterator = ws.getTicketTemplates().iterator(); iterator.hasNext();) {
                    TicketTemplate tt = iterator.next();
                    if (!tts.contains(tt)) {
                        iterator.remove();
                    }
                }
            }
        }

        // bind workflow step completed status
        for (WorkflowStep step : workflowSteps) {
            String wsStatus = ServletRequestUtils.getStringParameter(request, "wsStatus_"+step.getId());
            if(StringUtils.isNotBlank(wsStatus)){
                step.setStepCompletedStatus(statusService.getStatusById(Integer.valueOf(wsStatus)));
            } else {
                step.setStepCompletedStatus(null);
            }
        }
    }

    private void clearTicketTemplatesFromStep(WorkflowStep step) {
        List<TicketTemplate> toDelete = new ArrayList<TicketTemplate>(step.getTicketTemplates());
        for (TicketTemplate ticketTemplate : toDelete) {
            step.getTicketTemplates().remove(ticketTemplate);
            ticketTemplate.setWorkflowStep(null);
        }
    }

    private boolean containsRequestParamForStep(Map<String, String[]> paramMap, WorkflowStep step) {
        return (paramMap.containsKey("ws_" + step.getId()));
    }

    private boolean ticketTemplateUsed(TicketTemplate ticketTemplate, List<WorkflowStep> workflowSteps) {
        for (WorkflowStep workflowStep : workflowSteps) {
            if (workflowStep.getTicketTemplates().contains(ticketTemplate)) {
                return true;
            }
        }

        return false;
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        TicketTemplateMaster master = (TicketTemplateMaster) command;
        String action = ServletRequestUtils.getStringParameter(request, "action");
        if (StringUtils.equals(action, "addStep")) {
            // add step
            WorkflowStep ws = new WorkflowStep();
            ws.setStepNumber(master.getWorkflowSteps().size() + 1);
            master.addWorkflowStep(ws);
            templateMasterService.save(master);
        } else if (StringUtils.equals(action, "deleteStep")) {
            // delete step
            Integer wsId = ServletRequestUtils.getIntParameter(request, "wsId");
            WorkflowStep ws = workflowStepService.getById(wsId);
            master.getWorkflowSteps().remove(ws);
            workflowStepService.delete(ws);
            // reindex steps
            for (int i=0; i<master.getWorkflowSteps().size(); i++) {
                WorkflowStep wfs = master.getWorkflowSteps().get(i);
                wfs.setStepNumber(i+1);
            }

            templateMasterService.save(master);
        } else {
            // just save
            templateMasterService.save(master);
        }

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("tmId", master.getId());
        model.put("wfTab", "true");

        response.setStatus(HttpServletResponse.SC_OK);

        request.setAttribute("flash.success", getMessageSourceAccessor().getMessage("ticketTemplate.workFlow.saveSuccesssful"));

        return null;
    }

    public void setTemplateMasterService(TemplateMasterService templateMasterService) {
        this.templateMasterService = templateMasterService;
    }

    public void setStatusService(StatusService statusService) {
        this.statusService = statusService;
    }

    public void setWorkflowStepService(WorkflowStepService workflowStepService) {
        this.workflowStepService = workflowStepService;
    }

    public void setTicketTemplateService(TicketTemplateService ticketTemplateService) {
        this.ticketTemplateService = ticketTemplateService;
    }

    public void setWorkflowStepStatusService(WorkflowStepStatusService workflowStepStatusService) {
        this.workflowStepStatusService = workflowStepStatusService;
    }
}
