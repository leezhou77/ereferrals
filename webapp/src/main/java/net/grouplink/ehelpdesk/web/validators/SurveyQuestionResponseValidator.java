package net.grouplink.ehelpdesk.web.validators;

import net.grouplink.ehelpdesk.domain.SurveyQuestionResponse;
import net.grouplink.ehelpdesk.service.SurveyQuestionResponseService;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Mar 25, 2008
 * Time: 6:58:56 PM
 */
public class SurveyQuestionResponseValidator implements Validator {

	private SurveyQuestionResponseService surveyQuestionResponseService;

    public boolean supports(Class clazz) {
		return clazz.equals(SurveyQuestionResponse.class);
	}

    public void validate(Object target, Errors errors) {
		SurveyQuestionResponse surveyQuestionResponse = (SurveyQuestionResponse) target;
        if (surveyQuestionResponseService.hasDuplicateSurveyQuestionResponseText(surveyQuestionResponse)) {
			errors.rejectValue("surveyQuestionResponseText", "surveys.validate.question.response.text.notunique", "Survey question response text already exists");
		}
        if(StringUtils.isBlank(surveyQuestionResponse.getSurveyQuestionResponseText())){
            errors.rejectValue("surveyQuestionResponseText", "surveys.validate.question.response.text.empty", "Survey question response text cannot be empty");
        }
    }

    public void setSurveyQuestionResponseService(SurveyQuestionResponseService surveyQuestionResponseService) {
        this.surveyQuestionResponseService = surveyQuestionResponseService;
    }
}
