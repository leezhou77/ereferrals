package net.grouplink.ehelpdesk.web.controllers.asset;

import net.grouplink.ehelpdesk.domain.asset.AssetType;
import net.grouplink.ehelpdesk.service.asset.AssetTypeService;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * User: jaymehafen
 * Date: Jul 30, 2008
 * Time: 1:46:04 PM
 */
public class AssetTypeFormController extends SimpleFormController {
    private AssetTypeService assetTypeService;

    public AssetTypeFormController() {
        setCommandClass(AssetType.class);
        setCommandName("assetType");
        setFormView("assetTypeEdit");
    }

    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        AssetType assetType;
        String assetTypeId = ServletRequestUtils.getStringParameter(request, "assetTypeId");
        if (StringUtils.isNotBlank(assetTypeId))
            assetType = assetTypeService.getById(new Integer(assetTypeId));
        else
            assetType = new AssetType();
        
        return assetType;
    }

    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        AssetType assetType = (AssetType) command;
        assetTypeService.saveAssetType(assetType);
        return new ModelAndView(new RedirectView("assetTypes.glml"));
    }

    public void setAssetTypeService(AssetTypeService assetTypeService) {
        this.assetTypeService = assetTypeService;
    }
}
