package net.grouplink.ehelpdesk.web.propertyeditors;

import net.grouplink.ehelpdesk.domain.Role;
import net.grouplink.ehelpdesk.service.RoleService;
import org.apache.commons.lang.StringUtils;

import java.beans.PropertyEditorSupport;
import java.util.NoSuchElementException;


public class RoleEditor extends PropertyEditorSupport {

    private RoleService roleService;

    public RoleEditor() {
    }
    
    public RoleEditor(RoleService roleService) {
    	setRoleService(roleService);
    }
    
    public String getAsText() {
        Role role = (Role)getValue();
        return (role !=  null)?role.getId().toString():"";
    }

    public void setAsText(String roleId) throws NoSuchElementException {
    	if (StringUtils.isNotBlank(roleId)) {
    		setValue(roleService.getRoleById(new Integer(roleId)));
    	} else {
            setValue(null);
        }
    }

    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }
}