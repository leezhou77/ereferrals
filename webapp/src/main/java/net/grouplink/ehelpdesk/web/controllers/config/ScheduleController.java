package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.domain.*;
import net.grouplink.ehelpdesk.service.*;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import net.grouplink.ehelpdesk.web.validators.ScheduleStatusValidator;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.*;

/**
 * author: zpearce
 * Date: 12/5/11
 * Time: 1:49 PM
 */
@Controller
@RequestMapping("/groups")
public class ScheduleController extends ApplicationObjectSupport {
    @Autowired private GroupService groupService;
    @Autowired private ScheduleStatusService scheduleStatusService;
    @Autowired private CategoryService categoryService;
    @Autowired private CategoryOptionService categoryOptionService;
    @Autowired private StatusService statusService;
    @Autowired private TicketPriorityService ticketPriorityService;
    @Autowired private PermissionService permissionService;

    @RequestMapping(method = RequestMethod.GET, value = "/schedules")
    public String groups(Model model) {
        List<Group> groupList = groupService.getGroups();
        Iterator<Group> iterator = groupList.iterator();
        while (iterator.hasNext()) {
            if(!permissionService.hasGroupPermission("group.scheduledTaskManagement", iterator.next())) {
                iterator.remove();
            }
        }
        Collections.sort(groupList);
        model.addAttribute("groupList", groupList);
        return "schedules/groups";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{group}/schedules/index")
    public String index(Model model, @PathVariable("group") Group group) {
        if(!permissionService.hasGroupPermission("group.scheduledTaskManagement", group)) {
            throw new AccessDeniedException("Access is denied");
        }

        List<ScheduleStatus> scheduleStatuses = scheduleStatusService.getScheduleStatusByGroupId(group.getId());
        model.addAttribute("scheduleList", scheduleStatuses);
        model.addAttribute(group);
        return "schedules/index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{group}/schedules/new")
    public String newForm(Model model, @PathVariable("group") Group group) {
        if(!permissionService.hasGroupPermission("group.scheduledTaskManagement", group)) {
            throw new AccessDeniedException("Access is denied");
        }

        ScheduleStatus scheduleStatus = new ScheduleStatus();
        scheduleStatus.setActionFrequencyOneTime(true);
        model.addAttribute("schedule", scheduleStatus);
        model.addAllAttributes(referenceData(group));
        return "schedules/form";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{group}/schedules")
    public String create(Model model,
                         @PathVariable("group") Group group,
                         @Valid @ModelAttribute("schedule") ScheduleStatus schedule,
                         BindingResult bindingResult,
                         HttpServletRequest request) {
        if(!permissionService.hasGroupPermission("group.scheduledTaskManagement", group)) {
            throw new AccessDeniedException("Access is denied");
        }

        model.addAllAttributes(referenceData(group));
        if(!bindingResult.hasErrors()) {
            schedule.setRunning(true);
            scheduleStatusService.saveScheduleStatus(schedule);
            request.setAttribute("flash.success", getMessageSourceAccessor().getMessage("scheduler.saved"));
        }
        return "schedules/form";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{group}/schedules/{id}/edit")
    public String edit(Model model, @PathVariable("group") Group group, @PathVariable("id") ScheduleStatus schedule) {
        if(!permissionService.hasGroupPermission("group.scheduledTaskManagement", group)) {
            throw new AccessDeniedException("Access is denied");
        }

        model.addAttribute("schedule", schedule);
        model.addAllAttributes(referenceData(group));
        return "schedules/form";
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{group}/schedules/{schedule}")
    public String update(Model model,
                         @PathVariable("group") Group group,
                         @Valid @ModelAttribute("schedule") ScheduleStatus schedule,
                         BindingResult bindingResult,
                         HttpServletRequest request) {
        if(!permissionService.hasGroupPermission("group.scheduledTaskManagement", group)) {
            throw new AccessDeniedException("Access is denied");
        }

        model.addAllAttributes(referenceData(group));
        if(!bindingResult.hasErrors()) {
            scheduleStatusService.saveScheduleStatus(schedule);
            request.setAttribute("flash.success", getMessageSourceAccessor().getMessage("scheduler.saved"));
        }
        return "schedules/form";
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/schedules/{schedule}")
    public void delete(@PathVariable("schedule") ScheduleStatus schedule, HttpServletResponse response) throws JSONException, IOException {
        if(!permissionService.hasGroupPermission("group.scheduledTaskManagement", schedule.getGroup())) {
            throw new AccessDeniedException("Access is denied");
        }

        scheduleStatusService.deleteScheduleStatus(schedule);
        JSONObject obj = new JSONObject();
        obj.put("success", true);
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);
        response.getWriter().write(obj.toString());
    }

    @RequestMapping(method = RequestMethod.POST, value = "/schedules/{schedule}/toggleRunning")
    public void toggleRunning(@PathVariable("schedule") ScheduleStatus schedule, HttpServletResponse response) throws JSONException, IOException {
        if(!permissionService.hasGroupPermission("group.scheduledTaskManagement", schedule.getGroup())) {
            throw new AccessDeniedException("Access is denied");
        }

        schedule.setRunning(!schedule.isRunning());
        scheduleStatusService.saveScheduleStatus(schedule);
        JSONObject obj = new JSONObject();
        obj.put("success", true);
        obj.put("running", schedule.isRunning());
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);
        response.getWriter().write(obj.toString());
    }

    private Map<String, Object> referenceData(Group group) {
        Map<String, Object> model = new HashMap<String, Object>();

        List<Category> categories = categoryService.getAllCategoriesByGroup(group);
        model.put("categories", categories);

        List<CategoryOption> categoryOptions = categoryOptionService.getCategoryOptionByGroupId(group.getId());
        model.put("categoryOptions", categoryOptions);

        List<Status> statusesNonClosed = statusService.getNonClosedStatus();
        model.put("statusesNonClosed", statusesNonClosed);

        List<Status> statusesAll = statusService.getStatus();
        model.put("statusesAll", statusesAll);

        List<TicketPriority> priorities = ticketPriorityService.getPriorities();
        model.put("priorities", priorities);

        return model;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        if(binder.getTarget() instanceof ScheduleStatus) {
            Validator validator = binder.getValidator();
            binder.setValidator(new ScheduleStatusValidator(validator));
        }
    }
}
