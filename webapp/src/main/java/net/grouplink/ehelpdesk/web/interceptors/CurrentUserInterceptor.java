package net.grouplink.ehelpdesk.web.interceptors;

import net.grouplink.ehelpdesk.common.utils.UserUtils;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.service.UserDetailsImpl;
import net.grouplink.ehelpdesk.service.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CurrentUserInterceptor implements HandlerInterceptor {
    private UserService userService;

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        if (UserUtils.getLoggedInUser() == null) {
            SecurityContext securityContext = SecurityContextHolder.getContext();
            if (securityContext != null) {
                Authentication authentication = securityContext.getAuthentication();
                if (authentication != null) {
                    if (authentication.getPrincipal() instanceof UserDetails) {
                        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
                        User user = userDetails.getUser();
                        user = userService.getUserById(user.getId());
                        UserUtils.setLoggedInUser(user);
                    }
                }
            }

        }

        return true;
    }

    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        UserUtils.unbindUser();
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
