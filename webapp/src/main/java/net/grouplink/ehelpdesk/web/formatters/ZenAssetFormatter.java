package net.grouplink.ehelpdesk.web.formatters;

import net.grouplink.ehelpdesk.domain.asset.ZenAsset;
import net.grouplink.ehelpdesk.service.asset.ZenAssetService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class ZenAssetFormatter implements Formatter<ZenAsset> {
    @Autowired
    private ZenAssetService zenAssetService;

    public ZenAsset parse(String text, Locale locale) throws ParseException {
        if (StringUtils.isNotBlank(text)) {
            return zenAssetService.getById(Integer.valueOf(text));
        } else {
            return null;
        }

    }

    public String print(ZenAsset object, Locale locale) {
        return object == null ? "" : object.getId().toString();
    }
}
