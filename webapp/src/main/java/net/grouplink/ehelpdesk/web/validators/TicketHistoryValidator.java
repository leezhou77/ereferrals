package net.grouplink.ehelpdesk.web.validators;

import net.grouplink.ehelpdesk.common.utils.Constants;
import net.grouplink.ehelpdesk.common.utils.PropertiesConstants;
import net.grouplink.ehelpdesk.domain.TicketHistory;
import net.grouplink.ehelpdesk.service.PropertiesService;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author mrollins
 * @version 1.0
 */
public class TicketHistoryValidator extends BaseValidator {
    private PropertiesService propertiesService;


    public boolean supports(Class clazz) {
        return clazz.equals(TicketHistory.class);
    }


    public void validate(Object target, Errors errors) {
        TicketHistory ticketHistory = (TicketHistory)target;

        // Validate comment type specific fields
        switch(ticketHistory.getCommentType()){
            case Constants.THISTTYPE_APPOINTMENT:
            	if (StringUtils.isNotBlank(ticketHistory.getRecipients())) {
                    if(!isValidEmailString(ticketHistory.getRecipients())){
                        errors.rejectValue("recipients", "mail.validation.address", "Invalid email address listed.");
                    }
            	}
                break;
            case Constants.THISTTYPE_COMMENT:
                break;
            case Constants.THISTTYPE_PHONEMADE:
                break;
            case Constants.THISTTYPE_PHONERECD:
                break;
            case Constants.THISTTYPE_TASK:
                break;
            default:
                break;
        }

        // One of either subject or not is required
        if(StringUtils.isBlank(ticketHistory.getSubject()) && StringUtils.isBlank(ticketHistory.getNote())){
            ValidationUtils.rejectIfEmpty(errors, "subject", "blank", "");
            ValidationUtils.rejectIfEmpty(errors, "note", "ticket.required.subjectOrNote", "Either Subject or Note field is required");
        }

        // validate attachment sizes
        List attchList = ticketHistory.getAttchmnt();
        for (Object anAttchList : attchList) {
            MultipartFile mpf = (MultipartFile) anAttchList;

            String maxVal = propertiesService.getPropertiesValueByName(PropertiesConstants.MAX_UPLOAD_SIZE);
            Integer maxUpload;
            if (StringUtils.isBlank(maxVal)) {
                maxUpload = 1;
                propertiesService.saveProperties(PropertiesConstants.MAX_UPLOAD_SIZE, String.valueOf(maxUpload));
            } else {
                maxUpload = Integer.valueOf(maxVal);
            }


            if (mpf.getSize() > (maxUpload * 1024 * 1024)) {
                errors.rejectValue("attchmnt", "ticket.attch.sizeLimit", "Attachment file size is too large");
            }
        }

        // Validate the email strings if they are not blank
        if (StringUtils.isNotBlank(ticketHistory.getRecipients())) {
            if(!isValidEmailString(ticketHistory.getRecipients())){
                errors.rejectValue("recipients", "mail.validation.address", "Invalid email address listed.");
            }
        }
        if (StringUtils.isNotBlank(ticketHistory.getBc())) {
            if(!isValidEmailString(ticketHistory.getBc())){
                errors.rejectValue("bc", "mail.validation.address", "Invalid email address listed.");
            }
        }
    }

    public void setPropertiesService(PropertiesService propertiesService) {
        this.propertiesService = propertiesService;
    }
}
