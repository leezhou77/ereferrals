package net.grouplink.ehelpdesk.web.validators;

import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.service.LocationService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class LocationValidator implements Validator {
    @Autowired
	private LocationService locationService;

    public boolean supports(Class clazz) {
		return clazz.equals(Location.class);
	}

    public void validate(Object target, Errors errors) {
		Location location = (Location) target;
        if(StringUtils.isBlank(location.getName())){
            errors.rejectValue("name", "ticketSettings.location.validate.noname", "Location name can not be empty");
        }
        if (locationService.hasDuplicateLocationName(location)) {
			errors.rejectValue("name", "ticketSettings.location.validate.notuniqueid", "Location name already exists");
		}
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }
}
