package net.grouplink.ehelpdesk.web.views;

import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.CustomFieldValues;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.domain.Status;
import net.grouplink.ehelpdesk.domain.SurveyData;
import net.grouplink.ehelpdesk.domain.SurveyQuestion;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketHistory;
import net.grouplink.ehelpdesk.domain.TicketPriority;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.ZenAsset;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilterCriteriaHelper;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilterOperators;
import net.grouplink.ehelpdesk.service.CategoryOptionService;
import net.grouplink.ehelpdesk.service.CategoryService;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LocationService;
import net.grouplink.ehelpdesk.service.StatusService;
import net.grouplink.ehelpdesk.service.SurveyQuestionService;
import net.grouplink.ehelpdesk.service.TicketPriorityService;
import net.grouplink.ehelpdesk.service.TicketService;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.asset.AssetService;
import net.grouplink.ehelpdesk.service.asset.ZenAssetService;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Date: Mar 31, 2010
 * Time: 5:57:24 PM
 */
public class TicketFilterListExcelView extends AbstractExcelView {

    private final Log log = LogFactory.getLog(TicketListExcelView.class);
    private final SimpleDateFormat filterDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private MessageSourceAccessor messages;

    private UserRoleGroupService userRoleGroupService;
    private CategoryService categoryService;
    private CategoryOptionService categoryOptionService;
    private UserService userService;
    private GroupService groupService;
    private LocationService locationService;
    private TicketPriorityService ticketPriorityService;
    private StatusService statusService;
    private SurveyQuestionService surveyQuestionService;
    private ZenAssetService zenAssetService;
    private TicketService ticketService;
    private AssetService assetService;

    @SuppressWarnings("unchecked")
    protected void buildExcelDocument(Map model, HSSFWorkbook workbook, HttpServletRequest request,
                                      HttpServletResponse response) throws Exception {

        TicketFilter ticketFilter = (TicketFilter) model.get("ticketFilter");
        List<Ticket> ticketList = (List<Ticket>) model.get("ticketList");
        Locale locale = RequestContextUtils.getLocale(request);

        messages = getMessageSourceAccessor();

        // Create a new sheet
        HSSFSheet summarySheet = workbook.createSheet(messages.getMessage("ticketList.summary"));

        // We simply put an error message on the first cell if no list is available
        // Nevertheless, it should never be null as the controller verify it.
        if (ticketList == null || ticketList.size() == 0) {
            HSSFRichTextString noListMsg = new HSSFRichTextString(messages.getMessage("ticketList.nolist"));
            getCell(summarySheet, 0, 0).setCellValue(noListMsg);
            return;
        }

        // Create a font for headers
        HSSFFont f = workbook.createFont();
        // set font 1 to 12 point type
        f.setFontHeightInPoints((short) 10);
        f.setColor(HSSFColor.BLACK.index);
        f.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

        // The same for properties data
        HSSFFont fp = workbook.createFont();
        fp.setColor((short) 0xc);
        HSSFCellStyle csp = workbook.createCellStyle();
        csp.setFont(fp);
        csp.setAlignment(HSSFCellStyle.ALIGN_CENTER);

        // We create a date time style
        HSSFCellStyle dateTimeStyle = workbook.createCellStyle();
        dateTimeStyle.setFont(fp);
        dateTimeStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        dateTimeStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy h:mm"));

        // We create a date style
        HSSFCellStyle dateStyle = workbook.createCellStyle();
        dateStyle.setFont(fp);
        dateStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        dateStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy"));

        // Set the colum width of the two first columns
        summarySheet.setColumnWidth((short) 0, (short) (20 * 256));
        summarySheet.setColumnWidth((short) 1, (short) (20 * 512));

        int row = 0;

        row = buildSummaryCells(ticketFilter, summarySheet, csp, dateTimeStyle, ticketList.size(), row);

        // Applied Filters
        buildAppliedFiltersCells(workbook, ticketFilter, summarySheet, csp, row, locale);

        // Create a second sheet for the tickets
        buildTicketListSheet(workbook, ticketFilter, ticketList, f, dateTimeStyle, dateStyle);
    }

    private int buildSummaryCells(TicketFilter ticketFilter, HSSFSheet sheet, HSSFCellStyle csp, HSSFCellStyle dateStyle,
                                  Integer ticketCount, int row) {

        getCell(sheet, row, 0).setCellValue(new HSSFRichTextString(messages.getMessage("ticketList.dateExported")));
        getCell(sheet, row, 1).setCellValue(new Date());
        getCell(sheet, row, 1).setCellStyle(dateStyle);
        row++;

        getCell(sheet, row, 0).setCellValue(new HSSFRichTextString(messages.getMessage("ticketList.nbTickets")));
        getCell(sheet, row, 1).setCellValue(ticketCount);
        getCell(sheet, row, 1).setCellStyle(csp);
        row++;

        getCell(sheet, row, 0).setCellValue(new HSSFRichTextString(messages.getMessage("ticketFilter.title")));
        getCell(sheet, row, 1).setCellValue(new HSSFRichTextString(ticketFilter.getName()));
        getCell(sheet, row, 1).setCellStyle(csp);
        row++;

        row += 2;
        return row;
    }

    private void buildAppliedFiltersCells(HSSFWorkbook workbook, TicketFilter ticketFilter, HSSFSheet sheet,
                                          HSSFCellStyle csp, int row, Locale locale) {

        getCell(sheet, row, 0).setCellValue(new HSSFRichTextString(messages.getMessage("ticketList.filters")));
        row++;

        HSSFCellStyle fsp = workbook.createCellStyle();
        fsp.setAlignment(HSSFCellStyle.ALIGN_RIGHT);

        DateFormat df = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT, locale);

        // Assigned To
        if (!ArrayUtils.isEmpty(ticketFilter.getAssignedToIds())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < ticketFilter.getAssignedToIds().length; i++) {
                // Get associated operator
                Integer operator = ticketFilter.getAssignedToIdsOps()[i];
                String opMsgCode = TicketFilterOperators.getMessageCode(operator);
                String opMessage = getMessageSourceAccessor().getMessage(opMsgCode, "Equals");

                Integer integer = ticketFilter.getAssignedToIds()[i];
                UserRoleGroup urg = userRoleGroupService.getUserRoleGroupById(integer);
                if (sb.length() > 0) sb.append(", ");
                if (urg.getUserRole() != null) {
                    sb.append(opMessage).append(" - ").append(urg.getUserRole().getUser().getFirstName()).append(" ")
                            .append(urg.getUserRole().getUser().getLastName());
                } else {
                    sb.append(opMessage).append(" - ").append(urg.getGroup().getName());
                }
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_ASSIGNED, sb.toString());
            row++;
        }

        // Category
        if (!ArrayUtils.isEmpty(ticketFilter.getCategoryIds())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < ticketFilter.getCategoryIds().length; i++) {
                // Get associated operator
                Integer operator = ticketFilter.getCategoryIdsOps()[i];
                String opMsgCode = TicketFilterOperators.getMessageCode(operator);
                String opMessage = getMessageSourceAccessor().getMessage(opMsgCode, "Equals");

                Integer integer = ticketFilter.getCategoryIds()[i];
                Category cat = categoryService.getCategoryById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(opMessage).append(" - ").append(cat.getName());
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_CATEGORY, sb.toString());
            row++;
        }

        // Category Option
        if (!ArrayUtils.isEmpty(ticketFilter.getCategoryOptionIds())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < ticketFilter.getCategoryOptionIds().length; i++) {
                // Get associated operator
                Integer operator = ticketFilter.getCategoryOptionIdsOps()[i];
                String opMsgCode = TicketFilterOperators.getMessageCode(operator);
                String opMessage = getMessageSourceAccessor().getMessage(opMsgCode, "Equals");

                Integer integer = ticketFilter.getCategoryOptionIds()[i];
                CategoryOption catOpt = categoryOptionService.getCategoryOptionById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(opMessage).append(" - ").append(catOpt.getName());
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_CATOPT, sb.toString());
            row++;
        }

        // Contact
        if (!ArrayUtils.isEmpty(ticketFilter.getContactIds())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < ticketFilter.getContactIds().length; i++) {
                // Get associated operator
                Integer operator = ticketFilter.getContactIdsOps()[i];
                String opMsgCode = TicketFilterOperators.getMessageCode(operator);
                String opMessage = getMessageSourceAccessor().getMessage(opMsgCode, "Equals");

                Integer integer = ticketFilter.getContactIds()[i];
                User u = userService.getUserById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(opMessage).append(" - ").append(u.getFirstName()).append(" ").append(u.getLastName());
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_CONTACT, sb.toString());
            row++;
        }

        // Created Date
        if (ticketFilter.getCreatedDateOperator() != null &&
                !ticketFilter.getCreatedDateOperator().equals(TicketFilterOperators.DATE_ANY)) {

            String s = getDateFilterDescription(df, ticketFilter.getCreatedDateOperator(), ticketFilter.getCreatedDate(),
                                                ticketFilter.getCreatedDate2(), ticketFilter.getCreatedDateLastX());
            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_CREATED, s);
            row++;
        }


        // Estimated Date
        if (ticketFilter.getEstimatedDateOperator() != null &&
                !ticketFilter.getEstimatedDateOperator().equals(TicketFilterOperators.DATE_ANY)) {

            String s = getDateFilterDescription(df, ticketFilter.getEstimatedDateOperator(), ticketFilter.getEstimatedDate(),
                                                ticketFilter.getEstimatedDate2(), ticketFilter.getEstimatedDateLastX());
            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_ESTIMATED, s);
            row++;
        }

        // Group
        if (!ArrayUtils.isEmpty(ticketFilter.getGroupIds())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < ticketFilter.getGroupIds().length; i++) {
                // Get associated operator
                Integer operator = ticketFilter.getGroupIdsOps()[i];
                String opMsgCode = TicketFilterOperators.getMessageCode(operator);
                String opMessage = getMessageSourceAccessor().getMessage(opMsgCode, "Equals");

                Integer integer = ticketFilter.getGroupIds()[i];
                Group g = groupService.getGroupById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(opMessage).append(" - ").append(g.getName());
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_GROUP, sb.toString());
            row++;
        }

        // Location
        if (!ArrayUtils.isEmpty(ticketFilter.getLocationIds())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < ticketFilter.getLocationIds().length; i++) {
                // Get associated operator
                Integer operator = ticketFilter.getLocationIdsOps()[i];
                String opMsgCode = TicketFilterOperators.getMessageCode(operator);
                String opMessage = getMessageSourceAccessor().getMessage(opMsgCode, "Equals");

                Integer integer = ticketFilter.getLocationIds()[i];
                Location loc = locationService.getLocationById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(opMessage).append(" - ").append(loc.getName());
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_LOCATION, sb.toString());
            row++;
        }

        // Modified Date
        if (ticketFilter.getModifiedDateOperator() != null &&
                !ticketFilter.getModifiedDateOperator().equals(TicketFilterOperators.DATE_ANY)) {

            String s = getDateFilterDescription(df, ticketFilter.getModifiedDateOperator(), ticketFilter.getModifiedDate(),
                                                ticketFilter.getModifiedDate2(), ticketFilter.getModifiedDateLastX());
            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_MODIFIED, s);
            row++;
        }

        // Note
        if (!ArrayUtils.isEmpty(ticketFilter.getNote())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < ticketFilter.getNote().length; i++) {
                // Get associated operator
                Integer operator = ticketFilter.getNoteOps()[i];
                String opMsgCode = TicketFilterOperators.getMessageCode(operator);
                String opMessage = getMessageSourceAccessor().getMessage(opMsgCode, "Equals");

                String s = ticketFilter.getNote()[i];
                if (sb.length() > 0) sb.append(", ");
                sb.append(opMessage).append(" - ").append(s);
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_NOTE, sb.toString());
            row++;
        }

        // Priority
        if (!ArrayUtils.isEmpty(ticketFilter.getPriorityIds())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < ticketFilter.getPriorityIds().length; i++) {
                // Get associated operator
                Integer operator = ticketFilter.getPriorityIdsOps()[i];
                String opMsgCode = TicketFilterOperators.getMessageCode(operator);
                String opMessage = getMessageSourceAccessor().getMessage(opMsgCode, "Equals");

                Integer integer = ticketFilter.getPriorityIds()[i];
                TicketPriority p = ticketPriorityService.getPriorityById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(opMessage).append(" - ").append(p.getName());
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_PRIORITY, sb.toString());
            row++;
        }

        // Status
        if (!ArrayUtils.isEmpty(ticketFilter.getStatusIds())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < ticketFilter.getStatusIds().length; i++) {
                // Get associated operator
                Integer operator = ticketFilter.getStatusIdsOps()[i];
                String opMsgCode = TicketFilterOperators.getMessageCode(operator);
                String opMessage = getMessageSourceAccessor().getMessage(opMsgCode, "Equals");

                Integer integer = ticketFilter.getStatusIds()[i];
                Status status = statusService.getStatusById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(opMessage).append(" - ").append(status.getName());
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_STATUS, sb.toString());
            row++;
        }

        // Subject
        if (!ArrayUtils.isEmpty(ticketFilter.getSubject())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < ticketFilter.getSubject().length; i++) {
                // Get associated operator
                Integer operator = ticketFilter.getSubjectOps()[i];
                String opMsgCode = TicketFilterOperators.getMessageCode(operator);
                String opMessage = getMessageSourceAccessor().getMessage(opMsgCode, "Equals");

                String s = ticketFilter.getSubject()[i];
                if (sb.length() > 0) sb.append(", ");
                sb.append(opMessage).append(" - ").append(s);
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_SUBJECT, sb.toString());
            row++;
        }

        // Submitted By
        if (!ArrayUtils.isEmpty(ticketFilter.getSubmittedByIds())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < ticketFilter.getSubmittedByIds().length; i++) {
                // Get associated operator
                Integer operator = ticketFilter.getSubmittedByIdsOps()[i];
                String opMsgCode = TicketFilterOperators.getMessageCode(operator);
                String opMessage = getMessageSourceAccessor().getMessage(opMsgCode, "Equals");

                Integer integer = ticketFilter.getSubmittedByIds()[i];
                User u = userService.getUserById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(opMessage).append(" - ").append(u.getFirstName()).append(" ").append(u.getLastName());
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_SUBMITTED, sb.toString());
            row++;
        }

        // Ticket Number
        if (!ArrayUtils.isEmpty(ticketFilter.getTicketNumbers())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < ticketFilter.getTicketNumbers().length; i++) {
                // Get associated operator
                Integer operator = ticketFilter.getTicketNumbersOps()[i];
                String opMsgCode = TicketFilterOperators.getMessageCode(operator);
                String opMessage = getMessageSourceAccessor().getMessage(opMsgCode, "Equals");

                Integer integer = ticketFilter.getTicketNumbers()[i];
                if (sb.length() > 0) sb.append(", ");
                sb.append(opMessage).append(" - ").append(integer);
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_TICKETNUMBER, sb.toString());
            row++;
        }

        // Work Time
        if (StringUtils.isNotBlank(ticketFilter.getWorkTimeOperator())) {
            StringBuilder sb = new StringBuilder();
            if (ticketFilter.getWorkTimeOperator().equals("more")) {
                sb.append(messages.getMessage("ticketSearch.worktime.more")).append(" ");
            } else if (ticketFilter.getWorkTimeOperator().equals("less")) {
                sb.append(messages.getMessage("ticketSearch.worktime.less")).append(" ");
            } else {
                sb.append("unknown work time operator! ");
            }

            if (StringUtils.isNotBlank(ticketFilter.getWorkTimeHours())) {
                sb.append(ticketFilter.getWorkTimeHours()).append(" ")
                        .append(messages.getMessage("scheduler.time.hour")).append(" ");
            }

            if (StringUtils.isNotBlank(ticketFilter.getWorkTimeMinutes())) {
                sb.append(ticketFilter.getWorkTimeMinutes()).append(" ")
                        .append(messages.getMessage("scheduler.time.mins"));
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_WORKTIME, sb.toString());
            row++;
        }

        // Surveys
        String[] qids = ticketFilter.getSurveyQuestionId();
        String[] vals = ticketFilter.getSurveyQuestionValue();
        if ((qids != null) && (vals != null)) {
            for (int i = 0; i < qids.length; i++) {
                SurveyQuestion sq = surveyQuestionService.getSurveyQuestionById(new Integer(qids[i]));
                String qText = sq.getSurveyQuestionText();
                buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_SURVEYS, qText);
                if (i < qids.length - 1) row++;
            }
        }

        // zenAssetIds
        if (!ArrayUtils.isEmpty(ticketFilter.getZenAssetIds())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < ticketFilter.getZenAssetIds().length; i++) {
                Integer integer = ticketFilter.getZenAssetIds()[i];
                ZenAsset za = zenAssetService.getById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(za.getAssetName());
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_ZENWORKS10ASSET, sb.toString());
            row++;
        }

         // assetIds
        if (!ArrayUtils.isEmpty(ticketFilter.getInternalAssetIds())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < ticketFilter.getInternalAssetIds().length; i++) {
                Integer integer = ticketFilter.getInternalAssetIds()[i];
                Asset asset = assetService.getAssetById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(asset.getName());
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_INTERNALASSET, sb.toString());
            row++;
        }

        // Custom Fields
        if (MapUtils.isNotEmpty(ticketFilter.getCustomFields())) {
            for (Map.Entry<String, List<TicketFilterCriteriaHelper>> cfMapEntry : ticketFilter.getCustomFields().entrySet()) {

                StringBuilder sb = new StringBuilder();
                for (TicketFilterCriteriaHelper ticketFilterCFHelper : cfMapEntry.getValue()) {
                    // Get associated operator
                    Integer operator = ticketFilterCFHelper.getOperatorId();
                    String opMsgCode = TicketFilterOperators.getMessageCode(operator);
                    String opMessage = getMessageSourceAccessor().getMessage(opMsgCode, "Equals");

                    if (sb.length() > 0) sb.append(", ");
                    sb.append(opMessage).append(" - ").append(ticketFilterCFHelper.getValue());
                }

                buildAppliedFilterCell(sheet, row, fsp, csp, cfMapEntry.getKey(), sb.toString());
                row++;
            }
        }

    }

    private void buildAppliedFilterCell(HSSFSheet sheet, int row, HSSFCellStyle codeStyle,
                                        HSSFCellStyle descriptionStyle, String code, String description) {
        getCell(sheet, row, 0).setCellValue(new HSSFRichTextString(messages.getMessage(code, code)));
        getCell(sheet, row, 0).setCellStyle(codeStyle);
        getCell(sheet, row, 1).setCellValue(new HSSFRichTextString(description));
        getCell(sheet, row, 1).setCellStyle(descriptionStyle);
    }

    private void buildTicketListSheet(HSSFWorkbook workbook, TicketFilter ticketFilter, List<Ticket> ticketList,
                                      HSSFFont f, HSSFCellStyle dateTimeStyle, HSSFCellStyle dateStyle) {

        HSSFSheet sheet = workbook.createSheet(messages.getMessage("ticketList.title"));
        int row = 0;

        // Create a style for headers
        HSSFCellStyle cs = workbook.createCellStyle();
        cs.setFont(f);
        cs.setAlignment(HSSFCellStyle.ALIGN_CENTER);

        // Column Headers
        String[] colHeads = getColumnHeadings(ticketFilter);
        HSSFCell cell;
        for (int i = 0; i < colHeads.length; i++) {
            cell = getCell(sheet, row, i);
            cell.setCellStyle(cs);
            cell.setCellValue(new HSSFRichTextString(colHeads[i]));
        }

        row++;

        String[] columns = ticketFilter.getColumnOrder();

        // Process the ticket list
        for (Ticket ticket : ticketList) {

            for (int i = 0; i < columns.length; i++) {
                String column = columns[i];

                if (column.equals(TicketFilter.COLUMN_PRIORITY)) {
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticket.getPriority().getName()));
                } else if (column.equals(TicketFilter.COLUMN_TICKETNUMBER)) {
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticket.getTicketId().toString()));
                } else if (column.equals(TicketFilter.COLUMN_LOCATION)) {
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticket.getLocation().getName()));
                } else if (column.equals(TicketFilter.COLUMN_SUBJECT)) {
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticket.getSubject()));
                } else if (column.equals(TicketFilter.COLUMN_NOTE)) {
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString((ticket.getNote() == null) ? "" : ticket.getNote()));
                } else if (column.equals(TicketFilter.COLUMN_CATOPT)) {
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticket.getCategoryOption() == null ? "" :
                            ticket.getCategoryOption().getName()));
                } else if (column.equals(TicketFilter.COLUMN_ASSIGNED)) {
                    UserRoleGroup assignedToUrg = ticket.getAssignedTo();
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(assignedToUrg.getUserRole() == null ?
                            assignedToUrg.getGroup().getName() : assignedToUrg.getUserRole().getUser().getFirstName() +
                            " " + ticket.getAssignedTo().getUserRole().getUser().getLastName()));
                } else if (column.equals(TicketFilter.COLUMN_STATUS)) {
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticket.getStatus().getName()));
                } else if (column.equals(TicketFilter.COLUMN_CREATED)) {
                    getCell(sheet, row, i).setCellValue(ticket.getCreatedDate());
                    getCell(sheet, row, i).setCellStyle(dateTimeStyle);
                } else if (column.equals(TicketFilter.COLUMN_MODIFIED)) {
                    getCell(sheet, row, i).setCellValue(ticket.getModifiedDate());
                    getCell(sheet, row, i).setCellStyle(dateTimeStyle);
                } else if (column.equals(TicketFilter.COLUMN_ESTIMATED)) {
                    if (ticket.getEstimatedDate() != null) getCell(sheet, row, i).setCellValue(ticket.getEstimatedDate());
                    getCell(sheet, row, i).setCellStyle(dateStyle);
                } else if (column.equals(TicketFilter.COLUMN_CONTACT)) {
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticket.getContact().getFirstName() + " " +
                            ticket.getContact().getLastName()));
                } else if (column.equals(TicketFilter.COLUMN_SUBMITTED)) {
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticket.getSubmittedBy().getFirstName() + " " +
                            ticket.getSubmittedBy().getLastName()));
                } else if (column.equals(TicketFilter.COLUMN_GROUP)) {
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticket.getGroup().getName()));
                } else if (column.equals(TicketFilter.COLUMN_CATEGORY)) {
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticket.getCategory() == null ? "" : ticket.getCategory().getName()));
                } else if (column.equals(TicketFilter.COLUMN_WORKTIME)) {
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticket.getWorkTime() == null ?
                            "" : ticket.getWorkTime().toString()));
                } else if (column.equals(TicketFilter.COLUMN_SURVEYS)) {
                    String[] vals = ticketFilter.getSurveyQuestionValue();
                    for (int x = 0; x < vals.length; x++) {
                        getCell(sheet, row, i+x).setCellValue(new HSSFRichTextString(vals[x]));
                    }
                } else if (column.equals(TicketFilter.COLUMN_ZENWORKS10ASSET)) {
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticket.getZenAsset() == null ? "" : ticket.getZenAsset().getAssetName()));
                } else if (column.equals(TicketFilter.COLUMN_INTERNALASSET)) {
                    StringBuilder sb = new StringBuilder();
                    if (ticket.getAsset() != null) {
                        if (ticket.getAsset().getAssetNumber() != null) sb.append(ticket.getAsset().getAssetNumber());
                        if (ticket.getAsset().getName() != null) sb.append(" ").append(ticket.getAsset().getName());
                    } else {
                        sb.append("");
                    }
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(sb.toString()));
                } else if (column.equals(TicketFilter.COLUMN_HISTORYSUBJECT)) {
                    TicketHistory ticketHistory = ticketService.getMostRecentTicketHistory(ticket);
                    if (ticketHistory == null) {
                        getCell(sheet, row, i).setCellValue(new HSSFRichTextString(""));
                    } else {
                        getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticketHistory.getSubject() == null ? "" : ticketHistory.getSubject()));
                    }
                } else if (column.equals(TicketFilter.COLUMN_HISTORYNOTE)) {
                    TicketHistory ticketHistory = ticketService.getMostRecentTicketHistory(ticket);
                    if (ticketHistory == null) {
                        getCell(sheet, row, i).setCellValue(new HSSFRichTextString(""));
                    } else {
                        getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticketHistory.getNote() == null ? "" : ticketHistory.getNote()));
                    }
                } else if (column.startsWith(TicketFilter.COLUMN_CUSTOMFIELDPREFIX)) {
                    CustomFieldValues cfv = ticketService.getCustomFieldValue(ticket, StringUtils.removeStart(column, TicketFilter.COLUMN_CUSTOMFIELDPREFIX));
                    if (cfv == null) {
                        getCell(sheet, row, i).setCellValue(new HSSFRichTextString(""));
                    } else {
                        getCell(sheet, row, i).setCellValue(new HSSFRichTextString(cfv.getFieldValue() == null ? "" : cfv.getFieldValue()));
                    }
                } else if (column.startsWith(TicketFilter.COLUMN_SURVEYPREFIX)) {
                    SurveyData sd = ticketService.getSurveyData(ticket, StringUtils.removeStart(column, TicketFilter.COLUMN_SURVEYPREFIX));
                    if (sd == null) {
                        getCell(sheet, row, i).setCellValue(new HSSFRichTextString(""));
                    } else {
                        getCell(sheet, row, i).setCellValue(new HSSFRichTextString(sd.getValue() == null ? "" : sd.getValue()));
                    }
                }
            }
            row++;
        }
    }

    private String[] getColumnHeadings(TicketFilter ticketFilter) {
        String[] columns = ticketFilter.getColumnOrder();
        String[] colHeads = new String[columns.length];
        for (int i = 0; i < columns.length; i++) {
            String column = columns[i];
            if (column.startsWith(TicketFilter.COLUMN_CUSTOMFIELDPREFIX)) {
                colHeads[i] = StringUtils.removeStart(column, TicketFilter.COLUMN_CUSTOMFIELDPREFIX);
            } else if (column.startsWith(TicketFilter.COLUMN_SURVEYPREFIX)) {
                colHeads[i] = StringUtils.removeStart(column,  TicketFilter.COLUMN_SURVEYPREFIX);
            } else {
                colHeads[i] = getMessageSourceAccessor().getMessage(column);
            }
        }
        return colHeads;
    }

    private String getDateFilterDescription(DateFormat df, Integer dateOperator, String date1, String date2, Integer lastX) {
        StringBuilder sb = new StringBuilder();
        if (dateOperator.equals(TicketFilterOperators.DATE_TODAY)) {
            sb.append(getMessageSourceAccessor().getMessage("date.today"));
        } else if (dateOperator.equals(TicketFilterOperators.DATE_WEEK)) {
            sb.append(getMessageSourceAccessor().getMessage("date.thisweek"));
        } else if (dateOperator.equals(TicketFilterOperators.DATE_MONTH)) {
            sb.append(getMessageSourceAccessor().getMessage("date.thismonth"));
        } else if (dateOperator.equals(TicketFilterOperators.DATE_ON)) {
            Date d1 = parseDate(date1);
            sb.append(getMessageSourceAccessor().getMessage("date.on")).append(" ").append(df.format(d1));
        } else if (dateOperator.equals(TicketFilterOperators.DATE_BEFORE)) {
            Date d1 = parseDate(date1);
            sb.append(getMessageSourceAccessor().getMessage("date.before")).append(" ").append(df.format(d1));
        } else if (dateOperator.equals(TicketFilterOperators.DATE_AFTER)) {
            Date d1 = parseDate(date1);
            sb.append(getMessageSourceAccessor().getMessage("date.after")).append(" ").append(df.format(d1));
        } else if (dateOperator.equals(TicketFilterOperators.DATE_RANGE)) {
            Date d1 = parseDate(date1);
            Date d2 = parseDate(date2);
            sb.append(getMessageSourceAccessor().getMessage("date.range")).append(" ").append(df.format(d1))
                    .append(" - ").append(df.format(d2));
        } else if(dateOperator.equals(TicketFilterOperators.DATE_LAST_X_HOURS)){
            sb.append(getMessageSourceAccessor().getMessage("date.lastx.hours")).append(" ").append(lastX);
        } else if(dateOperator.equals(TicketFilterOperators.DATE_LAST_X_DAYS)){
            sb.append(getMessageSourceAccessor().getMessage("date.lastx.days")).append(" ").append(lastX);
        } else if(dateOperator.equals(TicketFilterOperators.DATE_LAST_X_WEEKS)){
            sb.append(getMessageSourceAccessor().getMessage("date.lastx.weeks")).append(" ").append(lastX);
        } else if(dateOperator.equals(TicketFilterOperators.DATE_LAST_X_MONTHS)){
            sb.append(getMessageSourceAccessor().getMessage("date.lastx.months")).append(" ").append(lastX);
        } else if(dateOperator.equals(TicketFilterOperators.DATE_LAST_X_YEARS)){
            sb.append(getMessageSourceAccessor().getMessage("date.lastx.years")).append(" ").append(lastX);
        }
        return sb.toString();
    }

    private Date parseDate(String date1) {
        Date selectedDate;
        try {
            selectedDate = filterDateFormat.parse(date1);
        } catch (ParseException e) {
            log.error("error parsing date: " + date1, e);
            throw new RuntimeException(e);
        }

        return selectedDate;
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setCategoryOptionService(CategoryOptionService categoryOptionService) {
        this.categoryOptionService = categoryOptionService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

    public void setTicketPriorityService(TicketPriorityService ticketPriorityService) {
        this.ticketPriorityService = ticketPriorityService;
    }

    public void setStatusService(StatusService statusService) {
        this.statusService = statusService;
    }

    public void setSurveyQuestionService(SurveyQuestionService surveyQuestionService) {
        this.surveyQuestionService = surveyQuestionService;
    }

    public void setZenAssetService(ZenAssetService zenAssetService) {
        this.zenAssetService = zenAssetService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }
}
