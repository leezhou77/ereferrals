package net.grouplink.ehelpdesk.web.domain;

import java.util.Date;

/**
 * @author mrollins
 * @version 1.0
 */
public class License {
    private String description;
    private Date expirationDate;
    private Integer groupCount;
    private Integer technicianCount;
    private Integer wfParticipantCount;
    private byte[] fileData;
    private String edition;
    private Date upgradeDate;
    private boolean dashboards;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Integer getTechnicianCount() {
        return technicianCount;
    }

    public void setTechnicianCount(Integer technicianCount) {
        this.technicianCount = technicianCount;
    }

    public byte[] getFileData() {
        return fileData;
    }

    public void setFileData(byte[] fileData) {
        this.fileData = fileData;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public Date getUpgradeDate() {
        return upgradeDate;
    }

    public void setUpgradeDate(Date upgradeDate) {
        this.upgradeDate = upgradeDate;
    }

    public Integer getGroupCount() {
        return groupCount;
    }

    public void setGroupCount(Integer groupCount) {
        this.groupCount = groupCount;
    }

    public boolean isDashboards() {
        return dashboards;
    }

    public void setDashboards(boolean dashboards) {
        this.dashboards = dashboards;
    }

    public Integer getWfParticipantCount() {
        return wfParticipantCount;
    }

    public void setWfParticipantCount(Integer wfParticipantCount) {
        this.wfParticipantCount = wfParticipantCount;
    }
}
