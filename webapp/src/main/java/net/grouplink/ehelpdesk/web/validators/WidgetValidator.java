package net.grouplink.ehelpdesk.web.validators;

import net.grouplink.ehelpdesk.domain.dashboard.Widget;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

public class WidgetValidator extends BaseValidator {

    public boolean supports(Class clazz) {
        return clazz.equals(Widget.class);
    }

    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "name", "global.fieldRequired", "This field cannot be empty");
        ValidationUtils.rejectIfEmpty(errors, "report", "global.fieldRequired", "This field cannot be empty");
        ValidationUtils.rejectIfEmpty(errors, "row", "global.fieldRequired", "This field cannot be empty");
        ValidationUtils.rejectIfEmpty(errors, "column", "global.fieldRequired", "This field cannot be empty");
        ValidationUtils.rejectIfEmpty(errors, "type", "global.fieldRequired", "This field cannot be empty");
    }
}
