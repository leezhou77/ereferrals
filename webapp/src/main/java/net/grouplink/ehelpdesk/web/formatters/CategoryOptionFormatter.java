package net.grouplink.ehelpdesk.web.formatters;

import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.service.CategoryOptionService;
import org.codehaus.plexus.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class CategoryOptionFormatter implements Formatter<CategoryOption> {
    @Autowired
    private CategoryOptionService categoryOptionService;

    public CategoryOption parse(String id, Locale locale) throws ParseException {
        if (StringUtils.isNotBlank(id)) {
            return categoryOptionService.getCategoryOptionById(Integer.parseInt(id));
        } else {
            return null;
        }
    }

    public String print(CategoryOption categoryOption, Locale locale) {
        return categoryOption == null ? "" : categoryOption.getId().toString();
    }
}
