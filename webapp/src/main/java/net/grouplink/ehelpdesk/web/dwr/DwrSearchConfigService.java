package net.grouplink.ehelpdesk.web.dwr;

import net.grouplink.ehelpdesk.service.TicketFullTextSearchService;

/**
 * Author: zpearce
 * Date: 4/27/11
 * Time: 11:22 AM
 */
public class DwrSearchConfigService {
    private TicketFullTextSearchService ticketFullTextSearchService;

    public void reindexTickets() {
        ticketFullTextSearchService.reindexTickets();
    }

    public int getReindexStatusPercent() {
        return ticketFullTextSearchService.getReindexStatusPercent();
    }

    public void setTicketFullTextSearchService(TicketFullTextSearchService ticketFullTextSearchService) {
        this.ticketFullTextSearchService = ticketFullTextSearchService;
    }
}
