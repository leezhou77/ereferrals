package net.grouplink.ehelpdesk.web.tags;

import net.grouplink.ehelpdesk.service.UpdateCheckerService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;
import java.io.IOException;
import java.util.Locale;

public class CheckForUpdate implements Tag {
    private final Log log = LogFactory.getLog(getClass());
    private PageContext pc;
    private Tag parent;

    public void setPageContext(PageContext pc) {
        this.pc = pc;
    }

    public void setParent(Tag t) {
        this.parent = t;
    }

    public Tag getParent() {
        return parent;
    }

    public int doStartTag() throws JspException {
        UpdateCheckerService updateCheckerService = getUpdateCheckerService();
        boolean updateAvailable = updateCheckerService.isUpdateAvailable();
        if (updateAvailable) {
            MessageSource messageSource = getMessageSource();
            Locale locale = LocaleContextHolder.getLocale();

            StringBuilder sb = new StringBuilder();
            sb.append("<a href=\"").append(updateCheckerService.getDownloadUrl()).append("\" target=\"_blank\">")
                    .append("<img src=\"")
                    .append(pc.getServletContext().getContextPath())
                    .append("/images/theme/icons/fugue/information-ocre.png")
                    .append("\"> ")
                    .append(messageSource.getMessage("updateChecker.newVersionAvailable", null, locale))
                    .append("</a>");

            try {
                pc.getOut().write(sb.toString());
            } catch (IOException e) {
                log.error("error in CheckForUpdate tag", e);
            }
        }
        return Tag.SKIP_BODY;
    }

    public int doEndTag() throws JspException {
        return Tag.EVAL_PAGE;
    }

    public void release() {
        this.pc = null;
        this.parent = null;
    }

    public UpdateCheckerService getUpdateCheckerService() {
        return (UpdateCheckerService) WebApplicationContextUtils.getWebApplicationContext(pc.getServletContext()).getBean("updateCheckerService");
    }

    public MessageSource getMessageSource() {
        return (MessageSource) WebApplicationContextUtils.getWebApplicationContext(pc.getServletContext()).getBean("messageSource");
    }
}
