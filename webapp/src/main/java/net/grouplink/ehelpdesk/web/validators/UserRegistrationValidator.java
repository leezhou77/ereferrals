package net.grouplink.ehelpdesk.web.validators;

import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.apache.commons.lang.StringUtils;

@Component
public class UserRegistrationValidator extends BaseValidator {

    @Autowired
	private UserService userService;

    public boolean supports(Class clazz) {
		return clazz.equals(User.class);
	}
	
	public void validate(Object target, Errors errors) {
		User user = (User) target;

        // check for required fields
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "user.required.firstName", "First name required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "user.required.lastName", "Last name required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "loginId", "user.required.loginId", "Login id required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "user.required.email", "Email address required");

        // check password fields
//        ValidationUtils.rejectIfEmpty(errors, "changePassword", "required.password", "Password required");
        if (user.getId() == null) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "changePassword",  "user.required.password", "Password required");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "retypePassword",  "user.required.retypePassword", "Please re-type your password");
        }

        if (StringUtils.isNotBlank(user.getChangePassword()) || StringUtils.isNotBlank(user.getRetypePassword())){
            if(!StringUtils.equals(user.getChangePassword(), user.getRetypePassword())){
//                errors.rejectValue("changePassword", "global.passwordMatchError", "Passwords don't match");
                errors.rejectValue("retypePassword", "global.passwordMatchError", "Passwords don't match");
            }
        }

        // check valid email
        if (StringUtils.isNotBlank(user.getEmail())) {
            if (!isValidEmailString(user.getEmail())) {
                errors.rejectValue("email", "mail.validation.address", "Invalid email address listed.");
            }
        }

        // check for duplicate username and email
        if (user.getId() == null) {
            // new user
            if (userService.getUserByUsername(user.getLoginId()) != null) {
                // duplicate username
                // reject
                errors.rejectValue("loginId", "register.validate.notuniqueid", "Login id already exists");
            }

            if (userService.getUserByEmail(user.getEmail()) != null) {
                // duplicate email
                // reject
                errors.rejectValue("email", "register.validate.email.notunique", "Email address already exists");
            }

            
        } else {
            // existing user
            if (!user.getId().equals(User.ADMIN_ID) && user.getLoginId().equals(User.ADMIN_LOGIN)) {
                // reject
                errors.rejectValue("loginId", "register.validate.notuniqueid", "Login id already exists");
            }

            if (!StringUtils.equalsIgnoreCase(user.getLoginId(), user.getOldUser().getLoginId())) {
                // existing user changing their username
                // check for dups
                if (userService.getUserByUsername(user.getLoginId()) != null) {
                    // duplicate username
                    // reject
                    errors.rejectValue("loginId", "register.validate.notuniqueid", "Login id already exists");
                }
            }

            if (!StringUtils.equalsIgnoreCase(user.getEmail(), user.getOldUser().getEmail())) {
                // existing user changing their email
                // check for dups
                if (userService.getUserByEmail(user.getEmail()) != null) {
                    // duplicate email
                    // reject
                    errors.rejectValue("email", "register.validate.email.notunique", "Email address already exists");
                }
            }
        }

//        if ((user.getId() == null && userService.getUserByUsername(user.getLoginId()) != null)
//                || (user.getId() != null && !user.getId().equals(User.ADMIN_ID) && user.getLoginId().equals(User.ADMIN_LOGIN))) {
//			errors.rejectValue("loginId", "register.validate.notuniqueid", "Login id already exists");
//		}

	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}
