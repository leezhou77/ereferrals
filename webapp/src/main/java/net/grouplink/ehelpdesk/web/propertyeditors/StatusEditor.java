package net.grouplink.ehelpdesk.web.propertyeditors;

import net.grouplink.ehelpdesk.domain.Status;
import net.grouplink.ehelpdesk.service.StatusService;
import org.apache.commons.lang.StringUtils;

import java.beans.PropertyEditorSupport;

/**
 * @author mrollins
 * @version 1.0
 */
public class StatusEditor extends PropertyEditorSupport {

    private StatusService statusService;

    /**
     * Constructor for use by derived PropertyEditor classes.
     */
    public StatusEditor() {
    }

    /**
     * Constructor for use by derived PropertyEditor classes.
     * @param statusService the service for getting status
     */
    public StatusEditor(StatusService statusService) {
        this.statusService = statusService;
    }

    /**
     * Gets the property value as a string suitable for presentation
     * to a human to edit.
     *
     * @return The property value as a string suitable for presentation
     *         to a human to edit.
     *         <p>   Returns "null" is the value can't be expressed as a string.
     *         <p>   If a non-null value is returned, then the PropertyEditor should
     *         be prepared to parse that string back in setAsText().
     */
    public String getAsText() {
        Status status = (Status) getValue();
        return (status != null) ? status.getId().toString() : "";
    }

    /**
     * Sets the property value by parsing a given String.  May raise
     * java.lang.IllegalArgumentException if either the String is
     * badly formatted or if this kind of property can't be expressed
     * as text.
     *
     * @param text The string to be parsed.
     */
    public void setAsText(String text) throws IllegalArgumentException {
        if(StringUtils.isNotBlank(text)){
            setValue(statusService.getStatusById(Integer.valueOf(text)));
        } else {
            setValue(null);
        }
    }

    public void setTicketPriorityService(StatusService statusService) {
        this.statusService = statusService;
    }
}
