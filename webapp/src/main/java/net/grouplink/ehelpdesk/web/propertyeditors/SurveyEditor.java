package net.grouplink.ehelpdesk.web.propertyeditors;

import net.grouplink.ehelpdesk.domain.Survey;
import net.grouplink.ehelpdesk.service.SurveyService;
import org.apache.commons.lang.StringUtils;

import java.beans.PropertyEditorSupport;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Jun 25, 2008
 * Time: 11:49:03 PM
 */
public class SurveyEditor extends PropertyEditorSupport
{
    private SurveyService surveyService;

    /**
     * Constructor for use by derived PropertyEditor classes.
     */
    public SurveyEditor() {
    }

    /**
     * Constructor for use by derived PropertyEditor classes.
     * @param surveyService the service for getting survey
     */
    public SurveyEditor(SurveyService surveyService) {
        this.surveyService = surveyService;
    }

    /**
     * Gets the property value as a string suitable for presentation
     * to a human to edit.
     *
     * @return The property value as a string suitable for presentation
     *         to a human to edit.
     *         <p>   Returns "null" is the value can't be expressed as a string.
     *         <p>   If a non-null value is returned, then the PropertyEditor should
     *         be prepared to parse that string back in setAsText().
     */
    public String getAsText() {
        Survey survey = (Survey) getValue();
        return (survey != null) ? survey.getId().toString() : "";
    }

    /**
     * Sets the property value by parsing a given String.  May raise
     * java.lang.IllegalArgumentException if either the String is
     * badly formatted or if this kind of property can't be expressed
     * as text.
     *
     * @param text The string to be parsed.
     */
    public void setAsText(String text) throws IllegalArgumentException {
        if (StringUtils.isNotBlank(text)){
            setValue(surveyService.getSurveyById(Integer.valueOf(text)));
        } else {
            setValue(null);
        }
    }

    public void setSurveyService(SurveyService surveyService) {
        this.surveyService = surveyService;
    }

}
