package net.grouplink.ehelpdesk.web.surveys;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Jun 20, 2008
 * Time: 9:36:48 AM
 */
public class SurveyItem {

    private String question;
    private String response;

    public SurveyItem(String question, String response) {
        this.question = question;
        this.response = response;
    }

    public String getQuestion() {
        return question;
    }

    public String getResponse() {
        return response;
    }
}
