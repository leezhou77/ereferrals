package net.grouplink.ehelpdesk.web.formatters;

import net.grouplink.ehelpdesk.domain.Phone;
import net.grouplink.ehelpdesk.service.PhoneService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class PhoneFormatter implements Formatter<Phone> {
    @Autowired
    private PhoneService phoneService;

    public Phone parse(String text, Locale locale) throws ParseException {
        if (StringUtils.isNotBlank(text)) {
            return phoneService.getById(Integer.valueOf(text));
        } else {
            return null;
        }
    }

    public String print(Phone object, Locale locale) {
        return object == null ? "" : object.getId().toString();
    }
}
