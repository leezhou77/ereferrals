package net.grouplink.ehelpdesk.web.context;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.context.ConfigurableWebApplicationContext;
import org.springframework.web.context.ContextLoader;

import javax.servlet.ServletContext;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * User: jaymehafen
 * Date: Mar 8, 2009
 * Time: 9:02:03 PM
 */
public class GLContextLoader extends ContextLoader {
    private Log log = LogFactory.getLog(getClass());
    
    @Override
    protected void customizeContext(ServletContext servletContext, ConfigurableWebApplicationContext applicationContext) {
        log.debug("loading zen10config.properties");
        Properties props = new Properties();
        try {
            props.load(GLContextLoader.class.getClassLoader().getResourceAsStream("conf/zen10config.properties"));
        } catch (IOException e) {
            log.error("error loading zen10config.properties: " + e.getMessage(), e);
        }

        boolean zen10Enabled = Boolean.valueOf(props.getProperty("zen10.enabled", "false"));
        log.debug("zen10Enabled: " + zen10Enabled);
        if (zen10Enabled) {
            List<String> configList = new ArrayList<String>(Arrays.asList(applicationContext.getConfigLocations()));
            configList.add("classpath:net/grouplink/ehelpdesk/common/appContext-zen10-db.xml");
            configList.add("classpath:net/grouplink/ehelpdesk/dao/hibernate/appContext-zen10-dao.xml");
            configList.add("classpath:net/grouplink/ehelpdesk/zen10/appContext-zen10-service.xml");
            applicationContext.setConfigLocations(configList.toArray(new String[configList.size()]));
        }

        log.debug("application.configLocations: " + Arrays.toString(applicationContext.getConfigLocations()));
    }
}
