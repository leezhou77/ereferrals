package net.grouplink.ehelpdesk.web.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * author: zpearce
 * Date: 8/10/11
 * Time: 4:04 PM
 */
public class Tabs extends TagSupport {
    private String currentTab, htmlClass;

    public int doStartTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();
            out.print("<ul");
            if(htmlClass != null) {
                out.print(" class=\"" + htmlClass + "\"");
            }
            out.print(">");
        } catch (IOException ioe) {
            throw new JspException("Error: IOException while writing to client.");
        }
        return EVAL_BODY_INCLUDE;
    }

    public int doEndTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();
            out.print("</ul>");
        } catch (IOException ioe) {
            throw new JspException("Error: IOException while writing to client.");
        }
        return EVAL_PAGE;
    }

    public void setHtmlClass(String htmlClass) {
        this.htmlClass = htmlClass;
    }

    public void setCurrentTab(String currentTab) {
        this.currentTab = currentTab;
    }

    public String getCurrentTab() {
        return this.currentTab;
    }
}
