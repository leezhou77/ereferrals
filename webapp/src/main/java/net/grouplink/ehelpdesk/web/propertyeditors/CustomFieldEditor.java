package net.grouplink.ehelpdesk.web.propertyeditors;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: mabbasi
 * Date: May 10, 2007
 * Time: 9:48:37 PM
 */
public class CustomFieldEditor {

    private List customFieldTypes;
    private List categoryOptions;
    private List customFieldCategoryOptions;
    private String fieldName;
    private String[] fieldValue;
    private String[] displayValue;
    private int noOfFields;
    private Integer typeId;
    private Integer categoryId;
    private boolean empty;
    private boolean required;

    public List getCustomFieldTypes() {
        return customFieldTypes;
    }

    public void setCustomFieldTypes(List customFieldTypes) {
        this.customFieldTypes = customFieldTypes;
    }

    public List getCategoryOptions() {
        return categoryOptions;
    }

    public void setCategoryOptions(List categoryOptions) {
        this.categoryOptions = categoryOptions;
    }

    public List getCustomFieldCategoryOptions() {
        return customFieldCategoryOptions;
    }

    public void setCustomFieldCategoryOptions(List customFieldCategoryOptions) {
        this.customFieldCategoryOptions = customFieldCategoryOptions;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String[] getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String[] fieldValue) {
        this.fieldValue = fieldValue;
    }

    public String[] getDisplayValue() {
        return displayValue;
    }

    public void setDisplayValue(String[] displayValue) {
        this.displayValue = displayValue;
    }

    public int getNoOfFields() {
        return noOfFields;
    }

    public void setNoOfFields(int noOfFields) {
        this.noOfFields = noOfFields;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public boolean getEmpty() {
        return empty;
    }

    public void setEmpty(boolean empty) {
        this.empty = empty;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }
}
