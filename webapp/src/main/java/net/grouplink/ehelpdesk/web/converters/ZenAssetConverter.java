package net.grouplink.ehelpdesk.web.converters;

import net.grouplink.ehelpdesk.domain.asset.ZenAsset;
import net.grouplink.ehelpdesk.service.asset.ZenAssetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

/**
 * author: zpearce
 * Date: 3/27/12
 * Time: 4:44 PM
 */
public class ZenAssetConverter implements Converter<String, ZenAsset> {
    @Autowired private ZenAssetService zenAssetService;
    
    public ZenAsset convert(String id) {
        try {
            int zid = Integer.parseInt(id);
            return zenAssetService.getById(zid);
        } catch (Exception e) {
            return null;
        }
    }
}
