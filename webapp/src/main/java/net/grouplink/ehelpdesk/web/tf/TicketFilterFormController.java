package net.grouplink.ehelpdesk.web.tf;

import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilterCriteriaHelper;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilterOperators;
import net.grouplink.ehelpdesk.service.CategoryOptionService;
import net.grouplink.ehelpdesk.service.CategoryService;
import net.grouplink.ehelpdesk.service.CustomFieldsService;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LocationService;
import net.grouplink.ehelpdesk.service.StatusService;
import net.grouplink.ehelpdesk.service.SurveyQuestionService;
import net.grouplink.ehelpdesk.service.TicketFilterService;
import net.grouplink.ehelpdesk.service.TicketPriorityService;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import net.grouplink.ehelpdesk.service.asset.AssetService;
import net.grouplink.ehelpdesk.service.asset.ZenAssetService;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class TicketFilterFormController extends SimpleFormController {

    public static final String TFC_FORM_TICKETFILTER = "TFFC.FORM.TICKETFILTER";

    private UserService userService;
    private TicketPriorityService ticketPriorityService;
    private LocationService locationService;
    private UserRoleGroupService userRoleGroupService;
    private CategoryService categoryService;
    private CategoryOptionService categoryOptionService;
    private StatusService statusService;
    private GroupService groupService;
    private CustomFieldsService customFieldsService;
    private TicketFilterService ticketFilterService;
    private SurveyQuestionService surveyQuestionService;
    private ZenAssetService zenAssetService;
    private AssetService assetService;
    private PermissionService permissionService;

    public TicketFilterFormController() {
        super();
        setCommandClass(TicketFilter.class);
        setCommandName("ticketFilter");
        setFormView("ticketFilterEdit");
        setSessionForm(true);
    }

    @Override
    protected String getFormSessionAttributeName(HttpServletRequest request) {
        return TFC_FORM_TICKETFILTER;
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        Integer tfId = ServletRequestUtils.getIntParameter(request, "tfId");
        TicketFilter ticketFilter;

        HttpSession sesh = request.getSession();
        boolean formReset = ServletRequestUtils.getBooleanParameter(request, "clearSessionAttr", false);
        if (formReset) {
            if (tfId != null && tfId > -1) {
                ticketFilter = ticketFilterService.getById(tfId);
                initTicketFilter(ticketFilter);
            } else {
                ticketFilter = getNewTicketFilter();
            }

            sesh.setAttribute(TFC_FORM_TICKETFILTER, ticketFilter);
        } else {
            // Check for the session fbo first
            ticketFilter = (TicketFilter) sesh.getAttribute(TFC_FORM_TICKETFILTER);

            if (ticketFilter != null) {
                // ticket filter was already in session
                // check if tfId is present
                if (tfId != null) {
                    // if it is, check if it's different than ticketFilter.id
                    if (!tfId.equals(ticketFilter.getId())) {
                        // tfId is different that ticketFilter.id
                        ticketFilter = ticketFilterService.getById(tfId);
                        initTicketFilter(ticketFilter);
                    }
                }
            } else {
                // ticketFilter was not in session
                if (tfId != null) {
                    ticketFilter = ticketFilterService.getById(tfId);
                    initTicketFilter(ticketFilter);
                } else
                    ticketFilter = getNewTicketFilter();
            }
        }

        return ticketFilter;
    }

    private TicketFilter getNewTicketFilter() {
        TicketFilter ticketFilter = new TicketFilter();
        ticketFilter.setPrivateFilter(Boolean.TRUE);

        // Set the default checks for show column
        String[] colorder = {
            TicketFilter.COLUMN_PRIORITY,
            TicketFilter.COLUMN_TICKETNUMBER,
            TicketFilter.COLUMN_LOCATION,
            TicketFilter.COLUMN_SUBJECT,
            TicketFilter.COLUMN_CATOPT,
            TicketFilter.COLUMN_ASSIGNED,
            TicketFilter.COLUMN_STATUS,
            TicketFilter.COLUMN_CREATED
        };
        ticketFilter.setColumnOrder(colorder);
        ticketFilter.setName(getMessageSourceAccessor().getMessage("ticketFilter.defaultName"));
        return ticketFilter;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
        TicketFilter ticketFilter = (TicketFilter) command;

        Map<String, Object> refData = new HashMap<String, Object>();
        refData.put("priorities", ticketPriorityService.getPriorities());
        refData.put("status", statusService.getStatus());

        if (!ArrayUtils.isEmpty(ticketFilter.getLocationIds())) {
            Map<Integer, String> locationIdToNameMap = new HashMap<Integer, String>();
            for (Integer locId : ticketFilter.getLocationIds()) {
                locationIdToNameMap.put(locId, locationService.getLocationById(locId).getName());
            }
            refData.put("locationIdToNameMap", locationIdToNameMap);
        }

        refData.put("groups", groupService.getGroups());

        if (!ArrayUtils.isEmpty(ticketFilter.getCategoryIds())) {
            Map<Integer, String> catIdToNameMap = new HashMap<Integer, String>();
            for (Integer catId : ticketFilter.getCategoryIds()) {
                catIdToNameMap.put(catId, categoryService.getCategoryById(catId).getName());
            }
            refData.put("catIdToNameMap", catIdToNameMap);
        }

        if (!ArrayUtils.isEmpty(ticketFilter.getCategoryOptionIds())) {
            Map<Integer, String> catOptIdToNameMap = new HashMap<Integer, String>();
            for (Integer catOptId : ticketFilter.getCategoryOptionIds()) {
                catOptIdToNameMap.put(catOptId, categoryOptionService.getCategoryOptionById(catOptId).getName());
            }
            refData.put("catOptIdToNameMap", catOptIdToNameMap);
        }

        if (!ArrayUtils.isEmpty(ticketFilter.getContactIds())) {
            Map<Integer, String> contactIdToNameMap = new HashMap<Integer, String>();
            for (Integer userId : ticketFilter.getContactIds()) {
                User u = userService.getUserById(userId);
                contactIdToNameMap.put(userId, u.getLastName() + ", " + u.getFirstName());
            }
            refData.put("contactIdToNameMap", contactIdToNameMap);
        }

        if (!ArrayUtils.isEmpty(ticketFilter.getSubmittedByIds())) {
            Map<Integer, String> submittedIdToNameMap = new HashMap<Integer, String>();
            for (Integer userId : ticketFilter.getSubmittedByIds()) {
                User u = userService.getUserById(userId);
                submittedIdToNameMap.put(userId, u.getLastName() + ", " + u.getFirstName());
            }
            refData.put("submittedIdToNameMap", submittedIdToNameMap);
        }

        if (!ArrayUtils.isEmpty(ticketFilter.getAssignedToIds())) {
            Map<Integer, String> assignedToIdToNameMap = new HashMap<Integer, String>();
            for (Integer urgId : ticketFilter.getAssignedToIds()) {
                UserRoleGroup urg = userRoleGroupService.getUserRoleGroupById(urgId);

                assignedToIdToNameMap.put(urgId, (urg.getUserRole() != null) ?
                        urg.getUserRole().getUser().getLastName() + ", " + urg.getUserRole().getUser().getFirstName() :
                        urg.getGroup().getName());
            }
            refData.put("assignedToIdToNameMap", assignedToIdToNameMap);
        }

        if (!ArrayUtils.isEmpty(ticketFilter.getAssignedToUserIds())) {
            Map<Integer, String> assignedToUserIdToNameMap = new HashMap<Integer, String>();
            for (Integer userId : ticketFilter.getAssignedToUserIds()) {
                User u = userService.getUserById(userId);
                assignedToUserIdToNameMap.put(userId, u.getLastName() + ", " + u.getFirstName() + " (" + u.getLoginId() + ")");
            }
            refData.put("assignedToUserIdToNameMap", assignedToUserIdToNameMap);
        }

        refData.put("dateOps", getDateOpOptions());
        List<String> uniqueCustomFields = customFieldsService.getByUniqueName();
        Collections.sort(uniqueCustomFields);
        refData.put("customFields", uniqueCustomFields);

        List<String> uniqueSurveyQuestions = surveyQuestionService.getByUniqueQuestionText();
        Collections.sort(uniqueSurveyQuestions);
        refData.put("surveyQuestions", uniqueSurveyQuestions);

        if (!ArrayUtils.isEmpty(ticketFilter.getZenAssetIds())) {
            Map<Integer, String> zenAssetIdToNameMap = new HashMap<Integer, String>();
            for (Integer zenId : ticketFilter.getZenAssetIds()) {
                zenAssetIdToNameMap.put(zenId, zenAssetService.getById(zenId).getAssetName());
            }
            refData.put("zenAssetIdToNameMap", zenAssetIdToNameMap);
        }

        Integer[] internalAssetIds = ticketFilter.getInternalAssetIds();
        Map<Integer, String> assetIdToNameMap = new HashMap<Integer, String>();
        if (!ArrayUtils.isEmpty(internalAssetIds)) {
            for (Integer assetId : internalAssetIds) {
                assetIdToNameMap.put(assetId, assetService.getAssetById(assetId).getName());
            }

            refData.put("assetIdToNameMap", assetIdToNameMap);
        }

        Map<Integer, String> operatorNameMap = new HashMap<Integer, String>();
        operatorNameMap.put(TicketFilterOperators.EQUALS, getMessageSourceAccessor().getMessage("ticketFilter.op.equals"));
        operatorNameMap.put(TicketFilterOperators.NOT_EQUALS, getMessageSourceAccessor().getMessage("ticketFilter.op.notEquals"));
        operatorNameMap.put(TicketFilterOperators.GREATER_THAN, getMessageSourceAccessor().getMessage("ticketFilter.op.greaterThan"));
        operatorNameMap.put(TicketFilterOperators.LESS_THAN, getMessageSourceAccessor().getMessage("ticketFilter.op.lessThan"));
        operatorNameMap.put(TicketFilterOperators.CONTAINS, getMessageSourceAccessor().getMessage("ticketFilter.op.contains"));
        operatorNameMap.put(TicketFilterOperators.NOT_CONTAINS, getMessageSourceAccessor().getMessage("ticketFilter.op.notContains"));
        operatorNameMap.put(TicketFilterOperators.STARTS_WITH, getMessageSourceAccessor().getMessage("ticketFilter.op.startsWith"));
        operatorNameMap.put(TicketFilterOperators.ENDS_WIDTH, getMessageSourceAccessor().getMessage("ticketFilter.op.endsWith"));
        refData.put("opNameMap", operatorNameMap);

        refData.put("reset", ServletRequestUtils.getStringParameter(request, "reset", "false"));

        Map<String, String> criteriaCols = getColumnTranslationMap(getCriterias(ticketFilter));
        Map<String, String> allColumns = getColumnTranslationMap(Arrays.asList(TicketFilter.ALL_COLUMNS));

        refData.put("allColumns", allColumns);
        refData.put("criteriaCols", criteriaCols);

        boolean showSaveButton = hasPermission(ticketFilter);

        refData.put("showSaveButton", showSaveButton);

        return refData;
    }

    private boolean hasPermission(TicketFilter ticketFilter) {
        boolean showSaveButton = true;
        if (ticketFilter.getId() != null &&
                !ticketFilter.getUser().equals(userService.getLoggedInUser()) &&
                !ticketFilter.getPrivateFilter()) {
            // check permission
            showSaveButton = permissionService.hasGlobalPermission("global.publicTicketFilterManagement");
        }
        return showSaveButton;
    }

    private List<String> getCriterias(TicketFilter ticketFilter) {
        List<String> retVal = new ArrayList<String>();

        if(!ArrayUtils.isEmpty(ticketFilter.getTicketNumbers())){
            retVal.add(TicketFilter.COLUMN_TICKETNUMBER);
        }
        if(!ArrayUtils.isEmpty(ticketFilter.getPriorityIds())){
            retVal.add(TicketFilter.COLUMN_PRIORITY);
        }
        if(!ArrayUtils.isEmpty(ticketFilter.getStatusIds())){
            retVal.add(TicketFilter.COLUMN_STATUS);
        }
        if(!ArrayUtils.isEmpty(ticketFilter.getSubject())){
            retVal.add(TicketFilter.COLUMN_SUBJECT);
        }
        if(!ArrayUtils.isEmpty(ticketFilter.getNote())){
            retVal.add(TicketFilter.COLUMN_NOTE);
        }
        if(!ArrayUtils.isEmpty(ticketFilter.getHistorySubject())){
            retVal.add(TicketFilter.COLUMN_HISTORYSUBJECT);
        }
        if(!ArrayUtils.isEmpty(ticketFilter.getHistoryNote())){
            retVal.add(TicketFilter.COLUMN_HISTORYNOTE);
        }
        if(StringUtils.isNotEmpty(ticketFilter.getWorkTimeHours())){
            retVal.add(TicketFilter.COLUMN_WORKTIME);
        }
        if(ticketFilter.getCreatedDateOperator() != null && ticketFilter.getCreatedDateOperator()!= 0){
            retVal.add(TicketFilter.COLUMN_CREATED);
        }
        if(ticketFilter.getModifiedDateOperator() != null && ticketFilter.getModifiedDateOperator() != 0){
            retVal.add(TicketFilter.COLUMN_MODIFIED);
        }
        if(ticketFilter.getEstimatedDateOperator() != null && ticketFilter.getEstimatedDateOperator() != 0){
            retVal.add(TicketFilter.COLUMN_ESTIMATED);
        }
        if(!ArrayUtils.isEmpty(ticketFilter.getContactIds())){
            retVal.add(TicketFilter.COLUMN_CONTACT);
        }
        if(!ArrayUtils.isEmpty(ticketFilter.getSubmittedByIds())){
            retVal.add(TicketFilter.COLUMN_SUBMITTED);
        }
        if(!ArrayUtils.isEmpty(ticketFilter.getAssignedToIds()) || !ArrayUtils.isEmpty(ticketFilter.getAssignedToUserIds())) {
            retVal.add(TicketFilter.COLUMN_ASSIGNED);
        }
        if(!ArrayUtils.isEmpty(ticketFilter.getLocationIds())){
            retVal.add(TicketFilter.COLUMN_LOCATION);
        }
        if(!ArrayUtils.isEmpty(ticketFilter.getGroupIds())){
            retVal.add(TicketFilter.COLUMN_GROUP);
        }
        if(!ArrayUtils.isEmpty(ticketFilter.getCategoryIds())){
            retVal.add(TicketFilter.COLUMN_CATEGORY);
        }
        if(!ArrayUtils.isEmpty(ticketFilter.getCategoryOptionIds())){
            retVal.add(TicketFilter.COLUMN_CATOPT);
        }
        if(!ArrayUtils.isEmpty(ticketFilter.getSurveyQuestionId())){
            retVal.add(TicketFilter.COLUMN_SURVEYS);
        }
//        if(!ArrayUtils.isEmpty(ticketFilter.getZenAssetIds())){
//            retVal.add(TicketFilter.COLUMN_ZENWORKS10ASSET);
//        }
        if(!ArrayUtils.isEmpty(ticketFilter.getInternalAssetIds())){
            retVal.add(TicketFilter.COLUMN_INTERNALASSET);
        }

        // Custom fields
        Map<String, List<TicketFilterCriteriaHelper>> cfMap = ticketFilter.getCustomFields();
        if(cfMap != null){
            for (String cfName : cfMap.keySet()) {
                retVal.add("customField."+cfName);
            }
        }
        return retVal;
    }

    private Map<String, String> getColumnTranslationMap(List<String> columns) {
        Map<String, String> transmap = new TreeMap<String, String>();
        for(String colName : columns) {
            if(colName.startsWith("ticketFilter.")) {
                transmap.put(getMessageSourceAccessor().getMessage(colName), colName);
            } else {
                transmap.put(StringUtils.replace(colName, "customField.", ""), colName);
            }
        }
        return transmap;
    }

    private List<TicketFilterOperators> getDateOpOptions() {
        List<TicketFilterOperators> list = new ArrayList<TicketFilterOperators>();
        // Any
        TicketFilterOperators ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_ANY);
        ticketFilterOperators.setDisplay("-- " + getMessageSourceAccessor().getMessage("ticketSearch.any") + " --");
        list.add(ticketFilterOperators);
        // Today
        ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_TODAY);
        ticketFilterOperators.setDisplay(getMessageSourceAccessor().getMessage("date.today"));
        list.add(ticketFilterOperators);
        // Week
        ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_WEEK);
        ticketFilterOperators.setDisplay(getMessageSourceAccessor().getMessage("date.thisweek"));
        list.add(ticketFilterOperators);
        // Month
        ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_MONTH);
        ticketFilterOperators.setDisplay(getMessageSourceAccessor().getMessage("date.thismonth"));
        list.add(ticketFilterOperators);
        // On
        ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_ON);
        ticketFilterOperators.setDisplay(getMessageSourceAccessor().getMessage("date.on"));
        list.add(ticketFilterOperators);
        // Before
        ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_BEFORE);
        ticketFilterOperators.setDisplay(getMessageSourceAccessor().getMessage("date.before"));
        list.add(ticketFilterOperators);
        // After
        ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_AFTER);
        ticketFilterOperators.setDisplay(getMessageSourceAccessor().getMessage("date.after"));
        list.add(ticketFilterOperators);
        // Range
        ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_RANGE);
        ticketFilterOperators.setDisplay(getMessageSourceAccessor().getMessage("date.range"));
        list.add(ticketFilterOperators);
        // Last X Hours
        ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_LAST_X_HOURS);
        ticketFilterOperators.setDisplay(getMessageSourceAccessor().getMessage("date.lastx.hours"));
        list.add(ticketFilterOperators);
        // Last X days
        ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_LAST_X_DAYS);
        ticketFilterOperators.setDisplay(getMessageSourceAccessor().getMessage("date.lastx.days"));
        list.add(ticketFilterOperators);
        // Last X weeks
        ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_LAST_X_WEEKS);
        ticketFilterOperators.setDisplay(getMessageSourceAccessor().getMessage("date.lastx.weeks"));
        list.add(ticketFilterOperators);
        // Last X months
        ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_LAST_X_MONTHS);
        ticketFilterOperators.setDisplay(getMessageSourceAccessor().getMessage("date.lastx.months"));
        list.add(ticketFilterOperators);
        // Last X years
        ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_LAST_X_YEARS);
        ticketFilterOperators.setDisplay(getMessageSourceAccessor().getMessage("date.lastx.years"));
        list.add(ticketFilterOperators);

        return list;
    }


    @Override
    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        TicketFilter ticketFilter = (TicketFilter) command;
        User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);
        ticketFilter.setUser(user);

        // always save this ticketfilter on the session
        request.getSession().setAttribute(TFC_FORM_TICKETFILTER, ticketFilter);

        // determine whether to persist to db (save), just show results (apply),  delete(delete)
        // or just reload the filter to the filter edit page (reload)
        String formAction = ServletRequestUtils.getStringParameter(request, "formAction", "save");
        if (!"apply".equals(formAction)) {
            if ("reset".equals(formAction)) {
                Integer id = ticketFilter.getId();
                request.setAttribute("flash.success", getMessageSourceAccessor().getMessage("ticketFilter.resetSuccess"));
                if (id != null) {
                    TicketFilter ntf = ticketFilterService.getById(id);
                    initTicketFilter(ntf);
                    request.getSession().setAttribute(TFC_FORM_TICKETFILTER, ntf);
                } else {
                    request.getSession().removeAttribute(TFC_FORM_TICKETFILTER);
                }
            } else if ("saveas".equals(formAction)) {
                String saveAsName = ServletRequestUtils.getStringParameter(request, "saveAsName");
                ticketFilter.setId(null);
                ticketFilter.setName(saveAsName);
                ticketFilterService.save(ticketFilter);
                request.getSession().setAttribute(TFC_FORM_TICKETFILTER, ticketFilter);
                request.setAttribute("flash.success", getMessageSourceAccessor().getMessage("global.saveSuccess"));
            } else { // save
                // check permission
                if (!hasPermission(ticketFilter)) {
                    throw new AccessDeniedException("Access denied");
                }

                ticketFilterService.save(ticketFilter);
                request.setAttribute("flash.success", getMessageSourceAccessor().getMessage("global.saveSuccess"));
            }
        }

        if (ticketFilter.getId() != null) {
            return new ModelAndView(new RedirectView("/tf/ticketFilterEdit.glml?tfId=" + ticketFilter.getId(), true));
        } else {
            return new ModelAndView(new RedirectView("/tf/ticketFilterEdit.glml", true));
        }
    }

    private void initTicketFilter(TicketFilter tf) {
        // force fields to initialize
        if (tf.getUser() != null) {
            tf.getUser().getLoginId();
        }
    }

    public void setTicketFilterService(TicketFilterService ticketFilterService) {
        this.ticketFilterService = ticketFilterService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setTicketPriorityService(TicketPriorityService ticketPriorityService) {
        this.ticketPriorityService = ticketPriorityService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setCategoryOptionService(CategoryOptionService categoryOptionService) {
        this.categoryOptionService = categoryOptionService;
    }

    public void setStatusService(StatusService statusService) {
        this.statusService = statusService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setCustomFieldsService(CustomFieldsService customFieldsService) {
        this.customFieldsService = customFieldsService;
    }

    public void setSurveyQuestionService(SurveyQuestionService surveyQuestionService) {
        this.surveyQuestionService = surveyQuestionService;
    }

    public void setZenAssetService(ZenAssetService zenAssetService) {
        this.zenAssetService = zenAssetService;
    }

    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }

    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }
}
