package net.grouplink.ehelpdesk.web.controllers.asset;

import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.asset.*;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilterOperators;
import net.grouplink.ehelpdesk.service.*;
import net.grouplink.ehelpdesk.service.asset.*;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.DateFormat;
import java.util.*;

/**
 * Author: zpearce
 * Date: 5/2/12
 * Time: 2:26 PM
 */
@Controller
@RequestMapping("/assets/filters")
public class AssetFilterController extends ApplicationObjectSupport {
    private Log log = LogFactory.getLog(getClass());
    public static final String AFC_FORM_ASSETFILTER = "AFC.FORM.ASSETFILTER";

    @Autowired private AssetFilterService assetFilterService;
    @Autowired private AssetService assetService;
    @Autowired private AssetCustomFieldService assetCustomFieldService;
    @Autowired private AssetFieldGroupService assetFieldGroupService;
    @Autowired private AssetTypeService assetTypeService;
    @Autowired private UserService userService;
    @Autowired private VendorService vendorService;
    @Autowired private LocationService locationService;
    @Autowired private GroupService groupService;
    @Autowired private CategoryService categoryService;
    @Autowired private CategoryOptionService categoryOptionService;
    @Autowired private AssetStatusService assetStatusService;
    @Autowired private LicenseService licenseService;
    @Autowired private SoftwareLicenseService softwareLicenseService;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        if (!licenseService.isAssetTracker()) {
            return "ehd-asset/assetTrackerAd";
        }

        User user = userService.getLoggedInUser();
        model.addAttribute("myFilters", assetFilterService.getByUser(user));
        model.addAttribute("pubFilters", assetFilterService.getPublicMinusUser(user));

        return "ehd-asset/filters/index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/new")
    public String newForm(Model model, HttpSession session) {
        session.removeAttribute(AFC_FORM_ASSETFILTER);
        AssetFilter filter = createNewAssetFilter();
        session.setAttribute(AFC_FORM_ASSETFILTER, filter);

        model.addAttribute("assetFilter", filter);
        model.addAllAttributes(referenceData(filter));
        return "ehd-asset/filters/form";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String create(Model model, HttpServletRequest request, HttpSession session) throws Exception {
        // Manually do binding here because @SessionAttributes is stupid and can only apply to an entire controller
        AssetFilter assetFilter = (AssetFilter)session.getAttribute(AFC_FORM_ASSETFILTER);
        ServletRequestDataBinder binder = new ServletRequestDataBinder(assetFilter);
        binder.bind(request);

        BindingResult bindingResult = binder.getBindingResult();

        session.setAttribute(AFC_FORM_ASSETFILTER, assetFilter);
        User user = userService.getLoggedInUser();
        assetFilter.setUser(user);

        if(!bindingResult.hasErrors()) {
            String formAction = ServletRequestUtils.getStringParameter(request, "formAction", "save");
            if(!"apply".equals(formAction)) {
                if ("reset".equals(formAction)) {
                    return "redirect:/assets/filters/new";
                } else {
                    assetFilterService.save(assetFilter);
                    request.setAttribute("flash.success", getMessageSourceAccessor().getMessage("global.saveSuccess"));
                    return "redirect:/assets/filters/" + assetFilter.getId() + "/edit";
                }
            }
        }

        model.addAttribute("assetFilter", assetFilter);
        model.addAllAttributes(referenceData(assetFilter));
        return "ehd-asset/filters/form";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{assetFilter}/edit")
    public String edit(@ModelAttribute("assetFilter") AssetFilter assetFilter, Model model, HttpSession session) {
        session.setAttribute(AFC_FORM_ASSETFILTER, assetFilter);

        model.addAttribute("assetFilter", assetFilter);
        model.addAllAttributes(referenceData(assetFilter));
        return "ehd-asset/filters/form";
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{assetFilter}")
    public String update(Model model, HttpServletRequest request, HttpSession session) {
        AssetFilter assetFilter = (AssetFilter)session.getAttribute(AFC_FORM_ASSETFILTER);
        ServletRequestDataBinder binder = new ServletRequestDataBinder(assetFilter);
        binder.bind(request);
        BindingResult bindingResult = binder.getBindingResult();

        session.setAttribute(AFC_FORM_ASSETFILTER, assetFilter);
        if(!bindingResult.hasErrors()) {
            String formAction = ServletRequestUtils.getStringParameter(request, "formAction", "save");
            if(!"apply".equals(formAction)) {
                if("reset".equals(formAction)) {
                    return "redirect:/assets/filters/" + assetFilter.getId() + "/edit";
                } else {
                    assetFilterService.save(assetFilter);
                    request.setAttribute("flash.success", getMessageSourceAccessor().getMessage("global.saveSuccess"));
                }
            }
            return "redirect:/assets/filters/" + assetFilter.getId() + "/edit";
        }

        model.addAttribute("assetFilter", assetFilter);
        model.addAllAttributes(referenceData(assetFilter));
        return "ehd-asset/filters/form";
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{assetFilter}")
    public void destroy(@ModelAttribute("assetFilter") AssetFilter assetFilter, HttpServletResponse response) {
        assetFilterService.delete(assetFilter);
        response.setStatus(HttpServletResponse.SC_OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{assetFilter}/copy")
    public String copy(@ModelAttribute("assetFilter") AssetFilter assetFilter, HttpServletRequest request) throws Exception {
        String newName = ServletRequestUtils.getRequiredStringParameter(request, "saveAsName");
        AssetFilter newFilter = assetFilter.clone();
        newFilter.setName(newName);
        newFilter.setUser(userService.getLoggedInUser());
        assetFilterService.save(newFilter);
        return "redirect:/assets/filters/" + newFilter.getId() + "/edit";
    }

    @RequestMapping(method = RequestMethod.GET, value = {"/current/results", "/{assetFilter}/results"})
    public String results(@ModelAttribute("assetFilter") AssetFilter assetFilter, Model model, HttpSession session) {
        AssetFilter filter;
        if(assetFilter.getId() == null) {
            filter = (AssetFilter) session.getAttribute(AFC_FORM_ASSETFILTER);
        } else {
            filter = assetFilter;
        }

        if (assetFilter.getReport()) {
            String url = "redirect:/asset/report/dynamicAssetReport.glml";
            if (assetFilter.getId() != null) {
                url += "?filterId=" + assetFilter.getId();
            }

            return url;
        }

        model.addAttribute(filter);
        model.addAllAttributes(referenceData(filter));
        return "ehd-asset/filters/results";
    }

    @RequestMapping(method = RequestMethod.GET, value = {"/current/store", "/{assetFilter}/store"})
    public void store(@ModelAttribute("assetFilter") AssetFilter assetFilter, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        AssetFilter filter;
        if(assetFilter.getId() == null) {
            filter = (AssetFilter) session.getAttribute(AFC_FORM_ASSETFILTER);
        } else {
            filter = assetFilter;
        }

        int start = ServletRequestUtils.getIntParameter(request, "start", 0);
        int count = ServletRequestUtils.getIntParameter(request, "count", -1);
        String sort = ServletRequestUtils.getStringParameter(request, "sort");

        User user = userService.getLoggedInUser();
        Integer assetCount = assetFilterService.getAssetCount(filter, user);
        List<Asset> assets = assetFilterService.getAssets(filter, user, start, count, sort);

        Locale locale = RequestContextUtils.getLocale(request);
        List<JSONObject> jsonAssets = new ArrayList<JSONObject>();
        for(Asset asset : assets) {
            try {
                jsonAssets.add(getAssetJsonObject(asset, locale, filter));
            } catch (JSONException e) {
                log.error("can't get json object for asset " + asset.getId(), e);
            }
        }

        JSONObject store = new JSONObject();
        try {
            store.put("identifier", AssetFilter.COLUMN_ASSETNUMBER);
            store.put("numRows", assetCount);
            store.put("items", jsonAssets);
        } catch (JSONException e) {
            log.error("error setting store values", e);
        }

        response.setContentType("application/json");
        try {
            response.getWriter().print(store.toString());
        } catch (IOException e) {
            log.error("error writing response", e);
        }
    }

    private JSONObject getAssetJsonObject(Asset asset, Locale locale, AssetFilter assetFilter) throws JSONException {
        JSONObject item = new JSONObject();

        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.SHORT, locale);

        item.put(AssetFilter.COLUMN_ASSETNUMBER, asset.getAssetNumber());
        item.put(AssetFilter.COLUMN_ASSETID, asset.getId());
        item.put(AssetFilter.COLUMN_NAME, asset.getName());
        item.put(AssetFilter.COLUMN_TYPE, (asset.getAssetType() == null ? "" : asset.getAssetType().getName()));
        item.put(AssetFilter.COLUMN_OWNER, (asset.getOwner() == null ? "" : asset.getOwner().getDisplayName()));
        item.put(AssetFilter.COLUMN_VENDOR, (asset.getVendor() == null ? "" : asset.getVendor().getName()));
        item.put(AssetFilter.COLUMN_ASSETLOCATION, asset.getAssetLocation());
        item.put(AssetFilter.COLUMN_ACQUISITIONDATE, (asset.getAcquisitionDate() == null ? "" : dateFormat.format(asset.getAcquisitionDate())));
        item.put(AssetFilter.COLUMN_LOCATION, (asset.getLocation() == null ? "" : asset.getLocation().getName()));
        item.put(AssetFilter.COLUMN_GROUP, (asset.getGroup() == null ? "" : asset.getGroup().getName()));
        item.put(AssetFilter.COLUMN_CATEGORY, (asset.getCategory() == null ? "" : asset.getCategory().getName()));
        item.put(AssetFilter.COLUMN_CATEGORYOPTION, (asset.getCategoryOption() == null ? "" : asset.getCategoryOption().getName()));
        item.put(AssetFilter.COLUMN_MANUFACTURER, asset.getManufacturer());
        item.put(AssetFilter.COLUMN_MODELNUMBER, asset.getModelNumber());
        item.put(AssetFilter.COLUMN_SERIALNUMBER, asset.getSerialNumber());
        item.put(AssetFilter.COLUMN_DESCRIPTION, asset.getDescription());
        item.put(AssetFilter.COLUMN_STATUS, (asset.getStatus() == null ? "" : asset.getStatus().getName()));
        item.put(AssetFilter.COLUMN_AI_PONUMBER, asset.getAccountingInfo().getPoNumber());
        item.put(AssetFilter.COLUMN_AI_ACCOUNTNUMBER, asset.getAccountingInfo().getAccountNumber());
        item.put(AssetFilter.COLUMN_AI_ACCUMULATEDNUMBER, asset.getAccountingInfo().getAccumulatedNumber());
        item.put(AssetFilter.COLUMN_AI_EXPENSENUMBER, asset.getAccountingInfo().getExpenseNumber());
        item.put(AssetFilter.COLUMN_AI_ACQUISITIONVALUE, asset.getAccountingInfo().getAcquisitionValue());
        item.put(AssetFilter.COLUMN_AI_LEASENUMBER, asset.getAccountingInfo().getLeaseNumber());
        item.put(AssetFilter.COLUMN_AI_LEASEDURATION, asset.getAccountingInfo().getLeaseDuration());
        item.put(AssetFilter.COLUMN_AI_LEASEFREQUENCY, asset.getAccountingInfo().getLeaseFrequency());
        item.put(AssetFilter.COLUMN_AI_LEASEFREQUENCYPRICE, asset.getAccountingInfo().getLeaseFrequencyPrice());
        item.put(AssetFilter.COLUMN_AI_DISPOSALMETHOD, asset.getAccountingInfo().getDisposalMethod());
        item.put(AssetFilter.COLUMN_AI_DISPOSALDATE, (asset.getAccountingInfo().getDisposalDate() == null ? "" : dateFormat.format(asset.getAccountingInfo().getDisposalDate())));
        item.put(AssetFilter.COLUMN_AI_INSURANCECATEGORY, asset.getAccountingInfo().getInsuranceCategory());
        item.put(AssetFilter.COLUMN_AI_REPLACEMENTVALUECATEGORY, asset.getAccountingInfo().getReplacementValueCategory());
        item.put(AssetFilter.COLUMN_AI_REPLACEMENTVALUE, asset.getAccountingInfo().getReplacementValue());
        item.put(AssetFilter.COLUMN_AI_MAINTENANCECOST, asset.getAccountingInfo().getMaintenanceCost());
        item.put(AssetFilter.COLUMN_AI_DEPRECIATIONMETHOD, asset.getAccountingInfo().getDepreciationMethod());
        item.put(AssetFilter.COLUMN_AI_LEASEEXPIRATIONDATE, (asset.getAccountingInfo().getLeaseExpirationDate() == null ? "" : dateFormat.format(asset.getAccountingInfo().getLeaseExpirationDate())));
        item.put(AssetFilter.COLUMN_AI_WARRANTYDATE, (asset.getAccountingInfo().getWarrantyDate() == null ? "" : dateFormat.format(asset.getAccountingInfo().getWarrantyDate())));

        if(!asset.getSoftwareLicenses().isEmpty()) {
            StringBuilder licNames = new StringBuilder();
            Object[] licArray = asset.getSoftwareLicenses().toArray();
            for(int i = 0; i < licArray.length; i++) {
                licNames.append(((SoftwareLicense)licArray[i]).getProductName());
                if(i < licArray.length - 1) {
                    licNames.append("\n");
                }
            }
            item.put(AssetFilter.COLUMN_SOFTLICS, licNames);
        }

        for(AssetCustomField cf : getCustomFields(assetFilter)) {
            AssetCustomFieldValue acfv = assetCustomFieldService.getCustomFieldValue(asset, cf);
            if(acfv == null) {
                item.put("customField." + cf.getName(), "");
            } else {
                item.put("customField." + cf.getName(), acfv.getFieldValue() == null ? "" : acfv.getFieldValue());
            }
        }

        return item;
    }

    private List<AssetCustomField> getCustomFields(AssetFilter assetFilter) {
        ArrayList<AssetCustomField> cfs = new ArrayList<AssetCustomField>();
        for(String col : assetFilter.getColumnOrder()) {
            if(col.startsWith("customField.")) {
                AssetCustomField cf = assetCustomFieldService.getByName(col.replace("customField.", ""));
                cfs.add(cf);
            }
        }
        return cfs;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/current/addItem")
    public void addItem(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        AssetFilter assetFilter = (AssetFilter) session.getAttribute(AssetFilterFormController.AFC_FORM_ASSETFILTER);

        // get the type and value to be added.
        String type = ServletRequestUtils.getStringParameter(request, "type");
        String value = ServletRequestUtils.getStringParameter(request, "value");

        String htmlTable = "";
        String ctxtPath = request.getContextPath();

        if (type.equals("assetNumbers")) {
            String[] items = assetFilter.getAssetNumbers();
            if (!ArrayUtils.contains(assetFilter.getAssetNumbers(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getAssetNumbers(), value);
                assetFilter.setAssetNumbers(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("names")) {
            String[] items = assetFilter.getNames();
            if (!ArrayUtils.contains(assetFilter.getNames(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getNames(), value);
                assetFilter.setNames(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("typeIds")) {
            Integer[] items = assetFilter.getTypeIds();
            if (!ArrayUtils.contains(assetFilter.getTypeIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(assetFilter.getTypeIds(), new Integer(value));
                assetFilter.setTypeIds(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("ownerIds")) {
            Integer[] items = assetFilter.getOwnerIds();
            if (!ArrayUtils.contains(assetFilter.getOwnerIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(assetFilter.getOwnerIds(), new Integer(value));
                assetFilter.setOwnerIds(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("vendorIds")) {
            Integer[] items = assetFilter.getVendorIds();
            if (!ArrayUtils.contains(assetFilter.getVendorIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(assetFilter.getVendorIds(), new Integer(value));
                assetFilter.setVendorIds(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("notes")) {
            String[] items = assetFilter.getNotes();
            if (!ArrayUtils.contains(assetFilter.getNotes(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getNotes(), value);
                assetFilter.setNotes(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("assetLocations")) {
            String[] items = assetFilter.getAssetLocations();
            if (!ArrayUtils.contains(assetFilter.getAssetLocations(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getAssetLocations(), value);
                assetFilter.setAssetLocations(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("locationIds")) {
            Integer[] items = assetFilter.getLocationIds();
            if (!ArrayUtils.contains(assetFilter.getLocationIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(assetFilter.getLocationIds(), new Integer(value));
                assetFilter.setLocationIds(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("groupIds")) {
            Integer[] items = assetFilter.getGroupIds();
            if (!ArrayUtils.contains(assetFilter.getGroupIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(assetFilter.getGroupIds(), new Integer(value));
                assetFilter.setGroupIds(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("categoryIds")) {
            Integer[] items = assetFilter.getCategoryIds();
            if (!ArrayUtils.contains(assetFilter.getCategoryIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(assetFilter.getCategoryIds(), new Integer(value));
                assetFilter.setCategoryIds(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("categoryOptionIds")) {
            Integer[] items = assetFilter.getCategoryOptionIds();
            if (!ArrayUtils.contains(assetFilter.getCategoryOptionIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(assetFilter.getCategoryOptionIds(), new Integer(value));
                assetFilter.setCategoryOptionIds(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("manufacturers")) {
            String[] items = assetFilter.getManufacturers();
            if (!ArrayUtils.contains(assetFilter.getManufacturers(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getManufacturers(), value);
                assetFilter.setManufacturers(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("modelNumbers")) {
            String[] items = assetFilter.getModelNumbers();
            if (!ArrayUtils.contains(assetFilter.getModelNumbers(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getModelNumbers(), value);
                assetFilter.setModelNumbers(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("serialNumbers")) {
            String[] items = assetFilter.getSerialNumbers();
            if (!ArrayUtils.contains(assetFilter.getSerialNumbers(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getSerialNumbers(), value);
                assetFilter.setSerialNumbers(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("descriptions")) {
            String[] items = assetFilter.getDescriptions();
            if (!ArrayUtils.contains(assetFilter.getDescriptions(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getDescriptions(), value);
                assetFilter.setDescriptions(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("statusIds")) {
            Integer[] items = assetFilter.getStatusIds();
            if (!ArrayUtils.contains(assetFilter.getStatusIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(assetFilter.getStatusIds(), new Integer(value));
                assetFilter.setStatusIds(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("softwarelicenseIds")) {
            Integer[] items = assetFilter.getSoftLicIds();
            if (!ArrayUtils.contains(assetFilter.getSoftLicIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(assetFilter.getSoftLicIds(), new Integer(value));
                assetFilter.setSoftLicIds(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        }
        // Accounting info fields
        else if ("poNumbers".equals(type)) {
            String[] items = assetFilter.getPoNumbers();
            if (!ArrayUtils.contains(assetFilter.getPoNumbers(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getPoNumbers(), value);
                assetFilter.setPoNumbers(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("accountNumbers".equals(type)) {
            String[] items = assetFilter.getAccountNumbers();
            if (!ArrayUtils.contains(assetFilter.getAccountNumbers(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getAccountNumbers(), value);
                assetFilter.setAccountNumbers(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("accumulatedNumbers".equals(type)) {
            String[] items = assetFilter.getAccumulatedNumbers();
            if (!ArrayUtils.contains(assetFilter.getAccumulatedNumbers(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getAccumulatedNumbers(), value);
                assetFilter.setAccumulatedNumbers(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("expenseNumbers".equals(type)) {
            String[] items = assetFilter.getExpenseNumbers();
            if (!ArrayUtils.contains(assetFilter.getExpenseNumbers(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getExpenseNumbers(), value);
                assetFilter.setExpenseNumbers(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("acquisitionValues".equals(type)) {
            String[] items = assetFilter.getAcquisitionValues();
            if (!ArrayUtils.contains(assetFilter.getAcquisitionValues(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getAcquisitionValues(), value);
                assetFilter.setAcquisitionValues(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("leaseNumbers".equals(type)) {
            String[] items = assetFilter.getLeaseNumbers();
            if (!ArrayUtils.contains(assetFilter.getLeaseNumbers(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getLeaseNumbers(), value);
                assetFilter.setLeaseNumbers(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("leaseDurations".equals(type)) {
            String[] items = assetFilter.getLeaseDurations();
            if (!ArrayUtils.contains(assetFilter.getLeaseDurations(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getLeaseDurations(), value);
                assetFilter.setLeaseDurations(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("leaseFrequencys".equals(type)) {
            String[] items = assetFilter.getLeaseFrequencys();
            if (!ArrayUtils.contains(assetFilter.getLeaseFrequencys(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getLeaseFrequencys(), value);
                assetFilter.setLeaseFrequencys(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("leaseFrequencyPrices".equals(type)) {
            String[] items = assetFilter.getLeaseFrequencyPrices();
            if (!ArrayUtils.contains(assetFilter.getLeaseFrequencyPrices(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getLeaseFrequencyPrices(), value);
                assetFilter.setLeaseFrequencyPrices(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("disposalMethods".equals(type)) {
            String[] items = assetFilter.getDisposalMethods();
            if (!ArrayUtils.contains(assetFilter.getDisposalMethods(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getDisposalMethods(), value);
                assetFilter.setDisposalMethods(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("insuranceCategories".equals(type)) {
            String[] items = assetFilter.getInsuranceCategories();
            if (!ArrayUtils.contains(assetFilter.getInsuranceCategories(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getInsuranceCategories(), value);
                assetFilter.setInsuranceCategories(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("replacementValuecategories".equals(type)) {
            String[] items = assetFilter.getReplacementValuecategories();
            if (!ArrayUtils.contains(assetFilter.getReplacementValuecategories(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getReplacementValuecategories(), value);
                assetFilter.setReplacementValuecategories(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("replacementValues".equals(type)) {
            String[] items = assetFilter.getReplacementValues();
            if (!ArrayUtils.contains(assetFilter.getReplacementValues(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getReplacementValues(), value);
                assetFilter.setReplacementValues(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("maintenanceCosts".equals(type)) {
            String[] items = assetFilter.getMaintenanceCosts();
            if (!ArrayUtils.contains(assetFilter.getMaintenanceCosts(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getMaintenanceCosts(), value);
                assetFilter.setMaintenanceCosts(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("depreciationMethods".equals(type)) {
            String[] items = assetFilter.getDepreciationMethods();
            if (!ArrayUtils.contains(items, value)) {
                items = (String[]) ArrayUtils.add(items, value);
                assetFilter.setDepreciationMethods(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        }
        // Custom Field
        else if (type.startsWith("customFieldBox")) {
            String cfId = type.split("_")[1];

            Map<String, String[]> custFldMap = assetFilter.getCustomFields();
            if (custFldMap == null) custFldMap = new HashMap<String, String[]>();

            String[] items = custFldMap.get(cfId);
            if (!ArrayUtils.contains(items, value)) {
                items = (String[]) ArrayUtils.add(items, value);
                custFldMap.put(cfId, items);
                assetFilter.setCustomFields(custFldMap);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        }

        response.getOutputStream().print(htmlTable);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/current/deleteItem")
    public void deleteItem(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        // Get the command object from the session
        AssetFilter assetFilter = (AssetFilter) session.getAttribute(AssetFilterFormController.AFC_FORM_ASSETFILTER);

        // get the type and value to be added.
        String type = ServletRequestUtils.getStringParameter(request, "type");
        int row = ServletRequestUtils.getIntParameter(request, "row", -1);

        String htmlTable = "";
        String ctxtPath = request.getContextPath();

        // remove the item from the right array
        if (type.equals("assetNumbers")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getAssetNumbers(), row);
            assetFilter.setAssetNumbers(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("names")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getNames(), row);
            assetFilter.setNames(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("typeIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(assetFilter.getTypeIds(), row);
            assetFilter.setTypeIds(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("ownerIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(assetFilter.getOwnerIds(), row);
            assetFilter.setOwnerIds(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("vendorIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(assetFilter.getVendorIds(), row);
            assetFilter.setVendorIds(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("notes")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getNotes(), row);
            assetFilter.setNotes(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("assetLocations")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getAssetLocations(), row);
            assetFilter.setAssetLocations(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("locationIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(assetFilter.getLocationIds(), row);
            assetFilter.setLocationIds(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("groupIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(assetFilter.getGroupIds(), row);
            assetFilter.setGroupIds(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("categoryIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(assetFilter.getCategoryIds(), row);
            assetFilter.setCategoryIds(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("categoryOptionIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(assetFilter.getCategoryOptionIds(), row);
            assetFilter.setCategoryOptionIds(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("manufacturers")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getManufacturers(), row);
            assetFilter.setManufacturers(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("modelNumbers")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getModelNumbers(), row);
            assetFilter.setModelNumbers(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("serialNumbers")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getSerialNumbers(), row);
            assetFilter.setSerialNumbers(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("descriptions")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getDescriptions(), row);
            assetFilter.setDescriptions(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("statusIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(assetFilter.getStatusIds(), row);
            assetFilter.setStatusIds(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("softwarelicenseIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(assetFilter.getSoftLicIds(), row);
            assetFilter.setSoftLicIds(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        }
        // Accounting info fields
        else if (type.equals("poNumbers")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getPoNumbers(), row);
            assetFilter.setPoNumbers(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("accountNumbers")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getAccountNumbers(), row);
            assetFilter.setAccountNumbers(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("accumulatedNumbers")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getAccumulatedNumbers(), row);
            assetFilter.setAccumulatedNumbers(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("expenseNumbers")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getExpenseNumbers(), row);
            assetFilter.setExpenseNumbers(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("acquisitionValues")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getAcquisitionValues(), row);
            assetFilter.setAcquisitionValues(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("leaseNumbers")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getLeaseNumbers(), row);
            assetFilter.setLeaseNumbers(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("leaseDurations")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getLeaseDurations(), row);
            assetFilter.setLeaseDurations(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("leaseFrequencys")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getLeaseFrequencys(), row);
            assetFilter.setLeaseFrequencys(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("leaseFrequencyPrices")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getLeaseFrequencyPrices(), row);
            assetFilter.setLeaseFrequencyPrices(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("disposalMethods")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getDisposalMethods(), row);
            assetFilter.setDisposalMethods(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("insuranceCategories")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getInsuranceCategories(), row);
            assetFilter.setInsuranceCategories(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("replacementValuecategories")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getReplacementValuecategories(), row);
            assetFilter.setReplacementValuecategories(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("replacementValues")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getReplacementValues(), row);
            assetFilter.setReplacementValues(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("maintenanceCosts")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getMaintenanceCosts(), row);
            assetFilter.setMaintenanceCosts(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("depreciationMethods")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getDepreciationMethods(), row);
            assetFilter.setDepreciationMethods(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        }
        // Custom Fields
        else if (type.startsWith("customFieldBox")) {
            String cfId = type.split("_")[1];

            Map<String, String[]> custFldMap = assetFilter.getCustomFields();
            if (custFldMap == null) custFldMap = new HashMap<String, String[]>();

            String[] items = custFldMap.get(cfId);
            items = (String[]) ArrayUtils.remove(items, row);
            custFldMap.put(cfId, items);
            assetFilter.setCustomFields(custFldMap);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        }

        response.getOutputStream().print(htmlTable);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/current/clearProperty")
    public void clearProperty(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        AssetFilter filter = (AssetFilter) session.getAttribute(AFC_FORM_ASSETFILTER);

        String columnName = ServletRequestUtils.getStringParameter(request, "columnName");
        String cfId = ServletRequestUtils.getStringParameter(request, "cfId", "");

        if(columnName.equals(AssetFilter.COLUMN_ASSETNUMBER)) {
            filter.setAssetNumbers(null);
        } else if(columnName.equals(AssetFilter.COLUMN_NAME)) {
            filter.setName(null);
        } else if(columnName.equals(AssetFilter.COLUMN_TYPE)) {
            filter.setTypeIds(null);
        } else if(columnName.equals(AssetFilter.COLUMN_OWNER)) {
            filter.setOwnerIds(null);
        } else if(columnName.equals(AssetFilter.COLUMN_VENDOR)) {
            filter.setVendorIds(null);
        } else if(columnName.equals(AssetFilter.COLUMN_ASSETLOCATION)) {
            filter.setAssetLocations(null);
        } else if(columnName.equals(AssetFilter.COLUMN_ACQUISITIONDATE)) {
            filter.setAcquisitionDate(null);
            filter.setAcquisitionDate2(null);
            filter.setAcquisitionDateOperator(null);
        } else if(columnName.equals(AssetFilter.COLUMN_LOCATION)) {
            filter.setLocationIds(null);
        } else if(columnName.equals(AssetFilter.COLUMN_GROUP)) {
            filter.setGroupIds(null);
        } else if(columnName.equals(AssetFilter.COLUMN_CATEGORY)) {
            filter.setCategoryIds(null);
        } else if(columnName.equals(AssetFilter.COLUMN_CATEGORYOPTION)) {
            filter.setCategoryOptionIds(null);
        } else if(columnName.equals(AssetFilter.COLUMN_MANUFACTURER)) {
            filter.setManufacturers(null);
        } else if(columnName.equals(AssetFilter.COLUMN_MODELNUMBER)) {
            filter.setModelNumbers(null);
        } else if(columnName.equals(AssetFilter.COLUMN_SERIALNUMBER)) {
            filter.setSerialNumbers(null);
        } else if(columnName.equals(AssetFilter.COLUMN_DESCRIPTION)) {
            filter.setDescriptions(null);
        } else if(columnName.equals(AssetFilter.COLUMN_STATUS)) {
            filter.setStatusIds(null);
        } else if(columnName.equals(AssetFilter.COLUMN_SOFTLICS)) {
            filter.setSoftLicIds(null);
        } else if(columnName.equals(AssetFilter.COLUMN_AI_PONUMBER)) {
            filter.setPoNumbers(null);
        } else if(columnName.equals(AssetFilter.COLUMN_AI_ACCOUNTNUMBER)) {
            filter.setAccountNumbers(null);
        } else if(columnName.equals(AssetFilter.COLUMN_AI_ACCUMULATEDNUMBER)) {
            filter.setAccumulatedNumbers(null);
        } else if(columnName.equals(AssetFilter.COLUMN_AI_EXPENSENUMBER)) {
            filter.setExpenseNumbers(null);
        } else if(columnName.equals(AssetFilter.COLUMN_AI_ACQUISITIONVALUE)) {
            filter.setAcquisitionValues(null);
        } else if(columnName.equals(AssetFilter.COLUMN_AI_LEASENUMBER)) {
            filter.setLeaseNumbers(null);
        } else if(columnName.equals(AssetFilter.COLUMN_AI_LEASEDURATION)) {
            filter.setLeaseDurations(null);
        } else if(columnName.equals(AssetFilter.COLUMN_AI_LEASEFREQUENCY)) {
            filter.setLeaseFrequencys(null);
        } else if(columnName.equals(AssetFilter.COLUMN_AI_LEASEFREQUENCYPRICE)) {
            filter.setLeaseFrequencyPrices(null);
        } else if(columnName.equals(AssetFilter.COLUMN_AI_DISPOSALMETHOD)) {
            filter.setDisposalMethods(null);
        } else if(columnName.equals(AssetFilter.COLUMN_AI_DISPOSALDATE)) {
            filter.setDisposalDate(null);
            filter.setDisposalDate2(null);
            filter.setDisposalDateOperator(null);
        } else if(columnName.equals(AssetFilter.COLUMN_AI_INSURANCECATEGORY)) {
            filter.setInsuranceCategories(null);
        } else if(columnName.equals(AssetFilter.COLUMN_AI_REPLACEMENTVALUECATEGORY)) {
            filter.setReplacementValuecategories(null);
        } else if(columnName.equals(AssetFilter.COLUMN_AI_REPLACEMENTVALUE)) {
            filter.setReplacementValues(null);
        } else if(columnName.equals(AssetFilter.COLUMN_AI_MAINTENANCECOST)) {
            filter.setMaintenanceCosts(null);
        } else if(columnName.equals(AssetFilter.COLUMN_AI_DEPRECIATIONMETHOD)) {
            filter.setDepreciationMethods(null);
        } else if(columnName.equals(AssetFilter.COLUMN_AI_LEASEEXPIRATIONDATE)) {
            filter.setLeaseExpirationDate(null);
            filter.setLeaseExpirationDate2(null);
            filter.setLeaseExpirationDateOperator(null);
        } else if(columnName.equals(AssetFilter.COLUMN_AI_WARRANTYDATE)) {
            filter.setWarrantyDate(null);
            filter.setWarrantyDate2(null);
            filter.setWarrantyDateOperator(null);
        } else if(columnName.startsWith("customField.") && !cfId.equals("")) {
            Map<String, String[]> custFields = filter.getCustomFields();
            custFields.remove(cfId);
            filter.setCustomFields(custFields);
        }


        response.setStatus(HttpServletResponse.SC_OK);
    }

    private AssetFilter createNewAssetFilter() {
        AssetFilter assetFilter = new AssetFilter();
        assetFilter.setPrivateFilter(Boolean.TRUE);

        // load some default columOrders
        String[] defCols = {
                "asset.assetNumber",
                "asset.name",
                "asset.location",
                "asset.assetlocation",
                "asset.group",
                "asset.category",
                "asset.categoryoption"
        };
        assetFilter.setColumnOrder(defCols);
        assetFilter.setName(getMessageSourceAccessor().getMessage("assetFilter.defaultName"));
        return assetFilter;
    }

    private Map<String, Object> referenceData(AssetFilter assetFilter) {
        Map<String, Object> refData = new HashMap<String, Object>();

        if (!ArrayUtils.isEmpty(assetFilter.getTypeIds())) {
            Map<Integer, String> typeIdToNameMap = new HashMap<Integer, String>();
            for (Integer typeId : assetFilter.getTypeIds()) {
                AssetType t = assetTypeService.getById(typeId);
                typeIdToNameMap.put(typeId, t.getName());
            }
            refData.put("typeIdToNameMap", typeIdToNameMap);
        }

        if (!ArrayUtils.isEmpty(assetFilter.getOwnerIds())) {
            Map<Integer, String> ownerIdToNameMap = new HashMap<Integer, String>();
            for (Integer userId : assetFilter.getOwnerIds()) {
                User u = userService.getUserById(userId);
                ownerIdToNameMap.put(userId, u.getLastName() + ", " + u.getFirstName());
            }
            refData.put("ownerIdToNameMap", ownerIdToNameMap);
        }

        if (!ArrayUtils.isEmpty(assetFilter.getVendorIds())) {
            Map<Integer, String> vendorIdToNameMap = new HashMap<Integer, String>();
            for (Integer vendorId : assetFilter.getVendorIds()) {
                Vendor v = vendorService.getById(vendorId);
                vendorIdToNameMap.put(vendorId, v.getName());
            }
            refData.put("vendorIdToNameMap", vendorIdToNameMap);
        }

        if (!ArrayUtils.isEmpty(assetFilter.getLocationIds())) {
            Map<Integer, String> locationIdToNameMap = new HashMap<Integer, String>();
            for (Integer locId : assetFilter.getLocationIds()) {
                Location l = locationService.getLocationById(locId);
                locationIdToNameMap.put(locId, l.getName());
            }
            refData.put("locationIdToNameMap", locationIdToNameMap);
        }

        if (!ArrayUtils.isEmpty(assetFilter.getCategoryIds())) {
            Map<Integer, String> catIdToNameMap = new HashMap<Integer, String>();
            for (Integer catId : assetFilter.getCategoryIds()) {
                Category c = categoryService.getCategoryById(catId);
                catIdToNameMap.put(catId, c.getName());
            }
            refData.put("catIdToNameMap", catIdToNameMap);
        }

        if (!ArrayUtils.isEmpty(assetFilter.getCategoryOptionIds())) {
            Map<Integer, String> catOptIdToNameMap = new HashMap<Integer, String>();
            for (Integer catOptId : assetFilter.getCategoryOptionIds()) {
                CategoryOption c = categoryOptionService.getCategoryOptionById(catOptId);
                catOptIdToNameMap.put(catOptId, c.getName());
            }
            refData.put("catOptIdToNameMap", catOptIdToNameMap);
        }

        if (!ArrayUtils.isEmpty(assetFilter.getStatusIds())) {
            Map<Integer, String> assetStatusIdToNameMap = new HashMap<Integer, String>();
            for (Integer statId : assetFilter.getStatusIds()) {
                AssetStatus s = assetStatusService.getById(statId);
                assetStatusIdToNameMap.put(statId, s.getName());
            }
            refData.put("assetStatusIdToNameMap", assetStatusIdToNameMap);
        }

        if (!ArrayUtils.isEmpty(assetFilter.getSoftLicIds())) {
            Map<Integer, String> softLicIdToNameMap = new HashMap<Integer, String>();
            for (Integer licId : assetFilter.getSoftLicIds()) {
                SoftwareLicense lic = softwareLicenseService.getById(licId);
                softLicIdToNameMap.put(licId, lic.getProductName());
            }
            refData.put("softLicIdToNameMap", softLicIdToNameMap);
        }

        if (!assetFilter.getCustomFields().isEmpty()) {
            Map<String, String> customFieldIdToNameMap = new HashMap<String, String>();
            for (String customFieldId : assetFilter.getCustomFields().keySet()) {
                AssetCustomField customField = assetCustomFieldService.getById(Integer.parseInt(customFieldId));
                customFieldIdToNameMap.put(customFieldId, customField.getName());
            }
            refData.put("customFieldIdToNameMap", customFieldIdToNameMap);
        }

        Map<String, String> allColumns = getColumnTranslationMap(Arrays.asList(AssetFilter.ALL_COLUMNS));
        Map<String, String> criteriaCols = getColumnTranslationMap(getCriteria(assetFilter));

        List<AssetCustomField> assetCustomFields = new ArrayList<AssetCustomField>();
        List<AssetFieldGroup> fieldGroups = assetFieldGroupService.getAllAssetFieldGroups();
        for (AssetFieldGroup assetFieldGroup : fieldGroups) {
            Set<AssetCustomField> customFields = assetFieldGroup.getCustomFields();
            for (AssetCustomField assetCustomField : customFields) {
                assetCustomFields.add(assetCustomField);
            }
        }

        List<AssetReportGroupByOptions> groupByOptions = new ArrayList<AssetReportGroupByOptions>();
        groupByOptions.add(new AssetReportGroupByOptions(AssetReportGroupByOptions.TYPE, getMessageSourceAccessor().getMessage("asset.type")));
        groupByOptions.add(new AssetReportGroupByOptions(AssetReportGroupByOptions.STATUS, getMessageSourceAccessor().getMessage("asset.status")));
        groupByOptions.add(new AssetReportGroupByOptions(AssetReportGroupByOptions.LOCATION, getMessageSourceAccessor().getMessage("asset.location")));
        groupByOptions.add(new AssetReportGroupByOptions(AssetReportGroupByOptions.GROUP, getMessageSourceAccessor().getMessage("asset.group")));
        groupByOptions.add(new AssetReportGroupByOptions(AssetReportGroupByOptions.CATEGORY, getMessageSourceAccessor().getMessage("asset.category")));
        groupByOptions.add(new AssetReportGroupByOptions(AssetReportGroupByOptions.CATEGORY_OPTION, getMessageSourceAccessor().getMessage("asset.categoryoption")));
        groupByOptions.add(new AssetReportGroupByOptions(AssetReportGroupByOptions.OWNER, getMessageSourceAccessor().getMessage("asset.owner")));

        refData.put("allColumns", allColumns);
        refData.put("criteriaCols", criteriaCols);
        refData.put("customFields", assetCustomFields);
        refData.put("assetTypes", assetTypeService.getAllAssetTypes());
        refData.put("vendors", vendorService.getAllVendors());
        refData.put("dateOps", getDateOpOptions());
        refData.put("locations", locationService.getLocations());
        refData.put("groups", groupService.getGroups());
        refData.put("assetStatuses", assetStatusService.getAllAssetStatuses());
        refData.put("softwareLicenses", softwareLicenseService.getAllSoftwareLicenses());
        refData.put("groupByOptions", groupByOptions);

        return refData;
    }

    private Map<String, String> getColumnTranslationMap(List<String> columns) {
        Map<String, String> transmap = new TreeMap<String, String>();
        for(String colName : columns) {
            if(colName.startsWith("asset") || colName.startsWith("accountinginfo.")) {
                transmap.put(colName, getMessageSourceAccessor().getMessage(colName));
            } else {
                transmap.put(colName, StringUtils.replace(colName, "customField.", ""));
            }
        }
        return transmap;
    }

    private List<String> getCriteria(AssetFilter assetFilter) {
        List<String> retVal = new ArrayList<String>();

        if(!ArrayUtils.isEmpty(assetFilter.getAssetNumbers())){
            retVal.add(AssetFilter.COLUMN_ASSETNUMBER);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getNames())){
            retVal.add(AssetFilter.COLUMN_NAME);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getTypeIds())){
            retVal.add(AssetFilter.COLUMN_TYPE);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getOwnerIds())){
            retVal.add(AssetFilter.COLUMN_OWNER);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getVendorIds())){
            retVal.add(AssetFilter.COLUMN_VENDOR);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getAssetLocations())){
            retVal.add(AssetFilter.COLUMN_ASSETLOCATION);
        }
        if(assetFilter.getAcquisitionDateOperator() != null && assetFilter.getAcquisitionDateOperator() != 0){
            retVal.add(AssetFilter.COLUMN_ACQUISITIONDATE);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getLocationIds())){
            retVal.add(AssetFilter.COLUMN_LOCATION);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getGroupIds())){
            retVal.add(AssetFilter.COLUMN_GROUP);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getCategoryIds())){
            retVal.add(AssetFilter.COLUMN_CATEGORY);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getCategoryOptionIds())) {
            retVal.add(AssetFilter.COLUMN_CATEGORYOPTION);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getManufacturers())){
            retVal.add(AssetFilter.COLUMN_MANUFACTURER);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getModelNumbers())){
            retVal.add(AssetFilter.COLUMN_MODELNUMBER);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getSerialNumbers())){
            retVal.add(AssetFilter.COLUMN_SERIALNUMBER);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getDescriptions())){
            retVal.add(AssetFilter.COLUMN_DESCRIPTION);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getStatusIds())){
            retVal.add(AssetFilter.COLUMN_STATUS);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getSoftLicIds())){
            retVal.add(AssetFilter.COLUMN_SOFTLICS);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getPoNumbers())){
            retVal.add(AssetFilter.COLUMN_AI_PONUMBER);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getAccountNumbers())){
            retVal.add(AssetFilter.COLUMN_AI_ACCOUNTNUMBER);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getAccumulatedNumbers())){
            retVal.add(AssetFilter.COLUMN_AI_ACCUMULATEDNUMBER);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getExpenseNumbers())){
            retVal.add(AssetFilter.COLUMN_AI_EXPENSENUMBER);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getAcquisitionValues())){
            retVal.add(AssetFilter.COLUMN_AI_ACQUISITIONVALUE);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getLeaseNumbers())){
            retVal.add(AssetFilter.COLUMN_AI_LEASENUMBER);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getLeaseDurations())){
            retVal.add(AssetFilter.COLUMN_AI_LEASEDURATION);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getLeaseFrequencys())){
            retVal.add(AssetFilter.COLUMN_AI_LEASEFREQUENCY);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getLeaseFrequencyPrices())){
            retVal.add(AssetFilter.COLUMN_AI_LEASEFREQUENCYPRICE);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getDisposalMethods())){
            retVal.add(AssetFilter.COLUMN_AI_DISPOSALMETHOD);
        }
        if(assetFilter.getDisposalDateOperator() != null && assetFilter.getDisposalDateOperator() != 0){
            retVal.add(AssetFilter.COLUMN_AI_DISPOSALDATE);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getInsuranceCategories())){
            retVal.add(AssetFilter.COLUMN_AI_INSURANCECATEGORY);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getReplacementValuecategories())){
            retVal.add(AssetFilter.COLUMN_AI_REPLACEMENTVALUECATEGORY);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getReplacementValues())){
            retVal.add(AssetFilter.COLUMN_AI_REPLACEMENTVALUE);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getMaintenanceCosts())){
            retVal.add(AssetFilter.COLUMN_AI_MAINTENANCECOST);
        }
        if(!ArrayUtils.isEmpty(assetFilter.getDepreciationMethods())){
            retVal.add(AssetFilter.COLUMN_AI_DEPRECIATIONMETHOD);
        }
        if(assetFilter.getLeaseExpirationDateOperator() != null && assetFilter.getLeaseExpirationDateOperator() != 0){
            retVal.add(AssetFilter.COLUMN_AI_LEASEEXPIRATIONDATE);
        }
        if(assetFilter.getWarrantyDateOperator() != null && assetFilter.getWarrantyDateOperator() != 0){
            retVal.add(AssetFilter.COLUMN_AI_WARRANTYDATE);
        }

        // Custom fields
        Map<String, String[]> cfMap = assetFilter.getCustomFields();
        if(cfMap != null){
            for (String cfName : cfMap.keySet()) {
                retVal.add("customField." + cfName);
            }
        }
        return retVal;
    }

    // creates a list of the system date operator options. Used to create a drop-down
    // list for the asset search filters on dates
    private List getDateOpOptions() {
        List<TicketFilterOperators> list = new ArrayList<TicketFilterOperators>();
        // Any
        TicketFilterOperators ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_ANY);
        ticketFilterOperators.setDisplay("-- " + getMessageSourceAccessor().getMessage("ticketSearch.any") + " --");
        list.add(ticketFilterOperators);
        // Today
        ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_TODAY);
        ticketFilterOperators.setDisplay(getMessageSourceAccessor().getMessage("date.today"));
        list.add(ticketFilterOperators);
        // Week
        ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_WEEK);
        ticketFilterOperators.setDisplay(getMessageSourceAccessor().getMessage("date.thisweek"));
        list.add(ticketFilterOperators);
        // Month
        ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_MONTH);
        ticketFilterOperators.setDisplay(getMessageSourceAccessor().getMessage("date.thismonth"));
        list.add(ticketFilterOperators);
        // On
        ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_ON);
        ticketFilterOperators.setDisplay(getMessageSourceAccessor().getMessage("date.on"));
        list.add(ticketFilterOperators);
        // Before
        ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_BEFORE);
        ticketFilterOperators.setDisplay(getMessageSourceAccessor().getMessage("date.before"));
        list.add(ticketFilterOperators);
        // After
        ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_AFTER);
        ticketFilterOperators.setDisplay(getMessageSourceAccessor().getMessage("date.after"));
        list.add(ticketFilterOperators);
        // Range
        ticketFilterOperators = new TicketFilterOperators();
        ticketFilterOperators.setValue(TicketFilterOperators.DATE_RANGE);
        ticketFilterOperators.setDisplay(getMessageSourceAccessor().getMessage("date.range"));
        list.add(ticketFilterOperators);

        return list;
    }

    private String createHtmlTable(String propertyName, List items, String ctxtPath) {
        String deleteMsg = getMessageSourceAccessor().getMessage("global.delete", "Delete");

        String displayName;
        StringBuilder sb = new StringBuilder("<table class=\"filterValues\">");
        for (int i = 0; i < items.size(); i++) {
            displayName = resolveDisplayName(propertyName, items.get(i));
            sb.append("<tr>")
                    .append("<td>").append(displayName).append("</td>")
                    .append("<td class=\"delete\">")
                    .append("  <input type='hidden' name='").append(propertyName).append("' value='").append(items.get(i)).append("'/>")
                    .append("  <a class='deleteItem' href='javascript:void(0)' data-type='").append(propertyName).append("' data-div-id='").append(propertyName).append("' data-row-id='").append(i).append("' title='").append(deleteMsg).append("'>")
                    .append("    <img src='").append(ctxtPath).append("/images/theme/icons/fugue/cross-circle.png' alt=''/>")
                    .append("  </a>")
                    .append("</td>")
                    .append("</tr>");
        }
        sb.append("</table>");
        return sb.toString();
    }

    // 'type' determines whether to do something special to get the display value
    // if nothing special is needed then just returns 'nameOrId' as a string
    private String resolveDisplayName(String type, Object nameOrId) {
        if (type.equals("groupIds")) {
            return groupService.getGroupById((Integer) nameOrId).getName();
        } else if (type.equals("locationIds")) {
            return locationService.getLocationById((Integer) nameOrId).getName();
        } else if (type.equals("categoryIds")) {
            return categoryService.getCategoryById((Integer) nameOrId).getName();
        } else if (type.equals("categoryOptionIds")) {
            return categoryOptionService.getCategoryOptionById((Integer) nameOrId).getName();
        } else if (type.equals("typeIds")) {
            return assetTypeService.getById((Integer) nameOrId).getName();
        } else if (type.equals("ownerIds")) {
            User user = userService.getUserById((Integer) nameOrId);
            return user.getLastName() + ", " + user.getFirstName();
        } else if (type.equals("vendorIds")) {
            return vendorService.getById((Integer) nameOrId).getName();
        } else if (type.equals("statusIds")) {
            return assetStatusService.getById((Integer) nameOrId).getName();
        } else if (type.equals("softwarelicenseIds")) {
            return softwareLicenseService.getById((Integer) nameOrId).getProductName();
        }
        return (String) nameOrId;
    }
}
