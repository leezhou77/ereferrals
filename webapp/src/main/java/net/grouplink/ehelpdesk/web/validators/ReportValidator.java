package net.grouplink.ehelpdesk.web.validators;

import net.grouplink.ehelpdesk.domain.Report;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

public class ReportValidator extends BaseValidator {
    public boolean supports(Class clazz) {
        return clazz.equals(Report.class);
    }

    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "name", "NotEmpty.report.name");
        ValidationUtils.rejectIfEmpty(errors, "ticketFilter", "NotEmpty.report.ticketFilter");

        Report report = (Report)target;
        if(report.getShowRecurrenceSchedule()){
            if(!isValidEmailString(report.getSchedule().getEmailMessageTemplate().getTo())){
                errors.rejectValue("schedule.emailMessageTemplate.to", "mail.validation.address");
            }
        }
    }
}
