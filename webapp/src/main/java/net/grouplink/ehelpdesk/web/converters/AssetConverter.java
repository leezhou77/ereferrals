package net.grouplink.ehelpdesk.web.converters;

import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.service.asset.AssetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

/**
 * author: zpearce
 * Date: 3/27/12
 * Time: 4:41 PM
 */
public class AssetConverter implements Converter<String, Asset> {
    @Autowired private AssetService assetService;
    
    public Asset convert(String id) {
        try {
            int aid = Integer.parseInt(id);
            return assetService.getAssetById(aid);
        } catch (Exception e) {
            return null;
        }
    }
}
