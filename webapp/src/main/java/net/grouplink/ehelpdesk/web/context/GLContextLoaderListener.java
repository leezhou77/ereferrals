package net.grouplink.ehelpdesk.web.context;

import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.ContextLoaderListener;

/**
 * User: jaymehafen
 * Date: Mar 8, 2009
 * Time: 9:00:42 PM
 */
public class GLContextLoaderListener extends ContextLoaderListener {
    @Override
    protected ContextLoader createContextLoader() {
        return new GLContextLoader();    
    }
}
