package net.grouplink.ehelpdesk.web.controllers;

import net.grouplink.ehelpdesk.common.beans.support.GLPagedListSourceProvider;
import net.grouplink.ehelpdesk.common.beans.support.GLRefreshablePagedListHolder;
import net.grouplink.ehelpdesk.common.utils.MailConfig;
import net.grouplink.ehelpdesk.common.utils.MailMessage;
import net.grouplink.ehelpdesk.domain.Attachment;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.KnowledgeBase;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.KnowledgeBaseService;
import net.grouplink.ehelpdesk.service.MailService;
import net.grouplink.ehelpdesk.service.TicketService;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.web.filters.KBManagementFilter;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * @author mrollins
 * @version 1.0
 */
public class KBSearchController extends MultiActionController implements InitializingBean {

    private KnowledgeBaseService knowledgeBaseService;
    private MailService mailService;
    private UserRoleGroupService userRoleGroupService;
    private UserService userService;
    private GroupService groupService;
    private TicketService ticketService;

    public ModelAndView handleKbList(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        assert response != null;

        HttpSession session = request.getSession(true);
        session.setAttribute("showHeader", Boolean.TRUE);

        GLRefreshablePagedListHolder listHolder =
                (GLRefreshablePagedListHolder) session.getAttribute(SessionConstants.ATTR_KB_LIST);

        Locale locale = RequestContextUtils.getLocale(request);

        Integer pageSize = 10;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if ("kbSearchPageSize".equals(cookie.getName())) {
                    pageSize = new Integer(cookie.getValue());
                }
            }
        }

        if (listHolder == null) {
            listHolder = new GLRefreshablePagedListHolder();
            listHolder.setMaxLinkedPages(20);
            listHolder.setPageSize(pageSize);
            listHolder.setSourceProvider(new KBProvider());
            KBManagementFilter filter = new KBManagementFilter();
            filter.setSearchText("%");
            filter.setUser(getUserFromSession(request));
            listHolder.setFilter(filter);
            session.setAttribute(SessionConstants.ATTR_KB_LIST, listHolder);
        }

        processSortToggling(request, session, listHolder);

        ServletRequestDataBinder bdr = new ServletRequestDataBinder(listHolder, "kbList");
        bdr.bind(request);

        //ToDo:  Create a KBUserFilter for ROLE_USER only.
        KBManagementFilter _filter = (KBManagementFilter) listHolder.getFilter();
        if (_filter.getSearchText().trim().equals("")) {
            _filter.setSearchText("%");
            listHolder.setFilter(_filter);
        }

        listHolder.setLocale(locale);
        listHolder.setSource(listHolder.getSourceProvider().loadList(locale, listHolder.getFilter()));
        int page = listHolder.getPage();
        listHolder.resort();
        listHolder.setPage(page);

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("listHolder", bdr.getBindingResult().getModel());
        map.put("groupList", groupService.getGroups());
        return new ModelAndView("kbSearch", map);
    }

    public ModelAndView kbManagement(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        assert response != null;

        HttpSession session = request.getSession(true);

        Boolean showHeader = ServletRequestUtils.getBooleanParameter(request, "showHeader", true);
        session.setAttribute("showHeader", showHeader);

        User user = (User) session.getAttribute(SessionConstants.ATTR_USER);
        String groupId = ServletRequestUtils.getStringParameter(request, "groupId");
        List<Group> groupList = new ArrayList<Group>();
        if (user.getLoginId().equals(User.ADMIN_LOGIN)) {
            groupList = groupService.getGroups();
        } else {
            List<UserRoleGroup> urgList = userRoleGroupService.getUserRoleGroupByUserId(user.getId());
            for (UserRoleGroup anUrgList : urgList) {
                groupList.add(anUrgList.getGroup());
            }
        }

        UserRoleGroup urg = null;
        if (StringUtils.isNotBlank(groupId)) {
            urg = userRoleGroupService.getUserRoleGroupByUserIdAndGroupId(user.getId(), new Integer(groupId));
        }

        if (urg == null) {
            groupId = "-1";
        }

        GLRefreshablePagedListHolder listHolder =
                (GLRefreshablePagedListHolder) session.getAttribute(SessionConstants.ATTR_KB_MANAGEMENT_LIST);

        Locale locale = RequestContextUtils.getLocale(request);

        Integer pageSize = 10;
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if ("kbMgmtPageSize".equals(cookie.getName())) {
                pageSize = new Integer(cookie.getValue());
            }
        }

        if (listHolder == null) {
            listHolder = new GLRefreshablePagedListHolder();
            listHolder.setMaxLinkedPages(20);
            listHolder.setPageSize(pageSize);
            listHolder.setSourceProvider(new KBManagementProvider());
            KBManagementFilter filter = new KBManagementFilter(groupId);
            filter.setUser(getUserFromSession(request));
            listHolder.setFilter(filter);
            session.setAttribute(SessionConstants.ATTR_KB_MANAGEMENT_LIST, listHolder);
        }

        processSortToggling(request, session, listHolder);

        ServletRequestDataBinder bdr = new ServletRequestDataBinder(listHolder, "kbList");
        bdr.bind(request);

        listHolder.setLocale(locale);
        listHolder.setSource(listHolder.getSourceProvider().loadList(locale, listHolder.getFilter()));
        int page = listHolder.getPage();
        listHolder.resort();
        listHolder.setPage(page);

        Map<String, Object> map = new HashMap<String, Object>();
        Map lhMap = bdr.getBindingResult().getModel();
        map.put("listHolder", lhMap);
        map.put("groupId", groupId);
        map.put("groupList", groupList);
        map.put("refreshUrl", "/kbManagement/kbManagement.glml");
        return new ModelAndView("kbManagement", map);
    }

    private void processSortToggling(HttpServletRequest request, HttpSession session, GLRefreshablePagedListHolder listHolder) {
        Boolean refreshParent = Boolean.valueOf(String.valueOf(session.getAttribute("refreshParent")));
        if (request.getParameter("pageSize") != null || refreshParent) {
            ((MutableSortDefinition) listHolder.getSort()).setToggleAscendingOnProperty(false);
            session.setAttribute("refreshParent", "false");
        } else {
            if (!StringUtils.isBlank(listHolder.getSort().getProperty())) {
                ((MutableSortDefinition) listHolder.getSort()).setToggleAscendingOnProperty(true);
            }
        }
    }

    public ModelAndView removeKBAttach(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        assert response != null;

        String kbId = ServletRequestUtils.getStringParameter(request, "kbId");
        String attachId = ServletRequestUtils.getStringParameter(request, "attachId");
        if (StringUtils.isNotBlank(kbId) && StringUtils.isNotBlank(attachId)) {
            KnowledgeBase kb = knowledgeBaseService.getKnowledgeBaseById(new Integer(kbId));
            if (kb.getIsPrivate()) {
                if (!hasPrivateKbAccess(kb, getUserFromSession(request))) {
                    throw new RuntimeException("access denied");
                }
            }

            Integer attId = new Integer(attachId);
            List<Attachment> attachList = new ArrayList<Attachment>(kb.getAttachments());
            for (Attachment anAttachList : attachList) {
                if (anAttachList.getId().equals(attId)) {
                    kb.getAttachments().remove(anAttachList);
                    knowledgeBaseService.deleteKnowledgeBaseAttachments(anAttachList);
                    break;
                }
            }

            response.setStatus(HttpServletResponse.SC_OK);
        }

        return null;
    }

    public ModelAndView kbView(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        assert response != null;

        String kbId = ServletRequestUtils.getStringParameter(request, "kbId");
        if (StringUtils.isBlank(kbId)) {
            return new ModelAndView(new RedirectView("kbSearch.glml"));
        }

        KnowledgeBase kb = knowledgeBaseService.getKnowledgeBaseById(new Integer(kbId));

        if (kb == null) {
            return new ModelAndView(new RedirectView("kbSearch.glml"));
        }
        
        // check if private
        if (kb.getIsPrivate()) {
            User user = getUserFromSession(request);
            if (user == null) {
                // since kb is private, user is required. redirect to secure kbView page
                return new ModelAndView(new RedirectView("/kbManagement/kbView.glml?kbId=" + kbId, true));
            }

            if (!hasPrivateKbAccess(kb, user)) {
                Map<String, Object> model = new HashMap<String, Object>();
                model.put("kbAccess", Boolean.TRUE);
                model.put("message", "global.accessDenied");
                return new ModelAndView("noTicketAccess", model);
            }
        }

        // update the timesViewed counter
        int counter = kb.getTimesViewed();
        kb.setTimesViewed(counter + 1);
        knowledgeBaseService.saveKnowledgeBase(kb);

        return new ModelAndView("kbView", "kb", kb);
    }

    public ModelAndView kbRemove(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        assert response != null;

        String kbId = ServletRequestUtils.getStringParameter(request, "kbId");
        boolean showHeader = ServletRequestUtils.getBooleanParameter(request, "showHeader", true);
        if (StringUtils.isBlank(kbId)) {
            return new ModelAndView(new RedirectView("kbSearch.glml"));
        }

        KnowledgeBase kb = knowledgeBaseService.getKnowledgeBaseById(new Integer(kbId));
        if (kb.getIsPrivate()) {
            if (!hasPrivateKbAccess(kb, getUserFromSession(request))) {
                throw new RuntimeException("access denied");
            }
        }

        // remove kb from any associated tickets
        if (CollectionUtils.isNotEmpty(kb.getTickets())) {
            for (Ticket ticket : kb.getTickets()) {
                if (ticket.getKnowledgeBase().contains(kb)) {
                    ticket.getKnowledgeBase().remove(kb);
                    ticketService.saveTicket(ticket, getUserFromSession(request));
                }
            }
        }

        knowledgeBaseService.deleteKnowledgeBase(kb);
        return new ModelAndView(new RedirectView("kbManagement.glml?forceRefresh=true&showHeader=" + showHeader));
    }

    public ModelAndView kbSend(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        assert response != null;

        String kbId = ServletRequestUtils.getStringParameter(request, "kbId");
        String eType = ServletRequestUtils.getStringParameter(request, "emailType");
        String email = ServletRequestUtils.getStringParameter(request, "email");
        if (StringUtils.isBlank(kbId) || StringUtils.isBlank(email)) {
            return new ModelAndView(new RedirectView("kbView.glml?kbId=" + kbId));
        }

        KnowledgeBase kb = knowledgeBaseService.getKnowledgeBaseById(new Integer(kbId));
        User user = getUserFromSession(request);
        if (kb.getIsPrivate()) {
            if (!hasPrivateKbAccess(kb, user)) {
                throw new RuntimeException("access denied");
            }
        }

        MailMessage msg = mailService.newMessage();
        try {
            if (user != null) {
                msg.setFrom(user.getEmail(), user.getLastName() + ", " + user.getFirstName());
            } else {
                MailConfig mc = mailService.getMailConfig();
                msg.setFrom(mc.getEmailFromAddress(), mc.getEmailDisplayName());
            }

            msg.setTo(email);
            msg.setSubject(getMessageSourceAccessor().getMessage("kb.knowledgeBaseArticle") + " #" + kb.getId() + " - " + kb.getSubject());
            if (eType.equals("1")) {
                msg.setText(kbToText(kb), kbToHTML(kb));
            } else {
                msg.setText(kbLink(request, kb));
            }

            mailService.sendMail(msg);
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return new ModelAndView(new RedirectView("kbView.glml?kbId=" + kb.getId()));
    }

    private String kbToHTML(KnowledgeBase kb) {
        StringBuilder html = new StringBuilder("<table style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;\" cellpadding=\"6\" cellspacing=\"1\" border=\"0\" width=\"99%\" align=\"center\"><tr><td ><div><strong>");
        html.append(getMessageSourceAccessor().getMessage("kb.articleNumber"));
        html.append("</strong> ");
        html.append(kb.getId());
        html.append("<br/><strong>");
        html.append(getMessageSourceAccessor().getMessage("kb.lastModified"));
        html.append(":</strong> ");
        SimpleDateFormat sp = new SimpleDateFormat("MMM d, yyyy");
        html.append(sp.format(kb.getDate()));
        html.append("</div></td></tr><tr><td><div><strong>");
        html.append(kb.getSubject());
        html.append("</strong></div><hr size=\"1\" style=\"color:#D1D1E1\" /><div>");
        html.append(kb.getProblem());
        html.append("</div><hr size=\"1\" style=\"color:#D1D1E1\" /><div>");
        html.append(kb.getResolution());
        html.append("</div></td></tr></table>");
        return html.toString();
    }

    private String kbToText(KnowledgeBase kb) {
        StringBuilder text = new StringBuilder(getMessageSourceAccessor().getMessage("kb.articleNumber"));
        text.append(" ");
        text.append(kb.getId());
        text.append("\n");
        text.append(getMessageSourceAccessor().getMessage("kb.lastModified"));
        text.append(": ");
        SimpleDateFormat sp = new SimpleDateFormat("MMM d, yyyy");
        text.append(sp.format(kb.getDate()));
        text.append("\n\n");
        text.append(kb.getSubject());
        text.append("\n\n");
        text.append(kb.getProblemText());
        text.append("\n\n");
        text.append(kb.getResolutionText());
        return text.toString();
    }

    private String kbLink(HttpServletRequest request, KnowledgeBase kb) {
        StringBuffer text = request.getRequestURL();
        text.replace(text.lastIndexOf("/"), text.length(), "/kbView.glml");
        text.append("?kbId=").append(kb.getId());
        return text.toString();
    }

    public ModelAndView makePrivate(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        assert response != null;

        String kbId = ServletRequestUtils.getStringParameter(request, "kbId");
        String type = ServletRequestUtils.getStringParameter(request, "type");
        KnowledgeBase kb = new KnowledgeBase();
        if (StringUtils.isNotBlank(kbId)) {
            kb = knowledgeBaseService.getKnowledgeBaseById(new Integer(kbId));
            if (kb.getIsPrivate()) {
                if (!hasPrivateKbAccess(kb, getUserFromSession(request))) {
                    throw new RuntimeException("access denied");
                }
            }

            if (StringUtils.isNotBlank(type) && type.equals("private")) {
                kb.setIsPrivate(Boolean.TRUE);
            }

            if (StringUtils.isNotBlank(type) && type.equals("public")) {
                kb.setIsPrivate(Boolean.FALSE);
            }

            knowledgeBaseService.saveKnowledgeBase(kb);
        }

        return new ModelAndView(new RedirectView("kbView.glml?kbId=" + kb.getId()));
    }

    public ModelAndView resetCounter(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        assert response != null;

        String kbId = ServletRequestUtils.getStringParameter(request, "kbId");
        KnowledgeBase kb = new KnowledgeBase();
        if (StringUtils.isNotBlank(kbId)) {
            kb = knowledgeBaseService.getKnowledgeBaseById(new Integer(kbId));
            if (kb.getIsPrivate()) {
                if (!hasPrivateKbAccess(kb, getUserFromSession(request))) {
                    throw new RuntimeException("access denied");
                }
            }

            kb.setTimesViewed(0);
            knowledgeBaseService.saveKnowledgeBase(kb);
        }

        return new ModelAndView(new RedirectView("kbView.glml?kbId=" + kb.getId()));
    }

    private User getUserFromSession(HttpServletRequest request) {
        return (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);
    }

    private boolean hasPrivateKbAccess(KnowledgeBase kb, User user) {
        boolean canView = false;
        user = userService.getUserById(user.getId());
        if (user.getId().equals(User.ADMIN_ID))
            return true;

        List<Group> groups = user.getGroups();
        if (groups.contains(kb.getGroup())) {
            canView = true;
        }

        return canView;
    }

    public void setKnowledgeBaseService(KnowledgeBaseService knowledgeBaseService) {
        this.knowledgeBaseService = knowledgeBaseService;
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    /**
     * @param mailService the mailService to set
     */
    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    private class KBProvider implements GLPagedListSourceProvider {

        public List loadList(Locale loc, Object filter) {
            KBManagementFilter tf = (KBManagementFilter) filter;
            return knowledgeBaseService.searchKnowLedgeBase(tf.getSearchText(), new Integer(tf.getSearchType()), new Integer(tf.getGroupId()), tf.getUser());
        }
    }

    private class KBManagementProvider implements GLPagedListSourceProvider {

        public List loadList(Locale loc, Object filter) {
            KBManagementFilter tf = (KBManagementFilter) filter;
            return knowledgeBaseService.searchKnowLedgeBaseByGroupId(tf.getSearchText(), new Integer(tf.getSearchType()), new Integer(tf.getGroupId()), tf.getUser());
        }
    }

    public void afterPropertiesSet() throws Exception {
        String propName = null;
        if (knowledgeBaseService == null) propName = "knowledgeBaseService";
        if (mailService == null) propName = "mailService";

        if (propName != null)
            throw new BeanCreationException(getClass().getName() + " Property not set: " + propName);
    }
}