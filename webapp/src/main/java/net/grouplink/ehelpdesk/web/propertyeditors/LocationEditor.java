package net.grouplink.ehelpdesk.web.propertyeditors;

import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.service.LocationService;
import org.apache.commons.lang.StringUtils;

import java.beans.PropertyEditorSupport;


public class LocationEditor extends PropertyEditorSupport {

    private LocationService locationService;

    /**
     * Constructor for use by derived PropertyEditor classes.
     */
    public LocationEditor() {
    }

    /**
     * Constructor for use by derived PropertyEditor classes.
     *
     * @param locationService service for getting locations
     */
    public LocationEditor(LocationService locationService) {
        this.locationService = locationService;
    }

    /**
     * Gets the property value as a string suitable for presentation
     * to a human to edit.
     *
     * @return The property value as a string suitable for presentation
     *         to a human to edit.
     *         <p>   Returns "null" is the value can't be expressed as a string.
     *         <p>   If a non-null value is returned, then the PropertyEditor should
     *         be prepared to parse that string back in setAsText().
     */
    public String getAsText() {
        Location location = (Location) getValue();
        return (location != null) ? location.getId().toString() : "";
    }

    /**
     * Sets the property value by parsing a given String.  May raise
     * java.lang.IllegalArgumentException if either the String is
     * badly formatted or if this kind of property can't be expressed
     * as text.
     *
     * @param text The string to be parsed.
     */
    public void setAsText(String text) throws IllegalArgumentException {
        if (StringUtils.isNotBlank(text)){
            setValue(locationService.getLocationById(new Integer(text)));
        } else {
            setValue(null);
        }
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

}