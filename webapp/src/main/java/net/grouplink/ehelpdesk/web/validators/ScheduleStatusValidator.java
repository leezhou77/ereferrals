package net.grouplink.ehelpdesk.web.validators;

import net.grouplink.ehelpdesk.domain.ScheduleStatus;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * @author mrollins
 * @version 1.0
 */
public class ScheduleStatusValidator extends BaseValidator {
    private Validator validator;

    public ScheduleStatusValidator(Validator validator) {
        this.validator = validator;
    }

    public boolean supports(Class clazz) {
        return clazz.equals(ScheduleStatus.class);
    }

    public void validate(Object target, Errors errors) {
        ScheduleStatus ss = (ScheduleStatus)target;

        validator.validate(target, errors);

        // if email is chosen, check that a recipient and action frequency are supplied
        if(ss.isSendMail()){
            if (!ss.getActionFrequencyOneTime() && !isParsableAsNumber(ss.getActionFrequencyDigit())) {
                errors.rejectValue("actionFrequencyDigit", "scheduler.validate.actionFrequency", "Frequency must be a number");
            }

            if (StringUtils.contains(ss.getMailSubject(), "{NOTE}")) {
                errors.rejectValue("mailSubject", "scheduler.validate.mailSubjectNote", "Cannot use {NOTE} in mail subject line");
            }
        }
    }
}
