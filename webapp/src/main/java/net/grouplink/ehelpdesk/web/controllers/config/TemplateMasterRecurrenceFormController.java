package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.domain.RecurrenceSchedule;
import net.grouplink.ehelpdesk.domain.TicketTemplateMaster;
import net.grouplink.ehelpdesk.service.TemplateMasterService;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TemplateMasterRecurrenceFormController extends SimpleFormController {
    private TemplateMasterService templateMasterService;

    public TemplateMasterRecurrenceFormController() {
        setFormView("ticketTemplates/templateMasterRecurrenceEdit");
        setCommandClass(RecurrenceSchedule.class);
        setCommandName("schedule");
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        Integer templateMasterId = ServletRequestUtils.getIntParameter(request, "ticketTemplateMasterId");
        TicketTemplateMaster templateMaster = templateMasterService.getById(templateMasterId);
        RecurrenceSchedule recurrenceSchedule;
        if (templateMaster.getRecurrenceSchedule() != null) {
            recurrenceSchedule = templateMaster.getRecurrenceSchedule();
        } else {
            recurrenceSchedule = new RecurrenceSchedule();
            recurrenceSchedule.setPattern(RecurrenceSchedule.PATTERN_DAILY);
            recurrenceSchedule.setDailyInterval(1);
            recurrenceSchedule.setWeeklyInterval(1);
            recurrenceSchedule.setMonthlySchedule(RecurrenceSchedule.MONTHLY_DAY_OF_MONTH);
            recurrenceSchedule.setMonthlyDayOfMonth(1);
            recurrenceSchedule.setMonthlyDayOfMonthOccurrence(1);
            recurrenceSchedule.setYearlySchedule(RecurrenceSchedule.YEARLY_DAY_OF_YEAR);
            recurrenceSchedule.setYearlyDayOfYearDay(1);
            recurrenceSchedule.setYearlyDayOfYearMonth(1);
            recurrenceSchedule.setRangeStartDate(new Date());
            recurrenceSchedule.setRangeEnd(RecurrenceSchedule.RANGE_END_NEVER);
            recurrenceSchedule.setTicketTemplateMaster(templateMaster);
            templateMaster.setRecurrenceSchedule(recurrenceSchedule);
        }

        return recurrenceSchedule;
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        RecurrenceSchedule schedule = (RecurrenceSchedule) command;

        TicketTemplateMaster master = schedule.getTicketTemplateMaster();
        templateMasterService.save(master);
        
        return null;
    }

    @Override
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
        super.initBinder(request, binder);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
    }

    public void setTemplateMasterService(TemplateMasterService templateMasterService) {
        this.templateMasterService = templateMasterService;
    }
}
