package net.grouplink.ehelpdesk.web.validators;

import net.grouplink.ehelpdesk.domain.MyTicketTab;
import net.grouplink.ehelpdesk.web.domain.MyTicketsConfig;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;

public class MyTicketsConfigValidator extends BaseValidator {

    public boolean supports(Class clazz) {
        return clazz.equals(MyTicketsConfig.class);
    }

    public void validate(Object target, Errors errors) {
        MyTicketsConfig mtc = (MyTicketsConfig) target;
        // check that each of the mytickettabs has a name and ticketfilter
        for (MyTicketTab myTicketTab : mtc.getMyTicketTabs()) {
            if (StringUtils.isBlank(myTicketTab.getName())) {
                errors.reject("myTicketTabs.tabName.required");
            }

            if (myTicketTab.getTicketFilter() == null) {
                errors.reject("myTicketTabs.ticketFilter.required");
            }
        }
    }
}
