package net.grouplink.ehelpdesk.web.controllers.asset;

import net.grouplink.ehelpdesk.domain.asset.AssetStatus;
import net.grouplink.ehelpdesk.service.asset.AssetStatusService;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * User: jaymehafen
 * Date: Jul 31, 2008
 * Time: 12:39:54 PM
 */
public class AssetStatusFormController extends SimpleFormController {
    private AssetStatusService assetStatusService;

    public AssetStatusFormController() {
        setCommandClass(AssetStatus.class);
        setCommandName("assetStatus");
        setFormView("assetStatusEdit");
    }

    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        AssetStatus assetStatus;
        String assetStatusId = ServletRequestUtils.getStringParameter(request, "assetStatusId");
        if (StringUtils.isNotBlank(assetStatusId)) {
            assetStatus = assetStatusService.getById(new Integer(assetStatusId));
        } else {
            assetStatus = new AssetStatus();
        }

        return assetStatus;
    }

    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        AssetStatus assetStatus = (AssetStatus) command;
        assetStatusService.saveAssetStatus(assetStatus);
        return new ModelAndView(new RedirectView("statuses.glml"));
    }

    public void setAssetStatusService(AssetStatusService assetStatusService) {
        this.assetStatusService = assetStatusService;
    }
}
