package net.grouplink.ehelpdesk.web.controllers;

import net.grouplink.ehelpdesk.common.utils.PropertiesConstants;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketHistory;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.ZenAsset;
import net.grouplink.ehelpdesk.domain.zen.Workstation;
import net.grouplink.ehelpdesk.service.PropertiesService;
import net.grouplink.ehelpdesk.service.TicketService;
import net.grouplink.ehelpdesk.service.asset.AssetService;
import net.grouplink.ehelpdesk.service.asset.ZenAssetService;
import net.grouplink.ehelpdesk.service.zen.WorkstationService;
import net.grouplink.ehelpdesk.service.zen.ZenSystemSettingService;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * User: jaymehafen
 * Date: Apr 22, 2009
 * Time: 1:35:08 PM
 */
public class WebstartController extends MultiActionController {
    private PropertiesService propertiesService;
    private ZenAssetService zenAssetService;
    private WorkstationService workstationService;
    private TicketService ticketService;
    private ZenSystemSettingService zenSystemSettingService;
    private AssetService assetService;

    public ModelAndView dashboard(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        Map<String, Object> model = new HashMap<String, Object>();

        String baseUrl = propertiesService.getPropertiesValueByName(PropertiesConstants.BASE_URL);
        String dashboardId = ServletRequestUtils.getStringParameter(request, "dashboardId");
        User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);
        Locale locale = LocaleContextHolder.getLocale();

        model.put("baseUrl", baseUrl);
        model.put("dashboardId", dashboardId);
        model.put("userId", user.getId());
        model.put("locale", locale.toString());

        response.setContentType("application/x-java-jnlp-file");
        return new ModelAndView("dashboard", model);
    }

    public ModelAndView vncviewer(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        assert request != null;
        
        Map<String, Object> model = new HashMap<String, Object>();

        String baseUrl = propertiesService.getPropertiesValueByName(PropertiesConstants.BASE_URL);
        String host = null;
        String port = null;
        Integer zenAssetId = ServletRequestUtils.getIntParameter(request, "zenAssetId");
        if (zenAssetId != null) {
            ZenAsset zenAsset = zenAssetService.getById(zenAssetId);
            Workstation workstation = getWorkstationService().getById(zenAsset.getWorkstationId());
            host = workstation.getIpAddress();
            port = getZenSystemSettingService().getRemotePort();
            if (StringUtils.isBlank(port)) {
                port = "5950";
            }

            Integer ticketId = ServletRequestUtils.getIntParameter(request, "ticketId");
            if (ticketId != null) {
                // add history comment to ticket
                User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);
                Ticket ticket = ticketService.getTicketById(ticketId);
                TicketHistory th = new TicketHistory();
                th.setTicket(ticket);
                Date createdDate = new Date();
                th.setCreatedDate(createdDate);
                Locale locale = LocaleContextHolder.getLocale();
                DateFormat df = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, locale);
                String note = getMessageSourceAccessor().getMessage("remoteControl.requestedNote", new Object[]{user.getFirstName() + " " + user.getLastName(), zenAsset.getAssetName(), df.format(createdDate)});
                th.setNote(note);
                th.setSubject(getMessageSourceAccessor().getMessage("remoteControl.remoteControl"));
                th.setUser(user);
                ticket.addTicketHistory(th);
                ticketService.saveTicket(ticket, user);
            }
        }

        model.put("baseUrl", baseUrl);
        model.put("host", host);
        model.put("port", port);

        response.setContentType("application/x-java-jnlp-file");
        return new ModelAndView("tightvnc-viewer", model);
    }

    public ModelAndView vncviewerAsset(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        Map<String, Object> model = new HashMap<String, Object>();

        String baseUrl = propertiesService.getPropertiesValueByName(PropertiesConstants.BASE_URL);

        Integer assetId = ServletRequestUtils.getIntParameter(request, "assetId");
        if (assetId == null) {
            throw new ServletException("assetId cannot be null");
        }

        Asset asset = assetService.getAssetById(assetId);
        String host = asset.getHostname();
        String port = asset.getVncPort().toString();

        Integer ticketId = ServletRequestUtils.getIntParameter(request, "ticketId");
        if (ticketId != null) {
            // add history comment to ticket
            User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);
            Ticket ticket = ticketService.getTicketById(ticketId);
            TicketHistory th = new TicketHistory();
            th.setTicket(ticket);
            Date createdDate = new Date();
            th.setCreatedDate(createdDate);
            Locale locale = LocaleContextHolder.getLocale();
            DateFormat df = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, locale);
            String note = getMessageSourceAccessor().getMessage("remoteControl.requestedNote", new Object[]{user.getFirstName() + " " + user.getLastName(), asset.getName(), df.format(createdDate)});
            th.setNote(note);
            th.setSubject(getMessageSourceAccessor().getMessage("remoteControl.remoteControl"));
            th.setUser(user);
            ticket.addTicketHistory(th);
            ticketService.saveTicket(ticket, user);
        }

        model.put("baseUrl", baseUrl);
        model.put("host", host);
        model.put("port", port);

        response.setContentType("application/x-java-jnlp-file");
        return new ModelAndView("tightvnc-viewer", model);
    }

    public void setPropertiesService(PropertiesService propertiesService) {
        this.propertiesService = propertiesService;
    }

    public void setZenAssetService(ZenAssetService zenAssetService) {
        this.zenAssetService = zenAssetService;
    }

    public WorkstationService getWorkstationService() {
        if (workstationService == null) {
            workstationService = (WorkstationService) getApplicationContext().getBean("workstationService");
        }
        
        return workstationService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public ZenSystemSettingService getZenSystemSettingService() {
        if (zenSystemSettingService == null) {
            zenSystemSettingService = (ZenSystemSettingService) getApplicationContext().getBean("zenSystemSettingService");
        }

        return zenSystemSettingService;
    }

    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }
}
