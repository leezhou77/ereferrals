package net.grouplink.ehelpdesk.web.dwr.dto;

import java.util.Date;

public class MassUpdateDto {

    private String[] cbx_workTime;
    private String workTime;

    private String[] cbx_contact;
    private Integer contact;

    private String[] cbx_assignedTo;
    private Integer assignedTo;

    private String[] cbx_priority;
    private Integer priority;

    private String[] cbx_status;
    private Integer status;

    private String[] cbx_estimatedDate;
    private Date estimatedDate;

    private String[] cbx_addComment;
    private String addCommentSubject;
    private String addCommentNote;

    private String[] cbx_groupAsChildren;
    private Integer groupAsChildren;

    private String gridId;

    public String[] getCbx_workTime() {
        return cbx_workTime;
    }

    public void setCbx_workTime(String[] cbx_workTime) {
        this.cbx_workTime = cbx_workTime;
    }

    public String getWorkTime() {
        return workTime;
    }

    public void setWorkTime(String workTime) {
        this.workTime = workTime;
    }

    public String[] getCbx_contact() {
        return cbx_contact;
    }

    public void setCbx_contact(String[] cbx_contact) {
        this.cbx_contact = cbx_contact;
    }

    public Integer getContact() {
        return contact;
    }

    public void setContact(Integer contact) {
        this.contact = contact;
    }

    public String[] getCbx_assignedTo() {
        return cbx_assignedTo;
    }

    public void setCbx_assignedTo(String[] cbx_assignedTo) {
        this.cbx_assignedTo = cbx_assignedTo;
    }

    public Integer getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(Integer assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String[] getCbx_priority() {
        return cbx_priority;
    }

    public void setCbx_priority(String[] cbx_priority) {
        this.cbx_priority = cbx_priority;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String[] getCbx_status() {
        return cbx_status;
    }

    public void setCbx_status(String[] cbx_status) {
        this.cbx_status = cbx_status;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String[] getCbx_estimatedDate() {
        return cbx_estimatedDate;
    }

    public void setCbx_estimatedDate(String[] cbx_estimatedDate) {
        this.cbx_estimatedDate = cbx_estimatedDate;
    }

    public Date getEstimatedDate() {
        return estimatedDate;
    }

    public void setEstimatedDate(Date estimatedDate) {
        this.estimatedDate = estimatedDate;
    }

    public String[] getCbx_addComment() {
        return cbx_addComment;
    }

    public void setCbx_addComment(String[] cbx_addComment) {
        this.cbx_addComment = cbx_addComment;
    }

    public String getAddCommentSubject() {
        return addCommentSubject;
    }

    public void setAddCommentSubject(String addCommentSubject) {
        this.addCommentSubject = addCommentSubject;
    }

    public String getAddCommentNote() {
        return addCommentNote;
    }

    public void setAddCommentNote(String addCommentNote) {
        this.addCommentNote = addCommentNote;
    }

    public String[] getCbx_groupAsChildren() {
        return cbx_groupAsChildren;
    }

    public void setCbx_groupAsChildren(String[] cbx_groupAsChildren) {
        this.cbx_groupAsChildren = cbx_groupAsChildren;
    }

    public Integer getGroupAsChildren() {
        return groupAsChildren;
    }

    public void setGroupAsChildren(Integer groupAsChildren) {
        this.groupAsChildren = groupAsChildren;
    }

    public String getGridId() {
        return gridId;
    }

    public void setGridId(String gridId) {
        this.gridId = gridId;
    }
}
