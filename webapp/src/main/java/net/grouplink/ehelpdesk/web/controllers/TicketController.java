package net.grouplink.ehelpdesk.web.controllers;

import net.grouplink.ehelpdesk.common.utils.MailConfig;
import net.grouplink.ehelpdesk.common.utils.ldap.LdapConstants;
import net.grouplink.ehelpdesk.domain.Attachment;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.CustomField;
import net.grouplink.ehelpdesk.domain.CustomFieldValues;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.LdapField;
import net.grouplink.ehelpdesk.domain.LdapUser;
import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.domain.Status;
import net.grouplink.ehelpdesk.domain.SurveyData;
import net.grouplink.ehelpdesk.domain.SurveyQuestion;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketPriority;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRole;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.domain.ZenInformation;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.ZenAsset;
import net.grouplink.ehelpdesk.service.AssetTrackerZen10Service;
import net.grouplink.ehelpdesk.service.CategoryOptionService;
import net.grouplink.ehelpdesk.service.CategoryService;
import net.grouplink.ehelpdesk.service.CustomFieldsService;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LdapFieldService;
import net.grouplink.ehelpdesk.service.LdapService;
import net.grouplink.ehelpdesk.service.LocationService;
import net.grouplink.ehelpdesk.service.MailService;
import net.grouplink.ehelpdesk.service.StatusService;
import net.grouplink.ehelpdesk.service.SurveyDataService;
import net.grouplink.ehelpdesk.service.AssignmentService;
import net.grouplink.ehelpdesk.service.TicketPriorityService;
import net.grouplink.ehelpdesk.service.TicketService;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import net.grouplink.ehelpdesk.service.asset.AssetService;
import net.grouplink.ehelpdesk.service.asset.ZenAssetService;
import net.grouplink.ehelpdesk.web.propertyeditors.AssetEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.CategoryEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.CategoryOptionEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.GroupEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.LocationEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.StatusEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.TicketPriorityEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.UserEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.UserRoleGroupEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.ZenAssetEditor;
import net.grouplink.ehelpdesk.web.surveys.SurveyItem;
import net.grouplink.ehelpdesk.web.utils.ApplicationConstants;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.text.DateFormat;
import java.util.*;

/**
 * author: zpearce
 * Date: 12/27/11
 * Time: 9:40 AM
 */
@Controller
@RequestMapping("/tickets")
public class TicketController extends ApplicationObjectSupport {
    @Autowired protected TicketService ticketService;
    @Autowired protected TicketPriorityService ticketPriorityService;
    @Autowired protected StatusService statusService;
    @Autowired protected GroupService groupService;
    @Autowired protected CategoryService categoryService;
    @Autowired protected CategoryOptionService categoryOptionService;
    @Autowired protected MailService mailService;
    @Autowired protected UserService userService;
    @Autowired protected LocationService locationService;
    @Autowired protected TicketPriorityService priorityService;
    @Autowired protected SurveyDataService surveyDataService;
    @Autowired protected UserRoleGroupService userRoleGroupService;
    @Autowired protected LdapService ldapService;
    @Autowired protected AssetService assetService;
    @Autowired protected ZenAssetService zenAssetService;
    @Autowired protected AssetTrackerZen10Service assetTrackerZen10Service;
    @Autowired protected LdapFieldService ldapFieldService;
    @Autowired protected CustomFieldsService customFieldsService;
    @Autowired protected PermissionService permissionService;
    @Autowired protected AssignmentService assignmentService;
    @Autowired protected Validator validator;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);

        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));

        // have to use UserRoleGroupEditor instead of UserRoleGroupFormatter because of the way the fake urg id's are
        // returned from the assignments store, and because the spring formatter/parsers aren't allowed to return null
        binder.registerCustomEditor(UserRoleGroup.class, new UserRoleGroupEditor(userRoleGroupService));
    }

    protected Ticket createNewTicket(User user, User contact, Group group, Integer parentId) {
        Ticket ticket = new Ticket();

        if (parentId != null) {
            Ticket parent = ticketService.getTicketById(parentId);
            ticket.setParent(parent);
            ticket.setLocation(parent.getLocation());
            if (group == null) {
                ticket.setGroup(parent.getGroup());
                ticket.setCategory(parent.getCategory());
                ticket.setCategoryOption(parent.getCategoryOption());
                ticket.setAssignedTo(parent.getAssignedTo());
            } else {
                ticket.setGroup(group);
            }

            if (contact == null) {
                ticket.setContact(parent.getContact());
            } else {
                ticket.setContact(contact);
            }
        } else {
            ticket.setContact(contact == null ? user : contact);
            ticket.setLocation(contact==null ? user.getLocation() : contact.getLocation());
            ticket.setGroup(group);
        }

        ticket.setSubmittedBy(user);
        ticket.setPriority(ticketPriorityService.getInitialPriority());
        ticket.setStatus(statusService.getInitialStatus());

        // Set the default location based on the contact's location

        // Figure out those check check notify tech check techs
        // Get notification defaults from mailconfig
        MailConfig mc = mailService.getMailConfig();
        Boolean checkNotifyUser = mc.getNotifyUser();
        Boolean checkNotifyTech = mc.getNotifyTechnician();

        // check user against ticket contact
        if (user.getId().equals(ticket.getContact().getId())) {
            checkNotifyUser = Boolean.FALSE;
        }

        // check user against ticket assignment
        UserRoleGroup urg = ticket.getAssignedTo();
        if (urg != null) {
            UserRole ur = urg.getUserRole();
            if (ur != null) {
                User atu = ur.getUser();
                if (atu != null) {
                    Integer atid = atu.getId();
                    if (user.getId().equals(atid)) {
                        checkNotifyTech = Boolean.FALSE;
                    }
                }
            }
        }

        ticket.setNotifyTech(checkNotifyTech);
        ticket.setNotifyUser(checkNotifyUser);

        return ticket;
    }

    private Ticket getZenWorkInfo(Ticket ticket, LdapUser ldapUser) {
        try {
            String zenDn = ldapUser.getAttributeMap().get("zenwmLoggedInWorkstation");
            logger.debug("ZEN7 - zenDn  = " + zenDn);
            logger.debug("ZEN7 - ldapUser.attributeMap");
            for (Object o : ldapUser.getAttributeMap().keySet()) {
                logger.debug(o + " == " + ldapUser.getAttributeMap().get(o));
            }

            if (StringUtils.isNotBlank(zenDn)) {

                String[] zenArray = zenDn.split(";");

                for (String aZenArray : zenArray) {

                    String dn = aZenArray.trim();

                    LdapUser zen = ldapService.getLdapUser(dn);
                    Map zenMap = zen.getAttributeMap();

                    ZenInformation newZen = new ZenInformation();
                    newZen.setDn(zen.getDn());
                    newZen.setWorkstation(MapUtils.getString(zenMap, LdapConstants.ZEN_WORKSTATION));
                    newZen.setLoggedInWorkstation(MapUtils.getString(zenMap, LdapConstants.ZEN_LOGGED_IN_WORKSTATION));
                    newZen.setIpAddress(MapUtils.getString(zenMap, LdapConstants.ZEN_IP_ADDRESS));
                    newZen.setSubnetAddress(MapUtils.getString(zenMap, LdapConstants.ZEN_SUBNET_ADDRESS));
                    newZen.setMacAddress(MapUtils.getString(zenMap, LdapConstants.ZEN_MAC_ADDRESS));
                    newZen.setDiskInfo(MapUtils.getString(zenMap, LdapConstants.ZEN_DISK_INFO));
                    newZen.setMemorySize(MapUtils.getString(zenMap, LdapConstants.ZEN_MEMORY_SIZE));
                    newZen.setProcessorType(MapUtils.getString(zenMap, LdapConstants.ZEN_PROCESSOR_TYPE));
                    newZen.setBiosType(MapUtils.getString(zenMap, LdapConstants.ZEN_BIOS_TYPE));
                    newZen.setVideoType(MapUtils.getString(zenMap, LdapConstants.ZEN_VIDEO_TYPE));
                    newZen.setNicType(MapUtils.getString(zenMap, LdapConstants.ZEN_NIC_TYPE));
                    newZen.setNcv(MapUtils.getString(zenMap, LdapConstants.ZEN_NCV));
                    newZen.setOsRevision(MapUtils.getString(zenMap, LdapConstants.ZEN_OS_REVISION));
                    newZen.setOsType(MapUtils.getString(zenMap, LdapConstants.ZEN_OS_TYPE));
                    newZen.setAssetTag(MapUtils.getString(zenMap, LdapConstants.ZEN_ASSET_TAG));
                    newZen.setSerialNumber(MapUtils.getString(zenMap, LdapConstants.ZEN_SERIAL_NUMBER));
                    newZen.setModelNumber(MapUtils.getString(zenMap, LdapConstants.ZEN_MODEL_NUMBER));
                    newZen.setComputerModel(MapUtils.getString(zenMap, LdapConstants.ZEN_COMPUTER_MODEL));
                    newZen.setComputerType(MapUtils.getString(zenMap, LdapConstants.ZEN_COMPUTER_TYPE));
                    newZen.setUserHistory(MapUtils.getString(zenMap, LdapConstants.ZEN_USER_HISTORY));
                    newZen.setLastUser(MapUtils.getString(zenMap, LdapConstants.ZEN_LAST_USER));
                    newZen.setOperator(MapUtils.getString(zenMap, LdapConstants.ZEN_OPERATOR));
                    newZen.setComputerName(MapUtils.getString(zenMap, LdapConstants.ZEN_COMPUTER_NAME));
                    logger.debug("ZEN7 - newZen.getDn()  = " + newZen.getDn());
                    if (StringUtils.isNotBlank(newZen.getDn())) {
                        newZen.setTicket(ticket);
                        ticket.getZenInformation().add(newZen);
                    }
                }
            }
            logger.debug("ZEN7 - ticket.getZenInformation().size()  = " + ticket.getZenInformation().size());
        } catch (NullPointerException e) {
            logger.error(e.getMessage(), e);
        }

        return ticket;
    }

    @ModelAttribute
    protected void referenceData(ModelMap refData, Ticket ticket, HttpServletRequest request) throws ServletRequestBindingException {
        refData.put("locations", locationService.getLocations());
        refData.put("priorities", priorityService.getPriorities());
        refData.put("ticketStatus", statusService.getStatus());

        List<SurveyItem> surveyItems = new ArrayList<SurveyItem>();
        Boolean surveyCompleted = Boolean.FALSE;
        List<SurveyData> surveyDataList = surveyDataService.getByTicketId(ticket.getId());
        if ((surveyDataList != null) && (surveyDataList.size() > 0)) {
            surveyCompleted = Boolean.TRUE;
            for (SurveyData surveyData : surveyDataList) {
                String value = surveyData.getValue();
                SurveyQuestion surveyQuestion = surveyData.getSurveyQuestion();
                String question = surveyQuestion.getSurveyQuestionText();
                SurveyItem surveyItem = new SurveyItem(question, value);
                surveyItems.add(surveyItem);
            }
        }

        refData.put("surveyCompleted", surveyCompleted);
        refData.put("surveyItems", surveyItems);

        String zen10BaseUrl = assetTrackerZen10Service.getZen10BaseUrl();
        if (StringUtils.isNotBlank(zen10BaseUrl)) {
            refData.put("zenBaseUrl", zen10BaseUrl);
        } else {
            refData.put("zenBaseUrl", "");
        }

        boolean zenEhdVncEnabled = assetTrackerZen10Service.isEhdVncEnabled();
        refData.put("zenEhdVncEnabled", zenEhdVncEnabled);

        //Searching for LDAP information
        ServletContext application = request.getSession().getServletContext();
        Boolean ldapIntegration = (Boolean) application.getAttribute(ApplicationConstants.LDAP_INTEGRATION);
        LdapUser ldapUser = new LdapUser();
        if (ldapIntegration != null &&
                ldapIntegration &&
                ticket.getContact() != null) {
            if (StringUtils.isNotBlank(ticket.getContact().getLdapDN())) {
                ldapUser = ldapService.getLdapUser(ticket.getContact().getLdapDN());
            }

            List<LdapField> ldapFieldList = ldapFieldService.getLdapFields();
            refData.put("ldapFieldList", ldapFieldList);
            refData.put("ldapUser", ldapUser);
        }

        if (StringUtils.isNotBlank(ServletRequestUtils.getStringParameter(request, "dt"))) {
            refData.put("workTimeInitialValue", ServletRequestUtils.getStringParameter(request, "dt"));
            refData.put("timerStatus", ServletRequestUtils.getStringParameter(request, "ts"));
        } else {
            refData.put("timerStatus", "OFF");
        }

        if (StringUtils.isNotBlank(ServletRequestUtils.getStringParameter(request, "smtl"))) {
            refData.put("smtl", ServletRequestUtils.getBooleanParameter(request, "smtl"));
        }
    }
    
    protected String getTextChanges(Ticket ticket) {
        if (ticket.getTicketTemplateStatus() == null || !ticket.getTicketTemplateStatus().equals(-1)) {
            return ticketService.createTextComments(ticket, ticket.getOldTicket());
        }
        return "";
    }
    
    private String getHtmlChanges(Ticket ticket) {
        return ticketService.createHTMLComments(ticket, ticket.getOldTicket());
    }

    protected boolean processTicket(Ticket ticket, HttpServletRequest request, BindingResult bindingResult) throws IOException, MessagingException {
        User user = userService.getLoggedInUser();
        validator.validate(ticket, bindingResult);
        processCustomFields(ticket, request, bindingResult);

        if(!bindingResult.hasErrors()) {
            // Convert multipart files to Attachment objects
            List<Object> attchmnts = ticket.getAttchmnt();
            for (Object o : attchmnts) {
                MultipartFile attchmnt = (MultipartFile) o;
                if (attchmnt == null) continue;

                if (attchmnt.getSize() != 0) {
                    Attachment attachment = new Attachment();
                    attachment.setContentType(attchmnt.getContentType());
                    attachment.setFileData(attchmnt.getBytes());
                    attachment.setFileName(attchmnt.getOriginalFilename());
                    ticket.getAttachments().add(attachment);
                }
            }

            boolean newTicket = ticket.getId() == null;

            if (userService.isEndUser(user) && newTicket) {
                if (ticket.getAssignedTo() != null && ticket.getAssignedTo().getUserRole() == null) {
                    ticket.setStatus(statusService.getTicketPoolStatus());
                }
            }

            if (!newTicket) {
                if (ticket.getStatus() == null || ticket.getPriority() == null) {
                    // get the status and priority from the deeb
                    ticketService.evict(ticket);
                    Ticket deebTicket = ticketService.getTicketById(ticket.getId());
                    ticketService.evict(deebTicket);
                    if(ticket.getStatus() == null){
                        ticket.setStatus(deebTicket.getStatus());
                    }
                    if(ticket.getPriority() == null){
                        ticket.setPriority(deebTicket.getPriority());
                    }
                }
            }

            //Reassigning ticket if necessary
            UserRoleGroup tech = ticket.getAssignedTo();
            if (tech.getBackUpTech() != null && ticket.getId() == null) {
                UserRoleGroup backUp = tech.getBackUpTech();
                List urgList = userRoleGroupService.getUserRoleGroupByGroupId(tech.getGroup().getId());
                int count = 2;
                while (backUp.getBackUpTech() != null && count < urgList.size()) {
                    backUp = backUp.getBackUpTech();
                    count++;
                }
                if (count >= urgList.size()) {
                    backUp = userRoleGroupService.getTicketPoolByGroupId(tech.getGroup().getId());
                }
                ticket.setAssignedTo(backUp);
            }

            // Getting Zen information
            LdapUser ldapUser;
            HttpSession session = request.getSession();
            Boolean ldapIntergration = (Boolean) session.getServletContext().getAttribute(ApplicationConstants.LDAP_INTEGRATION);
            logger.debug("ZEN7 - ldapIntegration  = " + ldapIntergration);
            logger.debug("ZEN7 - ticket.getContact().getLdapDN()  = " + ticket.getContact().getLdapDN());
            if (ldapIntergration && StringUtils.isNotBlank(ticket.getContact().getLdapDN())) {
                logger.debug("ZEN7 - looking up ldapUser");
                ldapUser = ldapService.getLdapUser(ticket.getContact().getLdapDN());
                logger.debug("ZEN7 - ldapUser != null  = " + (ldapUser != null));
                logger.debug("ZEN7 - ticket.getId()  = " + ticket.getId());
                if (ldapUser != null && ticket.getId() == null) {
                    ticket = getZenWorkInfo(ticket, ldapUser);
                }
            }

            ticketService.saveTicket(ticket, user);
            if (ticket.getTicketTemplateStatus() == null || !ticket.getTicketTemplateStatus().equals(-1) && newTicket) {
                ticketService.sendEmailNotification(ticket, user, getHtmlChanges(ticket), getTextChanges(ticket));
            }
            return true;
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    private void processCustomFields(Ticket ticket, HttpServletRequest request, BindingResult bindingResult) {
        //process custom fields
        Map<String, String[]> reqParams = request.getParameterMap();
        for (Map.Entry<String, String[]> entry : reqParams.entrySet()) {
            String paramName = entry.getKey();
            String[] paramVals = entry.getValue();

            if (paramName.startsWith("cf_") || paramName.startsWith("_cf_")) {

                String cfValue;

                // Get the custom field id from the paramName
                Integer cfId;

                if (paramName.startsWith("_cf_")) {
                    cfId  = Integer.parseInt(paramName.split("_")[2]);
                    if (reqParams.containsKey(paramName.substring(1))) {
                        continue;
                    }
                } else {
                    cfId  = Integer.parseInt(paramName.split("_")[1]);
                }

                CustomField cf = customFieldsService.getCustomFieldById(cfId);

                CustomFieldValues cfv = null;
                for (CustomFieldValues cfvFound : ticket.getCustomeFieldValues()) {
                    if (cfvFound.getCustomField().getId().equals(cf.getId())) {
                        cfv = cfvFound;
                        break;
                    }
                }

                if (cfv == null) {
                    cfv = new CustomFieldValues();
                }

                if (cf.getFieldType().equals(CustomField.VALUETYPE_CHECKBOX)) {
                    cfValue = String.valueOf(ServletRequestUtils.getBooleanParameter(request, paramName, false));
                } else {
                    cfValue = paramVals[0];
                }

                cfv.setCustomField(cf);
                cfv.setTicket(ticket);
                cfv.setFieldValue(cfValue);
                ticket.getCustomeFieldValues().add(cfv);

                // Check required fields
                if (cf.isRequired() && StringUtils.isBlank(paramVals[0])) {
                    bindingResult.reject("customField.validate.requiredField", new String[]{cf.getName()}, "{0} is required");
                }

                if (cf.getFieldType().equals("datetime") && ((StringUtils.isNotBlank(paramVals[0]) && !cf.isRequired()) || cf.isRequired())) {
                    Locale locale = LocaleContextHolder.getLocale();
                    DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, locale);
                    formatter.setLenient(false);
                    try {
                        formatter.parse(cfValue);
                    } catch(Exception e) {
                        bindingResult.reject("customField.validate.invalidDateTime", new String[]{cf.getName(), ((SimpleDateFormat)formatter).toLocalizedPattern()}, "{0} is invalid. Format should be {1}");
                    }
                }
            }
        }
    }
}
