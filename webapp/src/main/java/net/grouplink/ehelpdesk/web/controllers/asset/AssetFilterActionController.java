package net.grouplink.ehelpdesk.web.controllers.asset;

import net.grouplink.ehelpdesk.common.beans.support.GLRefreshablePagedListHolder;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.AssetFilter;
import net.grouplink.ehelpdesk.service.CategoryOptionService;
import net.grouplink.ehelpdesk.service.CategoryService;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LocationService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.LicenseService;
import net.grouplink.ehelpdesk.service.asset.AssetFilterService;
import net.grouplink.ehelpdesk.service.asset.AssetService;
import net.grouplink.ehelpdesk.service.asset.AssetStatusService;
import net.grouplink.ehelpdesk.service.asset.AssetTypeService;
import net.grouplink.ehelpdesk.service.asset.VendorService;
import net.grouplink.ehelpdesk.web.pagedlistproviders.AssetFilterProvider;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: Sep 3, 2008
 * Time: 5:51:57 PM
 */
public class AssetFilterActionController extends MultiActionController {
    protected final Log logger = LogFactory.getLog(getClass());

    private AssetFilterService assetFilterService;
    private LocationService locationService;
    private GroupService groupService;
    private CategoryService categoryService;
    private CategoryOptionService categoryOptionService;
    private AssetTypeService assetTypeService;
    private UserService userService;
    private VendorService vendorService;
    private AssetStatusService assetStatusService;
    private AssetService assetService;
    private LicenseService licenseService;


    /**
     * Displays the main asset filter/search page loaded with the session user's filters
     * and any public filters that my have been defined.
     *
     * @param request  http request
     * @param response http response
     * @return assetFilter view
     * @throws Exception when one occurs
     */
    public ModelAndView displayFilterPage(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;

        // Shortcircuit to the ad page if license doesn't allow the Asset Tracker feature
        if (!licenseService.isAssetTracker()) {
            return new ModelAndView("assetTrackerAd");
        }

        User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("myFilters", assetFilterService.getByUser(user));
        model.put("pubFilters", assetFilterService.getPublicMinusUser(user));

        return new ModelAndView("assetFilter", model);
    }

    /**
     * Adds an item to the <code>AssetFilter</code> object on the http session according to
     * expected "type" and "value" parameters on the request then returns the appropriate html.
     *
     * @param request  the http request
     * @param response the http response
     * @return null. this method intended to be called by ajax. html is written on the response.
     * @throws Exception when one occurs.
     */
    public ModelAndView addItem(HttpServletRequest request, HttpServletResponse response) throws Exception {

        // Get the command object from the session
        AssetFilter assetFilter =
                (AssetFilter) request.getSession().getAttribute(AssetFilterFormController.AFC_FORM_ASSETFILTER);

        // get the type and value to be added.
        String type = ServletRequestUtils.getStringParameter(request, "type");
        String value = ServletRequestUtils.getStringParameter(request, "value");

        String htmlTable = "";
        String ctxtPath = request.getContextPath();

        if (type.equals("assetNumbers")) {
            String[] items = assetFilter.getAssetNumbers();
            if (!ArrayUtils.contains(assetFilter.getAssetNumbers(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getAssetNumbers(), value);
                assetFilter.setAssetNumbers(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("names")) {
            String[] items = assetFilter.getNames();
            if (!ArrayUtils.contains(assetFilter.getNames(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getNames(), value);
                assetFilter.setNames(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("typeIds")) {
            Integer[] items = assetFilter.getTypeIds();
            if (!ArrayUtils.contains(assetFilter.getTypeIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(assetFilter.getTypeIds(), new Integer(value));
                assetFilter.setTypeIds(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("ownerIds")) {
            Integer[] items = assetFilter.getOwnerIds();
            if (!ArrayUtils.contains(assetFilter.getOwnerIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(assetFilter.getOwnerIds(), new Integer(value));
                assetFilter.setOwnerIds(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("vendorIds")) {
            Integer[] items = assetFilter.getVendorIds();
            if (!ArrayUtils.contains(assetFilter.getVendorIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(assetFilter.getVendorIds(), new Integer(value));
                assetFilter.setVendorIds(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("notes")) {
            String[] items = assetFilter.getNotes();
            if (!ArrayUtils.contains(assetFilter.getNotes(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getNotes(), value);
                assetFilter.setNotes(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("assetLocations")) {
            String[] items = assetFilter.getAssetLocations();
            if (!ArrayUtils.contains(assetFilter.getAssetLocations(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getAssetLocations(), value);
                assetFilter.setAssetLocations(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("locationIds")) {
            Integer[] items = assetFilter.getLocationIds();
            if (!ArrayUtils.contains(assetFilter.getLocationIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(assetFilter.getLocationIds(), new Integer(value));
                assetFilter.setLocationIds(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("groupIds")) {
            Integer[] items = assetFilter.getGroupIds();
            if (!ArrayUtils.contains(assetFilter.getGroupIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(assetFilter.getGroupIds(), new Integer(value));
                assetFilter.setGroupIds(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("categoryIds")) {
            Integer[] items = assetFilter.getCategoryIds();
            if (!ArrayUtils.contains(assetFilter.getCategoryIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(assetFilter.getCategoryIds(), new Integer(value));
                assetFilter.setCategoryIds(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("categoryOptionIds")) {
            Integer[] items = assetFilter.getCategoryOptionIds();
            if (!ArrayUtils.contains(assetFilter.getCategoryOptionIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(assetFilter.getCategoryOptionIds(), new Integer(value));
                assetFilter.setCategoryOptionIds(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("manufacturers")) {
            String[] items = assetFilter.getManufacturers();
            if (!ArrayUtils.contains(assetFilter.getManufacturers(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getManufacturers(), value);
                assetFilter.setManufacturers(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("modelNumbers")) {
            String[] items = assetFilter.getModelNumbers();
            if (!ArrayUtils.contains(assetFilter.getModelNumbers(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getModelNumbers(), value);
                assetFilter.setModelNumbers(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("serialNumbers")) {
            String[] items = assetFilter.getSerialNumbers();
            if (!ArrayUtils.contains(assetFilter.getSerialNumbers(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getSerialNumbers(), value);
                assetFilter.setSerialNumbers(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("descriptions")) {
            String[] items = assetFilter.getDescriptions();
            if (!ArrayUtils.contains(assetFilter.getDescriptions(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getDescriptions(), value);
                assetFilter.setDescriptions(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("statusIds")) {
            Integer[] items = assetFilter.getStatusIds();
            if (!ArrayUtils.contains(assetFilter.getStatusIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(assetFilter.getStatusIds(), new Integer(value));
                assetFilter.setStatusIds(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        }
        // Accounting info fields
        else if ("poNumbers".equals(type)) {
            String[] items = assetFilter.getPoNumbers();
            if (!ArrayUtils.contains(assetFilter.getPoNumbers(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getPoNumbers(), value);
                assetFilter.setPoNumbers(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("accountNumbers".equals(type)) {
            String[] items = assetFilter.getAccountNumbers();
            if (!ArrayUtils.contains(assetFilter.getAccountNumbers(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getAccountNumbers(), value);
                assetFilter.setAccountNumbers(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("accumulatedNumbers".equals(type)) {
            String[] items = assetFilter.getAccumulatedNumbers();
            if (!ArrayUtils.contains(assetFilter.getAccumulatedNumbers(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getAccumulatedNumbers(), value);
                assetFilter.setAccumulatedNumbers(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("expenseNumbers".equals(type)) {
            String[] items = assetFilter.getExpenseNumbers();
            if (!ArrayUtils.contains(assetFilter.getExpenseNumbers(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getExpenseNumbers(), value);
                assetFilter.setExpenseNumbers(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("acquisitionValues".equals(type)) {
            String[] items = assetFilter.getAcquisitionValues();
            if (!ArrayUtils.contains(assetFilter.getAcquisitionValues(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getAcquisitionValues(), value);
                assetFilter.setAcquisitionValues(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("leaseNumbers".equals(type)) {
            String[] items = assetFilter.getLeaseNumbers();
            if (!ArrayUtils.contains(assetFilter.getLeaseNumbers(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getLeaseNumbers(), value);
                assetFilter.setLeaseNumbers(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("leaseDurations".equals(type)) {
            String[] items = assetFilter.getLeaseDurations();
            if (!ArrayUtils.contains(assetFilter.getLeaseDurations(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getLeaseDurations(), value);
                assetFilter.setLeaseDurations(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("leaseFrequencys".equals(type)) {
            String[] items = assetFilter.getLeaseFrequencys();
            if (!ArrayUtils.contains(assetFilter.getLeaseFrequencys(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getLeaseFrequencys(), value);
                assetFilter.setLeaseFrequencys(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("leaseFrequencyPrices".equals(type)) {
            String[] items = assetFilter.getLeaseFrequencyPrices();
            if (!ArrayUtils.contains(assetFilter.getLeaseFrequencyPrices(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getLeaseFrequencyPrices(), value);
                assetFilter.setLeaseFrequencyPrices(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("disposalMethods".equals(type)) {
            String[] items = assetFilter.getDisposalMethods();
            if (!ArrayUtils.contains(assetFilter.getDisposalMethods(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getDisposalMethods(), value);
                assetFilter.setDisposalMethods(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("insuranceCategories".equals(type)) {
            String[] items = assetFilter.getInsuranceCategories();
            if (!ArrayUtils.contains(assetFilter.getInsuranceCategories(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getInsuranceCategories(), value);
                assetFilter.setInsuranceCategories(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("replacementValuecategories".equals(type)) {
            String[] items = assetFilter.getReplacementValuecategories();
            if (!ArrayUtils.contains(assetFilter.getReplacementValuecategories(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getReplacementValuecategories(), value);
                assetFilter.setReplacementValuecategories(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("replacementValues".equals(type)) {
            String[] items = assetFilter.getReplacementValues();
            if (!ArrayUtils.contains(assetFilter.getReplacementValues(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getReplacementValues(), value);
                assetFilter.setReplacementValues(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("maintenanceCosts".equals(type)) {
            String[] items = assetFilter.getMaintenanceCosts();
            if (!ArrayUtils.contains(assetFilter.getMaintenanceCosts(), value)) {
                items = (String[]) ArrayUtils.add(assetFilter.getMaintenanceCosts(), value);
                assetFilter.setMaintenanceCosts(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if ("depreciationMethods".equals(type)) {
            String[] items = assetFilter.getDepreciationMethods();
            if (!ArrayUtils.contains(items, value)) {
                items = (String[]) ArrayUtils.add(items, value);
                assetFilter.setDepreciationMethods(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        }
        // Custom Field
        else if (type.startsWith("customFieldsDiv")) {
            String cfId = type.split("_")[1];

            Map<String, String[]> custFldMap = assetFilter.getCustomFields();
            if (custFldMap == null) custFldMap = new HashMap<String, String[]>();

            String[] items = custFldMap.get(cfId);
            if (!ArrayUtils.contains(items, value)) {
                items = (String[]) ArrayUtils.add(items, value);
                custFldMap.put(cfId, items);
                assetFilter.setCustomFields(custFldMap);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        }

        response.getOutputStream().print(htmlTable);
        return null;
    }

    /**
     * Deletes an item from the <code>AssetFilter</code> object on the http session according to
     * expected "type" and "row" parameters on the request then returns the appropriate html.
     *
     * @param request  the http request
     * @param response the http response
     * @return null. HTML is written to the response. used from an ajax call.
     * @throws Exception every time one occurs, maybe
     */
    public ModelAndView deleteItem(HttpServletRequest request, HttpServletResponse response) throws Exception {
        // Get the command object from the session
        AssetFilter assetFilter =
                (AssetFilter) request.getSession().getAttribute(AssetFilterFormController.AFC_FORM_ASSETFILTER);

        // get the type and value to be added.
        String type = ServletRequestUtils.getStringParameter(request, "type");
        int row = ServletRequestUtils.getIntParameter(request, "row", -1);

        String htmlTable = "";
        String ctxtPath = request.getContextPath();

        // remove the item from the right array
        if (type.equals("assetNumbers")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getAssetNumbers(), row);
            assetFilter.setAssetNumbers(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("names")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getNames(), row);
            assetFilter.setNames(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("typeIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(assetFilter.getTypeIds(), row);
            assetFilter.setTypeIds(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("ownerIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(assetFilter.getOwnerIds(), row);
            assetFilter.setOwnerIds(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("vendorIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(assetFilter.getVendorIds(), row);
            assetFilter.setVendorIds(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("notes")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getNotes(), row);
            assetFilter.setNotes(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("assetLocations")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getAssetLocations(), row);
            assetFilter.setAssetLocations(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("locationIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(assetFilter.getLocationIds(), row);
            assetFilter.setLocationIds(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("groupIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(assetFilter.getGroupIds(), row);
            assetFilter.setGroupIds(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("categoryIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(assetFilter.getCategoryIds(), row);
            assetFilter.setCategoryIds(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("categoryOptionIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(assetFilter.getCategoryOptionIds(), row);
            assetFilter.setCategoryOptionIds(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("manufacturers")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getManufacturers(), row);
            assetFilter.setManufacturers(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("modelNumbers")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getModelNumbers(), row);
            assetFilter.setModelNumbers(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("serialNumbers")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getSerialNumbers(), row);
            assetFilter.setSerialNumbers(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("descriptions")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getDescriptions(), row);
            assetFilter.setDescriptions(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("statusIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(assetFilter.getStatusIds(), row);
            assetFilter.setStatusIds(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        }
        // Accounting info fields
        else if (type.equals("poNumbers")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getPoNumbers(), row);
            assetFilter.setPoNumbers(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("accountNumbers")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getAccountNumbers(), row);
            assetFilter.setAccountNumbers(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("accumulatedNumbers")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getAccumulatedNumbers(), row);
            assetFilter.setAccumulatedNumbers(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("expenseNumbers")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getExpenseNumbers(), row);
            assetFilter.setExpenseNumbers(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("acquisitionValues")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getAcquisitionValues(), row);
            assetFilter.setAcquisitionValues(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("leaseNumbers")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getLeaseNumbers(), row);
            assetFilter.setLeaseNumbers(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("leaseDurations")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getLeaseDurations(), row);
            assetFilter.setLeaseDurations(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("leaseFrequencys")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getLeaseFrequencys(), row);
            assetFilter.setLeaseFrequencys(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("leaseFrequencyPrices")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getLeaseFrequencyPrices(), row);
            assetFilter.setLeaseFrequencyPrices(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("disposalMethods")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getDisposalMethods(), row);
            assetFilter.setDisposalMethods(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("insuranceCategories")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getInsuranceCategories(), row);
            assetFilter.setInsuranceCategories(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("replacementValuecategories")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getReplacementValuecategories(), row);
            assetFilter.setReplacementValuecategories(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("replacementValues")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getReplacementValues(), row);
            assetFilter.setReplacementValues(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("maintenanceCosts")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getMaintenanceCosts(), row);
            assetFilter.setMaintenanceCosts(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        } else if (type.equals("depreciationMethods")) {
            String[] items = (String[]) ArrayUtils.remove(assetFilter.getDepreciationMethods(), row);
            assetFilter.setDepreciationMethods(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        }
        // Custom Fields
        else if (type.startsWith("customFieldsDiv")) {
            String cfId = type.split("_")[1];

            Map<String, String[]> custFldMap = assetFilter.getCustomFields();
            if (custFldMap == null) custFldMap = new HashMap<String, String[]>();

            String[] items = custFldMap.get(cfId);
            items = (String[]) ArrayUtils.remove(items, row);
            custFldMap.put(cfId, items);
            assetFilter.setCustomFields(custFldMap);
            htmlTable = createHtmlTable(type, Arrays.asList(items), ctxtPath);
        }

        response.getOutputStream().print(htmlTable);
        return null;
    }

    /**
     * Return the categoryFetcher view; a dialog for selecting categories
     *
     * @param request  the http request
     * @param response the http response
     * @return the categoryFetcher view with needed data
     * @throws Exception when one occurs
     */
    public ModelAndView fetchCategories(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert request != null;
        assert response != null;

        // Shortcircuit to the ad page if license doesn't allow the Asset Tracker feature
        if (!licenseService.isAssetTracker()) {
            return new ModelAndView("assetTrackerAd");
        }

        List<Category> categories = categoryService.getCategories();
        Map<String, List<Category>> model = new HashMap<String, List<Category>>();
        for (Object category1 : categories) {
            Category category = (Category) category1;
            List<Category> cats = model.get(category.getGroup().getName());
            if (cats == null) {
                cats = new ArrayList<Category>();
            }
            cats.add(category);
            model.put(category.getGroup().getName(), cats);
        }
        return new ModelAndView("categoryFetcher", "model", model);
    }

    /**
     * Return the categoryOptionFetcher view; a dialog for selecting category options
     *
     * @param request  the http request
     * @param response the http response
     * @return the categoryOptionFetcher view with needed data
     * @throws Exception when one occurs
     */
    public ModelAndView fetchCategoryOptions(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert request != null;
        assert response != null;

        // Shortcircuit to the ad page if license doesn't allow the Asset Tracker feature
        if (!licenseService.isAssetTracker()) {
            return new ModelAndView("assetTrackerAd");
        }

        List<CategoryOption> catOpts = categoryOptionService.getCategoryOptions();
        Map<String, List<CategoryOption>> model = new HashMap<String, List<CategoryOption>>();
        for (CategoryOption catOpt : catOpts) {
            String key = catOpt.getCategory().getGroup().getName() + " - " +
                    catOpt.getCategory().getName();
            List<CategoryOption> catOps = model.get(key);
            if (catOps == null) {
                catOps = new ArrayList<CategoryOption>();
            }
            catOps.add(catOpt);
            model.put(key, catOps);
        }
        return new ModelAndView("catOptFetcher", "model", model);
    }

    /**
     * Displays the list of Assets as determined by the results of a search based on the
     * sessioned <code>AssetFilter</code>
     *
     * @param request  the http request
     * @param response the http response
     * @return the assetFilterList view
     * @throws Exception when one occurs
     */
    public ModelAndView showList(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;

        // Shortcircuit to the ad page if license doesn't allow the Asset Tracker feature
        if (!licenseService.isAssetTracker()) {
            return new ModelAndView("assetTrackerAd");
        }

        HttpSession session = request.getSession();

        User user = (User) session.getAttribute(SessionConstants.ATTR_USER);
        user = userService.getUserById(user.getId());
        GLRefreshablePagedListHolder listHolder = (GLRefreshablePagedListHolder) session.getAttribute("ASSET_FILTER_LIST");
        Locale locale = RequestContextUtils.getLocale(request);

        AssetFilter assetFilter = (AssetFilter) session.getAttribute(AssetFilterFormController.AFC_FORM_ASSETFILTER);
        if (listHolder == null) {
            if (assetFilter == null) {
                assetFilter = new AssetFilter();
                session.setAttribute(AssetFilterFormController.AFC_FORM_ASSETFILTER, assetFilter);
            }

            listHolder = new GLRefreshablePagedListHolder();
            listHolder.setMaxLinkedPages(20);
            listHolder.setSourceProvider(new AssetFilterProvider(user, assetFilterService));
            listHolder.setFilter(assetFilter);
            session.setAttribute("ASSET_FILTER_LIST", listHolder);

        } else {
            listHolder.setFilter(assetFilter);
        }

        ServletRequestDataBinder bdr = new ServletRequestDataBinder(listHolder, "assets");
        bdr.bind(request);

        listHolder.setLocale(locale);
        listHolder.setSource(listHolder.getSourceProvider().loadList(locale, listHolder.getFilter()));

        int page = listHolder.getPage();
        listHolder.resort();
        listHolder.setPage(page);


        Map<String, Object> model = new HashMap<String, Object>();
        model.put("listHolder", bdr.getBindingResult().getModel());
        model.put("refreshUrl", "/asset/filter/results.glml");
        model.put("colOrder", assetFilter.getColumnOrder());

        return new ModelAndView("assetFilterList", model);
    }

    /**
     * Reloads the filter dropdown list on the asset filter page
     *
     * @param request  the http request
     * @param response the http response
     * @return null. outputs html to build a dropdown of the AssetFilters in the system
     * @throws Exception when one occurs
     */
    public ModelAndView reloadFilterList(HttpServletRequest request, HttpServletResponse response) throws Exception {
        User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);
        List myFilters = assetFilterService.getByUser(user);
        List pubFilters = assetFilterService.getPublicMinusUser(user);

        // get the id of the newly saved assetFilter to show as selected
        Integer tfid = ServletRequestUtils.getIntParameter(request, "id");

        MessageSourceAccessor msg = getMessageSourceAccessor();
        StringBuilder sb = new StringBuilder("<select name=\"namedFilters\" onchange=\"loadFilter(this.value);\">");
        sb.append("<option value=\"-1\">").append(msg.getMessage("ticketFilter.createNew")).append("</option>");
        if (myFilters.size() > 0) {
            sb.append("<optgroup label=\"").append(msg.getMessage("ticketFilter.myFilters")).append("\">");
            for (Object myFilter : myFilters) {
                AssetFilter tf = (AssetFilter) myFilter;
                sb.append("<option value=\"").append(tf.getId()).append("\"");
                if (tfid != null && tfid.equals(tf.getId())) sb.append(" selected");
                sb.append(">");
                sb.append(tf.getName());
                sb.append("</option>");
            }
            sb.append("</optgroup>");
        }
        if (pubFilters.size() > 0) {
            sb.append("<optgroup label=\"").append(msg.getMessage("ticketFilter.publicFilters")).append("\">");
            for (Object pubFilter : pubFilters) {
                AssetFilter tf = (AssetFilter) pubFilter;
                sb.append("<option value=\"").append(tf.getId()).append("\">").append(tf.getName()).append("</option>");
            }
            sb.append("</optgroup>");
        }
        sb.append("</select>");


        response.getOutputStream().print(sb.toString());
        return null;
    }

    /**
     * Takes a list of asset ids on the request and calls the service deleted method on them.
     *
     * @param request  the http request
     * @param response the http response
     * @return showList
     * @throws Exception whenever one occurs
     */
    public ModelAndView massDeleteAssets(HttpServletRequest request, HttpServletResponse response) throws Exception {

        int[] assetIds = ServletRequestUtils.getIntParameters(request, "assetCbx");

        // "delete" the asset
        for (int assetId : assetIds) {
            Asset asset = assetService.getAssetById(assetId);
            assetService.deleteAsset(asset);
        }
        assetService.flush();
        return showList(request, response);
    }

    /* Private Utility methods */

    // For the asset search page. This method creates an html table that is displayed
    // showing any entered search criteria.

    private String createHtmlTable(String propertyName, List items, String ctxtPath) {
        String deleteMsg = getMessageSourceAccessor().getMessage("global.delete", "Delete");

        String displayName;
        StringBuilder sb = new StringBuilder("<table width=\"100%\">");
        for (int i = 0; i < items.size(); i++) {
            displayName = resolveDisplayName(propertyName, items.get(i));
            sb.append("<tr>")
                    .append("<td>").append(displayName).append("</td>")
                    .append("<td align='right'>")
                    .append("  <input type='hidden' name='").append(propertyName).append("' value='").append(items.get(i)).append("'/>")
                    .append("  <a href='javascript://' onclick=\"delItem('").append(propertyName).append("', '")
                    .append(i).append("')\" title='").append(deleteMsg).append("'>")
                    .append("    <img src='").append(ctxtPath).append("/images/delete.gif' alt=''/>")
                    .append("  </a>")
                    .append("</td>")
                    .append("</tr>");
        }
        sb.append("</table>");
        return sb.toString();
    }

    // 'type' determines whether to do something special to get the display value
    // if nothing special is needed then just returns 'nameOrId' as a string
    private String resolveDisplayName(String type, Object nameOrId) {
        if (type.equals("groupIds")) {
            return groupService.getGroupById((Integer) nameOrId).getName();
        } else if (type.equals("locationIds")) {
            return locationService.getLocationById((Integer) nameOrId).getName();
        } else if (type.equals("categoryIds")) {
            return categoryService.getCategoryById((Integer) nameOrId).getName();
        } else if (type.equals("categoryOptionIds")) {
            return categoryOptionService.getCategoryOptionById((Integer) nameOrId).getName();
        } else if (type.equals("typeIds")) {
            return assetTypeService.getById((Integer) nameOrId).getName();
        } else if (type.equals("ownerIds")) {
            User user = userService.getUserById((Integer) nameOrId);
            return user.getLastName() + ", " + user.getFirstName();
        } else if (type.equals("vendorIds")) {
            return vendorService.getById((Integer) nameOrId).getName();
        } else if (type.equals("statusIds")) {
            return assetStatusService.getById((Integer) nameOrId).getName();
        }
        return (String) nameOrId;
    }

    /* Setters */
    public void setAssetFilterService(AssetFilterService assetFilterService) {
        this.assetFilterService = assetFilterService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setCategoryOptionService(CategoryOptionService categoryOptionService) {
        this.categoryOptionService = categoryOptionService;
    }

    public void setAssetTypeService(AssetTypeService assetTypeService) {
        this.assetTypeService = assetTypeService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setVendorService(VendorService vendorService) {
        this.vendorService = vendorService;
    }

    public void setAssetStatusService(AssetStatusService assetStatusService) {
        this.assetStatusService = assetStatusService;
    }

    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }

    public void setLicenseService(LicenseService licenseService) {
        this.licenseService = licenseService;
    }
}
