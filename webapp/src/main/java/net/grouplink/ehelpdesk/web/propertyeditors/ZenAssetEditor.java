package net.grouplink.ehelpdesk.web.propertyeditors;

import net.grouplink.ehelpdesk.domain.asset.ZenAsset;
import net.grouplink.ehelpdesk.service.asset.ZenAssetService;
import org.apache.commons.lang.StringUtils;

import java.beans.PropertyEditorSupport;

public class ZenAssetEditor extends PropertyEditorSupport {

    private ZenAssetService zenAssetService;

    public ZenAssetEditor() {
    }

    public ZenAssetEditor(ZenAssetService zenAssetService) {
        this.zenAssetService = zenAssetService;
    }

    /**
     * Gets the property value as a string suitable for presentation
     * to a human to edit.
     *
     * @return The property value as a string suitable for presentation
     *         to a human to edit.
     *         <p>   Returns "null" is the value can't be expressed as a string.
     *         <p>   If a non-null value is returned, then the PropertyEditor should
     *         be prepared to parse that string back in setAsText().
     */
    public String getAsText() {
        ZenAsset asset = (ZenAsset)getValue();
        return (asset == null) ? "" : asset.getAssetName();
    }

    /**
     * Sets the property value by parsing a given String.  May raise
     * java.lang.IllegalArgumentException if either the String is
     * badly formatted or if this kind of property can't be expressed
     * as text.
     *
     * @param text The string to be parsed.
     */
    public void setAsText(String text) throws IllegalArgumentException {
        if (StringUtils.isNotBlank(text)) {
            setValue(zenAssetService.getById(Integer.valueOf(text)));
        } else {
            setValue(null);
        }
    }

    public void setAssetService(ZenAssetService assetService) {
        this.zenAssetService = assetService;
    }

}