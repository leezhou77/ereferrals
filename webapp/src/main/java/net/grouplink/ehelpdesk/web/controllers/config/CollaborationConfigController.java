package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.common.collaboration.CollaborationConfig;
import net.grouplink.ehelpdesk.common.utils.PropertiesConstants;
import net.grouplink.ehelpdesk.service.PropertiesService;
import net.grouplink.ehelpdesk.service.collaboration.CollaborationService;
import net.grouplink.ehelpdesk.web.utils.ApplicationConstants;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

/**
 * author: zpearce
 * Date: 9/23/11
 * Time: 12:09 PM
 */
@Controller
@RequestMapping(value = "/collaboration")
public class CollaborationConfigController extends ApplicationObjectSupport {
    @Autowired private CollaborationService collaborationService;
    @Autowired private PropertiesService propertiesService;

    @RequestMapping(method = RequestMethod.GET, value = "/collaborationConfig.glml")
    public String getNewForm(Model model) {
        CollaborationConfig collaborationConfig = collaborationService.getCollaborationConfig();
        model.addAttribute(collaborationConfig);
        return "collaborationConfig";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/collaborationConfig.glml")
    public String updateConfig(@Valid CollaborationConfig collaborationConfig, BindingResult bindingResult, HttpServletRequest request) {
        if(bindingResult.hasErrors()) {
            return "collaborationConfig";
        }
        collaborationService.saveCollaborationConfig(collaborationConfig);

        ServletContext application = request.getSession().getServletContext();
        if (StringUtils.isNotBlank(collaborationConfig.getTrustedAppName())
        		&& StringUtils.isNotBlank(collaborationConfig.getTrustedAppKey())) {
        	application.setAttribute(ApplicationConstants.GROUP_WISE_TRUSTED_APP, Boolean.TRUE);
        }
        else {
        	application.setAttribute(ApplicationConstants.GROUP_WISE_TRUSTED_APP, Boolean.FALSE);
        }
        application.setAttribute(ApplicationConstants.OUTLOOK_INTEGRATION, propertiesService.getBoolValueByName(PropertiesConstants.OUTLOOK_INTEGRATION, false));
        application.setAttribute(ApplicationConstants.GROUP_WISE_INTEGRATION, propertiesService.getBoolValueByName(PropertiesConstants.GROUP_WISE_INTEGRATION, false));
        application.setAttribute(ApplicationConstants.GROUP_WISE_INTEGRATION_6X, propertiesService.getBoolValueByName(PropertiesConstants.GROUP_WISE_INTEGRATION_6X, false));

        String msg = getMessageSourceAccessor().getMessage("collaborationConfig.save.success");
        request.setAttribute("flash.success", msg);
        return "redirect:/config/collaboration/collaborationConfig.glml";
    }
}
