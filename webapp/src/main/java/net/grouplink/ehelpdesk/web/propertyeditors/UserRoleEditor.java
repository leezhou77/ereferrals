package net.grouplink.ehelpdesk.web.propertyeditors;

import net.grouplink.ehelpdesk.domain.UserRole;
import net.grouplink.ehelpdesk.service.UserRoleService;
import org.apache.commons.lang.StringUtils;

import java.beans.PropertyEditorSupport;
import java.util.NoSuchElementException;

public class UserRoleEditor extends PropertyEditorSupport {

    private UserRoleService userRoleService;

    public UserRoleEditor() {
    }
    
    public UserRoleEditor(UserRoleService userRoleService) {
    	setUserRoleService(userRoleService);
    }
    
    public String getAsText() {
        UserRole userRole = (UserRole)getValue();
        return (userRole !=  null)?userRole.getId().toString():"";
    }

    public void setAsText(String roleId) throws NoSuchElementException {
    	if (StringUtils.isNotBlank(roleId)) {
    		setValue(userRoleService.getUserRoleById(new Integer(roleId)));
    	} else {
            setValue(null);
        }
    }

	public void setUserRoleService(UserRoleService userRoleService) {
		this.userRoleService = userRoleService;
	}
}
