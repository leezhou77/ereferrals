package net.grouplink.ehelpdesk.web.formatters;

import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.service.TicketService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class TicketFormatter implements Formatter<Ticket> {
    @Autowired
    private TicketService ticketService;

    public Ticket parse(String text, Locale locale) throws ParseException {
        if (StringUtils.isNotBlank(text)) {
            return ticketService.getTicketById(Integer.valueOf(text));
        } else {
            return null;
        }
    }

    public String print(Ticket object, Locale locale) {
        return object == null ? "" : object.getId().toString();
    }
}
