package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.domain.TicketPriority;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.service.TicketPriorityService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import net.grouplink.ehelpdesk.web.forms.PriorityForm;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.AutoPopulatingList;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * author: zpearce
 * Date: 11/29/11
 * Time: 2:49 PM
 */
@Controller
@RequestMapping(value = "priorities")
public class TicketPriorityController extends ApplicationObjectSupport {
    @Autowired private TicketPriorityService ticketPriorityService;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        PriorityForm priorityForm = new PriorityForm();
        AutoPopulatingList<TicketPriority> priorities = new AutoPopulatingList<TicketPriority>(TicketPriority.class);
        priorities.addAll(ticketPriorityService.getPriorities());
        priorityForm.setPriorities(priorities);
        model.addAttribute("priorityForm", priorityForm);
        return "priorities/index";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String savePriorities(@Valid @ModelAttribute("priorityForm") PriorityForm priorityForm,
                                 BindingResult bindingResult,
                                 @RequestParam(value = "idsToDelete", required = false) Integer[] idsToDelete,
                                 HttpServletRequest request) {
        if(bindingResult.hasErrors()) {
            return "priorities/index";
        }
        for(int id : idsToDelete) {
            TicketPriority priority = priorityForm.getPriorityById(id);
            ticketPriorityService.deletePriority(priority);
            priorityForm.getPriorities().remove(priority);
        }
        for(TicketPriority priority : priorityForm.getPriorities()) {
            ticketPriorityService.savePriority(priority);
        }
        request.setAttribute("flash.success", getMessageSourceAccessor().getMessage("prioritySetup.saved"));
        return "redirect:/config/priorities";
    }
}
