package net.grouplink.ehelpdesk.web.controllers.asset;

import net.grouplink.ehelpdesk.common.utils.AssetSearch;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.AssetCustomField;
import net.grouplink.ehelpdesk.domain.asset.AssetFieldGroup;
import net.grouplink.ehelpdesk.domain.asset.AssetType;
import net.grouplink.ehelpdesk.domain.asset.SoftwareLicense;
import net.grouplink.ehelpdesk.domain.asset.Vendor;
import net.grouplink.ehelpdesk.service.CategoryOptionService;
import net.grouplink.ehelpdesk.service.CategoryService;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LicenseService;
import net.grouplink.ehelpdesk.service.LocationService;
import net.grouplink.ehelpdesk.service.asset.AssetCustomFieldService;
import net.grouplink.ehelpdesk.service.asset.AssetFieldGroupService;
import net.grouplink.ehelpdesk.service.asset.AssetService;
import net.grouplink.ehelpdesk.service.asset.AssetStatusService;
import net.grouplink.ehelpdesk.service.asset.AssetTypeService;
import net.grouplink.ehelpdesk.service.asset.SoftwareLicenseService;
import net.grouplink.ehelpdesk.service.asset.VendorService;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AssetTrackerController extends MultiActionController implements InitializingBean {

    private GroupService groupService;
    private LocationService locationService;
    private CategoryService categoryService;
    private CategoryOptionService categoryOptionService;
    private AssetService assetService;
    private AssetTypeService assetTypeService;
    private AssetStatusService assetStatusService;
    private AssetFieldGroupService assetFieldGroupService;
    private VendorService vendorService;
    private SoftwareLicenseService softwareLicenseService;
    private AssetCustomFieldService assetCustomFieldService;
    private LicenseService licenseService;

    public ModelAndView assetTracker(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert request != null && response != null;

        // Shortcircuit to the ad page if license doesn't allow the Asset Tracker feature
        if (!licenseService.isAssetTracker()) {
            return new ModelAndView("assetTrackerAd");
        }

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("contextHelpUrl", "/help/eHelpDesk.htm#Asset_Tracker.htm");
        return new ModelAndView("assetTracker", map);
    }

    public ModelAndView addAsset(HttpServletRequest request, HttpServletResponse response) {
        assert request != null && response != null;

        // Shortcircuit to the ad page if license doesn't allow the Asset Tracker feature
        if (!licenseService.isAssetTracker()) {
            return new ModelAndView("assetTrackerAd");
        }

        return new ModelAndView("addAsset");
    }

    public ModelAndView vendors(HttpServletRequest request, HttpServletResponse response) {
        assert request != null && response != null;

        // Shortcircuit to the ad page if license doesn't allow the Asset Tracker feature
        if (!licenseService.isAssetTracker()) {
            return new ModelAndView("assetTrackerAd");
        }

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("vendors", vendorService.getAllVendors());
        model.put("nonDelVendors", vendorService.getUndeletableVendors());
        return new ModelAndView("vendors", model);
    }

    public ModelAndView viewVendor(HttpServletRequest request, HttpServletResponse response) {
        assert request != null && response != null;

        // Shortcircuit to the ad page if license doesn't allow the Asset Tracker feature
        if (!licenseService.isAssetTracker()) {
            return new ModelAndView("assetTrackerAd");
        }

        return new ModelAndView("viewVendor");
    }

    public ModelAndView addVendor(HttpServletRequest request, HttpServletResponse response) {
        assert request != null && response != null;

        // Shortcircuit to the ad page if license doesn't allow the Asset Tracker feature
        if (!licenseService.isAssetTracker()) {
            return new ModelAndView("assetTrackerAd");
        }

        return new ModelAndView("addVendor");
    }

    public ModelAndView deleteVendor(HttpServletRequest request, HttpServletResponse response) throws ServletRequestBindingException {
        Integer vendorId = ServletRequestUtils.getIntParameter(request, "vendorId");
        Vendor vendor = vendorService.getById(vendorId);
        // assume exception means this vendor is in use and can't be deleted
        try {
            vendorService.deleteVendor(vendor);
            vendorService.flush();
        } catch (Exception e) {
            logger.error("error deleting vendor: " + e.getMessage(), e);
            return new ModelAndView("message", "message", "Unable to delete this vendor. Try deleting associations with it first");
        }

        return vendors(request, response);
    }

    public ModelAndView assetTypes(HttpServletRequest request, HttpServletResponse response) {
        assert request != null && response != null;

        // Shortcircuit to the ad page if license doesn't allow the Asset Tracker feature
        if (!licenseService.isAssetTracker()) {
            return new ModelAndView("assetTrackerAd");
        }

        @SuppressWarnings("unchecked")
        Map<String, Object> model = new ModelMap();
        model.put("assetTypes", assetTypeService.getAllAssetTypes());
        model.put("nonDelAssetTypes", assetTypeService.getUndeletableAssetTypes());
        return new ModelAndView("assetTypes", model);
    }

    public ModelAndView viewAssetType(HttpServletRequest request, HttpServletResponse response) {
        assert request != null && response != null;

        // Shortcircuit to the ad page if license doesn't allow the Asset Tracker feature
        if (!licenseService.isAssetTracker()) {
            return new ModelAndView("assetTrackerAd");
        }

        return new ModelAndView("viewAssetType");
    }

    public ModelAndView addAssetType(HttpServletRequest request, HttpServletResponse response) {
        assert request != null && response != null;

        // Shortcircuit to the ad page if license doesn't allow the Asset Tracker feature
        if (!licenseService.isAssetTracker()) {
            return new ModelAndView("assetTrackerAd");
        }

        return new ModelAndView("addAssetType");
    }

    public ModelAndView deleteAssetType(HttpServletRequest request, HttpServletResponse response) throws ServletRequestBindingException {
        Integer assetTypeId = ServletRequestUtils.getIntParameter(request, "assetTypeId");
        AssetType assetType = assetTypeService.getById(assetTypeId);
        // I put a client side check for being able to delete assettype but wrapping
        // this in try/catch with redirect and message just in case ;)
        try {
            assetTypeService.deleteAssetType(assetType);
            assetTypeService.flush();
        } catch (Exception e) {
            logger.error("error deleting assettype: " + e.getMessage(), e);
            return new ModelAndView("message", "message", "Unable to delete this asset type. Try deleting associations with it first");
        }

        return assetTypes(request, response);
    }

    public ModelAndView statuses(HttpServletRequest request, HttpServletResponse response) {
        assert request != null && response != null;

        // Shortcircuit to the ad page if license doesn't allow the Asset Tracker feature
        if (!licenseService.isAssetTracker()) {
            return new ModelAndView("assetTrackerAd");
        }

        @SuppressWarnings("unchecked")
        Map<String, Object> model = new ModelMap();
        model.put("statuses", assetStatusService.getAllAssetStatuses());
        model.put("nonDelStatus", assetStatusService.getUndeletableStatus());
        return new ModelAndView("statuses", model);
    }

    public ModelAndView viewStatus(HttpServletRequest request, HttpServletResponse response) {
        assert request != null && response != null;

        // Shortcircuit to the ad page if license doesn't allow the Asset Tracker feature
        if (!licenseService.isAssetTracker()) {
            return new ModelAndView("assetTrackerAd");
        }

        return new ModelAndView("viewStatus");
    }

    public ModelAndView deleteAssetStatus(HttpServletRequest request, HttpServletResponse response) throws ServletRequestBindingException {
        Integer assetStatusId = ServletRequestUtils.getIntParameter(request, "assetStatusId");
        // I put a client side check for being able to delete status but wrapping
        // this in try/catch with redirect and message just in case ;)
        try {
            assetStatusService.deleteAssetStatus(assetStatusService.getById(assetStatusId));
            assetStatusService.flush();
        } catch (Exception e) {
            logger.error("error deleting assetstatus: " + e.getMessage(), e);
            return new ModelAndView("message", "message", "Unable to delete this status. Try deleting associations with it first");
        }

        return statuses(request, response);
    }

    public ModelAndView softwareLicenses(HttpServletRequest request, HttpServletResponse response) {
        assert request != null && response != null;

        // Shortcircuit to the ad page if license doesn't allow the Asset Tracker feature
        if (!licenseService.isAssetTracker()) {
            return new ModelAndView("assetTrackerAd");
        }

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("softwareLicenses", softwareLicenseService.getAllSoftwareLicenses());
        return new ModelAndView("softwareLicenses", model);
    }

    public ModelAndView viewSoftwareLicense(HttpServletRequest request, HttpServletResponse response) {
        assert request != null && response != null;

        // Shortcircuit to the ad page if license doesn't allow the Asset Tracker feature
        if (!licenseService.isAssetTracker()) {
            return new ModelAndView("assetTrackerAd");
        }

        return new ModelAndView("viewSoftwareLicense");
    }

    public ModelAndView addSoftwareLicense(HttpServletRequest request, HttpServletResponse response) {
        assert request != null && response != null;

        // Shortcircuit to the ad page if license doesn't allow the Asset Tracker feature
        if (!licenseService.isAssetTracker()) {
            return new ModelAndView("assetTrackerAd");
        }

        return new ModelAndView("addSoftwareLicense");
    }

    public ModelAndView deleteSoftwareLicense(HttpServletRequest request, HttpServletResponse response) throws ServletRequestBindingException {
        Integer slId = ServletRequestUtils.getIntParameter(request, "slId");
        SoftwareLicense softwareLicense = softwareLicenseService.getById(slId);
        // try the delete. assume exception means that software license is in use.
        try {
            softwareLicenseService.delete(softwareLicense);
            softwareLicenseService.flush();
        } catch (Exception e) {
            logger.error("error deleting softwarelicense: " + e.getMessage(), e);
            return new ModelAndView("message", "message", "Unable to delete this software license. Try deleting associations with it first");
        }

        return softwareLicenses(request, response);
    }

    public ModelAndView fieldGroups(HttpServletRequest request, HttpServletResponse response) {
        assert request != null && response != null;

        // Shortcircuit to the ad page if license doesn't allow the Asset Tracker feature
        if (!licenseService.isAssetTracker()) {
            return new ModelAndView("assetTrackerAd");
        }

        List fieldGroups = assetFieldGroupService.getAllAssetFieldGroups();
        @SuppressWarnings("unchecked")
        Map<String, Object> model = new ModelMap();
        model.put("fieldGroups", fieldGroups);
        return new ModelAndView("fieldGroups", model);
    }

    public ModelAndView deleteFieldGroup(HttpServletRequest request, HttpServletResponse response) throws ServletRequestBindingException {
        Integer fieldGroupId = ServletRequestUtils.getIntParameter(request, "fieldGroupId");
        AssetFieldGroup afg = assetFieldGroupService.getById(fieldGroupId);
        // try the delete. assume exception means that field group is in use.
        try {
            assetFieldGroupService.deleteAssetFieldGroup(afg);
            assetFieldGroupService.flush();
        } catch (Exception e) {
            logger.error("error deleting assetfieldgroup: " + e.getMessage(), e);
            return new ModelAndView("message", "message", "Unable to delete this field group. Try deleting associations with it first");
        }

        return fieldGroups(request, response);
    }

    public ModelAndView deleteCustomField(HttpServletRequest request, HttpServletResponse response) throws ServletRequestBindingException {
        Integer custFieldId = ServletRequestUtils.getIntParameter(request, "custFieldId");
        AssetCustomField acf = assetCustomFieldService.getById(custFieldId);
        // try the delete. assume exception means that custom field is in use.
        try {
            assetCustomFieldService.deleteAssetCustomField(acf);
            assetCustomFieldService.flush();
        } catch (Exception e) {
            logger.error("error deleting assetcustomfield: " + e.getMessage(), e);
            return new ModelAndView("message", "message", "Unable to delete this custom field. It is being used by an asset.");
        }

        return customFields(request, response);
    }

    public ModelAndView customFields(HttpServletRequest request, HttpServletResponse response) throws ServletRequestBindingException {
        assert response != null;

        // Shortcircuit to the ad page if license doesn't allow the Asset Tracker feature
        if (!licenseService.isAssetTracker()) {
            return new ModelAndView("assetTrackerAd");
        }

        String fieldGroupId = ServletRequestUtils.getStringParameter(request, "fieldGroupId");
        @SuppressWarnings("unchecked")
        Map<String, Object> model = new ModelMap();
        model.put("fieldGroupId", fieldGroupId);
        model.put("assetFieldGroups", assetFieldGroupService.getAllAssetFieldGroups());
        return new ModelAndView("assetCustomFields", model);
    }

    public ModelAndView deleteAsset(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;
        Asset asset;
        String assetId = ServletRequestUtils.getStringParameter(request, "assetId");
        if (StringUtils.isNotBlank(assetId)) {
            asset = assetService.getAssetById(Integer.valueOf(assetId));
            assetService.deleteAsset(asset);

            request.setAttribute("flash.success", getMessageSourceAccessor().getMessage("assetTracker.assetDeletedSuccessfully", "Asset Deleted Successfully"));
        }

        return new ModelAndView(new RedirectView("/asset/listAsset.glml", true));

//        return new ModelAndView("message", "message", "Asset \'" + name + "\' has been deleted.");
    }

    public ModelAndView searchAsset(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;

        // Shortcircuit to the ad page if license doesn't allow the Asset Tracker feature
        if (!licenseService.isAssetTracker()) {
            return new ModelAndView("assetTrackerAd");
        }

        AssetSearch assetSearch = new AssetSearch();
        ServletRequestDataBinder binder = new ServletRequestDataBinder(assetSearch);
        binder.bind(request);
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("assetSearch", assetSearch);
        model.put("assetList", assetService.searchAsset(assetSearch));
        return new ModelAndView("searchAsset", model);
    }

    public ModelAndView optionCategoryList(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String groupId = ServletRequestUtils.getStringParameter(request, "groupId");
        List<Category> categories;
        if (StringUtils.isNotBlank(groupId)) {
            categories = categoryService.getCategoryByGroupId(Integer.valueOf(groupId));
            Collections.sort(categories);
        } else {
            categories = new ArrayList<Category>();
        }

        StringBuilder retVal = new StringBuilder("obj.options[obj.options.length] = new Option('','');\n");
        for (Category category : categories) {
            String catName = StringEscapeUtils.escapeJavaScript(category.getName());
            retVal.append("obj.options[obj.options.length] = new Option('").append(catName).append("','")
                    .append(category.getId()).append("');\n");
        }

        response.getOutputStream().println(retVal.toString());
        return null;
    }

    public ModelAndView optionCategoryOptionList(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String categoryId = ServletRequestUtils.getStringParameter(request, "categoryId");
        List<CategoryOption> catOpts;
        if (StringUtils.isNotBlank(categoryId)) {
            catOpts = this.categoryOptionService.getCategoryOptionByCategoryId(Integer.valueOf(categoryId));
            Collections.sort(catOpts);
        } else {
            catOpts = new ArrayList<CategoryOption>();
        }

        StringBuilder retVal = new StringBuilder("obj.options[obj.options.length] = new Option('','');\n");
        for (CategoryOption categoryOption : catOpts) {
            String catOptName = StringEscapeUtils.escapeJavaScript(categoryOption.getName());
            retVal.append("obj.options[obj.options.length] = new Option('").append(catOptName)
                    .append("','").append(categoryOption.getId()).append("');\n");
        }

        response.getOutputStream().println(retVal.toString());
        return null;
    }

    /**
     * Displays the ticketList.jsp page with tickets owned by the <code>User</code> on the session
     *
     * @param request  the HttpServletRequest
     * @param response the HttpServletResponse
     * @return <code>ModelAndView</code> with appropriate ticket list, metadata and the refresh url '/tickets/tlist_user.glml'
     * @throws javax.servlet.ServletException whenever one occurs
     */
    public ModelAndView listAsset(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        assert response != null;

        // Shortcircuit to the ad page if license doesn't allow the Asset Tracker feature
        if (!licenseService.isAssetTracker()) {
            return new ModelAndView("assetTrackerAd");
        }

        return new ModelAndView("assetList");
    }

    public ModelAndView exportAssets(HttpServletRequest request, HttpServletResponse response) {
        assert request != null && response != null;
        return new ModelAndView("assetExport");
    }

    public ModelAndView reports(HttpServletRequest request, HttpServletResponse response) {
        assert request != null && response != null;
        return new ModelAndView("assetReports");
    }

    public void afterPropertiesSet() throws Exception {
        if (groupService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: groupService");
        if (locationService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: locationService");
        if (categoryService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: categoryService");
        if (categoryOptionService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: categoryOptionService");
        if (assetService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: assetService");
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setCategoryOptionService(CategoryOptionService categoryOptionService) {
        this.categoryOptionService = categoryOptionService;
    }

    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }

    public void setAssetTypeService(AssetTypeService assetTypeService) {
        this.assetTypeService = assetTypeService;
    }

    public void setAssetStatusService(AssetStatusService assetStatusService) {
        this.assetStatusService = assetStatusService;
    }

    public void setAssetFieldGroupService(AssetFieldGroupService assetFieldGroupService) {
        this.assetFieldGroupService = assetFieldGroupService;
    }

    public void setVendorService(VendorService vendorService) {
        this.vendorService = vendorService;
    }

    public void setSoftwareLicenseService(SoftwareLicenseService softwareLicenseService) {
        this.softwareLicenseService = softwareLicenseService;
    }

    public void setAssetCustomFieldService(AssetCustomFieldService assetCustomFieldService) {
        this.assetCustomFieldService = assetCustomFieldService;
    }

    public void setLicenseService(LicenseService licenseService) {
        this.licenseService = licenseService;
    }
}
