package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.common.utils.UserSearch;
import net.grouplink.ehelpdesk.domain.LdapUser;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.service.LdapFieldService;
import net.grouplink.ehelpdesk.service.LdapService;
import net.grouplink.ehelpdesk.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: 3/22/12
 * Time: 5:38 PM
 */

@Controller
@RequestMapping(value = "/users/import")
public class UserLdapImportController extends ApplicationObjectSupport {
    @Autowired
    private LdapService ldapService;
    @Autowired
    private LdapFieldService ldapFieldService;
    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("userSearch", new UserSearch());
        return "users/import/index";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String searchResults(Model model, @ModelAttribute UserSearch userSearch) {

        List<LdapUser> ldapUserList = new ArrayList<LdapUser>(ldapService.searchLdapUser(userSearch, 2));
        Collections.sort(ldapUserList);

        model.addAttribute("ldapUserList", ldapUserList);
        model.addAttribute("ldapFieldList", ldapFieldService.getLdapFields());
        model.addAttribute("userSearch", userSearch);

        return "users/import/index";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/checkedUsers")
    public void importSelectedUsers(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        StringBuilder successfulImports = new StringBuilder();
        StringBuilder unsuccessfulImports = new StringBuilder();
        StringBuilder alreadyImported = new StringBuilder();

        String[] ldapDNs = ServletRequestUtils.getStringParameters(request, "ldapUserCbx");
        for (String ldapDN : ldapDNs) {
            LdapUser ldapUser = ldapService.getLdapUser(URLDecoder.decode(ldapDN, "UTF-8"));

            User user = userService.getUserByUsername(ldapUser.getCn());
            if (user == null) {
                user = userService.synchronizedUserWithLdap(user, null, ldapUser);
                if (user != null) {
                    successfulImports.append(ldapUser.getCn()).append(" ");
                } else {
                    unsuccessfulImports.append(ldapUser.getCn()).append(" ");
                }
            } else {
                userService.synchronizedUserWithLdap(user, null, ldapUser);
                alreadyImported.append(ldapUser.getCn()).append(" ");
            }
        }

        StringBuilder successMsg = new StringBuilder(getMessageSourceAccessor().getMessage("userManagement.import.success"));
        successMsg.append(successfulImports);
        StringBuilder warningMsg = new StringBuilder(getMessageSourceAccessor().getMessage("userManagement.import.alreadydone"));
        warningMsg.append(alreadyImported);
        StringBuilder errorMsg = new StringBuilder(getMessageSourceAccessor().getMessage("userManagement.import.nosuccess"));
        errorMsg.append(unsuccessfulImports);

        // only set the flash attribute if there are users to report on
        if (StringUtils.isNotBlank(successfulImports.toString())) {
            request.setAttribute("flash.success", successMsg);
        }
        if (StringUtils.isNotBlank(unsuccessfulImports.toString())) {
            request.setAttribute("flash.error", errorMsg);
        }
        if (StringUtils.isNotBlank(alreadyImported.toString())) {
            request.setAttribute("flash.info", warningMsg);
        }

        response.setStatus(HttpServletResponse.SC_OK);
    }
}
