package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.domain.DatabaseConfig;
import net.grouplink.ehelpdesk.service.DatabaseHandlerService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Date: Jul 23, 2007
 * Time: 1:57:32 PM
 */
public class DatabaseFormController extends SimpleFormController {

	private DatabaseHandlerService databaseService;
    protected final Log logger = LogFactory.getLog(getClass());

    public DatabaseFormController() {
        super();
        setCommandClass(DatabaseConfig.class);
        setCommandName("dbConfig");
        setFormView("dbConfig");
        setSuccessView("dbConfig.glml");
    }

    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        String path = request.getSession().getServletContext().getRealPath("");
        return databaseService.uploadJdbcProperties(path);
	}

    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        DatabaseConfig dbConfig = (DatabaseConfig) command;

        //TODO when flash messages are implemented show success messages and redirect instead of throwing everything into errors
        String testButton = ServletRequestUtils.getStringParameter(request, "testButton");
        if (StringUtils.isNotBlank(testButton)) {
            String error = dbConfig.canConnect();
            if (StringUtils.isNotBlank(error)) {
                errors.reject("error", "Test Failed: " + error);
            } else {
                errors.reject("error", "Connection Successful");
            }
        } else {
            String error = dbConfig.canConnect();
            if (StringUtils.isNotBlank(error)) {
                errors.reject("error", "Information not saved. Error: " + error);
            } else {
                String path = request.getSession().getServletContext().getRealPath("");
                databaseService.updateJdbcProperties(dbConfig, path);
                errors.reject("error", "Information saved successfully. Application server restart required.");
            }
        }

        return showForm(request, response, errors);
    }

    public void setDatabaseService(DatabaseHandlerService databaseService) {
        this.databaseService = databaseService;
    }
}
