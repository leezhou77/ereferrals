package net.grouplink.ehelpdesk.web.utils;

import net.grouplink.ehelpdesk.common.utils.Constants;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Jan 3, 2008
 * Time: 4:35:21 PM
 */
public class AdminLicenseIssue {

    private int type = 0;
    private String msg = "No issues";

    public AdminLicenseIssue(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMsg() {
        switch (type)
        {
            case Constants.TECHS_EXCEEDED:
                msg = "Number of technicians exceeds number allowed by license.";
                break;

            case Constants.STANDARD_GROUPS_EXCEEDED:
                msg = "Number of groups exceeds number allowed for Standard edition.";
                break;

            case Constants.ENTERPRISE_GROUPS_EXCEEDED:
                msg = "Number of groups exceeds number allowed for Enterprise edition.";
                break;
        }
        return msg;
    }





}
