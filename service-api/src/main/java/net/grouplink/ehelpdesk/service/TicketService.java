package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.AdvancedTicketsFilter;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.EmailToTicketConfig;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Status;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketPriority;
import net.grouplink.ehelpdesk.domain.TicketTemplateLaunch;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.domain.TicketTemplate;
import net.grouplink.ehelpdesk.domain.TicketHistory;
import net.grouplink.ehelpdesk.domain.CustomFieldValues;
import net.grouplink.ehelpdesk.domain.SurveyData;
import net.grouplink.ehelpdesk.domain.asset.ZenAsset;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Services for accessing and modifying a Ticket.
 *
 * @see net.grouplink.ehelpdesk.domain.Ticket
 */
public interface TicketService {

    List<Ticket> getTickets();

    /**
     * The ticket object return by this function contains a copy of the old ticket in the oldTicket field
     * the oldTicket can be accessed by calling the getOldTicket() function
     * The oldTicket wil lbe null if the ticket is a new object
     *
     * @param id the id of the ticket to retrieve
     * @return the Ticket
     */
    Ticket getTicketById(Integer id);

//    public void saveTicketAndSendNotification(User user, String mailMessage, Ticket ticket, String emailFromAddress, String emailDisplayName, Boolean notifyTech, Boolean notifyUser, Boolean sendLinkToUser, String baseUrl) throws MessagingException, UnsupportedEncodingException;

    void saveTicket(Ticket ticket, User user);

    void saveTickets(Collection<Ticket> ticketList, User user);

    /**
     * Marks a Ticket with Deleted status. Does NOT remove it from the db.
     * @param ticket the ticket to mark as deleted
     * @param user the user deleting the ticket
     */
    void deleteTicket(Ticket ticket, User user);

    /**
     * Removes a Ticket from the database.
     * @param ticket the ticket to delete
     */
    void purgeTicket(Ticket ticket);

    List<Ticket> getTicketsByOwner(User user, Boolean showClosed);

    Integer getTicketsByOwnerCount(User user);

    Integer getTicketsByUrgIdCount(Integer urgId);

    List<Ticket> getTicketsAssignedToByUserId(Integer userId);

    List<Ticket> getTicketsAssignedToByUserRoleGroup(UserRoleGroup userRoleGroup, Boolean showClosed);

    List<Ticket> getAllGroupTicketsByGroupId(Integer groupId, Boolean showClosed);

    List<Ticket> getFilteredTickets(User user, UserRoleGroup urg, Locale locale, String grouping, Boolean showClosed);

    List<Ticket> getFilteredTickets(String byWho, User user, UserRoleGroup urg, Locale locale, String ticketId, String subject,
                            String categoryOption, String assignedTo, String status, String creationDate,
                            String location, String priority, String grouping, Boolean showClosed, String contact);

    List<Ticket> getTicketsByCriteria(Group group, Set<Status> statuses, Set<TicketPriority> priorities, Category category, CategoryOption categoryOption);

    List<Ticket> getTicketsByAdvancedSearch(AdvancedTicketsFilter filter);

    void flush();

    Ticket getTicketByOldTicketIdAndGroupId(Integer oldTicketId, Integer groupId);

    void clear();

    int getTicketCountByGroupId(Integer groupId);

    int getTicketCountByCatOptId(Integer catOptId);

    int getTicketCountByStatusId(Integer statusId);

    int getTicketCountByPriorityId(Integer priorityId);

    void saveMigrationTicket(Ticket ticket);

    /**
     * Sends email notification to users associated with ticket.
     *
     * @param ticket      the Ticket
     * @param user        the User that triggered the creation or change to the ticket
     * @param htmlMessage the HTML message to be sent in the email
     * @param textMessage the text message to be sent in the email
     * @throws MessagingException           if a mail error occurs
     * @throws UnsupportedEncodingException if the mailMessage encoding is not supported
     */
    void sendEmailNotification(Ticket ticket, User user, String htmlMessage, String textMessage)
            throws MessagingException, UnsupportedEncodingException;

    /**
     * Creates an html String containing a description of a new Ticket, or
     * changes to an existing Ticket (by comparing the newTicket and oldTicket).
     *
     * @param newTicket the new Ticket or Ticket with changes that have been made
     * @param oldTicket the previous state of the Ticket, or null for a new Ticket
     * @return an html String containing the description
     */
    String createHTMLComments(Ticket newTicket, Ticket oldTicket);

    /**
     * Creates a test String containing a description of a new Ticket, or
     * changes to an existing Ticket (by comparing the newTicket and oldTicket).
     *
     * @param newTicket the new Ticket or Ticket with changes that have been made
     * @param oldTicket the previous state of the Ticket, or null for a new Ticket
     * @return an text String containing the description
     */
    String createTextComments(Ticket newTicket, Ticket oldTicket);

    /**
     * This should only be used on ticketFromController after setting up the custom field
     * on the onSubmit function
     *
     * @param ticket the Ticket to save the custom field for
     */
    void saveTicketCustomFields(Ticket ticket);

    List<Ticket> getTicketsByEmailToTicketConfig(EmailToTicketConfig emailToTicketConfig);

    Ticket getTicketByOldTicketId(Integer ticketId);

    List<Ticket> getTicketsByZenAsset(ZenAsset zenAsset);

    List<Ticket> getTopLevelTicketsByTicketTemplateLaunch(TicketTemplateLaunch ttl);

    List<Ticket> getTicketsByTicketTemplate(TicketTemplate ticketTemplate);

    /**
     * Gets the most recent TicketHistory for a Ticket, or null if no
     * TicketHistory objects exist.
     * @param ticket the Ticket
     * @return the most recent TicketHistory, or null if empty
     */
    TicketHistory getMostRecentTicketHistory(Ticket ticket);

    /**
     * Gets the CustomFieldValues for the Ticket that matches the given column.
     * @param ticket the Ticket
     * @param column the column name, matches the customFieldValues.customField.name
     * @return the CustomFieldValues object, or null if no CustomFieldValues found for this name
     */
    CustomFieldValues getCustomFieldValue(Ticket ticket, String column);

    /**
     * Gets the SurveyData for the given Ticket and question text.
     * @param ticket the Ticket
     * @param questionName the survey question text
     * @return the SurveyData if it exists, null otherwise
     */
    SurveyData getSurveyData(Ticket ticket, String questionName);

    /**
     * Converts the ids that may be in the from and to fields to the appropriate object name
     *
     * @param ticket the command object
     */
    void initializeAuditList(Ticket ticket);

    int getCountByCategory(Category category);

    int getCountByCategoryOption(CategoryOption categoryOption);

    void evict(Ticket ticket);
}
