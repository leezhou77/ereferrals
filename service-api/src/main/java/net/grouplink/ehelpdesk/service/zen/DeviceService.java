package net.grouplink.ehelpdesk.service.zen;

import net.grouplink.ehelpdesk.domain.zen.Device;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Apr 1, 2009
 * Time: 2:48:31 PM
 */
public interface DeviceService {
    List<Device> getAll();
}
