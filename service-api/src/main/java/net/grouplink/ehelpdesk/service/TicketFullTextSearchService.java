package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.User;

import java.util.List;

/**
 * Author: zpearce
 * Date: 4/27/11
 * Time: 10:29 AM
 */
public interface TicketFullTextSearchService {
    void reindexTickets();
    int getReindexStatusPercent();
    List<Ticket> search(String query, int startPage, int pageSize, User user);
    int searchCount(String query, User user);
}
