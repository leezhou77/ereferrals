package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.CustomField;
import net.grouplink.ehelpdesk.domain.CustomFieldType;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.domain.Ticket;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: mabbasi
 * Date: May 5, 2007
 * Time: 3:13:22 PM
 */
public interface CustomFieldsService {
    void saveCustomField(CustomField customField);

    List<CustomField> getCustomFieldsByCategoryOptionId(Integer id);

    CustomField getCustomFieldById(Integer id);

    void deleteCustomField(CustomField customField);

    Map<String, String> getCustomFieldsInHtml(Ticket ticket);

    CustomField getCustomFieldByOldIdAndCatOptId(Integer oldId, Integer catOptId);

    List<CustomField> getAll();

    List<String> getByUniqueName();

    int getCountByCategoryOption(CategoryOption categoryOption);

    List<CustomField> getGlobalCustomFields();

    List<CustomField> getByLocation();

    void migrateFieldTypes();

    List<CustomField> getCustomFields(Location location, Group group, Category category, CategoryOption categoryOption);

    List<CustomField> getCustomFieldsByGroup(Group group);
}
