package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.CustomField;
import net.grouplink.ehelpdesk.domain.CustomFieldValues;

/**
 * Created by IntelliJ IDEA.
 * User: mabbasi
 * Date: May 7, 2007
 * Time: 9:57:18 PM
 */
public interface CustomFieldValuesService {
    public CustomFieldValues getValuesByTicketCustomField(Integer ticketId, CustomField customField);
    public void saveCustomFieldValues(CustomFieldValues customFieldValues);
    public boolean customFieldHasValues(CustomField customField);
}
