package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.TicketTemplateRecurrenceSchedule;

public interface TicketTemplateRecurrenceScheduleService {

    void save(TicketTemplateRecurrenceSchedule ticketTemplateRecurrenceSchedule);

    void delete(TicketTemplateRecurrenceSchedule ticketTemplateRecurrenceSchedule);

    void processSchedules();

    void cleanupOrphanedPending();
}
