package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.WorkflowStep;

public interface WorkflowStepService {
    WorkflowStep getById(Integer workflowStepId);

    void delete(WorkflowStep ws);
}
