package net.grouplink.ehelpdesk.service;

import org.springframework.security.core.AuthenticationException;

import java.util.Map;
import java.util.Date;
import java.io.File;
import java.io.IOException;

/**
 * @author mrollins
 * @version 1.0
 */
public interface LicenseService {

    final static String LICENSE_FILE = File.separator + "WEB-INF" + File.separator + "classes" + File.separator + "conf" + File.separator + "license.lic";
    final static String HELPDESK_DESC = "HelpDesk.Desc";
    final static String HELPDESK_TECHNICIANS = "HelpDesk.Technicians";
    final static String HELPDESK_GROUPS = "HelpDesk.Groups";
    final static String HELPDESK_BOMB = "HelpDesk.Bomb";
    final static String HELPDESK_VERSION = "HelpDesk.Version";
    final static String HELPDESK_ENTERPRISE = "HelpDesk.Enterprise";
    final static String HELPDESK_UPGRADE_DATE = "HelpDesk.Upgrade Date Int";
    final static String HELPDESK_DASHBOARD = "HelpDesk.Dashboard";
    final static String HELPDESK_TICKETTEMPLATES = "HelpDesk.Ticket Templates";
    final static String HELPDESK_SCHEDULEDREPORTS = "HelpDesk.Scheduled Reports";
    final static String HELPDESK_ASSETTRACKER = "HelpDesk.Asset Tracker";
    final static String HELPDESK_ZEN10INTEGRATION = "HelpDesk.Zen10 Integration";
    final static String HELPDESK_WF_PARTICIPANTS = "HelpDesk.Workflow Participants";

    void init(String realPath);

    Map getLicenseProperties() throws Exception;

    void uploadLicenseFile(String path, byte[] bytes) throws IOException;

    String getDescription();

    Integer getTechnicianCount() throws AuthenticationException;

    Integer getWFParticpantCount() throws AuthenticationException;

    Integer getGroupCount() throws AuthenticationException;

    boolean areYouThereLicense();

    Date getGlobalBombDate() throws AuthenticationException;

    Boolean isEnterprise() throws AuthenticationException;

    boolean isValidEHDLicense(String testFilePath) throws Exception;

    Date getReleaseDate();

    Date getUPDate();

    boolean isDashboard();

    boolean isTicketTemplates();

    boolean isScheduledReports();

    boolean isAssetTracker();

    boolean isZen10Integration();

    boolean isMaintenanceExpired();
}
