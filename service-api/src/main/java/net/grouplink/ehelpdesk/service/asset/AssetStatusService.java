package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.domain.asset.AssetStatus;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Jul 31, 2008
 * Time: 11:54:32 AM
 */
public interface AssetStatusService {
    AssetStatus getById(Integer assetStatusId);

    AssetStatus getByName(String name);

    List<AssetStatus> getAllAssetStatuses();

    List<AssetStatus> getUndeletableStatus();

    void saveAssetStatus(AssetStatus assetStatus);

    void deleteAssetStatus(AssetStatus assetStatus);

    void flush();
}
