package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.UserRole;
import net.grouplink.ehelpdesk.domain.User;

public interface UserRoleService {
	public UserRole getUserRoleById(Integer id);
	public void deleteUserRole(UserRole userRole);
    public UserRole getUserRoleByUserIdAndRoleId(Integer userId, Integer roleId);
    public User getUserByUserNameAndRoleId(String userName, Integer roleId);

    User getUserByEmailAndRoleId(String email, Integer roleId);

    void flush();
}
