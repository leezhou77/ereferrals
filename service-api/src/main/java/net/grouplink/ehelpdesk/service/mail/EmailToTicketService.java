package net.grouplink.ehelpdesk.service.mail;

import net.grouplink.ehelpdesk.domain.EmailToTicketConfig;

/**
 * User: jaymehafen
 * Date: Jan 26, 2008
 * Time: 4:19:34 PM
 */
public interface EmailToTicketService {

    /**
     * Process all incoming mail, based on configured EmailToTicketConfig
     * objects.
     */
    void processIncomingMail();

    /**
     * Connect to the Folder specified by the EmailToTicketConfig. Returns the
     * JavaMail Folder if successful, throws EmailToTicketRuntimeException is
     * unsuccessful.
     *
     * @param emailToTicketConfig the EmailToTicketConfig
     * @return the JavaMail Folder
     */
    MailMonitorFolder connectToFolder(EmailToTicketConfig emailToTicketConfig);

}
