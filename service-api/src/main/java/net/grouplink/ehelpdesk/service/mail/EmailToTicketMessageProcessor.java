package net.grouplink.ehelpdesk.service.mail;

import net.grouplink.ehelpdesk.domain.mail.EmailMessage;
import net.grouplink.ehelpdesk.domain.EmailToTicketConfig;

/**
 * User: jaymehafen
 * Date: Feb 4, 2008
 * Time: 1:24:39 PM
 */
public interface EmailToTicketMessageProcessor {
    /**
     * Process a single EmailMessage, creating a new Ticket or adding a
     * TicketHistory object to an existing Ticket.
     * @param emailMessage the contents of the email
     * @param emailToTicketConfig the EmailToTicketConfig object
     */
    void processE2TMessage(EmailMessage emailMessage, EmailToTicketConfig emailToTicketConfig);
}
