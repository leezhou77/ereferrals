package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.TicketTemplateLaunch;
import net.grouplink.ehelpdesk.domain.TicketTemplateMaster;
import net.grouplink.ehelpdesk.domain.User;

import java.util.List;

public interface TemplateMasterService {

    List<TicketTemplateMaster> getAll();

    List<TicketTemplateMaster> getAllPublic();

    void flush();

    TicketTemplateMaster getById(Integer id);

    void save(TicketTemplateMaster master);

    void delete(TicketTemplateMaster master);

    TicketTemplateLaunch launchTicketTemplateMaster(TicketTemplateMaster ticketTemplateMaster, User user);

    TicketTemplateMaster cloneTicketTemplateMaster(TicketTemplateMaster master);

    int getCountByGroup(Group group);

    void clear();
}
