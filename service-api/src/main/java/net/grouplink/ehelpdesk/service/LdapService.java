package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.LdapUser;
import net.grouplink.ehelpdesk.common.utils.TreeNode;
import net.grouplink.ehelpdesk.common.utils.UserSearch;
import net.grouplink.ehelpdesk.common.utils.ldap.LdapConfig;
import org.springframework.security.core.AuthenticationException;

import java.util.List;
import java.util.Set;

public interface LdapService {

	boolean init();
    Set<LdapUser> authenticateUser(String username, String password) throws AuthenticationException;
    List<TreeNode> getFirstLevelTree(String dn);
	LdapUser getLdapUser(String dn);
	LdapConfig getLdapConfig();
	void saveLdapConfig(LdapConfig ldapConfig);
    Set<LdapUser> searchLdapUser(UserSearch userSearch, int searchType);
    byte[] getGuidForUserDn(String dn);
}