package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.WorkflowStepStatus;
import net.grouplink.ehelpdesk.domain.WorkflowStep;

public interface WorkflowStepStatusService {

    WorkflowStepStatus getById(Integer id);

    void save(WorkflowStepStatus workflowStepStatus);

    void delete(WorkflowStepStatus workflowStepStatus);

    int getCountByWorkflowStep(WorkflowStep workflowStep);

    void checkForCompletedSteps();
}
