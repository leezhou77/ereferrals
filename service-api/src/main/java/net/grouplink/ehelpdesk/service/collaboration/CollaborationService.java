/**
 * 
 * Currently, this only defines the integration with the calendaring component
 * of the collaboration system, specifically Appointments, Tasks, and Free/Busy
 * searches.
 * 
 * @author derickson
 * Date: 4/23/07
 */
package net.grouplink.ehelpdesk.service.collaboration;

import net.grouplink.ehelpdesk.common.collaboration.AddressBookContactsResult;
import net.grouplink.ehelpdesk.common.collaboration.CalendarItem;
import net.grouplink.ehelpdesk.common.collaboration.FreeBusyResult;
import net.grouplink.ehelpdesk.common.collaboration.NameAndEmail;
import net.grouplink.ehelpdesk.common.collaboration.Result;
import net.grouplink.ehelpdesk.common.collaboration.Session;
import net.grouplink.ehelpdesk.common.collaboration.CollaborationConfig;
import net.grouplink.ehelpdesk.common.collaboration.LoginResult;
import net.grouplink.ehelpdesk.domain.User;

import java.util.Calendar;

/**
 * Defines an interface to integrate with a collaboration system (i.e. Groupwise).
 * @author derickson
 */
public interface CollaborationService {

	public static final int CONNECTION_ERROR = 1;
	public static final int NOT_CONFIGURED = 2;
	public static final String NOT_CONFIGURED_MSG = "Collaboration service has not been configured";

	public CollaborationConfig getCollaborationConfig();
	public void saveCollaborationConfig(CollaborationConfig config);

	/**
	 * Perform initial configuration of the service.
	 */
	public void configure();

	public void disableConfiguration();

	/**
	 * Reports the configuration status of the service.
	 * @return Configuration status of the service
	 */
	public boolean isConfigured();

	/**
	 * Login as a user to the collaboration system.  This form of authentication
	 * does not require a password.
	 *
	 * @param userName - the user name to login as.
	 * @return LoginResult - If the login succeeds, this will contain a session
	 * 	object to be used when invoking other collaboration service methods.
	 * 	If the login fails, this will contain error information.
	 */
	public LoginResult login(String userName);

	/**
	 * Login as a user to the collaboration system.  This form of authentication
	 * requires a password.
	 *
	 * @param userName - the user name to login as.
	 * @param password - the corresponding password.
	 * @return LoginResult - If the login succeeds, this will contain a session
	 * 	object to be used when invoking other collaboration service methods.
	 * 	If the login fails, this will contain error information.
	 */
	public LoginResult login(String userName, String password);

	/**
	 * Log a user out of the collaboration system.
	 * @param session - the session object obtained from the login method.
	 * @return Result - contains error code and description on failure.
	 * 					The error code will be zero on success.
	 */
	public Result logout(Session session);

	/**
	 * Schedules or posts a calendar item on the users calendar
	 * @param item - the calendar item to schedule
	 * @param session - the session object obtained from the login method.
	 * @return Result - contains error code and description on failure.
	 * 					The error code will be zero on success.
	 */
	public Result scheduleCalendarItem(CalendarItem item, Session session);

	/**
	 * Perform a free/busy search on the users between the specified times.
	 * @param users - list of users included in the free/busy search
	 * @param startDate - Date and time for the beginning of the busy search
	 * @param endDate - Date and time for the end of the busy search
	 * @param session - the session object obtained from the login method.
	 * @return FreeBusyResult - On success, this will contain a list of
	 * 						FreeBusyInfo objects containing the free/busy
	 * 						information for each user.
	 */
	public FreeBusyResult freeBusySearch(NameAndEmail[] users, Calendar startDate,
			Calendar endDate, Session session);

	/**
	 * Get a list of users (consisting of their display name and email address)
	 * from the system address book.
	 * @param session - the session object obtained from the login method.
	 * @return AddressBookContactsResult - On success, this will contain a list
	 * 				of NameAndEmail objects.  Otherwise, it will contain error
	 * 				information.
	 */
	public AddressBookContactsResult getSystemAddressBookContacts(Session session);

    Session getCollaborationSession(User user, String password);
}