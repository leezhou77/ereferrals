package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.MyTicketTab;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;

import java.util.List;

public interface MyTicketTabService {

    MyTicketTab getById(Integer id);

    List<MyTicketTab> getByUser(User user);

    void save(MyTicketTab myTicketTab);

    void delete(MyTicketTab myTicketTab);

    void saveAll(List<MyTicketTab> myTicketTabs);

    List<MyTicketTab> initializeTabs(User user);

    List<MyTicketTab> getByTicketFilter(TicketFilter tf);
}
