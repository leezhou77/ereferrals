package net.grouplink.ehelpdesk.service.zen;

import net.grouplink.ehelpdesk.domain.zen.UserPrimaryDeviceInformation;

import java.util.List;

public interface UserPrimaryDeviceInformationService {
    List<UserPrimaryDeviceInformation> getByZuid(byte[] zuid);
}
