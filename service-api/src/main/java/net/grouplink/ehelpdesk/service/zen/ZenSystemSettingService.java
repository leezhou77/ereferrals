package net.grouplink.ehelpdesk.service.zen;

import net.grouplink.ehelpdesk.domain.zen.ZenSystemSetting;

public interface ZenSystemSettingService {

    ZenSystemSetting getByName(String name);

    String getRemotePort();

}
