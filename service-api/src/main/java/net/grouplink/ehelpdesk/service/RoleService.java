package net.grouplink.ehelpdesk.service;

import java.util.List;
import java.util.Locale;

import net.grouplink.ehelpdesk.domain.Role;

public interface RoleService {

    List<Role> getRoles();
    List<Role> getMngAndTechRoles();
    List<Role> getNonAdminRoles();
    List<Role> getGroupRoles();
    Role getRoleById(Integer id);
	Role getRoleByName(String roleName);
    List<String> getRoleNames(Locale locale);
    void init();
}
