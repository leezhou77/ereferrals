package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.CustomField;

import java.util.List;
import java.util.Set;

/**
 * @author mrollins
 * @version 1.0
 */
public interface CategoryOptionService {

    public List<CategoryOption> getCategoryOptions();

    public CategoryOption getCategoryOptionById(Integer id);

    public CategoryOption getCategoryOptionByName(String name);

    public List<CategoryOption> getCategoryOptionByGroupId(Integer id);

    public void saveCategoryOption(CategoryOption catOption);

    public void deleteCategoryOption(CategoryOption catOption);

    public CategoryOption getCategoryOptionByNameAndCategoryId(String catOptName, Category category);

    public CategoryOption getDuplicateCategoryOption(CategoryOption catOpt);

    public Set<CustomField> getCategoryOption(CategoryOption categoryOption, CustomField customField);

    public List<CategoryOption> getCategoryOptionByCategoryId(Integer categoryId);

    public void flush();

    public CategoryOption getCategoryOptionByNameAndGroupId(String catOptName, Integer id);

    boolean deleteCategoryOptionIfUnused(CategoryOption categoryOption);

    boolean categoryOptionCanBeDeleted(CategoryOption categoryOption);
}
