package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.domain.asset.AssetType;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Jul 17, 2008
 * Time: 1:35:04 PM
 */
public interface AssetTypeService {
    AssetType getById(Integer id);

    AssetType getByName(String name);

    List<AssetType> getAllAssetTypes();

    List<AssetType> getUndeletableAssetTypes();

    void saveAssetType(AssetType assetType);

    void deleteAssetType(AssetType assetType);

    void flush();
}
