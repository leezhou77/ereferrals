package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.EmailToTicketConfig;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.User;

import java.util.List;

/**
 * Service interface for working with EmailToTicketConfig domain objects.
 *
 * User: jaymehafen
 * Date: Jan 4, 2008
 * Time: 4:37:31 PM
 */
public interface EmailToTicketConfigService {

    /**
     * Gets a list of all EmailToTicketConfig objects in the database.
     * @return list of all EmailToTicketConfig objects
     */
    List<EmailToTicketConfig> getAll();

    /**
     * Gets a list of all enabled EmailToTicketConfig objects.
     * @return list of all enabled EmailToTicketConfig objects
     */
    List<EmailToTicketConfig> getAllEnabled();

    /**
     * Gets a particular EmailToTicketConfig instance by id.
     * @param id the id of the EmailToTicketConfig instance to get
     * @return the EmailToTicketConfig instance
     */
    EmailToTicketConfig getById(Integer id);

    /**
     * Looks up an EmailToTicketConfig with the given Group.
     * @param group The Group associated with the EmailToTicketConfig
     * @return the EmailToTicketConfig instance
     */
    EmailToTicketConfig getByGroup(Group group);

    /**
     * Saves the given EmailToTicketConfig object.
     * @param emailToTicketConfig the EmailToTicketConfig instance to save
     */
    void save(EmailToTicketConfig emailToTicketConfig);

    /**
     * Deletes the given EmailToTicketConfig instance.
     * @param emailToTicketConfig the EmailToTicketConfig instance to delete
     * @param user the user making the changes
     */
    void delete(EmailToTicketConfig emailToTicketConfig, User user);

   /**
     * Retrieves the number of EmailToTicketConfig objects that are associated with an active priority.
     * @param priorityId the id of the active priority that this object is associated with
     */
    int getEmailToTicketConfigCountByPriorityId(Integer priorityId);

    int getCountByGroup(Group group);
}
