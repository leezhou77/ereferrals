package net.grouplink.ehelpdesk.service;

import java.util.List;

import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.common.utils.GroupSearch;

public interface GroupService {

    /**
     * Gets the list of all active Groups.
     * @return the list of active Groups
     */
    List<Group> getGroups();

    Group getGroupById(Integer id);

    List<Group> getGroupsByTechId(Integer userId);

    List<Group> getGroupsByManagerId(Integer userId);

    void saveGroup(Group group);

    void deleteGroup(Group group);

    List<Group> searchGroup(GroupSearch groupSearch);

    boolean hasDuplicateGroupName(Group group);

    Group getGroupByName(String groupName);

    Integer getGroupCount();

    List<Group> getGroupsByAssetTrackerType(String assetTrackerType);

    void flush();

    /**
     * Gets the list of all Groups, both active and inactive.
     * @return the list of all Groups
     */
    List<Group> getAllGroups();

    boolean deleteGroupIfUnused(Group group);

    boolean groupCanBeDeleted(Group group);
}
