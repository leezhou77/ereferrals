package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.TicketPriority;

import java.util.List;

/**
 * @author mrollins
 * @version 1.0
 */
public interface TicketPriorityService {
    List<TicketPriority> getPriorities();
    TicketPriority getPriorityById(Integer id);
    TicketPriority getPriorityByName(String name);
    void savePriority(TicketPriority priority);
    void deletePriority(TicketPriority priority);
    TicketPriority getInitialPriority();

    void flush();
}
