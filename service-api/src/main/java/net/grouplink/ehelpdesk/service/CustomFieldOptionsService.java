package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.CustomFieldOption;
import net.grouplink.ehelpdesk.domain.CustomField;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: mabbasi
 * Date: May 5, 2007
 * Time: 3:11:52 PM
 */
public interface CustomFieldOptionsService {

    public void saveCustomFieldOption(CustomFieldOption customFieldOption);

    /**
     * Calls the CustomFieldOptionsDao object in the persistent layer to save a list of customFieldOption objects
     * and their corresponding attributes in the database.
     * @param customFieldOptions list
     */
    public void saveCustomFieldOptions(List<CustomFieldOption> customFieldOptions);

    /**
     * Returns a list of custom field options for a given custom field id from the persistent layer.
     * @param id the id
     * @return custom field options list (pojos)
     */
    public List<CustomFieldOption> getCustomFieldOptions(Integer id);

    /**
     * Returns a custom field option object for a given custom field id from the persistent layer.
     * @param id the id
     * @return custom field options (pojo)
     */
    public CustomFieldOption getCustomFieldOptionByOptionId(Integer id);

    /**
     * Creates a custom field options with all gathered attributes from the customField.jsp form.
     * @param fieldValue the label
     * @param displayValue  the value to display
     * @param customField the custom to create the Options
     * @return CustomFieldOption pojo
     */
    public CustomFieldOption createCustomFieldOption(String fieldValue, String displayValue, CustomField customField);

    /**
     * Calls the persistent layer to delete a list of customFieldOptions from the database.
     * @param customFieldOptions the options to delete
     */
    public void deleteCustomFieldOptions(List<CustomFieldOption> customFieldOptions);

    /**
     * Calls the persistent layer to delete a customFieldOption item from the database.
     * @param customFieldOption the option to delete
     */
    public void deleteCustomFieldOption(CustomFieldOption customFieldOption);
}
