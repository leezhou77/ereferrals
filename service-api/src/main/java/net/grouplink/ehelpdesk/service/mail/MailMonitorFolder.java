package net.grouplink.ehelpdesk.service.mail;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.mail.Folder;
import javax.mail.Store;

public class MailMonitorFolder {
    private Log log = LogFactory.getLog(MailMonitorFolder.class);
    private Store store;
    private Folder folder;

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Folder getFolder() {
        return folder;
    }

    public void setFolder(Folder folder) {
        this.folder = folder;
    }

    public void close(boolean expunge) {
        // close the folder
        if (folder != null) {
            try {
                folder.close(expunge);
            } catch (Exception e) {
                log.error("error closing mail folder", e);
            }
        }

        // close the store
        if (store != null) {
            try {
                store.close();
            } catch (Exception e) {
                log.error("error closing mail store", e);
            }
        }
    }
}
