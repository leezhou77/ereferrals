package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.Report;
import net.grouplink.ehelpdesk.domain.ReportRecurrenceSchedule;

public interface ReportRecurrenceScheduleService {

    void save(ReportRecurrenceSchedule reportRecurrenceSchedule);

    void delete(ReportRecurrenceSchedule reportRecurrenceSchedule);

    void processSchedules();

    void cleanupTmpDir(Report report);
}