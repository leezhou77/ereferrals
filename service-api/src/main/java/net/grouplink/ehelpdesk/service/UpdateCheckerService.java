package net.grouplink.ehelpdesk.service;

public interface UpdateCheckerService {
    boolean isUpdateAvailable();

    String getDownloadUrl();

    void checkForUpdate();
}
