package net.grouplink.ehelpdesk.newinstaller.install4j.actions;

import com.install4j.api.actions.InstallAction;
import com.install4j.api.context.Context;
import com.install4j.api.context.UserCanceledException;
import com.install4j.runtime.beans.actions.misc.RunExecutableAction;

/**
 * @author zpearce
 */
public class RetryExecutableAction extends RunExecutableAction implements InstallAction {
    private int retryLimit;
    private int retryIntervalMs;

    public int getRetryLimit() {
        return retryLimit;
    }

    public void setRetryLimit(int retryLimit) {
        this.retryLimit = retryLimit;
    }

    public int getRetryIntervalMs() {
        return retryIntervalMs;
    }

    public void setRetryIntervalMs(int retryIntervalMs) {
        this.retryIntervalMs = retryIntervalMs;
    }

    public boolean execute(Context context) throws UserCanceledException {
        if(retryLimit == -1) {
            while(true) {
                if(super.execute(context)) {
                    return true;
                }
                try {
                    Thread.sleep(retryIntervalMs);
                } catch (InterruptedException e) {
                    //do nothing
                }
            }
        }
        else {
            int counter = 0;
            while (counter < retryLimit) {
                if(super.execute(context)) {
                    return true;
                }
                try {
                    Thread.sleep(retryIntervalMs);
                } catch (InterruptedException e) {
                    //do nothing
                }
                counter++;
            }
        }
        return false;
    }
}
