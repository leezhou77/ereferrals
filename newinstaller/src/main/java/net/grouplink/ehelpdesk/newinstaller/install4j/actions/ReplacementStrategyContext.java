package net.grouplink.ehelpdesk.newinstaller.install4j.actions;

import com.install4j.api.context.InstallerContext;
import de.schlichtherle.io.File;

/**
 * @author zpearce
 */
public class ReplacementStrategyContext {
    private ReplacementStrategy strategy;
    
    public ReplacementStrategyContext(ReplacementStrategy strategy) {
        this.strategy = strategy;
    }

    public boolean executeStrategy(InstallerContext context, File file) {
        return strategy.execute(context, file);
    }
}
