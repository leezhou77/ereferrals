package net.grouplink.ehelpdesk.newinstaller.install4j.actions;

import com.install4j.api.actions.AbstractInstallAction;
import com.install4j.api.context.InstallerContext;
import de.schlichtherle.io.File;

/**
 * @author zpearce
 */
public class ReplaceInstallerVariablesInZipFileAction extends AbstractInstallAction {
    private String filePath;

    public String getFilePath() {
        return replaceVariables(filePath);
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public boolean install(InstallerContext context) {
        ReplacementStrategyContext stratContext;
        if(filePath.endsWith(".properties")) {
            stratContext = new ReplacementStrategyContext(new PropertiesFileReplacementStrategy());
        } else {
            stratContext = new ReplacementStrategyContext(new TextFileReplacementStrategy());
        }
        return stratContext.executeStrategy(context, new File(getFilePath()));
    }
}
