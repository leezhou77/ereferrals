package net.grouplink.ehelpdesk.newinstaller;

import de.schlichtherle.io.FileInputStream;
import junit.framework.AssertionFailedError;
import junit.framework.TestCase;
import net.grouplink.ehelpdesk.newinstaller.install4j.actions.CopyFileOrDirIntoZipFile;
import org.apache.commons.io.FileUtils;

import de.schlichtherle.io.File;
import java.io.IOException;
import java.util.Properties;

/**
 * @author zpearce
 */
public class InstallerActionsTest extends TestCase {
    private File destZip;
    
    public void setUp() throws IOException {
        //set up a copy of the test war file
        java.io.File originalZip = new java.io.File(getClass().getClassLoader().getResource("test.war").getPath());
        java.io.File copiedZip = new java.io.File(originalZip.getParentFile().toString() + "/testrun.war");
        FileUtils.copyFile(originalZip, copiedZip);

        destZip = new File(originalZip.getParentFile().toString() + "/testrun.war");
    }

    public void testCopyDirIntoZip() {
        CopyFileOrDirIntoZipFile action = new CopyFileOrDirIntoZipFile();
        action.setSourceFileOrDir(getClass().getClassLoader().getResource("conf").getPath());
        action.setDestFileOrDir(destZip + "/WEB-INF/classes/conf");
        assertTrue(action.install(null));

        Properties jdbcProps = new Properties();
        Properties zen10configProps = new Properties();
        Properties zen10JdbcProps = new Properties();

        try {
            FileInputStream fis = new FileInputStream(new File(destZip, "/WEB-INF/classes/conf/jdbc.properties"));
            jdbcProps.load(fis);
            assertEquals("YayGrouplink", jdbcProps.getProperty("test"));

            fis = new FileInputStream(new File(destZip, "/WEB-INF/classes/conf/zen10config.properties"));
            zen10configProps.load(fis);
            assertEquals("zenconfigawesomeness", zen10configProps.getProperty("test"));

            fis = new FileInputStream(new File(destZip, "/WEB-INF/classes/conf/zen10-jdbc.properties"));
            zen10JdbcProps.load(fis);
            assertEquals("zenjdbcisthebomb", zen10JdbcProps.getProperty("test"));
        } catch (Exception e) {
            AssertionFailedError ase = new AssertionFailedError(e.getMessage());
            ase.initCause(e);
            throw ase;
        }
    }

    public void testCopyFileIntoZip() {
        CopyFileOrDirIntoZipFile action = new CopyFileOrDirIntoZipFile();
        action.setSourceFileOrDir(getClass().getClassLoader().getResource("conf/jdbc.properties").getPath());
        action.setDestFileOrDir(destZip + "/WEB-INF/classes/test.properties");
        assertTrue(action.install(null));

        try {
            File jdbcFile = new File(destZip, "/WEB-INF/classes/test.properties");
            assertTrue(jdbcFile.exists());
        } catch (Exception e) {
            AssertionFailedError ase = new AssertionFailedError(e.getMessage());
            ase.initCause(e);
            throw ase;
        }
    }

    public void tearDown() {
        //copiedZip.deleteOnExit();
    }
}
