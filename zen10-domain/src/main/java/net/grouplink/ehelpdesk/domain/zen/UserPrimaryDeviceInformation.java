package net.grouplink.ehelpdesk.domain.zen;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.FetchType;
import java.io.Serializable;

@Entity
@Table(name = "zUserPrimaryDeviceInformation")
public class UserPrimaryDeviceInformation implements Serializable {
    private byte[] id;
    private Device device;
    private Integer loginCount;
    private Integer loginMinutes;
    private byte[] extraUserInformation;

    @Id
    @Column(name = "Id")
    public byte[] getId() {
        return id;
    }

    public void setId(byte[] id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DeviceUID")
    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    @Column(name = "LoginCount")
    public Integer getLoginCount() {
        return loginCount;
    }

    public void setLoginCount(Integer loginCount) {
        this.loginCount = loginCount;
    }

    @Column(name = "LoginMinutes")
    public Integer getLoginMinutes() {
        return loginMinutes;
    }

    public void setLoginMinutes(Integer loginMinutes) {
        this.loginMinutes = loginMinutes;
    }

    @Column(name = "ExtraUserInformation")
    public byte[] getExtraUserInformation() {
        return extraUserInformation;
    }

    public void setExtraUserInformation(byte[] extraUserInformation) {
        this.extraUserInformation = extraUserInformation;
    }
}
