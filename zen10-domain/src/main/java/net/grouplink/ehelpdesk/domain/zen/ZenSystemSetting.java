package net.grouplink.ehelpdesk.domain.zen;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "zSystemSetting")
public class ZenSystemSetting implements Serializable {
    private byte[] id;
    private byte[] objectUid;
    private String name;
    private Date modifiedTime;
    private String data;

    @Id
    @Column(name = "id")
    public byte[] getId() {
        return id;
    }

    public void setId(byte[] id) {
        this.id = id;
    }

    @Column(name = "ObjectUID")
    public byte[] getObjectUid() {
        return objectUid;
    }

    public void setObjectUid(byte[] objectUid) {
        this.objectUid = objectUid;
    }

    @Column(name = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ModifiedTime")
    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    @Column(name = "Data")
    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
