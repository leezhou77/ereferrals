package net.grouplink.ehelpdesk.domain.zen;

import org.apache.commons.codec.binary.Hex;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.FetchType;
import java.io.Serializable;

/**
 * User: jaymehafen
 * Date: Apr 1, 2009
 * Time: 2:32:00 PM
 */
@Entity
@Table(name = "zDevice")
public class Device implements Serializable {
    private byte[] zuid;
    private String platform;
    private String hostname;
    private String dns;
    private String osName;
    private Reference primaryUser;
    private String owner;
    private String site;
    private String department;
    private String location;
    private String serialNumber;

    @Id
    @Column(name = "ZUID")
    public byte[] getZuid() {
        return zuid;
    }

    public void setZuid(byte[] zuid) {
        this.zuid = zuid;
    }

    @Transient
    public String getZuidString() {
        return String.valueOf(Hex.encodeHex(zuid));
    }

    @Column(name = "Platform")
    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    @Column(name = "Hostname")
    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    @Column(name = "DNS")
    public String getDns() {
        return dns;
    }

    public void setDns(String dns) {
        this.dns = dns;
    }

    @Column(name = "OSName")
    public String getOsName() {
        return osName;
    }

    public void setOsName(String osName) {
        this.osName = osName;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PrimaryUser")
    public Reference getPrimaryUser() {
        return primaryUser;
    }

    public void setPrimaryUser(Reference primaryUser) {
        this.primaryUser = primaryUser;
    }

    @Column(name = "Owner")
    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Column(name = "Site")
    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    @Column(name = "Department")
    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    @Column(name = "Location")
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Column(name = "SerialNumber")
    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
}

