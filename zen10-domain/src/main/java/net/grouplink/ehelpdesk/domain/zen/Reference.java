package net.grouplink.ehelpdesk.domain.zen;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import java.io.Serializable;

/**
 * User: jaymehafen
 * Date: Apr 7, 2009
 * Time: 11:17:31 PM
 */
@Entity
@Table(name = "zReference")
public class Reference implements Serializable {
    private byte[] id;
//    private byte[] sourceUid;
    private String objectUid;

    @Id
    @Column(name = "ZUID")
    public byte[] getId() {
        return id;
    }

    public void setId(byte[] id) {
        this.id = id;
    }

    @Column(name = "ObjectUID")
    public String getObjectUid() {
        return objectUid;
    }

    public void setObjectUid(String objectUid) {
        this.objectUid = objectUid;
    }
}
