package net.grouplink.ehelpdesk.domain.zen;

import java.io.Serializable;

/**
 * User: jaymehafen
 * Date: Mar 31, 2009
 * Time: 6:35:52 PM
 */
public class ZenAssetSearch implements Serializable {
    private String assetName;
    private String assetType;
    private String operatingSystem;

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem;
    }
}
