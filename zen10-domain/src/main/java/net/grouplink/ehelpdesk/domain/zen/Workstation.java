package net.grouplink.ehelpdesk.domain.zen;

import org.apache.commons.codec.binary.Hex;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.FetchType;
import java.io.Serializable;

/**
 * User: jaymehafen
 * Date: Mar 31, 2009
 * Time: 7:23:17 PM
 */
@Entity
@Table(name = "NC_Workstation")
public class Workstation implements Serializable {
    private byte[] id;
    private User primaryUser;
    private String machineName;
    private String systemProduct;
    private String osProduct;
    private Device device;
    private String ipAddress;

    @Id
    @Column(name = "WorkstationOID")
    public byte[] getId() {
        return id;
    }

    public void setId(byte[] id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PrimaryUserOID")
    public User getPrimaryUser() {
        return primaryUser;
    }

    public void setPrimaryUser(User primaryUser) {
        this.primaryUser = primaryUser;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ZenWorksAgentId")
    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    @Column(name = "MachineName")
    public String getMachineName() {
        return machineName;
    }

    public void setMachineName(String machineName) {
        this.machineName = machineName;
    }

    @Column(name = "SystemProduct")
    public String getSystemProduct() {
        return systemProduct;
    }

    public void setSystemProduct(String systemProduct) {
        this.systemProduct = systemProduct;
    }

    @Column(name = "OSProduct")
    public String getOsProduct() {
        return osProduct;
    }

    public void setOsProduct(String osProduct) {
        this.osProduct = osProduct;
    }

    @Transient
    public String getIdString() {
        return String.valueOf(Hex.encodeHex(id));
    }

    @Column(name = "IPAddress")
    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
}
