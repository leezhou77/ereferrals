package net.grouplink.ehelpdesk.migration.dao.hibernate;

import net.grouplink.ehelpdesk.migration.dao.OldCwHdCdLinkDao;
import net.grouplink.ehelpdesk.migration.domain.CwHdCdLink;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.dao.support.DataAccessUtils;
import org.hibernate.Criteria;
import org.hibernate.CacheMode;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;

/**
 * Created by IntelliJ IDEA.
 * User: dgarcia
 * Date: Oct 30, 2007
 * Time: 2:26:54 PM
 */
public class OldCwHdCdLinkDaoImpl extends HibernateDaoSupport implements OldCwHdCdLinkDao {
    public ScrollableResults getAllCwHdCdLink() {
        Criteria cri = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(CwHdCdLink.class);
        cri.setCacheMode(CacheMode.IGNORE);
        return cri.scroll(ScrollMode.FORWARD_ONLY);
//        return getHibernateTemplate().find("select cd from CwHdCdLink as cd");
    }

    public int getAllCwHdCdLinkCount() {
        return DataAccessUtils.intResult(getHibernateTemplate().find("select count(*) from CwHdCdLink"));
    }
}
