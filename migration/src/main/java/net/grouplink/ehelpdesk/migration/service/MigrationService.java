package net.grouplink.ehelpdesk.migration.service;

public interface MigrationService {

	public void startMigration(String groupName, String addressDescription, String uploadFolder);
}
