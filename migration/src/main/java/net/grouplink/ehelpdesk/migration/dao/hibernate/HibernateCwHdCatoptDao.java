package net.grouplink.ehelpdesk.migration.dao.hibernate;

import java.util.List;

import net.grouplink.ehelpdesk.migration.dao.CwHdCatoptDao;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class HibernateCwHdCatoptDao extends HibernateDaoSupport implements
        CwHdCatoptDao {

	public List getCwHdCatopt() {
		return getHibernateTemplate().find("select co from CwHdCatopt as co");
	}

}
