package net.grouplink.ehelpdesk.migration.dao;

import org.hibernate.ScrollableResults;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: dgarcia
 * Date: Oct 30, 2007
 * Time: 2:26:28 PM
 */
public interface OldCwHdCdLinkDao {

    public ScrollableResults getAllCwHdCdLink();

    int getAllCwHdCdLinkCount();
}
