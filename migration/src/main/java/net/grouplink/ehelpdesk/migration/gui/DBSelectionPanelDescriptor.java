/*
 * DBSelectionPanelDescriptor.java
 *
 * Created on July 12, 2007, 11:49 AM
 *
 */

package net.grouplink.ehelpdesk.migration.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import net.grouplink.ehelpdesk.migration.gui.wizard.WizardPanelDescriptor;

/**
 *
 * @author mrollins
 */
public class DBSelectionPanelDescriptor extends WizardPanelDescriptor implements ActionListener {

    public static final String IDENTIFIER = "DB_SELECTION_PANEL";
    DBSelectionPanel dbPanel;

    public DBSelectionPanelDescriptor() {
        dbPanel = new DBSelectionPanel();

        // Put 'this' listener first so it is executed last
        ActionListener action0 = dbPanel.getTestButton().getActionListeners()[0];
        dbPanel.getTestButton().removeActionListener(action0);

        dbPanel.addTestButtonActionListener(this);
        dbPanel.addTestButtonActionListener(action0);

        setPanelDescriptorIdentifier(IDENTIFIER);
        setPanelComponent(dbPanel);
    }

    public Object getNextPanelDescriptor(){
        return MigratePanelDescriptor.IDENTIFIER;
    }

    public Object getBackPanelDescriptor() {
        return MigrationDBPanelDescriptor.IDENTIFIER;
    }

    public void aboutToDisplayPanel() {
        setNextButtonIfConnectionSuccessful();
    }

    public void actionPerformed(ActionEvent e) {
        setNextButtonIfConnectionSuccessful();
    }

    private void setNextButtonIfConnectionSuccessful() {
        if(dbPanel.getWizard() == null) dbPanel.setWizard(getWizard());
        getWizard().setNextFinishButtonEnabled(dbPanel.isConnectionSuccessful());
    }
}
