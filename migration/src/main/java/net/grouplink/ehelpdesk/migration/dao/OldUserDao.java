package net.grouplink.ehelpdesk.migration.dao;



import net.grouplink.ehelpdesk.migration.domain.CwHdUsersId;

import java.util.List;

public interface OldUserDao {

	public List getAdministrators();
    public List getTechnicians();
    public List getUsers();

    public CwHdUsersId getCwHdUserByName(String htiAssignedTo);

    void flush();
}
