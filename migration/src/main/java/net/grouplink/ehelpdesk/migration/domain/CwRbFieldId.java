package net.grouplink.ehelpdesk.migration.domain;

// Generated Jan 26, 2007 12:50:45 PM by Hibernate Tools 3.2.0.beta8

/**
 * CwRbFieldId generated by hbm2java
 */
public class CwRbFieldId implements java.io.Serializable {

	// Fields    

	private String feTablename;

	private String feFieldname;

	// Constructors

	/** default constructor */
	public CwRbFieldId() {
	}

	/** full constructor */
	public CwRbFieldId(String feTablename, String feFieldname) {
		this.feTablename = feTablename;
		this.feFieldname = feFieldname;
	}

	// Property accessors
	public String getFeTablename() {
		return this.feTablename;
	}

	public void setFeTablename(String feTablename) {
		this.feTablename = feTablename;
	}

	public String getFeFieldname() {
		return this.feFieldname;
	}

	public void setFeFieldname(String feFieldname) {
		this.feFieldname = feFieldname;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof CwRbFieldId))
			return false;
		CwRbFieldId castOther = (CwRbFieldId) other;

		return ((this.getFeTablename() == castOther.getFeTablename()) || (this
				.getFeTablename() != null
				&& castOther.getFeTablename() != null && this.getFeTablename()
				.equals(castOther.getFeTablename())))
				&& ((this.getFeFieldname() == castOther.getFeFieldname()) || (this
						.getFeFieldname() != null
						&& castOther.getFeFieldname() != null && this
						.getFeFieldname().equals(castOther.getFeFieldname())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getFeTablename() == null ? 0 : this.getFeTablename()
						.hashCode());
		result = 37
				* result
				+ (getFeFieldname() == null ? 0 : this.getFeFieldname()
						.hashCode());
		return result;
	}

}
