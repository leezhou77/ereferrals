package net.grouplink.ehelpdesk.migration.domain;

// Generated Jan 26, 2007 12:50:45 PM by Hibernate Tools 3.2.0.beta8

import java.util.Date;

/**
 * CwOpportunityHistory generated by hbm2java
 */
public class CwOpportunityHistory implements java.io.Serializable {

	// Fields    

	private int ohKey;

	private int ohOoKey;

	private int ohNoKey;

	private int ohType;

	private Date ohDate;

	private String ohSubject;

	private String ohCreator;

	private String ohMsginfo;

	private String ohControl;

	private Date ohTimestamp;

	// Constructors

	/** default constructor */
	public CwOpportunityHistory() {
	}

	/** minimal constructor */
	public CwOpportunityHistory(int ohKey, int ohOoKey, int ohNoKey,
			int ohType, Date ohDate, String ohControl) {
		this.ohKey = ohKey;
		this.ohOoKey = ohOoKey;
		this.ohNoKey = ohNoKey;
		this.ohType = ohType;
		this.ohDate = ohDate;
		this.ohControl = ohControl;
	}

	/** full constructor */
	public CwOpportunityHistory(int ohKey, int ohOoKey, int ohNoKey,
			int ohType, Date ohDate, String ohSubject, String ohCreator,
			String ohMsginfo, String ohControl, Date ohTimestamp) {
		this.ohKey = ohKey;
		this.ohOoKey = ohOoKey;
		this.ohNoKey = ohNoKey;
		this.ohType = ohType;
		this.ohDate = ohDate;
		this.ohSubject = ohSubject;
		this.ohCreator = ohCreator;
		this.ohMsginfo = ohMsginfo;
		this.ohControl = ohControl;
		this.ohTimestamp = ohTimestamp;
	}

	// Property accessors
	public int getOhKey() {
		return this.ohKey;
	}

	public void setOhKey(int ohKey) {
		this.ohKey = ohKey;
	}

	public int getOhOoKey() {
		return this.ohOoKey;
	}

	public void setOhOoKey(int ohOoKey) {
		this.ohOoKey = ohOoKey;
	}

	public int getOhNoKey() {
		return this.ohNoKey;
	}

	public void setOhNoKey(int ohNoKey) {
		this.ohNoKey = ohNoKey;
	}

	public int getOhType() {
		return this.ohType;
	}

	public void setOhType(int ohType) {
		this.ohType = ohType;
	}

	public Date getOhDate() {
		return this.ohDate;
	}

	public void setOhDate(Date ohDate) {
		this.ohDate = ohDate;
	}

	public String getOhSubject() {
		return this.ohSubject;
	}

	public void setOhSubject(String ohSubject) {
		this.ohSubject = ohSubject;
	}

	public String getOhCreator() {
		return this.ohCreator;
	}

	public void setOhCreator(String ohCreator) {
		this.ohCreator = ohCreator;
	}

	public String getOhMsginfo() {
		return this.ohMsginfo;
	}

	public void setOhMsginfo(String ohMsginfo) {
		this.ohMsginfo = ohMsginfo;
	}

	public String getOhControl() {
		return this.ohControl;
	}

	public void setOhControl(String ohControl) {
		this.ohControl = ohControl;
	}

	public Date getOhTimestamp() {
		return this.ohTimestamp;
	}

	public void setOhTimestamp(Date ohTimestamp) {
		this.ohTimestamp = ohTimestamp;
	}

}
