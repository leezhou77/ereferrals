package net.grouplink.ehelpdesk.migration.dao;

import org.hibernate.ScrollableResults;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: dgarcia
 * Date: Oct 23, 2007
 * Time: 4:23:17 PM
 */
public interface OldCwKbDao {

    public ScrollableResults getCwKb();

    int getCwKbCount();
}
