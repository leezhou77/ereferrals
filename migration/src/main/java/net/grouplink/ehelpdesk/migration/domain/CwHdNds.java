package net.grouplink.ehelpdesk.migration.domain;

// Generated Jan 26, 2007 12:50:45 PM by Hibernate Tools 3.2.0.beta8

/**
 * CwHdNds generated by hbm2java
 */
public class CwHdNds implements java.io.Serializable {

	// Fields    

	private Integer hdsKey;

	private CwHd hdsHtiKey;

	private String hdsDn;

	private String hdsWorkstation;

	private String hdsLoggedinworkstation;

	private String hdsWsdn;

	private String hdsIpaddress;

	private String hdsSubnetaddress;

	private String hdsMacaddress;

	private String hdsDiskinfo;

	private String hdsMemorysize;

	private String hdsProcessortype;

	private String hdsBiostype;

	private String hdsVideotype;

	private String hdsNictype;

	private String hdsNcv;

	private String hdsOsrevision;

	private String hdsOstype;

	private String hdsAssettag;

	private String hdsSerialnumber;

	private String hdsModelnumber;

	private String hdsComputermodel;

	private String hdsComputertype;

	private String hdsUserhistory;

	private String hdsLastuser;

	private String hdsOperator;

	private String hdsComputerName;



	// Property accessors
	public Integer getHdsKey() {
		return this.hdsKey;
	}

	public void setHdsKey(Integer hdsKey) {
		this.hdsKey = hdsKey;
	}

    public CwHd getHdsHtiKey() {
        return hdsHtiKey;
    }

    public void setHdsHtiKey(CwHd hdsHtiKey) {
        this.hdsHtiKey = hdsHtiKey;
    }

    public String getHdsDn() {
		return this.hdsDn;
	}

	public void setHdsDn(String hdsDn) {
		this.hdsDn = hdsDn;
	}

	public String getHdsWorkstation() {
		return this.hdsWorkstation;
	}

	public void setHdsWorkstation(String hdsWorkstation) {
		this.hdsWorkstation = hdsWorkstation;
	}

	public String getHdsLoggedinworkstation() {
		return this.hdsLoggedinworkstation;
	}

	public void setHdsLoggedinworkstation(String hdsLoggedinworkstation) {
		this.hdsLoggedinworkstation = hdsLoggedinworkstation;
	}

	public String getHdsWsdn() {
		return this.hdsWsdn;
	}

	public void setHdsWsdn(String hdsWsdn) {
		this.hdsWsdn = hdsWsdn;
	}

	public String getHdsIpaddress() {
		return this.hdsIpaddress;
	}

	public void setHdsIpaddress(String hdsIpaddress) {
		this.hdsIpaddress = hdsIpaddress;
	}

	public String getHdsSubnetaddress() {
		return this.hdsSubnetaddress;
	}

	public void setHdsSubnetaddress(String hdsSubnetaddress) {
		this.hdsSubnetaddress = hdsSubnetaddress;
	}

	public String getHdsMacaddress() {
		return this.hdsMacaddress;
	}

	public void setHdsMacaddress(String hdsMacaddress) {
		this.hdsMacaddress = hdsMacaddress;
	}

	public String getHdsDiskinfo() {
		return this.hdsDiskinfo;
	}

	public void setHdsDiskinfo(String hdsDiskinfo) {
		this.hdsDiskinfo = hdsDiskinfo;
	}

	public String getHdsMemorysize() {
		return this.hdsMemorysize;
	}

	public void setHdsMemorysize(String hdsMemorysize) {
		this.hdsMemorysize = hdsMemorysize;
	}

	public String getHdsProcessortype() {
		return this.hdsProcessortype;
	}

	public void setHdsProcessortype(String hdsProcessortype) {
		this.hdsProcessortype = hdsProcessortype;
	}

	public String getHdsBiostype() {
		return this.hdsBiostype;
	}

	public void setHdsBiostype(String hdsBiostype) {
		this.hdsBiostype = hdsBiostype;
	}

	public String getHdsVideotype() {
		return this.hdsVideotype;
	}

	public void setHdsVideotype(String hdsVideotype) {
		this.hdsVideotype = hdsVideotype;
	}

	public String getHdsNictype() {
		return this.hdsNictype;
	}

	public void setHdsNictype(String hdsNictype) {
		this.hdsNictype = hdsNictype;
	}

	public String getHdsNcv() {
		return this.hdsNcv;
	}

	public void setHdsNcv(String hdsNcv) {
		this.hdsNcv = hdsNcv;
	}

	public String getHdsOsrevision() {
		return this.hdsOsrevision;
	}

	public void setHdsOsrevision(String hdsOsrevision) {
		this.hdsOsrevision = hdsOsrevision;
	}

	public String getHdsOstype() {
		return this.hdsOstype;
	}

	public void setHdsOstype(String hdsOstype) {
		this.hdsOstype = hdsOstype;
	}

	public String getHdsAssettag() {
		return this.hdsAssettag;
	}

	public void setHdsAssettag(String hdsAssettag) {
		this.hdsAssettag = hdsAssettag;
	}

	public String getHdsSerialnumber() {
		return this.hdsSerialnumber;
	}

	public void setHdsSerialnumber(String hdsSerialnumber) {
		this.hdsSerialnumber = hdsSerialnumber;
	}

	public String getHdsModelnumber() {
		return this.hdsModelnumber;
	}

	public void setHdsModelnumber(String hdsModelnumber) {
		this.hdsModelnumber = hdsModelnumber;
	}

	public String getHdsComputermodel() {
		return this.hdsComputermodel;
	}

	public void setHdsComputermodel(String hdsComputermodel) {
		this.hdsComputermodel = hdsComputermodel;
	}

	public String getHdsComputertype() {
		return this.hdsComputertype;
	}

	public void setHdsComputertype(String hdsComputertype) {
		this.hdsComputertype = hdsComputertype;
	}

	public String getHdsUserhistory() {
		return this.hdsUserhistory;
	}

	public void setHdsUserhistory(String hdsUserhistory) {
		this.hdsUserhistory = hdsUserhistory;
	}

	public String getHdsLastuser() {
		return this.hdsLastuser;
	}

	public void setHdsLastuser(String hdsLastuser) {
		this.hdsLastuser = hdsLastuser;
	}

	public String getHdsOperator() {
		return this.hdsOperator;
	}

	public void setHdsOperator(String hdsOperator) {
		this.hdsOperator = hdsOperator;
	}

	public String getHdsComputerName() {
		return this.hdsComputerName;
	}

	public void setHdsComputerName(String hdsComputerName) {
		this.hdsComputerName = hdsComputerName;
	}

}
