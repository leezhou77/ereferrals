package net.grouplink.ehelpdesk.migration.domain;

// Generated Jan 26, 2007 12:50:45 PM by Hibernate Tools 3.2.0.beta8

/**
 * CwHdCatopt generated by hbm2java
 */
public class CwHdCatopt implements java.io.Serializable {

	// Fields    

	private Integer hcoKey;

	private String hcoName;

	private Integer hcoHcaKey;

	private Integer hcoDispatcher;

	private String hcoComments;

	private Boolean hcoSurvey;

    private Integer newCatOpt;


    public Integer getNewCatOpt() {
        return newCatOpt;
    }

    public void setNewCatOpt(Integer newCatOpt) {
        this.newCatOpt = newCatOpt;
    }

    public Integer getHcoKey() {
		return this.hcoKey;
	}

	public void setHcoKey(Integer hcoKey) {
		this.hcoKey = hcoKey;
	}

	public String getHcoName() {
		return this.hcoName;
	}

	public void setHcoName(String hcoName) {
		this.hcoName = hcoName;
	}

	public Integer getHcoHcaKey() {
		return this.hcoHcaKey;
	}

	public void setHcoHcaKey(Integer hcoHcaKey) {
		this.hcoHcaKey = hcoHcaKey;
	}

	public Integer getHcoDispatcher() {
		return this.hcoDispatcher;
	}

	public void setHcoDispatcher(Integer hcoDispatcher) {
		this.hcoDispatcher = hcoDispatcher;
	}

	public String getHcoComments() {
		return this.hcoComments;
	}

	public void setHcoComments(String hcoComments) {
		this.hcoComments = hcoComments;
	}
}
