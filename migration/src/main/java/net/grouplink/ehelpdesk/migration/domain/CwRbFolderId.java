package net.grouplink.ehelpdesk.migration.domain;

// Generated Jan 26, 2007 12:50:45 PM by Hibernate Tools 3.2.0.beta8

/**
 * CwRbFolderId generated by hbm2java
 */
public class CwRbFolderId implements java.io.Serializable {

	// Fields    

	private int foFolderid;

	private String foFoldername;

	// Constructors

	/** default constructor */
	public CwRbFolderId() {
	}

	/** full constructor */
	public CwRbFolderId(int foFolderid, String foFoldername) {
		this.foFolderid = foFolderid;
		this.foFoldername = foFoldername;
	}

	// Property accessors
	public int getFoFolderid() {
		return this.foFolderid;
	}

	public void setFoFolderid(int foFolderid) {
		this.foFolderid = foFolderid;
	}

	public String getFoFoldername() {
		return this.foFoldername;
	}

	public void setFoFoldername(String foFoldername) {
		this.foFoldername = foFoldername;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof CwRbFolderId))
			return false;
		CwRbFolderId castOther = (CwRbFolderId) other;

		return (this.getFoFolderid() == castOther.getFoFolderid())
				&& ((this.getFoFoldername() == castOther.getFoFoldername()) || (this
						.getFoFoldername() != null
						&& castOther.getFoFoldername() != null && this
						.getFoFoldername().equals(castOther.getFoFoldername())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getFoFolderid();
		result = 37
				* result
				+ (getFoFoldername() == null ? 0 : this.getFoFoldername()
						.hashCode());
		return result;
	}

}
