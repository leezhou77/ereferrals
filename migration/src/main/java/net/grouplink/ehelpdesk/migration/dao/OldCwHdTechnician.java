package net.grouplink.ehelpdesk.migration.dao;

import net.grouplink.ehelpdesk.migration.domain.CwHdTechnician;

import java.util.List;

import org.hibernate.ScrollableResults;

/**
 * Created by IntelliJ IDEA.
 * User: dgarcia
 * Date: Oct 4, 2007
 * Time: 12:13:42 AM
 */
public interface OldCwHdTechnician {

    public CwHdTechnician getCwHdTechnicianByEmail(String email);

    public CwHdTechnician getCwHdTechnicianByName(String name);

    public ScrollableResults getAllTechs();

    int getAllTechCount();

    void flushAndReset();
}
