/*
 * MigrationDBPanelDescriptor.java
 * 
 * Created on Nov 13, 2007, 11:48:22 PM
 * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.grouplink.ehelpdesk.migration.gui;

import java.awt.event.*;
import net.grouplink.ehelpdesk.migration.gui.wizard.WizardPanelDescriptor;

/**
 *
 * @author mrollins
 */
public class MigrationDBPanelDescriptor extends WizardPanelDescriptor implements ActionListener {
    
    public static final String IDENTIFIER = "MIGRATION_DB_PANEL";
    MigrationDBPanel mdbPanel;
    
    public MigrationDBPanelDescriptor(){
        mdbPanel = new MigrationDBPanel();
        
        // Put 'this' listener first so it is executed last.
        ActionListener action0 = mdbPanel.getTestButton().getActionListeners()[0];
        mdbPanel.getTestButton().removeActionListener(action0);
        
        mdbPanel.addTestButtonActionListener(this);
        mdbPanel.addTestButtonActionListener(action0);
        
        setPanelDescriptorIdentifier(IDENTIFIER);
        setPanelComponent(mdbPanel);
    }
    
    public Object getNextPanelDescriptor(){
        return DBSelectionPanelDescriptor.IDENTIFIER;
    }

    public Object getBackPanelDescriptor() {
        return null;
    }

    public void aboutToDisplayPanel() {
        if(mdbPanel.getWizard() == null) mdbPanel.setWizard(getWizard());
    }

    public void actionPerformed(ActionEvent e) {
        setNextButtonIfConnectionSuccessful();
    }

    public void setNextButtonIfConnectionSuccessful() {        
        getWizard().setNextFinishButtonEnabled(mdbPanel.isConnectionSuccessful());
    }
}
