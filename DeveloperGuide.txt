eHelpDesk Developer Guide

-------------------------


Getting the source
------------------
GroupLink's git repository uses keypairs for authentication. To generate a keypair,
run this command:

ssh-keygen -t rsa

It is recommended to put a passphrase on your key, but it is not required. After creating
the keypair, copy the id_rsa.pub file to a different location and rename it using your first
initial and last name (ex. zpearce.pub) and send it to the gitolite admin for installation
on the repository server.

Next, clone the repo to your machine using this command:

git clone git@dev1:ehelpdesk

This will copy the entire repository and all of its history to your machine. After doing
this, immediately checkout the develop branch (we don't do any work on the master branch)
using this command:

git checkout develop

A git cheat sheet is included later in this file.


Building
--------
Maven 3 is required to build the eHelpDesk project.  If you don't already have
Maven installed, you can download it from http://maven.apache.org. For more
instructions on setting up Maven 3, see the section "Setting up Maven 3".

Assuming you have a working copy of Maven 3, open a shell and change directory
to where the eHelpDesk project was checked out. From here, run:

mvn install


This will build the project, including all sub-modules, and install the
artifacts into the local Maven repository.  If you just want to compile the
code without instaling to the local repository, you could run:

mvn compile


To clean up a previous build, you run the 'clean' goal:

mvn clean


Multiple goals can be run at the same time:

mvn clean install

You may need to pass system properties to Maven to do things like disable unit
tests, or provide a target environment to build for. Examples:

mvn clean install -Dmaven.test.skip=true
(performs a clean, then install, skipping unit tests)

mvn clean install -Dmaven.test.skip=true -Denv=dist
(performs a clean, then install, skipping unit tests, and targeting the "dist" environment)

The goals provided can be either a pre-defined Maven lifecycle or a
Maven plugin.


Generating IDE project files
----------------------------
IntelliJ 8.x and later can open pom.xml files directly, and will create the IntelliJ project files automatically.

For pre IntelliJ 8.x, you can use the following:
To generate IntelliJ IDEA project files, run:

mvn idea:idea


For Eclipse, run:

mvn eclipse:eclipse


For Netbeans:

mvn netbeans-freeform:generate-netbeans-project

Note: Netbeans projects can only be generated at the module level, not the
entire project.


Maven Generated Documentation
-----------------------------
Maven generates project documentation using the 'site' plugin.  The current
site documentation for eHelpDesk is located at

http://dev1.grouplink.net/maven/site/ehelpdesk


Generating a Release
--------------------
Releases can be generated from a release- or a hotfix- branch. To generate a
release follow this procedure (replace <branchname> with the name of the git
branch and <version> with the version you are releasing, i.e. 9.3.2):

Ensure you have the latest changes
0) git pull

Update versions
1) git checkout <branchname>
2) mvn release:update-versions -DpushChanges=false
3) Answer each question with: <version>
4) git commit -a -m"Bumped version to <version>"

Merge branch into master
5) git checkout master
6) git merge --no-ff <branchname>
7) git tag -a release-<version>

Merge branch into develop
8) git checkout develop
9) git merge --no-ff <branchname>
10) Fix conflicts in pom files (there may be conflicts in other files as well. In
general you'll want to keep HEAD's changes in the poms and the branch's changes
in any other file. Ask if you're not sure.)
11) git commit -a -m"Merging <branchname>"

Push changes, including tags
10) git push
11) git push --tags

Package the release
12) git checkout master
13) run install4j plugin

Remove the branch
14) git branch -d <branchname>


Install4j Plugin
------------------
The Install4j plugin is used to build installer distributables. These currently come in two forms:
the full installer and the demo installer. Install4j must be installed on your machine to run this plugin.
JRE bundles must be installed. Go to smb://gwshare/development/eHD/jres and copy all of the tarballs
from there to your install4j install folder in the jres folder.

Running the following commands produces executable installers for each of the supported platforms in the
base project target directory. If you don't want to type a value for devenv.install4jc each time,
you can set the property in your ~/.m2/settings.xml file. The default path for install4jc v4 on Mac OSX is
/Applications/install4j\ 5/bin/install4jc

To build the full installer:
mvn clean install install4j:compile -Denv=dist -Dmaven.test.skip=true -Ddevenv.install4jc=<path to your install4j executable>

To build the demo installer:
mvn clean install install4j:compile -Denv=dist -Dmaven.test.skip=true -DinstallerType=demo -Ddevenv.install4jc=<path to your install4j executable>


Setting up Maven 3
------------------
Download the latest version of Maven 3 from http://maven.apache.org. Extract the
archive to a location on your machine, and set up an environment variable named
'MAVEN_HOME' to point to this location.  Add %MAVEN_HOME%\bin to your PATH.
Maven can be customized on a per-user basis by modifying the file
%USERDIR%\.m2\settings.xml. If you are deploying artifacts to a remote
repository, you will have to set up the <server> configuration in the settings
file.


Maven Repository
----------------
An internal GroupLink Maven repository is located at
http://dev1.grouplink.net:8081/artifactory (login is admin:admin). Artifactory
acts as the central repository for all GroupLink artifacts, as well as third
party artifacts. Artifactory will connect to other Maven repositories and cache
artifacts to increase performance and reduce bandwidth for Maven clients.


TeamCity
--------
TeamCity takes care of running the automated builds and notifying developers
when the build breaks.  The TeamCity server is located at
http://dev1.grouplink.net:8111


Fisheye + Crucible
------------------
Fisheye allows you to see all git activity by user, branch, etc. It
also allows you to view differences between versions. It can be accessed here:

http://dev1.grouplink.net:8060

Integrated with Fisheye is Crucible, which is a code review tool. You can
specify one or more revisions you would like to create a review for, choose
the reviewers, and Crucible will notify those users that there is a new review
requiring their attention. The reviewers then make comments and mark defects
which will notify the creator of the review. When the reviewer is done, they
mark the review as completed, then the review creator can summarize and close
the review.


Git Cheatsheet
----------
Most git operations can be done in your preferred IDE.  However, on
occasion you might want to use git on the command line.  Some examples
are:

Cloning the repo
git clone git@dev1:ehelpdesk

Checking out a branch
git checkout develop                        #checks out the branch named "develop"

Committing your work
git commit -a -m"Your commit message here"  #the -a flag includes any unstaged changes in this commit

Pushing to develop (after you've committed to your
    local repo and you're ready to check in your changes)
git push origin develop

Creating a local branch (to work on a bug or a new feature)
git checkout -b my-new-branch

Merging a local branch (once you're finished with your bug or feature)
git checkout develop
git merge --no-ff my-new-branch
git branch -d my-new-branch                 #deletes the bug/feature branch
git push origin develop                     #don't forget to push your changes to develop

See http://www.kernel.org/pub/software/scm/git/docs/gittutorial.html for a simple
git tutorial.


