package net.grouplink.ehelpdesk.dao.asset;

import net.grouplink.ehelpdesk.domain.asset.ZenAsset;
import net.grouplink.ehelpdesk.domain.zen.ZenAssetSearch;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Apr 5, 2009
 * Time: 10:53:08 PM
 */
public interface ZenAssetDao {

    ZenAsset getById(Integer id);

    void save(ZenAsset zenAsset);

    List<ZenAsset> getAll();

    void delete(ZenAsset zenAsset);

    ZenAsset getByWorkstationId(byte[] workstationId);

    List<ZenAsset> search(ZenAssetSearch zenAssetSearch);
}
