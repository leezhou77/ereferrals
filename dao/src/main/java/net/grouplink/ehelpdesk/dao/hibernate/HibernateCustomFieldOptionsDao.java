package net.grouplink.ehelpdesk.dao.hibernate;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import net.grouplink.ehelpdesk.dao.CustomFieldOptionsDao;
import net.grouplink.ehelpdesk.domain.CustomFieldOption;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: mabbasi
 * Date: May 5, 2007
 * Time: 4:05:21 AM
 */
public class HibernateCustomFieldOptionsDao extends HibernateDaoSupport implements CustomFieldOptionsDao {


    public void saveCustomFieldOption(CustomFieldOption customFieldOption) {
        getHibernateTemplate().saveOrUpdate(customFieldOption);
    }

    /**
     * Saves a list of customFieldOptions objects and all their attributes to the database.
     * @param customFieldOptions list
     */
    public void saveCustomFieldOptions(List<CustomFieldOption> customFieldOptions) {

        for (CustomFieldOption customFieldOption : customFieldOptions) {
            getHibernateTemplate().saveOrUpdate(customFieldOption);
        }
    }

    /**
     * Gets a list of custom field option pojos for a specifc custom field id.
     * @param id The custom field ID
     * @return Custom field option pojo
     */
    @SuppressWarnings("unchecked")
    public List<CustomFieldOption> getCustomFieldOptions(Integer id) {
        return getHibernateTemplate().findByNamedParam("select cfo from CustomFieldOption" +
                " cfo where cfo.customField.id=:CUTSOMFIELDID", "CUTSOMFIELDID", id);
    }

    /**
     * Gets a custom field option pojo for a specifc custom field id.
     * @param id The custom field option ID
     * @return Custom field option pojo
     */
    public CustomFieldOption getCustomFieldOptionByOptionId(Integer id) {
        List cfoList = getHibernateTemplate().findByNamedParam("select cfo from CustomFieldOption" +
        " cfo where cfo.id=:CUSTOMFIELDOPTIONID", "CUSTOMFIELDOPTIONID", id);
        return (CustomFieldOption) cfoList.get(0);
    }

    /**
     * Gets the custom field option pojo for a given id.
     * @param id The custom field option ID
     * @return CustomFieldOption pojo
     */
    public CustomFieldOption getCustomFieldOptionsById(Integer id) {
        List customFieldOption = getHibernateTemplate().findByNamedParam("select cfo from CustomFieldOption " +
                "cfo where cfo.id=:ID", "ID", id);

        if (customFieldOption.size() == 1)
            return (CustomFieldOption) customFieldOption.get(0);

        return null;
    }

    /**
     * Deletes a list of customFieldOptions from the database.
     * @param customFieldOptions The list of CustomFieldOption
     */
    public void deleteCustomFieldOptions(List<CustomFieldOption> customFieldOptions) {
        for (CustomFieldOption customFieldOption : customFieldOptions) {
            getHibernateTemplate().delete(customFieldOption);
        }
    }

    /**
     * Deletes a customFieldOption item from the database.
     * @param customFieldOption The CustomFieldOption object
     */
    public void deleteCustomFieldOption(CustomFieldOption customFieldOption) {
        getHibernateTemplate().delete(customFieldOption);
    }
}
