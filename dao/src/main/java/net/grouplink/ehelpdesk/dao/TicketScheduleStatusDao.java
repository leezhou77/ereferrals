package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.TicketScheduleStatus;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.ScheduleStatus;

import java.util.List;

/**
 * @author mrollins
 * @version 1.0
 */
public interface TicketScheduleStatusDao {
    public List<TicketScheduleStatus> getTicketScheduleStatus();
    public TicketScheduleStatus getTSSById(Integer id);
    public TicketScheduleStatus getTSSByTicket(Ticket ticket, ScheduleStatus scheduleStatus);
    public void saveTSS(TicketScheduleStatus ticketScheduleStatus);
    public void deleteTSS(TicketScheduleStatus ticketScheduleStatus);

    void flush();
}
