package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.CustomFieldTypesDao;
import net.grouplink.ehelpdesk.domain.CustomFieldType;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: mabbasi
 * Date: May 5, 2007
 * Time: 3:27:19 AM
 */
public class HibernateCustomFieldTypesDao extends HibernateDaoSupport implements CustomFieldTypesDao {

    /**
     * Gets a list of all custom field types from the database.
     * @return list of custom field types.
     */
    @SuppressWarnings("unchecked")
    public List<CustomFieldType> getCustomFieldTypes() {
        return getHibernateTemplate().find("select cft from CustomFieldType as cft order by cft.id");
    }

    /**
     * Gets a custom field type from its id.
     * @param id The custom field type id
     * @return custom field type object
     */
    public CustomFieldType getCustomFieldType(Integer id) {
        List custFieldTypes = getHibernateTemplate().findByNamedParam("select cft from CustomFieldType as cft where cft.id=:id", "id", id);

        if (!custFieldTypes.isEmpty())
            return (CustomFieldType) custFieldTypes.get(0);

        return null;
    }
}
