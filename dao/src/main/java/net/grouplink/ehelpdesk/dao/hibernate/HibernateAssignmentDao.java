package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.AssignmentDao;
import net.grouplink.ehelpdesk.domain.Assignment;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.domain.filter.AssignmentFilter;
import org.hibernate.Criteria;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class HibernateAssignmentDao extends HibernateDaoSupport implements AssignmentDao {

    @SuppressWarnings("unchecked")
    public Assignment getById(Integer id) {
        List<Assignment> techCat = (List<Assignment>) getHibernateTemplate().findByNamedParam("select tlco from Assignment as tlco where tlco.id=:id", "id", id);
        if (techCat.size() > 0) {
            return techCat.get(0);
        }
        return null;
    }

    public void save(Assignment assignment) {
        if (null != assignment.getUserRoleGroup()) {
            getHibernateTemplate().saveOrUpdate(assignment);
        }
    }

    public void delete(Assignment assignment) {
        getHibernateTemplate().delete(assignment);
    }

    @SuppressWarnings("unchecked")
    public List<Group> getGroupListByLocationId(Integer id) {
        return (List<Group>) getHibernateTemplate().findByNamedParam("select distinct tlco.categoryOption.category.group from Assignment as tlco where tlco.location.id=:id", "id", id);
    }

    @SuppressWarnings("unchecked")
    public List<Category> getCategoryListByLocationIdAndGroupId(Integer locationId, Integer groupId) {
        return (List<Category>) getHibernateTemplate().findByNamedParam("select distinct tlco.categoryOption.category from Assignment as tlco where tlco.location.id = :locationId and tlco.categoryOption.category.group.id = :groupId", new String[]{"locationId", "groupId"}, new Object[]{locationId, groupId});
    }

    @SuppressWarnings("unchecked")
    public List<Category> getCategoryListAssignedToUserByUrgId(Integer urgId) {
        List<Assignment> list = (List<Assignment>) getHibernateTemplate().findByNamedParam("select distinct tlco from Assignment as tlco where tlco.userRoleGroup.id = :urgId", "urgId", urgId);
        Set<Category> catSet = new HashSet<Category>();
        for (Assignment aList : list) {
            catSet.add(aList.getCategoryOption().getCategory());
        }
        return new ArrayList<Category>(catSet);
    }

    @SuppressWarnings("unchecked")
    public List<CategoryOption> getCategoryOptionListAssignedToUserByUrgIdAndCategoryId(Integer urgId, Integer categoryId) {
        return (List<CategoryOption>) getHibernateTemplate().findByNamedParam("select distinct tlco.categoryOption from Assignment as tlco where tlco.userRoleGroup.id = :urgId and tlco.categoryOption.category.id = :categoryId", new String[]{"urgId", "categoryId"}, new Object[]{urgId, categoryId});
    }

    public void flush() {
        getHibernateTemplate().flush();
    }

    public void clear() {
        getHibernateTemplate().clear();
    }

    @SuppressWarnings("unchecked")
    public List<Assignment> getByCatOptAndLocation(CategoryOption categoryOption, Location location) {
        return (List<Assignment>) getHibernateTemplate().findByNamedParam("from Assignment a where a.categoryOption = :catOpt and a.location = :loc",
                new String[]{"catOpt", "loc"}, new Object[]{categoryOption, location});
    }

    @SuppressWarnings("unchecked")
    public List<Assignment> getByCatOpt(CategoryOption categoryOption) {
        return (List<Assignment>) getHibernateTemplate().findByNamedParam("from Assignment a where a.categoryOption = :catOpt and a.location is null", "catOpt", categoryOption);
    }

    @SuppressWarnings("unchecked")
    public List<Assignment> getByCatAndLoc(Category category, Location location) {
        return (List<Assignment>) getHibernateTemplate().findByNamedParam("from Assignment a where a.category = :cat and a.location = :loc",
                new String[]{"cat", "loc"}, new Object[]{category, location});
    }

    @SuppressWarnings("unchecked")
    public List<Assignment> getByCat(Category category) {
        return (List<Assignment>) getHibernateTemplate().findByNamedParam("from Assignment a where a.category = :cat and a.location is null", "cat", category);
    }

    @SuppressWarnings("unchecked")
    public List<Assignment> getByGroupAndLoc(Group group, Location location) {
        return (List<Assignment>) getHibernateTemplate().findByNamedParam("from Assignment a where a.group = :group and a.location = :loc",
                new String[]{"group", "loc"}, new Object[]{group, location});
    }

    @SuppressWarnings("unchecked")
    public List<Assignment> getByGroup(Group group) {
        return (List<Assignment>) getHibernateTemplate().findByNamedParam("from Assignment a where a.group = :group and a.location is null", "group", group);
    }

    public void deleteList(List techLocCatOptList) {
        getHibernateTemplate().deleteAll(techLocCatOptList);
    }

    public int getCountByCategoryOption(CategoryOption categoryOption) {
        return DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam(
                "select count(*) from Assignment as tlco where tlco.categoryOption = :categoryOption", "categoryOption", categoryOption));

    }

    @SuppressWarnings("unchecked")
    public List<Assignment> getAssignments(AssignmentFilter filter, String queryParam, Integer start, Integer count, Map<String, Boolean> sortFieldMap) {
        DetachedCriteria dc = getAssignmentQueryCriteria(filter, queryParam, sortFieldMap);
        return (List<Assignment>) getHibernateTemplate().findByCriteria(dc, start, count);
    }

    private DetachedCriteria getAssignmentQueryCriteria(AssignmentFilter filter, String queryParam, Map<String, Boolean> sortFieldMap) {
        DetachedCriteria dc = DetachedCriteria.forClass(Assignment.class);
        dc.createAlias("category", "category", Criteria.LEFT_JOIN);

        Conjunction conjunction = Restrictions.conjunction();

        if (!filter.getUserRoleGroupList().isEmpty()) {
            Disjunction equalsDisj = Restrictions.disjunction();
            Criterion criterion;
            for (UserRoleGroup urg : filter.getUserRoleGroupList()) {
                criterion = Restrictions.eq("userRoleGroup", urg);
                equalsDisj.add(criterion);
            }
            conjunction.add(equalsDisj);
        }

        if (filter.getLocation() != null) {
            conjunction.add(Restrictions.eq("location", filter.getLocation()));
        }

        if (filter.getCategoryOption() != null) {
            conjunction.add(Restrictions.eq("categoryOption", filter.getCategoryOption()));
        } else if (filter.getCategory() != null) {
            conjunction.add((Restrictions.eq("category", filter.getCategory())));
        } else if (filter.getGroup() != null) {
            conjunction.add(Restrictions.eq("group", filter.getGroup()));
        }

//        if(StringUtils.isBlank(queryParam)){
//            // no restrictions to add
//        } else {
//            dc.createAlias("userRoleGroup", "userRoleGroup", Criteria.LEFT_JOIN);
//            dc.createAlias("userRoleGroup.userRole", "userRole", Criteria.LEFT_JOIN);
//            dc.createAlias("userRole.user", "user", Criteria.LEFT_JOIN);
//            dc.createAlias("location", "location", Criteria.LEFT_JOIN);
//            dc.createAlias("group", "group", Criteria.LEFT_JOIN);
//            dc.createAlias("category", "category", Criteria.LEFT_JOIN);
//            dc.createAlias("categoryOption", "categoryOption", Criteria.LEFT_JOIN);
//
//            // split on "," for the case when the queryParam comes in as Lastname, Firstname
//            // and change the hql appropriately
//            String[] splitParam = queryParam.split(",");
//            if (splitParam.length == 2){
//                // match the lastname and the firstname
//                dc.add(Restrictions.ilike("user.lastName", splitParam[0].trim(), MatchMode.START));
//                dc.add(Restrictions.ilike("user.firstName", splitParam[1].trim(), MatchMode.START));
//            } else {
//                Disjunction d = Restrictions.disjunction();
//                d.add(Restrictions.ilike("user.lastName", queryParam, MatchMode.START));
//                d.add(Restrictions.ilike("user.firstName", queryParam, MatchMode.START));
//                d.add(Restrictions.ilike("user.loginId", queryParam, MatchMode.ANYWHERE));
//                d.add(Restrictions.ilike("location.name", queryParam, MatchMode.START));
//                d.add(Restrictions.ilike("group.name", queryParam, MatchMode.START));
//                d.add(Restrictions.ilike("category.name", queryParam, MatchMode.START));
//                d.add(Restrictions.ilike("categoryOption.name", queryParam, MatchMode.START));
//                dc.add(d);
//            }
//        }

        dc.add(conjunction);

        for (String sortField : sortFieldMap.keySet()) {
            if (sortFieldMap.get(sortField)) {
                dc.addOrder(Order.asc(sortField));
            } else {
                dc.addOrder(Order.desc(sortField));
            }
        }

        dc.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return dc;
    }

    @SuppressWarnings("unchecked")
    public List<Assignment> getByUrgLocGroup(UserRoleGroup urg, Location location, Group group) {
        List<Assignment> assignments = new ArrayList<Assignment>();
        if (location == null) {
            assignments.addAll(getHibernateTemplate().findByNamedParam("from Assignment as tlco where tlco.userRoleGroup =:urg and tlco.group =:group", new String[]{"urg", "group"}, new Object[]{urg, group}));
            assignments.addAll(getHibernateTemplate().findByNamedParam("from Assignment as tlco where tlco.userRoleGroup =:urg and tlco.category.group =:group", new String[]{"urg", "group"}, new Object[]{urg, group}));
            assignments.addAll(getHibernateTemplate().findByNamedParam("from Assignment as tlco where tlco.userRoleGroup =:urg and tlco.categoryOption.category.group =:group", new String[]{"urg", "group"}, new Object[]{urg, group}));
        } else {
            assignments.addAll(getHibernateTemplate().findByNamedParam("from Assignment as tlco where tlco.userRoleGroup =:urg and tlco.location =:loc and tlco.group =:group", new String[]{"urg", "loc", "group"}, new Object[]{urg, location, group}));
            assignments.addAll(getHibernateTemplate().findByNamedParam("from Assignment as tlco where tlco.userRoleGroup =:urg and tlco.location =:loc and tlco.category.group =:group", new String[]{"urg", "loc", "group"}, new Object[]{urg, location, group}));
            assignments.addAll(getHibernateTemplate().findByNamedParam("from Assignment as tlco where tlco.userRoleGroup =:urg and tlco.location =:loc and tlco.categoryOption.category.group =:group", new String[]{"urg", "loc", "group"}, new Object[]{urg, location, group}));
        }
        return assignments;
    }

    @SuppressWarnings("unchecked")
    public List<Assignment> getByUrgLocGroupOnly(UserRoleGroup urg, Location location, Group group) {
        List<Assignment> assignments = new ArrayList<Assignment>();
        if (location == null) {
            assignments.addAll(getHibernateTemplate().findByNamedParam("from Assignment as tlco where tlco.userRoleGroup =:urg and tlco.group =:group", new String[]{"urg", "group"}, new Object[]{urg, group}));
        } else {
            assignments.addAll(getHibernateTemplate().findByNamedParam("from Assignment as tlco where tlco.userRoleGroup =:urg and tlco.location =:loc and tlco.group =:group", new String[]{"urg", "loc", "group"}, new Object[]{urg, location, group}));
        }
        return assignments;
    }

    @SuppressWarnings("unchecked")
    public List<Assignment> getByUrgLocCat(UserRoleGroup urg, Location location, Category cat) {
        List<Assignment> assignments = new ArrayList<Assignment>();
        if (location == null) {
            assignments.addAll(getHibernateTemplate().findByNamedParam("from Assignment as tlco where tlco.userRoleGroup =:urg and tlco.category =:cat", new String[]{"urg", "cat"}, new Object[]{urg, cat}));
            assignments.addAll(getHibernateTemplate().findByNamedParam("from Assignment as tlco where tlco.userRoleGroup =:urg and tlco.categoryOption.category =:cat", new String[]{"urg", "cat"}, new Object[]{urg, cat}));
        } else {
            assignments.addAll(getHibernateTemplate().findByNamedParam("from Assignment as tlco where tlco.userRoleGroup =:urg and tlco.location =:loc and tlco.category =:cat", new String[]{"urg", "loc", "cat"}, new Object[]{urg, location, cat}));
            assignments.addAll(getHibernateTemplate().findByNamedParam("from Assignment as tlco where tlco.userRoleGroup =:urg and tlco.location =:loc and tlco.categoryOption.category =:cat", new String[]{"urg", "loc", "cat"}, new Object[]{urg, location, cat}));
        }
        return assignments;
    }

    @SuppressWarnings("unchecked")
    public List<Assignment> getByUrgLocCatOnly(UserRoleGroup urg, Location location, Category cat) {
        List<Assignment> assignments = new ArrayList<Assignment>();
        if (location == null) {
            assignments.addAll(getHibernateTemplate().findByNamedParam("from Assignment as tlco where tlco.userRoleGroup =:urg and tlco.category =:cat", new String[]{"urg", "cat"}, new Object[]{urg, cat}));
        } else {
            assignments.addAll(getHibernateTemplate().findByNamedParam("from Assignment as tlco where tlco.userRoleGroup =:urg and tlco.location =:loc and tlco.category =:cat", new String[]{"urg", "loc", "cat"}, new Object[]{urg, location, cat}));
        }
        return assignments;
    }

    @SuppressWarnings("unchecked")
    public List<Assignment> getByUrgLocCatOpt(UserRoleGroup urg, Location location, CategoryOption catOpt) {
        if (location == null) {
            return getHibernateTemplate().findByNamedParam("from Assignment as tlco where tlco.userRoleGroup =:urg and tlco.categoryOption =:catOpt", new String[]{"urg", "catOpt"}, new Object[]{urg, catOpt});
        } else {
            return getHibernateTemplate().findByNamedParam("from Assignment as tlco where tlco.userRoleGroup =:urg and tlco.location =:loc and tlco.categoryOption =:catOpt", new String[]{"urg", "loc", "catOpt"}, new Object[]{urg, location, catOpt});
        }
    }

    public int getAssignmentCount(AssignmentFilter assignmentFilter, String queryParam) {
        DetachedCriteria dc = getAssignmentQueryCriteria(assignmentFilter, queryParam, new HashMap<String, Boolean>());
        dc.setProjection(Projections.rowCount());
        return DataAccessUtils.intResult(getHibernateTemplate().findByCriteria(dc));
    }

    @SuppressWarnings("unchecked")
    public List<UserRoleGroup> getAssignments(Location location, Group group, Category category, CategoryOption categoryOption) {
        DetachedCriteria criteria = DetachedCriteria.forClass(Assignment.class);
        Disjunction disj = Restrictions.disjunction();

        if (categoryOption != null) {
            Conjunction conj = Restrictions.conjunction();
            conj.add(Restrictions.eq("categoryOption", categoryOption));
            addLocationRestriction(location, conj);
            disj.add(conj);
        }

        if (category != null) {
            Conjunction conj = Restrictions.conjunction();
            conj.add(Restrictions.eq("category", category));
            conj.add(Restrictions.isNull("categoryOption"));
            addLocationRestriction(location, conj);
            disj.add(conj);
        }

        if (group != null) {
            Conjunction conj = Restrictions.conjunction();
            conj.add(Restrictions.eq("group", group));
            conj.add(Restrictions.isNull("category"));
            conj.add(Restrictions.isNull("categoryOption"));
            addLocationRestriction(location, conj);
            disj.add(conj);
        }

        if (location != null) {
            Conjunction conj = Restrictions.conjunction();
            conj.add(Restrictions.eq("location", location));
            conj.add(Restrictions.isNull("group"));
            conj.add(Restrictions.isNull("category"));
            conj.add(Restrictions.isNull("categoryOption"));
            disj.add(conj);
        }

        // check global assignments
        Conjunction conj = Restrictions.conjunction();
        conj.add(Restrictions.isNull("location"));
        conj.add(Restrictions.isNull("group"));
        conj.add(Restrictions.isNull("category"));
        conj.add(Restrictions.isNull("categoryOption"));
        disj.add(conj);

        criteria.add(disj);

        List<Assignment> tlcos = getHibernateTemplate().findByCriteria(criteria);

        List<UserRoleGroup> urgs = new ArrayList<UserRoleGroup>();
        for (Assignment tlco : tlcos) {
            if (!urgs.contains(tlco.getUserRoleGroup()))
                urgs.add(tlco.getUserRoleGroup());
        }

        return urgs;
    }

    @SuppressWarnings("unchecked")
    public List<UserRoleGroup> getMoreBetterAssignments(Location location, Group group, Category category, CategoryOption categoryOption) {
        DetachedCriteria criteria = DetachedCriteria.forClass(Assignment.class);
        Disjunction disj = Restrictions.disjunction();

        if (categoryOption != null) {
            disj.add(Restrictions.eq("categoryOption", categoryOption));
        }

        if (category != null) {
            disj.add(Restrictions.eq("category", category));
        }

        disj.add(Restrictions.eq("group", group));
        disj.add(Restrictions.isNull("group"));

        if (location != null) {
            disj.add(Restrictions.eq("location", location));
        }

        criteria.add(disj);

        List<Assignment> tlcos = getHibernateTemplate().findByCriteria(criteria);

        // This block is for 'legacy' assignments. New assignments can be tied to either location, group, cat, or catOpt. Old assignments were tied to location AND catOpt.
        List<UserRoleGroup> urgs = new ArrayList<UserRoleGroup>();
        for (Assignment tlco : tlcos) {
            // If there's no group on this assignment, it is a 'legacy' assignment because group is now a required field on Assignment. Add it to the final urgs list because it matched our Criteria above.
            if(tlco.getGroup() != null) {
                if (!urgs.contains(tlco.getUserRoleGroup())) {
                    urgs.add(tlco.getUserRoleGroup());
                }
            // In a perfect world where everyone deleted their 'legacy' assignments and did them over, we could let the database do all this
            // If we're filtering on location, also make sure the urg is in the group that's being filtered on
            } else if((location != null && tlco.getLocation() != null && location.equals(tlco.getLocation()) && tlco.getUserRoleGroup() != null && tlco.getUserRoleGroup().getGroup().equals(group)) ||
                      (categoryOption != null && tlco.getCategoryOption() != null && categoryOption.equals(tlco.getCategoryOption()) && tlco.getCategoryOption().getCategory().getGroup().equals(group)) ||
                      (category != null && tlco.getCategory() != null && category.equals(tlco.getCategory()) && tlco.getCategory().getGroup().equals(group))) {
                if (!urgs.contains(tlco.getUserRoleGroup())) {
                    urgs.add(tlco.getUserRoleGroup());
                }
            }
        }

        return urgs;
    }

    private void addLocationRestriction(Location location, Conjunction conj) {
        if (location != null) {
            conj.add(Restrictions.eq("location", location));
        }
    }
}
