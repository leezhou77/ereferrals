package net.grouplink.ehelpdesk.dao.hibernate.asset;

import net.grouplink.ehelpdesk.dao.asset.AssetFieldGroupDao;
import net.grouplink.ehelpdesk.domain.asset.AssetFieldGroup;
import net.grouplink.ehelpdesk.domain.asset.AssetType;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * User: jaymehafen
 * Date: Jul 17, 2008
 * Time: 5:41:02 PM
 */
public class HibernateAssetFieldGroupDao extends HibernateDaoSupport implements AssetFieldGroupDao {
    public AssetFieldGroup getById(Integer id) {
        return (AssetFieldGroup) getHibernateTemplate().get(AssetFieldGroup.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<AssetFieldGroup> getByAssetType(AssetType assetType) {
        return (List<AssetFieldGroup>) getHibernateTemplate().findByNamedParam("from AssetFieldGroup afg where afg.assetType = :ASSETTYPE", "ASSETTYPE", assetType);
    }

    public void saveAssetFieldGroup(AssetFieldGroup assetFieldGroup) {
        getHibernateTemplate().saveOrUpdate(assetFieldGroup);
    }

    public void deleteAssetFieldGroup(AssetFieldGroup assetFieldGroup) {
        getHibernateTemplate().delete(assetFieldGroup);
    }

    @SuppressWarnings("unchecked")
    public List<AssetFieldGroup> getAllAssetFieldGroups() {
        return (List<AssetFieldGroup>) getHibernateTemplate().find("from AssetFieldGroup afg");
    }

    public void flush() {
        getHibernateTemplate().flush();
    }

    @SuppressWarnings("unchecked")
    public AssetFieldGroup getByName(String name) {
        List<AssetFieldGroup> l = (List<AssetFieldGroup>) getHibernateTemplate().findByNamedParam("from AssetFieldGroup afg where afg.name = :NAME", "NAME", name);
        return (l.size() > 0) ? l.get(0) : null;
    }
}
