package net.grouplink.ehelpdesk.dao.zen;

import net.grouplink.ehelpdesk.domain.zen.ZenSystemSetting;

public interface ZenSystemSettingDao {
    ZenSystemSetting getByName(String name);
}
