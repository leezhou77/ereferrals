package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.Group;

import java.util.List;

/**
 * @author mrollins
 * @version 1.0
 */
public interface CategoryDao {
    public List<Category> getCategories();

    public Category getCategoryById(Integer id);

    public Category getCategoryByName(String name);

    public List<Category> getCategoryByGroupId(Integer id);

    public void saveCategory(Category category);

    public void deleteCategory(Category category);

    public boolean isDuplicateCategory(Category cat);

    public void flush();

    public Category getCategoryByNameAndGroupId(String categoryName, Integer groupId);

    List<Category> getAllCategoriesByGroup(Group group);

    void justDeleteCategory(Category category);
}
