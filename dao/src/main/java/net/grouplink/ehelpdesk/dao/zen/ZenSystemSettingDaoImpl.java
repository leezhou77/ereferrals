package net.grouplink.ehelpdesk.dao.zen;

import net.grouplink.ehelpdesk.domain.zen.ZenSystemSetting;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.dao.support.DataAccessUtils;

public class ZenSystemSettingDaoImpl extends HibernateDaoSupport implements ZenSystemSettingDao {
    public ZenSystemSetting getByName(String name) {
        return (ZenSystemSetting) DataAccessUtils.singleResult(getHibernateTemplate()
                .findByNamedParam("from ZenSystemSetting zss where zss.name = :name", "name", name));
    }
}
