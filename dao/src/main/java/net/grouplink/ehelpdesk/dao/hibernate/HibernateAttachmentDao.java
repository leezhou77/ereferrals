package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.AttachmentDao;
import net.grouplink.ehelpdesk.domain.Attachment;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class HibernateAttachmentDao extends HibernateDaoSupport implements AttachmentDao {
    
    public Attachment getById(Integer id) {
        return (Attachment) getHibernateTemplate().get(Attachment.class, id);
    }

    public void delete(Attachment attachment) {
        getHibernateTemplate().delete(attachment);
    }
}
