package net.grouplink.ehelpdesk.dao.ldap;

import net.grouplink.ehelpdesk.common.utils.PropertiesConstants;
import net.grouplink.ehelpdesk.common.utils.UserSearch;
import net.grouplink.ehelpdesk.dao.LdapDao;
import net.grouplink.ehelpdesk.dao.LdapFieldDao;
import net.grouplink.ehelpdesk.dao.PropertiesDao;
import net.grouplink.ehelpdesk.domain.LdapUser;
import net.grouplink.ehelpdesk.domain.Properties;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.ldap.BadLdapGrammarException;
import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DistinguishedName;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.ldap.filter.LikeFilter;
import org.springframework.ldap.filter.OrFilter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.SpringSecurityMessageSource;

import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.ldap.InitialLdapContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class LdapDaoImpl implements LdapDao {

    protected final Log log = LogFactory.getLog(getClass());

    protected MessageSourceAccessor messages = SpringSecurityMessageSource.getAccessor();
    private LdapTemplate ldapTemplate;
    private LdapContextSource ldapContextSource;
    private List<String> baseSearchList = new ArrayList<String>();
    private LdapFieldDao ldapFieldDao;
    private String ldapUrl;
    private String base;
    private String filter = "(objectClass=*)";
    private PropertiesDao propertiesDao;

    protected LdapDaoImpl() {
    }

    public boolean init(String ldapUrl, String ldapUserName, String ldapPassword, String base, List<String> baseSearch, String filter) {
        this.ldapContextSource = new LdapContextSource();
        this.ldapContextSource.setUrl(ldapUrl);
        DistinguishedName dn = new DistinguishedName(ldapUserName);
        this.ldapContextSource.setUserDn(dn.encode());
        this.ldapContextSource.setPassword(ldapPassword);
        this.ldapContextSource.setBase(base);
        Map<String, String> envProps = new HashMap<String, String>();
        envProps.put("java.naming.ldap.attributes.binary", "guid objectGUID");
        this.ldapContextSource.setBaseEnvironmentProperties(envProps);
        InitialLdapContext dirContext = null;
        try {
            this.ldapContextSource.afterPropertiesSet();
            dirContext = (InitialLdapContext) this.ldapContextSource.getReadOnlyContext();
            this.ldapTemplate.setContextSource(this.ldapContextSource);
        } catch (Exception e) {
            log.error("error getting ldap context", e);
            return false;
        } finally {
            if (dirContext != null) {
                try {
                    dirContext.close();
                } catch (Exception ignored) {
                }
            }
        }

        this.ldapUrl = ldapUrl;
        this.base = base;
        baseSearchList = baseSearch;
        this.filter = filter;

        return true;
    }

    @SuppressWarnings("unchecked")
    public Set<LdapUser> authenticateUser(String username, String password) throws AuthenticationException {
        log.debug("called LdapDaoImpl.authenticateUser, username = " + username);
        Set<LdapUser> userList = new HashSet<LdapUser>();
        try {
            log.debug("searching ldap for dn " + username);
            DistinguishedName dn = new DistinguishedName(username);
            userList.add(getLdapUserForAuthentication(dn));
        } catch (BadLdapGrammarException e) {
            log.debug("caught BadLdapGrammarException, username was not dn");

            log.debug("searching for user in baseSearchList");
            for (String aBaseSearchList : baseSearchList) {
                log.debug("aBaseSearchList = " + aBaseSearchList);
                List<LdapUser> list = searchLdapUserByUsername(aBaseSearchList, username);
                log.debug("found " + list.size() + " matches");
                userList.addAll(list);
            }

            if (baseSearchList.size() == 0) {
                log.debug("baseSearchList is empty");
                log.debug("searching for user in base");
                List<LdapUser> list = searchLdapUserByUsername("", username);
                log.debug("found " + list.size() + " matches");
                userList.addAll(list);
            }
        }

        log.debug("userList.size() = " + userList.size());
        if (userList.size() == 1) {

            LdapUser ldapUser = null;
            for (LdapUser anUserList : userList) {
                ldapUser = anUserList;
            }

            DirContext dirContext = null;
            try {
                log.debug("attempting to authenticate with dn " + ldapUser.getDn());
                DistinguishedName dn = new DistinguishedName(ldapUser.getDn());
                LdapContextSource tempLdap = new LdapContextSource();
                tempLdap.setUrl(ldapUrl);
                tempLdap.setUserDn(dn.encode());
                tempLdap.setPassword(password);
                tempLdap.setBase(base);
                tempLdap.afterPropertiesSet();
                dirContext = tempLdap.getReadOnlyContext();
                log.debug("authenticated successfully");
            } catch (Exception e) {
                log.debug("unable to authenticate");
//                logger.error("error authenticating ldap user " + ldapUser.getDn() + " in base " + base, e);
                log.debug("base = " + base);
                if (StringUtils.isNotBlank(base)) {
                    log.debug("attempting to authenticate with dn " + base + "," + ldapUser.getDn());
                    DirContext dirContext2 = null;
                    try {
                        DistinguishedName dn = new DistinguishedName(ldapUser.getDn());
                        dn.prepend(new DistinguishedName(base));
                        LdapContextSource tempLdap = new LdapContextSource();
                        tempLdap.setUrl(ldapUrl);
                        tempLdap.setUserDn(dn.encode());
                        tempLdap.setPassword(password);
                        tempLdap.setBase(base);
                        tempLdap.afterPropertiesSet();
                        dirContext2 = tempLdap.getReadOnlyContext();
                        log.debug("authentication successfully");
                    } catch (Exception ex) {
                        log.error("error authenticating ldap user " + ldapUser.getDn() + " in base " + base, e);
                        throw new BadCredentialsException(messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"));
                    } finally {
                        if (dirContext2 != null) {
                            try {
                                dirContext2.close();
                            } catch (Exception ignored) {
                            }
                        }
                    }
                } else {
                    throw new BadCredentialsException(messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"));
                }
            } finally {
                if (dirContext != null) {
                    try {
                        dirContext.close();
                    } catch (Exception ignored) {
                    }
                }
            }
        }

        return userList;
    }

    public LdapUser getLdapUserForAuthentication(DistinguishedName dn) throws BadLdapGrammarException {
        try {
            List ldapUserList;
            SearchControls controls = new SearchControls();
            controls.setSearchScope(SearchControls.OBJECT_SCOPE);
            controls.setReturningObjFlag(true);
            ldapUserList = ldapTemplate.search(dn, filter, controls, getContextMapper(1, ldapFieldDao));
            if (ldapUserList.size() > 0) {
                return (LdapUser) ldapUserList.get(0);
            } else {
                return null;
            }
        } catch (Exception e) {
            log.error("error getting ldap user for authentication" + e);
            return null;
        }
    }

    public LdapUser getLdapUser(String dn) {
        try {
            List ldapUserList;
            SearchControls controls = new SearchControls();
            controls.setSearchScope(SearchControls.OBJECT_SCOPE);
            controls.setReturningObjFlag(true);
            DistinguishedName dN = new DistinguishedName(dn);
            ldapUserList = ldapTemplate.search(dN, filter, controls, getContextMapper(1, ldapFieldDao));
            if (ldapUserList.size() > 0) {
                return (LdapUser) ldapUserList.get(0);
            } else {
                return new LdapUser();
            }
        } catch (Exception e) {
            log.error("error getting ldap user", e);
            return new LdapUser();
        }
    }

    public byte[] getGuidForUserDn(String dn) {
        SearchControls controls = new SearchControls();
        controls.setSearchScope(SearchControls.OBJECT_SCOPE);
        controls.setReturningObjFlag(true);
        controls.setReturningAttributes(new String[]{"guid", "objectGUID"});
        DistinguishedName dN = new DistinguishedName(dn);
        try {
            List guidList = ldapTemplate.search(dN, filter, controls, getGuidContextMapper());
            return (byte[]) guidList.get(0);
        } catch (Exception e) {
            log.error("error", e);
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public List<LdapUser> findAllFirstLevel(String path, int searchType) {
        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(SearchControls.ONELEVEL_SCOPE);
        searchControls.setReturningObjFlag(true);
        DistinguishedName dN = new DistinguishedName(path);
        if (StringUtils.isBlank(filter)) {
            filter = "(objectClass=*)";
        }

        return ldapTemplate.search(dN, filter, searchControls, getContextMapper(searchType, ldapFieldDao));
    }

    @SuppressWarnings("unchecked")
    public List<LdapUser> searchLdapUser(String path, UserSearch userSearch, int searchType) {
        SearchControls controls = new SearchControls();
        controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        controls.setReturningObjFlag(true);

        // limit max results to 1000 - ticket # 9692
//        controls.setCountLimit(1000);

        DistinguishedName dN = new DistinguishedName(path);

        AndFilter andFilter = new AndFilter();
        andFilter.and(new EqualsFilter("objectclass", "person"));
//        andFilter.and(new NotFilter(new EqualsFilter("objectclass", "computer")));
        if (!StringUtils.isBlank(userSearch.getFirstName())) {
            andFilter.and(new LikeFilter("givenName", userSearch.getFirstName() + "*"));
        }

        if (!StringUtils.isBlank(userSearch.getLastName())) {
            andFilter.and(new LikeFilter("sn", userSearch.getLastName() + "*"));
        }

        if (!StringUtils.isBlank(userSearch.getEmail())) {
            andFilter.and(new LikeFilter("mail", userSearch.getEmail() + "*"));
        }

        String finalFilter = "(&" + filter + " " + andFilter.encode() + ")";

        // limit max results to 1000 - ticket # 9692
//        InitialLdapContext context = (InitialLdapContext) ldapTemplate.getContextSource();
//        context.setRequestControls(new Control[]{new PagedResultsControl(1000, Control.CRITICAL)});
        return ldapTemplate.search(dN, finalFilter, controls, getContextMapper(searchType, ldapFieldDao));
    }

    @SuppressWarnings("unchecked")
    public List<LdapUser> searchLdapUserByUsername(String path, String userName) {
        log.debug("called LdapDaoImpl.searchLdapUserByUsername, path=" + path + ", username=" + userName);
        List ldapUserList = new ArrayList<LdapUser>();
        SearchControls controls = new SearchControls();
        controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        controls.setReturningObjFlag(true);
        DistinguishedName dN = new DistinguishedName(path);
        if (!StringUtils.isBlank(userName)) {
            AndFilter andFilter = new AndFilter();
            andFilter.and(new EqualsFilter("objectclass", "person"));

            // if ldapconfig.usernameAttribute is not blank, search only by that,
            // otherwise, search by uid, cn, or samaccountname
            String usernameAttr = getUsernameAttribute();
            if (StringUtils.isNotBlank(usernameAttr)) {
                andFilter.and(new EqualsFilter(usernameAttr, userName));
            } else {
                OrFilter orFilter = new OrFilter();
                orFilter.or(new EqualsFilter("uid", userName));
                orFilter.or(new EqualsFilter("cn", userName));
                orFilter.or(new EqualsFilter("sAMAccountName", userName));
                andFilter.and(orFilter);
            }

            String finalFilter = "(&" + filter + " " + andFilter.encode() + ")";
            log.debug("username filter: " + finalFilter);
            ldapUserList = ldapTemplate.search(dN, finalFilter, controls, getContextMapper(1, ldapFieldDao));
        }

        return ldapUserList;
    }

    private String getUsernameAttribute() {
        String usernameAttr = null;
        Properties usernameAttrProp = propertiesDao.getPropertiesByName(PropertiesConstants.LDAP_USERNAME_ATTRIBUTE);
        if (usernameAttrProp != null) {
            usernameAttr = usernameAttrProp.getValue();
        }

        return usernameAttr;
    }

    protected ContextMapper getContextMapper(int searchType, LdapFieldDao ldapFieldDao) {
        PersonContextMapper perCxtMapp = new PersonContextMapper(searchType);
        perCxtMapp.setLdapFieldDao(ldapFieldDao);
        perCxtMapp.setUsernameAttribute(getUsernameAttribute());
        return perCxtMapp;
    }

    protected ContextMapper getGuidContextMapper() {
        return new ContextMapper() {
            public Object mapFromContext(Object ctx) {
                DirContextAdapter context = (DirContextAdapter) ctx;
                Object guid = context.getObjectAttribute("guid");
                if (guid == null) {
                    guid = context.getObjectAttribute("objectGUID");
                }

                return guid;
            }
        };
    }

    public void setLdapTemplate(LdapTemplate ldapTemplate) {
        this.ldapTemplate = ldapTemplate;
    }

    public void setLdapContextSource(LdapContextSource ldapContextSource) {
        this.ldapContextSource = ldapContextSource;
    }

    public void setLdapFieldDao(LdapFieldDao ldapFieldDao) {
        this.ldapFieldDao = ldapFieldDao;
    }

    public void setPropertiesDao(PropertiesDao propertiesDao) {
        this.propertiesDao = propertiesDao;
    }

}
