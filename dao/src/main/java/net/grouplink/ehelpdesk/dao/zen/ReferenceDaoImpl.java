package net.grouplink.ehelpdesk.dao.zen;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.dao.support.DataAccessUtils;
import net.grouplink.ehelpdesk.domain.zen.Reference;

public class ReferenceDaoImpl extends HibernateDaoSupport implements ReferenceDao {
    public byte[] getZuidForObjectUid(String objectUid) {
        Reference reference = (Reference) DataAccessUtils.singleResult(getHibernateTemplate().findByNamedParam("from Reference r where r.objectUid = :objectUid", "objectUid", objectUid));
        if (reference != null) {
            return reference.getId();
        } else {
            return null;
        }
    }
}
