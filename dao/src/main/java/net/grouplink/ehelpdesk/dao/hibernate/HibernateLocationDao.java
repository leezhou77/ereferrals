package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.LocationDao;
import net.grouplink.ehelpdesk.domain.Location;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Projections;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.dao.support.DataAccessUtils;

import java.util.List;

/**
 * @author mrollins
 * @version 1.0
 */
public class HibernateLocationDao extends HibernateDaoSupport implements LocationDao {

    @SuppressWarnings("unchecked")
    public List<Location> getLocations() {
        return getHibernateTemplate().find("select l from Location as l where l.active = true order by l.name asc");
    }

    @SuppressWarnings("unchecked")
    public List<Location> getAllLocations(){
        return getHibernateTemplate().find("from Location as l order by l.name asc");
    }

    @SuppressWarnings("unchecked")
    public List<Location> getLocations(String queryParam, int start, int count) {
        DetachedCriteria dc = getLocationQueryCriteria(queryParam);
        // order by
        dc.addOrder(Order.asc("name"));
        return getHibernateTemplate().findByCriteria(dc, start, count);
    }

    public int getLocationCount(String queryParam) {
        DetachedCriteria dc = getLocationQueryCriteria(queryParam);
        dc.setProjection(Projections.rowCount());
        return DataAccessUtils.intResult(getHibernateTemplate().findByCriteria(dc));
    }

    private DetachedCriteria getLocationQueryCriteria(String queryParam) {
        DetachedCriteria dc = DetachedCriteria.forClass(Location.class);

        // only the active users
        dc.add(Restrictions.eq("active", Boolean.TRUE));
        if (StringUtils.isNotBlank(queryParam)) {
            // Match the location name
            dc.add(Restrictions.ilike("name", queryParam, MatchMode.ANYWHERE));
        }
        
        return dc;
    }

    public Location getLocationById(Integer id) {
        List locs = getHibernateTemplate().findByNamedParam("select l from Location as l where l.id=:ID", "ID", id);
		if(locs.size() > 0){
			return (Location) locs.get(0);
		}
		return null;
    }

    public Location getLocationByName(String name) {
        List locs = getHibernateTemplate().findByNamedParam("select l from Location as l where l.name=:NAME", "NAME", name);
		if(locs.size() > 0){
			return (Location) locs.get(0);
		}
		return null;
    }

    public void saveLocation(Location location) {
        getHibernateTemplate().saveOrUpdate(location);
    }

    public void deleteLocation(Location location) {
        getHibernateTemplate().delete(location);
    }

    public boolean hasDuplicateLocationName(Location location) {
        Integer id = Integer.valueOf("-1");
        if(location.getId() != null){
            id = location.getId();
        }
        List list = getHibernateTemplate().findByNamedParam(
                "from Location as l where l.id != :locationId and l.name = :locationName",
                new String[]{"locationId", "locationName"}, new Object[]{id, location.getName()});
        return list.size() > 0;
    }

    public void flush() {
        getHibernateTemplate().flush();
    }
}
