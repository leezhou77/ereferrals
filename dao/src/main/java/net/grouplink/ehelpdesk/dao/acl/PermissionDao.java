package net.grouplink.ehelpdesk.dao.acl;

import net.grouplink.ehelpdesk.domain.acl.*;

import java.util.List;

public interface PermissionDao {
    GlobalPermissionEntry getGlobalPermissionEntry(Permission permission);

    Permission getPermissionByKey(String key);

    List<Permission> getGlobalPermissions();

    List<Permission> getSchemePermissions();
    
    List<Permission> getFieldPermissionsByModelName(String modelName);

    void savePermission(Permission permission);

    void deletePermission(Permission permission);

    List<PermissionScheme> getPermissionSchemes();

    void savePermissionScheme(PermissionScheme permissionScheme);

    void deletePermissionScheme(PermissionScheme permissionScheme);

    void saveGlobalPermissionEntry(GlobalPermissionEntry globalPermissionEntry);

    void deleteGlobalPermissionEntry(GlobalPermissionEntry globalPermissionEntry);

    void savePermissionSchemeEntry(PermissionSchemeEntry permissionSchemeEntry);

    List<GlobalPermissionEntry> getGlobalPermissionEntries();

    Permission getPermissionById(Integer gpId);

    PermissionScheme getPermissionSchemeById(Integer id);

    List<Permission> getGroupPermissions();

    PermissionSchemeEntry getPermissionSchemeEntry(PermissionScheme permissionScheme, Permission permission);
    
    List<PermissionSubject> getPermissionSubjects();
    
    PermissionSubject getPermissionSubjectByModelClass(String modelClass);
    
    PermissionSubject getPermissionSubjectByModelClassAndField(String modelClass, String fieldName);
    
    PermissionSubject getPermissionSubjectByModelNameAndField(String modelName, String fieldName);

    void savePermissionSubject(PermissionSubject permissionSubject);
}
