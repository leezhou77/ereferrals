package net.grouplink.ehelpdesk.dao.asset;

import net.grouplink.ehelpdesk.domain.asset.Vendor;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Jul 14, 2008
 * Time: 11:55:56 AM
 */
public interface VendorDao {
    Vendor getById(Integer id);

    Vendor getByName(String name);

    List<Vendor> getAllVendors();

    void saveVendor(Vendor vendor);

    void deleteVendor(Vendor vendor);

    void flush();
}
