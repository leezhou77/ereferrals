package net.grouplink.ehelpdesk.dao;


import net.grouplink.ehelpdesk.domain.Address;

public interface AddressDao {


    void saveAddress(Address address);

    void deleteAddress(Address address);

    void flush();

    Address getById(int addressId);
}
