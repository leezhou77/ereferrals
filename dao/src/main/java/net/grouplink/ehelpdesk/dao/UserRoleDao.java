package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.UserRole;
import net.grouplink.ehelpdesk.domain.User;

public interface UserRoleDao {
	public UserRole getUserRoleById(Integer userRoleId);
	public void deleteUserRole(UserRole userRole);
    public UserRole getUserRoleByUserIdAndRoleId(Integer userId, Integer roleId);
    public User getUserByUserNameAndRoleId(String userName, Integer roleId);

    User getUserByEmailAndRoleId(String email, Integer roleId);

    void flush();

    void inactivateUserRole(UserRole userRole);

    void saveUserRole(UserRole userRole);
}
