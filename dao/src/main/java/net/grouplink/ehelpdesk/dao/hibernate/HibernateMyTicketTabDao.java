package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.MyTicketTabDao;
import net.grouplink.ehelpdesk.domain.MyTicketTab;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class HibernateMyTicketTabDao extends HibernateDaoSupport implements MyTicketTabDao {
    public MyTicketTab getById(Integer id) {
        return (MyTicketTab) getHibernateTemplate().get(MyTicketTab.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<MyTicketTab> getByUser(User user) {
        return getHibernateTemplate().findByNamedParam("from MyTicketTab m where m.user = :user order by m.order",
                "user", user);
    }

    public void save(MyTicketTab myTicketTab) {
        getHibernateTemplate().saveOrUpdate(myTicketTab);
    }

    public void delete(MyTicketTab myTicketTab) {
        getHibernateTemplate().delete(myTicketTab);
    }

    public void saveAll(List<MyTicketTab> myTicketTabs) {
        getHibernateTemplate().saveOrUpdateAll(myTicketTabs);
    }

    @SuppressWarnings("unchecked")
    public List<MyTicketTab> getByTicketFilter(TicketFilter tf) {
        return getHibernateTemplate().findByNamedParam("from MyTicketTab m where m.ticketFilter = :ticketFilter",
                "ticketFilter", tf);
    }
}
