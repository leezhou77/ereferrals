package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.User;

import java.util.List;

/**
 * Author: zpearce
 * Date: 4/11/11
 * Time: 11:48 AM
 */
public interface TicketFullTextSearchDao {
    void reindexTickets();
    int getReindexStatusPercent();
    List<Ticket> fullTextSearch(String query, String[] fields, int startPage, int pageSize, User user);
    int fullTextSearchCount(String query, String[] fields, User user);
}
