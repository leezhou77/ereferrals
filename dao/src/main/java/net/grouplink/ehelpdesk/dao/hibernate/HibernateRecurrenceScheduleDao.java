package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.RecurrenceScheduleDao;
import net.grouplink.ehelpdesk.domain.RecurrenceSchedule;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

public class HibernateRecurrenceScheduleDao extends HibernateDaoSupport implements RecurrenceScheduleDao {
    public RecurrenceSchedule getById(Integer id) {
        return (RecurrenceSchedule) getHibernateTemplate().get(RecurrenceSchedule.class, id);
    }

    public void save(RecurrenceSchedule recurrenceSchedule) {
        getHibernateTemplate().saveOrUpdate(recurrenceSchedule);
    }

    @SuppressWarnings("unchecked")
    public List<RecurrenceSchedule> getAllRunning() {
        return getHibernateTemplate().find("from RecurrenceSchedule r where r.running = true");
    }

    @SuppressWarnings("unchecked")
    public List<RecurrenceSchedule> getRunningTicketTemplateSchedules() {
        return getHibernateTemplate().find("from RecurrenceSchedule r where r.running = true and " +
                "r.ticketTemplateMaster is not null and r.ticketTemplateMaster.active = true");
    }

    @SuppressWarnings("unchecked")
    public List<RecurrenceSchedule> getRunningScheduleStatusSchedules() {
        return getHibernateTemplate().find("from RecurrenceSchedule r where r.running = true and r.scheduleStatus is not null");
    }

    @SuppressWarnings("unchecked")
    public List<RecurrenceSchedule> getRunningReportSchedules() {
        return getHibernateTemplate().find("from RecurrenceSchedule r where r.running = true and r.report is not null");
    }

    public void delete(RecurrenceSchedule recurrenceSchedule) {
        getHibernateTemplate().delete(recurrenceSchedule);
    }
}
