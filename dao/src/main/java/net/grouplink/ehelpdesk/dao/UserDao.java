package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.Address;
import net.grouplink.ehelpdesk.domain.Phone;
import net.grouplink.ehelpdesk.domain.Role;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.common.utils.UserSearch;

import java.util.List;
import java.util.Map;

/**
 * Identifies a User data access object
 *
 * @author mrollins
 * @version 1.0
 */
public interface UserDao {
    public User getUserById(Integer id);
    public User getUserByUsername(String username);
    public User getUserByUsernameAndPassword(String username, String password);
    public User getUserByEmail(String email);
    public List<User> getUsers();
    public List<User> getActiveUsers();    
    public void saveUser(Object user);
    public void deleteUser(User user);
    public List<User> searchUser(UserSearch userSearch);
    public void removePhone(Phone phone);
    public void removeAddress(Address address);

    public boolean userInUse(User user);

    public void flush();

    public User getUserByFirstAndLastName(String firstName, String lastName);

    public List<User> getFilteredUsers(String sort, boolean ascending, String group, String firstName, String lastName, String role, Boolean status, String username, String email);

    void clear();

    List<User> getUsers(String queryParam, int start, int count, Map<String, Boolean> sortFieldsOrder, boolean includeInactive);

    int getUserCount(String queryParam, boolean includeInactive);

    List<User> getUsersWithAnyRole(List<Role> roles);
}
