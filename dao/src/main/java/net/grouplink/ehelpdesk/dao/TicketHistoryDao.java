package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.TicketHistory;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.Attachment;

import java.util.List;

import org.hibernate.ScrollableResults;

/**
 * @author mrollins
 * @version 1.0
 */
public interface TicketHistoryDao {
    void saveTicketHistoryEntry(TicketHistory ticketHistory);

    TicketHistory getTicketHistoryById(Integer id);

    List<TicketHistory> getTicketHistoriesByTicket(Ticket ticket);

    List<TicketHistory> getTicketHistoriesByTicket(Ticket ticket, boolean sortDesc);

    List<TicketHistory> getEventTicketHistoriesByUser(User user);

    ScrollableResults getAllTicketHistoryByGroupId(Integer groupId);

    void flush();

    void clear();

    int getHistoryCount(Integer groupId);
}
