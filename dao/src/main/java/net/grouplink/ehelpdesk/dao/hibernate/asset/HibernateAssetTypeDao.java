package net.grouplink.ehelpdesk.dao.hibernate.asset;

import net.grouplink.ehelpdesk.dao.asset.AssetTypeDao;
import net.grouplink.ehelpdesk.domain.asset.AssetType;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * User: jaymehafen
 * Date: Jul 17, 2008
 * Time: 1:37:31 PM
 */
public class HibernateAssetTypeDao extends HibernateDaoSupport implements AssetTypeDao {
    public AssetType getById(Integer id) {
        return (AssetType) getHibernateTemplate().get(AssetType.class, id);
    }

    @SuppressWarnings("unchecked")
    public AssetType getByName(String name) {
        List<AssetType> list = (List<AssetType>) getHibernateTemplate().findByNamedParam("from AssetType at where at.name = :NAME", "NAME", name);
        if(list.size() > 0){
            return list.get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public List<AssetType> getAllAssetTypes() {
        return (List<AssetType>) getHibernateTemplate().find("from AssetType at");
    }

    public void saveAssetType(AssetType assetType) {
        getHibernateTemplate().saveOrUpdate(assetType);
    }

    public void deleteAssetType(AssetType assetType) {
        getHibernateTemplate().delete(assetType);
    }

    public void flush() {
        getHibernateTemplate().flush();
    }
}
