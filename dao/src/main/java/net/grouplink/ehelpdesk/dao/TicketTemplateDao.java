package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.TicketTemplate;
import net.grouplink.ehelpdesk.domain.TicketTemplateMaster;

import java.util.List;

public interface TicketTemplateDao {

    TicketTemplate getById(Integer id);

    List<TicketTemplate> getByTicketTemplateMaster(TicketTemplateMaster ticketTemplateMaster);

    int getTicketTemplateCountByPriorityId(Integer priorityId);

    List<TicketTemplate> getTopLevelByTicketTemplateMaster(TicketTemplateMaster ticketTemplateMaster);

    void save(TicketTemplate ticketTemplate);

    void delete(TicketTemplate ticketTemplate);

    void flush();

    int getCountByGroup(Group group);

    int getCountByCategory(Category category);

    int getCountByCategoryOption(CategoryOption categoryOption);
}
