package net.grouplink.ehelpdesk.dao.asset;

import net.grouplink.ehelpdesk.domain.asset.AssetFieldGroup;
import net.grouplink.ehelpdesk.domain.asset.AssetType;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Jul 17, 2008
 * Time: 5:39:52 PM
 */
public interface AssetFieldGroupDao {
    AssetFieldGroup getById(Integer id);
    List<AssetFieldGroup> getByAssetType(AssetType assetType);
    void saveAssetFieldGroup(AssetFieldGroup assetFieldGroup);
    void deleteAssetFieldGroup(AssetFieldGroup assetFieldGroup);

    List<AssetFieldGroup> getAllAssetFieldGroups();

    void flush();

    AssetFieldGroup getByName(String name);
}
