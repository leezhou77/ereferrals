package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.LdapUser;
import net.grouplink.ehelpdesk.common.utils.UserSearch;
import org.springframework.ldap.core.DistinguishedName;
import org.springframework.security.core.AuthenticationException;

import java.util.List;
import java.util.Set;

public interface LdapDao {

	boolean init(String ldapUrl, String ldapUserName, String ldapPassword, String base, List<String> baseSearch, String filter);
    Set<LdapUser> authenticateUser(String username, String password) throws AuthenticationException;
    List<LdapUser> findAllFirstLevel(String path, int searchType);
	LdapUser getLdapUserForAuthentication(DistinguishedName dn);
    LdapUser getLdapUser(String dn);
    List<LdapUser> searchLdapUser(String path, UserSearch userSearch, int searchType);
    byte[] getGuidForUserDn(String dn);
}
