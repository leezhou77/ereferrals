package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Survey;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Feb 13, 2008
 * Time: 1:56:46 PM
 */
public interface SurveyDao {

    public Survey getSurveyByName(String name);

    public Survey getSurveyById(Integer id);

    public List<Survey> getActiveSurveys();

    public void saveSurvey(Survey survey);

    public void deleteSurvey(Survey survey);

    public boolean hasDuplicateSurveyName(Survey survey);

    public void flush();

    List<Survey> getAllSurveys();
}
