package net.grouplink.ehelpdesk.dao.ldap;

import net.grouplink.ehelpdesk.dao.LdapFieldDao;
import net.grouplink.ehelpdesk.domain.LdapField;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * Created by IntelliJ IDEA.
 * User: dgarcia
 * Date: Apr 13, 2007
 * Time: 4:40:52 PM
 */
public class LdapFieldDaoImpl extends HibernateDaoSupport implements LdapFieldDao {

    @SuppressWarnings("unchecked")
    public List<LdapField> getLdapFields() {
        return getHibernateTemplate().find("select lf from LdapField as lf order by lf.name");
    }

    public LdapField getLdapFieldByAttributeID(String fieldName) {
        List list = getHibernateTemplate().findByNamedParam("select lf from LdapField as lf where lf.name = :fieldName", "fieldName", fieldName);
        if(list.size()>0){
            return (LdapField) list.get(0);
        }
        return null;
    }

    public LdapField getLdapFieldById(Integer fieldId) {
        List list = getHibernateTemplate().findByNamedParam("select lf from LdapField as lf where lf.id = :id", "id", fieldId);
        if(list.size()>0){
            return (LdapField) list.get(0);
        }
        return null;
    }

    public void saveLdapField(LdapField ldapField) {
        getHibernateTemplate().saveOrUpdate(ldapField);
        getHibernateTemplate().flush();
    }

    public void saveLdapFields(List ldapFieldList) {
        getHibernateTemplate().saveOrUpdateAll(ldapFieldList);
    }

    public void deleteLdapField(LdapField ldapField) {
        getHibernateTemplate().delete(ldapField);
        getHibernateTemplate().flush();
    }

    public void deleteLdapFieldList(List list) {
        getHibernateTemplate().deleteAll(list);
    }
}
