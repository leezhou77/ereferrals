package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;

import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: Feb 20, 2008
 * Time: 7:10:06 PM
 */
public interface TicketFilterDao {
    List<TicketFilter> getAll();
    List<TicketFilter> getAllPublic();
    List<TicketFilter> getByUser(User user);
    TicketFilter getById(Integer id);
    void save(TicketFilter ticketFilter);
    void delete(TicketFilter ticketFilter);

    List<Ticket> getTickets(TicketFilter ticketFilter, User user, Map<Group, Boolean> hasViewTicketsPermissions);

    List<Ticket> getTickets(TicketFilter ticketFilter, User user, int start, int count, String sort, Map<Group, Boolean> hasViewTicketsPermissions);

    Integer getTicketCount(TicketFilter f, User user, Map<Group, Boolean> hasViewTicketsPermissions);

    int getUsageCount(TicketFilter ticketFilter);

    void flush();

    List<TicketFilter> getPublicPlusUser(User user);

    List<TicketFilter> getByVersionLessThan(Integer version);
}
