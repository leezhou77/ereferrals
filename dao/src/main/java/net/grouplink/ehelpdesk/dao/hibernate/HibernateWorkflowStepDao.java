package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.WorkflowStepDao;
import net.grouplink.ehelpdesk.domain.WorkflowStep;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class HibernateWorkflowStepDao extends HibernateDaoSupport implements WorkflowStepDao {
    public WorkflowStep getById(Integer workflowStepId) {
        return (WorkflowStep) getHibernateTemplate().get(WorkflowStep.class, workflowStepId);
    }

    public void delete(WorkflowStep ws) {
        getHibernateTemplate().delete(ws);
    }
}
