package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.WorkflowStepStatusDao;
import net.grouplink.ehelpdesk.domain.WorkflowStepStatus;
import net.grouplink.ehelpdesk.domain.WorkflowStep;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.dao.support.DataAccessUtils;

import java.util.List;

public class HibernateWorkflowStepStatusDao extends HibernateDaoSupport implements WorkflowStepStatusDao {
    public WorkflowStepStatus getById(Integer id) {
        return (WorkflowStepStatus) getHibernateTemplate().get(WorkflowStepStatus.class, id);
    }

    public void save(WorkflowStepStatus workflowStepStatus) {
        getHibernateTemplate().saveOrUpdate(workflowStepStatus);
    }

    public void delete(WorkflowStepStatus workflowStepStatus) {
        getHibernateTemplate().delete(workflowStepStatus);
    }

    @SuppressWarnings("unchecked")
    public List<WorkflowStepStatus> getIncompleteSteps() {
        return getHibernateTemplate().find("from WorkflowStepStatus w where w.completedDate is null and w.workflowStep.stepCompletedStatus is not null");
    }

    public int getCountByWorkflowStep(WorkflowStep workflowStep) {
        return DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam("select count(wss) from WorkflowStepStatus wss where wss.workflowStep = :workflowStep", "workflowStep", workflowStep));
    }
}
