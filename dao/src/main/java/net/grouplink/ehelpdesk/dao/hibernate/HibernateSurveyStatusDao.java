package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.SurveyStatusDao;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Feb 27, 2008
 * Time: 11:45:32 AM
 */
public class HibernateSurveyStatusDao extends HibernateDaoSupport implements SurveyStatusDao {
}
