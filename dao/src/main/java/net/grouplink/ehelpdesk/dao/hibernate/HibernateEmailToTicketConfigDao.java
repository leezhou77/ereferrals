package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.EmailToTicketConfigDao;
import net.grouplink.ehelpdesk.domain.EmailToTicketConfig;
import net.grouplink.ehelpdesk.domain.Group;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.dao.support.DataAccessUtils;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: Jan 4, 2008
 * Time: 11:01:03 AM
 */
public class HibernateEmailToTicketConfigDao extends HibernateDaoSupport implements EmailToTicketConfigDao {

    @SuppressWarnings("unchecked")
    public List<EmailToTicketConfig> getAll() {
        return getHibernateTemplate().loadAll(EmailToTicketConfig.class);
    }

    @SuppressWarnings("unchecked")
    public List<EmailToTicketConfig> getAllEnabled() {
        return getHibernateTemplate().find("from EmailToTicketConfig as et where et.enabled = true");
    }

    public EmailToTicketConfig getById(Integer id) {
        List list = getHibernateTemplate().findByNamedParam("from EmailToTicketConfig as et where et.id = :id", "id", id);
        if (list.size() > 0) {
            return (EmailToTicketConfig) list.get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public int getEmailToTicketConfigCountByPriorityId(Integer priorityId) {
        return DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam("select count(*) from EmailToTicketConfig ettc where ettc.ticketPriority.id = :priorityId", "priorityId", priorityId));
    }


    public void save(EmailToTicketConfig emailToTicketConfig) {
        getHibernateTemplate().saveOrUpdate(emailToTicketConfig);
    }

    public void delete(EmailToTicketConfig emailToTicketConfig) {
        getHibernateTemplate().delete(emailToTicketConfig);
    }

    public EmailToTicketConfig getByGroup(Group group) {
        List list = getHibernateTemplate().find("from EmailToTicketConfig as et where et.group = ?", group);
        if (list.size() > 0) {
            return (EmailToTicketConfig) list.get(0);
        }
        return null;
    }

    public int getCountByGroup(Group group) {
        return DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam(
                "select count(*) from EmailToTicketConfig as ettc where ettc.group = :group", "group", group));
    }
}
