package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.Report;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;

import java.util.List;

public interface ReportDao {
    Report getById(Integer id);
    List<Report> getByUser(User user);
    List<Report> getPublicMinusUser(User user);
    void delete(Report report);
    void flush();
    void save(Report report);
    int getUsageCount(Report report);
    List<Report> getByTicketFilter(TicketFilter ticketFilter);
}
