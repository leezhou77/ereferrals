package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.WorkflowStep;

public interface WorkflowStepDao {
    WorkflowStep getById(Integer workflowStepId);

    void delete(WorkflowStep ws);
}
