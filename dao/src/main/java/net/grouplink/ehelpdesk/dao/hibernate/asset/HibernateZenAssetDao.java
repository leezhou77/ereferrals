package net.grouplink.ehelpdesk.dao.hibernate.asset;

import net.grouplink.ehelpdesk.dao.asset.ZenAssetDao;
import net.grouplink.ehelpdesk.domain.asset.ZenAsset;
import net.grouplink.ehelpdesk.domain.zen.ZenAssetSearch;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Apr 5, 2009
 * Time: 10:55:27 PM
 */
public class HibernateZenAssetDao extends HibernateDaoSupport implements ZenAssetDao {

    public ZenAsset getById(Integer id) {
        return (ZenAsset) getHibernateTemplate().get(ZenAsset.class, id);
    }

    public void save(ZenAsset zenAsset) {
        getHibernateTemplate().saveOrUpdate(zenAsset);
    }

    @SuppressWarnings("unchecked")
    public List<ZenAsset> getAll() {
        return getHibernateTemplate().find("from ZenAsset");
    }

    public void delete(ZenAsset zenAsset) {
        getHibernateTemplate().delete(zenAsset);
    }

    public ZenAsset getByWorkstationId(byte[] workstationId) {
        return (ZenAsset) DataAccessUtils.singleResult(getHibernateTemplate().findByNamedParam("from ZenAsset z where z.workstationId = :workstationId", "workstationId", workstationId));
    }

    @SuppressWarnings("unchecked")
    public List<ZenAsset> search(ZenAssetSearch zas) {
        Criteria c = getSession().createCriteria(ZenAsset.class);
        if (StringUtils.isNotBlank(zas.getAssetName())) {
            c.add(Restrictions.ilike("assetName", zas.getAssetName(), MatchMode.ANYWHERE));
        }

        if (StringUtils.isNotBlank(zas.getAssetType())) {
            c.add(Restrictions.ilike("assetType", zas.getAssetType(), MatchMode.ANYWHERE));
        }

        if (StringUtils.isNotBlank(zas.getOperatingSystem())) {
            c.add(Restrictions.ilike("operatingSystem", zas.getOperatingSystem(), MatchMode.ANYWHERE));
        }

        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return c.list();
    }
}
