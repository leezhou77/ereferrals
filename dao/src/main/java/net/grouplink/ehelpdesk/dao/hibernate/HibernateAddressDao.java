package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.AddressDao;

import net.grouplink.ehelpdesk.domain.Address;
import net.grouplink.ehelpdesk.domain.Phone;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class HibernateAddressDao extends HibernateDaoSupport implements AddressDao {

    public void saveAddress(Address address) {
        getHibernateTemplate().saveOrUpdate(address);
    }

    public void deleteAddress(Address address) {
        getHibernateTemplate().delete(address);
    }

    public void flush() {
        getHibernateTemplate().flush();
    }

    public Address getById(int addressId) {
        return getHibernateTemplate().get(Address.class, addressId);
    }
}
