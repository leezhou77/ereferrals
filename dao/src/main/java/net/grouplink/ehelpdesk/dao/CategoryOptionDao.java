package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;

import java.util.List;

/**
 * @author mrollins
 * @version 1.0
 */
public interface CategoryOptionDao {

    public List<CategoryOption> getCategoryOptions();
    public CategoryOption getCategoryOptionById(Integer id);
    public CategoryOption getCategoryOptionByName(String name);
    public void saveCategoryOption(CategoryOption catOption);
    public void deleteCategoryOption(CategoryOption catOption);
    public List<CategoryOption> getCategoryOptionByGroupId(Integer groupId);
    public CategoryOption getCategoryOptionByNameAndCategoryId(String catOptName, Category category);
    public CategoryOption getDuplicateCategoryOption(CategoryOption catOpt);

    public List<CategoryOption> getCategoryOptionByCategoryId(Integer categoryId);

    public void flush();

    public CategoryOption getCategoryOptionByNameAndGroupId(String catOptName, Integer groupId);

    void justDeleteCategoryOption(CategoryOption categoryOption);
}
