package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.AssignmentDao;
import net.grouplink.ehelpdesk.dao.CategoryOptionDao;
import net.grouplink.ehelpdesk.dao.TicketDao;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * @author mrollins
 * @version 1.0
 */
public class HibernateCategoryOptionDao extends HibernateDaoSupport implements CategoryOptionDao {

    private TicketDao ticketDao;
    private AssignmentDao assignmentDao;

    @SuppressWarnings("unchecked")
    public List<CategoryOption> getCategoryOptions() {
        return getHibernateTemplate().find("select co from CategoryOption as co where co.active = true");
    }

    public CategoryOption getCategoryOptionById(Integer id) {
        List catOpts = getHibernateTemplate().findByNamedParam("select co from CategoryOption as co where co.id=:ID", "ID", id);
        if (catOpts.size() > 0) {
            return (CategoryOption) catOpts.get(0);
        }
        return null;
    }

    public CategoryOption getCategoryOptionByName(String name) {
        List catOpts = getHibernateTemplate().findByNamedParam("from CategoryOption as c where c.name=:NAME", "NAME", name);
        if (catOpts.size() > 0) {
            return (CategoryOption) catOpts.get(0);
        }
        return null;
    }

    public void saveCategoryOption(CategoryOption catOption) {
        getHibernateTemplate().saveOrUpdate(catOption);
    }

    public void deleteCategoryOption(CategoryOption catOption) {
        if(catOption.getCustomFields().size() > 0 || ticketDao.getTicketCountByCatOptId(catOption.getId()) > 0){
            catOption.setActive(false);
//            catOption.getTechLocationCategoryOptions().clear();
            List techLocCatOptList = assignmentDao.getByCatOpt(catOption);
            assignmentDao.deleteList(techLocCatOptList);
            getHibernateTemplate().saveOrUpdate(catOption);
        }
        else{
            List techLocCatOptList = assignmentDao.getByCatOpt(catOption);
            assignmentDao.deleteList(techLocCatOptList);
            getHibernateTemplate().delete(catOption);
        }
                        
    }

    @SuppressWarnings("unchecked")
    public List<CategoryOption> getCategoryOptionByGroupId(Integer groupId) {
        return (List<CategoryOption>) getHibernateTemplate().findByNamedParam("select co from CategoryOption as co where co.category.group.id= :groupId and  co.active = true order by co.name asc", "groupId", groupId);
    }

    public CategoryOption getCategoryOptionByNameAndCategoryId(String catOptName, Category category) {
        List catOptList = getHibernateTemplate().findByNamedParam("select catOpt from CategoryOption as catOpt where catOpt.name = :catOptName and catOpt.category = :category", new String[]{"catOptName", "category"}, new Object[]{catOptName, category});
		if(catOptList.size() > 0){
			return (CategoryOption) catOptList.get(0);
		}
		return null;
    }

    public CategoryOption getDuplicateCategoryOption(CategoryOption catOpt) {
        //List catOptList = getHibernateTemplate().findByNamedParam("select catOpt from CategoryOption as catOpt where catOpt.name = :catOptName and catOpt.category.id = :categoryId and catOpt.id != :catOptId", new String[]{"catOptName", "categoryId", "catOptId"}, new Object[]{catOpt.getName(), catOpt.getCategory().getId(), catOpt.getId()});
    	List catOptList = getHibernateTemplate().findByNamedParam("select catOpt from CategoryOption as catOpt where catOpt.name = :catOptName and catOpt.category.id = :categoryId and  catOpt.active = true", new String[]{"catOptName", "categoryId"}, new Object[]{catOpt.getName(), catOpt.getCategory().getId()});
        if(catOptList.size() > 0){
			return (CategoryOption) catOptList.get(0);
		}
		return null;
    }

    @SuppressWarnings("unchecked")
    public List<CategoryOption> getCategoryOptionByCategoryId(Integer categoryId) {
        return getHibernateTemplate().findByNamedParam("select co from CategoryOption as co where co.category.id= :categoryId and co.active = true order by co.name asc", "categoryId", categoryId);
    }

    public void flush() {
        getHibernateTemplate().flush();
    }

    @SuppressWarnings("unchecked")
    public CategoryOption getCategoryOptionByNameAndGroupId(String catOptName, Integer groupId) {
        List<CategoryOption> status = (List<CategoryOption>) getHibernateTemplate().findByNamedParam("select c from CategoryOption as c where c.name = :catOptName and c.category.group.id = :groupId", new String[]{"catOptName", "groupId"}, new Object[]{catOptName, groupId});
        if (status.size() > 0) {
            return status.get(0);
        }
        return null;
    }

    public void justDeleteCategoryOption(CategoryOption categoryOption) {
        getHibernateTemplate().delete(categoryOption);
    }

    public void setTicketDao(TicketDao ticketDao) {
        this.ticketDao = ticketDao;
    }

    public void setAssignmentDao(AssignmentDao assignmentDao) {
        this.assignmentDao = assignmentDao;
    }
}
