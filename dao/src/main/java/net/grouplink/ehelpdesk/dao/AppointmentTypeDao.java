package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.AppointmentType;

import java.util.List;

/**
 * @author mrollins
 * @version 1.0
 */
public interface AppointmentTypeDao {
    public List<AppointmentType> getApptTypes();
    public AppointmentType getApptTypeById(Integer id);
    public AppointmentType getApptTypeByName(String name);
    public void saveApptType(AppointmentType apptType);
    public void deleteApptType(AppointmentType apptType);
}
