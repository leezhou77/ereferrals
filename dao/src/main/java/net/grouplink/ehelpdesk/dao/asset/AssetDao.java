package net.grouplink.ehelpdesk.dao.asset;

import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.asset.AccountingInfo;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.AssetType;
import net.grouplink.ehelpdesk.domain.asset.AssetStatus;
import net.grouplink.ehelpdesk.domain.asset.Vendor;
import net.grouplink.ehelpdesk.common.utils.AssetSearch;

import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: dgarcia
 * Date: Aug 27, 2007
 * Time: 3:40:23 PM
 */
public interface AssetDao {
    public List<Asset> getAllAssets();

    public Asset getAssetById(Integer id);

    public void saveOrUpdate(Object asset);

    public void delete(Object asset);

    public List<Asset> searchAssets(AssetSearch assetSearch);

    int getAssetCount(String searchText);

    List<Asset> searchAssets(String searchText, int start, int count, Map<String, Boolean> sortFieldMap);

    public Asset getAssetByAssetNumber(String assetNumber);

    List<AssetType> getUniqueTypes();

    List<AssetStatus> getUniqueStatus();

    List<Vendor> getUniqueVendors();

    Asset getByAssetNumber(String assetNumber);

    void flush();

    List<Asset> getAssetsWithLeaseExpirationDate();

    List<Asset> getAssetsWithWarrantyExpirationDate();

    List<Asset> getAssetsWithAcquisitionDate();

    int getCountByGroup(Group group);

    int getCountByCategory(Category category);

    int getCountByCategoryOption(CategoryOption categoryOption);

    List<AccountingInfo> getAllAccountingInfo(int start, int count);

    void removeAssetAccountingInfoForeignKey();

    int getAccountingInfoCount();

    void deleteAccountingInfoList(List<AccountingInfo> accountingInfos);
}
