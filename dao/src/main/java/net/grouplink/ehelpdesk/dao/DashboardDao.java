package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.dashboard.Dashboard;

import java.util.List;

public interface DashboardDao {
    List<Dashboard> getAll();

    Dashboard getById(Integer id);

    void save(Dashboard dashboard);

    void delete(Dashboard dashboard);

    void flush();
}
