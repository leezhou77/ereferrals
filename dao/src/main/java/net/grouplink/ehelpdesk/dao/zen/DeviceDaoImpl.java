package net.grouplink.ehelpdesk.dao.zen;

import net.grouplink.ehelpdesk.domain.zen.Device;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * User: jaymehafen
 * Date: Apr 1, 2009
 * Time: 2:44:20 PM
 */
public class DeviceDaoImpl extends HibernateDaoSupport implements DeviceDao {

    @SuppressWarnings("unchecked")
    public List<Device> getAll() {
        return getHibernateTemplate().find("from Device");
    }
}
