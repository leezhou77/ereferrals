package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.TicketScheduleStatusDao;
import net.grouplink.ehelpdesk.domain.TicketScheduleStatus;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.ScheduleStatus;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * @author mrollins
 * @version 1.0
 */
public class HibernateTicketScheduleStatusDao extends HibernateDaoSupport implements TicketScheduleStatusDao {

    @SuppressWarnings("unchecked")
    public List<TicketScheduleStatus> getTicketScheduleStatus() {
        return (List<TicketScheduleStatus>) getHibernateTemplate().find("select tss from TicketScheduleStatus as tss");
    }

    @SuppressWarnings("unchecked")
    public TicketScheduleStatus getTSSById(Integer id) {
        List<TicketScheduleStatus> tsss = (List<TicketScheduleStatus>) getHibernateTemplate().findByNamedParam("select tss from TicketScheduleStatus as tss where tss.id=:TSSID", "TSSID", id);
		if(tsss.size() == 1){
			return tsss.get(0);
		}
		return null;
    }

    @SuppressWarnings("unchecked")
    public TicketScheduleStatus getTSSByTicket(Ticket ticket, ScheduleStatus scheduleStatus) {
        List<TicketScheduleStatus> tsss = (List<TicketScheduleStatus>) getHibernateTemplate().findByNamedParam("select tss from TicketScheduleStatus as tss where tss.ticket=:TIKET and tss.scheduleStatus=:SCHEDSTAT",
                new String[]{"TIKET", "SCHEDSTAT"}, new Object[]{ticket, scheduleStatus});
		if(tsss.size() > 0){
			return tsss.get(0);
		}
		return null;
    }

    public void saveTSS(TicketScheduleStatus ticketScheduleStatus) {
        getHibernateTemplate().saveOrUpdate(ticketScheduleStatus);
    }

    public void deleteTSS(TicketScheduleStatus ticketScheduleStatus) {
        getHibernateTemplate().delete(ticketScheduleStatus);
    }

    public void flush() {
        getHibernateTemplate().flush();
    }
}
