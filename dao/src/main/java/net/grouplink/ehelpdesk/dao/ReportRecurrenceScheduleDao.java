package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.Report;
import net.grouplink.ehelpdesk.domain.RecurrenceSchedule;
import net.grouplink.ehelpdesk.domain.ReportRecurrenceSchedule;

public interface ReportRecurrenceScheduleDao {

    ReportRecurrenceSchedule getByReportAndSchedule(Report report, RecurrenceSchedule schedule);

    void save(ReportRecurrenceSchedule reportRecurrenceSchedule);

    void delete(ReportRecurrenceSchedule reportRecurrenceSchedule);
}
