package net.grouplink.ehelpdesk.dao.asset;

import net.grouplink.ehelpdesk.domain.asset.SoftwareLicense;
import net.grouplink.ehelpdesk.domain.asset.Vendor;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Jul 8, 2008
 * Time: 4:35:08 PM
 */
public interface SoftwareLicenseDao {
    SoftwareLicense getById(Integer id);

    List<SoftwareLicense> getAllSoftwareLicenses();

    List<Vendor> getUniqueVendors();

    void saveSoftwareLicense(SoftwareLicense softwareLicense);

    void delete(SoftwareLicense softwareLicense);

    void flush();
}
