package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.TicketTemplateRecurrenceScheduleDao;
import net.grouplink.ehelpdesk.domain.TicketTemplateRecurrenceSchedule;
import net.grouplink.ehelpdesk.domain.TicketTemplateMaster;
import net.grouplink.ehelpdesk.domain.RecurrenceSchedule;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.dao.support.DataAccessUtils;

public class HibernateTicketTemplateRecurrenceScheduleDao extends HibernateDaoSupport implements TicketTemplateRecurrenceScheduleDao {

    public void save(TicketTemplateRecurrenceSchedule ticketTemplateRecurrenceSchedule) {
        getHibernateTemplate().saveOrUpdate(ticketTemplateRecurrenceSchedule);
    }

    public void delete(TicketTemplateRecurrenceSchedule ticketTemplateRecurrenceSchedule) {
        getHibernateTemplate().delete(ticketTemplateRecurrenceSchedule);
    }

    public TicketTemplateRecurrenceSchedule getByTicketTemplateMasterAndSchedule(TicketTemplateMaster ticketTemplateMaster, RecurrenceSchedule schedule) {
        return (TicketTemplateRecurrenceSchedule) DataAccessUtils.singleResult(getHibernateTemplate()
                .findByNamedParam("from TicketTemplateRecurrenceSchedule ttrs where ttrs.ticketTemplateMaster = :templateMaster and ttrs.recurrenceSchedule = :recurrenceSchedule",
                new String[]{"templateMaster", "recurrenceSchedule"}, new Object[]{ticketTemplateMaster, schedule}));
    }
}
