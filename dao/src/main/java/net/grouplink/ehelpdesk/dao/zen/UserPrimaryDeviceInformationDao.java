package net.grouplink.ehelpdesk.dao.zen;

import net.grouplink.ehelpdesk.domain.zen.UserPrimaryDeviceInformation;

import java.util.List;

public interface UserPrimaryDeviceInformationDao {
    List<UserPrimaryDeviceInformation> getByZuid(byte[] zuid);
}
