package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.AdvancedTicketsFilter;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.EmailToTicketConfig;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Status;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketPriority;
import net.grouplink.ehelpdesk.domain.TicketTemplateLaunch;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.domain.TicketTemplate;
import net.grouplink.ehelpdesk.domain.asset.ZenAsset;

import java.util.List;
import java.util.Set;

public interface TicketDao {

	List<Ticket> getTickets();

	Ticket getTicketById(Integer id);

	List<Ticket> getTicketsByOwner(User user, Boolean showClosed);

	Integer getTicketsByOwnerCount(User user);

    Integer getTicketsByUrgIdCount(Integer urgId);

    /**
     * This method is only to be called by ticketService to make sure tickets get final modified date stamp
     * and status time table added to the ticket
     * If you need to save a ticket always call ticketService.saveTicket(Ticket ticket)
     * @param ticket Entity object of type Ticket
     */
    void saveServiceTicket(Ticket ticket);

	void deleteTicket(Ticket ticket);

    List<Ticket> getTicketsAssignedToByUserId(Integer userId);

	List<Ticket> getTicketsAssignedToByUserRoleGroup(UserRoleGroup userRoleGroup, Boolean showClosed);

	List<Ticket> getAllGroupTicketsByGroupId(Integer groupId, Boolean showClosed);

    List<Ticket> getTicketsByCriteria(Group group, Set<Status> statuses, Set<TicketPriority> priorities, Category category, CategoryOption categoryOption);

    List<Ticket> getTicketsByAdvancedSearch(AdvancedTicketsFilter filter);

    void flush();

    List<Ticket> getTicketsByTicketPool(Integer urgId, Boolean showClosed);
    
    Ticket getTicketByOldTicketIdAndGroupId(Integer oldTicketId, Integer groupId);

    void clear();

    int getTicketCountByGroupId(Integer groupId);

    int getTicketCountByCatOptId(Integer catOptId);

    int getTicketCountByStatusId(Integer statusId);

    int getTicketCountByPriorityId(Integer priorityId);

    List<Ticket> getAllTicketsById(Integer ticketId);

    int cleanUpDummyTickets();

    List<Ticket> getTicketsByEmailToTicketConfig(EmailToTicketConfig emailToTicketConfig);

    Ticket getTicketByOldTicketId(Integer ticketId);

    List<Ticket> getTicketsByZenAsset(ZenAsset zenAsset);

    List<Ticket> getTopLevelTicketsByTicketTemplateLaunch(TicketTemplateLaunch ttl);

    List<Ticket> getTicketsByTicketTemplate(TicketTemplate ticketTemplate);

    int getCountByCategory(Category category);

    int getCountByCategoryOption(CategoryOption categoryOption);

    void evict(Ticket ticket);
}
