package net.grouplink.ehelpdesk.dao.hibernate;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Order;
import net.grouplink.ehelpdesk.dao.StatusDao;
import net.grouplink.ehelpdesk.domain.Status;

import java.util.List;

/**
 * @author mrollins
 * @version 1.0
 */
public class HibernateStatusDao extends HibernateDaoSupport implements StatusDao {

    @SuppressWarnings("unchecked")
    public List<Status> getStatus() {
        return getHibernateTemplate().find("select s from Status as s where s.id > -1 and (s.active is null or s.active = true) order by s.order");
    }

    public Status getStatusById(Integer id) {
        List status = getHibernateTemplate().findByNamedParam("select s from Status as s where s.id=:STATUSID", "STATUSID", id);
        if(status.size() > 0) {
			return (Status) status.get(0);
		}

        return null;
    }

    public Status getStatusByName(String name) {
        List status = getHibernateTemplate().findByNamedParam("select s from Status as s where s.name=:NAME and (s.active is null or s.active = true)", "NAME", name);
		if(status.size() == 1){
			return (Status) status.get(0);
		}

        return null;
    }

    public void saveStatus(Status status) {
        getHibernateTemplate().saveOrUpdate(status);
    }

    public void deleteStatus(Status status) {
        //Preventing from deleting status with id -1, 1, 2 and 6
        if(status.getId() > 2 && status.getId() != 6){
            getHibernateTemplate().delete(status);
        }
    }

    public Status getInitialStatus() {
        List list = getHibernateTemplate().find("select s from Status as s where s.id = 2");
		if(list.size() > 0){
			return (Status) list.get(0);
		}
		return null;
    }

    public Status getTicketPoolStatus() {
        List list = getHibernateTemplate().find("select s from Status as s where s.id = 1");
		if(list.size() > 0){
			return (Status) list.get(0);
		}
		return null;
    }

    public Status getClosedStatus() {
        List list = getHibernateTemplate().find("select s from Status as s where s.id = 6");
		if(list.size() > 0){
			return (Status) list.get(0);
		}
		return null;
    }

    @SuppressWarnings("unchecked")
    public List<Status> getAllStatuses() {
        return getHibernateTemplate().find("from Status status where (status.active is null or status.active = true) and status.id > 0 order by status.order");
    }

    @SuppressWarnings("unchecked")
    public List<Status> getCustomStatus() {
        Criteria cri = getSessionFactory().getCurrentSession().createCriteria(Status.class);

        cri.add(Restrictions.ne("id", Integer.valueOf("-2")));
        cri.add(Restrictions.ne("id", Integer.valueOf("-1")));
        cri.add(Restrictions.ne("id", Integer.valueOf("1")));
        cri.add(Restrictions.ne("id", Integer.valueOf("2")));
        cri.add(Restrictions.ne("id", Integer.valueOf("6")));

        cri.add(Restrictions.or(Restrictions.isNull("active"), Restrictions.eq("active", Boolean.TRUE)));

        cri.addOrder(Order.asc("order"));
        cri.addOrder(Order.asc("id"));
        return cri.list();
    }

    @SuppressWarnings("unchecked")
    public List<Status> getNonClosedStatus() {
        return getHibernateTemplate().find("select s from Status as s where s.id != 6 and s.id != -1 and s.id != -2 and (s.active is null or s.active = true) order by s.order");
    }

    public void flush() {
        getHibernateTemplate().flush();
    }

    public void saveOnly(Status status) {
        // TODO: consider using separate domain object with 'assigned' id generator OR
        // TODO: consider custom id generator that allows the assinged id or other (increment)
        getHibernateTemplate().getSessionFactory().getCurrentSession().save(status, status.getId());
    }
}
