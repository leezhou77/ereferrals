package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.Phone;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: 1/9/12
 * Time: 5:18 PM
 */
public interface PhoneDao {
    void savePhone(Phone phone);
    void deletePhone(Phone phone);
    void flush();

    Phone getById(int phoneId);
}
