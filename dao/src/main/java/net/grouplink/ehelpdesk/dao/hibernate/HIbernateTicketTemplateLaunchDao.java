package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.TicketTemplateLaunchDao;
import net.grouplink.ehelpdesk.domain.TicketTemplateLaunch;
import net.grouplink.ehelpdesk.domain.TicketTemplateMaster;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.dao.support.DataAccessUtils;

import java.util.List;
import java.util.Date;

public class HIbernateTicketTemplateLaunchDao extends HibernateDaoSupport implements TicketTemplateLaunchDao {

    public TicketTemplateLaunch getById(Integer id) {
        return (TicketTemplateLaunch) getHibernateTemplate().get(TicketTemplateLaunch.class, id);
    }

    public void save(TicketTemplateLaunch ticketTemplateLaunch) {
        getHibernateTemplate().saveOrUpdate(ticketTemplateLaunch);
    }

    public void delete(TicketTemplateLaunch ticketTemplateLaunch) {
        getHibernateTemplate().delete(ticketTemplateLaunch);
    }

    public int getLaunchCountByTemplateMaster(TicketTemplateMaster ttm) {
        return DataAccessUtils.intResult(getHibernateTemplate()
                .findByNamedParam("select count(ttl) from TicketTemplateLaunch ttl where ttl.ticketTemplateMaster = :ttm", "ttm", ttm));
    }

    @SuppressWarnings("unchecked")
    public List<TicketTemplateLaunch> getPendingOlderThan(Date activeDate) {
        return getHibernateTemplate().findByNamedParam("from TicketTemplateLaunch ttl where ttl.status = :pendingStatus and ttl.createdDate <= :createdDate",
                new String[]{"pendingStatus", "createdDate"}, new Object[]{TicketTemplateLaunch.STATUS_PENDING, activeDate});
    }
}
