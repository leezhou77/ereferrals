package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.GroupDao;
import net.grouplink.ehelpdesk.dao.TicketFilterDao;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Role;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRole;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.domain.Status;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilterOperators;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: Feb 20, 2008
 * Time: 7:15:31 PM
 */
public class HibernateTicketFilterDao extends HibernateDaoSupport implements TicketFilterDao {
    private final Log log = LogFactory.getLog(HibernateTicketFilterDao.class);
    private SimpleDateFormat filterDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private GroupDao groupDao;

    @SuppressWarnings("unchecked")
    public List<TicketFilter> getAll() {
        return (List<TicketFilter>) getHibernateTemplate().loadAll(TicketFilter.class);
    }

    @SuppressWarnings("unchecked")
    public List<TicketFilter> getAllPublic() {
        return (List<TicketFilter>) getHibernateTemplate().find("from TicketFilter as tf where tf.privateFilter = false order by tf.name");
    }

    @SuppressWarnings("unchecked")
    public List<TicketFilter> getByUser(User user) {
        return (List<TicketFilter>) getHibernateTemplate().findByNamedParam("from TicketFilter as tf where tf.user = :user order by tf.name", "user", user);
    }

    @SuppressWarnings("unchecked")
    public TicketFilter getById(Integer id) {
        List<TicketFilter> list = (List<TicketFilter>) getHibernateTemplate().findByNamedParam("from TicketFilter as tf where tf.id = :id", "id", id);
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    public void save(TicketFilter ticketFilter) {
        getHibernateTemplate().saveOrUpdate(ticketFilter);
    }

    public void delete(TicketFilter ticketFilter) {
        getHibernateTemplate().delete(ticketFilter);
    }

    @SuppressWarnings("unchecked")
    public List<Ticket> getTickets(TicketFilter f, User user, Map<Group, Boolean> hasViewTicketsPermissions) {
        return getTickets(f, user, -1, -1, null, hasViewTicketsPermissions);
    }

    @SuppressWarnings("unchecked")
    public List<Ticket> getTickets(TicketFilter f, User user, int firstResult, int maxResults, String sort, Map<Group, Boolean> hasViewTicketsPermissions) {
        Criteria c = getTicketFilterCriteria(f, user, hasViewTicketsPermissions);
        if (firstResult > -1) {
            c.setFirstResult(firstResult);
        }

        if (maxResults > -1) {
            c.setMaxResults(maxResults);
        }

        addOrder(c, f, sort);

        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return (List<Ticket>) c.list();
    }

    private void addOrder(Criteria c, TicketFilter f, String sort) {
        // check if sort string is not blank
        if (StringUtils.isNotBlank(sort)) {
            addOrderProperty(c, sort);

            // if sort string is not TicketFilter.COLUMN_TICKETNUMBER, then add "id"
            // order by to guarantee ticket order
            if (!StringUtils.contains(sort, TicketFilter.COLUMN_TICKETNUMBER)) {
                c.addOrder(Order.asc("id"));
            }
        } else if (!ArrayUtils.isEmpty(f.getColumnOrder()) && StringUtils.isNotBlank(f.getColumnOrder()[0])){
            // sort by first property in column order
            addOrderProperty(c, f.getColumnOrder()[0]);

            // if first column is not TicketFilter.COLUMN_TICKETNUMBER, then add "id"
            // order by to guarantee ticket order
            if (!f.getColumnOrder()[0].equals(TicketFilter.COLUMN_TICKETNUMBER)) {
                c.addOrder(Order.asc("id"));
            }
        }
    }

    private void addOrderProperty(Criteria c, String sort ) {
        if (StringUtils.contains(sort, TicketFilter.COLUMN_TICKETNUMBER)) {
            addOrderAscDesc(c, sort, "id");
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_PRIORITY)) {
            c.createAlias("priority", "priority", CriteriaSpecification.LEFT_JOIN);
            addOrderAscDesc(c, sort, "priority.order");
            addOrderAscDesc(c, sort, "priority.name");
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_STATUS)) {
            addOrderAscDesc(c, sort, "status.order");
            addOrderAscDesc(c, sort, "status.name");
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_SUBJECT)) {
            addOrderAscDesc(c, sort, "subject");
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_NOTE)) {
            addOrderAscDesc(c, sort, "note");
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_WORKTIME)) {
            addOrderAscDesc(c, sort, "workTime");
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_CREATED)) {
            addOrderAscDesc(c, sort, "createdDate");
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_MODIFIED)) {
            addOrderAscDesc(c, sort, "modifiedDate");
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_ESTIMATED)) {
            addOrderAscDesc(c, sort, "estimatedDate");
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_CONTACT)) {
            c.createAlias("contact", "contact", CriteriaSpecification.LEFT_JOIN);
            addOrderAscDesc(c, sort, "contact.lastName");
            addOrderAscDesc(c, sort, "contact.firstName");
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_SUBMITTED)) {
            c.createAlias("submittedBy", "submittedBy", CriteriaSpecification.LEFT_JOIN);
            addOrderAscDesc(c, sort, "submittedBy.lastName");
            addOrderAscDesc(c, sort, "submittedBy.firstName");
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_ASSIGNED)) {
            // assignedto aliases already created
            addOrderAscDesc(c, sort, "user.lastName");
            addOrderAscDesc(c, sort, "user.firstName");
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_LOCATION)) {
            c.createAlias("location", "location", CriteriaSpecification.LEFT_JOIN);
            addOrderAscDesc(c, sort, "location.name");
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_GROUP)) {
            c.createAlias("group", "group", CriteriaSpecification.LEFT_JOIN);
            addOrderAscDesc(c, sort, "group.name");
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_CATEGORY)) {
            c.createAlias("category", "category", CriteriaSpecification.LEFT_JOIN);
            addOrderAscDesc(c, sort, "category.name");
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_CATOPT)) {
            c.createAlias("categoryOption", "categoryOption", CriteriaSpecification.LEFT_JOIN);
            addOrderAscDesc(c, sort, "categoryOption.name");
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_CUSTOMFIELD)) {

        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_SURVEYS)) {

        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_ZENWORKS10ASSET)) {
//            sortProp = "zenAsset";
//            addOrderAscDesc(c, sort, "priority.order");
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_INTERNALASSET)) {
            c.createAlias("asset", "asset", CriteriaSpecification.LEFT_JOIN);
            addOrderAscDesc(c, sort, "asset.assetNumber");
        }
    }

    private void addOrderAscDesc(Criteria c, String sort, String sortProp) {
        if (sort.startsWith("-"))
            c.addOrder(Order.desc(sortProp));
        else
            c.addOrder(Order.asc(sortProp));
    }

    @SuppressWarnings("unchecked")
    public Integer getTicketCount(TicketFilter f, User user, Map<Group, Boolean> hasViewTicketsPermissions) {
        Criteria c = getTicketFilterCriteria(f, user, hasViewTicketsPermissions);
        c.setProjection(Projections.rowCount());
        List results = c.list();
        Long countLong = (Long) results.get(0);
        return safeLongToInt(countLong);
    }

    private static int safeLongToInt(long l) {
        if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
            throw new IllegalArgumentException(l + " cannot be cast to int without changing its value.");
        }
        return (int) l;
    }


    public int getUsageCount(TicketFilter ticketFilter) {
        int count = 0;

        // check dashboard widgets
        count += DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam("select count(w) from Widget w where w.report.ticketFilter = :tf", "tf", ticketFilter));
        // check reports
        count += DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam("select count(r) from Report r where r.ticketFilter = :tf", "tf", ticketFilter));
        // check mytickettab
        count += DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam("select count(m) from MyTicketTab m where m.ticketFilter = :tf", "tf", ticketFilter));

        return count;
    }

    public void flush() {
        getHibernateTemplate().flush();
    }

    @SuppressWarnings("unchecked")
    public List<TicketFilter> getPublicPlusUser(User user) {
        return getHibernateTemplate().findByNamedParam("from TicketFilter tf where tf.privateFilter = false or tf.user = :user order by tf.name", "user", user);
    }

    @SuppressWarnings("unchecked")
    public List<TicketFilter> getByVersionLessThan(Integer version) {
        return getHibernateTemplate().findByNamedParam("from TicketFilter tf where tf.tfVersion is null or tf.tfVersion < :version", "version", version);
    }

    private Criteria getTicketFilterCriteria(TicketFilter f, User user, Map<Group, Boolean> hasViewTicketsPermissions) {
        Criteria c = getSessionFactory().getCurrentSession().createCriteria(Ticket.class);

        c.createAlias("status", "status", CriteriaSpecification.LEFT_JOIN);

        // exclude deleted and ttstatus pending tickets
        c.add(Restrictions.disjunction().add(Restrictions.isNull("status"))
                .add(Restrictions.ne("status.id", Status.DELETED)));
        c.add(Restrictions.disjunction().add(Restrictions.isNull("ticketTemplateStatus"))
                .add(Restrictions.ne("ticketTemplateStatus", Ticket.TT_STATUS_PENDING)));

        applyUserCriteriaRestrictions(user, c, hasViewTicketsPermissions);

        addAssignedToCriteria(c, f);
        addCriteriaIfArrayNotEmpty(c, f.getCategoryIds(), f.getCategoryIdsOps(), "category.id");
        addCriteriaIfArrayNotEmpty(c, f.getCategoryOptionIds(), f.getCategoryOptionIdsOps(), "categoryOption.id");
        addCriteriaIfArrayNotEmpty(c, f.getContactIds(), f.getContactIdsOps(), "contact.id");
        addCriteriaIfArrayNotEmpty(c, f.getGroupIds(), f.getGroupIdsOps(), "group.id");
        addCriteriaIfArrayNotEmpty(c, f.getLocationIds(), f.getLocationIdsOps(), "location.id");
        addCriteriaIfArrayNotEmpty(c, f.getPriorityIds(), f.getPriorityIdsOps(), "priority.id");
        addCriteriaIfArrayNotEmpty(c, f.getStatusIds(), f.getStatusIdsOps(), "status.id");
        addCriteriaIfArrayNotEmpty(c, f.getSubmittedByIds(), f.getSubmittedByIdsOps(), "submittedBy.id");
        addCriteriaIfArrayNotEmpty(c, f.getZenAssetIds(), f.getZenAssetIdOps(), "zenAsset.id");
        addCriteriaIfArrayNotEmpty(c, f.getInternalAssetIds(), f.getInternalAssetIdOps(), "asset.id");

        addTicketNumberCriteria(c, f);

        if (!ArrayUtils.isEmpty(f.getSubject())) {
            String propertyName = "subject";
            String[] values = f.getSubject();
            Integer[] ops = f.getSubjectOps();

            addStringCriteria(c, propertyName, values, ops);
        }

        if (!ArrayUtils.isEmpty(f.getHistorySubject())) {
            c.createAlias("ticketHistory", "ticketHistory", Criteria.LEFT_JOIN);

            String propertyName = "ticketHistory.subject";
            String[] values = f.getHistorySubject();
            Integer[] ops = f.getHistorySubjectOps();

            addStringCriteria(c, propertyName, values, ops);
        }

        if (f.getCreatedDateOperator() != null) {
            addDateCriteria(c, "createdDate", f.getCreatedDateOperator(), f.getCreatedDate(), f.getCreatedDate2(),
                    f.getCreatedDateLastX());
        }

        if (f.getEstimatedDateOperator() != null) {
            addDateCriteria(c, "estimatedDate", f.getEstimatedDateOperator(), f.getEstimatedDate(),
                    f.getEstimatedDate2(), f.getEstimatedDateLastX());
        }

        if (f.getModifiedDateOperator() != null) {
            addDateCriteria(c, "modifiedDate", f.getModifiedDateOperator(), f.getModifiedDate(), f.getModifiedDate2(),
                    f.getModifiedDateLastX());
        }

        // work time
        addWorkTimeCriteria(f, c);

        return c;
    }

    private void addWorkTimeCriteria(TicketFilter f, Criteria c) {
        String workTimeOperator = f.getWorkTimeOperator();
        if (StringUtils.isNotBlank(workTimeOperator)) {
            int hours = 0;
            if (StringUtils.isNotBlank(f.getWorkTimeHours())) {
                hours = Integer.parseInt(f.getWorkTimeHours());
            }

            int minutes = 0;
            if (StringUtils.isNotBlank(f.getWorkTimeMinutes())) {
                minutes = Integer.parseInt(f.getWorkTimeMinutes());
            }

            int seconds = (hours * 60 * 60) + (minutes * 60);
            if (workTimeOperator.equals("more")) {
                c.add(Restrictions.ge("workTime", seconds));
            }

            if (workTimeOperator.equals("less")) {
                c.add(Restrictions.lt("workTime", seconds));
            }
        }
    }

    private void addStringCriteria(Criteria criteria, String propertyName, String[] values, Integer[] operators) {
        Disjunction disj = Restrictions.disjunction();
        Conjunction notConj = Restrictions.conjunction();
        for (int i = 0; i < values.length; i++) {
            String n = values[i];
            Integer op = operators[i];
            if (op.equals(TicketFilterOperators.EQUALS)) {
                disj.add(Restrictions.eq(propertyName, n));
            } else if (op.equals(TicketFilterOperators.NOT_EQUALS)) {
                notConj.add(Restrictions.ne(propertyName, n));
            } else if (op.equals(TicketFilterOperators.CONTAINS)) {
                disj.add(Restrictions.ilike(propertyName, n, MatchMode.ANYWHERE));
            } else if (op.equals(TicketFilterOperators.NOT_CONTAINS)) {
                notConj.add(Restrictions.not(Restrictions.ilike(propertyName, n, MatchMode.ANYWHERE)));
            } else if (op.equals(TicketFilterOperators.STARTS_WITH)) {
                disj.add(Restrictions.ilike(propertyName, n, MatchMode.START));
            } else if (op.equals(TicketFilterOperators.ENDS_WIDTH)) {
                disj.add(Restrictions.ilike(propertyName, n, MatchMode.END));
            }
        }

        criteria.add(disj);
        criteria.add(notConj);
    }

    private void addTicketNumberCriteria(Criteria c, TicketFilter f) {
        if (!ArrayUtils.isEmpty(f.getTicketNumbers())) {
            Disjunction disj = Restrictions.disjunction();
            Conjunction notConj = Restrictions.conjunction();
            for (int i = 0; i < f.getTicketNumbers().length; i++) {
                Integer ticketnumber = f.getTicketNumbers()[i];
                Integer op = f.getTicketNumbersOps()[i];
                Disjunction d = Restrictions.disjunction();
                if (op.equals(TicketFilterOperators.EQUALS)) {
                    d.add(Restrictions.eq("id", ticketnumber));
                    d.add(Restrictions.eq("oldTicketId", ticketnumber));
                    disj.add(d);
                } else if (op.equals(TicketFilterOperators.NOT_EQUALS)) {
                    d.add(Restrictions.ne("id", ticketnumber));
                    d.add(Restrictions.ne("oldTicketId", ticketnumber));
                    notConj.add(d);
                } else if (op.equals(TicketFilterOperators.GREATER_THAN)) {
                    d.add(Restrictions.gt("id", ticketnumber));
                    d.add(Restrictions.gt("oldTicketId", ticketnumber));
                    disj.add(d);
                } else if (op.equals(TicketFilterOperators.LESS_THAN)) {
                    d.add(Restrictions.lt("id", ticketnumber));
                    d.add(Restrictions.lt("oldTicketId", ticketnumber));
                    disj.add(d);
                } else {
                    log.error("invalid operator value: " + op);
                }
            }

            c.add(disj);
            c.add(notConj);
        }
    }

    private void addDateCriteria(Criteria c, String propertyName, Integer dateOperator, String date1, String date2, Integer lastXValue) {
        if (dateOperator != null) {
            Date now = Calendar.getInstance().getTime();
            if (dateOperator.equals(TicketFilterOperators.DATE_ANY)) {
                // do nothing
            } else if (dateOperator.equals(TicketFilterOperators.DATE_TODAY)) {
                Date todayStart = DateUtils.truncate(new Date(), Calendar.DATE);
                c.add(Restrictions.ge(propertyName, todayStart));
                Date end = DateUtils.addDays(todayStart, 1);
                c.add(Restrictions.lt(propertyName, end));
            } else if (dateOperator.equals(TicketFilterOperators.DATE_WEEK)) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                Date weekStart = calendar.getTime();
                weekStart = DateUtils.truncate(weekStart, Calendar.DATE);
                c.add(Restrictions.ge(propertyName, weekStart));
                calendar.add(Calendar.DAY_OF_MONTH, 7);
                Date weekEnd = calendar.getTime();
                weekEnd = DateUtils.truncate(weekEnd, Calendar.DATE);
                c.add(Restrictions.lt(propertyName, weekEnd));
            } else if (dateOperator.equals(TicketFilterOperators.DATE_MONTH)) {
                Date monthStart = DateUtils.truncate(new Date(), Calendar.MONTH);
                c.add(Restrictions.ge(propertyName, monthStart));
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MONTH, 1);
                Date monthEnd = calendar.getTime();
                monthEnd = DateUtils.truncate(monthEnd, Calendar.DATE);
                c.add(Restrictions.lt(propertyName, monthEnd));
            } else if (dateOperator.equals(TicketFilterOperators.DATE_ON)) {
                Date selectedDate = parseDate(date1);
                Date start = DateUtils.truncate(selectedDate, Calendar.DATE);
                Date end = DateUtils.addDays(start, 1);
                c.add(Restrictions.ge(propertyName, start));
                c.add(Restrictions.lt(propertyName, end));
            } else if (dateOperator.equals(TicketFilterOperators.DATE_BEFORE)) {
                Date selectedDate = parseDate(date1);
                selectedDate = DateUtils.truncate(selectedDate, Calendar.DATE);
                c.add(Restrictions.lt(propertyName, selectedDate));
            } else if (dateOperator.equals(TicketFilterOperators.DATE_AFTER)) {
                Date selectedDate = parseDate(date1);
                selectedDate = DateUtils.addDays(selectedDate, 1);
                selectedDate = DateUtils.truncate(selectedDate, Calendar.DATE);
                c.add(Restrictions.ge(propertyName, selectedDate));
            } else if (dateOperator.equals(TicketFilterOperators.DATE_RANGE)) {
                Date d1 = parseDate(date1);
                d1 = DateUtils.truncate(d1, Calendar.DATE);
                Date d2 = parseDate(date2);
                d2 = DateUtils.addDays(d2, 1);
                d2 = DateUtils.truncate(d2, Calendar.DATE);
                c.add(Restrictions.gt(propertyName, d1));
                c.add(Restrictions.lt(propertyName, d2));
            } else if (dateOperator.equals(TicketFilterOperators.DATE_LAST_X_HOURS)) {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.HOUR, -(lastXValue));
                Date d1 = cal.getTime();
                c.add(Restrictions.gt(propertyName, d1));
                //for all last X operators, make sure not to include future dates (Ticket # 16569)
                c.add(Restrictions.le(propertyName, now));
            } else if (dateOperator.equals(TicketFilterOperators.DATE_LAST_X_DAYS)) {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -(lastXValue));
                Date d1 = DateUtils.truncate(cal.getTime(), Calendar.DATE);
                c.add(Restrictions.gt(propertyName, d1));
                c.add(Restrictions.le(propertyName, now));
            } else if (dateOperator.equals(TicketFilterOperators.DATE_LAST_X_WEEKS)) {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -(lastXValue * 7));
                Date d1 = DateUtils.truncate(cal.getTime(), Calendar.DATE);
                c.add(Restrictions.gt(propertyName, d1));
                c.add(Restrictions.le(propertyName, now));
            } else if (dateOperator.equals(TicketFilterOperators.DATE_LAST_X_MONTHS)) {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.MONTH, -(lastXValue));
                Date d1 = DateUtils.truncate(cal.getTime(), Calendar.DATE);
                c.add(Restrictions.gt(propertyName, d1));
                c.add(Restrictions.le(propertyName, now));
            } else if (dateOperator.equals(TicketFilterOperators.DATE_LAST_X_YEARS)) {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.YEAR, -(lastXValue));
                Date d1 = DateUtils.truncate(cal.getTime(), Calendar.DATE);
                c.add(Restrictions.gt(propertyName, d1));
                c.add(Restrictions.le(propertyName, now));
            } else {
                log.error("invalid date operator value: " + dateOperator);
            }
        }
    }

    private Date parseDate(String date1) {
        Date selectedDate;
        try {
            selectedDate = filterDateFormat.parse(date1);
        } catch (ParseException e) {
            log.error("error parsing date: " + date1, e);
            throw new RuntimeException(e);
        }

        return selectedDate;
    }

    private void applyUserCriteriaRestrictions(User user, Criteria c, Map<Group, Boolean> hasViewTicketsPermissions) {

        c.createAlias("assignedTo", "assignedTo", Criteria.LEFT_JOIN);
        c.createAlias("assignedTo.userRole", "userRole", Criteria.LEFT_JOIN);
        c.createAlias("userRole.user", "user", Criteria.LEFT_JOIN);

        if (isUserInRole(user, Role.ROLE_ADMINISTRATOR_ID)) {
            // all tickets viewable, no restrictions to be added
            return;
        }

        // groups that user has full view access
        List<Integer> userGroupIds = new ArrayList<Integer>();

        // groups where user has restrictions
        List<Junction> nonAllGroupTicketsConjunctions = new ArrayList<Junction>();

        for (Map.Entry<Group, Boolean> groupBooleanEntry : hasViewTicketsPermissions.entrySet()) {
            Group group = groupBooleanEntry.getKey();
            // check if user has group.viewAllGroupTickets or user has manager role
            if (groupBooleanEntry.getValue() || isUserInRole(user, group, Role.ROLE_MANAGER_ID)) {
                userGroupIds.add(group.getId());
            } else {
                if (isUserInRole(user, group, Role.ROLE_TECHNICIAN_ID)) {
                    Conjunction conj = Restrictions.conjunction();
                    conj.add(Restrictions.eq("group.id", group.getId()));
                    Disjunction dis = Restrictions.disjunction();
                    dis.add(Restrictions.eq("contact.id", user.getId()));
                    dis.add(Restrictions.isNull("assignedTo.userRole"));
                    dis.add(Restrictions.eq("user.id", user.getId()));
                    dis.add(Restrictions.eq("submittedBy.id", user.getId()));
                    conj.add(dis);
                    nonAllGroupTicketsConjunctions.add(conj);
                } else {
                    // show tickets where user is the contact or submittedby
                    Conjunction conj = Restrictions.conjunction();
                    conj.add(Restrictions.eq("group.id", group.getId()));
                    Disjunction dis = Restrictions.disjunction();
                    dis.add(Restrictions.eq("contact.id", user.getId()));
                    dis.add(Restrictions.eq("submittedBy.id", user.getId()));
                    conj.add(dis);
                    nonAllGroupTicketsConjunctions.add(conj);
                }
            }
        }

        Disjunction disj = Restrictions.disjunction();
        if (CollectionUtils.isNotEmpty(userGroupIds))
            disj.add(Restrictions.in("group.id", userGroupIds));

        if (CollectionUtils.isNotEmpty(nonAllGroupTicketsConjunctions)) {
            for (Object nonAllGroupTicketsConjunction : nonAllGroupTicketsConjunctions) {
                Conjunction conj = (Conjunction) nonAllGroupTicketsConjunction;
                disj.add(conj);
            }
        }

        c.add(disj);
    }

    private boolean isUserInRole(User user, Group group, Integer role) {
        for (UserRole userRole : user.getUserRoles()) {
            if (userRole.getRole() != null && userRole.getRole().getId().equals(role)) {
                for (UserRoleGroup userRoleGroup : userRole.getUserRoleGroup()) {
                    if (userRoleGroup.getGroup().getId().equals(group.getId())) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private boolean isUserInRole(User user, Integer role) {
        for (UserRole userRole : user.getUserRoles()) {
            if (userRole.getRole() != null && userRole.getRole().getId().equals(role))
                return true;
        }

        return false;
    }

    private void addAssignedToCriteria(Criteria criteria, TicketFilter ticketFilter) {
        Disjunction disjunction = Restrictions.disjunction();
        boolean addDisjunction = false;
        if (ArrayUtils.isNotEmpty(ticketFilter.getAssignedToIds())) {
            disjunction.add(buildCriteriaForArray(ticketFilter.getAssignedToIds(), ticketFilter.getAssignedToIdsOps(), "assignedTo.id"));
            addDisjunction = true;
        }

        if (ArrayUtils.isNotEmpty(ticketFilter.getAssignedToUserIds())) {
            disjunction.add(buildCriteriaForArray(ticketFilter.getAssignedToUserIds(), ticketFilter.getAssignedToUserIdsOps(), "user.id"));
            addDisjunction = true;
        }

        if (addDisjunction) {
            criteria.add(disjunction);
        }
    }

    private void addCriteriaIfArrayNotEmpty(Criteria c, Object[] ticketFilterArray, Integer[] operators,
                                            String propertyName) {
        if (ArrayUtils.isEmpty(ticketFilterArray)) return;

        Conjunction mainConjunction = buildCriteriaForArray(ticketFilterArray, operators, propertyName);
        c.add(mainConjunction);
    }

    private Conjunction buildCriteriaForArray(Object[] ticketFilterArray, Integer[] operators, String propertyName) {
        Conjunction mainConjunction = Restrictions.conjunction();

        Disjunction equalsDisj = Restrictions.disjunction();
        Conjunction notEqualsConj = Restrictions.conjunction();
        for (int i = 0; i < ticketFilterArray.length; i++) {
            Object o = ticketFilterArray[i];
            Criterion criterion;
            Integer op = operators == null? TicketFilterOperators.EQUALS:operators[i];
            if (op.equals(TicketFilterOperators.EQUALS)) {
                criterion = Restrictions.eq(propertyName, o);
                equalsDisj.add(criterion);
            } else if (op.equals(TicketFilterOperators.NOT_EQUALS)) {
                criterion = Restrictions.ne(propertyName, o);
                notEqualsConj.add(criterion);
            } else {
                log.error("invalid operator value: " + op);
            }
        }

        mainConjunction.add(equalsDisj);
        mainConjunction.add(notEqualsConj);
        return mainConjunction;
    }

    public void setGroupDao(GroupDao groupDao) {
        this.groupDao = groupDao;
    }
}
