package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.dashboard.Widget;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;

import java.util.List;

public interface WidgetDao {

    List<Widget> getAll();

    Widget getById(Integer id);

    void save(Widget widget);

    void delete(Widget widget);

    List<Widget> getByTicketFilter(TicketFilter filter);
}
