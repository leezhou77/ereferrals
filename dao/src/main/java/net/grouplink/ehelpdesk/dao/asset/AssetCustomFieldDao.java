package net.grouplink.ehelpdesk.dao.asset;

import net.grouplink.ehelpdesk.domain.asset.AssetCustomField;
import net.grouplink.ehelpdesk.domain.asset.AssetFieldGroup;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.AssetCustomFieldValue;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Jul 18, 2008
 * Time: 1:29:19 PM
 */
public interface AssetCustomFieldDao {
    AssetCustomField getById(Integer id);
    List<AssetCustomField> getAll();
    List<AssetCustomField> getByAssetFieldGroup(AssetFieldGroup assetFieldGroup);
    List<AssetCustomField> getByAsset(Asset asset);
    void saveAssetCustomField(AssetCustomField assetCustomField);
    void deleteAssetCustomField(AssetCustomField assetCustomField);

    AssetCustomField getByName(String name);

    void flush();

    AssetCustomFieldValue getCustomFieldValue(Asset asset, AssetCustomField assetCustomField);
}
