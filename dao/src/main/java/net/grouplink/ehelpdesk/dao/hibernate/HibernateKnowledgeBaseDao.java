package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.KnowledgeBaseDao;
import net.grouplink.ehelpdesk.domain.Attachment;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.KnowledgeBase;
import net.grouplink.ehelpdesk.domain.User;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.*;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


public class HibernateKnowledgeBaseDao extends HibernateDaoSupport implements KnowledgeBaseDao {

    public KnowledgeBase getKnowledgeBaseById(Integer id) {
        return (KnowledgeBase) DataAccessUtils.singleResult(getHibernateTemplate()
                .findByNamedParam("select kb from KnowledgeBase as kb where kb.id = :id", "id", id));
    }

    @SuppressWarnings("unchecked")
    public List<KnowledgeBase> searchKnowLedgeBase(String searchText, Integer type, Integer groupId, User user) {
        Criteria cri = getSessionFactory().getCurrentSession().createCriteria(KnowledgeBase.class);
        List<String> searchTextList = searchStringToList(new StringBuilder(searchText));
        if (searchTextList.size() == 0) {
            return new ArrayList();
        }

        // only show articles that are not private, or where the group is one of the users groups
        Disjunction dis = Restrictions.disjunction();
        dis.add(Restrictions.eq("isPrivate", Boolean.FALSE));
        if (user != null && CollectionUtils.isNotEmpty(user.getGroups())) {
            dis.add(Restrictions.in("group", user.getGroups()));
        }

        cri.add(dis);

        if (type == 0) {
            for (String aSearchTextArray : searchTextList) {
                cri.add(Restrictions.ilike("subject", aSearchTextArray, MatchMode.ANYWHERE));
            }
        } else if (type == 1) {
            for (String aSearchTextArray : searchTextList) {
                cri.add(Restrictions.or(Restrictions.like("problem", aSearchTextArray, MatchMode.ANYWHERE),
                        Restrictions.like("resolution", aSearchTextArray, MatchMode.ANYWHERE)));
            }
        } else {
            for (String aSearchTextArray : searchTextList) {
                cri.add(Restrictions.or(Restrictions.ilike("subject", aSearchTextArray, MatchMode.ANYWHERE),
                        Restrictions.or(Restrictions.like("problem", aSearchTextArray, MatchMode.ANYWHERE),
                                Restrictions.like("resolution", aSearchTextArray, MatchMode.ANYWHERE))));
            }
        }

        if (groupId != -1) {
            cri.add(Restrictions.eq("group.id", groupId));
        }

        cri.addOrder(Order.desc("timesViewed"));
        return cri.list();
    }

    @SuppressWarnings("unchecked")
    public List<KnowledgeBase> searchKnowLedgeBaseByGroupId(String searchText, Integer type, Integer groupId, User user) {
        Criteria cri = getSessionFactory().getCurrentSession().createCriteria(KnowledgeBase.class);
        List<String> searchTextList = searchStringToList(new StringBuilder(searchText));

        // only show articles that are not private, or where the group is one of the users groups
        Disjunction dis = Restrictions.disjunction();
        dis.add(Restrictions.eq("isPrivate", Boolean.FALSE));
        if (user != null && CollectionUtils.isNotEmpty(user.getGroups())) {
            dis.add(Restrictions.in("group", user.getGroups()));

        }

        cri.add(dis);
        
        if (type == 0) {
            for (String aSearchTextArray : searchTextList) {
                cri.add(Restrictions.ilike("subject", aSearchTextArray, MatchMode.ANYWHERE));
            }
        } else if (type == 1) {
            for (String aSearchTextArray : searchTextList) {
                cri.add(Restrictions.or(Restrictions.like("problem", aSearchTextArray, MatchMode.ANYWHERE),
                        Restrictions.like("resolution", aSearchTextArray, MatchMode.ANYWHERE)));
            }
        } else {
            for (String aSearchTextArray : searchTextList) {
                cri.add(Restrictions.or(Restrictions.ilike("subject", aSearchTextArray, MatchMode.ANYWHERE),
                        Restrictions.or(Restrictions.like("problem", aSearchTextArray, MatchMode.ANYWHERE),
                                Restrictions.like("resolution", aSearchTextArray, MatchMode.ANYWHERE))));
            }
        }

        if (groupId != -1) {
            cri.add(Restrictions.eq("group.id", groupId));
        }
        
        cri.addOrder(Order.desc("timesViewed"));
        return cri.list();
    }

    public List<KnowledgeBase> searchKnowledgeBaseByUser(String searchText, int start, int count, Map<String, Boolean> sortFieldsOrder, User user) {
        DetachedCriteria cri = getKbArticleQueryCriteria(searchText, user);

        for(String sortField : sortFieldsOrder.keySet()) {
            if(sortFieldsOrder.get(sortField)) {
                cri.addOrder(Order.asc(sortField));
            } else {
                cri.addOrder(Order.desc(sortField));
            }
        }

        return getHibernateTemplate().findByCriteria(cri, start, count);
    }

    public int getKbArticleCountByUser(String searchText, User user) {
        DetachedCriteria cri = getKbArticleQueryCriteria(searchText, user);
        cri.setProjection(Projections.rowCount());
        return DataAccessUtils.intResult(getHibernateTemplate().findByCriteria(cri));
    }

    private DetachedCriteria getKbArticleQueryCriteria(String searchText, User user) {
        DetachedCriteria cri = DetachedCriteria.forClass(KnowledgeBase.class).createAlias("group", "gr");

        Disjunction dis = Restrictions.disjunction();
        //admin can see all articles
        if (user.getId() != User.ADMIN_ID) {
            //user can see private articles in groups they belong to and all public articles
            if(CollectionUtils.isNotEmpty(user.getGroups())) {
                dis.add(Restrictions.or(
                        Restrictions.and(Restrictions.in("group", user.getGroups()), Restrictions.eq("isPrivate", Boolean.TRUE)),
                        Restrictions.eq("isPrivate", Boolean.FALSE)));
            } else {
                dis.add(Restrictions.eq("isPrivate", Boolean.FALSE));
            }
        }

        cri.add(dis);

        if(StringUtils.isNotBlank(searchText)) {
            Disjunction d = Restrictions.disjunction();
            d.add(Restrictions.ilike("subject", searchText, MatchMode.ANYWHERE));
            d.add(Restrictions.like("problem", searchText, MatchMode.ANYWHERE));
            d.add(Restrictions.like("resolution", searchText, MatchMode.ANYWHERE));
            d.add(Restrictions.ilike("gr.name", searchText, MatchMode.ANYWHERE));
            cri.add(d);
        }

        return cri;
    }

    private List<String> searchStringToList(StringBuilder text) {
        List<String> resultList = new ArrayList<String>();
        while (text.indexOf("\"") != -1 &&
                text.indexOf("\"", text.indexOf("\"") + 1) != -1) {
            resultList.add(text.substring(text.indexOf("\"") + 1, text.indexOf("\"", text.indexOf("\"") + 1)));
            text.delete(text.indexOf("\""), text.indexOf("\"", text.indexOf("\"") + 1) + 1);
        }

        if (StringUtils.isNotBlank(text.toString().trim())) {
            resultList.addAll(Arrays.asList(text.toString().trim().split(" ")));
        }

        return resultList;
    }

    public void deleteKnowledgeBase(KnowledgeBase knowledgeBase) {
        getHibernateTemplate().delete(knowledgeBase);
    }

    public void deleteKnowledgeBaseAttachments(Attachment attach) {
        getHibernateTemplate().delete(attach);
    }

    public void flush() {
        getHibernateTemplate().flush();
    }

    public void clear() {
        getHibernateTemplate().clear();
    }

    public Attachment getNonPrivateAttachment(Integer attachmentId) {
        Criteria cri = getSessionFactory().getCurrentSession().createCriteria(KnowledgeBase.class);
        cri.createAlias("attachments", "attachments").add(Restrictions.eq("attachments.id", attachmentId));
        cri.add(Restrictions.eq("isPrivate", Boolean.FALSE));
        List list = cri.list();
        if (list.size() > 0) {
            return (Attachment) getHibernateTemplate().load(Attachment.class, attachmentId);
        }
        
        return null;
    }

    public int getCountByGroup(Group group) {
        return DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam(
                "select count(*) from KnowledgeBase as kb where kb.group = :group", "group", group));
    }

    public void saveKnowledgeBase(KnowledgeBase knowledgeBase) {
        getHibernateTemplate().saveOrUpdate(knowledgeBase);
    }

}