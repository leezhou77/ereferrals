package net.grouplink.ehelpdesk.dao.hibernate.asset;

import net.grouplink.ehelpdesk.dao.asset.AssetStatusDao;
import net.grouplink.ehelpdesk.domain.asset.AssetStatus;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Jul 31, 2008
 * Time: 11:59:04 AM
 */
public class HibernateAssetStatusDao extends HibernateDaoSupport implements AssetStatusDao {

    @SuppressWarnings("unchecked")
    public List<AssetStatus> getAllAssetStatuses() {
        return (List<AssetStatus>) getHibernateTemplate().find("from AssetStatus assetStatus");
    }

    public AssetStatus getById(Integer assetStatusId) {
        return (AssetStatus) getHibernateTemplate().get(AssetStatus.class, assetStatusId);
    }

    @SuppressWarnings("unchecked")
    public AssetStatus getByName(String name) {
        List<AssetStatus> list = (List<AssetStatus>) getHibernateTemplate().findByNamedParam("from AssetStatus ass where ass.name=:NAME", "NAME", name);
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    public void saveAssetStatus(AssetStatus assetStatus) {
        getHibernateTemplate().saveOrUpdate(assetStatus);
    }

    public void deleteAssetStatus(AssetStatus assetStatus) {
        getHibernateTemplate().delete(assetStatus);
    }

    public void flush() {
        getHibernateTemplate().flush();
    }
}
