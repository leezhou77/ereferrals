package net.grouplink.ehelpdesk.dao.zen;

import net.grouplink.ehelpdesk.domain.zen.UserPrimaryDeviceInformation;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class UserPrimaryDeviceInformationDaoImpl extends HibernateDaoSupport implements UserPrimaryDeviceInformationDao {
    
    @SuppressWarnings("unchecked")
    public List<UserPrimaryDeviceInformation> getByZuid(byte[] zuid) {
        return getHibernateTemplate().findByNamedParam("from UserPrimaryDeviceInformation u where u.extraUserInformation = :zuid", "zuid", zuid);
    }
}
