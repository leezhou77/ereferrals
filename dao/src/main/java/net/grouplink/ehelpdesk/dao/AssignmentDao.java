package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.Assignment;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.domain.filter.AssignmentFilter;

import java.util.List;
import java.util.Map;

public interface AssignmentDao {

    Assignment getById(Integer id);

    void save(Assignment assignment);

    void delete(Assignment assignment);

    List<Group> getGroupListByLocationId(Integer locationId);

    List<Category> getCategoryListByLocationIdAndGroupId(Integer locationId, Integer groupId);

    List<Category> getCategoryListAssignedToUserByUrgId(Integer urgId);

    List<CategoryOption> getCategoryOptionListAssignedToUserByUrgIdAndCategoryId(Integer urgId, Integer categoryId);

    void deleteList(List techLocCatOptList);

    int getCountByCategoryOption(CategoryOption categoryOption);

    List<Assignment> getAssignments(AssignmentFilter assignmentFilter, String queryParam, Integer start, Integer count, Map<String, Boolean> sortFieldMap);

    List<Assignment> getByUrgLocGroup(UserRoleGroup urg, Location location, Group group);

    List<Assignment> getByUrgLocGroupOnly(UserRoleGroup urg, Location location, Group group);

    List<Assignment> getByUrgLocCat(UserRoleGroup urg, Location location, Category cat);

    List<Assignment> getByUrgLocCatOnly(UserRoleGroup urg, Location location, Category cat);

    List<Assignment> getByUrgLocCatOpt(UserRoleGroup urg, Location location, CategoryOption catOpt);

    int getAssignmentCount(AssignmentFilter assignmentFilter, String queryParam);

    List<UserRoleGroup> getAssignments(Location location, Group group, Category category, CategoryOption categoryOption);

    List<UserRoleGroup> getMoreBetterAssignments(Location location, Group group, Category category, CategoryOption categoryOption);

    List<Assignment> getByCatOptAndLocation(CategoryOption categoryOption, Location location);

    List<Assignment> getByCatOpt(CategoryOption categoryOption);

    List<Assignment> getByCatAndLoc(Category category, Location location);

    List<Assignment> getByCat(Category category);

    List<Assignment> getByGroupAndLoc(Group group, Location location);

    List<Assignment> getByGroup(Group group);

    void flush();

    void clear();

}
