package net.grouplink.ehelpdesk.dao.zen;

import net.grouplink.ehelpdesk.domain.zen.Workstation;
import net.grouplink.ehelpdesk.domain.zen.ZenAssetSearch;
import net.grouplink.ehelpdesk.domain.zen.UserPrimaryDeviceInformation;
import net.grouplink.ehelpdesk.domain.zen.Device;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;
import java.util.ArrayList;

/**
 * User: jaymehafen
 * Date: Mar 31, 2009
 * Time: 8:48:42 PM
 */
public class WorkstationDaoImpl extends HibernateDaoSupport implements WorkstationDao {
    private Log log = LogFactory.getLog(getClass());

    @SuppressWarnings("unchecked")
    public List<Workstation> getAll() {
        return getHibernateTemplate().find("from Workstation");
    }

    @SuppressWarnings("unchecked")
    public List<Workstation> search(ZenAssetSearch zas) {
        Criteria c = getSession().createCriteria(Workstation.class);
        if (StringUtils.isNotBlank(zas.getAssetName())) {
            c.add(Restrictions.ilike("machineName", zas.getAssetName(), MatchMode.ANYWHERE));
        }

        if (StringUtils.isNotBlank(zas.getAssetType())) {
            c.add(Restrictions.ilike("systemProduct", zas.getAssetType(), MatchMode.ANYWHERE));
        }

        if (StringUtils.isNotBlank(zas.getOperatingSystem())) {
            c.add(Restrictions.ilike("osProduct", zas.getOperatingSystem(), MatchMode.ANYWHERE));
        }

        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return c.list();
    }

    public Workstation getById(byte[] id) {
        return (Workstation) getHibernateTemplate().get(Workstation.class, id);
    }

    public Workstation getByIdString(String id) {
        byte[] guid = new byte[0];
        try {
            guid = Hex.decodeHex(id.toCharArray());
        } catch (DecoderException e) {
            log.error("error decoding hex string " + id + ": " + e.getMessage(), e);
        }
        return getById(guid);
    }

    @SuppressWarnings("unchecked")
    public List<Workstation> getByPrimaryUser(String primaryUserGuid) {
        return getHibernateTemplate().findByNamedParam("from Workstation w where w.device.primaryUser.objectUid = :guid", "guid", primaryUserGuid);
    }

    @SuppressWarnings("unchecked")
    public List<Workstation> getByUserPrimaryDeviceInformation(List<UserPrimaryDeviceInformation> userPrimaryDeviceInformations) {
        if (CollectionUtils.isEmpty(userPrimaryDeviceInformations)) {
            return new ArrayList<Workstation>();
        }

        DetachedCriteria dc = DetachedCriteria.forClass(Workstation.class);
        List<Device> devices = new ArrayList<Device>();
        for (UserPrimaryDeviceInformation updi : userPrimaryDeviceInformations) {
            devices.add(updi.getDevice());
        }

        dc.add(Restrictions.in("device", devices));
        return getHibernateTemplate().findByCriteria(dc);
    }
}
