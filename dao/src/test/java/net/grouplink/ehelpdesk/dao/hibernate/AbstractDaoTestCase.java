package net.grouplink.ehelpdesk.dao.hibernate;

import org.springframework.test.AbstractTransactionalSpringContextTests;

/**
 * User: jaymehafen
 * Date: Feb 27, 2008
 * Time: 11:07:13 PM
 */
public abstract class AbstractDaoTestCase extends AbstractTransactionalSpringContextTests {
    protected String[] getConfigLocations() {
       return new String[]{"classpath:net/grouplink/ehelpdesk/common/appContext-db.xml",
                "classpath:net/grouplink/ehelpdesk/common/appContext.xml",
                "classpath:net/grouplink/ehelpdesk/dao/hibernate/appContext-dao.xml"};
    }
}
