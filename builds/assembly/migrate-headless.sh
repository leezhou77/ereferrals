#!/bin/sh

# Database migration script for GroupLink eHelpDesk
JAVA_OPTS="-Xmx256m -Dcom.sun.management.jmxremote"


#JAVA_HOME=/System/Library/Frameworks/Java.VM/...

# Make sure prerequisite environment variables are set
if [ -z "$JAVA_HOME" ]; then
    echo "JAVA_HOME environment variable is defined"
    echo "This environment variable is needed to run this program"
    echo "NB: JAVA_HOME should point to a JDK not a JRE"
    exit 1
fi

# determine EHELPDESK_HOME
PRG="$0"
while [ -h "$PRG" ]; do
  ls=`ls -ld "$PRG"`
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '/.*' > /dev/null; then
    PRG="$link"
  else
    PRG=`dirname "$PRG"`/"$link"
  fi
done
echo PRG=$PRG

# Get standard environment variables
PRGDIR=`dirname "$PRG"`
echo PRGDIR=$PRGDIR

[ -z "$EHELPDESK_HOME" ] && EHELPDESK_HOME=`cd "$PRGDIR" ; pwd`

echo "EHELPDESK_HOME:   $EHELPDESK_HOME"
echo "JAVA_HOME:        $JAVA_HOME"
echo "JAVA_OPTS:        $JAVA_OPTS"
echo "CLASSPATH:        $CLASSPATH"

$JAVA_HOME/bin/java $JAVA_OPTS -Dehelpdesk.home="$EHELPDESK_HOME" -Djava.awt.headless=true -jar $EHELPDESK_HOME/migration/ehd-migration-${project.version}.jar
