GroupLink Product
LICENSE AGREEMENT
20071017

Before proceeding with the installation of this software, you must accept the terms of the
following license agreement.  Indicate your acceptance or rejection of this agreement by clicking
on the appropriate button below.  If you click on a button denoting or connoting "I accept,"
installation will continue.  If you click on a button denoting or connoting "I reject," installation
will abort.  If you do not agree with these terms, you should not use the software and promptly
return it in accordance with the terms of your agreement, if applicable.

Grant of License
GroupLink Corporation ("Licensor"), a Utah corporation, hereby grants to the person or
organization accepting this License, and those who lawfully install the software accepted by such
person or organization, ("Licensee") a non-exclusive, non-transferable and non-assignable
license to use this version of the computer software and associated user documentation which
may accompany this Agreement (all referred to herein as the "Licensed Software") in accordance
with the terms and conditions of this Agreement.  THIS LICENSE GRANT IS SUBJECT TO
AND CONDITIONED UPON COLLECTION OF APPROPRIATE PAYMENT AND/OR
REGISTRATION IN ACCORDANCE WITH YOUR PURCHASE AGREEMENT WITH
LICENSOR OR LICENSOR'S AUTHORIZED DEALER/REPRESENTATIVE.

Scope of Permitted Use
You may only use the attached software by the number of users or on the number of computers
granted by this license in accordance with the terms of your purchase agreement.  If the number
of users (or installations) of the software exceeds the licenses, you must have a reasonable
process in place to assure that the number of persons using the software does not exceed the
number of licenses.

DISCLAIMER OF WARRANTY
Although Licensor has tested the software and reviewed the documentation, THE LICENSED
SOFTWARE IS FURNISHED BY LICENSOR AND ACCEPTED BY LICENSEE "AS IS,"
WITHOUT ANY WARRANTY WHATSOEVER.  WITHOUT LIMITING THE
GENERALITY OF THE FOREGOING, ALL WARRANTIES, EXPRESS OR IMPLIED,
INCLUDING ANY WARRANTIES OF MERCHANTABILITY OR FITNESS FOR ANY
PARTICULAR PURPOSE, ARE SPECIFICALLY EXCLUDED AND DISCLAIMED BY
LICENSOR AND ITS SUPPLIERS.  LICENSOR DOES NOT WARRANT THAT THE
LICENSED SOFTWARE WILL MEET LICENSEE'S REQUIREMENTS OR THAT THE
OPERATION OF THE LICENSED SOFTWARE WILL BE UNINTERRUPTED OR ERROR
FREE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE
LICENSED SOFTWARE IS WITH LICENSEE.  Some states do not allow the exclusion of
implied warranties or liability for incidental or consequential damages, so the above limitation or
exclusion may not apply to you.  This warranty gives you specific legal rights, and you may also
have other rights that vary from state to state.

LIMITATION OF LICENSOR LIABILITY
IN NO EVENT WILL LICENSOR OR ITS SUPPLIERS BE LIABLE TO LICENSEE OR ANY
OTHER PERSON FOR ANY LOST PROFITS, LOST SAVINGS, LOST DATA, OR OTHER
DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL OR INCIDENTAL DAMAGES
ARISING OUT OF OR RELATING TO THE LICENSED SOFTWARE, EVEN IF LICENSOR
HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH LOSS OR DAMAGE; PROVIDED,
HOWEVER, THAT NOTHING IN THIS AGREEMENT SHALL OPERATE TO RELIEVE
LICENSOR FROM LIABILITY FOR ITS OWN WILLFUL OR WANTON RECKLESSNESS
OR INTENTIONAL TORTS.

Limited Warranty
If this software is delivered on physical medium and if you discover physical defects in the
media, Licensor will replace the media or documentation at no charge to you, provided you
return the item to be replaced with proof of payment to Licensor during the 90-day period after
having taken delivery of the software.  No Licensor dealer, representative, agent, or employee is
authorized to make any modifications or additions to this limited warranty.

Proprietary Expression and Information
Licensor is the owner of all intellectual property rights in the Licensed Software including all
copyrights, trade secrets, patents, and trademarks relating thereto.  Those rights include, but are
not limited to, all source code, object code, text, graphics, artwork,  ideas, procedures, processes,
systems, methods of operation, and concepts which are embodied within the Licensed Software
and all documentation relating thereto.  This license is not a sale of a copy of the Licensed
Software and does not render Licensee the owner of a copy of the Licensed Software.
Ownership of the Licensed Software and all components and copies thereof shall at all times
remain with Licensor, regardless of who may be deemed the owner of the tangible media in or on
which the Licensed Software may be copied, encoded or otherwise fixed.

Support Services
Licensor may provide you with support services related to the Licensed Software ("Support
Services"). Use of Support Services is governed by the Licensor policies and programs described
in the user manual, in "online" documentation, and/or in other Licensor-provided materials. Any
supplemental software code provided to you as part of the Support Services shall be considered
part of the Licensed Software and subject to the terms and conditions of this license agreement.
With respect to technical information you provide to Licensor as part of the Support Services,
Licensor may use such information for its business purposes, including for product support and
development. Licensor will not utilize such technical information in a form that personally
identifies you.  Licensee acknowledges it will provide, at its own expense, software, hardware
and support services for its own computer infrastructure, including, but not limited to, operating
systems, networking (including Internet), emailing, database and other similar 3rd party software
/ systems.  Licensee also acknowledges that any software support, for Licensed Software, it may
contract from Licensor does not include support for such infrastructure components.

Solicitation of Employment - - Before Payment is Made for Licensed Software
Licensee confirms genuine interest in evaluating and / or using Licensed Software, and agrees
not to target, and not to suggest that others target, employees or contractors of Licensor as
possible candidates for employment or contract work - - directly or indirectly.  Furthermore,
Licensee agrees not to knowingly solicit to or actually hire, retain, contract with or engage the
employment or services of any current employee or consultant of Licensor, except through
Licensor, during the term of this agreement and for a period of twelve (12) months thereafter.
Furthermore, Licensee hereby agrees, covenants and warrants that it shall not, during the term of
this agreement and for a period of twelve (12) months thereafter, knowingly retain in any manner
except through Licensor, either directly or indirectly (including through a 3rd party or consulting
firm), the services of any of Licensor's current employees or consultants.

Solicitation of Employment - - After Payment is Made for Licensed Software
Licensee agrees not to target, and not to suggest that others target, employees or contractors of
Licensor as possible candidates for employment or contract work - - directly or indirectly.
Furthermore, Licensee agrees not to knowingly solicit to or actually hire, retain, contract with or
engage the employment or services of any current or former employee or consultant of Licensor,
except through Licensor, during the term of this agreement and for a period of twelve (12)
months thereafter.  Furthermore, Licensee hereby agrees, covenants and warrants that it shall not,
during the term of this agreement and for a period of twelve (12) months thereafter, knowingly
retain in any manner except through Licensor, either directly or indirectly (including through a
3rd party or consulting firm), the services of any of Licensor's current or former employees or
consultants.

Restrictions Upon Duplication, Reverse Engineering and Disclosure
Any copy of the Licensed Software made by Licensee must bear the same copyright and other
proprietary notices that appear on the copy furnished to Licensee by Licensor.  Licensee will not
disassemble, decompile, translate or otherwise attempt to "reverse engineer" the Licensed
Software, nor shall Licensee permit any other person to do so.  Licensee will make reasonable
efforts to prevent any unauthorized copying of the Licensed Software or disclosure or use of
Licensor's trade secret information, and Licensee will advise Licensee's employees who are
permitted access to the Licensed Software of the restrictions upon duplication, reverse
engineering, disclosure and use contained in this Agreement.  Licensee will be liable for any
unauthorized copying, reverse engineering and/or disclosure by Licensee's employees or agents.
Only with specific written permission (e.g., a License addendum) from an officer of Licensor
may any party other than Licensor produce any derivative work based on the Licensed Software.
Otherwise neither Licensee nor any other party may produce any derivative work based on the
Licensed Software.  Licensee will not use the Licensed Software to create for distribution a
product that is generally competitive with LicensorŐs products, including with any of the
functionalities of the Licensed Software.  Licensee will not use the Licensed Software, or any
elements thereof, on a rental or timeshare basis or to operate a service bureau facility for the
benefit of third parties.  Licensee will not use the Licensed Software, or any elements thereof, to
create for distribution a product that converts the report file (.RPT) format to an alternative report
file format used by any general-purpose report writing, data analysis or report delivery product
that is not the property of Licensor.

Restriction upon Transfer
Licensee will not lease, rent, sell, distribute, pledge, assign, sublicense, loan or otherwise transfer
to any third party any part of the Licensed Software or any copy thereof or any of Licensee's
rights under this Agreement.

Termination
This license and your right to use this software automatically terminates if you fail to comply
with any provision of this license agreement.  This license and your right to use this software
also automatically terminates if you fail to pay the applicable license fees as stated in your
purchase agreement with Licensor or Licensor's authorized dealer/representative.

U.S. Government Restricted Rights
The Licensed Software is provided with RESTRICTED RIGHTS.  Use, duplication or disclosure
by the Government is subject to restrictions as set forth in subparagraph (c)(1)(ii) in The Rights
in Technical Data and Computer Software Clause at DFARS  252.227-7013, or subparagraphs
(c)(1) and (2) of the Commercial Computer Software - Restricted Rights at 48 CFR 52.227-19,
as applicable.  Contractor/manufacturer is GroupLink Corporation, 563 West 500 South, Suite
400, Bountiful, Utah  84010.

Non-Waiver
The failure by either party at any time to enforce any of the provisions of this Agreement or any
right or remedy available hereunder or at law or in equity, or to exercise any option herein
provided, shall not constitute a waiver of such provision, right, remedy or option or in any way
affect the validity of this Agreement.  The waiver of any default by either party shall not be
deemed a continuing waiver, but shall apply solely to the instance to which such waiver is
directed.

Severability and Choice of Law
Every provision of this Agreement shall be construed, to the extent possible, so as to be valid and
enforceable.  If any provision of this Agreement so construed is held by a court of competent
jurisdiction to be invalid, illegal or otherwise unenforceable, such provision shall be deemed
severed from this Agreement, and all other provisions shall remain in full force and effect.  This
Agreement shall in all respects be governed by and interpreted, construed and enforced in
accordance with the laws of the United States of America and the state of Utah.  Venue for any
action between Licensor and Licensee will be exclusively in an applicable state or federal court
in either Davis County or Salt Lake County, State of Utah, and Licensee irrevocably submits to
the personal jurisdiction of such courts for such purpose.  In the event of litigation or arbitration,
the prevailing party shall recover and be awarded all costs and expenses incurred in pursuing
said litigation or arbitration, including all reasonable attorneys' fees.

Assignment and Binding Effect
Licensor may assign, delegate and/or otherwise transfer this Agreement or its rights and
obligations to any person or entity.  Licensee may not assign, delegate or otherwise transfer this
Agreement or any of Licensee's rights or obligations hereunder without the prior written consent
of Licensor.  This Agreement shall be binding upon and inure to the benefit of the parties and
their respective successors and permitted assigns.

Entire Agreement
This Agreement sets forth the entire agreement and understanding between Licensor and
Licensee regarding the subject matter hereof and supersedes any prior representations,
advertisements, statements, proposals, negotiations, discussions, understandings, or agreements
regarding the same subject matter.  This Agreement may not be modified or amended except by
a writing signed by the party against whom the same is sought to be enforced.