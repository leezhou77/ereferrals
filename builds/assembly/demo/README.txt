everything HelpDesk v10.1
All Rights Reserved GroupLink 2013
----------------------------------

This is the README file for the everything HelpDesk DEMO distribution.

Installation instructions are located in the INSTALL.txt file.

A list of changes contained in this and previous releases can be found in
RELEASE-NOTES.txt

Thanks for choosing GroupLink!
