package net.grouplink.ehelpdesk.dashboard.charts;

import net.grouplink.ehelpdesk.dashboard.DashboardAppUtils;
import net.grouplink.ehelpdesk.domain.dashboard.Widget;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.dial.ArcDialFrame;
import org.jfree.chart.plot.dial.DialBackground;
import org.jfree.chart.plot.dial.DialPlot;
import org.jfree.chart.plot.dial.DialPointer;
import org.jfree.chart.plot.dial.StandardDialScale;
import org.jfree.data.general.DefaultValueDataset;
import org.jfree.ui.GradientPaintTransformType;
import org.jfree.ui.StandardGradientPaintTransformer;

import java.awt.*;

public class HorizontalArcDial {

    public static JFreeChart getChart(Widget widget, Integer userId) {

//        TicketFilter ticketFilter = widget.getTicketFilter();
        TicketFilter ticketFilter = widget.getReport().getTicketFilter();

        Integer count = DashboardAppUtils.getDashboardService().getTicketCount(ticketFilter.getId(), userId);

        return getCustomChart(count, widget);
    }

    private static JFreeChart getCustomChart(Integer count, Widget widget) {

        DialPlot dialplot = new DialPlot();
        dialplot.setView(0.20999999999999999D, 0.0D, 0.57999999999999996D, 0.29999999999999999D);
        dialplot.setDataset(new DefaultValueDataset(count));

        ArcDialFrame arcdialframe = new ArcDialFrame(60D, 60D);
        arcdialframe.setInnerRadius(0.59999999999999998D);
        arcdialframe.setOuterRadius(0.90000000000000002D);
        arcdialframe.setForegroundPaint(Color.darkGray);
        arcdialframe.setStroke(new BasicStroke(3F));
        dialplot.setDialFrame(arcdialframe);

        GradientPaint gradientpaint = new GradientPaint(new Point(), new Color(255, 255, 255), new Point(), new Color(240, 240, 240));
        DialBackground dialbackground = new DialBackground(gradientpaint);
        dialbackground.setGradientPaintTransformer(new StandardGradientPaintTransformer(GradientPaintTransformType.VERTICAL));
        dialplot.addLayer(dialbackground);

        StandardDialScale standarddialscale = new StandardDialScale(0.0D, 100D, 115D, -50D, 10D, 4);
        standarddialscale.setTickRadius(0.88D);
        standarddialscale.setTickLabelOffset(0.070000000000000007D);
        standarddialscale.setMajorTickIncrement(25D);
        dialplot.addScale(0, standarddialscale);

        DialPointer.Pin pin = new DialPointer.Pin();
        pin.setRadius(0.81999999999999995D);
        dialplot.addLayer(pin);

        JFreeChart jfreechart = new JFreeChart(dialplot);
        jfreechart.setTitle(widget.getName());
        return jfreechart;
    }


}
