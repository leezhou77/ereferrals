package net.grouplink.ehelpdesk.dashboard.charts;

import net.grouplink.ehelpdesk.dashboard.DashboardAppUtils;
import net.grouplink.ehelpdesk.domain.dashboard.Widget;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.MultiplePiePlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.util.TableOrder;

import java.util.Map;

public class MultiplePie {

    public static JFreeChart getChart(Widget widget, Integer userId) {

//        TicketFilter ticketFilter = widget.getTicketFilter();
        TicketFilter ticketFilter = widget.getReport().getTicketFilter();
//        Map<String, Map<String, Integer>> groupByByMap = DashboardAppUtils.getDashboardService().getTicketCountTwoGroups(ticketFilter.getId(), userId);
        Map<String, Map<String, Integer>> groupByByMap = DashboardAppUtils.getDashboardService().getTicketCountTwoGroups(widget.getId(), userId);

        return getCustomChart(widget, groupByByMap);
    }

    private static JFreeChart getCustomChart(Widget widget, Map<String, Map<String, Integer>> groupByByMap) {

        CategoryDataset categoryDataset = createDataset(groupByByMap);

        JFreeChart jFreeChart = ChartFactory.createMultiplePieChart3D(widget.getName(), categoryDataset, TableOrder.BY_ROW, true, true, false);

        MultiplePiePlot multiplepieplot = (MultiplePiePlot)jFreeChart.getPlot();
        multiplepieplot.setBackgroundPaint(null);
        multiplepieplot.setOutlineStroke(null);

        JFreeChart jfreechart1 = multiplepieplot.getPieChart();

        PiePlot pieplot = (PiePlot)jfreechart1.getPlot();
        pieplot.setBackgroundPaint(null);
        pieplot.setOutlineStroke(null);
        pieplot.setIgnoreNullValues(true);
        pieplot.setMaximumLabelWidth(0.35);

        return jFreeChart;
    }

    private static CategoryDataset createDataset(Map<String, Map<String, Integer>> groupByByMap) {

        DefaultCategoryDataset defaultCategoryDataset = new DefaultCategoryDataset();
        for (Map.Entry<String, Map<String, Integer>> entry : groupByByMap.entrySet()) {
            for (Map.Entry<String, Integer> byEntry : entry.getValue().entrySet()) {
                defaultCategoryDataset.addValue(byEntry.getValue(), byEntry.getKey(), entry.getKey());
            }
        }
        return defaultCategoryDataset;
    }
}
