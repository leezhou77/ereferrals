package net.grouplink.ehelpdesk.dashboard.charts;

import net.grouplink.ehelpdesk.dashboard.DashboardAppUtils;
import net.grouplink.ehelpdesk.domain.dashboard.Widget;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.ThermometerPlot;
import org.jfree.data.general.DefaultValueDataset;
import org.jfree.ui.RectangleInsets;

import java.awt.*;

public class Thermometer {

    public static JFreeChart getChart(Widget widget, Integer userId) {

//        TicketFilter ticketFilter = widget.getTicketFilter();
        TicketFilter ticketFilter = widget.getReport().getTicketFilter();

        Integer count = DashboardAppUtils.getDashboardService().getTicketCount(ticketFilter.getId(), userId);

        return getCustomChart(count, widget);
    }

    private static JFreeChart getCustomChart(Integer count, Widget widget) {

        DefaultValueDataset dataset = new DefaultValueDataset(count);

        ThermometerPlot thermometerplot = new ThermometerPlot(dataset);
        thermometerplot.setInsets(new RectangleInsets(5D, 5D, 5D, 5D));
        thermometerplot.setPadding(new RectangleInsets(10D, 10D, 10D, 10D));
        thermometerplot.setThermometerStroke(new BasicStroke(2.0F));
        thermometerplot.setThermometerPaint(Color.lightGray);
        thermometerplot.setUnits(1);
        thermometerplot.setGap(3);
        if (widget.getMinimum() != null && widget.getMaximum() != null) {
            thermometerplot.setRange(widget.getMinimum(), widget.getMaximum());
        }

        thermometerplot.setBackgroundPaint(null);
        thermometerplot.setOutlineStroke(null);

        if (widget.getGreenRangeBegin() != null && widget.getGreenRangeEnd() != null) {
            thermometerplot.setSubrange(0, widget.getGreenRangeBegin(), widget.getGreenRangeEnd());
            thermometerplot.setSubrangePaint(0, Color.green);
        }

        if (widget.getYellowRangeBegin() != null && widget.getYellowRangeEnd() != null) {
            thermometerplot.setSubrange(1, widget.getYellowRangeBegin(), widget.getYellowRangeEnd());
            thermometerplot.setSubrangePaint(1, Color.orange);
        }

        if (widget.getRedRangeBegin() != null && widget.getRedRangeEnd() != null) {
            thermometerplot.setSubrange(2, widget.getRedRangeBegin(), widget.getRedRangeEnd());
            thermometerplot.setSubrangePaint(2, Color.red);
        }

        JFreeChart jfreechart = new JFreeChart(thermometerplot);
        jfreechart.setTitle(widget.getName());
        return jfreechart;
    }
}
