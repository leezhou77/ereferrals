package net.grouplink.ehelpdesk.dashboard;

import net.grouplink.ehelpdesk.common.gui.SplashScreen;
import net.grouplink.ehelpdesk.dashboard.charts.Bar;
import net.grouplink.ehelpdesk.dashboard.charts.Dial;
import net.grouplink.ehelpdesk.dashboard.charts.HorizontalArcDial;
import net.grouplink.ehelpdesk.dashboard.charts.MultiplePie;
import net.grouplink.ehelpdesk.dashboard.charts.Pie;
import net.grouplink.ehelpdesk.dashboard.charts.StackedBar;
import net.grouplink.ehelpdesk.dashboard.charts.Thermometer;
import net.grouplink.ehelpdesk.dashboard.charts.VerticalArcDial;
import net.grouplink.ehelpdesk.domain.dashboard.Dashboard;
import net.grouplink.ehelpdesk.domain.dashboard.Widget;
import net.grouplink.ehelpdesk.service.DashboardService;
import org.apache.commons.lang.LocaleUtils;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.Timer;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.net.URL;
import java.util.Locale;
import java.util.Set;

public class DashboardApp extends ApplicationFrame {

    private Dashboard dashboard;
    private Integer userId;

    private static String imagePath = "net/grouplink/ehelpdesk/common/images/";
    private static String ehdLogo = "ehd_logo.gif";

    public DashboardApp(String title) {
        super(title);
    }

    private JComponent createContent() {
        JPanel panel = new JPanel(new GridBagLayout());

        Set<Widget> widgets = dashboard.getWidgets();
        for (Widget widget : widgets) {
            panel.add(createWidgetPanel(widget),
                    DashboardAppUtils.buildGBC(widget.getRow(), widget.getColumn(), widget.getColspan(), widget.getRowspan()));
        }

        return panel;
    }

    private JPanel createWidgetPanel(Widget widget) {
        JFreeChart jFreeChart = null;

        // Determine chart type from widget type
        switch (widget.getType()) {
            case Widget.DIAL:
                jFreeChart = Dial.getChart(widget, userId);
                break;
            case Widget.HORIZONTAL_ARC_DIAL:
                jFreeChart = HorizontalArcDial.getChart(widget, userId);
                break;
            case Widget.VERTICAL_ARC_DIAL:
                jFreeChart = VerticalArcDial.getChart(widget, userId);
                break;
            case Widget.THERMOMETER:
                jFreeChart = Thermometer.getChart(widget, userId);
                break;
            case Widget.BAR_CHART:
                jFreeChart = Bar.getChart(widget, userId);
                break;
            case Widget.STACKED_BAR_CHART:
                jFreeChart = StackedBar.getChart(widget, userId);
                break;
            case Widget.PIE_CHART:
                jFreeChart = Pie.getChart(widget, userId);
                break;
            case Widget.MULTI_PIE_CHART:
                jFreeChart = MultiplePie.getChart(widget, userId);
                break;
        }

        return new ChartPanel(jFreeChart);
    }


    /**
     * Starts the dashboard app.
     *
     * @param args command line arguments.
     *             args[0] = Dashboard.id
     *             args[1] = User.id
     *             args[2] = locale
     */
    public static void main(String[] args) {

        Frame splashFrame;
        URL imageURL = DashboardApp.class.getClassLoader().getResource(imagePath + ehdLogo);
        splashFrame = SplashScreen.splash(Toolkit.getDefaultToolkit().createImage(imageURL));
        
        // Setup context and get service
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
                new String[]{"classpath:appContext-dashboard.xml"}
        );
        DashboardAppUtils.setAppContext(context);
        final DashboardService dbs = DashboardAppUtils.getDashboardService();

        // Process command line arguments
        final Integer dashId = Integer.parseInt(args[0]);
        Integer userId = Integer.parseInt(args[1]);
        String localeStr = args[2];

        Locale locale = LocaleUtils.toLocale(localeStr);
        DashboardAppUtils.setLocale(locale);

        // Create and display the dashboard app
        final DashboardApp dashboardApp = new DashboardApp("");
        Dashboard db = dbs.getById(dashId);
        dashboardApp.setDashboard(db);
        dashboardApp.setUserId(userId);
        dashboardApp.setTitle(dashboardApp.getDashboard().getName());
        dashboardApp.setContentPane(dashboardApp.createContent());
        dashboardApp.setPreferredSize(new Dimension(800, 600));
        dashboardApp.pack();
        RefineryUtilities.centerFrameOnScreen(dashboardApp);

        splashFrame.dispose();
        dashboardApp.setVisible(true);

        int refreshDelay = Integer.valueOf(System.getProperty("dashboard.refresh", "60000"));
        new Timer(refreshDelay, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                try {
                    System.out.println("refreshing dashboard");
                    dashboardApp.setDashboard(dbs.getById(dashId));
                    dashboardApp.setTitle(dashboardApp.getDashboard().getName());
                    dashboardApp.setContentPane(dashboardApp.createContent());
                    dashboardApp.validate();
                } catch (Exception e2) {
                    System.out.println("error occurred while refreshing dashboard");
                    e2.printStackTrace();
                }
            }
        }).start();

    }

    public Dashboard getDashboard() {
        return dashboard;
    }

    public void setDashboard(Dashboard dashboard) {
        this.dashboard = dashboard;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
