package net.grouplink.ehelpdesk.dashboard.charts;

import net.grouplink.ehelpdesk.dashboard.DashboardAppUtils;
import net.grouplink.ehelpdesk.domain.dashboard.Widget;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

import java.awt.*;
import java.util.Map;

public class Pie {

    public static JFreeChart getChart(Widget widget, Integer userId) {

//        TicketFilter ticketFilter = widget.getTicketFilter();
//        TicketFilter ticketFilter = widget.getReport().getTicketFilter();

//        Map<String, Integer> groupByMap = DashboardAppUtils.getDashboardService().getTicketCountOneGroup(ticketFilter.getId(), userId);
        Map<String, Integer> groupByMap = DashboardAppUtils.getDashboardService().getTicketCountOneGroup(widget.getId(), userId);

        return getCustomChart(widget, groupByMap);
    }

    private static JFreeChart getCustomChart(Widget widget, Map<String, Integer> groupByMap) {

        PieDataset dataSet = createDataset(groupByMap);

        JFreeChart jFreeChart = ChartFactory.createPieChart3D(widget.getName(), dataSet, true, true, false);

        PiePlot piePlot = (PiePlot) jFreeChart.getPlot();
        piePlot.setLabelFont(new Font("SansSerif", 0, 12));
        piePlot.setNoDataMessage(DashboardAppUtils.getMessage("dashboard.widget.noData"));
        piePlot.setCircular(false);
        piePlot.setLabelGap(0.02D);
        piePlot.setOutlineStroke(null);
        piePlot.setBackgroundPaint(null);
        piePlot.setIgnoreNullValues(true);

        return jFreeChart;
    }

    private static PieDataset createDataset(Map<String, Integer> groupByMap) {

        DefaultPieDataset defaultPieDataset = new DefaultPieDataset();
        for (Map.Entry<String, Integer> entry : groupByMap.entrySet()) {
            defaultPieDataset.setValue(entry.getKey(), entry.getValue());
        }

        return defaultPieDataset;
    }
}
