package net.grouplink.ehelpdesk.dashboard.charts;

import net.grouplink.ehelpdesk.dashboard.DashboardAppUtils;
import net.grouplink.ehelpdesk.domain.dashboard.Widget;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.dial.ArcDialFrame;
import org.jfree.chart.plot.dial.DialBackground;
import org.jfree.chart.plot.dial.DialPlot;
import org.jfree.chart.plot.dial.DialPointer;
import org.jfree.chart.plot.dial.StandardDialScale;
import org.jfree.data.general.DefaultValueDataset;
import org.jfree.ui.GradientPaintTransformType;
import org.jfree.ui.StandardGradientPaintTransformer;

import java.awt.*;

public class VerticalArcDial {

    public static JFreeChart getChart(Widget widget, Integer userId) {

//        TicketFilter ticketFilter = widget.getTicketFilter();
        TicketFilter ticketFilter = widget.getReport().getTicketFilter();

        Integer count = DashboardAppUtils.getDashboardService().getTicketCount(ticketFilter.getId(), userId);

        return getCustomChart(count, widget);
    }

    private static JFreeChart getCustomChart(Integer count, Widget widget) {

        DialPlot dialPlot = new DialPlot();
        dialPlot.setView(0.78000000000000003D, 0.37D, 0.22D, 0.26000000000000001D);
        dialPlot.setDataset(new DefaultValueDataset(count));

        ArcDialFrame arcDialFrame = new ArcDialFrame(-10D, 20D);
        arcDialFrame.setInnerRadius(0.69999999999999996D);
        arcDialFrame.setOuterRadius(0.90000000000000002D);
        arcDialFrame.setForegroundPaint(Color.darkGray);
        arcDialFrame.setStroke(new BasicStroke(3F));
        dialPlot.setDialFrame(arcDialFrame);

        GradientPaint gradientPaint = new GradientPaint(new Point(), new Color(255, 255, 255), new Point(), new Color(240, 240, 240));
        DialBackground dialbackground = new DialBackground(gradientPaint);
        dialbackground.setGradientPaintTransformer(new StandardGradientPaintTransformer(GradientPaintTransformType.VERTICAL));
        dialPlot.addLayer(dialbackground);

        StandardDialScale standardDialScale = new StandardDialScale(0.0D, 100D, -8D, 16D, 10D, 4);
        standardDialScale.setTickRadius(0.81999999999999995D);
        standardDialScale.setTickLabelOffset(-0.040000000000000001D);
        standardDialScale.setMajorTickIncrement(25D);
        standardDialScale.setTickLabelFont(new Font("Dialog", 0, 14));
        dialPlot.addScale(0, standardDialScale);

        DialPointer.Pin pin = new DialPointer.Pin();
        pin.setRadius(0.83999999999999997D);
        dialPlot.addLayer(pin);

        JFreeChart jFreeChart = new JFreeChart(dialPlot);
        jFreeChart.setTitle(widget.getName());
        return jFreeChart;
    }
}
