package net.grouplink.ehelpdesk.dashboard;

import net.grouplink.ehelpdesk.service.DashboardService;
import net.grouplink.ehelpdesk.service.TicketFilterService;
import net.grouplink.ehelpdesk.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.i18n.LocaleContextHolder;

import java.awt.*;
import java.util.Locale;

public class DashboardAppUtils {

    private static Locale locale;

    private DashboardAppUtils(){}

    private static ApplicationContext appContext;

    public static ApplicationContext getAppContext() {
        return appContext;
    }

    public static void setAppContext(ApplicationContext appContext) {
        DashboardAppUtils.appContext = appContext;
    }

    // Service getters    
    public static UserService getUserService(){
        return (UserService) appContext.getBean("userService");
    }

    public static DashboardService getDashboardService(){
        return (DashboardService) appContext.getBean("dashboardService");
    }

    public static TicketFilterService getTicketFilterService(){
        return (TicketFilterService) appContext.getBean("ticketFilterService");
    }

    public static String getMessage(String code){
        return getMessage(code, null);
    }

    public static String getMessage(String code, Object[] args){
        return appContext.getMessage(code, args, getLocale());
    }

    // Some UI helpers
    /**
     * Builds <code>GridBagConstraints</code> based on the row, column and
     * number of columns to span as taken from a Dashboard Widget
     *
     * @param row the row a Widget is to be displayed in (gridy).
     * @param col the column a Widget is to be displayed in (gridx).
     * @param colspan the number of columns a Widget is to span (gridwith).
     * @param rowspan the number of rows a Widget is to span (gridwith).
     * @return the <code>GridBagConstraints</code> with the approprate parameters set.
     */
    public static GridBagConstraints buildGBC(int row, int col, int colspan, int rowspan){

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = col-1;
        gbc.gridy = row-1;
        gbc.gridwidth = colspan;
        gbc.gridheight = rowspan;
        gbc.fill = java.awt.GridBagConstraints.BOTH;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;

        return gbc;
    }

    public static Locale getLocale() {
        if(locale == null) locale = LocaleContextHolder.getLocale();
        return locale;
    }

    public static void setLocale(Locale loc) {
        locale = loc;
    }
}