package net.grouplink.ehelpdesk.common.utils;

import org.apache.commons.collections.map.Flat3Map;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

/**
 * User: mrollins
 * Date: Jan 3, 2007
 * Time: 4:25:35 PM
 */
public class FormatUtils {

    public static final int DECIMAL = 0;
    public static final int PERCENT = 1;
    public static final int CURRENCY = 2;
    public static final int INTEGER = 3;

    private static ThreadLocal formatMapHolder = new ThreadLocal();

    public static String format(Date date, String pattern)
    {
        return getDateFormat(pattern).format(date);
    }

    public static String format(Number number, Locale locale)
    {
        return formatDecimal(number, locale);
    }

    public static String format(Number number)
    {
        return format(number, Locale.getDefault());
    }

    public static String format(double number)
    {
        return format(number, Locale.getDefault());
    }

    public static String format(Number number, Locale locale, int fractionDigits)
    {
        return formatDecimal(number, locale, fractionDigits);
    }

    public static String formatInteger(Number number, Locale locale)
    {
        return getIntegerFormat(locale).format(number);
    }

    public static String formatInteger(Number number, Locale locale, int minDigits)
    {
        return getIntegerFormat(locale, minDigits).format(number);
    }

    public static String formatDecimal(Number number, Locale locale)
    {
        return getDecimalFormat(locale).format(number);
    }

    public static String formatDecimal(Number number, Locale pattern, int fractionDigits)
    {
        return getDecimalFormat(pattern, fractionDigits).format(number);
    }

    public static String formatPercent(Number number, Locale locale)
    {
        return getPercentFormat(locale).format(number);
    }

    public static String formatPercent(Number number, Locale pattern, int fractionDigits)
    {
        return getPercentFormat(pattern, fractionDigits).format(number);
    }

    public static String formatPercent(Number number, Locale pattern, int minFractionDigits, int maxFractionDigits,
                                       int minIntegerDigits, int maxIntegerDigits)
    {
        return getPercentFormat(pattern, minFractionDigits, maxFractionDigits, minIntegerDigits, maxIntegerDigits).format(number);
    }

    public static String formatPercent(Number number, Locale pattern, int minFractionDigits, int maxFractionDigits,
                                       int minIntegerDigits)
    {
        return getPercentFormat(pattern, minFractionDigits, maxFractionDigits, minIntegerDigits).format(number);
    }

    public static String formatCurrency(Number number, Locale locale)
    {
        return getCurrencyFormat(locale).format(number);
    }

    public static String formatCurrency(Number number)
    {
        return getCurrencyFormat(Locale.getDefault()).format(number);
    }

    public static String formatCurrency(Number number, Locale locale, int fractionDigits)
    {
        return getCurrencyFormat(locale, fractionDigits).format(number);
    }

    public static String format(double number, Locale locale, int fractionDigits)
    {
        return formatDecimal(number, locale, fractionDigits);
    }

    public static String formatInteger(int number, Locale locale)
    {
        return getIntegerFormat(locale).format(number);
    }

    public static String formatInteger(int number, Locale locale, int minDigits)
    {
        return getIntegerFormat(locale, minDigits).format(number);
    }

    public static String formatDecimal(int number, Locale locale)
    {
        return getDecimalFormat(locale).format(number);
    }

    public static String formatDecimal(double number, Locale pattern, int fractionDigits)
    {
        return getDecimalFormat(pattern, fractionDigits).format(number);
    }

    public static String formatPercent(double number, Locale locale)
    {
        return getPercentFormat(locale).format(number);
    }

    public static String formatPercent(double number, Locale pattern, int fractionDigits)
    {
        return getPercentFormat(pattern, fractionDigits).format(number);
    }

    public static String formatPercent(double number, Locale pattern, int minFractionDigits, int maxFractionDigits,
                                       int minIntegerDigits, int maxIntegerDigits)
    {
        return getPercentFormat(pattern, minFractionDigits, maxFractionDigits, minIntegerDigits, maxIntegerDigits).format(number);
    }

    public static String formatPercent(double number, Locale pattern, int minFractionDigits, int maxFractionDigits,
                                       int minIntegerDigits)
    {
        return getPercentFormat(pattern, minFractionDigits, maxFractionDigits, minIntegerDigits).format(number);
    }

    public static String formatCurrency(double number, Locale locale)
    {
        return getCurrencyFormat(locale).format(number);
    }

    public static String formatCurrency(double number)
    {
        return formatCurrency(new Double(number));
    }

    public static String formatCurrency(double number, Locale locale, int fractionDigits)
    {
        return getCurrencyFormat(locale, fractionDigits).format(number);
    }

    public static Date parseDate(String date, String pattern) throws ParseException {
        return getDateFormat(pattern).parse(date);
    }

    public static Number parseDecimal(String number, Locale locale) throws ParseException
    {
        return getDecimalFormat(locale).parse(number);
    }

    public static Number parseCurrency(String number, Locale locale) throws ParseException
    {
        return getDecimalFormat(locale).parse(number);
    }

    public static Number parsePercent(String number, Locale locale) throws ParseException
    {
        return getDecimalFormat(locale).parse(number);
    }

    public static Number parseDecimal(String number, Locale locale, int fractionDigits) throws ParseException
    {
        return getDecimalFormat(locale, fractionDigits).parse(number);
    }

    public static Number parseCurrency(String number, Locale locale, int fractionDigits) throws ParseException {
        return getCurrencyFormat(locale, fractionDigits).parse(number);
    }

    public static Number parsePercent(String number, Locale locale, int fractionDigits) throws ParseException
    {
        return getPercentFormat(locale, fractionDigits).parse(number);
    }

    public static DateFormat getDateFormat(String pattern)
    {
        return getThreadLocalDateFormat(pattern);
    }

    public static NumberFormat getDecimalFormat()
    {
        return getThreadLocalNumberFormat(DECIMAL, Locale.getDefault(), -1, -1, -1, -1, false);
    }

    public static NumberFormat getDecimalFormat(Locale locale)
    {
        return getThreadLocalNumberFormat(DECIMAL, locale, -1, -1, -1, -1, false);
    }

    public static NumberFormat getDecimalFormat(Locale locale, int fractionDigits)
    {
        return getThreadLocalNumberFormat(DECIMAL, locale, fractionDigits, fractionDigits, -1, -1, false);
    }

    public static NumberFormat getDecimalFormat(Locale locale, int fractionDigits, boolean useGrouping)
    {
        return getThreadLocalNumberFormat(DECIMAL, locale, fractionDigits, fractionDigits, -1, -1, useGrouping);
    }

    public static NumberFormat getCurrencyFormat(Locale locale)
    {
        return getThreadLocalNumberFormat(CURRENCY, locale, -1, -1, -1, -1, true);
    }

    public static NumberFormat getCurrencyFormat()
    {
        return getThreadLocalNumberFormat(CURRENCY, Locale.getDefault(), -1, -1, -1, -1, true);
    }

    public static NumberFormat getCurrencyFormat(int fractionDigits)
    {
        return getThreadLocalNumberFormat(CURRENCY, Locale.getDefault(), fractionDigits, fractionDigits, -1, -1, true);
    }

    public static NumberFormat getCurrencyFormat(Locale locale, int fractionDigits)
    {
        return getThreadLocalNumberFormat(CURRENCY, locale, fractionDigits, fractionDigits, -1, -1, true);
    }

    public static NumberFormat getPercentFormat(Locale locale)
    {
        return getThreadLocalNumberFormat(PERCENT, locale, -1, -1, -1, -1, true);
    }

    public static NumberFormat getPercentFormat()
    {
        return getThreadLocalNumberFormat(PERCENT, Locale.getDefault(), -1, -1, -1, -1, true);
    }

    public static NumberFormat getPercentFormat(Locale locale, int fractionDigits)
    {
        return getThreadLocalNumberFormat(PERCENT, locale, fractionDigits, fractionDigits, -1, -1, true);
    }

    public static NumberFormat getPercentFormat(Locale locale, int minFractionDigits, int maxFractionDigits, int minIntegerDigits, int maxIntegerDigits)
    {
        return getThreadLocalNumberFormat(PERCENT, locale, minFractionDigits, maxFractionDigits, minIntegerDigits, maxIntegerDigits, true);
    }

    public static NumberFormat getPercentFormat(Locale locale, int minFractionDigits, int maxFractionDigits, int minIntegerDigits)
    {
        return getThreadLocalNumberFormat(PERCENT, locale, minFractionDigits, maxFractionDigits, minIntegerDigits, -1, true);
    }

    public static NumberFormat getIntegerFormat()
    {
        return getThreadLocalNumberFormat(INTEGER, Locale.getDefault(), -1, -1, -1, -1, true);
    }

    public static NumberFormat getIntegerFormat(Locale locale)
    {
        return getThreadLocalNumberFormat(INTEGER, locale, -1, -1, -1, -1, true);
    }

    public static NumberFormat getIntegerFormat(Locale locale, int minDigits)
    {
        return getThreadLocalNumberFormat(INTEGER, locale, -1, -1, minDigits, -1, true);
    }

    public static NumberFormat getIntegerFormat(Locale locale, int minDigits, boolean useGrouping)
    {
        return getThreadLocalNumberFormat(INTEGER, locale, -1, -1, minDigits, -1, useGrouping);
    }

    @SuppressWarnings("unchecked")
    private static DateFormat getThreadLocalDateFormat(String key)
    {
        Map formatMap = getThreadLocalFormatMap();
        DateFormat format = (DateFormat) formatMap.get(key);
        if (format == null)
        {
            format = new SimpleDateFormat(key);
            formatMap.put(key, format);
        }
        return format;
    }

    @SuppressWarnings("unchecked")
    private static NumberFormat getThreadLocalNumberFormat(int type, Locale locale, int minFractionDigits, int maxFractionDigits,
                                                           int minIntegerDigits, int maxIntegerDigits, boolean useGrouping)
    {
        Map formatMap = getThreadLocalFormatMap();

        String key = locale.toString() + "|" + minFractionDigits + "|" + minIntegerDigits + "|" + useGrouping;

        NumberFormat format = (NumberFormat) formatMap.get(key);
        if (format == null)
        {
            switch (type)
            {
                case DECIMAL:
                    format = NumberFormat.getNumberInstance();
                    break;
                case INTEGER:
                    format = NumberFormat.getIntegerInstance();
                    break;
                case CURRENCY:
                    format = NumberFormat.getCurrencyInstance(locale);
                    break;
                case PERCENT:
                    format = NumberFormat.getPercentInstance();
                    break;
                default:
                    throw new IllegalStateException();
            }

            if (minFractionDigits >= 0)
                format.setMinimumFractionDigits(minFractionDigits);

            if (maxFractionDigits >= 0)
                format.setMaximumFractionDigits(maxFractionDigits);

            if (minIntegerDigits >= 0)
                format.setMinimumIntegerDigits(minIntegerDigits);

            if (maxIntegerDigits >= 0)
                format.setMaximumIntegerDigits(maxIntegerDigits);

            format.setGroupingUsed(useGrouping);
            formatMap.put(key, format);
        }

        return format;
    }

    @SuppressWarnings("unchecked")
    private static Map getThreadLocalFormatMap()
    {
        Map formatMap = (Map) formatMapHolder.get();
        if (formatMap == null)
        {
            formatMap = new Flat3Map();
            formatMapHolder.set(formatMap);
        }

        return formatMap;
    }

}
