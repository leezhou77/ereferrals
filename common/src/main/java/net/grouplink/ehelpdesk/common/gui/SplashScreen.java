/*
 * SplashScreen.java
 *
 * Created on Oct 1, 2007, 1:38:06 PM
 *
 */

package net.grouplink.ehelpdesk.common.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 *
 * @author mark
 */
public class SplashScreen extends Window {

    private Image splashImage;
    private boolean paintCalled = false;

    /**
     * Creates a new instance of SplashScreen. Centers it on the screen.
     * The user can close the splashscreen by clicking on it.
     *
     * @param owner The frame owning the splash screen.
     * @param splashImage The image to be displayed as the splash screen
     */
    public SplashScreen(Frame owner, Image splashImage) {
        super(owner);
        this.splashImage = splashImage;

        // load the image
        MediaTracker mt = new MediaTracker(this);
        mt.addImage(splashImage, 0);
        try {
            mt.waitForID(0);
        } catch (InterruptedException ignore) {
        }

        // Center the window on the screen.
        int imgWidth = splashImage.getWidth(this);
        int imgHeight = splashImage.getHeight(this);
        setSize(imgWidth, imgHeight);
        Dimension screenDim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((screenDim.width - imgWidth) / 2, (screenDim.height - imgHeight) / 2);

        // Allow users to close the splash window by clicking on its display
        // area. This mouse listener listens for mouse clicks and disposes
        //the splash window.
        MouseAdapter disposeOnClick = new MouseAdapter() {

            public void mouseClicked(MouseEvent evt) {
                // Note: To keep the splash() from hanging,
                // paintCalled must be set to true and notifyAll() called.
                // This is necessary because the mouse click may
                // occur before the content of the window is painted.
                synchronized (SplashScreen.this) {
                    SplashScreen.this.paintCalled = true;
                    SplashScreen.this.notifyAll();
                }
                dispose();
            }
        };
        addMouseListener(disposeOnClick);
    }

    /**
     * Constructs and displays a splash screen.
     * <p>
     *    This method is useful for startup splash screens. Dispose the return
     *    frame to get rid of the splash screen.
     * </p>
     *
     * @param splashImage The image to be displayed
     * @return Returns the frame that owns the splash screen.
     */
    public static Frame splash(Image splashImage) {
        Frame f = new Frame();
        SplashScreen screen = new SplashScreen(f, splashImage);

        // Show the window
        screen.toFront();
        screen.setVisible(true);

        if (!EventQueue.isDispatchThread()) {
            synchronized (screen) {
                try {
                    screen.wait(1000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return f;
    }

    /**
     * Paints the container. This forwards the paint to any lightweight
     * components that are children of this container. If this method is
     * reimplemented, super.paint(g) should be called so that lightweight
     * components are properly rendered. If a child component is entirely
     * clipped by the current clipping setting in g, paint() will not be
     * forwarded to that child.
     *
     *
     * @param g the specified Graphics window
     * @see Component#update(Graphics)
     */
    public void paint(Graphics g) {
        g.drawImage(splashImage, 0, 0, this);
        if (!paintCalled) {
            paintCalled = true;
            synchronized (this) {
                notifyAll();
            }
        }
    }

    /**
     * Updates the container.  This forwards the update to any lightweight
     * components that are children of this container.  If this method is
     * reimplemented, super.update(g) should be called so that lightweight
     * components are properly rendered.  If a child component is entirely
     * clipped by the current clipping setting in g, update() will not be
     * forwarded to that child.
     *
     *
     * @param g the specified Graphics window
     * @see Component#update(Graphics)
     */
    public void update(Graphics g) {
        g.setColor(getForeground());
        paint(g);
    }

    /**
     * Sets the flag to close the splash screen.
     */
    public void close() {
        paintCalled = true;
    }
}