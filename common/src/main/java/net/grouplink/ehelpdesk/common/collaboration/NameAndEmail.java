package net.grouplink.ehelpdesk.common.collaboration;

import java.io.Serializable;

public class NameAndEmail implements Serializable {

	private String displayName;
	private String emailAddress;

	public NameAndEmail() {
		
	}
	public NameAndEmail(String displayName, String emailAddress) {
		setDisplayName(displayName);
		setEmailAddress(emailAddress);
	}
	
	public NameAndEmail(String displayName) {
		setDisplayName(displayName);
	}
	
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

}
