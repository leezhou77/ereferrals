package net.grouplink.ehelpdesk.common.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * User: dgarcia
 * Date: Aug 8, 2007
 * Time: 3:45:50 PM
 */
public class SqlSeed {

    private static List<String> generalSeed;
    private static List<String> othersSeed;
    private static List<String> postgresSeed;

    public static void init(){

        generalSeed = new ArrayList<String>();

        //ROLL (ID, NAME)
        generalSeed.add("INSERT INTO ROLL (ID, NAME) VALUES (1, 'ROLE_ADMINISTRATOR')");
        generalSeed.add("INSERT INTO ROLL (ID, NAME) VALUES (2, 'ROLE_MANAGER')");
        generalSeed.add("INSERT INTO ROLL (ID, NAME) VALUES (3, 'ROLE_TECHNICIAN')");
        generalSeed.add("INSERT INTO ROLL (ID, NAME) VALUES (4, 'ROLE_USER')");
        generalSeed.add("INSERT INTO ROLL (ID, NAME) VALUES (5, 'ROLE_MEMBER')");
        generalSeed.add("INSERT INTO ROLL (ID, NAME) VALUES (6, 'ROLE_WFPARTICIPANT')");

        //TICKETPRIORITY ( ID, NAME )
        generalSeed.add("INSERT INTO TICKETPRIORITY (ID, NAME, ICON, PORDER) values(-3, 'High', 'red', 0)");
        generalSeed.add("INSERT INTO TICKETPRIORITY (ID, NAME, ICON, PORDER) values(-2, 'Medium', 'orange', 0)");
        generalSeed.add("INSERT INTO TICKETPRIORITY (ID, NAME, ICON, PORDER) values(-1, 'Low', 'blank', 0)");

        //APPOINTMENTTYPE ( ID, NAME );
        generalSeed.add("INSERT INTO APPOINTMENTTYPE (ID, NAME) values(1, 'apptType.free')");
        generalSeed.add("INSERT INTO APPOINTMENTTYPE (ID, NAME) values(2, 'apptType.tentative')");
        generalSeed.add("INSERT INTO APPOINTMENTTYPE (ID, NAME) values(3, 'apptType.busy')");
        generalSeed.add("INSERT INTO APPOINTMENTTYPE (ID, NAME) values(4, 'apptType.outofoffice')");

        //CUSTOMFIELDTYPES ( ID , NAME , HTMLTYPE )
//        generalSeed.add("INSERT INTO CUSTOMFIELDTYPES (ID, NAME, HTMLTYPE) VALUES (1, 'customField.textField', 'text')");
//        generalSeed.add("INSERT INTO CUSTOMFIELDTYPES (ID, NAME, HTMLTYPE) VALUES (2, 'customField.textArea', 'textarea')");
//        generalSeed.add("INSERT INTO CUSTOMFIELDTYPES (ID, NAME, HTMLTYPE) VALUES (3, 'customField.radio', 'radio')");
//        generalSeed.add("INSERT INTO CUSTOMFIELDTYPES (ID, NAME, HTMLTYPE) VALUES (4, 'customField.checkbox', 'checkbox')");
//        generalSeed.add("INSERT INTO CUSTOMFIELDTYPES (ID, NAME, HTMLTYPE) VALUES (5, 'customField.dropDown', 'select')");

        //PROPERTIES (ID, NAME, VALUE)
        generalSeed.add("INSERT INTO PROPERTIES (ID, NAME, VALUE) VALUES (1, 'appCustomizationPortalUrl', 'http://www.grouplink.net')");
        generalSeed.add("INSERT INTO PROPERTIES (ID, NAME, VALUE) VALUES (2, 'appCustomizationMarquee', 'Welcome to GroupLink''s eReferrals')");

        postgresSeed = new ArrayList<String>();

        /////////////////////////////////Adding statements for PostgreSql only
        //USERS ( ID , SALUTATION , FIRSTNAME , LASTNAME , MIDDLENAME , EMAIL , LOGINID , PASSWORD , ACTIVE , ONVACATION ,
        //GENDER , CREATIONDATE , CREATOR , COMMENTS , COLLOGINID , COLPASSWORD , LDAPDN )
        postgresSeed.add("INSERT INTO USERS(ID, FIRSTNAME, LASTNAME, EMAIL, LOGINID, PASSWORD, ACTIVE, ONVACATION, CREATOR) VALUES (0, 'SYSTEM', 'ADMIN', 'admin@ehelpdesk.net', 'admin', '21232f297a57a5a743894a0e4a801fc3', true, false, 'SYSTEM')");
        postgresSeed.add("INSERT INTO USERS(ID, FIRSTNAME, LASTNAME, EMAIL, LOGINID, PASSWORD, ACTIVE, ONVACATION, CREATOR) VALUES (-1, 'Anonymous', 'Account', '', 'Anonymous', '5f4dcc3b5aa765d61d8327deb882cf99', false, false, 'SYSTEM')");

        //USERROLE( ID , USERID , ROLEID, ACTIVE)
        postgresSeed.add("INSERT INTO USERROLE(ID, USERID, ROLEID, ACTIVE) VALUES (0, 0, 1, true)");

        //STATUS ( ID , NAME )
        postgresSeed.add("INSERT INTO STATUS (ID, NAME, SORDER, ACTIVE) values(-1, 'Deleted', 0, true)");
        postgresSeed.add("INSERT INTO STATUS (ID, NAME, SORDER, ACTIVE) values(1, 'Awaiting Dispatch', 0, true)");
        postgresSeed.add("INSERT INTO STATUS (ID, NAME, SORDER, ACTIVE) values(2, 'Assigned Not Updated', 0, true)");
        postgresSeed.add("INSERT INTO STATUS (ID, NAME, SORDER, ACTIVE) values(3, 'Work In Progress', 0, true)");
        postgresSeed.add("INSERT INTO STATUS (ID, NAME, SORDER, ACTIVE) values(4, 'On Hold', 0, true)");
        postgresSeed.add("INSERT INTO STATUS (ID, NAME, SORDER, ACTIVE) values(5, 'Resolved', 0, true)");
        postgresSeed.add("INSERT INTO STATUS (ID, NAME, SORDER, ACTIVE) values(6, 'Closed', 0, true)");

        othersSeed = new ArrayList<String>();

        /////////////////////////////////Adding statements for all other databases but PostgreSql
        //USERS ( ID , SALUTATION , FIRSTNAME , LASTNAME , MIDDLENAME , EMAIL , LOGINID , PASSWORD , ACTIVE , ONVACATION ,
        //GENDER , CREATIONDATE , CREATOR , COMMENTS , COLLOGINID , COLPASSWORD , LDAPDN )
        othersSeed.add("INSERT INTO USERS(ID, FIRSTNAME, LASTNAME, EMAIL, LOGINID, PASSWORD, ACTIVE, ONVACATION, CREATOR) VALUES (0, 'SYSTEM', 'ADMIN', 'admin@ehelpdesk.net', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 0, 'SYSTEM')");
        othersSeed.add("INSERT INTO USERS(ID, FIRSTNAME, LASTNAME, EMAIL, LOGINID, PASSWORD, ACTIVE, ONVACATION, CREATOR) VALUES (-1, 'Anonymous', 'Account', '', 'Anonymous', '5f4dcc3b5aa765d61d8327deb882cf99', 0, 0, 'SYSTEM')");

        //USERROLE( ID , USERID , ROLEID, ACTIVE)
        othersSeed.add("INSERT INTO USERROLE(ID, USERID, ROLEID, ACTIVE) VALUES (0, 0, 1, 1)");

        //STATUS ( ID , NAME )
//        othersSeed.add("INSERT INTO STATUS (ID, NAME, SORDER, ACTIVE) values(-2, 'TT Pending', 0, 1)");
//        othersSeed.add("INSERT INTO STATUS (ID, NAME, SORDER, ACTIVE) values(-1, 'Deleted', 0, 1)");
//        othersSeed.add("INSERT INTO STATUS (ID, NAME, SORDER, ACTIVE) values(1, 'Awaiting Dispatch', 0, 1)");
//        othersSeed.add("INSERT INTO STATUS (ID, NAME, SORDER, ACTIVE) values(2, 'Assigned Not Updated', 0, 1)");
        othersSeed.add("INSERT INTO STATUS (ID, NAME, SORDER, ACTIVE) values(3, 'Work In Progress', 0, 1)");
        othersSeed.add("INSERT INTO STATUS (ID, NAME, SORDER, ACTIVE) values(4, 'On Hold', 0, 1)");
        othersSeed.add("INSERT INTO STATUS (ID, NAME, SORDER, ACTIVE) values(5, 'Resolved', 0, 1)");
//        othersSeed.add("INSERT INTO STATUS (ID, NAME, SORDER, ACTIVE) values(6, 'Closed', 0, 1)");

        /*** Moved survey sqls to surveymanagementcontroller.surveyActivate ***/
    }

    public static String[] getSqlSeed(String dialect){
        init();
        List<String> returnList = generalSeed;
        if(dialect.equals("org.hibernate.dialect.PostgreSQLDialect")){
            returnList.addAll(postgresSeed);
        }
        else{
            returnList.addAll(othersSeed);
        }
        String[] result = new String[returnList.size()];
        for(int i=0;i<returnList.size();i++){
            result[i] = returnList.get(i);
        }
        return result;
    }
}
