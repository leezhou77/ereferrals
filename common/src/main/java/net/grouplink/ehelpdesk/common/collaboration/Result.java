/**
 * Date: 4/23/2007
 * @author derickson
 */
package net.grouplink.ehelpdesk.common.collaboration;

import java.io.Serializable;

/**
 * Typically the base class of the result object returned from methods of the
 * collaboration service.
 * 
 * 
 * @author derickson
 */
public class Result implements Serializable {
	private int errorCode = -1;
	private String errorMessage = null;
	
	public Result() {
	}
	
	public Result(int errorCode, String errorMessage) {
		setErrorCode(errorCode);
		setErrorMessage(errorMessage);
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
