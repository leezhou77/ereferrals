package net.grouplink.ehelpdesk.common.utils.ldap;

/**
 * Created by IntelliJ IDEA.
 * User: dgarcia
 * Date: May 31, 2007
 * Time: 3:33:03 AM
 */
public class LdapConstants {

    public static final String ZEN_ASSET_TAG = "zENINVAssetTag";
    public static final String ZEN_BIOS_TYPE = "zENINVBIOSType";
    public static final String ZEN_COMPUTER_MODEL = "zENINVComputerModel";
    public static final String ZEN_COMPUTER_TYPE = "zENINVComputerType";
    public static final String ZEN_DISK_INFO = "zENINVDiskInfo";
    public static final String ZEN_IP_ADDRESS = "zENINVIPAddress";
    public static final String ZEN_LAST_USER = "wMNAMEUser";
    public static final String ZEN_LOGGED_IN_WORKSTATION = "zenwmLoggedInWorkstation";
    public static final String ZEN_MAC_ADDRESS = "zENINVMACAddress";
    public static final String ZEN_MEMORY_SIZE = "zENINVMemorySize";
    public static final String ZEN_MODEL_NUMBER = "zENINVModelNumber";
    public static final String ZEN_NCV = "zENINVNovellClientVersion";
    public static final String ZEN_NIC_TYPE = "zENINVNICType";
    public static final String ZEN_OPERATOR = "operator";
    public static final String ZEN_OS_REVISION = "zENINVOSRevision";
    public static final String ZEN_OS_TYPE = "zENINVOSType";
    public static final String ZEN_PROCESSOR_TYPE = "zENINVProcessorType";
    public static final String ZEN_SERIAL_NUMBER = "zENINVSerialNumber";
    public static final String ZEN_SUBNET_ADDRESS = "zENINVSubNetAddress";
    public static final String ZEN_USER_HISTORY = "wMUserHistory";
    public static final String ZEN_VIDEO_TYPE = "zENINVVideoType";
    public static final String ZEN_WORKSTATION = "wMWorkstation";
	public static final String ZEN_COMPUTER_NAME = "wMNAMEComputer";
}
