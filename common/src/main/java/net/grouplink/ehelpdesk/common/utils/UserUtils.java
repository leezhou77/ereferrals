package net.grouplink.ehelpdesk.common.utils;

import net.grouplink.ehelpdesk.domain.User;

public class UserUtils {

    private static ThreadLocal<User> userRef = new ThreadLocal<User>();

    public static void setLoggedInUser(User user) {
        userRef.set(user);
    }

    public static User getLoggedInUser() {
        return userRef.get();
    }

    public static void unbindUser() {
        userRef.remove();
    }
}
