package net.grouplink.ehelpdesk.common.utils.ldap;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.AutoPopulatingList;

import java.util.ArrayList;
import java.util.List;

public class LdapConfig {

    private String url = "";
    private String base = "";
    private String login = "";
    private String password = "";
    private String changePassword = "";
    private List<String> baseSearchList = new AutoPopulatingList<String>(String.class);
    private Boolean ldapIntegration = Boolean.FALSE;
    private String filter = "";
    private String usernameAttribute;
    private Boolean ehdFallback = Boolean.TRUE;
    private String locationAttribute = "";
    private Boolean createLocation = Boolean.FALSE;

    public String getFilter() {
        if (StringUtils.isBlank(filter)) {
            filter = "(objectClass=*)";
        }

        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public List<String> getBaseSearchList() {
        return baseSearchList;
    }

    public void setBaseSearchList(List<String> baseSearchList) {
        this.baseSearchList = baseSearchList;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getChangePassword() {
        return changePassword;
    }

    public void setChangePassword(String password) {
        this.changePassword = password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getLdapIntegration() {
        return ldapIntegration;
    }

    public void setLdapIntegration(Boolean ldapIntegration) {
        this.ldapIntegration = ldapIntegration;
    }

    public String getUsernameAttribute() {
        return usernameAttribute;
    }

    public void setUsernameAttribute(String usernameAttribute) {
        this.usernameAttribute = usernameAttribute;
    }

    public Boolean getEhdFallback() {
        return ehdFallback;
    }

    public void setEhdFallback(Boolean ehdFallback) {
        this.ehdFallback = ehdFallback;
    }

    public String getLocationAttribute() {
        return locationAttribute;
    }

    public void setLocationAttribute(String locationAttribute) {
        this.locationAttribute = locationAttribute;
    }

    public Boolean getCreateLocation() {
        return createLocation;
    }

    public void setCreateLocation(Boolean createLocation) {
        this.createLocation = createLocation;
    }
}
