/**
 * Date: 4/23/2007
 * @author derickson
 */
package net.grouplink.ehelpdesk.common.collaboration;

/**
 * Result returned after logging in to the collaboration system.
 * 
 * @author derickson
 */
public class LoginResult extends Result {
	private Session session = null;

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}
}
