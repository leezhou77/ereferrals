package net.grouplink.ehelpdesk.common.utils;

import org.apache.commons.lang.StringUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Date: May 6, 2007
 * Time: 11:59:42 PM
 */
public class GLTest {

    private final static String GL_HELPDESK_NET = "9007B80BBB9FDC850A92B08769B2F827";

    private GLTest() {
    }

    public static String getMd5Password(String plaintext) {

        byte[] bytes;
        StringBuilder tempText = null;
        try {
            if (StringUtils.isNotBlank(plaintext)) {
                MessageDigest md5 = MessageDigest.getInstance("MD5");
                bytes = md5.digest(plaintext.getBytes());
                tempText = new StringBuilder();
                for (byte aByte : bytes) {
                    int x = aByte & 0xFF;
                    if (x < 0x10) {
                        tempText.append("0");
                    }
                    tempText.append(Integer.toHexString(x));
                }
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return tempText != null ? tempText.toString() : null;
    }

    public static String encryptPassword(String password) {
        if (StringUtils.isBlank(password)) {
            return "";
        }
        return EncryptionUtils.encrypt(GLTest.GL_HELPDESK_NET, password);
    }

    public static String decryptPassword(String password) {
        if (StringUtils.isBlank(password)) {
            return "";
        }
        return EncryptionUtils.decrypt(GLTest.GL_HELPDESK_NET, password);
    }

    public static void main(String[] args) {
        String test = "";
        System.out.println(GLTest.decryptPassword(test));
    }
}
