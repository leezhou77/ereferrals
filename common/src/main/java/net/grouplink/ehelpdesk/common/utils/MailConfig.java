package net.grouplink.ehelpdesk.common.utils;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Digits;

public class MailConfig {

    public static final String DEF_SMTP_PORT = "25";
    public static final String DEF_SMTP_PORT_SSL = "465";

    public static final String SECURITY_NONE = "none";
    public static final String SECURITY_TLSIFAVAILABLE = "tlsIfAvailable";
    public static final String SECURITY_TLS = "tls";
    public static final String SECURITY_SSL = "ssl";

	private String host = "";
    private String port = "";
    /** Deprecated, use security property instead */
    private Boolean useSsl = Boolean.FALSE;
	private String userName = "";
	private String password = "";
    private String changePassword = "";
    private String email = "";
    private Boolean notifyUser = Boolean.FALSE;
    private Boolean notifyTechnician = Boolean.FALSE;
    private Boolean sendLinkOnNotification = Boolean.FALSE;
    private String emailDisplayName = "";
    private String emailFromAddress = "";
    private String encoding = "";
    private String security;

    @NotEmpty
    public String getHost() {
		return host;
	}

    public void setHost(String host) {
		this.host = host;
	}

    @Range(min = 1, max = 65535)
    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    /**
     * Deprecated. Use security property instead.
     * @return Boolean ssl enabled
     */
    public Boolean getUseSsl() {
        return useSsl;
    }

    /**
     * Deprecated. Use security property instead.
     * @param useSsl enable ssl
     */
    public void setUseSsl(Boolean useSsl) {
        this.useSsl = useSsl;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
		return password;
	}

    public void setPassword(String password) {
		this.password = password;
	}

    public String getChangePassword() {
        return changePassword;
    }

    public void setChangePassword(String password) {
        this.changePassword = password;
    }

    public String getEmail() {
		return email;
	}

    public void setEmail(String email) {
		this.email = email;
	}

    public Boolean getNotifyUser() {
        return notifyUser;
    }

    public void setNotifyUser(Boolean notifyUser) {
        this.notifyUser = notifyUser;
    }

    public Boolean getNotifyTechnician() {
        return notifyTechnician;
    }

    public void setNotifyTechnician(Boolean notifyTechnician) {
        this.notifyTechnician = notifyTechnician;
    }

    public Boolean getSendLinkOnNotification() {
        return sendLinkOnNotification;
    }

    public void setSendLinkOnNotification(Boolean sendLinkOnNotification) {
        this.sendLinkOnNotification = sendLinkOnNotification;
    }

    public String getEmailDisplayName() {
        return emailDisplayName;
    }

    public void setEmailDisplayName(String emailDisplayName) {
        this.emailDisplayName = emailDisplayName;
    }

    @NotEmpty
    @Email
    public String getEmailFromAddress() {
        return emailFromAddress;
    }

    public void setEmailFromAddress(String emailFromAddress) {
        this.emailFromAddress = emailFromAddress;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public String getSecurity() {
        return security;
    }

    public void setSecurity(String security) {
        this.security = security;
    }
}
