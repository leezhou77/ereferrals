/**
 * @author derickson
 * Date: 4/23/2007
 * 
 */
package net.grouplink.ehelpdesk.common.collaboration;

import java.util.Calendar;

public class Task extends CalendarItem {
	private Calendar assignedDate = null;
	private Calendar startDate = null;
	private Calendar dueDate = null;
	private boolean completed = false;
	private String priority = null;
	
	public Calendar getAssignedDate() {
		return assignedDate;
	}
	public void setAssignedDate(Calendar assignedDate) {
		this.assignedDate = assignedDate;
	}
	public boolean isCompleted() {
		return completed;
	}
	public void setCompleted(boolean completed) {
		this.completed = completed;
	}
	public Calendar getDueDate() {
		return dueDate;
	}
	public void setDueDate(Calendar dueDate) {
		this.dueDate = dueDate;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public Calendar getStartDate() {
		return startDate;
	}
	public void setStartDate(Calendar startDate) {
		this.startDate = startDate;
	}
}
