package net.grouplink.ehelpdesk.common.utils;


public class KBSearch {

    private String searchText = "";
    private int searchField = 0;
    
    public int getSearchField() {
        return searchField;
    }
    
    public void setSearchField(int searchField) {
        this.searchField = searchField;
    }
    
    public String getSearchText() {
        return searchText;
    }
    
    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }
}
