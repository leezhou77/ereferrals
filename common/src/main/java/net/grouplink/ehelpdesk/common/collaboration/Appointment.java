/**
 * @author derickson
 * Date: 4/23/2007
 */
package net.grouplink.ehelpdesk.common.collaboration;

import java.util.Calendar;

public class Appointment extends CalendarItem {
	private String place;
	private Calendar startDate = null;
	private Calendar endDate = null;
	private boolean allDayEvent = false;
	private boolean alarmEnabled = false;
	private int alarmTimeInMinutes = 0;
	private String acceptLevel = null;
	
	public String getAcceptLevel() {
		return acceptLevel;
	}

	public void setAcceptLevel(String acceptLevel) {
		this.acceptLevel = acceptLevel;
	}

	public boolean isAlarmEnabled() {
		return alarmEnabled;
	}

	public void setAlarmEnabled(boolean alarmEnabled) {
		this.alarmEnabled = alarmEnabled;
	}

	public int getAlarmTimeInMinutes() {
		return alarmTimeInMinutes;
	}

	public void setAlarmTimeInMinutes(int alarmTimeInMinutes) {
		this.alarmTimeInMinutes = alarmTimeInMinutes;
	}

	public boolean isAllDayEvent() {
		return allDayEvent;
	}

	public void setAllDayEvent(boolean allDayEvent) {
		this.allDayEvent = allDayEvent;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public Calendar getEndDate() {
		return endDate;
	}

	public void setEndDate(Calendar endDate) {
		this.endDate = endDate;
	}

	public Calendar getStartDate() {
		return startDate;
	}

	public void setStartDate(Calendar startDate) {
		this.startDate = startDate;
	}
}
