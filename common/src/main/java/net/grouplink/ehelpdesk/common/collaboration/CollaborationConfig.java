package net.grouplink.ehelpdesk.common.collaboration;

public class CollaborationConfig {

	private boolean groupwiseIntegration = false;
	private boolean groupwiseIntegration6x = false;
    private boolean outlookIntegration = false;

    private String soapUrl = null;
	private boolean soapSsl = false;
	private String trustedAppName = null;
	private String trustedAppKey = null;

    public boolean isGroupwiseIntegration6x() {
        return groupwiseIntegration6x;
    }

    public void setGroupwiseIntegration6x(boolean groupwiseIntegration6x) {
        this.groupwiseIntegration6x = groupwiseIntegration6x;
    }

    public boolean isGroupwiseIntegration() {
		return groupwiseIntegration;
	}

    public void setGroupwiseIntegration(boolean groupwiseIntegration) {
		this.groupwiseIntegration = groupwiseIntegration;
	}

    public boolean isOutlookIntegration() {
		return outlookIntegration;
	}

    public void setOutlookIntegration(boolean outlookIntegration) {
		this.outlookIntegration = outlookIntegration;
	}

    public boolean isSoapSsl() {
		return soapSsl;
	}

    public void setSoapSsl(boolean soapSsl) {
		this.soapSsl = soapSsl;
	}

    public String getSoapUrl() {
		return soapUrl;
	}

    public void setSoapUrl(String soapUrl) {
		this.soapUrl = soapUrl;
	}

    public String getTrustedAppKey() {
		return trustedAppKey;
	}

    public void setTrustedAppKey(String trustedAppKey) {
		this.trustedAppKey = trustedAppKey;
	}

    public String getTrustedAppName() {
		return trustedAppName;
	}

    public void setTrustedAppName(String trustedAppName) {
		this.trustedAppName = trustedAppName;
	}
}
