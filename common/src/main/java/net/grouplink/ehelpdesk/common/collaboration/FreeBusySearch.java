package net.grouplink.ehelpdesk.common.collaboration;

import java.util.Date;

public class FreeBusySearch {
	private String recipients = null;
	private Date startDate = null;
	private Date startTime = null;
	private Date endDate = null;
	private Date endTime = null;
	
	public String getRecipients() {
		if (recipients == null) {
			recipients = "";
		}
		return recipients;
	}
	public void setRecipients(String recipients) {
		this.recipients = recipients;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
}