package net.grouplink.ehelpdesk.service;

import org.springframework.test.AbstractDependencyInjectionSpringContextTests;
import org.hibernate.SessionFactory;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Jan 9, 2008
 * Time: 12:26:47 PM
 */
public class UserRoleServiceTest extends AbstractDependencyInjectionSpringContextTests {

    private UserRoleService userRoleService;
    private SessionFactory sessionFactory;

    protected String[] getConfigLocations() {
        return new String[]{"classpath:net/grouplink/ehelpdesk/common/appContext.xml",
                            "classpath:net/grouplink/ehelpdesk/common/appContext-db.xml",
                            "classpath:net/grouplink/ehelpdesk/service/appContext-service.xml",
                            "classpath:net/grouplink/ehelpdesk/dao/hibernate/appContext-dao.xml"};
    }

    public void testDummy() {

    }

    public void setUserRoleService(UserRoleService userRoleService) {
        this.userRoleService = userRoleService;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
