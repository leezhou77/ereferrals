package net.grouplink.ehelpdesk.service;

import org.springframework.test.AbstractDependencyInjectionSpringContextTests;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.hibernate.SessionFactory;
import org.hibernate.Session;

import java.util.List;
import java.util.Iterator;
import java.util.Locale;

import net.grouplink.ehelpdesk.domain.Role;

/**
 * @author mrollins
 * @version 1.0
 */
public class RoleServiceTest extends AbstractDependencyInjectionSpringContextTests {

    private RoleService roleService;
    private SessionFactory sessionFactory;

    protected String[] getConfigLocations() {
        return new String[]{"classpath:net/grouplink/ehelpdesk/common/appContext.xml",
                "classpath:net/grouplink/ehelpdesk/common/appContext-db.xml",
                "classpath:net/grouplink/ehelpdesk/service/appContext-service.xml",
                "classpath:net/grouplink/ehelpdesk/dao/hibernate/appContext-dao.xml"};
    }


    public void testGetRoles(){
        List roles = roleService.getRoles();
        assertNotNull(roles);
        for (Iterator iterator = roles.iterator(); iterator.hasNext();) {
            Object obj = iterator.next();
            assertTrue(obj instanceof Role);
        }

        List roleNames = roleService.getRoleNames(Locale.US);
        for (Iterator iterator = roleNames.iterator(); iterator.hasNext();) {
            Object obj = iterator.next();
            assertTrue(obj instanceof String);

        }
    }

    // onSetup and onTearDown  are overridden to get the hibernate session which allows
    // the tests to use lazily initialized objects
    protected void onSetUp() throws Exception {
        super.onSetUp();
        Session s = sessionFactory.openSession();
        TransactionSynchronizationManager.bindResource(sessionFactory, new SessionHolder(s));
    }

    protected void onTearDown() throws Exception {
        super.onTearDown();
        SessionHolder holder = (SessionHolder) TransactionSynchronizationManager.getResource(sessionFactory);
	    Session s = holder.getSession();
	    s.flush();
	    TransactionSynchronizationManager.unbindResource(sessionFactory);
	    SessionFactoryUtils.releaseSession(s, sessionFactory);
    }
    
    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
