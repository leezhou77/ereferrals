package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.service.AbstractServiceIntegrationTest;
import net.grouplink.ehelpdesk.domain.asset.AssetStatus;

/**
 * User: jaymehafen
 * Date: Jul 31, 2008
 * Time: 11:52:54 AM
 */
public class AssetStatusServiceTest extends AbstractServiceIntegrationTest {
    private AssetStatusService assetStatusService;

    public void testAddAssetStatus() {
        AssetStatus as = new AssetStatus();
        as.setName("test asset status name");
        as.setDescription("test asset status description");
        assetStatusService.saveAssetStatus(as);
        assertNotNull(as.getId());
    }

    public void setAssetStatusService(AssetStatusService assetStatusService) {
        this.assetStatusService = assetStatusService;
    }
}
