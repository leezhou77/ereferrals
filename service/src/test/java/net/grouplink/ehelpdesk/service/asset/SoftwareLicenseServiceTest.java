package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.SoftwareLicense;
import net.grouplink.ehelpdesk.service.AbstractServiceIntegrationTest;

/**
 * User: jaymehafen
 * Date: Jul 8, 2008
 * Time: 4:28:06 PM
 */
public class SoftwareLicenseServiceTest extends AbstractServiceIntegrationTest {
    private SoftwareLicenseService softwareLicenseService;
    private AssetService assetService;

    /*public void testSetupTestData() {
        SoftwareLicense s = new SoftwareLicense();
        s.setNote("test software license 1");
        s.setNumberOfLicenses(new Integer(50));
        s.setProductName("everything HelpDesk");
        s.setProductVersion("8.1");


        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2008);
        cal.set(Calendar.MONTH, 7);
        cal.set(Calendar.DATE, 14);
        s.setPurchaseDate(cal.getTime());

        cal.add(Calendar.YEAR, 1);
        s.setSupportExpirationDate(cal.getTime());

        Vendor v = vendorService.getById(new Integer(1));
        s.setVendor(v);

        softwareLicenseService.saveSoftwareLicense(s);
        setComplete();
    }*/

    public void testAddSoftwareLicense() {
        SoftwareLicense sl = new SoftwareLicense();
        sl.setProductName("Microsoft Office");
        softwareLicenseService.saveSoftwareLicense(sl);
        assertNotNull(sl.getId());

        sl = softwareLicenseService.getById(sl.getId());
        assertEquals("Microsoft Office", sl.getProductName());
    }

    public void testSoftwareLicenseInstalledOnSet() {
        SoftwareLicense sl = softwareLicenseService.getById(new Integer(1));
        assertTrue(sl.getInstalledOnAssets().size() == 1);
        Asset a = (Asset) sl.getInstalledOnAssets().iterator().next();
        assertEquals("test asset 1", a.getName());
    }

    public void testAssetSoftwareLicensesSet() {
        Asset a = assetService.getAssetById(new Integer(1));
        assertTrue(a.getSoftwareLicenses().size() == 1);
        SoftwareLicense s = (SoftwareLicense) a.getSoftwareLicenses().iterator().next();
        assertEquals("eReferrals", s.getProductName());
    }

    public void setSoftwareLicenseService(SoftwareLicenseService softwareLicenseService) {
        this.softwareLicenseService = softwareLicenseService;
    }

    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }
}
