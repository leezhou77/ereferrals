package net.grouplink.ehelpdesk.service;

import org.springframework.test.AbstractDependencyInjectionSpringContextTests;
import org.hibernate.SessionFactory;

import java.util.List;

import net.grouplink.ehelpdesk.domain.Group;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Jan 8, 2008
 * Time: 3:19:10 PM
 */
public class GroupServiceTest extends AbstractDependencyInjectionSpringContextTests {

    private GroupService groupService;
    //private UserRoleService userRoleService;
    private SessionFactory sessionFactory;

    private static final int DEF_MANAGER_ID = 0;
    private static final int DEF_TECH_ID = 0;

    protected String[] getConfigLocations() {
        return new String[]{"classpath:net/grouplink/ehelpdesk/common/appContext.xml",
                            "classpath:net/grouplink/ehelpdesk/common/appContext-db.xml",
                            "classpath:net/grouplink/ehelpdesk/service/appContext-service.xml",
                            "classpath:net/grouplink/ehelpdesk/dao/hibernate/appContext-dao.xml"};
    }


    public void testGetGroups() {
        List groups = groupService.getGroups();
        assertNotNull(groups);
        if (groups.size() > 0) {
            Integer groupcount = groupService.getGroupCount();
            assertNotNull(groupcount);
            if (groupcount.intValue() > 0) {
                Group group = (Group) groups.get(0);
                assertNotNull(group);
                Integer groupid = group.getId();
                assertNotNull(groupid);
                assertNotNull(groupService.getGroupById(groupid));
                String groupname = group.getName();
                assertNotNull(groupname);
                assertNotNull(groupService.getGroupByName(groupname));
            }
        }
    }

//    public void test() {
//
//        userRoleService.
//        groupService.getGroupsByManagerId();
//    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

//    public void setUserRoleService(UserRoleService userRoleService) {
//        this.userRoleService = userRoleService;
//    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}


