package net.grouplink.ehelpdesk.service.mail;

import net.grouplink.ehelpdesk.service.AbstractServiceIntegrationTest;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: jaymehafen
 * Date: Jan 26, 2008
 * Time: 11:26:27 PM
 */
public class EmailToTicketServiceTest extends AbstractServiceIntegrationTest {

    EmailToTicketService emailToTicketService;

//    public void testProcessIncomingMail() {
//        emailToTicketService.processIncomingMail();
//    }

    public void testDummy() {
        
    }

    public void testSubjectRegex() {
        String subject = "RE: Ticket # 25731 - Career Development credit {TID:25731}";
        String tidRegex = "\\{TID:(\\d+)\\}";
        Pattern tidPattern = Pattern.compile(tidRegex);
        Matcher matcher = tidPattern.matcher(subject);
        boolean found = matcher.find();
        Integer ticketId = Integer.valueOf(matcher.group(1));
        assertEquals(Integer.valueOf(25731), ticketId);
    }

    public void setEmailToTicketService(EmailToTicketService emailToTicketService) {
        this.emailToTicketService = emailToTicketService;
    }

}
