package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.service.AbstractServiceIntegrationTest;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.domain.asset.AssetFilter;
import net.grouplink.ehelpdesk.domain.User;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Sep 16, 2008
 * Time: 12:06:18 PM
 */
public class AssetFilterServiceTest extends AbstractServiceIntegrationTest {
    private AssetFilterService assetFilterService;
    private UserService userService;

    public void testGetAssets() {
        AssetFilter filter = new AssetFilter();

        User user = getTestUser();
        List assets = assetFilterService.getAssets(filter, user);
        assertNotNull(assets);
    }

    public void setAssetFilterService(AssetFilterService assetFilterService) {
        this.assetFilterService = assetFilterService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public User getTestUser() {
        return userService.getUserById(User.ADMIN_ID);
    }
}
