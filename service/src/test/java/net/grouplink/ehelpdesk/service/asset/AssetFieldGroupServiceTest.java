package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.service.AbstractServiceIntegrationTest;
import net.grouplink.ehelpdesk.domain.asset.AssetFieldGroup;
import net.grouplink.ehelpdesk.domain.asset.AssetType;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Jul 17, 2008
 * Time: 1:32:19 PM
 */
public class AssetFieldGroupServiceTest extends AbstractServiceIntegrationTest {
    private AssetTypeService assetTypeService;
    private AssetFieldGroupService assetFieldGroupService;

    public void testAddAssetFieldGroup() {
        AssetFieldGroup afg = new AssetFieldGroup();

//        AssetType assetType = assetTypeService.getById(new Integer(1));
//        afg.setAssetType(assetType);
        afg.setDescription("asset field group description");
        afg.setName("asset field group 1");
        afg.setTabOrder(new Integer(10));
        assetFieldGroupService.saveAssetFieldGroup(afg);
        assertNotNull(afg.getId());
    }

//    public void testGetAssetFieldGroupsByAssetType() {
//        AssetType at = assetTypeService.getById(new Integer(1));
//        List/*<AssetFieldGroup>*/ assetFieldGroups = assetFieldGroupService.getByAssetType(at);
//        assertTrue(assetFieldGroups.size() > 0);
//
//    }

    public void testGetAssetById() {
        AssetFieldGroup afg = assetFieldGroupService.getById(new Integer(1));
        assertNotNull(afg);
        assertEquals("test asset field group 1", afg.getName());
        assertEquals("description of test asset field group 1", afg.getDescription());
        assertEquals(new Integer(10), afg.getTabOrder());
    }

    public void setAssetTypeService(AssetTypeService assetTypeService) {
        this.assetTypeService = assetTypeService;
    }

    public void setAssetFieldGroupService(AssetFieldGroupService assetFieldGroupService) {
        this.assetFieldGroupService = assetFieldGroupService;
    }
}
