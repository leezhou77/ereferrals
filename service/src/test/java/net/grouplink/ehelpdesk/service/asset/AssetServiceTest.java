package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.domain.asset.ImprovedAccountingInfo;
import net.grouplink.ehelpdesk.service.AbstractServiceIntegrationTest;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.AssetType;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Jul 8, 2008
 * Time: 11:44:58 AM
 */
public class AssetServiceTest extends AbstractServiceIntegrationTest {
    private AssetService assetService;
    private AssetTypeService assetTypeService;

    public void testGetAllAsset() {
        List/*<Asset>*/ allAssets = assetService.getAllAssets();
        assertNotNull(allAssets);
    }

    public void testAddAsset() {
        Asset asset = new Asset();
        asset.setName("asset 1");
        asset.setAssetNumber("asset number 1");

        AssetType at = assetTypeService.getById(1);
        asset.setAssetType(at);

        asset.setAccountingInfo(new ImprovedAccountingInfo());

        assetService.saveAsset(asset);
        assertNotNull(asset.getId());
    }

    public void testGetAssetsWithLeaseExpirationDate() {
        List l = assetService.getAssetsWithLeaseExpirationDate();
        assertNotNull(l);
    }


    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }

    public void setAssetTypeService(AssetTypeService assetTypeService) {
        this.assetTypeService = assetTypeService;
    }
}
