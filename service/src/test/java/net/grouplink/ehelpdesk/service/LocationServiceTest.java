package net.grouplink.ehelpdesk.service;

import org.springframework.test.AbstractDependencyInjectionSpringContextTests;
import org.hibernate.SessionFactory;

import java.util.List;

import net.grouplink.ehelpdesk.domain.Location;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Jan 8, 2008
 * Time: 3:32:48 PM
 */
public class LocationServiceTest extends AbstractDependencyInjectionSpringContextTests {

    private LocationService locationService;
    private SessionFactory sessionFactory;

    protected String[] getConfigLocations() {
        return new String[]{"classpath:net/grouplink/ehelpdesk/common/appContext.xml",
                            "classpath:net/grouplink/ehelpdesk/common/appContext-db.xml",
                            "classpath:net/grouplink/ehelpdesk/service/appContext-service.xml",
                            "classpath:net/grouplink/ehelpdesk/dao/hibernate/appContext-dao.xml"};
    }

    public void testGetLocations() {
        List locations = locationService.getLocations();
        assertNotNull(locations);
        if (locations.size() > 0) {
            Location location = (Location) locations.get(0);
            assertNotNull(location);
            Integer locid = location.getId();
            assertNotNull(locid);
            assertNotNull(locationService.getLocationById(locid));
            String locname = location.getName();
            assertNotNull(locname);
            assertNotNull(locationService.getLocationByName(locname));
        }
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
