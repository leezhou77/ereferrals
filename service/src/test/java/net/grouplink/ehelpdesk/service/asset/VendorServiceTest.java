package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.service.AbstractServiceIntegrationTest;
import net.grouplink.ehelpdesk.domain.asset.Vendor;

/**
 * User: jaymehafen
 * Date: Jul 14, 2008
 * Time: 11:40:52 AM
 */
public class VendorServiceTest extends AbstractServiceIntegrationTest {
    private VendorService vendorService;

    public void testAddVendor() {
        Vendor v = new Vendor();
        v.setName("test vendor");
        vendorService.saveVendor(v);
        assertNotNull(v.getId());
//        super.setComplete();
    }

    public void testGetVendor() {
        Vendor v = vendorService.getById(new Integer(1));
        assertNotNull(v);
        assertEquals("test vendor 1", v.getName());
    }

    public void setVendorService(VendorService vendorService) {
        this.vendorService = vendorService;
    }
}
