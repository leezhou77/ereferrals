package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.*;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Jan 26, 2008
 * Time: 10:50:55 PM
 */
public class EmailToTicketConfigServiceTest extends AbstractServiceIntegrationTest {

    EmailToTicketConfigService emailToTicketConfigService;
    GroupService groupService;
    private LocationService locationService;
    private TicketPriorityService ticketPriorityService;
    private TicketService ticketService;
    private UserService userService;
    private UserRoleGroupService userRoleGroupService;

    public void testCreateEmailToTicketConfig() {
        EmailToTicketConfig config = createE2TConfig();
        emailToTicketConfigService.save(config);
        assertNotNull(config.getId());
    }

    private EmailToTicketConfig createE2TConfig() {
        EmailToTicketConfig config = new EmailToTicketConfig();
        config.setAllowNonUsers(true);
        config.setEnabled(true);
        config.setGroup(getE2TGroup());
        config.setLocation(getE2TLocation());
        config.setImapFolder("INBOX");
        config.setUserName("e2ttest1");
        config.setPassword("grouplink");
        config.setPort(new Integer(143));
        config.setServerName("10.10.1.12");
        config.setServerType(EmailToTicketConfig.SERVER_TYPE_IMAP);
        config.setUseSsl(false);
        config.setReplyDisplayName("ehd");
        config.setReplyEmailAddress("e2ttest1@grouplink.net");
        config.setTicketPriority(getE2TTicketPriority());
        return config;
    }

    public void testDelete() {
        // create e2tconfig
        EmailToTicketConfig config = createE2TConfig();
        emailToTicketConfigService.save(config);

        // create ticket
        Ticket t = new Ticket();
        t.setEmailToTicketConfig(config);

        Location location = locationService.getLocationByName("High School 1");
        Group group = groupService.getGroupByName("Facilities");
        List urgs = userRoleGroupService.getUserRoleGroup();

        t.setGroup(group);
        t.setLocation(location);
        t.setAssignedTo((UserRoleGroup) urgs.get(0));

        User u = userService.getUserById(new Integer(3));
        ticketService.saveTicket(t, u);

        // try to delete the e2tconfig.. this should fail
        emailToTicketConfigService.delete(config, u);

    }

    private TicketPriority getE2TTicketPriority() {
        return ticketPriorityService.getPriorityById(Integer.valueOf("-2"));
    }

    private Location getE2TLocation() {
        return locationService.getLocationByName("EmailToTicket Location 1");
    }

    private Group getE2TGroup() {
        return groupService.getGroupByName("EmailToTicketGroup");
    }

    public void setEmailToTicketConfigService(EmailToTicketConfigService emailToTicketConfigService) {
        this.emailToTicketConfigService = emailToTicketConfigService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

    public void setTicketPriorityService(TicketPriorityService ticketPriorityService) {
        this.ticketPriorityService = ticketPriorityService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }
}
