package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.TicketTemplateLaunchDao;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketTemplateLaunch;
import net.grouplink.ehelpdesk.domain.TicketTemplateMaster;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.WorkflowStep;
import net.grouplink.ehelpdesk.domain.WorkflowStepStatus;
import net.grouplink.ehelpdesk.domain.CustomField;
import net.grouplink.ehelpdesk.domain.CustomFieldValues;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;

import javax.mail.MessagingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.Date;
import java.util.Set;
import java.io.UnsupportedEncodingException;

public class TicketTemplateLaunchServiceImpl implements TicketTemplateLaunchService {

    private Log log = LogFactory.getLog(getClass());

    private TicketTemplateLaunchDao ticketTemplateLaunchDao;
    private TicketService ticketService;
    private WorkflowStepStatusService workflowStepStatusService;
    private PermissionService permissionService;

    public TicketTemplateLaunch getById(Integer id) {
        return ticketTemplateLaunchDao.getById(id);
    }

    public void save(TicketTemplateLaunch ticketTemplateLaunch) {
        ticketTemplateLaunchDao.save(ticketTemplateLaunch);
    }

    public void delete(TicketTemplateLaunch ticketTemplateLaunch) {
        ticketTemplateLaunchDao.delete(ticketTemplateLaunch);
    }

    public List<Ticket> readyForActivation(TicketTemplateLaunch ttLaunch) {
        List<Ticket> errorTickets = new ArrayList<Ticket>();
        for (Ticket ticket : ttLaunch.getTickets()) {
            if (!ticketReadyForActivation(ticket)) {
                errorTickets.add(ticket);
            }
        }

        return errorTickets;
    }

    private boolean ticketReadyForActivation(Ticket ticket) {
        log.debug("called ticketReadyForActivation for ticket " + ticket.getId());

        boolean ready = true;
        if (ticket.getLocation() == null) {
            ready = false;
            log.debug("location is null");
        }
        if (ticket.getGroup() == null) {
            ready = false;
            log.debug("group is null");
        }
        if (ticket.getAssignedTo() == null) {
            ready = false;
            log.debug("assignedTo is null");
        }
        if (ticket.getContact() == null) {
            ready = false;
            log.debug("contact is null");
        }
        if (ticket.getStatus() == null) {
            ready = false;
            log.debug("status is null");
        }
        if (ticket.getPriority() == null) {
            ready = false;
            log.debug("priority is null");
        }
        if (StringUtils.isBlank(ticket.getSubject())) {
            ready = false;
            log.debug("subject is blank");
        }

        if (ticket.getCategoryOption() != null) {
            // check for required custom fields
            Set<CustomField> customFields = ticket.getCategoryOption().getCustomFields();
            for (CustomField customField : customFields) {
                if (customField.isActive() && customField.isRequired()) {
                    CustomFieldValues customFieldValues = ticketService.getCustomFieldValue(ticket, customField.getName());
                    if (customFieldValues == null || StringUtils.isBlank(customFieldValues.getFieldValue())) {
                        ready = false;
                        log.debug("customFieldValue is blank for required custom field " + customField.getName() + ", customField.id=" + customField.getId());
                    }
                }
            }
        }

        log.debug("ready for activation:" + ready);

        return ready;
    }

    public void activate(TicketTemplateLaunch ticketTemplateLaunch, User user) {
        log.debug("called activate for ticketTemplateLaunch: " + ticketTemplateLaunch.getId() + ", user: " + user.getLoginId());
        log.debug("checking if tickets are ready for activation");
        List<Ticket> errorTickets = readyForActivation(ticketTemplateLaunch);
        if (CollectionUtils.isNotEmpty(errorTickets)) {
            log.error("ticketTemplateLaunch contains one or more tickets that are not ready for activation");
            for (Ticket errorTicket : errorTickets) {
                log.debug("ticket " + errorTicket.getId() + " not ready for activation");
            }
            
            throw new RuntimeException("TicketTemplateLaunch id " + ticketTemplateLaunch + " contains Tickets that cannot be launched");
        }

        log.debug("workflow enabled: " + ticketTemplateLaunch.getTicketTemplateMaster().getEnableWorkflow());
        if (ticketTemplateLaunch.getTicketTemplateMaster().getEnableWorkflow()) {
            // workflow
            // get first workflow step
            WorkflowStep firstStep = ticketTemplateLaunch.getTicketTemplateMaster().getWorkflowSteps().get(0);
            // set all tickets to deleted status except those with workflowStepStatus.workflowStep == firstStep
            for (Ticket ticket : ticketTemplateLaunch.getTickets()) {

                if (ticket.getWorkflowStepStatus() == null) {
                    log.debug("ticket " + ticket.getId() + " does not have a workflow step status, skipping");
                    continue;
                }

                if (ticket.getWorkflowStepStatus().getWorkflowStep().equals(firstStep)) {
                    log.debug("ticket " + ticket.getId() + " is a ticket in the first step of the workflow, setting to active status");
                    ticket.setTicketTemplateStatus(Ticket.TT_STATUS_ACTIVE);
                    ticket.setCreatedDate(null);
                    ticketService.saveTicket(ticket, user);
                    ticketService.flush();
                    String htmlComments = ticketService.createHTMLComments(ticket, null);
                    String textComments = ticketService.createTextComments(ticket, null);
                    // setting the notify flags to null will cause them to use the default notifiation rules
                    ticket.setNotifyTech(null);
                    ticket.setNotifyUser(null);
                    log.debug("sending email notification");
                    try {
                        ticketService.sendEmailNotification(ticket, user, htmlComments, textComments);
                    } catch (MessagingException e) {
                        log.error("error sending email notification for ticket " + ticket.getId(), e);
                    } catch (UnsupportedEncodingException e) {
                        log.error("error sending email notification for ticket " + ticket.getId(), e);
                    }
                }
            }
        } else {
            // no workflow
            // set all ticket to initial status
            for (Ticket ticket : ticketTemplateLaunch.getTickets()) {
                log.debug("setting ticket " + ticket.getId() + " to active status");
                ticket.setTicketTemplateStatus(Ticket.TT_STATUS_ACTIVE);
                ticket.setCreatedDate(null);
                ticketService.saveTicket(ticket, user);
                ticketService.flush();
                String htmlComments = ticketService.createHTMLComments(ticket, null);
                String textComments = ticketService.createTextComments(ticket, null);
                // setting the notify flags to null will cause them to use the default notifiation rules
                ticket.setNotifyTech(null);
                ticket.setNotifyUser(null);
                log.debug("sending email notification");
                try {
                    ticketService.sendEmailNotification(ticket, user, htmlComments, textComments);
                } catch (MessagingException e) {
                    log.error("error sending email notification for ticket " + ticket.getId(), e);
                } catch (UnsupportedEncodingException e) {
                    log.error("error sending email notification for ticket " + ticket.getId(), e);
                }
            }
        }

        // set launch status to active
        log.debug("setting ticketTemplateLaunch to active status");
        ticketTemplateLaunch.setStatus(TicketTemplateLaunch.STATUS_ACTIVE);
        save(ticketTemplateLaunch);
    }

    public void abort(TicketTemplateLaunch launch) {
        log.debug("called abort for launch " + launch.getId());
        // delete tickets
        for (Iterator<Ticket> i = launch.getTickets().iterator(); i.hasNext();) {
            Ticket ticket = i.next();
            log.debug("purging ticket " + ticket.getId());
            ticketService.purgeTicket(ticket);
            i.remove();
        }

        // delete workflowstepstatus
        if (CollectionUtils.isNotEmpty(launch.getWorkflowStepStatuses())) {
            log.debug("deleting workflowStepStatuses");
            for (Iterator<WorkflowStepStatus> i = launch.getWorkflowStepStatuses().iterator(); i.hasNext();) {
                WorkflowStepStatus workflowStepStatus = i.next();
                workflowStepStatusService.delete(workflowStepStatus);
                i.remove();
            }
        }

        // delete tt launch
        log.debug("deleting ticketTemplateLaunch");
        delete(launch);
    }

    public int getLaunchCountByTemplateMaster(TicketTemplateMaster ttm) {
        return ticketTemplateLaunchDao.getLaunchCountByTemplateMaster(ttm);
    }

    public List<TicketTemplateLaunch> getPendingOlderThan(Date activeDate) {
        return ticketTemplateLaunchDao.getPendingOlderThan(activeDate);
    }

    public boolean userHasLaunchPermission(User user, TicketTemplateMaster master) {
            if (!master.isPrivified()) return true;
            if (permissionService.hasGroupPermission(user, "group.ticketTemplateManagement", master.getGroup())) return true;
            if (permissionService.hasGroupPermission(user, "group.launchNonPublicTicketTemplates", master.getGroup())) return true;

            return false;
        }

    public void setTicketTemplateLaunchDao(TicketTemplateLaunchDao ticketTemplateLaunchDao) {
        this.ticketTemplateLaunchDao = ticketTemplateLaunchDao;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setWorkflowStepStatusService(WorkflowStepStatusService workflowStepStatusService) {
        this.workflowStepStatusService = workflowStepStatusService;
    }

    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }
}
