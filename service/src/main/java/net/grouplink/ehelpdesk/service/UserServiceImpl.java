package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.common.utils.UserUtils;
import net.grouplink.ehelpdesk.common.utils.ldap.LdapConfig;
import net.grouplink.ehelpdesk.dao.UserDao;
import net.grouplink.ehelpdesk.domain.*;
import net.grouplink.ehelpdesk.common.utils.GLTest;
import net.grouplink.ehelpdesk.common.utils.UserSearch;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Implements a User service.
 *
 * @author mrollins
 * @version 1.0
 * @see UserService
 */
//@WebService(endpointInterface = "net.grouplink.ehelpdesk.service.UserService")
public class UserServiceImpl implements UserService {

    private Log log = LogFactory.getLog(getClass());

    private UserDao userDao;
    private RoleService roleService;
    private UserRoleGroupService userRoleGroupService;
    private LdapService ldapService;
    private LocationService locationService;

    public User getUserById(Integer id) {
        User user = userDao.getUserById(id);

        // set old user property for comparing old and new values
        try {
            user.setOldUser(user.clone());
        } catch (CloneNotSupportedException e) {
            log.error("error cloning user", e);
        }

        return user;
    }

    public User getUserByUsername(String username) {
        return userDao.getUserByUsername(username);
    }

    public User getUserByUsernameAndPassword(String username, String password) {
        return userDao.getUserByUsernameAndPassword(username, password);
    }

    public User getUserByEmail(String email) {
        return userDao.getUserByEmail(email);
    }

    public String resetUserPassword(User user) {
        String newPassword = RandomStringUtils.randomAlphanumeric(6);
        user.setPassword(GLTest.getMd5Password(newPassword));
        saveUser(user);
        return newPassword;
    }

    public List<User> getUsers() {
        return userDao.getUsers();
    }

    public List<User> getActiveUsers() {
        return userDao.getActiveUsers();
    }

    public void saveUser(User user) {
        userDao.saveUser(user);
    }

    public boolean deleteUser(User user) {
        if (userInUse(user)) {
            inActivateUser(user);
            return false;
        } else {
            // clear out the userrolegroupnotifications
            userRoleGroupService.clearNotificationsForUser(user);
            userDao.deleteUser(user);
            return true;
        }
    }

    public boolean isEndUser(User user) {
        user = getUserById(user.getId());
        Set userRoles = user.getUserRoles();
        for (Object userRole1 : userRoles) {
            UserRole userRole = (UserRole) userRole1;
            if (userRole.getRole() != null && (userRole.getRole().getId().equals(Role.ROLE_ADMINISTRATOR_ID) ||
                    userRole.getRole().getId().equals(Role.ROLE_MANAGER_ID) ||
                    userRole.getRole().getId().equals(Role.ROLE_TECHNICIAN_ID))) return false;
        }
        return true;
    }

    public List<User> searchUser(UserSearch userSearch) {
        return userDao.searchUser(userSearch);
    }

    public void removePhone(Phone phone) {
        userDao.removePhone(phone);
    }

    public void removeAddress(Address address) {
        userDao.removeAddress(address);
    }

    public User synchronizedUserWithLdap(User user, String password, LdapUser ldapUser) {

        if (ldapUser == null) throw new IllegalArgumentException("ldapUser cannot be null");

        log.debug("called UserServiceImpl.synchronizedUserWithLdap, user = " + (user == null ? "null" : user.getId()) +
                ", ldapUser.dn = " + ldapUser.getDn() + ", ldapUser.cn = " + ldapUser.getCn());

        // Verifying if the user exists or not
        if (user == null) {
            log.debug("user is null");
            //Looking for user if may be in the db with a different role
            log.debug("looking up user by username (cn)");
            user = getUserByUsername(ldapUser.getCn());
            log.debug("user: " + (user == null ? "null" : user.getId()));
            //Try to see is user excists with email as username
            if (user == null && StringUtils.isNotBlank(ldapUser.getMail())) {
                log.debug("looking up user by username (mail): " + ldapUser.getMail());
                user = getUserByUsername(ldapUser.getMail());
            }

            log.debug("user: " + (user == null ? "null" : user.getId()));

            if (user == null) {
                log.debug("creating new user");
                user = new User();
                user.setCreationDate(new Date());
                user.setCreator("SYSTEM");
                if (StringUtils.isNotBlank(ldapUser.getCn())) {
                    log.debug("setting user loginid = " + ldapUser.getCn());
                    user.setLoginId(StringUtils.lowerCase(ldapUser.getCn()));
                }
            }

            //Adding user role to new user
            UserRole ur = new UserRole();
            ur.setUser(user);
            ur.setRole(roleService.getRoleById(Role.ROLE_USER_ID));
            user.getUserRoles().add(ur);
        }

        //Synchronize user data with ldap info
        user.setFirstName(ldapUser.getGivenName());
        user.setLastName(ldapUser.getSn());
        user.setLoginId(StringUtils.lowerCase(ldapUser.getCn()));
        if (StringUtils.isNotBlank(ldapUser.getMail())) {
            user.setEmail(StringUtils.lowerCase(ldapUser.getMail()));
        }

        //Adding phone number from ldap
        String ldapPhone = MapUtils.getString(ldapUser.getAttributeMap(), "telephoneNumber");
        if (StringUtils.isNotBlank(ldapPhone)) {
            // update or create new phone
            // get the "Office" phonetype
            Phone officePhone = null;
            for (Phone phone : user.getPhone()) {
                if (StringUtils.equals(phone.getPhoneType(), "Office")) {
                    officePhone = phone;
                }
            }

            if (officePhone == null) {
                officePhone = new Phone();
                officePhone.setPhoneType("Office");
                officePhone.setPrimaryPhone(Boolean.TRUE);

                officePhone.setUser(user);
                user.getPhone().add(officePhone);
            }

            officePhone.setNumber(ldapPhone);
        }

        //Checking for Location sync and adding if necessary
        LdapConfig ldapConfig = ldapService.getLdapConfig();
        String ldapLocField = ldapConfig.getLocationAttribute();
        if (StringUtils.isNotBlank(ldapLocField)) {
            Map attrMap = ldapUser.getAttributeMap();
            String userLocation = (String) attrMap.get(ldapLocField);
            if (StringUtils.isNotBlank(userLocation)) {
                Location location = locationService.getLocationByName(userLocation);
                if (location != null) {
                    // already exists ... set the user location and move on
                    user.setLocation(location);
                } else {
                    if (ldapConfig.getCreateLocation()) {
                        // create a new location and save it and set user location.
                        location = new Location();
                        location.setActive(true);
                        location.setName(userLocation);

                        locationService.saveLocation(location);
                        user.setLocation(location);
                    }
                }
            }
        }

        user.setLdapDN(ldapUser.getDn());
        if (StringUtils.isNotBlank(password)) {
            user.setPassword(GLTest.getMd5Password(password));
        }

        if (StringUtils.isNotBlank(ldapUser.getMail()) && StringUtils.isNotBlank(ldapUser.getCn())) {
            log.debug("saving user");
            saveUser(user);
            log.debug("user.getId() = " + user.getId());
            return user;
        } else {
            return null;
        }
    }

    public void inActivateUser(User user) {
        user.setActive(false);
        saveUser(user);

        List<UserRoleGroup> urgs = userRoleGroupService.getUserRoleGroupByUserId(user.getId());
        for (UserRoleGroup urg : urgs) {
            userRoleGroupService.deleteUserRoleGroup(urg);
        }
    }

    public boolean userInUse(User user) {
        return this.userDao.userInUse(user);
    }

    public void flush() {
        this.userDao.flush();
    }

    public User getUserByFirstAndLastName(String firstName, String lastName) {
        return this.userDao.getUserByFirstAndLastName(firstName, lastName);
    }

    public List<User> getFilteredUsers(String sort, boolean ascending, String group, String firstName, String lastName, String role, Boolean status, String username, String email) {
        return this.userDao.getFilteredUsers(sort, ascending, group, firstName, lastName, role, status, username, email);
    }

    public boolean isManagerForGroup(User user, UserRoleGroup urg) {
        user = getUserById(user.getId());
        Set userRoles = user.getUserRoles();
        for (Object userRole1 : userRoles) {
            UserRole userRole = (UserRole) userRole1;
            if (userRole.getRole() != null && (userRole.getRole().getId().equals(Role.ROLE_MANAGER_ID))) {
                if (userRole.getId().equals(urg.getUserRole().getId()))
                    return true;
            }
        }
        return false;
    }

    public void clear() {
        this.userDao.clear();
    }

    public List<User> getUsers(String queryParam, int start, int count, Map<String, Boolean> sortFieldsOrder, boolean includeInactive) {
        return userDao.getUsers(queryParam, start, count, sortFieldsOrder, includeInactive);
    }

    public int getUserCount(String queryParam, boolean includeInactive) {
        return userDao.getUserCount(queryParam, includeInactive);
    }

    public User getLoggedInUser() {
        User user = UserUtils.getLoggedInUser();
        if (user == null) {
            SecurityContext securityContext = SecurityContextHolder.getContext();
            if (securityContext != null) {
                Authentication authentication = securityContext.getAuthentication();
                if (authentication != null) {
                    if (authentication.getPrincipal() instanceof UserDetails) {
                        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
                        user = userDetails.getUser();
                        user = getUserById(user.getId());
                        UserUtils.setLoggedInUser(user);
                    }
                }
            }
        }

        if (user == null) {
            log.error("Unable to determine logged in user, returning anonymous user");
            user = getUserByUsername("anonymous");
        }

        return user;
    }

    public List<User> getUsersWithAnyRole(List<Role> roles) {
        return userDao.getUsersWithAnyRole(roles);
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setLdapService(LdapService ldapService) {
        this.ldapService = ldapService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }
}
