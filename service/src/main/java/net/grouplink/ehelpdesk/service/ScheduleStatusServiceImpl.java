package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.common.utils.MailMessage;
import net.grouplink.ehelpdesk.common.utils.RegexUtils;
import net.grouplink.ehelpdesk.common.utils.PropertiesConstants;
import net.grouplink.ehelpdesk.dao.ScheduleStatusDao;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.ScheduleStatus;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketHistory;
import net.grouplink.ehelpdesk.domain.TicketScheduleStatus;
import net.grouplink.ehelpdesk.domain.User;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.i18n.LocaleContextHolder;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;

/**
 * @author mrollins
 * @version 1.0
 */
public class ScheduleStatusServiceImpl implements ScheduleStatusService {

    private Log log = LogFactory.getLog(ScheduleStatusServiceImpl.class);

    private ScheduleStatusDao scheduleStatusDao;
    private TicketService ticketService;
    private MailService mailService;
    private TicketScheduleStatusService ticketScheduleStatusService;
    private TicketHistoryService ticketHistoryService;
    private UserService userService;
    private PropertiesService propertiesService;

    public List<ScheduleStatus> getScheduleStatus() {
        return scheduleStatusDao.getScheduleStatus();
    }

    public ScheduleStatus getScheduleStatusById(Integer id) {
        return scheduleStatusDao.getScheduleStatusById(id);
    }

    public void saveScheduleStatus(ScheduleStatus scheduleStatus) {
        scheduleStatusDao.saveScheduleStatus(scheduleStatus);
    }

    public void deleteScheduleStatus(ScheduleStatus scheduleStatus) {
        ticketScheduleStatusService.cleanHouse(new ArrayList(), scheduleStatus);
        scheduleStatusDao.deleteScheduleStatus(scheduleStatus);
    }

    public List<ScheduleStatus> getScheduleStatusByGroupId(Integer id) {
        return scheduleStatusDao.getScheduleStatusByGroupId(id);
    }

    public void checkProcesses() {
        log.debug("checking for scheduled tasks");
        List<ScheduleStatus> sss = getScheduleStatus();
        log.debug("found " + sss.size() + " scheduled tasks");
        for (ScheduleStatus ss : sss) {
            log.debug("processing scheduled task: " + ss.getName() + ", isRunning: " + ss.isRunning());
            if (ss.isRunning()) {
                List<Integer> ticketIdsToKeepTSS = new ArrayList<Integer>();
                // Get the tickets that match the chosen group, status and priority
                log.debug("looking up tickets that match scheduler criteria");
                List<Ticket> tickets = ticketService.getTicketsByCriteria(ss.getGroup(), ss.getStatuses(),
                        ss.getPriorities(), ss.getCategory(), ss.getCategoryOption());
                log.debug("found " + tickets.size() + " tickets matching scheduler criteria");

                for (Ticket ticket : tickets) {
                    ticket = ticketService.getTicketById(ticket.getId());
                    // Check for a match with elapsed time
                    Date now = new Date();
                    Date fromDate = ss.isFromModified() ? ticket.getModifiedDate() : ticket.getCreatedDate();
                    
                    long deltaTime = now.getTime() - fromDate.getTime();
                    long elapsedTime = Integer.parseInt(ss.getElapsedTimeDigit()) *
                            getMillisForUnit(ss.getElapsedTimeUnit());

                    log.debug("ticket # " + ticket.getId() + " deltaTime: " + deltaTime + ", elapsedTime: " + elapsedTime);
                    if (deltaTime > elapsedTime) {
                        boolean changeMade = false;
                        if (ss.isChangeStatusAction()) {
                            log.debug("setting ticket status to " + ss.getChangeStatus().getName());
                            ticket.setStatus(ss.getChangeStatus());
                            changeMade = true;
                        }
                        
                        if (ss.isChangePriorityAction()) {
                            log.debug("setting ticket priority to " + ss.getChangePriority().getName());
                            ticket.setPriority(ss.getChangePriority());
                            changeMade = true;
                        }
                        
                        if (ss.isSendMail()) {
                            log.debug("calling sendMailAction");
                            sendMailAction(ticket, ss);
                        }

                        if (changeMade) {
                            log.debug("change was made to ticket, saving ticket comments");
                            ticketHistoryService.saveComments(userService.getUserById(User.ADMIN_ID),
                                    TicketHistory.TICKET_SCHEDULER_UPDATE, ticket);
                        }
                        
                        ticketIdsToKeepTSS.add(ticket.getId());
                    }
                }

                // Clean out TicketScheduleStatus items that are not in ticketsToSave
                log.debug("cleaning up ticket ids that were not processed");
                ticketScheduleStatusService.cleanHouse(ticketIdsToKeepTSS, ss);
            }
        }
    }

    public int getCountByGroup(Group group) {
        return scheduleStatusDao.getCountByGroup(group);
    }

    public int getCountByCategory(Category category) {
        return scheduleStatusDao.getCountByCategory(category);
    }

    public int getCountByCategoryOption(CategoryOption categoryOption) {
        return scheduleStatusDao.getCountByCategoryOption(categoryOption);
    }

    private void sendMailAction(Ticket ticket, ScheduleStatus ss) {
        Date now = new Date();
        // get the ticketschedulestatus for this ticket
        log.debug("getting ticketSCheduleStatus for ticket " + ticket.getId());
        TicketScheduleStatus tss = ticketScheduleStatusService.getTSSByTicket(ticket, ss);

        if (tss == null) {
            log.debug("no tss found");
            // this must be the first time this ticket is being acted on by this scheduled task so create an entry
            tss = new TicketScheduleStatus();
            tss.setTicket(ticket);
            tss.setScheduleStatus(ss);
        } else {
            log.debug("found tss");
            // check if ss is one time only
            log.debug("ss.getActionFrequencyOneTime: " + ss.getActionFrequencyOneTime());
            if (ss.getActionFrequencyOneTime()) return;

            // Check that the interval time for sending emails is matched
            long deltaTime = now.getTime() - tss.getLastEmailed().getTime();
            long intervalTime = Integer.parseInt(ss.getActionFrequencyDigit()) *
                    getMillisForUnit(ss.getActionFrequencyUnit());
            log.debug("deltaTime: " + deltaTime + ", intervalTime: " + intervalTime);
            if (deltaTime < intervalTime) return;
        }

        log.debug("sending scheduler mail message for ticket " + ticket.getId());
        tss.setLastEmailed(now);
        ticketScheduleStatusService.saveTSS(tss);
        ticketScheduleStatusService.flush();
        try {
            MailMessage mm = createMailMessage(ticket, ss);
            mailService.sendMail(mm);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private MailMessage createMailMessage(Ticket ticket, ScheduleStatus ss) throws UnsupportedEncodingException,
            MessagingException {
        MailMessage mm = mailService.newMessage();
        mm.setFrom(mailService.getMailConfig().getEmailFromAddress(), mailService.getMailConfig().getEmailDisplayName());
        // Set To
        List<String> tos = new ArrayList<String>();
        if (ss.isMailAssignedTo()) {
            if (ticket.getAssignedTo().getUserRole() == null) {
                // ticket pool urg
                Set<User> notificationUsers = ticket.getAssignedTo().getNotificationUsers();
                for (User user : notificationUsers) {
                    tos.add(user.getEmail());
                }
            } else {
                tos.add(ticket.getAssignedTo().getUserRole().getUser().getEmail());
            }
        }

        if (ss.isMailContact() && ticket.getContact() != null) {
            if (ticket.getContact().getId().equals(User.ANONYMOUS_ID)) {
                tos.add(ticket.getAnonymousEmailAddress());
            } else {
                tos.add(ticket.getContact().getEmail());
            }
        }

        if (tos.size() > 0) {
            for (String to : tos) {
                mm.addTo(to);
            }
        }

        // Set CC
        if (StringUtils.isNotBlank(ss.getMailCC())) {
            String cc = ss.getMailCC();
            if (StringUtils.isNotBlank(ticket.getCc())) {
                cc = cc.replaceAll("\\{CC\\}", Matcher.quoteReplacement(ticket.getCc()));
            }

            mm.setCc(cc.split(RegexUtils.EMAIL_LIST_REGEX));
        }

        // Set BCC
        if (StringUtils.isNotBlank(ss.getMailBC())) {
            String bcc = ss.getMailBC();
            if (StringUtils.isNotBlank(ticket.getCc())) {
                bcc = bcc.replaceAll("\\{CC\\}", Matcher.quoteReplacement(ticket.getCc()));
            }

            mm.setBcc(bcc.split(RegexUtils.EMAIL_LIST_REGEX));
        }

        // Set subject
        String subject = ss.getMailSubject();
        if (StringUtils.isNotBlank(subject)) {
            subject = replaceTags(subject, ticket);
        } else {
            subject = "";
        }

        mm.setSubject(subject);

        // Set message
        String mailNote;
        if (StringUtils.isNotBlank(ss.getMailNoteText())) {
            mailNote = ss.getMailNoteText();
        } else if (StringUtils.isNotBlank(ss.getMailNote())) {
            mailNote = ss.getMailNote();
        } else {
            mailNote = "";
        }

        if (StringUtils.isNotBlank(mailNote)) {
            mailNote = replaceTags(mailNote, ticket);
        }

        mm.setText(mailNote);
        return mm;
    }

    private String replaceTags(String text, Ticket ticket) {
        Map<String, String> tagMap = getTagMap(ticket);
        for (Map.Entry<String, String> entry : tagMap.entrySet()) {
            String replacement = entry.getValue();
            text = text.replaceAll(entry.getKey(), Matcher.quoteReplacement(replacement));
        }

        return text;
    }

    private Map<String, String> getTagMap(Ticket t) {
        Map<String, String> tags = new HashMap<String, String>();
        tags.put("\\{TID\\}", t.getTicketId().toString());
        tags.put("\\{TSUBJ\\}", t.getSubject());
        tags.put("\\{NOTE\\}", t.getNote());
        tags.put("\\{TMSG\\}", t.getNote());
        tags.put("\\{CC\\}", t.getCc());
        tags.put("\\{PID\\}", t.getParent() != null ? t.getParent().getTicketId().toString() : "");
        tags.put("\\{CAT\\}", t.getCategory() != null ? t.getCategory().getName() : "");
        tags.put("\\{CATOP\\}", t.getCategoryOption() != null ? t.getCategoryOption().getName() : "");
        tags.put("\\{CONT\\}", t.getContact() != null ? t.getContact().getFirstName() + " " +
                t.getContact().getLastName() : "");
        tags.put("\\{CONT#\\}", (t.getContact() != null && t.getContact().getPrimaryPhone() != null) ?
                t.getContact().getPrimaryPhone().getNumber() : "");
        tags.put("\\{CONTEML\\}", t.getContact() != null ? t.getContact().getEmail() : "");
        tags.put("\\{LOC\\}", t.getLocation() != null ? t.getLocation().getName() : "");
        tags.put("\\{PRI\\}", t.getPriority() != null ? t.getPriority().getName() : "");
        tags.put("\\{STAT\\}", t.getStatus() != null ? t.getStatus().getName() : "");
        String assignedTo = "";
        if (t.getAssignedTo() != null) {
            if (t.getAssignedTo().getUserRole() != null) {
                assignedTo = t.getAssignedTo().getUserRole().getUser().getFirstName() + " " +
                        t.getAssignedTo().getUserRole().getUser().getLastName();
            } else {
                assignedTo = "Ticket Pool";
            }
        }

        tags.put("\\{ASSGN\\}", assignedTo);
        tags.put("\\{GRP\\}", t.getGroup() != null ? t.getGroup().getName() : "");
        tags.put("\\{SUBM\\}", t.getSubmittedBy() != null ? t.getSubmittedBy().getFirstName() + " " +
                t.getSubmittedBy().getLastName() : "");
        DateFormat df = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM,
                LocaleContextHolder.getLocale());
        tags.put("\\{CRTDATE\\}", t.getCreatedDate() != null ? df.format(t.getCreatedDate()) : "");
        tags.put("\\{MODDATE\\}", t.getModifiedDate() != null ? df.format(t.getModifiedDate()) : "");
        tags.put("\\{ESTCOMPDATE\\}", t.getEstimatedDate() != null ? df.format(t.getEstimatedDate()) : "");
        tags.put("\\{WRKTM\\}", t.getWorkTime() != null ? t.getWorkTime().toString() : "");
        tags.put("\\{ASSID\\}", t.getAsset() != null ? t.getAsset().getAssetNumber() : "");

        String baseUrl = propertiesService.getPropertiesValueByName(PropertiesConstants.BASE_URL);

        String linkUrl = baseUrl + "/tickets/" + t.getId() + "/edit?smtl=true";

        tags.put("\\{TLINK\\}", linkUrl);

        for (Map.Entry<String, String> entry : tags.entrySet()) {
            if (entry.getValue() == null) {
                tags.put(entry.getKey(), "");
            }
        }

        return tags;
    }

    private long getMillisForUnit(String unit) {
        if (unit.equals("mins")) {
            return 1000 * 60;
        } else if (unit.equals("hour")) {
            return 1000 * 60 * 60;
        } else if (unit.equals("days")) {
            return 1000 * 60 * 60 * 24;
        } else if (unit.equals("week")) {
            return 1000 * 60 * 60 * 24 * 7;
        }
        return 0;
    }

    public void setScheduleStatusDao(ScheduleStatusDao scheduleStatusDao) {
        this.scheduleStatusDao = scheduleStatusDao;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }

    public void setTicketScheduleStatusService(TicketScheduleStatusService ticketScheduleStatusService) {
        this.ticketScheduleStatusService = ticketScheduleStatusService;
    }

    public void setTicketHistoryService(TicketHistoryService ticketHistoryService) {
        this.ticketHistoryService = ticketHistoryService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setPropertiesService(PropertiesService propertiesService) {
        this.propertiesService = propertiesService;
    }
}
