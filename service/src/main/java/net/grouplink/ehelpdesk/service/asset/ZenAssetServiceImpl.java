package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.domain.asset.ZenAsset;
import net.grouplink.ehelpdesk.domain.zen.ZenAssetSearch;
import net.grouplink.ehelpdesk.dao.asset.ZenAssetDao;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Apr 5, 2009
 * Time: 11:00:18 PM
 */
public class ZenAssetServiceImpl implements ZenAssetService {

    private ZenAssetDao zenAssetDao;
    
    public ZenAsset getById(Integer id) {
        return zenAssetDao.getById(id);
    }

    public void save(ZenAsset zenAsset) {
        zenAssetDao.save(zenAsset);
    }

    public List<ZenAsset> getAll() {
        return zenAssetDao.getAll();
    }

    public ZenAsset getByWorkstationId(byte[] workstationId) {
        return zenAssetDao.getByWorkstationId(workstationId);
    }

    public void delete(ZenAsset zenAsset) {
        zenAssetDao.delete(zenAsset);
    }

    public List<ZenAsset> search(ZenAssetSearch zenAssetSearch) {
        return zenAssetDao.search(zenAssetSearch);
    }

    public void setZenAssetDao(ZenAssetDao zenAssetDao) {
        this.zenAssetDao = zenAssetDao;
    }
}
