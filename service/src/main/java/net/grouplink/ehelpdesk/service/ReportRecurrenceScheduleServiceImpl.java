package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.ReportRecurrenceScheduleDao;
import net.grouplink.ehelpdesk.domain.RecurrenceSchedule;
import net.grouplink.ehelpdesk.domain.Report;
import net.grouplink.ehelpdesk.domain.ReportRecurrenceSchedule;
import net.grouplink.ehelpdesk.domain.mail.EmailMessageTemplate;
import net.grouplink.ehelpdesk.service.mail.MultipartMailer;
import net.grouplink.ehelpdesk.common.utils.RegexUtils;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import javax.mail.MessagingException;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

public class ReportRecurrenceScheduleServiceImpl implements ReportRecurrenceScheduleService {
    private Log log = LogFactory.getLog(getClass());

    private RecurrenceScheduleService recurrenceScheduleService;
    private ReportRecurrenceScheduleDao reportRecurrenceScheduleDao;
    private ReportService reportService;
    private JavaMailSenderImpl mailSender;
    private MailService mailService;

    public void save(ReportRecurrenceSchedule reportRecurrenceSchedule) {
        reportRecurrenceScheduleDao.save(reportRecurrenceSchedule);
    }

    public void delete(ReportRecurrenceSchedule reportRecurrenceSchedule) {
        reportRecurrenceScheduleDao.delete(reportRecurrenceSchedule);
    }

    public void processSchedules() {
        log.debug("called ReportRecurrenceScheduleServiceImpl.processSchedules()");
        if (!mailService.isMailConfigured()) {
            return;
        }
        
        log.debug("checking for scheduled reports");
        List<RecurrenceSchedule> schedules = recurrenceScheduleService.getRunningReportSchedules();
        log.debug("found " + schedules.size() + " recurring report schedules");
        Date currentDate = new Date();
        for (RecurrenceSchedule schedule : schedules) {
            try {
                processSchedule(currentDate, schedule);
            } catch (Exception e) {
                log.error("error occured processing report schedule", e);
            }
        }
    }

    private void processSchedule(Date currentDate, RecurrenceSchedule schedule) {
        log.debug("processing recurrenceSchedule: " + schedule.getId());
        log.debug("getting reportRecurrenceSchedule");
        Report report = schedule.getReport();
        ReportRecurrenceSchedule rrs = getByReportAndSchedule(report, schedule);
        log.debug("rrs == null: " + (rrs == null));
        Date lastActionDate = null;
        int occurrenceCount = 0;
        if (rrs != null) {
            lastActionDate = rrs.getLastAction();
            if (rrs.getOccurrenceCount() == null) {
                occurrenceCount = 0;
            } else {
                occurrenceCount = rrs.getOccurrenceCount();
            }
            
            log.debug("rrs lastAction: " + lastActionDate + ", occurrenceCount: " + occurrenceCount);
        }

        log.debug("checking if schedule meets schedule criteria");
        boolean processSchedule = recurrenceScheduleService.meetsScheduleCriteria(schedule, currentDate, lastActionDate, occurrenceCount);
        log.debug("meets schedule criteria: " + processSchedule);
        if (processSchedule) {
            try {
                // create report
                log.debug("calling createReport");

                // if report format is html, force to pdf
                if (report.getFormat().equals(Report.FORMAT_HTML)) {
                    report.setFormat(Report.FORMAT_PDF);
                }

                createReport(report);

                // create email
                log.debug("calling createMail");
                createMail(report);

                // update report recurrence schedule
                log.debug("updating report recurrence schedule");
                if (rrs == null) {
                    rrs = new ReportRecurrenceSchedule();
                    rrs.setRecurrenceSchedule(schedule);
                    rrs.setReport(report);
                    rrs.setOccurrenceCount(1);
                } else {
                    rrs.setOccurrenceCount(rrs.getOccurrenceCount() + 1);
                }

                rrs.setLastAction(new Date());
                save(rrs);
            } catch (Exception e) {
                log.error("error generating or sending scheduled report", e);
            } finally {
                try {
                    cleanupTmpDir(report);
                } catch (Exception ignored) {}
            }
        }
    }

    public void createMail(Report report) throws MessagingException, UnsupportedEncodingException {
        MultipartMailer mm = new MultipartMailer(mailSender, MultipartMailer.MULTIPART_MIXED);
        mm.setCharset("UTF-8");
        EmailMessageTemplate template = report.getSchedule().getEmailMessageTemplate();
        mm.setFrom(mailService.getMailConfig().getEmailFromAddress(), mailService.getMailConfig().getEmailDisplayName());
        if (StringUtils.isNotBlank(template.getSubject())) {
            mm.setSubject(template.getSubject());
        } else {
            mm.setSubject("");
        }

        if (StringUtils.isNotBlank(template.getTo())) {
            String[] emails = template.getTo().split(RegexUtils.EMAIL_LIST_REGEX);
            for (String email : emails) {
                mm.addToRecipient(email);
            }
        }

        if (StringUtils.isNotBlank(template.getBody())) {
            mm.addTextPart(template.getBody());
        }

        // attach report file(s)
        String fileName = getReportOutputFileName(report);
        File file = new File(fileName);
        if (file.exists()) {
            mm.addFilePart(fileName);
        }

        if (report.getFormat().equals(Report.FORMAT_HTML)) {
            // add images
            String imagePath = getReportHtmlImagesDirName(report);
            File imageDir = new File(imagePath);
            String[] imagePaths = imageDir.list();
            for (String path : imagePaths) {
                mm.addFilePart(imagePath + File.separatorChar + path);
            }
        }

        mm.send();
    }

    public void createReport(Report report) throws JRException {
        JasperPrint jasperPrint;
        if (report.getShowReportByDate()) {
            jasperPrint = reportService.generateReportByDate(report, report.getUser(), null, report.getByDate());
        } else {
            jasperPrint = reportService.generateReport(report, report.getUser(), null);
        }

        if (report.getFormat().equals(Report.FORMAT_HTML)) {
            JRHtmlExporter htmlExporter = new JRHtmlExporter();
            htmlExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            htmlExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, getReportOutputFileName(report));
            htmlExporter.setParameter(JRHtmlExporterParameter.IS_OUTPUT_IMAGES_TO_DIR, true);
            htmlExporter.setParameter(JRHtmlExporterParameter.IMAGES_DIR_NAME, getReportHtmlImagesDirName(report));
            htmlExporter.exportReport();
        } else if (report.getFormat().equals(Report.FORMAT_PDF)) {
            JRPdfExporter pdfExporter = new JRPdfExporter();
            pdfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            pdfExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, getReportOutputFileName(report));
            pdfExporter.exportReport();
        } else if (report.getFormat().equals(Report.FORMAT_XLS)) {
            JExcelApiExporter xlsExporter = new JExcelApiExporter();
            xlsExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            xlsExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, getReportOutputFileName(report));
            xlsExporter.exportReport();
        } else if (report.getFormat().equals(Report.FORMAT_CSV)) {
            JRCsvExporter csvExporter = new JRCsvExporter();
            csvExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            csvExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, getReportOutputFileName(report));
            csvExporter.exportReport();
        } else {
            log.error("unknown report format: " + report.getFormat());
            throw new RuntimeException("unknown report format: " + report.getFormat());
        }
    }

    public void cleanupTmpDir(Report report) {
        File dir = new File(getReportTmpDir(report));
        FileUtils.deleteQuietly(dir);
    }

    private String getReportHtmlImagesDirName(Report report) {
        String imageTmpDir = getReportTmpDir(report) + "images";
        File tmpDir = new File(imageTmpDir);
        if (!tmpDir.exists()) {
            if (!tmpDir.mkdirs()) {
                throw new RuntimeException("error creating tmp dir: " + imageTmpDir);
            }
        }
        return imageTmpDir;
    }

    private String getReportOutputFileName(Report report) {
        StringBuilder path = new StringBuilder()
                .append(getReportTmpDir(report))
                .append("ehdreport.")
                .append(report.getFormat());
        return path.toString();
    }

    private String getReportTmpDir(Report report) {
        StringBuilder tmpPath = new StringBuilder()
            .append(System.getProperty("java.io.tmpdir"))
                .append(File.separatorChar)
                .append("ehelpdesk")
                .append(File.separatorChar)
                .append("scheduledreports")
                .append(File.separatorChar)
                .append(report.getId())
                .append(File.separatorChar);

        File tmpDir = new File(tmpPath.toString());
        if (!tmpDir.exists()) {
            if (!tmpDir.mkdirs()) {
                throw new RuntimeException("error creating tmp dir: " + tmpPath.toString());
            }
        }

        return tmpPath.toString();
    }

    private ReportRecurrenceSchedule getByReportAndSchedule(Report report, RecurrenceSchedule schedule) {
        return reportRecurrenceScheduleDao.getByReportAndSchedule(report, schedule);
    }


    public void setRecurrenceScheduleService(RecurrenceScheduleService recurrenceScheduleService) {
        this.recurrenceScheduleService = recurrenceScheduleService;
    }

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    public void setReportRecurrenceScheduleDao(ReportRecurrenceScheduleDao reportRecurrenceScheduleDao) {
        this.reportRecurrenceScheduleDao = reportRecurrenceScheduleDao;
    }

    public void setMailSender(JavaMailSenderImpl mailSender) {
        this.mailSender = mailSender;
    }

    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }
}