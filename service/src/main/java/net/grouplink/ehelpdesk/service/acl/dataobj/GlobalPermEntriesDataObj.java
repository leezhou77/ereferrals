package net.grouplink.ehelpdesk.service.acl.dataobj;

import java.util.ArrayList;
import java.util.List;

public class GlobalPermEntriesDataObj {
    private List<GlobalPermEntryDataObj> globalPermissionEntries = new ArrayList<GlobalPermEntryDataObj>();

    public List<GlobalPermEntryDataObj> getGlobalPermissionEntries() {
        return globalPermissionEntries;
    }

    public void setGlobalPermissionEntries(List<GlobalPermEntryDataObj> globalPermissionEntries) {
        this.globalPermissionEntries = globalPermissionEntries;
    }
}
