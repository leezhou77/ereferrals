package net.grouplink.ehelpdesk.service.acl.dataobj;

import java.util.ArrayList;
import java.util.List;

public class PermSchemesDataObj {
    private List<PermSchemeDataObj> permissionSchemes = new ArrayList<PermSchemeDataObj>();

    public List<PermSchemeDataObj> getPermissionSchemes() {
        return permissionSchemes;
    }

    public void setPermissionSchemes(List<PermSchemeDataObj> permissionSchemes) {
        this.permissionSchemes = permissionSchemes;
    }
}
