package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.LdapDao;
import net.grouplink.ehelpdesk.domain.LdapUser;
import net.grouplink.ehelpdesk.common.utils.GLTest;
import net.grouplink.ehelpdesk.common.utils.PropertiesConstants;
import net.grouplink.ehelpdesk.common.utils.TreeNode;
import net.grouplink.ehelpdesk.common.utils.UserSearch;
import net.grouplink.ehelpdesk.common.utils.ldap.LdapConfig;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.AuthenticationException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LdapServiceImpl implements LdapService {

    private PropertiesService propertiesService;
    private LdapDao ldapDao;
    protected final Log logger = LogFactory.getLog(getClass());

    public boolean init() {
        return ldapDao.init(propertiesService.getPropertiesValueByName(PropertiesConstants.LDAP_URL),
                propertiesService.getPropertiesValueByName(PropertiesConstants.LDAP_LOGIN),
                GLTest.decryptPassword(propertiesService.getPropertiesValueByName(PropertiesConstants.LDAP_PASSWORD)),
                propertiesService.getPropertiesValueByName(PropertiesConstants.LDAP_BASE),
                propertiesService.getPropertiesValueListByName(PropertiesConstants.LDAP_BASE_SEARCH),
                propertiesService.getPropertiesValueByName(PropertiesConstants.LDAP_FILTER));
    }

    public Set<LdapUser> authenticateUser(String username, String password) throws AuthenticationException {
        Set<LdapUser> ldapUsers = this.ldapDao.authenticateUser(username, password);

        // check for duplicate dn's
        if (ldapUsers.size() > 1) {
            ldapUsers = removeDuplicateUsers(ldapUsers);
        }

        return ldapUsers;
    }

    public Set<LdapUser> searchLdapUser(UserSearch userSearch, int searchType) {
        Set<LdapUser> ldapUsers = new HashSet<LdapUser>();
        List<String> ldapBaseSearchList = propertiesService
                .getPropertiesValueListByName(PropertiesConstants.LDAP_BASE_SEARCH);
        for (String aLdapBaseSearchList : ldapBaseSearchList) {
            ldapUsers.addAll(this.ldapDao.searchLdapUser(aLdapBaseSearchList, userSearch, searchType));
        }

        if (ldapBaseSearchList.size() == 0) {
            ldapUsers.addAll(this.ldapDao.searchLdapUser("", userSearch, searchType));
        }

        // check for duplicate dn's
        if (ldapUsers.size() > 1) {
            ldapUsers = removeDuplicateUsers(ldapUsers);
        }
        
        return ldapUsers;
    }

    private Set<LdapUser> removeDuplicateUsers(Set<LdapUser> ldapUsers) {
        Set<LdapUser> users = new HashSet<LdapUser>();
        Set<String> dnSet = new HashSet<String>();

        for (LdapUser ldapUser : ldapUsers) {
            String dn = ldapUser.getDn();
            if (!dnSet.contains(dn)) {
                users.add(ldapUser);
                dnSet.add(dn);
            }
        }

        return users;
    }

    public LdapUser getLdapUser(String dn) {
        LdapUser ldapUser;
        ldapUser = this.ldapDao.getLdapUser(dn);
        return ldapUser;
    }

    public LdapConfig getLdapConfig() {
        LdapConfig lc = new LdapConfig();
        lc.setLdapIntegration(propertiesService.getBoolValueByName(PropertiesConstants.LDAP_INTEGRATION, false));
        lc.setUrl(propertiesService.getPropertiesValueByName(PropertiesConstants.LDAP_URL));
        lc.setBase(propertiesService.getPropertiesValueByName(PropertiesConstants.LDAP_BASE));
        lc.setLogin(propertiesService.getPropertiesValueByName(PropertiesConstants.LDAP_LOGIN));
        lc.setPassword(GLTest.decryptPassword(propertiesService.getPropertiesValueByName(PropertiesConstants.LDAP_PASSWORD)));
        lc.setBaseSearchList(propertiesService.getPropertiesValueListByName(PropertiesConstants.LDAP_BASE_SEARCH));
        lc.setFilter(propertiesService.getPropertiesValueByName(PropertiesConstants.LDAP_FILTER));
        lc.setUsernameAttribute(propertiesService.getPropertiesValueByName(PropertiesConstants.LDAP_USERNAME_ATTRIBUTE));
        lc.setEhdFallback(propertiesService.getBoolValueByName(PropertiesConstants.LDAP_EHD_FALLBACK, true));
        lc.setLocationAttribute(propertiesService.getPropertiesValueByName(PropertiesConstants.LDAP_LOCATION_ATTRIBUTE));
        lc.setCreateLocation(propertiesService.getBoolValueByName(PropertiesConstants.LDAP_CREATE_LOCATION, false));
        return lc;
    }

    public void saveLdapConfig(LdapConfig ldapConfig) {
        propertiesService.saveProperties(PropertiesConstants.LDAP_INTEGRATION, ldapConfig.getLdapIntegration().toString());
        propertiesService.saveProperties(PropertiesConstants.LDAP_URL, ldapConfig.getUrl());
        propertiesService.saveProperties(PropertiesConstants.LDAP_LOGIN, ldapConfig.getLogin());
        propertiesService.saveProperties(PropertiesConstants.LDAP_PASSWORD, GLTest.encryptPassword(ldapConfig.getPassword()));
        propertiesService.saveProperties(PropertiesConstants.LDAP_BASE, ldapConfig.getBase());
        propertiesService.saveProperties(PropertiesConstants.LDAP_BASE_SEARCH, ldapConfig.getBaseSearchList());
        propertiesService.saveProperties(PropertiesConstants.LDAP_FILTER, ldapConfig.getFilter());
        propertiesService.saveProperties(PropertiesConstants.LDAP_USERNAME_ATTRIBUTE, ldapConfig.getUsernameAttribute());
        propertiesService.saveProperties(PropertiesConstants.LDAP_EHD_FALLBACK, ldapConfig.getEhdFallback().toString());
        propertiesService.saveProperties(PropertiesConstants.LDAP_LOCATION_ATTRIBUTE, ldapConfig.getLocationAttribute());
        propertiesService.saveProperties(PropertiesConstants.LDAP_CREATE_LOCATION, ldapConfig.getCreateLocation().toString());
        propertiesService.flush();
    }

    public List<TreeNode> getFirstLevelTree(String dn) {
        List<LdapUser> list = this.ldapDao.findAllFirstLevel(dn, 0);
        List<TreeNode> resultList = new ArrayList<TreeNode>();
        for (LdapUser aList : list) {
            TreeNode tn = new TreeNode();
            tn.setName(aList.getDn());
            tn.setFolder(true);
            resultList.add(tn);
        }

        return resultList;
    }

    public byte[] getGuidForUserDn(String dn) {
        return ldapDao.getGuidForUserDn(dn);
    }

    public void setPropertiesService(PropertiesService propertiesService) {
        this.propertiesService = propertiesService;
    }

    public void setLdapDao(LdapDao ldapDao) {
        this.ldapDao = ldapDao;
    }
}
