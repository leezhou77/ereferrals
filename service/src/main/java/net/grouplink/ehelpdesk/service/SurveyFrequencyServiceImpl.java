package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.SurveyFrequency;
import net.grouplink.ehelpdesk.dao.SurveyFrequencyDao;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Mar 6, 2008
 * Time: 9:39:34 AM
 */
public class SurveyFrequencyServiceImpl implements SurveyFrequencyService {

    private SurveyFrequencyDao surveyFrequencyDao;

    public List<SurveyFrequency> getSurveyFrequencies() {
        return surveyFrequencyDao.getSurveyFrequencies();
    }

    public SurveyFrequency getDefaultSurveyFrequency() {
        return surveyFrequencyDao.getDefaultSurveyFrequency();
    }

    public SurveyFrequency getSurveyFrequencyById(Integer id) {
        return surveyFrequencyDao.getSurveyFrequencyById(id);
    }

    public void save(SurveyFrequency surveyFrequency) {
        surveyFrequencyDao.save(surveyFrequency);
    }

    public void flush() {
        surveyFrequencyDao.flush();
    }

    // Spring injection
    public void setSurveyFrequencyDao(SurveyFrequencyDao surveyFrequencyDao) {
        this.surveyFrequencyDao = surveyFrequencyDao;        
    }
}
