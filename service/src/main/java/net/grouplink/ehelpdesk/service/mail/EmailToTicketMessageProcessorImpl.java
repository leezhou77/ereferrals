package net.grouplink.ehelpdesk.service.mail;

import net.grouplink.ehelpdesk.common.utils.MailConfig;
import net.grouplink.ehelpdesk.common.utils.MailMessage;
import net.grouplink.ehelpdesk.common.utils.PropertiesConstants;
import net.grouplink.ehelpdesk.common.utils.RegexUtils;
import net.grouplink.ehelpdesk.common.utils.UserSearch;
import net.grouplink.ehelpdesk.domain.Attachment;
import net.grouplink.ehelpdesk.domain.EmailToTicketConfig;
import net.grouplink.ehelpdesk.domain.LdapUser;
import net.grouplink.ehelpdesk.domain.Role;
import net.grouplink.ehelpdesk.domain.Status;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketHistory;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRole;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.domain.mail.EmailAttachment;
import net.grouplink.ehelpdesk.domain.mail.EmailMessage;
import net.grouplink.ehelpdesk.service.EmailToTicketConfigService;
import net.grouplink.ehelpdesk.service.LdapService;
import net.grouplink.ehelpdesk.service.MailService;
import net.grouplink.ehelpdesk.service.PropertiesService;
import net.grouplink.ehelpdesk.service.RoleService;
import net.grouplink.ehelpdesk.service.StatusService;
import net.grouplink.ehelpdesk.service.TicketHistoryService;
import net.grouplink.ehelpdesk.service.TicketService;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import net.grouplink.ehelpdesk.service.UserService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.LocaleUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.context.support.MessageSourceAccessor;

import javax.mail.MessagingException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: jaymehafen
 * Date: Feb 4, 2008
 * Time: 1:25:41 PM
 */
public class EmailToTicketMessageProcessorImpl extends ApplicationObjectSupport implements EmailToTicketMessageProcessor {

    private static final int MAX_NOTE_LENGTH = 10000;

    private Log log = LogFactory.getLog(EmailToTicketMessageProcessorImpl.class);

    private EmailToTicketConfigService emailToTicketConfigService;
    private UserService userService;
    private StatusService statusService;
    private TicketHistoryService ticketHistoryService;
    private UserRoleGroupService userRoleGroupService;

    private TicketService ticketService;
//    private String ticketNumberRegex = "^.*[tT][iI][cC][kK][eE][tT]\\s*\\#\\s*(\\d+).*";
    private String ticketNumberRegex = "\\{TID:(\\d+)\\}";
    private String tidRegex = "\\{TID:(\\d+)\\}";
    private Pattern tidPattern = Pattern.compile(tidRegex);
    private Pattern ticketNumberPattern = Pattern.compile(ticketNumberRegex);
    private MailService mailService;
    private PropertiesService propertiesService;
    private RoleService roleService;
    private LdapService ldapService;

    public void processE2TMessage(EmailMessage emailMessage, EmailToTicketConfig emailToTicketConfig) {
        try {
            emailToTicketConfig = emailToTicketConfigService.getById(emailToTicketConfig.getId());
            // look up user
            String emailFrom = emailMessage.getFromAddress();
            log.debug("looking up user for email address: " + emailFrom);
            User user = userService.getUserByEmail(emailFrom);
            if (user == null) {
                log.debug("user not found for email " + emailFrom);

                // if ldap integrated, do an ldap search for email address
                Boolean ldapIntegrationEnabled = propertiesService.getBoolValueByName(PropertiesConstants.LDAP_INTEGRATION, false);
                if (ldapIntegrationEnabled) {
                    UserSearch us = new UserSearch();
                    us.setEmail(emailFrom);
                    Set<LdapUser> ldapUserSet = ldapService.searchLdapUser(us, 2);
                    if (CollectionUtils.isNotEmpty(ldapUserSet)) {
                        LdapUser ldapUser = ldapUserSet.iterator().next();
                        user = userService.getUserByUsername(ldapUser.getCn());
                        if (user == null) {
                            // user found in ldap but not ehd, create new ehd user
                            user = userService.synchronizedUserWithLdap(user, null, ldapUser);
                        }
                    }
                }

                if (user == null) {

                    if (emailToTicketConfig.getNonEhdUserHandlingMethod() == null) {
                        emailToTicketConfig.setNonEhdUserHandlingMethod(emailToTicketConfig.isAllowNonUsers() ? EmailToTicketConfig.NONEHDUSER_ANONYMOUS : EmailToTicketConfig.NONEHDUSER_REJECT);
                    }

                    // check non-ehd user handling method
                    if (emailToTicketConfig.getNonEhdUserHandlingMethod().equals(EmailToTicketConfig.NONEHDUSER_REJECT)) {
                        // reject email
                        log.info("user not found for address: " + emailFrom + ", and anonymous users are not allowed");
                        rejectMail(emailMessage, "User not found for address: " + emailFrom + ", and anonymous users are not allowed");
                        return;
                    } else if (emailToTicketConfig.getNonEhdUserHandlingMethod().equals(EmailToTicketConfig.NONEHDUSER_ANONYMOUS)) {
                        // contact is anonymous account
                        log.debug("system is configured to allow non-users");
                        user = getAnonymousUser();
                        if (user == null) {
                            log.error("anonymous user not found");
                            rejectMail(emailMessage, "Anonymous user not found");
                            return;
                        }
                    } else if (emailToTicketConfig.getNonEhdUserHandlingMethod().equals(EmailToTicketConfig.NONEHDUSER_CREATE)) {
                        // create new user account
                        user = createUserFromEmail(emailMessage);
                    }
                }
            }

            Ticket ticket = null;
            boolean subjectNotBlank = StringUtils.isNotBlank(emailMessage.getSubject());
            Matcher m = null;
            boolean found = false;
            if(subjectNotBlank) {
                m = ticketNumberPattern.matcher(emailMessage.getSubject());
                found = m.find();
                if (found) log.debug("ticket subject " + emailMessage.getSubject() + " matches ticketNumberRegex");
            }
            
            if (subjectNotBlank && !found){
                m = tidPattern.matcher(emailMessage.getSubject());
                found = m.find();
                if (found) log.debug("ticket subject " + emailMessage.getSubject() + " matches tidRegex");
            }
            
            if (subjectNotBlank && found) {
                Integer ticketId = Integer.valueOf(m.group(1));
                log.debug("ticket number parsed from subject: " + ticketId);
                // search for ticket by oldTicketId first
                log.debug("searching for ticket with oldTicketId = " + ticketId);
                ticket = ticketService.getTicketByOldTicketId(ticketId);
                // if ticket not found, search by ticket.id
                if (ticket == null) {
                    log.debug("no ticket with oldTicketId " + ticketId + " found, searching by new ticket id");
                    ticket = ticketService.getTicketById(ticketId);
                }
            }

            if (ticket != null) {
                log.debug("found ticket, ticket.id = " + ticket.getId());
                try {
                    log.debug("creating ticket history");
                    ticket.setEmailToTicketConfig(emailToTicketConfig);
                    createTicketHistory(ticket, emailMessage, user);
                } catch (EmailToTicketRuntimeException e) {
                    log.error("error creating TicketHistory for ticket " + ticket.getId(), e);
                }
            } else {
                // new ticket
                try {
                    log.debug("creating new ticket");
                    createNewTicket(emailToTicketConfig, emailMessage, user);
                } catch (EmailToTicketRuntimeException e) {
                    log.error("error creating ticket for email with subject: " + emailMessage.getSubject(), e);
                }
            }

        } finally {
            // clean up attachments from tmp dir
            try {
                emailMessage.deleteAttachments();
            } catch (Exception ignored) {
            }
        }
    }

    private User createUserFromEmail(EmailMessage emailMessage) {
        // create user
        User user;
        user = new User();
        user.setActive(true);
        user.setCreationDate(new Date());
        user.setCreator("SYSTEM");
        user.setEmail(emailMessage.getFromAddress());
        user.setLoginId(emailMessage.getFromAddress());
        String name = emailMessage.getFromPersonalName();
        if (StringUtils.isNotBlank(name)) {
            String[] nameParts = name.split("\\s");

            if (nameParts.length == 1) {
                user.setFirstName(nameParts[0]);
                user.setLastName(nameParts[0]);
            } else if (nameParts.length == 2){
                if (nameParts[0].endsWith(",")) {
                    user.setFirstName(nameParts[1]);
                    user.setLastName(StringUtils.removeEnd(nameParts[0], ","));
                } else {
                    user.setFirstName(nameParts[0]);
                    user.setLastName(nameParts[1]);
                }
            } else if (nameParts.length > 2) {
                user.setFirstName(nameParts[0]);
                StringBuilder last = new StringBuilder();
                for (int i = 1; i < nameParts.length; i++) {
                    last.append(nameParts[i]);
                }

                user.setLastName(last.toString());
            }
        } else {
            user.setFirstName(StringUtils.substringBefore(emailMessage.getFromAddress(), "@"));
            user.setLastName(StringUtils.substringBefore(emailMessage.getFromAddress(), "@"));
        }

        UserRole userRole = new UserRole();
        userRole.setUser(user);
        Role role = roleService.getRoleById(Role.ROLE_USER_ID);
        userRole.setRole(role);
        user.getUserRoles().add(userRole);

        // save the user
        String password = userService.resetUserPassword(user);

        // notify the user of their account
        try {
            MailConfig mc = mailService.getMailConfig();
            MailMessage message = mailService.newMessage();
            message.setTo(user.getEmail());
            message.setFrom(mc.getEmailFromAddress(), mc.getEmailDisplayName());
			message.setSubject("Your eReferrals Account Created");
            StringBuilder text = new StringBuilder()
                    .append("Your eReferrals account has been created successfully. Here are the details of your account:\n\n")
                    .append("Login ID: ").append(user.getLoginId()).append("\n")
                    .append("Email: ").append(user.getEmail()).append("\n")
                    .append("Password: ").append(password).append("\n\n")
                    .append("You may login to check the status of your tickets, and to update your user profile at the following URL:\n\n")
                    .append(propertiesService.getPropertiesValueByName(PropertiesConstants.BASE_URL));
            message.setText(text.toString());
			mailService.sendMail(message);
        } catch (MessagingException e) {
            log.error("error sending email", e);
        } catch (UnsupportedEncodingException e) {
            log.error("error sending email", e);
        }

        return user;
    }

    private void rejectMail(EmailMessage emailMessage, String description) {
        MailMessage msg = mailService.newMessage();
        if (msg == null) {
            log.error("error creating new MailMessage, can't reject mail from " + emailMessage.getFrom());
            return;
        }

        MailConfig mc = mailService.getMailConfig();
        try {
            // from
            msg.setFrom(mc.getEmailFromAddress(), mc.getEmailDisplayName());
            // subject
            msg.setSubject("Mail rejected for: " + emailMessage.getSubject());
            // to
            msg.addTo(emailMessage.getFrom());
            // msg
            StringBuilder sb = new StringBuilder();
            sb.append("Sorry, your message could not be accepted for the following reason:")
                    .append(description)
                    .append("\n\n")
                    .append("Please contact your eReferrals administrator for further assistance.\n\n")
                    .append("Original message:\n");
            if (StringUtils.isNotBlank(emailMessage.getTextContent()))
                sb.append(emailMessage.getTextContent());
            else if (StringUtils.isNotBlank(emailMessage.getHtmlContent()))
                sb.append(RegexUtils.stripHtmlTags(emailMessage.getHtmlContent()));

            msg.setText(sb.toString());
        } catch (MessagingException e) {
            log.error("error setting rejection email fields", e);
            return;
        } catch (UnsupportedEncodingException e) {
            log.error("error setting rejection email fields", e);
            return;
        }

        mailService.sendMail(msg);
    }

    private void createTicketHistory(Ticket ticket, EmailMessage emailMessage, User user) {
        TicketHistory ticketHistory = new TicketHistory();
        ticketHistory.setTicket(ticket);
        ticketHistory.setUser(user);
        ticketHistory.setCreatedDate(new Date());
        ticketHistory.setSubject(emailMessage.getSubject());
        ticketHistory.setTextFormat(true);

        if (!emailMessage.getCcAddresses().isEmpty()) {
            String cc = "";
            for (int i=0; i<emailMessage.getCcAddresses().size(); i++) {
                if (i > 0) cc+= ", ";
                cc += emailMessage.getCcAddresses().get(i);
            }
            ticketHistory.setCc(cc);
        }

        String note = "";
        if (StringUtils.isNotBlank(emailMessage.getTextContent())) {
            note = emailMessage.getTextContent();
        } else if (StringUtils.isNotBlank(emailMessage.getHtmlContent())) {
            note = convertHtmlToText(emailMessage.getHtmlContent());
        }

        if (StringUtils.length(note) > MAX_NOTE_LENGTH) {
            log.warn("note from email is larger than max note length of " + MAX_NOTE_LENGTH + " characters, truncating note");
            note = StringUtils.substring(note, 0, MAX_NOTE_LENGTH);
        }

        ticketHistory.setNote(note);

        for (EmailAttachment emailAttachment : emailMessage.getAttachments()) {
            addEmailAttachment(emailAttachment, ticketHistory.getAttachments());
        }

        MailConfig mc = mailService.getMailConfig();
        if (mc.getNotifyTechnician() != null)
            ticketHistory.setNotifyTech(mc.getNotifyTechnician());
        if (mc.getNotifyUser() != null)
            ticketHistory.setNotifyUser(mc.getNotifyUser());

        ticketHistoryService.saveTicketHistory(ticketHistory, user);
        ticketHistoryService.flush();

        try {
            ticketHistoryService.sendEmailNotification(ticketHistory);
        } catch (MessagingException e) {
            log.error("error sending email notification", e);
        } catch (UnsupportedEncodingException e) {
            log.error("error sending email notification", e);
        }
    }

    private void createNewTicket(EmailToTicketConfig emailToTicketConfig, EmailMessage emailMessage, User user) {
        Ticket ticket = new Ticket();
        ticket.setContact(user);
        ticket.setSubmittedBy(user);
        if (user.getId().equals(User.ANONYMOUS_ID)) {
            ticket.setAnonymousEmailAddress(emailMessage.getFromAddress());
        }

        ticket.setCreatedDate(new Date());
        ticket.setGroup(emailToTicketConfig.getGroup());
        ticket.setLocation(emailToTicketConfig.getLocation());
        ticket.setSubject((StringUtils.isNotBlank(emailMessage.getSubject())) ? emailMessage.getSubject() : "");
        ticket.setStatus(statusService.getStatusById(Status.AWAITING_DISPATCH));
        ticket.setPriority(emailToTicketConfig.getTicketPriority());
        ticket.setEmailToTicketConfig(emailToTicketConfig);
        UserRoleGroup ticketPoolURG =
                userRoleGroupService.getTicketPoolByGroupId(emailToTicketConfig.getGroup().getId());
        ticket.setAssignedTo(ticketPoolURG);

        if (!emailMessage.getCcAddresses().isEmpty()) {
            String cc = "";
            for (int i=0; i<emailMessage.getCcAddresses().size(); i++) {
                if (i > 0) cc+= ", ";
                cc += emailMessage.getCcAddresses().get(i);
            }
            ticket.setCc(cc);
        }

        // Get message source accessor
        MessageSourceAccessor msa = getMessageSourceAccessor();
        // Get default locale
        Locale locale = LocaleContextHolder.getLocale();
        // Check to see if this ticket has a locale definition
        EmailToTicketConfig ettcfg = ticket.getEmailToTicketConfig();
        String locString = ettcfg != null ? ettcfg.getLocaleString() : null;
        if (StringUtils.isNotBlank(locString)) locale = LocaleUtils.toLocale(locString);

        // Build ticket note
        StringBuilder noteSb = new StringBuilder()
                .append(msa.getMessage("emailToTicket.emailFrom", new Object[]{emailMessage.getFrom()}, "Email From: {0}", locale)).append("\n\n");

        if (StringUtils.isNotBlank(emailMessage.getTextContent())) {
            noteSb.append(emailMessage.getTextContent());
        } else if (StringUtils.isNotBlank(emailMessage.getHtmlContent())) {
            String text = convertHtmlToText(emailMessage.getHtmlContent());
            noteSb.append(text);
        }

        String note = noteSb.toString();
        if (StringUtils.length(note) > MAX_NOTE_LENGTH) {
            log.warn("note from email is larger than max note length of " + MAX_NOTE_LENGTH + " characters, truncating note");
            note = StringUtils.substring(note, 0, MAX_NOTE_LENGTH);
        }

        ticket.setNote(note);

        for (EmailAttachment emailAttachment : emailMessage.getAttachments()) {
            addEmailAttachment(emailAttachment, ticket.getAttachments());
        }

        MailConfig mc = mailService.getMailConfig();
        if (mc.getNotifyTechnician() != null)
            ticket.setNotifyTech(mc.getNotifyTechnician());

        // Always notify user on a new ticket
//        ticket.setNotifyUser(Boolean.TRUE);
        ticket.setNotifyUser(emailToTicketConfig.getSendDefaultEmail());

        ticketService.saveTicket(ticket, getSystemUser());
        ticketService.flush();
        
        String htmlMessage = ticketService.createHTMLComments(ticket, null);
        String textMessage = ticketService.createTextComments(ticket, null);

        try {
            ticketService.sendEmailNotification(ticket, getSystemUser(), htmlMessage, textMessage);
        } catch (MessagingException e) {
            log.error("error sending email notification", e);
        } catch (UnsupportedEncodingException e) {
            log.error("error sending email notification", e);
        }
    }

    private String convertHtmlToText(String html) {
        String text = html;
        // remove newlines
        text = RegexUtils.stripNewLines(text);
        // remove any css tags
        text = RegexUtils.stripCss(text);
        // replace <br> tags with newline
        text = RegexUtils.replaceBRTagsWithNewLine(text);
        // replace p tags with newline
        text = RegexUtils.replacePTagsWithNewLine(text);
        // remove html opening and closing tags
        text = RegexUtils.stripHtmlTags(text);
        // replace html entities remaining in string
        text = StringEscapeUtils.unescapeHtml(text);

        return text;
    }

    private User getSystemUser() {
        return userService.getUserById(User.ADMIN_ID);
    }

    private User getAnonymousUser() {
        return userService.getUserById(User.ANONYMOUS_ID);
    }

    private void addEmailAttachment(EmailAttachment emailAttachment, Set<Attachment> attachments) {
        Attachment attachment = new Attachment();
        attachment.setContentType(emailAttachment.getContentType());
        try {
            attachment.setFileData(readFileData(emailAttachment.getFile()));
        } catch (IOException e) {
            log.error("error reading attachment file data", e);
        }
        attachment.setFileName(emailAttachment.getFilename());
        attachments.add(attachment);
    }

    private byte[] readFileData(File file) throws IOException {
        FileInputStream fos = null;
        try {
            fos = new FileInputStream(file);
            byte[] b = new byte[fos.available()];
            int bytesRead = fos.read(b);
            log.debug("bytesRead = " + bytesRead);
            return b;
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (Exception e) {
                    log.debug("error closing fileoutputstream", e);
                }
            }
        }
    }

    public void setEmailToTicketConfigService(EmailToTicketConfigService emailToTicketConfigService) {
        this.emailToTicketConfigService = emailToTicketConfigService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setStatusService(StatusService statusService) {
        this.statusService = statusService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setTicketHistoryService(TicketHistoryService ticketHistoryService) {
        this.ticketHistoryService = ticketHistoryService;
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }

    public void setPropertiesService(PropertiesService propertiesService) {
        this.propertiesService = propertiesService;
    }

    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    public void setLdapService(LdapService ldapService) {
        this.ldapService = ldapService;
    }
}
