package net.grouplink.ehelpdesk.service.acl.dataobj;

public class GlobalPermEntryDataObj {
    private String key;
    private String[] roles;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String[] getRoles() {
        return roles;
    }

    public void setRoles(String[] roles) {
        this.roles = roles;
    }
}
