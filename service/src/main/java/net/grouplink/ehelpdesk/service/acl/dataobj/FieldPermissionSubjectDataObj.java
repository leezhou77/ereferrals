package net.grouplink.ehelpdesk.service.acl.dataobj;

import java.util.ArrayList;
import java.util.List;

/**
 * author: zpearce
 * Date: 1/12/12
 * Time: 3:48 PM
 */
public class FieldPermissionSubjectDataObj {
    private String modelName;
    private String modelClass;
    private List<String> fields = new ArrayList<String>();

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getModelClass() {
        return modelClass;
    }

    public void setModelClass(String modelClass) {
        this.modelClass = modelClass;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }
}
