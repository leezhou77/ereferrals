package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.Attachment;
import net.grouplink.ehelpdesk.dao.AttachmentDao;

public class AttachmentServiceImpl implements AttachmentService {
    private AttachmentDao attachmentDao;

    public Attachment getById(Integer id) {
        return attachmentDao.getById(id);
    }

    public void delete(Attachment attachment) {
        attachmentDao.delete(attachment);
    }

    public void setAttachmentDao(AttachmentDao attachmentDao) {
        this.attachmentDao = attachmentDao;
    }
}
