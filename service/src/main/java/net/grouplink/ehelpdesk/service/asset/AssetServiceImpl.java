package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.common.utils.AssetSearch;
import net.grouplink.ehelpdesk.dao.asset.AssetDao;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.asset.AccountingInfo;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.AssetStatus;
import net.grouplink.ehelpdesk.domain.asset.AssetType;
import net.grouplink.ehelpdesk.domain.asset.Vendor;

import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: dgarcia
 * Date: Aug 27, 2007
 * Time: 3:39:22 PM
 */
public class AssetServiceImpl implements AssetService {

    private AssetDao assetDao;

    public List<Asset> getAllAssets() {
        return assetDao.getAllAssets();
    }

    public Asset getAssetById(Integer id) {
        return assetDao.getAssetById(id);
    }

    public void saveAsset(Asset asset) {
        assetDao.saveOrUpdate(asset);
    }

    public void deleteAsset(Asset asset) {
        asset.setActive(Boolean.FALSE);
        assetDao.saveOrUpdate(asset);
    }

    public List<Asset> searchAsset(AssetSearch assetSearch) {
        return assetDao.searchAssets(assetSearch);
    }

    public int getAssetCount(String searchText) {
        return assetDao.getAssetCount(searchText);
    }

    public List<Asset> searchAsset(String searchText, int start, int count, Map<String, Boolean> sortFieldMap) {
        return assetDao.searchAssets(searchText, start, count, sortFieldMap);
    }

    public List<AssetType> getUniqueTypes() {
        return assetDao.getUniqueTypes();
    }

    public List<AssetStatus> getUniqueStatus() {
        return assetDao.getUniqueStatus();
    }

    public List<Vendor> getUniqueVendors() {
        return assetDao.getUniqueVendors();
    }

    public Asset getAssetByAssetNumber(String assetNumber) {
        return assetDao.getAssetByAssetNumber(assetNumber);
    }

    public Asset getByAssetNumber(String assetNumber) {
        return assetDao.getByAssetNumber(assetNumber);
    }

    public void flush() {
        assetDao.flush();
    }

    public List<Asset> getAssetsWithLeaseExpirationDate() {
        return assetDao.getAssetsWithLeaseExpirationDate();
    }

    public List<Asset> getAssetsWithWarrantyExpirationDate() {
        return assetDao.getAssetsWithWarrantyExpirationDate();
    }

    public List<Asset> getAssetsWithAcquisitionDate() {
        return assetDao.getAssetsWithAcquisitionDate();
    }

    public int getCountByGroup(Group group) {
        return assetDao.getCountByGroup(group);
    }

    public int getCountByCategory(Category category) {
        return assetDao.getCountByCategory(category);
    }

    public int getCountByCategoryOption(CategoryOption categoryOption) {
        return assetDao.getCountByCategoryOption(categoryOption);
    }

    public List<AccountingInfo> getAllAccountingInfo(int start, int count) {
        return assetDao.getAllAccountingInfo(start, count);
    }

    public int getAccountingInfoCount() {
        return assetDao.getAccountingInfoCount();
    }

    public void deleteAccountingInfoList(List<AccountingInfo> accountingInfos) {
        assetDao.deleteAccountingInfoList(accountingInfos);
    }

    public void setAssetDao(AssetDao assetDao) {
        this.assetDao = assetDao;
    }
}
