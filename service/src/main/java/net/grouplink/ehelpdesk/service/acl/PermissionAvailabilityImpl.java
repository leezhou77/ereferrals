package net.grouplink.ehelpdesk.service.acl;

import net.grouplink.ehelpdesk.domain.Role;
import net.grouplink.ehelpdesk.domain.acl.Permission;

import java.util.HashMap;
import java.util.List;

public class PermissionAvailabilityImpl implements PermissionAvailability {
    private HashMap<Permission, List<Role>> permissions = new HashMap<Permission, List<Role>>();

    public void addPermission(Permission permission, List<Role> roles) {
        permissions.put(permission, roles);
    }

    public List<Role> getRoles(Permission permission) {
        return permissions.get(permission);
    }
}
