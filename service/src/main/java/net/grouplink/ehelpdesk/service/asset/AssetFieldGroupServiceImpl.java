package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.domain.asset.AssetFieldGroup;
import net.grouplink.ehelpdesk.domain.asset.AssetType;
import net.grouplink.ehelpdesk.dao.asset.AssetFieldGroupDao;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Jul 17, 2008
 * Time: 5:39:21 PM
 */
public class AssetFieldGroupServiceImpl implements AssetFieldGroupService {
    private AssetFieldGroupDao assetFieldGroupDao;

    public AssetFieldGroup getById(Integer id) {
        return assetFieldGroupDao.getById(id);
    }

    public List<AssetFieldGroup> getByAssetType(AssetType assetType) {
        return assetFieldGroupDao.getByAssetType(assetType);
    }

    public void saveAssetFieldGroup(AssetFieldGroup assetFieldGroup) {
        assetFieldGroupDao.saveAssetFieldGroup(assetFieldGroup);
    }

    public void deleteAssetFieldGroup(AssetFieldGroup assetFieldGroup) {
        assetFieldGroupDao.deleteAssetFieldGroup(assetFieldGroup);
    }

    public List<AssetFieldGroup> getAllAssetFieldGroups() {
        return assetFieldGroupDao.getAllAssetFieldGroups();
    }

    public void flush() {
        assetFieldGroupDao.flush();
    }

    public AssetFieldGroup getByName(String name) {
        return assetFieldGroupDao.getByName(name);
    }

    public void setAssetFieldGroupDao(AssetFieldGroupDao assetFieldGroupDao) {
        this.assetFieldGroupDao = assetFieldGroupDao;
    }
}
