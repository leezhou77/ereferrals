package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.CustomFieldOptionsDao;
import net.grouplink.ehelpdesk.domain.CustomFieldOption;
import net.grouplink.ehelpdesk.domain.CustomField;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: mabbasi
 * Date: May 5, 2007
 * Time: 3:12:17 PM
 */
public class CustomFieldOptionsServiceImpl implements CustomFieldOptionsService {

    public CustomFieldOptionsDao customFieldOptionsDao;


    public void saveCustomFieldOption(CustomFieldOption customFieldOption) {
        customFieldOptionsDao.saveCustomFieldOption(customFieldOption);
    }

    public void saveCustomFieldOptions(List<CustomFieldOption> customFieldOptions) {
        customFieldOptionsDao.saveCustomFieldOptions(customFieldOptions);
    }

    public List<CustomFieldOption> getCustomFieldOptions(Integer id) {
        return customFieldOptionsDao.getCustomFieldOptions(id);
    }

    public CustomFieldOption getCustomFieldOptionByOptionId(Integer id) {
        return customFieldOptionsDao.getCustomFieldOptionByOptionId(id);    
    }

    public CustomFieldOption createCustomFieldOption(String fieldValue, String displayValue, CustomField customField) {
        CustomFieldOption cfo = new CustomFieldOption();
        cfo.setCustomField(customField);
        cfo.setValue(fieldValue);
        cfo.setDisplayValue(displayValue);
        return cfo;
    }

    public void deleteCustomFieldOptions(List<CustomFieldOption> customFieldOptions) {
        customFieldOptionsDao.deleteCustomFieldOptions(customFieldOptions);
    }

    public void deleteCustomFieldOption(CustomFieldOption customFieldOption) {
        customFieldOptionsDao.deleteCustomFieldOption(customFieldOption);    
    }

    public void setCustomFieldOptionsDao(CustomFieldOptionsDao customFieldOptionsDao) {
        this.customFieldOptionsDao = customFieldOptionsDao;
    }
}
