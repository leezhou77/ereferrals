package net.grouplink.ehelpdesk.service.mail;

import net.grouplink.ehelpdesk.domain.EmailToTicketConfig;
import net.grouplink.ehelpdesk.domain.mail.EmailMessage;
import net.grouplink.ehelpdesk.service.EmailToTicketConfigService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Flags;
import java.util.List;

/**
 * User: jaymehafen
 * Date: Jan 26, 2008
 * Time: 4:22:46 PM
 */
public class EmailToTicketServiceImpl implements EmailToTicketService {

    private Log log = LogFactory.getLog(EmailToTicketServiceImpl.class);

    private EmailToTicketConfigService emailToTicketConfigService;
    private MailMonitor imapMailMonitorService, pop3MailMonitorService,
            imapStartTlsMailMonitorService, pop3StartTlsMailMonitorService,
            imapStartTlsRequiredMailMonitorService, pop3StartTlsRequiredMailMonitorService,
            imapSslMailMonitorService, pop3SslMailMonitorService;
    private EmailToTicketMessageProcessor emailToTicketMessageProcessor;

    public void processIncomingMail() {
        log.debug("called processIncomingEmail");
        List<EmailToTicketConfig> configItems = emailToTicketConfigService.getAllEnabled();
        log.debug("found " + configItems.size() + " e2t enabled mailboxes");
        for (EmailToTicketConfig emailToTicketConfig : configItems) {
            processEmailToTicketConfigHandleExceptions(emailToTicketConfig);
        }
    }

    private void processEmailToTicketConfigHandleExceptions(EmailToTicketConfig emailToTicketConfig) {
        MailMonitorFolder mmf = null;
        Folder folder;
        try {
            log.debug("processing mailbox " + emailToTicketConfig.getServerName() + ":" + emailToTicketConfig.getPort() + "/" + emailToTicketConfig.getUserName());
            MailMonitor mm = determineMailMonitorImpl(emailToTicketConfig);
            log.debug("connecting to mailbox folder");
            mmf = connectToFolder(emailToTicketConfig);
            folder = mmf.getFolder();
            Message[] messages;
            try {
                log.debug("getting messages from folder");
                messages = folder.getMessages();
                log.debug("found " + messages.length + " in folder");
            } catch (MessagingException e) {
                log.error("error getting messages from folder", e);
                throw new EmailToTicketRuntimeException(e);
            }

            for (Message message : messages) {
                try {
                    if (message.isSet(Flags.Flag.DELETED)) {
                        log.warn("message " + message.getMessageNumber() + " from " + message.getFrom()[0] +
                                ", with subject " + message.getSubject() + " has been marked as deleted, skipping");
                        continue;
                    }

                    if (!mm.isValidMessage(message)) {
                        log.warn("message " + message.getMessageNumber() + " from " + message.getFrom()[0] +
                                ", with subject " + message.getSubject() + " is not able to be processed, skipping");
                        continue;
                    }

                    log.debug("creating email wrapper for message" + message.getMessageNumber() + " from " +
                            message.getFrom()[0] + ", with subject " + message.getSubject());
                    EmailMessage emailMessage = mm.createEmailWrapperForMessage(message);

                    log.debug("creating/updating ticket for message");
                    emailToTicketMessageProcessor.processE2TMessage(emailMessage, emailToTicketConfig);

                    log.debug("marking message as deleted");
                    message.setFlag(Flags.Flag.DELETED, true);
                } catch (Exception e) {
                    log.error("error", e);
                }
            }
        } catch (Exception e) {
            log.error("error processing incoming mail", e);
        } finally {
            log.debug("closing folder with expunge true");
            if (mmf != null) mmf.close(true);
        }
    }

    public MailMonitorFolder connectToFolder(EmailToTicketConfig emailToTicketConfig) {
        MailMonitor mm = determineMailMonitorImpl(emailToTicketConfig);
        String mailBox = determineFolderName(emailToTicketConfig);
        return mm.connectToFolder(emailToTicketConfig.getServerName(), emailToTicketConfig.getPort(),
                emailToTicketConfig.getUserName(), emailToTicketConfig.getPassword(), mailBox);
    }

    private String determineFolderName(EmailToTicketConfig emailToTicketConfig) {
        return (StringUtils.isNotBlank(emailToTicketConfig.getImapFolder())) ?
                emailToTicketConfig.getImapFolder() : "INBOX";
    }

    private MailMonitor determineMailMonitorImpl(EmailToTicketConfig emailToTicketConfig) {
        MailMonitor mm;
        String security = emailToTicketConfig.getSecurity();

        // check if security is blank. if so, set to the appropriate value (to handle pre-9.1.4)
        if (StringUtils.isBlank(security)) {
            if (emailToTicketConfig.isUseSsl()) {
                security = EmailToTicketConfig.SECURITY_SSL;
            } else {
                security = EmailToTicketConfig.SECURITY_NONE;
            }

            emailToTicketConfig.setSecurity(security);
        }

        switch (emailToTicketConfig.getServerType()) {
            case EmailToTicketConfig.SERVER_TYPE_IMAP:
                if (StringUtils.equals(security, EmailToTicketConfig.SECURITY_NONE)) {
                    mm = imapMailMonitorService;
                } else if (StringUtils.equals(security, EmailToTicketConfig.SECURITY_TLSIFAVAILABLE)) {
                    mm = imapStartTlsMailMonitorService;
                } else if (StringUtils.equals(security, EmailToTicketConfig.SECURITY_TLS)) {
                    mm = imapStartTlsRequiredMailMonitorService;
                } else if (StringUtils.equals(security, EmailToTicketConfig.SECURITY_SSL)) {
                    mm = imapSslMailMonitorService;
                } else {
                    log.warn("unknown security value: " + security);
                    mm = imapMailMonitorService;
                }

                break;
            case EmailToTicketConfig.SERVER_TYPE_POP:
                if (StringUtils.equals(security, EmailToTicketConfig.SECURITY_NONE)) {
                    mm = pop3MailMonitorService;
                } else if (StringUtils.equals(security, EmailToTicketConfig.SECURITY_TLSIFAVAILABLE)) {
                    mm = pop3StartTlsMailMonitorService;
                } else if (StringUtils.equals(security, EmailToTicketConfig.SECURITY_TLS)) {
                    mm = pop3StartTlsRequiredMailMonitorService;
                } else if (StringUtils.equals(security, EmailToTicketConfig.SECURITY_SSL)) {
                    mm = pop3SslMailMonitorService;
                } else {
                    log.warn("unknown security value: " + security);
                    mm = pop3MailMonitorService;
                }
                
                break;
            default:
                log.error("unknown emailToTicketConfig.serverType: " + emailToTicketConfig.getServerType());
                throw new EmailToTicketRuntimeException("invalid serverType: " + emailToTicketConfig.getServerType());
        }

        return mm;
    }

    public void setEmailToTicketConfigService(EmailToTicketConfigService emailToTicketConfigService) {
        this.emailToTicketConfigService = emailToTicketConfigService;
    }

    public void setImapMailMonitorService(MailMonitor imapMailMonitorService) {
        this.imapMailMonitorService = imapMailMonitorService;
    }

    public void setPop3MailMonitorService(MailMonitor pop3MailMonitorService) {
        this.pop3MailMonitorService = pop3MailMonitorService;
    }

    public void setImapSslMailMonitorService(MailMonitor imapSslMailMonitorService) {
        this.imapSslMailMonitorService = imapSslMailMonitorService;
    }

    public void setPop3SslMailMonitorService(MailMonitor pop3SslMailMonitorService) {
        this.pop3SslMailMonitorService = pop3SslMailMonitorService;
    }

    public void setImapStartTlsMailMonitorService(MailMonitor imapStartTlsMailMonitorService) {
        this.imapStartTlsMailMonitorService = imapStartTlsMailMonitorService;
    }

    public void setPop3StartTlsMailMonitorService(MailMonitor pop3StartTlsMailMonitorService) {
        this.pop3StartTlsMailMonitorService = pop3StartTlsMailMonitorService;
    }

    public void setImapStartTlsRequiredMailMonitorService(MailMonitor imapStartTlsRequiredMailMonitorService) {
        this.imapStartTlsRequiredMailMonitorService = imapStartTlsRequiredMailMonitorService;
    }

    public void setPop3StartTlsRequiredMailMonitorService(MailMonitor pop3StartTlsRequiredMailMonitorService) {
        this.pop3StartTlsRequiredMailMonitorService = pop3StartTlsRequiredMailMonitorService;
    }

    public void setEmailToTicketMessageProcessor(EmailToTicketMessageProcessor emailToTicketMessageProcessor) {
        this.emailToTicketMessageProcessor = emailToTicketMessageProcessor;
    }
}
