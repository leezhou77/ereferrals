package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UserDetailsServiceImpl implements UserDetailsService {

    private UserService userService;

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.getUserByUsername(username);
        UserDetailsImpl userDetails = new UserDetailsImpl();
        userDetails.setUser(user);
        return userDetails;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
