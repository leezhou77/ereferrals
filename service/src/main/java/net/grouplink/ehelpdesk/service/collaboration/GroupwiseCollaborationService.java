/**
 * Currently, this allows creation of standard calendar items, as well as
 * querying Free/Busy information for other users in the Groupwise system.
 *
 * @author derickson
 * Date: 4/23/2007
 */
package net.grouplink.ehelpdesk.service.collaboration;

import com.novell.groupwise.ws.AcceptLevel;
import com.novell.groupwise.ws.AddressBook;
import com.novell.groupwise.ws.Alarm;
import com.novell.groupwise.ws.AttachmentInfo;
import com.novell.groupwise.ws.AttachmentItemInfo;
import com.novell.groupwise.ws.Authentication;
import com.novell.groupwise.ws.Contact;
import com.novell.groupwise.ws.Distribution;
import com.novell.groupwise.ws.DistributionType;
import com.novell.groupwise.ws.Filter;
import com.novell.groupwise.ws.FilterEntry;
import com.novell.groupwise.ws.FilterOp;
import com.novell.groupwise.ws.FreeBusyInfoList;
import com.novell.groupwise.ws.FreeBusyUserList;
import com.novell.groupwise.ws.From;
import com.novell.groupwise.ws.GetAddressBookListResponse;
import com.novell.groupwise.ws.GetFreeBusyResponse;
import com.novell.groupwise.ws.GetItemsResponse;
import com.novell.groupwise.ws.GroupWisePortType;
import com.novell.groupwise.ws.GroupwiseService_Impl;
import com.novell.groupwise.ws.Host;
import com.novell.groupwise.ws.Item;
import com.novell.groupwise.ws.LoginResponse;
import com.novell.groupwise.ws.MessageBody;
import com.novell.groupwise.ws.MessagePart;
import com.novell.groupwise.ws.PlainText;
import com.novell.groupwise.ws.RecipientList;
import com.novell.groupwise.ws.SendItemResponse;
import com.novell.groupwise.ws.StartFreeBusySessionResponse;
import com.novell.groupwise.ws.Status;
import com.novell.groupwise.ws.TrustedApplication;
import net.grouplink.ehelpdesk.common.collaboration.AddressBookContactsResult;
import net.grouplink.ehelpdesk.common.collaboration.Appointment;
import net.grouplink.ehelpdesk.common.collaboration.CalendarItem;
import net.grouplink.ehelpdesk.common.collaboration.CollaborationConfig;
import net.grouplink.ehelpdesk.common.collaboration.FreeBusyBlock;
import net.grouplink.ehelpdesk.common.collaboration.FreeBusyInfo;
import net.grouplink.ehelpdesk.common.collaboration.FreeBusyResult;
import net.grouplink.ehelpdesk.common.collaboration.GroupwiseSession;
import net.grouplink.ehelpdesk.common.collaboration.LoginResult;
import net.grouplink.ehelpdesk.common.collaboration.NameAndEmail;
import net.grouplink.ehelpdesk.common.collaboration.Recipient;
import net.grouplink.ehelpdesk.common.collaboration.Result;
import net.grouplink.ehelpdesk.common.collaboration.Session;
import net.grouplink.ehelpdesk.common.collaboration.Task;
import net.grouplink.ehelpdesk.common.utils.PropertiesConstants;
import net.grouplink.ehelpdesk.common.utils.GLTest;
import net.grouplink.ehelpdesk.domain.Attachment;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.service.AttachmentService;
import net.grouplink.ehelpdesk.service.PropertiesService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.rpc.Stub;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

/**
 * Groupwise implementation of the collaboration service.
 *
 * @author derickson
 */
public class GroupwiseCollaborationService implements CollaborationService {

    private final Log logger = LogFactory.getLog(getClass());

    private AttachmentService attachmentService;

    /**
	 * Error Codes
	 */
	private final int REDIRECTION_T0_NEW_HOST = 0xEA13; // Error code redirecting user login to new post office
	private final int INVALID_SESSION = 3;
	private final String INVALID_SESSION_MSG = "Invalid session";

    private final Object MapLock = new Object();

    /**
	 * Authentication variables used when logging in.
	 */
	private final String language = "en";
	private final BigDecimal version = new BigDecimal(1.0);
	private final String applicationName = "eHelpDesk";
	private final boolean trace = false;

    /**
	 * Default host configuration.
	 */
	private GroupWisePortType defaultHost = null;
	private boolean configured = false;
	private boolean ssl = false;
	private SSLSocketFactory sslSocketFactory = null;

	/**
	 * Internal objects used to track configured hosts and associate them with
	 * a user name and ip address.
	 */
	private Map<String, GroupWisePortType> userToHost = new Hashtable<String, GroupWisePortType>();
	private Map<String, GroupWisePortType> ipToHost = new Hashtable<String, GroupWisePortType>();

	/**
	 * Properties service for retrieving configuration information.
	 */
	private PropertiesService propertiesService;

	/**
	 * Configures the default host to connect to a Groupwise Post Office Agent.
	 * This function retrieves the URL and SSL flag from the database.
	 */
	public void configure() {

        boolean groupwiseIntegration = propertiesService.getBoolValueByName(PropertiesConstants.GROUP_WISE_INTEGRATION, false);
		String soapUrl = propertiesService.getPropertiesValueByName(PropertiesConstants.GROUPWISE_SOAP_URL);

        // FIXME somehow get away from the java 1.4 stuff!
		if (groupwiseIntegration && StringUtils.isNotBlank(soapUrl)) {
			try {
				// FIXME This is setup to accept any SSL certificate.
				// This should probably set up a store, and the Groupwise certificate should be installed.
				if (sslSocketFactory == null) {
					SSLContext sslContext = SSLContext.getInstance("SSL");
					sslContext.init(
						null,
						new TrustManager[]{
							new X509TrustManager(){
								public void checkClientTrusted(X509Certificate[] chain,
										String authType) throws CertificateException {}
								public void checkServerTrusted(X509Certificate[] chain,
										String authType) throws CertificateException {}
								public X509Certificate[] getAcceptedIssuers() {
									return new X509Certificate[0];
								}
							}
						},
						null
					);

                    sslSocketFactory = sslContext.getSocketFactory();
					HttpsURLConnection.setDefaultSSLSocketFactory(sslSocketFactory);
					HttpsURLConnection.setDefaultHostnameVerifier(
						new HostnameVerifier(){
							public boolean verify(String arg0, SSLSession arg1) {
								return true;
							}
						}
					);
				}
			} catch (Exception e) {
                logger.error("caught exception: " + e.getMessage(), e);
			}

			ssl = Boolean.valueOf(propertiesService.getPropertiesValueByName(PropertiesConstants.GROUPWISE_SOAP_SSL));
			defaultHost = getConfiguredHost(soapUrl, ssl);
			configured = true;
			logger.info("Collaboration service has been configured");
		}
	}

	/**
	 * Create a host connection using the ip address and port specified as an argunment,
	 * using ssl if required.  Correct connection information is not verified.
	 *
	 * @param addressAndPort e.g. 10.10.1.1:7191
	 * @param ssl - connect using ssl if set to true
	 * @return A configured endpoint for communicating with a Groupwise Post Office Agent
	 */
	private GroupWisePortType getConfiguredHost(String addressAndPort, boolean ssl) {
		GroupWisePortType host;
		String soapUrl = ssl ? "https://" : "http://";
		soapUrl += addressAndPort + "/soap";
		host = (new GroupwiseService_Impl()).getGroupwiseSOAPPort();
		((Stub)host)._setProperty(Stub.ENDPOINT_ADDRESS_PROPERTY, soapUrl);
		logger.info("Connected to Groupwise Post Office port using url: " + soapUrl);
		return host;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isConfigured() {
		return configured;
	}

	/**
	 * Use this method for logging in to a users mailbox as a Trusted Application.
	 * The trusted application values will be pulled from the properties in the
	 * database.
	 *
	 * @param userName - The login name of the user
	 * @return A Session object
	 */
	public LoginResult login(String userName) {
		LoginResult result;
		if (isConfigured()) {
			String appName = propertiesService.getPropertiesValueByName(PropertiesConstants.GROUPWISE_TRUSTEDAPP_NAME);
			String appKey = propertiesService.getPropertiesValueByName(PropertiesConstants.GROUPWISE_TRUSTEDAPP_KEY);
			TrustedApplication trustedApp = new TrustedApplication();
			trustedApp.setName(appName);
			trustedApp.setKey(appKey);
			trustedApp.setUsername(userName);

			GroupWisePortType host = getHost(userName, null);
			result = login(host, userName, trustedApp);
		} else {
			result = new LoginResult();
			result.setErrorCode(NOT_CONFIGURED);
			result.setErrorMessage(NOT_CONFIGURED_MSG);
			logger.info("Failed login: " + result.getErrorCode() + " " + result.getErrorMessage());
		}
		return result;
	}

	/**
	 * Use this method for doing a standard login, using the user name and password.
	 * @param userName - The login name of the user
	 * @param password - The user's password
	 * @return the LoginResult
	 */
	public LoginResult login(String userName, String password) {
		LoginResult result;
		if (isConfigured()) {
			PlainText plainText = new PlainText(userName, password);
			GroupWisePortType host = getHost(userName, null);
			result = login(host, userName, plainText);
		} else {
			result = new LoginResult();
			result.setErrorCode(NOT_CONFIGURED);
			result.setErrorMessage(NOT_CONFIGURED_MSG);
		}
		return result;
	}

	/**
	 * Performs the login into the Post Office Agent.  If the login request is
	 * redirected to another host, a new connection is made and the login request
	 * is repeated.  It is assumed that the host to which the login request is
	 * redirected is using the same type of port as the original host (normal or ssl).
	 *
	 * @param host - the connection used to communicate with the post office agent.
	 * @param userName the username
     * @param auth - The authentication method used (TrustedApp or PlainText).
	 * @return - the LoginResult object.
	 */
	private LoginResult login(GroupWisePortType host, String userName, Authentication auth) {
        String endPointAddress = (String) ((Stub)host)._getProperty(Stub.ENDPOINT_ADDRESS_PROPERTY);
        LoginResult result = new LoginResult();
		String session = null;
		try {
            logger.debug("calling GroupWisePortType.loginRequest, endPointAddress: " + endPointAddress + ", userName: " + userName + ", auth: " + auth);
            LoginResponse loginResp = host.loginRequest(auth, language, version, applicationName, true, trace);
			int statusCode = loginResp.getStatus().getCode();
            logger.debug("LoginResponse status code = " + statusCode);
            if (statusCode == 0) {
                logger.debug("login successful");
                session = loginResp.getSession();
				result.setErrorCode(statusCode);
			} else if (statusCode == REDIRECTION_T0_NEW_HOST) {
                logger.debug("status code is REDIRECTION_TO_NEW_HOST");
                // Attempt to connect to one of the other hosts in the list
                Host[] hosts = loginResp.getRedirectToHost();
                if (hosts == null || hosts.length == 0) {
                    logger.debug("redirectToHost list is empty!");
                } else {
                    logger.debug("redirect hosts:");
                    for (int i = 0; i < hosts.length; i++) {
                        Host h = hosts[i];
                        logger.debug("Host[" + i + "] ipAddress = " + h.getIpAddress() + ", port = " + h.getPort());
                    }
                }

                if (hosts != null && hosts.length > 0) {
					String ipAddress = hosts[0].getIpAddress() + ":" + hosts[0].getPort();
                    logger.debug("logging in to redirect host: " + ipAddress);

                    // find out if there is a host already configured with this ip address
					GroupWisePortType newHost = getHost(null, ipAddress);
					if (host == newHost) {
						newHost = getConfiguredHost(ipAddress, ssl);
					}

					// store this host to use later with this user
					// or other users on this post office
					putHost(userName, ipAddress, newHost);

					// try to login with the new host
					result = login(newHost, userName, auth);
				}
			} else {
				result.setErrorCode(statusCode);
				result.setErrorMessage(loginResp.getStatus().getDescription());
				logger.info("Login failed: " + statusCode + " " + result.getErrorMessage());
			}
		} catch (RemoteException e) {
			result.setErrorCode(CONNECTION_ERROR);
			result.setErrorMessage(e.getMessage());
            logger.error("caught RemoteException: " + e.getMessage(), e);
		}

		if (result.getErrorCode() == 0 && session != null) {
			result.setSession(new GroupwiseSession(userName, session));
		}

		return result;
	}

	/**
	 * TODO keep track of sessions that have logged in and make sure they don't timeout prematurely.
	 * Keeps the Groupwise session alive.  Default timeout is 30 minutes.
	 *
	 * @param session - The session object obtained from logging in.
	 * @return Result - no errors means the session will be active for at least
	 * 	another 30 minutes
	 */
/*	private Result keepAlive(GroupwiseSession session) {
		Result result = new Result();
		GroupWisePortType host = getHost(session.getUserName(), null);
		try {
			// noop set to true acts as a ping, performing no operations
			// but it is meant to keep the session alive.
			GetTimestampResponse resp = host.getTimestampRequest(Boolean.FALSE,
					Boolean.FALSE, Boolean.TRUE, session.getSession(), trace);
			if (resp.getStatus().getCode() != 0) {
				result.setErrorCode(resp.getStatus().getCode());
				result.setErrorMessage(resp.getStatus().getDescription());
				logger.info(resp.getStatus().getCode() + " " + resp.getStatus().getDescription());
			}
		} catch (RemoteException e) {
			result.setErrorCode(CONNECTION_ERROR);
			result.setErrorMessage(e.getMessage());
			logStackTrace(e);
		}
		return result;
	}
*/
	/**
	 * Performs the logout from the Post Office Agent.
	 * @param session - The session object obtained from logging in.
	 */
	public Result logout(Session session) {
		Result result = new Result();
		if (isConfigured()) {
			if (session instanceof GroupwiseSession) {
				GroupwiseSession gwSession = (GroupwiseSession) session;
				GroupWisePortType host = getHost(gwSession.getUserName(), null);
				try {
					Status status = host.logoutRequest(gwSession.getSession(), trace);
					if (status.getCode() != 0) {
						result.setErrorCode(status.getCode());
						result.setErrorMessage(status.getDescription());
						logger.info(status.getCode() + " " + status.getDescription());
					} else {
						result.setErrorCode(0);
					}
				} catch (RemoteException e) {
					result.setErrorCode(CONNECTION_ERROR);
					result.setErrorMessage(e.getMessage());
                    logger.error("caught RemoteException: " + e.getMessage(), e);
				}
			} else {
				result.setErrorCode(INVALID_SESSION);
				result.setErrorMessage(INVALID_SESSION_MSG);
				logger.info(INVALID_SESSION_MSG);
			}
		} else {
			result.setErrorCode(NOT_CONFIGURED);
			result.setErrorMessage(NOT_CONFIGURED_MSG);
		}

        return result;
	}

	/**
	 * Schedules or posts a calendar item (Appointment or Task)
	 *
	 * Note:
	 * 	According to Groupwise documentation, a task's priority can be numbers
	 * from A - Z or 1 - 999.  Combinations of letters and numbers must first
	 * begin with letters (A999, Z111)
	 *
	 * @param item the CalendarItem
	 * @param session - The session object obtained from logging in.
	 * @return Result
	 */
	public Result scheduleCalendarItem(CalendarItem item, Session session) {
		Result result = new Result();
		if (isConfigured()) {
			if (session instanceof GroupwiseSession) {
				GroupwiseSession gwSession = (GroupwiseSession) session;
				GroupWisePortType host = getHost(gwSession.getUserName(), null);

				try {
					com.novell.groupwise.ws.CalendarItem gwItem = getGroupwiseCalendarItem(item);

					SendItemResponse sendItemResp = host.sendItemRequest(gwItem, gwSession.getSession(), trace);
					if (sendItemResp.getStatus().getCode() != 0) {
						result.setErrorCode(sendItemResp.getStatus().getCode());
						result.setErrorMessage(sendItemResp.getStatus().getDescription());
						logger.info(sendItemResp.getStatus().getCode() + " " + sendItemResp.getStatus().getDescription());
					} else {
						result.setErrorCode(0);
					}
				} catch (RemoteException e) {
					result.setErrorCode(CONNECTION_ERROR);
					result.setErrorMessage(e.getMessage());
                    logger.error("caught RemoteException: " + e.getMessage(), e);
				} finally {
                    logout(session);
                }
			} else {
				result.setErrorCode(INVALID_SESSION);
				result.setErrorMessage(INVALID_SESSION_MSG);
				logger.info(INVALID_SESSION_MSG);
			}
		} else {
			result.setErrorCode(NOT_CONFIGURED);
			result.setErrorMessage(NOT_CONFIGURED_MSG);
		}

		return result;
	}

	/**
	 * 	Does a free/busy search on the specified users, between the start and end
	 * 	dates.
	 *
	 *	@param users - the users to search
	 *	@param startDate - the start date and time of the busy search
	 *	@param endDate - the end date and time of the busy search
	 *  @param session - The session object obtained from logging in.
	 */
	public FreeBusyResult freeBusySearch(NameAndEmail[] users, Calendar startDate, Calendar endDate, Session session) {
		FreeBusyResult result = new FreeBusyResult();
		if (isConfigured()) {
			if (session instanceof GroupwiseSession) {
				GroupwiseSession gwSession = (GroupwiseSession) session;
				GroupWisePortType host = getHost(gwSession.getUserName(), null);

				try {
					FreeBusyUserList userList = new FreeBusyUserList();
					mapFreeBusyUserList(users, userList);
					StartFreeBusySessionResponse response =
						host.startFreeBusySessionRequest(userList, startDate, endDate, gwSession.getSession(), trace);

					if (response.getStatus().getCode() == 0) {
						Integer freeBusySession = response.getFreeBusySessionId();

						extractFreeBusySearchResults(freeBusySession, gwSession, host, result);

						if (result.getErrorCode() == 0) {
							// figure out the display names associated with each email address
							mapDisplayNameToEmailAddresses(users, result);
						}

						// after retrieving all of the data or timing out, close the session
						Status status = host.closeFreeBusySessionRequest(freeBusySession, gwSession.getSession(), trace);
						if (status.getCode() != 0) {
							result.setErrorCode(status.getCode());
							result.setErrorMessage(status.getDescription());
							logger.info(status.getCode() + " " + status.getDescription());
						}
					} else {
						result.setErrorCode(response.getStatus().getCode());
						result.setErrorMessage(response.getStatus().getDescription());
						logger.info(response.getStatus().getCode() + " " + response.getStatus().getDescription());
					}
				} catch (RemoteException e) {
					result.setErrorCode(CONNECTION_ERROR);
					result.setErrorMessage(e.getMessage());
                    logger.error("caught RemoteException: " + e.getMessage(), e);
				} finally {
                    logout(session);
                }
			} else {
				result.setErrorCode(INVALID_SESSION);
				result.setErrorMessage(INVALID_SESSION_MSG);
				logger.info(INVALID_SESSION_MSG);
			}
		} else {
			result.setErrorCode(NOT_CONFIGURED);
			result.setErrorMessage(NOT_CONFIGURED_MSG);
		}

		return result;
	}

	/**
	 * Called after a free/busy search session has been started.  This function
	 * attempts to get the free/busy information immediately.  If all of the
	 * information has not been returned yet, it waits a specified time before
	 * trying again.  After a specified maximum number of tries, the information
	 * that has been gathered will be collected and returned.
	 *
	 * @param freeBusySession - the free/busy session id
	 * @param gwSession - The session object obtained from logging in.
	 * @param host - the Groupwise endpoint
	 * @param result - output parameter that will contain the list of busy information
	 * @throws RemoteException - problem connecting with the host
	 */
	private void extractFreeBusySearchResults(Integer freeBusySession,
			GroupwiseSession gwSession, GroupWisePortType host,
			FreeBusyResult result) throws RemoteException {

		// TODO make these configurable?
		int MAX_TRIES = 5;
		long millisBetweenTries = 1000; // milliseconds

		int tries = 1;
		boolean stop = false;
		List<FreeBusyInfo> freeBusyInfoList = new Vector<FreeBusyInfo>();

		// loop around until all of the free/busy info has been retrieved
		while (!stop && tries < MAX_TRIES) {
			GetFreeBusyResponse freeBusyResp = host.getFreeBusyRequest(freeBusySession.toString(), gwSession.getSession(), trace);
			if (freeBusyResp.getStatus().getCode() == 0) {
				int total = freeBusyResp.getFreeBusyStats().getTotal();
				int responded = freeBusyResp.getFreeBusyStats().getResponded();

				// if all the info is there, or this is the last time we are trying
				if (responded >= total || tries + 1 >= MAX_TRIES) {
					// extract all the users and their block information
					FreeBusyInfoList infoList = freeBusyResp.getFreeBusyInfo();
					com.novell.groupwise.ws.FreeBusyInfo[] gwFreeBusyInfo = infoList.getUser();
                    for (com.novell.groupwise.ws.FreeBusyInfo aGwFreeBusyInfo : gwFreeBusyInfo) {
                        FreeBusyInfo freeBusyInfo = new FreeBusyInfo();
                        mapFreeBusyInfo(aGwFreeBusyInfo, freeBusyInfo);
                        freeBusyInfoList.add(freeBusyInfo);
                    }
					result.setErrorCode(0);
					result.setFreeBusyInfo(freeBusyInfoList);
					stop = true;
				} else {
					// don't worry about reading the information until all of it has been reported
					try {
						// just wait for a little
						Thread.sleep(millisBetweenTries);
					} catch (InterruptedException e) {
                        logger.error("caught InterruptedException: " + e.getMessage(), e);
                    }
					// then try again
					tries++;
                }
			} else {
				result.setErrorCode(freeBusyResp.getStatus().getCode());
				result.setErrorMessage(freeBusyResp.getStatus().getDescription());
				logger.info(freeBusyResp.getStatus().getCode() + " " + freeBusyResp.getStatus().getDescription());
				stop = true;
			}
		}
	}

	/**
	 * Because the Free/Busy search only returns the email address, it is
	 * necessary to match up the display name with the search results.  This
	 * imposes the requirement that both the display name and email address
	 * are given when the free busy search is performed.
	 *
	 * TODO Another option would be to obtain a list of the users based on
	 * display name before beginning the search.
	 *
	 * @param users - list of display names and email addresses of the users to search
	 * @param result - output parameter that will contain the list of busy information
	 * @throws RemoteException - problem connecting with the host
	 */
	private void mapDisplayNameToEmailAddresses(NameAndEmail[] users, FreeBusyResult result) throws RemoteException {

		//String [] view = {"email name"};
		//Filter filter = new Filter();
		//FilterEntry displayNameMatch = new FilterEntry();
		//displayNameMatch.setOp(FilterOp.eq);
		//displayNameMatch.setField("displayName");
		//filter.setElement(displayNameMatch);

		//AddressBook systemAddrBook = null;
		List freeBusyInfoList = result.getFreeBusyInfo();
        for (NameAndEmail user1 : users) {

            /*if (StringUtils.isBlank(users[user].getEmailAddress())) {
                   // no email address, look it up in the system address book
                   if (systemAddrBook == null) {
                       GetAddressBookListResponse addrListResp = host.getAddressBookListRequest(gwSession.getSession(), trace);
                       if (addrListResp.getStatus().getCode() == 0) {
                           AddressBook[] addressBooks = addrListResp.getBooks().getBook();
                           for (int book = 0; book < addressBooks.length; book++) {
                               if (addressBooks[book].getId().startsWith("GroupWiseSystemAddressBook")) {
                                   systemAddrBook = addressBooks[book];
                                   break;
                               }
                           }
                       }
                   }
                   if (systemAddrBook != null) {
                       // look up this users
                       displayNameMatch.setValue(users[user].getDisplayName());
                       GetItemsResponse itemsResp = host.getItemsRequest(systemAddrBook.getId(),
                               view, filter, null, -1, gwSession.getSession(), trace);

                       if (itemsResp.getStatus().getCode() == 0) {
                           Items items = itemsResp.getItems();
                           Item[] itemArray = items.getItem();

                           boolean foundContact = false;
                           for (int item = 0; item < itemArray.length && !foundContact; item++) {
                               if (itemArray[item] instanceof Contact) {
                                   Contact contact = (Contact)itemArray[item];
                                   Iterator freeBusyInfoIter = freeBusyInfoList.iterator();
                                   while (freeBusyInfoIter.hasNext()) {
                                       FreeBusyInfo freeBusyInfo = (FreeBusyInfo)freeBusyInfoIter.next();
                                       if (contact.getEmailList().getPrimary().equalsIgnoreCase(freeBusyInfo.getEmail())) {
                                           freeBusyInfo.setDisplayName(contact.getName());
                                           foundContact = true;
                                           break;
                                       }
                                   }
                               }
                           }
                       }
                   }
               } else {
               */
            // email address is there, find the match
            for (Object aFreeBusyInfoList : freeBusyInfoList) {
                FreeBusyInfo freeBusyInfo = (FreeBusyInfo) aFreeBusyInfoList;
                if (user1.getEmailAddress().equalsIgnoreCase(freeBusyInfo.getEmailAddress())) {
                    freeBusyInfo.setDisplayName(user1.getDisplayName());
                    break;
                }
            }
            //}
        }
	}

	/**
	 * Maps the Grouplink calendar item to a Groupwise calendar item.  The
	 * calendar item can be an Appointment or a Task.
	 *
	 * @param item - An appointment or task to map.
	 * @return CalendarItem - the Groupwise representation of the calendar item.
	 */
	private com.novell.groupwise.ws.CalendarItem getGroupwiseCalendarItem(CalendarItem item) {
		com.novell.groupwise.ws.CalendarItem gwItem;

		if (item instanceof Appointment) {
			gwItem = new com.novell.groupwise.ws.Appointment();
			mapAppointment((Appointment)item, (com.novell.groupwise.ws.Appointment)gwItem);
		} else if (item instanceof Task) {
			gwItem = new com.novell.groupwise.ws.Task();
			mapTask((Task)item, (com.novell.groupwise.ws.Task)gwItem);
		} else {
			gwItem = new com.novell.groupwise.ws.CalendarItem();
			mapCalendarItem(item, gwItem);
		}
		return gwItem;
	}

	public AddressBookContactsResult getSystemAddressBookContacts(Session session) {
		AddressBookContactsResult result = new AddressBookContactsResult();
		if (session instanceof GroupwiseSession) {
			Filter filter = new Filter();
			FilterEntry contactTypeFilter = new FilterEntry();
			contactTypeFilter.setField("@type");
			contactTypeFilter.setOp(FilterOp.eq);
			contactTypeFilter.setValue("Contact");
			filter.setElement(contactTypeFilter);
			AddressBook systemAddrBook = getSystemAddressBook((GroupwiseSession)session);
			fillContacts(systemAddrBook.getId(), filter, (GroupwiseSession)session, result);
		} else {
			result.setErrorCode(INVALID_SESSION);
			result.setErrorMessage(INVALID_SESSION_MSG);
			logger.info(INVALID_SESSION_MSG);
		}
		return result;
	}

    public Session getCollaborationSession(User user, String password) {
        Session retVal = null;
        LoginResult result = new LoginResult();
        if (isConfigured() && user != null) {
            String appName = propertiesService.getPropertiesValueByName(PropertiesConstants.GROUPWISE_TRUSTEDAPP_NAME);
            String appKey = propertiesService.getPropertiesValueByName(PropertiesConstants.GROUPWISE_TRUSTEDAPP_KEY);
            if (StringUtils.isNotBlank(appName) && StringUtils.isNotBlank(appKey)) {
                // Just collaboration id, no password
                if (user.getColLoginId() != null) {
                    result = login(user.getColLoginId());
                }

                // eHelpDesk login id, no password
                // watch for a connection timeout so it doesn't time out over and over
                if (result.getErrorCode() != 0
                        && result.getErrorCode() != CollaborationService.CONNECTION_ERROR) {
                    result = login(user.getLoginId());
                }
            }

            // Collaboration id and password
            if (result.getErrorCode() != 0
                    && result.getErrorCode() != CollaborationService.CONNECTION_ERROR
                    && StringUtils.isNotBlank(user.getColLoginId())
                    && StringUtils.isNotBlank(user.getColPassword())) {
                result = login(user.getColLoginId(), GLTest.decryptPassword(user.getColPassword()));
            }

            // collaboration id and ehd password
            if (result.getErrorCode() != 0
                    && result.getErrorCode() != CollaborationService.CONNECTION_ERROR
                    && StringUtils.isNotBlank(user.getColLoginId())) {
                result = login(user.getColLoginId(), password);
            }

            // eHelpDesk id and password
            if (result.getErrorCode() != 0
                    && result.getErrorCode() != CollaborationService.CONNECTION_ERROR) {
                result = login(user.getLoginId(), password);
            }

            // if login was successful, store the collaboration session object
            // in the users web session
            if (result.getErrorCode() == 0) {
//                request.getSession().setAttribute(SessionConstants.COLLABORATION_SESSION, result.getSession());
                retVal = result.getSession();
                logger.info("Successfully logged in to collaboration system");
            } else {
                logger.info("Failed to login to collaboration system");
            }
        } else {
            logger.info("Integration with the collaboration system has not been configured");
        }

        return retVal;
    }

    private void fillContacts(String containerId, Filter filter, GroupwiseSession gwSession, AddressBookContactsResult result) {
		try {
			GroupWisePortType host = getHost(gwSession.getUserName(), null);
			GetItemsResponse response = host.getItemsRequest(containerId, new String[]{"name email"}, filter, null, -1, gwSession.getSession(), trace);
			if (response.getStatus().getCode() == 0) {
				Item[] items = response.getItems().getItem();
                for (Item item : items) {
                    if (item instanceof Contact) {
                        Contact contact = (Contact) item;
                        String displayName = contact.getName();
                        String email = "";
                        if (contact.getEmailList() != null) {
                            email = contact.getEmailList().getPrimary();
                        }
                        result.setErrorCode(0);
                        result.addContact(displayName, email);
                    }
                }
			} else {
				result.setErrorCode(response.getStatus().getCode());
				result.setErrorMessage(response.getStatus().getDescription());
				logger.info(response.getStatus().getCode() + " " + response.getStatus().getDescription());
			}
		} catch (RemoteException e) {
			result.setErrorCode(CONNECTION_ERROR);
			result.setErrorMessage(e.getMessage());
            logger.error("caught RemoteException: " + e.getMessage(), e);
		}
	}

	private AddressBook getSystemAddressBook(GroupwiseSession gwSession) {
		AddressBook systemAddrBook = null;
		GroupWisePortType host = getHost(gwSession.getUserName(), null);
		try {
			GetAddressBookListResponse addrListResp = host.getAddressBookListRequest(gwSession.getSession(), trace);
			if (addrListResp.getStatus().getCode() == 0) {
				AddressBook[] addressBooks = addrListResp.getBooks().getBook();
                for (AddressBook addressBook : addressBooks) {
                    if (addressBook.getId().startsWith("GroupWiseSystemAddressBook")) {
                        systemAddrBook = addressBook;
                        break;
                    }
                }
			}
		} catch (RemoteException e) {
            logger.error("caught RemoteException: " + e.getMessage(), e);
		}

        return systemAddrBook;
	}


	public CollaborationConfig getCollaborationConfig() {
		CollaborationConfig config = new CollaborationConfig();
		config.setSoapUrl(propertiesService.getPropertiesValueByName(PropertiesConstants.GROUPWISE_SOAP_URL));
		config.setSoapSsl(propertiesService.getBoolValueByName(PropertiesConstants.GROUPWISE_SOAP_SSL, false));
		config.setTrustedAppName(propertiesService.getPropertiesValueByName(PropertiesConstants.GROUPWISE_TRUSTEDAPP_NAME));
		config.setTrustedAppKey(propertiesService.getPropertiesValueByName(PropertiesConstants.GROUPWISE_TRUSTEDAPP_KEY));

        config.setOutlookIntegration(propertiesService.getBoolValueByName(PropertiesConstants.OUTLOOK_INTEGRATION, false));
        config.setGroupwiseIntegration(propertiesService.getBoolValueByName(PropertiesConstants.GROUP_WISE_INTEGRATION, false));
        config.setGroupwiseIntegration6x(propertiesService.getBoolValueByName(PropertiesConstants.GROUP_WISE_INTEGRATION_6X, false));
		return config;
	}

	public void saveCollaborationConfig(CollaborationConfig config) {

        propertiesService.saveProperties(PropertiesConstants.GROUP_WISE_INTEGRATION, Boolean.toString(config.isGroupwiseIntegration()));
        propertiesService.saveProperties(PropertiesConstants.GROUP_WISE_INTEGRATION_6X, Boolean.toString(config.isGroupwiseIntegration6x()));
        propertiesService.saveProperties(PropertiesConstants.OUTLOOK_INTEGRATION, Boolean.toString(config.isOutlookIntegration()));

        if (config.isGroupwiseIntegration()) {
            propertiesService.saveProperties(PropertiesConstants.GROUPWISE_SOAP_URL, config.getSoapUrl());
            propertiesService.saveProperties(PropertiesConstants.GROUPWISE_SOAP_SSL, Boolean.toString(config.isSoapSsl()));
			propertiesService.saveProperties(PropertiesConstants.GROUPWISE_TRUSTEDAPP_NAME, config.getTrustedAppName());
			propertiesService.saveProperties(PropertiesConstants.GROUPWISE_TRUSTEDAPP_KEY, config.getTrustedAppKey());
			configure();
		} else {
			disableConfiguration();
        }
    }

	public void disableConfiguration() {
		configured = false;
	}

	/**
	 * Tracks the configured host by user name and ip address.
	 * @param userName the user name
	 * @param ipAddress the ip address
	 * @param host the GroupWisePortType
	 */
	private void putHost(String userName, String ipAddress, GroupWisePortType host) {
		synchronized (MapLock) {
			userToHost.put(userName, host);
			ipToHost.put(ipAddress, host);
		}
	}

	/**
	 * Retrieves a configured host by user name or ipaddress.  First, a lookup
	 * is done against the user name.  If that is null or does not have an entry,
	 * a lookup is done on the ip address.  If that is null or does not have an
	 * entry, the default configured host is used.
	 * @param userName the user name
	 * @param ipAddress the ip address
	 * @return the GroupWisePortType
	 */
	private GroupWisePortType getHost(String userName, String ipAddress) {
		GroupWisePortType host = null;
		synchronized (MapLock) {
			if (userName != null) {
				host = userToHost.get(userName);
			}

            if (host == null && ipAddress != null) {
				host = ipToHost.get(ipAddress);
			}
		}

        if (host == null) {
			host = defaultHost;
		}

        return host;
	}

    /**
     * Maps the generic calendar item to a Groupwise calendar item
     * @param item - generic calendar item as input
     * @param gwItem - Groupwise calendar item as output
     */
    private void mapCalendarItem(CalendarItem item, com.novell.groupwise.ws.CalendarItem gwItem) {

        // subject
        gwItem.setSubject(item.getSubject());

        // body
        String message = item.getMessage();
        if (StringUtils.isNotBlank(message)) {
            if ("text/html".equalsIgnoreCase(item.getContentType())) {

                // html content is added as an attachment
                Attachment htmlAttachment = new Attachment();
                htmlAttachment.setFileName("Text.htm");
                htmlAttachment.setContentType("text/html");
                htmlAttachment.setFileData(message.getBytes());

                List<Attachment> attachments = item.getAttachments();

                if (attachments.size() > 0) {
                    Attachment firstAttachment = attachments.get(0);
                    if ("Text.htm".equals(firstAttachment.getFileName())) {
                        attachments.remove(0);
                    }
                }
                attachments.add(0, htmlAttachment);
            } else {
                MessagePart messagePart = new MessagePart();
                messagePart.set_value(item.getMessage().getBytes());
                messagePart.setContentType(item.getContentType());
                gwItem.setMessage(new MessageBody(new MessagePart[]{messagePart}));
            }
        }

        Distribution distribution = new Distribution();

        // Recipient list
        int totalRecipients = item.getRecipients().size();
        if (totalRecipients > 0) {
            RecipientList recipientList = new RecipientList();
            com.novell.groupwise.ws.Recipient[] recipients = new com.novell.groupwise.ws.Recipient[totalRecipients];

            for (int recipIndex = 0; recipIndex < totalRecipients; recipIndex++) {
                Recipient recip = item.getRecipients().get(recipIndex);
                recipients[recipIndex] = new com.novell.groupwise.ws.Recipient();
                recipients[recipIndex].setDisplayName(recip.getDisplayName());
                recipients[recipIndex].setEmail(recip.getEmailAddress());
                recipients[recipIndex].setDistType(DistributionType.fromString(recip.getTargetType().toString()));
            }

            recipientList.setRecipient(recipients);
            distribution.setRecipients(recipientList);
        }

        From gwFrom = new From();
        NameAndEmail from = item.getFrom();
        if (from != null) {
            gwFrom.setDisplayName(from.getDisplayName());
            gwFrom.setEmail(from.getEmailAddress());
            distribution.setFrom(gwFrom);
        }

        gwItem.setDistribution(distribution);

        // attachments
        int totalAttachments = item.getAttachments().size();
        if (totalAttachments > 0) {
            AttachmentInfo attachmentInfo = new AttachmentInfo();
            AttachmentItemInfo[] attachments = new AttachmentItemInfo[totalAttachments];
            for (int attachIndex = 0; attachIndex < totalAttachments; attachIndex++) {
                Attachment attachment = item.getAttachments().get(attachIndex);
                // reinitialize attachment to avoid lazy property error
                if(attachment.getId()!= null){
                    attachment = attachmentService.getById(attachment.getId());
                }
                attachments[attachIndex] = new AttachmentItemInfo();
                attachments[attachIndex].setName(attachment.getFileName());
                attachments[attachIndex].setContentType(attachment.getContentType());
                attachments[attachIndex].setData(attachment.getFileData());
            }
            
            attachmentInfo.setAttachment(attachments);
            gwItem.setAttachments(attachmentInfo);
        }
    }

    /**
     * Maps the generic appointment to a Groupwise appointment
     *
     * @param appt - generic appointment as input
     * @param gwAppt - Groupwise appointment as output
     */
    private void mapAppointment(Appointment appt, com.novell.groupwise.ws.Appointment gwAppt) {
        mapCalendarItem(appt, gwAppt);

        // place
        gwAppt.setPlace(appt.getPlace());

        // start and end dates and days
        gwAppt.setStartDate(appt.getStartDate());
        gwAppt.setEndDate(appt.getEndDate());
        gwAppt.setStartDay(appt.getStartDate());
        gwAppt.setEndDay(appt.getEndDate());

        // all day event
        gwAppt.setAllDayEvent(appt.isAllDayEvent());

        // alarm enabled.  Groupwise takes it in seconds.
        gwAppt.setAlarm(new Alarm(appt.isAlarmEnabled(), appt.getAlarmTimeInMinutes() * 60));

        // accept level
        if (appt.getAcceptLevel() != null) {
            gwAppt.setAcceptLevel(AcceptLevel.fromString(appt.getAcceptLevel()));
        }
    }

    /**
     * Maps the generic task to a Groupwise task
     *
     * @param task - generic task as input
     * @param gwTask - Groupwise task as output
     */
    private void mapTask(Task task, com.novell.groupwise.ws.Task gwTask) {
        mapCalendarItem(task, gwTask);

        // dates
        gwTask.setAssignedDate(task.getAssignedDate());
        gwTask.setStartDate(task.getStartDate());
        gwTask.setDueDate(task.getDueDate());

        // completed status
        gwTask.setCompleted(task.isCompleted());

        // priority
        gwTask.setTaskPriority(task.getPriority());
    }

    /**
     * Maps a generic list of NameAndEmail users to a FreeBusyUserList.
     *
     * @param users - generic array of NameAndEmail objects
     * @param userList - output parameter for the Groupwise list of FreeBusyUsers
     */
    private void mapFreeBusyUserList(NameAndEmail[] users, FreeBusyUserList userList) {
        com.novell.groupwise.ws.NameAndEmail[] gwUsers = new com.novell.groupwise.ws.NameAndEmail[users.length];
        for (int user = 0; user < users.length; user++) {
            gwUsers[user] = new com.novell.groupwise.ws.NameAndEmail();
            gwUsers[user].setDisplayName(users[user].getDisplayName());
            gwUsers[user].setEmail(users[user].getEmailAddress());
        }
        userList.setUser(gwUsers);
    }

    /**
     * Maps a Groupwise FreeBusyInfo object to a generic FreeBusyInfo object.
     *
     * @param gwFreeBusyInfo - Groupwise FreeBusyInfo object
     * @param freeBusyInfo - Generic FreeBusyInfo object
     */
    private void mapFreeBusyInfo(com.novell.groupwise.ws.FreeBusyInfo gwFreeBusyInfo, FreeBusyInfo freeBusyInfo) {
        freeBusyInfo.setDisplayName(gwFreeBusyInfo.getDisplayName());
        freeBusyInfo.setEmailAddress(gwFreeBusyInfo.getEmail());

        com.novell.groupwise.ws.FreeBusyBlock[] blocks = gwFreeBusyInfo.getBlocks().getBlock();
        for (com.novell.groupwise.ws.FreeBusyBlock block1 : blocks) {
            FreeBusyBlock newBlock = new FreeBusyBlock();

            newBlock.setAcceptLevel(block1.getAcceptLevel().toString());
            newBlock.setStartDate(block1.getStartDate());
            newBlock.setEndDate(block1.getEndDate());
            newBlock.setSubject(block1.getSubject());
            freeBusyInfo.addFreeBusyBlock(newBlock);
        }
    }

    /**
     * Setter for Spring when creating this service
     *
     * @param propertiesService the PropertiesService
     */
    public void setPropertiesService(PropertiesService propertiesService) {
        this.propertiesService = propertiesService;
    }

    public void setAttachmentService(AttachmentService attachmentService) {
        this.attachmentService = attachmentService;
    }

}