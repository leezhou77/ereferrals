package net.grouplink.ehelpdesk.service.mail;

import java.util.Properties;

public class ImapStartTlsRequiredMailMonitor extends ImapStartTlsIfAvailMailMonitor {
    @Override
    protected void customizeSessionProperties(Properties props) {
        super.customizeSessionProperties(props);

        // set tls required prop
        props.setProperty("mail.imap.starttls.required", "true");
    }
}
