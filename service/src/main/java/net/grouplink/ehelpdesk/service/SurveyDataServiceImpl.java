package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.SurveyDataDao;
import net.grouplink.ehelpdesk.domain.SurveyData;

import java.util.List;

/**
 * Date: Apr 6, 2008
 * Time: 8:27:01 PM
 */
public class SurveyDataServiceImpl implements SurveyDataService {

    private SurveyDataDao surveyDataDao;

    public void saveSurveyData(SurveyData surveyData) {
        surveyDataDao.saveSurveyData(surveyData);
    }

    public List<SurveyData> getByTicketId(Integer ticketId) {
        return surveyDataDao.getByTicketId(ticketId);
    }

    public int getCountByTicketId(Integer ticketId) {
        return surveyDataDao.getCountByTicketId(ticketId);
    }

    public SurveyDataDao getSurveyDataDao() {
        return surveyDataDao;
    }

    public void setSurveyDataDao(SurveyDataDao surveyDataDao) {
        this.surveyDataDao = surveyDataDao;
    }
}
