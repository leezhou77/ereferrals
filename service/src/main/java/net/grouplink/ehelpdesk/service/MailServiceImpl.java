package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.common.utils.GLTest;
import net.grouplink.ehelpdesk.common.utils.MailConfig;
import net.grouplink.ehelpdesk.common.utils.MailMessage;
import net.grouplink.ehelpdesk.common.utils.PropertiesConstants;
import net.grouplink.ehelpdesk.common.utils.RegexUtils;
import net.grouplink.ehelpdesk.domain.EmailToTicketConfig;
import net.grouplink.ehelpdesk.domain.Role;
import net.grouplink.ehelpdesk.domain.Survey;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketHistory;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRole;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.service.mail.MultipartMailer;
import org.apache.commons.lang.LocaleUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.util.ByteArrayDataSource;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.activation.DataHandler;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;
import java.util.Iterator;

public class MailServiceImpl extends ApplicationObjectSupport implements MailService, Serializable {

    protected final Log log = LogFactory.getLog(getClass());

    private JavaMailSenderImpl mailSender;
    private PropertiesService propertiesService;

    private boolean mailConfigured = false;
    private UserRoleGroupService userRoleGroupService;
    private EmailToTicketConfigService emailToTicketConfigService;

    public boolean init() {
        MailConfig mailConfig = getMailConfig();

        String host = mailConfig.getHost();
        if (StringUtils.isNotBlank(host)) {
            Properties javaMailProperties = mailSender.getJavaMailProperties();

            mailSender.setHost(host);

            String port = mailConfig.getPort();
            if (StringUtils.isNotBlank(port)) {
                mailSender.setPort(Integer.valueOf(port));
            } else {
                mailSender.setPort(JavaMailSenderImpl.DEFAULT_PORT);
            }

            String security = mailConfig.getSecurity();
            if (security.equals(MailConfig.SECURITY_NONE)) {
                mailSender.setProtocol(JavaMailSenderImpl.DEFAULT_PROTOCOL);
                javaMailProperties.setProperty("mail.smtp.starttls.enable", "false");
                javaMailProperties.setProperty("mail.smtp.starttls.required", "false");
            } else if (security.equals(MailConfig.SECURITY_TLSIFAVAILABLE)) {
                mailSender.setProtocol(JavaMailSenderImpl.DEFAULT_PROTOCOL);
                javaMailProperties.setProperty("mail.smtp.starttls.enable", "true");
                javaMailProperties.setProperty("mail.smtp.starttls.required", "false");
            } else if (security.equals(MailConfig.SECURITY_TLS)) {
                mailSender.setProtocol(JavaMailSenderImpl.DEFAULT_PROTOCOL);
                javaMailProperties.setProperty("mail.smtp.starttls.enable", "true");
                javaMailProperties.setProperty("mail.smtp.starttls.required", "true");
            } else if (security.equals(MailConfig.SECURITY_SSL)) {
                mailSender.setProtocol("smtps");
                javaMailProperties.setProperty("mail.smtp.starttls.enable", "false");
                javaMailProperties.setProperty("mail.smtp.starttls.required", "false");
            }

            String username = mailConfig.getUserName();
            if (StringUtils.isNotBlank(username)) {
                mailSender.setUsername(username);
                String password = mailConfig.getPassword();
                mailSender.setPassword(password);
                javaMailProperties.setProperty("mail.smtp.auth", "true");
                javaMailProperties.setProperty("mail.smtps.auth", "true");
            } else {
                javaMailProperties.setProperty("mail.smtp.auth", "false");
                javaMailProperties.setProperty("mail.smtps.auth", "false");
                mailSender.setUsername(null);
                mailSender.setPassword(null);
            }

            String encoding = mailConfig.getEncoding();
            if (StringUtils.isNotBlank(encoding)) {
                mailSender.setDefaultEncoding(encoding);
            }

            if (StringUtils.isNotBlank(System.getProperty("mail.debug"))) {
                javaMailProperties.setProperty("mail.debug", System.getProperty("mail.debug"));
            } else {
                javaMailProperties.setProperty("mail.debug", "false");
            }

            javaMailProperties.setProperty("mail.smtp.connectiontimeout", "60000");
            javaMailProperties.setProperty("mail.smtps.connectiontimeout", "60000");

            // initialize session
            Session session = Session.getInstance(javaMailProperties);
            mailSender.setSession(session);

            mailConfigured = true;
            return true;
        }

        mailConfigured = false;
        return false;
    }

    public MailMessage newMessage() {
        try {
            return new MailMessage(mailSender);
        }
        catch (MessagingException e) {
            log.error("Error creating new MailMessage", e);
        }
        return null;
    }

    public MultipartMailer newMultipartMailer() {
        try {
            return new MultipartMailer(mailSender, MultipartMailer.MULTIPART_ALTERNATIVE);
        } catch (MessagingException e) {
            log.error("Error creating new MultipartMailer", e);
        }
        return null;
    }

    public void sendMail(MailMessage mailMessage) {
        if (mailConfigured && mailMessage.isMessageReady()) {
            try {
                mailSender.send(mailMessage.getMimeMessage());
            }
            catch (Exception e) {
                log.error("error sending mail", e);
            }
        }
    }

    public void sendMail(MimeMessage mime) {
        if (mailConfigured) {
            try {
                mailSender.send(mime);
            }
            catch (Exception e) {
                log.error("error sending mail", e);
            }
        }
    }

    public MailConfig getMailConfig() {
        MailConfig mailConfig = new MailConfig();
        mailConfig.setHost(propertiesService.getPropertiesValueByName(PropertiesConstants.MAIL_HOST));
        mailConfig.setPort(propertiesService.getPropertiesValueByName(PropertiesConstants.MAIL_PORT));
        mailConfig.setUseSsl(propertiesService.getBoolValueByName(PropertiesConstants.MAIL_USESSL, false));
        mailConfig.setUserName(propertiesService.getPropertiesValueByName(PropertiesConstants.MAIL_USERNAME));
        mailConfig.setPassword(GLTest.decryptPassword(propertiesService.getPropertiesValueByName(PropertiesConstants.MAIL_PASSWORD)));
        mailConfig.setNotifyTechnician(propertiesService.getBoolValueByName(PropertiesConstants.MAIL_NOTIFY_TECHNIAN, false));
        mailConfig.setNotifyUser(propertiesService.getBoolValueByName(PropertiesConstants.MAIL_NOTIFY_USER, false));
        mailConfig.setSendLinkOnNotification(propertiesService.getBoolValueByName(PropertiesConstants.MAIL_SEND_LINK_ON_NOTIFICATION, false));
        mailConfig.setEmailDisplayName(propertiesService.getPropertiesValueByName(PropertiesConstants.MAIL_EMAIL_DISPLAY_NAME));
        mailConfig.setEmailFromAddress(propertiesService.getPropertiesValueByName(PropertiesConstants.MAIL_EMAIL_FROM_ADDRESS));
        mailConfig.setEncoding(propertiesService.getPropertiesValueByName(PropertiesConstants.MAIL_ENCODING));
        mailConfig.setSecurity(propertiesService.getPropertiesValueByName(PropertiesConstants.MAIL_SECURITY));

        // initialize mailConfig.security if blank
        if (StringUtils.isBlank(mailConfig.getSecurity())) {
            if (mailConfig.getUseSsl()) {
                mailConfig.setSecurity(MailConfig.SECURITY_SSL);
            } else {
                mailConfig.setSecurity(MailConfig.SECURITY_NONE);
            }
        }
        
        return mailConfig;
    }

    /**
     * After calling this method make sure ServletContext has been initialized
     * by calling ApplicationInitializer.mailInit()
     *
     * @param mailConfig the MailConfig to save
     */
    public void saveMailConfig(MailConfig mailConfig) {
        propertiesService.saveProperties(PropertiesConstants.MAIL_HOST, mailConfig.getHost());
        propertiesService.saveProperties(PropertiesConstants.MAIL_PORT, mailConfig.getPort());
        propertiesService.saveProperties(PropertiesConstants.MAIL_USESSL, mailConfig.getUseSsl().toString());
        propertiesService.saveProperties(PropertiesConstants.MAIL_USERNAME, mailConfig.getUserName());
        propertiesService.saveProperties(PropertiesConstants.MAIL_PASSWORD, GLTest.encryptPassword(mailConfig.getPassword()));
        propertiesService.saveProperties(PropertiesConstants.MAIL_NOTIFY_TECHNIAN, mailConfig.getNotifyTechnician().toString());
        propertiesService.saveProperties(PropertiesConstants.MAIL_NOTIFY_USER, mailConfig.getNotifyUser().toString());
        propertiesService.saveProperties(PropertiesConstants.MAIL_SEND_LINK_ON_NOTIFICATION, mailConfig.getSendLinkOnNotification().toString());
        propertiesService.saveProperties(PropertiesConstants.MAIL_EMAIL_DISPLAY_NAME, mailConfig.getEmailDisplayName());
        propertiesService.saveProperties(PropertiesConstants.MAIL_EMAIL_FROM_ADDRESS, mailConfig.getEmailFromAddress());
        propertiesService.saveProperties(PropertiesConstants.MAIL_ENCODING, mailConfig.getEncoding());
        propertiesService.saveProperties(PropertiesConstants.MAIL_SECURITY, mailConfig.getSecurity());
        propertiesService.flush();
        init();
    }

    public void notifyParties(User user, String htmlMessage, String txtMessage, Ticket ticket, String fromEmail, String displayEmail,
                              Boolean sendLinkToUser)
            throws MessagingException, UnsupportedEncodingException {

        sendEmailNotification(user, ticket, null, fromEmail, displayEmail, htmlMessage, txtMessage, ticket.getNotifyTech(), ticket.getNotifyUser(),
                sendLinkToUser);
    }

    public void notifyPartiesTicketHistory(User user, String htmlMessage, String txtMessage, TicketHistory ticketHistory, String fromEmail,
                                           String displayEmail, Boolean sendLinkToUser)
            throws MessagingException, UnsupportedEncodingException {

        sendEmailNotification(user, ticketHistory.getTicket(), ticketHistory, fromEmail, displayEmail, htmlMessage,
                txtMessage, ticketHistory.getNotifyTech(), ticketHistory.getNotifyUser(), sendLinkToUser);
    }

    private void sendEmailNotification(User user, Ticket ticket, TicketHistory ticketHistory, String fromEmail,
                                       String displayEmail, String htmlMessage, String textMessage, Boolean notifyTech, Boolean notifyUser,
                                       Boolean sendLinkToUser)
            throws MessagingException, UnsupportedEncodingException {

        assert user != null;

        MessageSourceAccessor accessor = getMessageSourceAccessor();

        // Get default locale
        Locale locale = LocaleContextHolder.getLocale();

        // Check to see if this ticket has a locale definition
        EmailToTicketConfig ettcfg = ticket.getEmailToTicketConfig();
        String locString = ettcfg!=null?ettcfg.getLocaleString():null;
        if(StringUtils.isNotBlank(locString)) locale = LocaleUtils.toLocale(locString);

        MimeMessage mime = new MimeMessage(mailSender.getSession());
        if (StringUtils.isBlank(displayEmail)) {
            mime.setFrom(new InternetAddress(fromEmail, true));
        } else {
            mime.setFrom(new InternetAddress(fromEmail, displayEmail));
        }

        int tid = ticket.getTicketId();
        String originalSubject = ticket.getSubject() == null?"":ticket.getSubject();
        String tidToken = "{TID:" + tid + "}";
        String subject;
        if(originalSubject.indexOf(tidToken) < 0) {
            subject = new StringBuilder().append(" {TID:").append(tid).append("} ")
                    .append(accessor.getMessage("ticket.number", locale))
                    .append(" ").append(tid).append(" - ").append(ticket.getSubject()).toString();
            mime.setSubject(subject, "UTF-8"); 
        }

        Multipart mimeMp = new MimeMultipart("alternative");
        BodyPart bpText = new MimeBodyPart();
        bpText.setContent(textMessage, "text/plain;charset=UTF-8");
        mimeMp.addBodyPart(bpText);

        // Sending link on HTML message part, if configured
        if (sendLinkToUser && StringUtils.isNotBlank(textMessage)) {
            String baseUrl = propertiesService.getPropertiesValueByName(PropertiesConstants.BASE_URL);
            htmlMessage += new StringBuilder()
                    .append("<p><a href=\"").append(baseUrl)
                    .append("/tickets/").append(ticket.getId()).append("/edit")
                    .append("?smtl=true")
                    .append("\">")
                    .append(accessor.getMessage("ticket.view", locale))
                    .append("</a></p>").toString();
        }
        
        try {
            BodyPart bpHtml = new MimeBodyPart();
            bpHtml.setDataHandler(new DataHandler(new ByteArrayDataSource(htmlMessage, "text/html;charset=UTF-8")));
//            bpHtml.setContent(htmlMessage, "text/html;charset=UTF-8");
            mimeMp.addBodyPart(bpHtml);
        } catch (IOException e) {
            log.error("error", e);
        }

        mime.setContent(mimeMp);
        
        UserRoleGroup assignedTo = ticket.getAssignedTo();
        boolean hasRecipient = false;
        if (notifyTech) {
            //Notify technician
            UserRole assignedToUserRole = assignedTo.getUserRole();
            if (assignedToUserRole != null) {
                String assignedToEmail = assignedToUserRole.getUser().getEmail();
                if (!isMonitoredMailboxEmail(assignedToEmail)) {
                    mime.addRecipient(MimeMessage.RecipientType.TO, new InternetAddress(assignedToEmail));
                    hasRecipient = true;
                }
            }

            //Notifying additional notification users
            Set<User> notificationUsers = assignedTo.getNotificationUsers();
            // clear the set of users that are no longer a part of this group
            notificationUsers = clearUnassociatedUsers(notificationUsers, ticket.getGroup().getId());
            if (notificationUsers.size() > 0) {
                for (User nUser : notificationUsers) {
                    if (!isMonitoredMailboxEmail(nUser.getEmail())) {
                        mime.addRecipient(MimeMessage.RecipientType.TO, new InternetAddress(nUser.getEmail()));
                        hasRecipient = true;
                    }
                }
            }
        }  // --> if (notifyTech.booleanValue())
        
        //Notify User
        if (notifyUser) {
            if (StringUtils.isNotBlank(ticket.getContact().getEmail())) {
                if (!isMonitoredMailboxEmail(ticket.getContact().getEmail())) {
                    mime.addRecipient(MimeMessage.RecipientType.TO, new InternetAddress(ticket.getContact().getEmail()));
                    hasRecipient = true;
                }
            }

            // Notify anonymousEmail
            if (ticket.getContact().getId().equals(User.ANONYMOUS_ID) &&
                    StringUtils.isNotBlank(ticket.getAnonymousEmailAddress())) {
                if (!isMonitoredMailboxEmail(ticket.getAnonymousEmailAddress())) {
                    mime.addRecipient(MimeMessage.RecipientType.TO, new InternetAddress(ticket.getAnonymousEmailAddress()));
                    hasRecipient = true;
                }
            }
        }

        //Setting to CC
        if (StringUtils.isNotBlank(ticket.getCc())) {
            String[] cc = ticket.getCc().split(RegexUtils.EMAIL_LIST_REGEX);
            for (String aCc : cc) {
                if (StringUtils.isNotBlank(aCc)) {
                    if (!isMonitoredMailboxEmail(aCc)) {
                        mime.addRecipient(MimeMessage.RecipientType.CC, new InternetAddress(aCc));
                        hasRecipient = true;
                    }
                }
            }
        }

        // bcc
        if (StringUtils.isNotBlank(ticket.getBc())) {
            String[] bcc = ticket.getBc().split(RegexUtils.EMAIL_LIST_REGEX);
            for (String aBcc : bcc) {
                if (StringUtils.isNotBlank(aBcc)) {
                    if (!isMonitoredMailboxEmail(aBcc)) {
                        mime.addRecipient(MimeMessage.RecipientType.BCC, new InternetAddress(aBcc));
                        hasRecipient = true;
                    }
                }
            }
        }

        if (ticketHistory != null && StringUtils.isNotBlank(ticketHistory.getCc())) {
            String[] cc = ticketHistory.getCc().split(RegexUtils.EMAIL_LIST_REGEX);
            for (String aCc : cc) {
                if (StringUtils.isNotBlank(aCc)) {
                    if (!isMonitoredMailboxEmail(aCc)) {
                        mime.addRecipient(MimeMessage.RecipientType.CC, new InternetAddress(aCc));
                        hasRecipient = true;
                    }
                }
            }
        }

        if ((StringUtils.isNotBlank(textMessage)) && (hasRecipient)) {
            sendMail(mime);
        }
    }

    /**
     * Checks if the given email address matches one of the mailboxes monitored
     * by emailtoticket.
     *
     * @param emailAddress the email address to check
     * @return true if it matches, false otherwise
     */
    private boolean isMonitoredMailboxEmail(String emailAddress) {
        List<EmailToTicketConfig> e2tConfigs = emailToTicketConfigService.getAll();
        for (EmailToTicketConfig e2tConfig : e2tConfigs) {
            if (StringUtils.equalsIgnoreCase(emailAddress, e2tConfig.getReplyEmailAddress())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Utility method to clear Users from a set by checking each User against a
     * list of Users belonging to a Group
     *
     * @param notificationUsers set of notification users
     * @param groupId the groupId that the notification users must be tech or
     *          manager of
     * @return the notificationUsers set with any users that are not a member
     *          of the group removed
     */
    private Set<User> clearUnassociatedUsers(Set<User> notificationUsers, Integer groupId) {
        List<User> groupTechs = userRoleGroupService.getTechnicianByGroupId(groupId);

        for (Iterator<User> iterator = notificationUsers.iterator(); iterator.hasNext();) {
            User user = iterator.next();
            if (!groupTechs.contains(user)) {
                iterator.remove();
            }
        }

        return notificationUsers;
    }

    public void sendSurveyNotificationEmail(Survey survey, Ticket ticket) throws Exception {
        if (isMailConfigured()) {

            // get message source accessor
            MessageSourceAccessor accessor = getMessageSourceAccessor();

            // Get default locale
            Locale locale = LocaleContextHolder.getLocale();

            MimeMessage mime = new MimeMessage(mailSender.getSession());

            Multipart mimeMp = new MimeMultipart("alternative");

            if (ticket.getContact().getId().equals(User.ANONYMOUS_ID) &&
                    StringUtils.isNotBlank(ticket.getAnonymousEmailAddress())) {
                mime.addRecipient(MimeMessage.RecipientType.TO, new InternetAddress(ticket.getAnonymousEmailAddress()));
            } else {
                mime.addRecipient(MimeMessage.RecipientType.TO, new InternetAddress(ticket.getContact().getEmail()));
            }

            Address[] fromAddress = new Address[1];
            fromAddress[0] = new InternetAddress(getMailConfig().getEmailFromAddress());
            mime.addFrom(fromAddress);

            mime.setSubject(accessor.getMessage("surveys.notification.subject", new Object[]{ticket.getId()}, locale), "UTF-8");

            // Build the URL for the survey form
            String base = propertiesService.getPropertiesValueByName(PropertiesConstants.BASE_URL);
            StringBuilder surveyUrl = new StringBuilder();
            surveyUrl.append(base);
            surveyUrl.append("/surveys/default.glml");
            surveyUrl.append("?stid=").append(ticket.getId()).append("&ssid=").append(survey.getId());

            // Build the message body of the mail
            StringBuilder txt = new StringBuilder();
            txt.append(getMessageSourceAccessor().getMessage("surveys.notification.message.text", new Object[]{surveyUrl}, locale));

            // add the text body part
            BodyPart bpText = new MimeBodyPart();
            bpText.setContent(txt.toString(), "text/plain;charset=UTF-8");
            mimeMp.addBodyPart(bpText);

            // build the html message
            StringBuilder msg = new StringBuilder();
            msg.append(getMessageSourceAccessor().getMessage("surveys.notification.message.html", new Object[]{ticket.getId(), surveyUrl, ticket.getSubject(), ticket.getNote() == null ? "" : ticket.getNote()}, locale));

            // add the html body part
            BodyPart bpHtml = new MimeBodyPart();
            bpHtml.setDataHandler(new DataHandler(new ByteArrayDataSource(msg.toString(), "text/html;charset=UTF-8")));
            mimeMp.addBodyPart(bpHtml);

            mime.setContent(mimeMp);

            sendMail(mime);
        }        
    }

    public void sendSurveyManagementNotificationEmail(Survey survey, Ticket ticket) throws Exception {
        if (isMailConfigured()) {

            // Get default locale
            Locale locale = LocaleContextHolder.getLocale();

            MimeMessage mime = new MimeMessage(mailSender.getSession());

            Multipart mimeMp = new MimeMultipart("alternative");

            User assignedToUser = ticket.getAssignedTo().getUserRole().getUser();
            if (assignedToUser != null) {
                String assignedToUserEmail = assignedToUser.getEmail();
                if (assignedToUserEmail != null)
                    mime.addRecipient(MimeMessage.RecipientType.TO, new InternetAddress(assignedToUserEmail));
                else
                    throw new Exception(" AssignedToUser (" + assignedToUser + ") does not have e-mail properly configured.");
            } else {
                throw new Exception("  No user assigned to this ticket.");
            }

            // Add the group managers to the notification e-mail
            List urgList = ticket.getGroup().getUserRoleGroupList();
            for (Object anUrgList : urgList) {
                UserRoleGroup urg = (UserRoleGroup) anUrgList;
                UserRole ur = urg.getUserRole();
                if (ur != null) {
                    Role role = ur.getRole();
                    if (role != null) {
                        Integer urid = role.getId();
                        if (urid == 2) {
                            mime.addRecipient(MimeMessage.RecipientType.TO, new InternetAddress(ur.getUser().getEmail()));
                        }
                    }
                }
            }

            Address[] fromAddress = new Address[1];
            fromAddress[0] = new InternetAddress(getMailConfig().getEmailFromAddress());
            mime.addFrom(fromAddress);

            mime.setSubject(getMessageSourceAccessor().getMessage("surveys.management.notification.subject", new Object[]{ticket.getId()}, locale), "UTF-8"); 

            String contactName;
            if (ticket.getContact().getId().equals(User.ANONYMOUS_ID) &&
                    StringUtils.isNotBlank(ticket.getAnonymousEmailAddress())) {
                contactName = "Anonymous (" + ticket.getAnonymousEmailAddress() + ")";
            } else {
                contactName = ticket.getContact().getFirstName() + " " + ticket.getContact().getLastName();
            }

            // Build the text message body of the mail
            StringBuilder txt = new StringBuilder();
            txt.append(getMessageSourceAccessor().getMessage("surveys.management.notification.message.text", new Object[]{ticket.getId(), contactName}, locale));

            BodyPart bpText = new MimeBodyPart();
            bpText.setContent(txt.toString(), "text/plain");
            mimeMp.addBodyPart(bpText);

            // build the html message
            StringBuilder msg = new StringBuilder();
            msg.append(getMessageSourceAccessor().getMessage("surveys.management.notification.message.html", new Object[]{ticket.getId(), contactName}, locale));

            BodyPart bpHtml = new MimeBodyPart();
            bpHtml.setDataHandler(new DataHandler(new ByteArrayDataSource(msg.toString(), "text/html;charset=UTF-8")));
            mimeMp.addBodyPart(bpHtml);

            mime.setContent(mimeMp);

            sendMail(mime);
        }
    }

    public boolean isMailConfigured() {
        return mailConfigured;
    }

    public void setMailSender(JavaMailSenderImpl mailSender) {
        this.mailSender = mailSender;
    }

    public void setPropertiesService(PropertiesService propertiesService) {
        this.propertiesService = propertiesService;
    }

    public void setMailConfigured(boolean mailConfigured) {
        this.mailConfigured = mailConfigured;
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setEmailToTicketConfigService(EmailToTicketConfigService emailToTicketConfigService) {
        this.emailToTicketConfigService = emailToTicketConfigService;
    }
}
