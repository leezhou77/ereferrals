package net.grouplink.ehelpdesk.service.acl;

import com.google.gson.Gson;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.acl.*;
import net.grouplink.ehelpdesk.domain.acl.PermissionActors;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.RoleService;
import net.grouplink.ehelpdesk.service.acl.dataobj.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class PermissionInitializerServiceImpl implements PermissionInitializerService {
    private PermissionService permissionService;
    private RoleService roleService;
    private GroupService groupService;

    public void init() {
        // load permission subjects
        initPermissionSubjects();
        
        // load permissions
        List<Permission> additionalPermissions = initPermissions();

        // load global permission entries
        initGlobalPermEntries();

        // load perm scheme
        initPermSchemes();

        initAdditionalPermissions(additionalPermissions);
    }

    private void initAdditionalPermissions(List<Permission> additionalPermissions) {
        for (Permission permission : additionalPermissions) {
            if (permission.getPermissionType().equals(Permission.PERMISSIONTYPE_GLOBAL)) {
                initAdditionalGlobalPerm(permission);
            } else if (permission.getPermissionType().equals(Permission.PERMISSIONTYPE_SCHEME) ||
                    permission.getPermissionType().equals(Permission.PERMISSIONTYPE_FIELD)) {
                initAdditionalGroupPerm(permission);
            }
        }
    }

    private void initPermissionSubjects() {
        boolean subjectsExist = permissionService.getPermissionSubjects().size() > 0;

        PermissionSubjectDataObj permSubjectDataObj = parsePermissionSubjects();
        for(PermissionSubject permissionSubject : permSubjectDataObj.getPermissionSubjects()) {
            boolean createSubject = true;
            if(subjectsExist) {
                PermissionSubject subject = permissionService.getPermissionSubjectByModelClass(permissionSubject.getModelClass());
                if(subject != null) {
                    createSubject = false;
                }
            }
            
            if(createSubject) {
                permissionService.savePermissionSubject(permissionSubject);
            }
        }
        
        for(FieldPermissionSubjectDataObj dataObj : permSubjectDataObj.getFieldPermissionSubjects()) {
            for(String fieldName : dataObj.getFields()) {
                boolean createSubject = true;
                if(subjectsExist) {
                    PermissionSubject subject = permissionService.getPermissionSubjectByModelClassAndField(dataObj.getModelClass(), fieldName);
                    if(subject != null) {
                        createSubject = false;
                    }
                }

                if(createSubject) {
                    PermissionSubject subject = new PermissionSubject();
                    subject.setModelClass(dataObj.getModelClass());
                    subject.setModelName(dataObj.getModelName());
                    subject.setModelField(fieldName);
                    permissionService.savePermissionSubject(subject);
                }
            }
        }
    }

    private List<Permission> initPermissions() {
        PermissionDataObj permDataObj = parsePermissions();

        // check if any permissions exist in the database
        boolean permsExist = permissionService.getGlobalPermissions().size() > 0;

        List<Permission> additionalPermissions = new ArrayList<Permission>();

        // global perms
        for (Permission permission : permDataObj.getGlobalPermissions()) {
            boolean createPerm = true;
            if (permsExist) {
                Permission p = permissionService.getPermissionByKey(permission.getKey());
                if (p != null) {
                    createPerm = false;
                } else {
                    // adding a new global permission to existing permissions
                    additionalPermissions.add(permission);
                }
            }

            if (createPerm) {
                permission.setPermissionType(Permission.PERMISSIONTYPE_GLOBAL);
                permissionService.savePermission(permission);
            }
        }

        // scheme perms
        for (Permission permission : permDataObj.getGroupPermissions()) {
            boolean createPerm = true;
            if (permsExist) {
                Permission p = permissionService.getPermissionByKey(permission.getKey());
                if (p != null) {
                    createPerm = false;
                } else {
                    // adding a new group permission to existing permissions
                    additionalPermissions.add(permission);
                }
            }

            if (createPerm) {
                permission.setPermissionType(Permission.PERMISSIONTYPE_SCHEME);
                permissionService.savePermission(permission);
            }
        }

        // field perms
        for (Permission permission : permDataObj.getFieldPermissions()) {
            boolean createPerm = true;
            if (permsExist) {
                Permission p = permissionService.getPermissionByKey(permission.getKey());
                if (p != null) {
                    createPerm = false;
                } else {
                    // adding a new field permission to existing permissions
                    additionalPermissions.add(permission);
                }
            }

            if (createPerm) {
                permission.setPermissionType(Permission.PERMISSIONTYPE_FIELD);
                String modelName = permission.getKey().split("\\.")[0];
                String field = permission.getKey().split("\\.")[2];
                permission.setPermissionSubject(permissionService.getPermissionSubjectByModelNameAndField(modelName, field));
                permissionService.savePermission(permission);
            }
        }

        return additionalPermissions;
    }

    private void initGlobalPermEntries() {
        if (permissionService.getGlobalPermissionEntries().size() > 0) return;

        GlobalPermEntriesDataObj entriesDataObj = parseGlobalPermEntries();
        for (GlobalPermEntryDataObj entryDataObj : entriesDataObj.getGlobalPermissionEntries()) {
            GlobalPermissionEntry gpe = new GlobalPermissionEntry();
            gpe.setPermission(permissionService.getPermissionByKey(entryDataObj.getKey()));
            PermissionActors pc = new PermissionActors();
            for (String s : entryDataObj.getRoles()) {
                pc.addRole(roleService.getRoleByName(s));
            }

            gpe.setPermissionActors(pc);

            permissionService.saveGlobalPermissionEntry(gpe);
        }

    }

    private void initAdditionalGlobalPerm(Permission permission) {
        // check if a default global perm entry is defined for this permission
        GlobalPermEntriesDataObj entriesDataObj = parseGlobalPermEntries();
        for (GlobalPermEntryDataObj entryDataObj : entriesDataObj.getGlobalPermissionEntries()) {
            if (entryDataObj.getKey().equals(permission.getKey())) {
                // found match, create new global perm entry
                GlobalPermissionEntry gpe = new GlobalPermissionEntry();
                gpe.setPermission(permissionService.getPermissionByKey(entryDataObj.getKey()));
                PermissionActors pc = new PermissionActors();
                for (String role : entryDataObj.getRoles()) {
                    pc.addRole(roleService.getRoleByName(role));
                }

                gpe.setPermissionActors(pc);

                permissionService.saveGlobalPermissionEntry(gpe);
            }
        }
    }

    private void initPermSchemes() {
        if (permissionService.getPermissionSchemes().size() > 0) return;

        PermSchemesDataObj permSchemesDataObj = parsePermissionSchemes();
        for (PermSchemeDataObj schemeDataObj : permSchemesDataObj.getPermissionSchemes()) {
            PermissionScheme permissionScheme = createPermissionSchemeFromDataobj(schemeDataObj);

            permissionService.savePermissionScheme(permissionScheme);

            // set all active groups to use this permission scheme
            List<Group> groups = groupService.getGroups();
            for (Group group : groups) {
                group.setPermissionScheme(permissionScheme);
                groupService.saveGroup(group);
            }
        }
    }

    private void initAdditionalGroupPerm(Permission permission) {
        // check if entry exists in default permission model for this new permission
        PermSchemesDataObj permSchemesDataObj = parsePermissionSchemes();
        for (PermSchemeDataObj schemeDataObj : permSchemesDataObj.getPermissionSchemes()) {
            for (PermSchemeEntryDataObj permSchemeEntryDataObj : schemeDataObj.getSchemeEntries()) {
                if (permSchemeEntryDataObj.getKey().equals(permission.getKey())) {
                    // found match, update all existing perm schemes with the a new entry
                    updateExistingPermissionSchemes(permission, permSchemeEntryDataObj);
                }
            }
        }
    }

    private void updateExistingPermissionSchemes(Permission permission, PermSchemeEntryDataObj permSchemeEntryDataObj) {
        List<PermissionScheme> permissionSchemeList = permissionService.getPermissionSchemes();
        for (PermissionScheme permissionScheme : permissionSchemeList) {
            PermissionSchemeEntry entry = new PermissionSchemeEntry();
            Permission p = permissionService.getPermissionByKey(permission.getKey());
            if (p == null) {
                throw new RuntimeException("permission not found for key " + permission.getKey());
            }

            entry.setPermission(p);
            PermissionActors pc = new PermissionActors();

            for (String roleName : permSchemeEntryDataObj.getRoles()) {
                pc.addRole(roleService.getRoleByName(roleName));
            }

            entry.setPermissionActors(pc);
            permissionScheme.addPermissionSchemeEntry(entry);
            permissionService.savePermissionScheme(permissionScheme);
        }
    }

    public void loadDefaultPermissionScheme(PermissionScheme permissionScheme) {
        PermSchemesDataObj permSchemesDataObj = parsePermissionSchemes();
        PermSchemeDataObj permSchemeDataObj = null;
        for (PermSchemeDataObj dataObj : permSchemesDataObj.getPermissionSchemes()) {
            if (dataObj.getName().equals("Default Permission Model")) {
                permSchemeDataObj = dataObj;
            }
        }

        if (permSchemeDataObj != null) {
            PermissionScheme defaultPermScheme = createPermissionSchemeFromDataobj(permSchemeDataObj);
            for (PermissionSchemeEntry entry : defaultPermScheme.getPermissionSchemeEntries()) {
                permissionScheme.addPermissionSchemeEntry(entry);
            }
        }
    }

    private PermissionScheme createPermissionSchemeFromDataobj(PermSchemeDataObj schemeDataObj) {
        PermissionScheme permissionScheme = new PermissionScheme();
        permissionScheme.setName(schemeDataObj.getName());


        // perm scheme entries
        for (PermSchemeEntryDataObj entryDataObj : schemeDataObj.getSchemeEntries()) {
            PermissionSchemeEntry entry = new PermissionSchemeEntry();
            entry.setPermissionScheme(permissionScheme);
            Permission p = permissionService.getPermissionByKey(entryDataObj.getKey());
            if (p == null) {
                throw new RuntimeException("permission not found for key " + entryDataObj.getKey());
            }
            
            entry.setPermission(p);
            PermissionActors pc = new PermissionActors();

            for (String roleName : entryDataObj.getRoles()) {
                pc.addRole(roleService.getRoleByName(roleName));
            }

            entry.setPermissionActors(pc);
            permissionScheme.addPermissionSchemeEntry(entry);
        }
        return permissionScheme;
    }

    public PermissionSubjectDataObj parsePermissionSubjects() {
        BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getClassLoader()
                .getResourceAsStream("net/grouplink/ehelpdesk/service/acl/permSubjects.json")));

        Gson gson = new Gson();
        return gson.fromJson(br, PermissionSubjectDataObj.class);
    }
    
    public PermissionDataObj parsePermissions() {
        BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getClassLoader()
                .getResourceAsStream("net/grouplink/ehelpdesk/service/acl/permissions.json")));

        Gson gson = new Gson();
        return gson.fromJson(br, PermissionDataObj.class);
    }

    public GlobalPermEntriesDataObj parseGlobalPermEntries() {
        BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getClassLoader()
                .getResourceAsStream("net/grouplink/ehelpdesk/service/acl/globalPermEntries.json")));

        Gson gson = new Gson();
        return gson.fromJson(br, GlobalPermEntriesDataObj.class);
    }

    public PermSchemesDataObj parsePermissionSchemes() {
        BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getClassLoader()
                .getResourceAsStream("net/grouplink/ehelpdesk/service/acl/defaultPermScheme.json")));


        Gson gson = new Gson();
        return gson.fromJson(br, PermSchemesDataObj.class);
    }

    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }
}
