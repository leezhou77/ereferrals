package net.grouplink.ehelpdesk.service;

import java.util.List;
import java.util.Date;
import java.util.Map;

import net.grouplink.ehelpdesk.dao.KnowledgeBaseDao;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.KnowledgeBase;
import net.grouplink.ehelpdesk.domain.Attachment;
import net.grouplink.ehelpdesk.domain.User;


public class KnowledgeBaseServiceImpl implements KnowledgeBaseService {

    private KnowledgeBaseDao knowledgeBaseDao;
    private UserService userService;

    public KnowledgeBase getKnowledgeBaseById(Integer id) {
	    return this.knowledgeBaseDao.getKnowledgeBaseById(id);
    }

    public List<KnowledgeBase> searchKnowLedgeBase(String searchText, Integer type, Integer groupId, User user) {
        if (user != null) {
            user = userService.getUserById(user.getId());
        }
        
	    return this.knowledgeBaseDao.searchKnowLedgeBase(searchText, type, groupId, user);
    }

    public List<KnowledgeBase> searchKnowLedgeBaseByGroupId(String searchText, Integer type, Integer groupId, User user) {
        if (user != null) {
            user = userService.getUserById(user.getId());
        }
        
	    return this.knowledgeBaseDao.searchKnowLedgeBaseByGroupId(searchText, type, groupId, user);
    }

    public List<KnowledgeBase> searchKnowledgeBaseByUser(String searchText, int start, int count, Map<String, Boolean> sortFieldsOrder, User user) {
        if (user != null) {
            user = userService.getUserById(user.getId());
        }
        return knowledgeBaseDao.searchKnowledgeBaseByUser(searchText, start, count, sortFieldsOrder, user);
    }

    public int getKbArticleCountByUser(String searchText, User user) {
        if (user != null) {
            user = userService.getUserById(user.getId());
        }
        return knowledgeBaseDao.getKbArticleCountByUser(searchText, user);
    }
    
    public void setKnowledgeBaseDao(KnowledgeBaseDao knowledgeBaseDao) {
        this.knowledgeBaseDao = knowledgeBaseDao;
    }

    public void deleteKnowledgeBase(KnowledgeBase knowledgeBase) {
	    this.knowledgeBaseDao.deleteKnowledgeBase(knowledgeBase);
    }

    public void deleteKnowledgeBaseAttachments(Attachment attach) {
        this.knowledgeBaseDao.deleteKnowledgeBaseAttachments(attach);
    }

    public void flush() {
        this.knowledgeBaseDao.flush();
    }

    public void clear() {
        this.knowledgeBaseDao.clear();
    }

    public Attachment getNonPrivateAttachment(Integer attachmentId) {
        return this.knowledgeBaseDao.getNonPrivateAttachment(attachmentId);
    }

    public int getCountByGroup(Group group) {
        return knowledgeBaseDao.getCountByGroup(group);
    }

    public void saveKnowledgeBase(KnowledgeBase knowledgeBase) {
        knowledgeBase.setDate(new Date());
        this.knowledgeBaseDao.saveKnowledgeBase(knowledgeBase);
    }

    public void saveKnowledgeBaseMigration(KnowledgeBase knowledgeBase) {
        this.knowledgeBaseDao.saveKnowledgeBase(knowledgeBase);
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
