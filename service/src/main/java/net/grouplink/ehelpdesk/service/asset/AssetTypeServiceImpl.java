package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.dao.asset.AssetTypeDao;
import net.grouplink.ehelpdesk.domain.asset.AssetType;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Jul 17, 2008
 * Time: 1:36:19 PM
 */
public class AssetTypeServiceImpl implements AssetTypeService {
    private AssetTypeDao assetTypeDao;
    private AssetService assetService;

    public AssetType getById(Integer id) {
        return assetTypeDao.getById(id);
    }

    public AssetType getByName(String name) {
        return assetTypeDao.getByName(name);
    }

    public List<AssetType> getAllAssetTypes() {
        return assetTypeDao.getAllAssetTypes();
    }

    public List<AssetType> getUndeletableAssetTypes() {
        return assetService.getUniqueTypes();
    }

    public void saveAssetType(AssetType assetType) {
        assetTypeDao.saveAssetType(assetType);
    }

    public void deleteAssetType(AssetType assetType) {
        assetTypeDao.deleteAssetType(assetType);
    }

    public void flush() {
        assetTypeDao.flush();
    }

    public void setAssetTypeDao(AssetTypeDao assetTypeDao) {
        this.assetTypeDao = assetTypeDao;
    }

    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }
}
