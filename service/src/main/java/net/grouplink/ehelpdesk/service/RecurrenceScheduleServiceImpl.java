package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.RecurrenceScheduleDao;
import net.grouplink.ehelpdesk.domain.RecurrenceSchedule;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.LocalDate;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class RecurrenceScheduleServiceImpl implements RecurrenceScheduleService {
    private static final long MILLIS_PER_DAY = DateUtils.MILLIS_PER_DAY;
    private static final long MILLIS_PER_WEEK = MILLIS_PER_DAY * 7;

    private Log log = LogFactory.getLog(getClass());

    private RecurrenceScheduleDao recurrenceScheduleDao;

    public RecurrenceSchedule getById(Integer id) {
        return recurrenceScheduleDao.getById(id);
    }

    public void save(RecurrenceSchedule recurrenceSchedule) {
        recurrenceScheduleDao.save(recurrenceSchedule);
    }

    public boolean meetsScheduleCriteria(RecurrenceSchedule schedule, Date processDate, Date lastActionDate, int occurrenceCount) {
        log.debug("called meetsScheduleCriteria() params: schedule=" + schedule.getId() + ", processDate=" + processDate + ", lastActionDate=" + lastActionDate + ", occurrenceCount=" + occurrenceCount);
        // check range criteria
        if (!meetsRangeCriteria(schedule, processDate, occurrenceCount)) {
            log.debug("range criteria not met");
            return false;
        }

        // truncate the date to the day for comparing the processDate and lastActionDate
        if (processDate != null) {
            processDate = DateUtils.truncate(processDate, Calendar.DAY_OF_MONTH);
            log.debug("processDate not null, truncated to: " + processDate);
        }

        if (lastActionDate != null) {
            lastActionDate = DateUtils.truncate(lastActionDate, Calendar.DAY_OF_MONTH);
            log.debug("lastActionDate not null, truncated to: " + lastActionDate);
        }

        boolean processSchedule = false;

        switch (schedule.getPattern()) {
            case RecurrenceSchedule.PATTERN_DAILY:
                log.debug("schedule.pattern=DAILY");
                processSchedule = meetsDailyCriteria(schedule, processDate, lastActionDate);
                break;
            case RecurrenceSchedule.PATTERN_WEEKLY:
                log.debug("schedule.pattern=WEEKLY");
                processSchedule = meetsWeeklyCriteria(schedule, processDate, lastActionDate);
                break;
            case RecurrenceSchedule.PATTERN_MONTHLY:
                log.debug("schedule.pattern=MONTHLY");
                processSchedule = meetsMonthlyCriteria(schedule, processDate, lastActionDate);
                break;
            case RecurrenceSchedule.PATTERN_YEARLY:
                log.debug("schedule.pattern=YEARLY");
                processSchedule = meetsYearlyCriteria(schedule, processDate, lastActionDate);
                break;
        }

        log.debug("processSchedule=" + processSchedule);             

        return processSchedule;
    }

    public List<RecurrenceSchedule> getAllRunning() {
        return recurrenceScheduleDao.getAllRunning();
    }

    public List<RecurrenceSchedule> getRunningTicketTemplateSchedules() {
        return recurrenceScheduleDao.getRunningTicketTemplateSchedules();
    }

    public List<RecurrenceSchedule> getRunningScheduleStatusSchedules() {
        return recurrenceScheduleDao.getRunningScheduleStatusSchedules();
    }

    public List<RecurrenceSchedule> getRunningReportSchedules() {
        return recurrenceScheduleDao.getRunningReportSchedules();
    }

    public void delete(RecurrenceSchedule recurrenceSchedule) {
        recurrenceScheduleDao.delete(recurrenceSchedule);
    }

    private boolean meetsRangeCriteria(RecurrenceSchedule schedule, Date currentDate, int occurrenceCount) {
        log.debug("called meetsRangeCriteria, params: scehdule=" + schedule.getId() + "currentDate=" + currentDate + ", occurrenceCount=" + occurrenceCount);
        if (schedule.getRangeStartDate() != null &&
                currentDate.before(schedule.getRangeStartDate())) {
            log.debug("range start date is before currentDate, returning false");
            return false;
        }

        if (schedule.getRangeEnd() != null) {
            switch (schedule.getRangeEnd()) {
                case RecurrenceSchedule.RANGE_END_AFTER:
                    log.debug("schedule.rangeEnd=end after occurenceCount " + schedule.getRangeEndAfterOccurence());
                    // end after x occurrences
                    if (occurrenceCount >= schedule.getRangeEndAfterOccurence()) {
                        log.debug("occurrenceCount >= schedule.rangeEndAfterOccurence, returning false");
                        return false;
                    }

                    break;
                case RecurrenceSchedule.RANGE_END_BY:
                    log.debug("schedule.rangeEnd=end after date " + schedule.getRangeEndByDate());
                    if (currentDate.after(schedule.getRangeEndByDate())) {
                        log.debug("currentDate after schedule.rangeEndByDate, returning false");
                        return false;
                    }
                
                    break;
            }
        }

        return true;
    }

    private boolean meetsDailyCriteria(RecurrenceSchedule schedule, Date currentDate, Date lastAction) {
        log.debug("called meetsDailyCriteria, params: schedule=" + schedule.getId() + ", currentDate=" + currentDate + ", lastAction=" + lastAction);
        boolean doAction = false;
        if (lastAction != null) {
            Integer dailyRepeatInterval = schedule.getDailyInterval();
            // if the current time minus the last action time is greater than the repeat interval, do action
            long delta = currentDate.getTime() - lastAction.getTime();
            log.debug("lastActionDate not null, dailyRepeatInterval=" + dailyRepeatInterval + ", delta=" + delta + ", dailyRepeatInterval * MILLIS_PER_DAY=" + (dailyRepeatInterval * MILLIS_PER_DAY));
            if (delta >= dailyRepeatInterval * MILLIS_PER_DAY){
                log.debug("delta is ge required interval, doAction=true");
                doAction = true;
            }
        } else {
            log.debug("lastActionDate is null, doAction=true");
            doAction = true;
        }

        log.debug("doAction=" + doAction);
        return doAction;
    }

    private boolean meetsWeeklyCriteria(RecurrenceSchedule schedule, Date currentDate, Date lastActionDate) {
        log.debug("called meetsWeeklyCriteria, params: scehdule=" + schedule.getId() + ", currentDate=" + currentDate + ", lastActionDate=" + lastActionDate);
        boolean doAction = false;
        long delta;

        Calendar lastActionCal = Calendar.getInstance();
        if (lastActionDate != null) {
            log.debug("lastActionDate is not null");
            lastActionCal.setTime(lastActionDate);
            lastActionCal.set(Calendar.DAY_OF_WEEK, lastActionCal.getFirstDayOfWeek());
            log.debug("lastActionCal.time=" + lastActionCal.getTime());
            Integer weeklyRepeatInterval = schedule.getWeeklyInterval();
            log.debug("schedule.weeklyInterval=" + weeklyRepeatInterval);
            // if the current time minus the last action time is greater than the repeat interval, do action
            delta = currentDate.getTime() - lastActionCal.getTime().getTime();
            log.debug("delta=" + delta);
            if (delta >= weeklyRepeatInterval * MILLIS_PER_WEEK) {
                log.debug("delta ge " + (weeklyRepeatInterval * MILLIS_PER_WEEK) + ", setting doAction=true");
                doAction = true;
            } else {
                log.debug("delta lt (" + weeklyRepeatInterval + " * " + MILLIS_PER_WEEK + ")");
            }
        } else {
            log.debug("lastActionDate is null, setting doAction=true");
            doAction = true;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);

        boolean sameWeek = false;
        if (lastActionDate != null) {
            log.debug("currentDateCal week of year=" + cal.get(Calendar.WEEK_OF_YEAR) + ", lastActionCal week of year=" + lastActionCal.get(Calendar.WEEK_OF_YEAR));
            if (cal.get(Calendar.WEEK_OF_YEAR) == lastActionCal.get(Calendar.WEEK_OF_YEAR)) {
                log.debug("currentDate and lastActionDate are the same week, setting sameWeek=true and doAction=true");
                sameWeek = true;
                doAction = true;
            }
        }

        log.debug("doAction=" + doAction + ", sameWeek=" + sameWeek);
        boolean dayOfWeekMatch = false;
        if (doAction || sameWeek) {
            int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
            switch (dayOfWeek) {
                case Calendar.SUNDAY:
                    log.debug("day of week is Sunday");
                    if (schedule.getOnSunday()) {
                        log.debug("schedule.onSunday is true, setting dayOfWeekMatch=true");
                        dayOfWeekMatch = true;
                    }
                    break;
                case Calendar.MONDAY:
                    log.debug("day of week is Monday");
                    if (schedule.getOnMonday()) {
                        log.debug("schedule.onMonday is true, setting dayOfWeekMatch=true");
                        dayOfWeekMatch = true;
                    }
                    break;
                case Calendar.TUESDAY:
                    log.debug("day of week is Tuesday");
                    if (schedule.getOnTuesday()) {
                        log.debug("schedule.onTuesday is true, setting dayOfWeekMatch=true");
                        dayOfWeekMatch = true;
                    }
                    break;
                case Calendar.WEDNESDAY:
                    log.debug("day of week is Wednesday");
                    if (schedule.getOnWednesday()) {
                        log.debug("schedule.onWednesday is true, setting dayOfWeekMatch=true");
                        dayOfWeekMatch = true;
                    }
                    break;
                case Calendar.THURSDAY:
                    log.debug("day of week is Thursday");
                    if (schedule.getOnThursday()) {
                        log.debug("schedule.onThursday is true, setting dayOfWeekMatch=true");
                        dayOfWeekMatch = true;
                    }
                    break;
                case Calendar.FRIDAY:
                    log.debug("day of week is Friday");
                    if (schedule.getOnFriday()) {
                        log.debug("schedule.onFriday is true, setting dayOfWeekMatch=true");
                        dayOfWeekMatch = true;
                    }
                    break;
                case Calendar.SATURDAY:
                    log.debug("day of week is Saturday");
                    if (schedule.getOnSaturday()) {
                        log.debug("schedule.onSaturday is true, setting dayOfWeekMatch=true");
                        dayOfWeekMatch = true;
                    }
                    break;

            }
        }

        return (doAction && dayOfWeekMatch);
    }

    private boolean meetsMonthlyCriteria(RecurrenceSchedule schedule, Date currentDate, Date lastAction) {
        log.debug("called meetsMonthlyCriteria, params: schedule=" + schedule.getId() + ", currentDate=" + currentDate + ", lastAction=" + lastAction);
        boolean doAction = false;
        switch (schedule.getMonthlySchedule()) {
            case RecurrenceSchedule.MONTHLY_DAY_OF_MONTH:
                log.debug("schedule is monthly - day of month");
                Date domIntervalDate = null;
                if (lastAction != null) {
                    domIntervalDate = DateUtils.addMonths(lastAction, schedule.getMonthlyDayOfMonthOccurrence());
                    log.debug("lastAction date is not null, domIntervalDate=" + domIntervalDate);
                }

                log.debug("domIntervalDate=" + domIntervalDate + ", currentDate=" + currentDate);
                if (domIntervalDate == null ||
                        currentDate.equals(domIntervalDate) ||
                        currentDate.after(domIntervalDate)) {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(currentDate);

                    int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
                    log.debug("currentDate dayOfMonth=" + dayOfMonth);
                    if (dayOfMonth == schedule.getMonthlyDayOfMonth()) {
                        log.debug("day of month matches schedule.monthlydayofmonth, doAction=true");
                        doAction = true;
                    }
                }

                break;
            case RecurrenceSchedule.MONTHLY_DAY_OF_WEEK:
                log.debug("schedule is monthly - day of week");
                Date dowIntervalDate = null;
                if (lastAction != null) {
                    // set date to first of month, to avoid problem with the interval being on different dates in different months
                    dowIntervalDate = DateUtils.setDays(lastAction, 1);
                    dowIntervalDate = DateUtils.addMonths(dowIntervalDate, schedule.getMonthlyDayOfWeekOccurrence());
                    log.debug("lastAction date is not null, setting dowIntervalDate=" + dowIntervalDate);
                }

                log.debug("dowIntervalDate=" + dowIntervalDate + ", currentDate=" + currentDate);
                if (dowIntervalDate == null ||
                        currentDate.equals(dowIntervalDate) ||
                        currentDate.after(dowIntervalDate)) {

                    Calendar currentDateCal = Calendar.getInstance();
                    currentDateCal.setTime(currentDate);
                    int currentMonth = currentDateCal.get(Calendar.MONTH) + 1;
                    // schedule.getMonthlyDayOfWeekDay() holds values based on java.util.Calendar i.e. Sun=1, Sat=7
                    // Joda wants Mon=1, Sun=7
                    int dayOfWeek = convertCalendarDayOfWeekToJoda(schedule.getMonthlyDayOfWeekDay());
                    int currentYear = currentDateCal.get(Calendar.YEAR);

                    Date schedDate = getNDayOfMonth(dayOfWeek, schedule.getMonthlyDayOfWeekInterval(), currentMonth, currentYear);
                    log.debug("must match schedule date=" + schedDate);
                    
                    if (DateUtils.truncate(currentDate, Calendar.DAY_OF_MONTH).equals(DateUtils.truncate(schedDate, Calendar.DAY_OF_MONTH))) {
                        log.debug("currentDate equals schedule date, setting doAction=true");
                        doAction = true;
                    }
                }

                break;
        }

        return doAction;
    }

    private int convertCalendarDayOfWeekToJoda(Integer calendarDayOfWeek) {
        int converted;
        if (calendarDayOfWeek > 1)
            converted = calendarDayOfWeek - 1;
        else
            converted = 7;

        return converted;
    }

    /**
     * Gets the nth day of the month. For example, gets the second Tuesday in April
     * @param dayweek Monday=1, Sun=7
     * @param nthweek nth occurrence of the given day
     * @param month Jan=1
     * @param year the year
     * @return the date
     */
    private Date getNDayOfMonth(int dayweek, int nthweek, int month, int year) {
        LocalDate d = new LocalDate(year, month, 1).withDayOfWeek(dayweek);
        if (d.getMonthOfYear() != month)
            d = d.plusWeeks(1);

        d = d.plusWeeks(nthweek-1);
        return d.toDate();
    }

    private boolean meetsYearlyCriteria(RecurrenceSchedule schedule, Date currentDate, Date lastAction) {
        log.debug("called meetsYearlyCriteria, params: schedule=" + schedule + ", currentDate=" + currentDate + ", lastAction=" + lastAction);
        boolean doAction = false;
        Date doyIntervalDate = null;
        if (lastAction != null) {
            doyIntervalDate = DateUtils.addYears(lastAction, 1);
            log.debug("lastAction is not null, setting doyIntervalDate=" + doyIntervalDate);
        }

        switch (schedule.getYearlySchedule()) {
            case RecurrenceSchedule.YEARLY_DAY_OF_YEAR:
                log.debug("schedule is yearly - day of year");
                if (doyIntervalDate == null ||
                        currentDate.equals(doyIntervalDate) ||
                        currentDate.after(doyIntervalDate)) {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(currentDate);

                    int currentMonth = cal.get(Calendar.MONTH);
                    int currentDayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
                    log.debug("currentMonth=" + currentMonth + ", currentDayOfMonth=" + currentDayOfMonth);
                    log.debug("schedule.yearlyDayOfYearMonth=" + schedule.getYearlyDayOfYearMonth() + ", schedule.yearlyDayOfYearDay=" + schedule.getYearlyDayOfYearDay());
                    if (currentMonth == schedule.getYearlyDayOfYearMonth() - 1
                            && currentDayOfMonth == schedule.getYearlyDayOfYearDay()) {
                        log.debug("currentMonth eq schedule month and currentDayOfMonth eq schedule day, setting doAction=true");
                        doAction = true;
                    }
                }

                break;
            case RecurrenceSchedule.YEARLY_DAY_OF_MONTH:
                log.debug("schedule is yearly - day of month");
                if (doyIntervalDate == null ||
                        currentDate.equals(doyIntervalDate) ||
                        currentDate.after(doyIntervalDate)) {

                    Calendar cal = Calendar.getInstance();
                    cal.setTime(currentDate);

                    log.debug("schedule.getYearlyDayOfMonthMonth=" + schedule.getYearlyDayOfMonthMonth() + ", schedule.yearlyDayOfMonthDay=" + schedule.getYearlyDayOfMonthDay() + ", schedule.yearlyDayOfMonthInterval=" + schedule.getYearlyDayOfMonthInterval());
                    if (cal.get(Calendar.MONTH) == schedule.getYearlyDayOfMonthMonth() - 1) {
                        log.debug("calendar month eq schedule.yearlyDayOfMonthMonth");
                        cal.set(Calendar.DAY_OF_MONTH, 1);
                        cal.set(Calendar.DAY_OF_WEEK, schedule.getYearlyDayOfMonthDay());
                        cal.set(Calendar.WEEK_OF_MONTH, schedule.getYearlyDayOfMonthInterval());

                        log.debug("cal time=" + cal.getTime());
                        if (DateUtils.truncate(currentDate, Calendar.DAY_OF_MONTH).equals(DateUtils.truncate(cal.getTime(), Calendar.DAY_OF_MONTH))) {
                            log.debug("currentDate eq cal date, setting doAction=true");
                            doAction = true;
                        }
                    }
                }

                break;
        }

        return doAction;
    }

    public void setRecurrenceScheduleDao(RecurrenceScheduleDao recurrenceScheduleDao) {
        this.recurrenceScheduleDao = recurrenceScheduleDao;
    }
}
