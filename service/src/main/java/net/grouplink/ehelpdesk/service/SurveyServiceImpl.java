package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.SurveyDao;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Survey;
import net.grouplink.ehelpdesk.domain.SurveyQuestion;
import net.grouplink.ehelpdesk.domain.Ticket;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Date: Feb 13, 2008
 * Time: 1:55:03 PM
 */
public class SurveyServiceImpl implements SurveyService{

    // Service members
    private CategoryOptionService categoryOptionService;
    private GroupService groupService;
    private MailService mailService;

    // DAO members
    private SurveyDao surveyDao;

    public Survey getSurveyByName(String name) {
        return surveyDao.getSurveyByName(name);
    }

    public Survey getSurveyById(int id) {
        return surveyDao.getSurveyById(id);
    }

    public Survey getSurveyById(Integer id) {
        return surveyDao.getSurveyById(id);
    }

    public List<Survey> getActiveSurveys() {
        return surveyDao.getActiveSurveys();
    }

    public List<Survey> getAllSurveys() {
        return surveyDao.getAllSurveys();
    }

    public boolean getDefaultSurveyIsActivatedForAllGroups() {
        List<Group> groupsList = groupService.getGroups();
        Iterator groupsListIterator = groupsList.iterator();
        boolean isActivated = true;
        while (groupsListIterator.hasNext()) {
            Group group = (Group) groupsListIterator.next();
            if (! getDefaultSurveyIsActivatedForGroup(group.getId().intValue()))
               return false;
        }
        
        return isActivated;
    }

    public boolean getDefaultSurveyIsActivatedForGroup(int groupId) {
        List<CategoryOption> categoryOptionsList = categoryOptionService.getCategoryOptionByGroupId(groupId);
        return getDefaultSurveyIsActivatedForCategory(categoryOptionsList);
    }

    public boolean getDefaultSurveyIsActivatedForCategory(int categoryId) {
        List<CategoryOption> categoryOptionsList = categoryOptionService.getCategoryOptionByCategoryId(categoryId);
        return getDefaultSurveyIsActivatedForCategory(categoryOptionsList);
    }

    private boolean getDefaultSurveyIsActivatedForCategory(List<CategoryOption> categoryOptionsList) {
        boolean isActivated = true;
        for (CategoryOption catOpt : categoryOptionsList) {
            Survey survey = catOpt.getSurvey();
            if (survey == null)
                return false;
            else {
                int sid = survey.getId();
                if (sid != 1)
                    return false;
            }
        }
        
        return isActivated;
    }

    public void activateSurveyForAllGroups(int surveyId) {
        Survey survey = getSurveyById(surveyId);
        List<CategoryOption> catOptsList = categoryOptionService.getCategoryOptions();
        survey.setActive(Boolean.TRUE);
        for (CategoryOption catOpt : catOptsList) {
            survey.getCategoryOptions().add(catOpt);
            saveSurvey(survey);

            catOpt.setSurvey(survey);
            categoryOptionService.saveCategoryOption(catOpt);
        }
    }

    public void deactivateSurveysForAllGroups() {
        List<Group> groups = groupService.getGroups();
        for (Group group : groups) {
            int gid = group.getId();
            deactivateSurveysForAllCatOptsInGroup(gid);
        }
    }

    public void activateSurveyForAllCatOptsInGroup(int surveyId, int groupId) {
        Survey survey = getSurveyById(surveyId);
        List<CategoryOption> catOptsList = categoryOptionService.getCategoryOptionByGroupId(groupId);
        Iterator it = catOptsList.iterator();
        survey.setActive(Boolean.TRUE);
        while (it.hasNext()) {
            CategoryOption catOpt = (CategoryOption) it.next();
            survey.getCategoryOptions().add(catOpt);
            saveSurvey(survey);

            catOpt.setSurvey(survey);
            categoryOptionService.saveCategoryOption(catOpt);
        }
    }

    public void deactivateSurveysForAllCatOptsInGroup(int groupId) {
        List<CategoryOption> catOpts = categoryOptionService.getCategoryOptionByGroupId(groupId);
        for (CategoryOption catOpt : catOpts) {
            deactivateSurveyForCategoryOption(catOpt.getId().intValue());
        }
    }

    public void activateSurveyForCategoryOption(int surveyId, int catOptId) {
        Survey survey = getSurveyById(surveyId);
        CategoryOption catOpt = categoryOptionService.getCategoryOptionById(catOptId);
        survey.getCategoryOptions().add(catOpt);
        saveSurvey(survey);

        catOpt.setSurvey(survey);
        categoryOptionService.saveCategoryOption(catOpt);
    }

    public void deactivateSurveyForCategoryOption(int catOptId) {
        CategoryOption catOpt = categoryOptionService.getCategoryOptionById(catOptId);
        Survey survey = catOpt.getSurvey();
        if (survey != null) {
            Survey _survey = new Survey();
            _survey.setActive(Boolean.TRUE);
            _survey.setName(survey.getName());
            _survey.setSurveyFrequency(survey.getSurveyFrequency());
            Set<SurveyQuestion> sqSet = new HashSet<SurveyQuestion>();
            for (SurveyQuestion surveyQuestion : survey.getSurveyQuestions()) {
                sqSet.add(surveyQuestion);
            }
            
            _survey.setSurveyQuestions(sqSet);
            saveSurvey(_survey);
            
            catOpt.setSurvey(null);
            categoryOptionService.saveCategoryOption(catOpt);
        }
    }

    public void activateSurveyForCategory(int surveyId, int catId) {
        Survey survey = getSurveyById(surveyId);
        survey.setActive(Boolean.TRUE);
        List<CategoryOption> catOptsList = categoryOptionService.getCategoryOptionByCategoryId(catId);
        for (CategoryOption catOpt : catOptsList) {
            survey.getCategoryOptions().add(catOpt);
            saveSurvey(survey);

            catOpt.setSurvey(survey);
            categoryOptionService.saveCategoryOption(catOpt);
        }
    }

    public void deactivateSurveysForAllOptsInCategory(int catId) {
        List<CategoryOption> catOpts = categoryOptionService.getCategoryOptionByCategoryId(catId);
        for (CategoryOption catOpt : catOpts) {
            deactivateSurveyForCategoryOption(catOpt.getId().intValue());
        }
    }

    public void sendSurveyNotificationEmail(Survey survey, Ticket ticket) throws Exception{
        mailService.sendSurveyNotificationEmail(survey, ticket);
    }

    public void sendSurveyManagementNotificationEmail(Survey survey, Ticket ticket) throws Exception {
        mailService.sendSurveyManagementNotificationEmail(survey, ticket);
    }

    public void saveSurvey(Survey survey) {
	    surveyDao.saveSurvey(survey);
    }

    public void deleteSurvey(Survey survey) {
        surveyDao.deleteSurvey(survey);
    }

    public boolean hasDuplicateSurveyName(Survey survey) {
        return surveyDao.hasDuplicateSurveyName(survey);
    }

    public void flush() {
        surveyDao.flush();
    }

    public int getCountByCategoryOption(CategoryOption categoryOption) {
        List<Survey> surveys = getAllSurveys();
        int count = 0;
        for (Survey survey : surveys) {
            Set<CategoryOption> categoryOptions = survey.getCategoryOptions();
            for (CategoryOption catOpt : categoryOptions) {
                if(catOpt.equals(catOpt)){
                    count++;
                }
            }
        }
        return count;
    }

    public void setCategoryOptionService(CategoryOptionService categoryOptionService) {
        this.categoryOptionService = categoryOptionService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }

    public void setSurveyDao(SurveyDao surveyDao) {
        this.surveyDao = surveyDao;
    }
}
