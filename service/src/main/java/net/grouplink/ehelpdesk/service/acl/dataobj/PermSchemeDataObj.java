package net.grouplink.ehelpdesk.service.acl.dataobj;

import java.util.ArrayList;
import java.util.List;

public class PermSchemeDataObj {
    private String name;
    private List<PermSchemeEntryDataObj> schemeEntries = new ArrayList<PermSchemeEntryDataObj>();
    private List<PermSchemeFieldDataObj> fieldPermissions = new ArrayList<PermSchemeFieldDataObj>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PermSchemeEntryDataObj> getSchemeEntries() {
        return schemeEntries;
    }

    public void setSchemeEntries(List<PermSchemeEntryDataObj> schemeEntries) {
        this.schemeEntries = schemeEntries;
    }

    public List<PermSchemeFieldDataObj> getFieldPermissions() {
        return fieldPermissions;
    }

    public void setFieldPermissions(List<PermSchemeFieldDataObj> fieldPermissions) {
        this.fieldPermissions = fieldPermissions;
    }
}
