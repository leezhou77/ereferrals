package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.AssignmentDao;
import net.grouplink.ehelpdesk.domain.Assignment;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.domain.filter.AssignmentFilter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class AssignmentServiceImpl implements AssignmentService {

    private AssignmentDao assignmentDao;
    private Log log = LogFactory.getLog(getClass());

    public Assignment getById(Integer id) {
        return assignmentDao.getById(id);
    }

    public void save(Assignment assignment) {
        // validate assignment
        // urg is required
        if (assignment.getUserRoleGroup() == null) {
            throw new RuntimeException("assignment.userRoleGroup is required");
        }

        // one of group, cat, or catopt is required
        int fieldCount = 0;
        if (assignment.getGroup() != null) {
            fieldCount++;
        }

        if (assignment.getCategory() != null) {
            fieldCount++;
        }

        if (assignment.getCategoryOption() != null) {
            fieldCount++;
        }

        if (fieldCount == 0) {
            throw new RuntimeException("assignment requires a group or cat or catopt");
        }

        if (fieldCount > 1) {
            throw new RuntimeException("assignment should only have one of group, cat, or catopt set");
        }

        assignmentDao.save(assignment);
    }

    public List<Group> getGroupListByLocationId(Integer id) {
        return assignmentDao.getGroupListByLocationId(id);
    }

    public List<Category> getCategoryListByLocationIdAndGroupId(Integer locationId, Integer groupId) {
        return assignmentDao.getCategoryListByLocationIdAndGroupId(locationId, groupId);
    }

    public boolean delete(Assignment assignment) {
        try {
            assignmentDao.delete(assignment);
            assignmentDao.flush();
        } catch (DataIntegrityViolationException e) {
            log.info("Unable to delete assignment - ID:" + assignment.getId());
            return false;
        }
        return true;
    }

    public void flush() {
        assignmentDao.flush();
    }

    public void clear() {
        assignmentDao.clear();
    }

    public List<Assignment> getByCatOptAndLocation(CategoryOption categoryOption, Location location) {
        return assignmentDao.getByCatOptAndLocation(categoryOption, location);
    }

    public int getCountByCategoryOption(CategoryOption categoryOption) {
        return assignmentDao.getCountByCategoryOption(categoryOption);
    }

    public List<Assignment> getAssignments(AssignmentFilter assignmentFilter, String queryParam, Integer start, Integer count, Map<String, Boolean> sortFieldMap) {
        return assignmentDao.getAssignments(assignmentFilter, queryParam, start, count, sortFieldMap);
    }

    public void saveUniqueAssignment(Assignment assignment) {
        UserRoleGroup urg = assignment.getUserRoleGroup();
        Location location = assignment.getLocation();

        if(location != null && assignment.getGroup() == null && assignment.getCategory() == null && assignment.getCategoryOption() == null) {
            save(assignment);
        }
        
        // Check for assignments that the incoming assignment might override and delete them, saving the incoming.
        // override means that there are catopt assignments that will be covered by an encompassing category or group.
        Group group = assignment.getGroup();
        if (group != null) {
            deleteAll(getByUrgLocGroup(urg, location, group));
            save(assignment);
        }

        Category category = assignment.getCategory();
        if (category != null) {
            // get this category's group to see if there's an assignment for it that takes care of this one.
            Group thiscatgroup = category.getGroup();
            List<Assignment> thiscatass = getByUrgLocGroupOnly(urg, location, thiscatgroup);
            
            if (thiscatass.size() > 0) { // found an assignment that covers this category. Do nothing                
            } else {
                deleteAll(getByUrgLocCat(urg, location, category));
                save(assignment);
            }
        }

        CategoryOption catOpt = assignment.getCategoryOption();
        if (catOpt != null) {
            // Check up the chain for assignment for the encompassing category and group
            Group thiscatoptgroup = catOpt.getCategory().getGroup();
            List<Assignment> thiscatOptass = getByUrgLocGroupOnly(urg, location, thiscatoptgroup);
            
            Category thiscatoptcat = catOpt.getCategory();
            thiscatOptass.addAll(getByUrgLocCatOnly(urg, location, thiscatoptcat));
            
            if(thiscatOptass.size() > 0){ // found an assignment that covers this categoryOption. Do nothing.
            } else {
                List<Assignment> catoptlist = getByUrgLocCatOpt(urg, location, catOpt);
                if(catoptlist.size() == 0) save(assignment);
            }
        }
    }

    private List<Assignment> getByUrgLocCatOnly(UserRoleGroup urg, Location location, Category category) {
        return assignmentDao.getByUrgLocCatOnly(urg, location, category);
    }

    private List<Assignment> getByUrgLocGroupOnly(UserRoleGroup urg, Location location, Group group) {
        return assignmentDao.getByUrgLocGroupOnly(urg, location, group);
    }

    public int getAssignmentCount(AssignmentFilter assignmentFilter, String queryParam) {
        return assignmentDao.getAssignmentCount(assignmentFilter, queryParam);
    }

    public List<UserRoleGroup> getAssignments(Location location, Group group, Category category, CategoryOption categoryOption) {
        return assignmentDao.getAssignments(location, group, category, categoryOption);
    }

    public List<UserRoleGroup> getMoreBetterAssignments(Location location, Group group, Category category, CategoryOption categoryOption) {

        List<Assignment> assignments = new ArrayList<Assignment>();

        if (categoryOption != null && location != null) {
            assignments = assignmentDao.getByCatOptAndLocation(categoryOption, location);
        }

        if (assignments.size() == 0 && categoryOption != null) {
            assignments = assignmentDao.getByCatOpt(categoryOption);
        }

        if (assignments.size() == 0 && category != null && location != null) {
            assignments = assignmentDao.getByCatAndLoc(category, location);
        }

        if (assignments.size() == 0 && category != null) {
            assignments = assignmentDao.getByCat(category);
        }

        if (assignments.size() == 0 && group != null && location != null) {
            assignments = assignmentDao.getByGroupAndLoc(group, location);
        }

        if (assignments.size() == 0 && group != null) {
            assignments = assignmentDao.getByGroup(group);
        }

        List<UserRoleGroup> userRoleGroups = new ArrayList<UserRoleGroup>();
        for (Assignment assignment : assignments) {
            userRoleGroups.add(assignment.getUserRoleGroup());
        }

        return userRoleGroups;
    }

    private void deleteAll(List<Assignment> assignments) {
        for (Assignment assignment : assignments) {
            UserRoleGroup urg = assignment.getUserRoleGroup();
            urg.getAssignments().remove(assignment);
            delete(assignment);
        }
    }

    private List<Assignment> getByUrgLocGroup(UserRoleGroup urg, Location location, Group group) {
        return assignmentDao.getByUrgLocGroup(urg, location, group);
    }

    private List<Assignment> getByUrgLocCat(UserRoleGroup urg, Location location, Category cat) {
        return assignmentDao.getByUrgLocCat(urg, location, cat);
    }

    private List<Assignment> getByUrgLocCatOpt(UserRoleGroup urg, Location location, CategoryOption catOpt) {
        return assignmentDao.getByUrgLocCatOpt(urg, location, catOpt);
    }

    public void setAssigmentDao(AssignmentDao assignmentDao) {
        this.assignmentDao = assignmentDao;
    }


}
