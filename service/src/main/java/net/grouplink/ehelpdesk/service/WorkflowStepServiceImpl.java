package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.WorkflowStep;
import net.grouplink.ehelpdesk.dao.WorkflowStepDao;

public class WorkflowStepServiceImpl implements WorkflowStepService {
    private WorkflowStepDao workflowStepDao;

    public WorkflowStep getById(Integer workflowStepId) {
        return workflowStepDao.getById(workflowStepId);
    }

    public void delete(WorkflowStep ws) {
        workflowStepDao.delete(ws);
    }

    public void setWorkflowStepDao(WorkflowStepDao workflowStepDao) {
        this.workflowStepDao = workflowStepDao;
    }
}
