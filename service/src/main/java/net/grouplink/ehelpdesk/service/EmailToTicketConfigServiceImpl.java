package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.EmailToTicketConfigDao;
import net.grouplink.ehelpdesk.domain.EmailToTicketConfig;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.User;
import org.apache.commons.lang.StringUtils;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Jan 4, 2008
 * Time: 4:38:13 PM
 */
public class EmailToTicketConfigServiceImpl implements EmailToTicketConfigService {

    EmailToTicketConfigDao emailToTicketConfigDao;
    private TicketService ticketService;

    public List<EmailToTicketConfig> getAll() {
        return emailToTicketConfigDao.getAll();
    }

    public List<EmailToTicketConfig> getAllEnabled() {
        return emailToTicketConfigDao.getAllEnabled();
    }

    public EmailToTicketConfig getById(Integer id) {
        EmailToTicketConfig emailToTicketConfig = emailToTicketConfigDao.getById(id);
        String security = emailToTicketConfig.getSecurity();

        // check if security is blank. if so, set to the appropriate value (to handle pre-9.1.4)
        if (StringUtils.isBlank(security)) {
            if (emailToTicketConfig.isUseSsl()) {
                security = EmailToTicketConfig.SECURITY_SSL;
            } else {
                security = EmailToTicketConfig.SECURITY_NONE;
            }

            emailToTicketConfig.setSecurity(security);
        }

        return emailToTicketConfig;
    }

    public EmailToTicketConfig getByGroup(Group group) {
        return emailToTicketConfigDao.getByGroup(group);
    }

    public void save(EmailToTicketConfig emailToTicketConfig) {
        emailToTicketConfigDao.save(emailToTicketConfig);
    }

    public void delete(EmailToTicketConfig emailToTicketConfig, User user) {
        // find all tickets referencing config and clear the reference
        List<Ticket> tickets = ticketService.getTicketsByEmailToTicketConfig(emailToTicketConfig);
        for (Object ticket1 : tickets) {
            Ticket ticket = (Ticket) ticket1;
            ticket.setEmailToTicketConfig(null);
        }
        ticketService.saveTickets(tickets, user);
        emailToTicketConfigDao.delete(emailToTicketConfig);
    }

    public int getEmailToTicketConfigCountByPriorityId(Integer priorityId) {
        return emailToTicketConfigDao.getEmailToTicketConfigCountByPriorityId(priorityId);
    }

    public int getCountByGroup(Group group) {
        return emailToTicketConfigDao.getCountByGroup(group);
    }

    public void setEmailToTicketConfigDao(EmailToTicketConfigDao emailToTicketConfigDao) {
        this.emailToTicketConfigDao = emailToTicketConfigDao;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }
}
