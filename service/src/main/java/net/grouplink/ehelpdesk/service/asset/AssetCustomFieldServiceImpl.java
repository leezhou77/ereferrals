package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.domain.asset.AssetCustomField;
import net.grouplink.ehelpdesk.domain.asset.AssetFieldGroup;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.AssetCustomFieldValue;
import net.grouplink.ehelpdesk.dao.asset.AssetCustomFieldDao;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Jul 18, 2008
 * Time: 1:28:45 PM
 */
public class AssetCustomFieldServiceImpl implements AssetCustomFieldService {
    private AssetCustomFieldDao assetCustomFieldDao;

    public AssetCustomField getById(Integer id) {
        return assetCustomFieldDao.getById(id);
    }

    public List<AssetCustomField> getAll() {
        return assetCustomFieldDao.getAll();
    }

    public List<AssetCustomField> getByAssetFieldGroup(AssetFieldGroup assetFieldGroup) {
        return assetCustomFieldDao.getByAssetFieldGroup(assetFieldGroup);
    }

    public List<AssetCustomField> getByAsset(Asset asset) {
        return assetCustomFieldDao.getByAsset(asset);
    }

    public void saveAssetCustomField(AssetCustomField assetCustomField) {
        assetCustomFieldDao.saveAssetCustomField(assetCustomField);
    }

    public void deleteAssetCustomField(AssetCustomField assetCustomField) {
        assetCustomFieldDao.deleteAssetCustomField(assetCustomField);
    }

    public AssetCustomField getByName(String name) {
        return assetCustomFieldDao.getByName(name);
    }

    public void flush() {
        assetCustomFieldDao.flush();
    }

    public AssetCustomFieldValue getCustomFieldValue(Asset asset, AssetCustomField assetCustomField) {
        return assetCustomFieldDao.getCustomFieldValue(asset, assetCustomField);
    }

    public void setAssetCustomFieldDao(AssetCustomFieldDao assetCustomFieldDao) {
        this.assetCustomFieldDao = assetCustomFieldDao;
    }
}
