package net.grouplink.ehelpdesk.service.zen;

import net.grouplink.ehelpdesk.domain.zen.Device;
import net.grouplink.ehelpdesk.dao.zen.DeviceDao;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Apr 1, 2009
 * Time: 2:48:58 PM
 */
public class DeviceServiceImpl implements DeviceService {
    private DeviceDao deviceDao;

    public List<Device> getAll() {
        return deviceDao.getAll();
    }

    public void setDeviceDao(DeviceDao deviceDao) {
        this.deviceDao = deviceDao;
    }
}
