package net.grouplink.ehelpdesk.service.acl.dataobj;

public class PermSchemeFieldDataObj {
    private String key;
    private String fieldPermissionScheme;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getFieldPermissionScheme() {
        return fieldPermissionScheme;
    }

    public void setFieldPermissionScheme(String fieldPermissionScheme) {
        this.fieldPermissionScheme = fieldPermissionScheme;
    }
}
