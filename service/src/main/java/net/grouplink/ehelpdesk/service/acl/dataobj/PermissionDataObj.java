package net.grouplink.ehelpdesk.service.acl.dataobj;

import net.grouplink.ehelpdesk.domain.acl.Permission;

import java.util.ArrayList;
import java.util.List;

public class PermissionDataObj {
    private List<Permission> globalPermissions = new ArrayList<Permission>();
    private List<Permission> groupPermissions = new ArrayList<Permission>();
    private List<Permission> fieldPermissions = new ArrayList<Permission>();

    public List<Permission> getGlobalPermissions() {
        return globalPermissions;
    }

    public void setGlobalPermissions(List<Permission> globalPermissions) {
        this.globalPermissions = globalPermissions;
    }

    public List<Permission> getGroupPermissions() {
        return groupPermissions;
    }

    public void setGroupPermissions(List<Permission> groupPermissions) {
        this.groupPermissions = groupPermissions;
    }

    public List<Permission> getFieldPermissions() {
        return fieldPermissions;
    }

    public void setFieldPermissions(List<Permission> fieldPermissions) {
        this.fieldPermissions = fieldPermissions;
    }
}
