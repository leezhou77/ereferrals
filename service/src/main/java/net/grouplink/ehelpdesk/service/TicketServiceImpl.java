package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.common.utils.MailConfig;
import net.grouplink.ehelpdesk.common.utils.RegexUtils;
import net.grouplink.ehelpdesk.dao.TicketDao;
import net.grouplink.ehelpdesk.domain.AdvancedTicketsFilter;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.CustomFieldValues;
import net.grouplink.ehelpdesk.domain.EmailToTicketConfig;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.domain.Status;
import net.grouplink.ehelpdesk.domain.Survey;
import net.grouplink.ehelpdesk.domain.SurveyData;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketAudit;
import net.grouplink.ehelpdesk.domain.TicketHistory;
import net.grouplink.ehelpdesk.domain.TicketPriority;
import net.grouplink.ehelpdesk.domain.TicketTemplate;
import net.grouplink.ehelpdesk.domain.TicketTemplateLaunch;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.ZenAsset;
import net.grouplink.ehelpdesk.service.asset.AssetService;
import org.apache.commons.lang.LocaleUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.context.support.MessageSourceAccessor;

import javax.mail.MessagingException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;

public class TicketServiceImpl extends ApplicationObjectSupport implements TicketService, Serializable {
    private Log log = LogFactory.getLog(getClass());
    private TicketDao ticketDao;
    private StatusService statusService;
    private MailService mailService;
    private EmailToTicketConfigService emailToTicketConfigService;
    private SurveyService surveyService;
    private SurveyDataService surveyDataService;
    private UserService userService;
    private LocationService locationService;
    private GroupService groupService;
    private CategoryService categoryService;
    private CategoryOptionService categoryOptionService;
    private UserRoleGroupService userRoleGroupService;
    private TicketPriorityService ticketPriorityService;
    private AssetService assetService;

    public void deleteTicket(Ticket ticket, User user) {
//        ticketDao.deleteTicket(ticket);
        //Tickets are not supposed to be deleted
        ticket.setStatus(statusService.getStatusById(Status.DELETED));
        saveTicket(ticket, user);
        ticketDao.flush();
    }

    public void purgeTicket(Ticket ticket) {
        // delete references

        ticketDao.deleteTicket(ticket);
    }

    public List<Ticket> getTickets() {
        return ticketDao.getTickets();
    }

    public void saveTicket(Ticket ticket, User user) {
        if ((ticket.getAssignedTo() == null || ticket.getLocation() == null || ticket.getGroup() == null) &&
                (ticket.getTicketTemplateStatus() != null && !ticket.getTicketTemplateStatus().equals(Ticket.TT_STATUS_PENDING))) {
            IllegalArgumentException iae =
                    new IllegalArgumentException("Error saving ticket. AssignedTo, Location, or Group is null");
            log.error("Error saving ticket. AssignedTo, Location, or Group is null", iae);
            throw iae;
        }

        Date now = new Date();
        if (ticket.getId() == null || ticket.getCreatedDate() == null) {
            ticket.setCreatedDate(now);
        }

        ticket.setModifiedDate(now);

        ticketDao.saveServiceTicket(ticket);

        // audit
        if (ticket.getTicketTemplateStatus() == null ||
                (ticket.getTicketTemplateStatus() != null &&
                        !ticket.getTicketTemplateStatus().equals(Ticket.TT_STATUS_PENDING))) {
            auditTicket(ticket, user, now);
        }

        // surveys
        sendSurveyLink(ticket);
    }

    private void sendSurveyLink(Ticket ticket) {
        boolean changedToClosedStatus = false;
        Status status = ticket.getStatus();
        Status oldStatus = (ticket.getOldTicket() != null) ? ticket.getOldTicket().getStatus() : null;
        if (oldStatus == null) {
            if (status != null && status.getId().equals(Status.CLOSED)) {
                changedToClosedStatus = true;
            }
        } else {
            if (!oldStatus.getId().equals(Status.CLOSED) && status != null && status.getId().equals(Status.CLOSED)) {
                changedToClosedStatus = true;
            }
        }

        if (changedToClosedStatus) {
            CategoryOption cOpt = ticket.getCategoryOption();
            if (cOpt != null && cOpt.getSurvey() != null) {
                if (surveyDataService.getCountByTicketId(ticket.getId()) == 0) {
                    Survey survey = surveyService.getSurveyById(cOpt.getSurvey().getId());
                    try {
                        surveyService.sendSurveyNotificationEmail(survey, ticket);
                    } catch (Exception e) {
                        log.error("error sending survey notification mail", e);
                    }
                }

            }
        }
    }

    private void auditTicket(Ticket ticket, User user, Date now) {
        Ticket oldTicket = ticket.getOldTicket();

        if (oldTicket == null)
            return;

        // Get default locale
        Locale locale = LocaleContextHolder.getLocale();
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, locale);

        // contact
        if (oldTicket.getContact() == null && ticket.getContact() == null) {
            // do nothing
        } else if (oldTicket.getContact() == null && ticket.getContact() != null) {
            String beforeValue = null;
            String afterValue = ticket.getContact().getId().toString();
            addTicketAudit(ticket, TicketAudit.PROPERTY_CONTACT, beforeValue, afterValue, null, null, user, now);
        } else if (oldTicket.getContact() != null && ticket.getContact() == null) {
            String beforeValue = oldTicket.getContact().getId().toString();
            String afterValue = null;
            addTicketAudit(ticket, TicketAudit.PROPERTY_CONTACT, beforeValue, afterValue, null, null, user, now);
        } else if (!oldTicket.getContact().getId().equals(ticket.getContact().getId())) {
            String beforeValue = oldTicket.getContact().getId().toString();
            String afterValue = ticket.getContact().getId().toString();
            addTicketAudit(ticket, TicketAudit.PROPERTY_CONTACT, beforeValue, afterValue, null, null, user, now);
        }

        // location
        if (oldTicket.getLocation() == null && ticket.getLocation() == null) {
            // do nothing
        } else if (oldTicket.getLocation() == null && ticket.getLocation() != null) {
            String beforeValue = null;
            String afterValue = ticket.getLocation().getId().toString();
            addTicketAudit(ticket, TicketAudit.PROPERTY_LOCATION, beforeValue, afterValue, null, null, user, now);
        } else if (oldTicket.getLocation() != null && ticket.getLocation() == null) {
            String beforeValue = oldTicket.getLocation().getId().toString();
            String afterValue = null;
            addTicketAudit(ticket, TicketAudit.PROPERTY_LOCATION, beforeValue, afterValue, null, null, user, now);
        } else if (!oldTicket.getLocation().getId().equals(ticket.getLocation().getId())) {
            String beforeValue = oldTicket.getLocation().getId().toString();
            String afterValue = ticket.getLocation().getId().toString();
            addTicketAudit(ticket, TicketAudit.PROPERTY_LOCATION, beforeValue, afterValue, null, null, user, now);
        }

        // group
        if (oldTicket.getGroup() == null && ticket.getGroup() == null) {
            // do nothing
        } else if (oldTicket.getGroup() == null && ticket.getGroup() != null) {
            String beforeValue = null;
            String afterValue = ticket.getGroup().getId().toString();
            addTicketAudit(ticket, TicketAudit.PROPERTY_GROUP, beforeValue, afterValue, null, null, user, now);
        } else if (oldTicket.getGroup() != null && ticket.getGroup() == null) {
            String beforeValue = oldTicket.getGroup().getId().toString();
            String afterValue = null;
            addTicketAudit(ticket, TicketAudit.PROPERTY_GROUP, beforeValue, afterValue, null, null, user, now);
        } else if (!oldTicket.getGroup().getId().equals(ticket.getGroup().getId())) {
            String beforeValue = oldTicket.getGroup().getId().toString();
            String afterValue = ticket.getGroup().getId().toString();
            addTicketAudit(ticket, TicketAudit.PROPERTY_GROUP, beforeValue, afterValue, null, null, user, now);
        }

        // category
        if (oldTicket.getCategory() == null && ticket.getCategory() == null) {
            // do nothing
        } else if (oldTicket.getCategory() == null && ticket.getCategory() != null) {
            String beforeValue = null;
            String afterValue = ticket.getCategory().getId().toString();
            addTicketAudit(ticket, TicketAudit.PROPERTY_CATEGORY, beforeValue, afterValue, null, null, user, now);
        } else if (oldTicket.getCategory() != null && ticket.getCategory() == null) {
            String beforeValue = oldTicket.getCategory().getId().toString();
            String afterValue = null;
            addTicketAudit(ticket, TicketAudit.PROPERTY_CATEGORY, beforeValue, afterValue, null, null, user, now);
        } else if (!oldTicket.getCategory().getId().equals(ticket.getCategory().getId())) {
            String beforeValue = oldTicket.getCategory().getId().toString();
            String afterValue = ticket.getCategory().getId().toString();
            addTicketAudit(ticket, TicketAudit.PROPERTY_CATEGORY, beforeValue, afterValue, null, null, user, now);
        }

        // category option
        if (oldTicket.getCategoryOption() == null && ticket.getCategoryOption() == null) {
            // do nothing
        } else if (oldTicket.getCategoryOption() == null && ticket.getCategoryOption() != null) {
            String beforeValue = null;
            String afterValue = ticket.getCategoryOption().getId().toString();
            addTicketAudit(ticket, TicketAudit.PROPERTY_CATEGORYOPTION, beforeValue, afterValue, null, null, user, now);
        } else if (oldTicket.getCategoryOption() != null && ticket.getCategoryOption() == null) {
            String beforeValue = oldTicket.getCategoryOption().getId().toString();
            String afterValue = null;
            addTicketAudit(ticket, TicketAudit.PROPERTY_CATEGORYOPTION, beforeValue, afterValue, null, null, user, now);
        } else if (!oldTicket.getCategoryOption().getId().equals(ticket.getCategoryOption().getId())) {
            String beforeValue = oldTicket.getCategoryOption().getId().toString();
            String afterValue = ticket.getCategoryOption().getId().toString();
            addTicketAudit(ticket, TicketAudit.PROPERTY_CATEGORYOPTION, beforeValue, afterValue, null, null, user, now);
        }

        // assignedto
        if (oldTicket.getAssignedTo() == null && ticket.getAssignedTo() == null) {
            // do nothing
        } else if (oldTicket.getAssignedTo() == null && ticket.getAssignedTo() != null) {
            String beforeValue = null;
            String afterValue = ticket.getAssignedTo().getId().toString();
            addTicketAudit(ticket, TicketAudit.PROPERTY_ASSIGNEDTO, beforeValue, afterValue, null, null, user, now);
        } else if (oldTicket.getAssignedTo() != null && ticket.getAssignedTo() == null) {
            String beforeValue = oldTicket.getAssignedTo().getId().toString();
            String afterValue = null;
            addTicketAudit(ticket, TicketAudit.PROPERTY_ASSIGNEDTO, beforeValue, afterValue, null, null, user, now);
        } else if (!oldTicket.getAssignedTo().getId().equals(ticket.getAssignedTo().getId())) {
            String beforeValue = oldTicket.getAssignedTo().getId().toString();
            String afterValue = ticket.getAssignedTo().getId().toString();
            addTicketAudit(ticket, TicketAudit.PROPERTY_ASSIGNEDTO, beforeValue, afterValue, null, null, user, now);
        }

        // priority
        if (oldTicket.getPriority() == null && ticket.getPriority() == null) {
            // do nothing
        } else if (oldTicket.getPriority() == null && ticket.getPriority() != null) {
            String beforeValue = null;
            String afterValue = ticket.getPriority().getId().toString();
            addTicketAudit(ticket, TicketAudit.PROPERTY_TICKETPRIORITY, beforeValue, afterValue, null, null, user, now);
        } else if (oldTicket.getPriority() != null && ticket.getPriority() == null) {
            String beforeValue = oldTicket.getPriority().getId().toString();
            String afterValue = null;
            addTicketAudit(ticket, TicketAudit.PROPERTY_TICKETPRIORITY, beforeValue, afterValue, null, null, user, now);
        } else if (!oldTicket.getPriority().getId().equals(ticket.getPriority().getId())) {
            String beforeValue = oldTicket.getPriority().getId().toString();
            String afterValue = ticket.getPriority().getId().toString();
            addTicketAudit(ticket, TicketAudit.PROPERTY_TICKETPRIORITY, beforeValue, afterValue, null, null, user, now);
        }

        // status
        if (oldTicket.getStatus() == null && ticket.getStatus() == null) {
            // do nothing
        } else if (oldTicket.getStatus() == null && ticket.getStatus() != null) {
            String beforeValue = null;
            String afterValue = ticket.getStatus().getId().toString();
            addTicketAudit(ticket, TicketAudit.PROPERTY_STATUS, beforeValue, afterValue, null, null, user, now);
        } else if (oldTicket.getStatus() != null && ticket.getStatus() == null) {
            String beforeValue = oldTicket.getStatus().getId().toString();
            String afterValue = null;
            addTicketAudit(ticket, TicketAudit.PROPERTY_STATUS, beforeValue, afterValue, null, null, user, now);
        } else if (!oldTicket.getStatus().getId().equals(ticket.getStatus().getId())) {
            String beforeValue = oldTicket.getStatus().getId().toString();
            String afterValue = ticket.getStatus().getId().toString();
            addTicketAudit(ticket, TicketAudit.PROPERTY_STATUS, beforeValue, afterValue, null, null, user, now);
        }

        // submitted by
        if (oldTicket.getSubmittedBy() == null && ticket.getSubmittedBy() == null) {
            // do nothing
        } else if (oldTicket.getSubmittedBy() == null && ticket.getSubmittedBy() != null) {
            String beforeValue = null;
            String afterValue = ticket.getSubmittedBy().getId().toString();
            addTicketAudit(ticket, TicketAudit.PROPERTY_SUBMITTEDBY, beforeValue, afterValue, null, null, user, now);
        } else if (oldTicket.getSubmittedBy() != null && ticket.getSubmittedBy() == null) {
            String beforeValue = oldTicket.getSubmittedBy().getId().toString();
            String afterValue = null;
            addTicketAudit(ticket, TicketAudit.PROPERTY_SUBMITTEDBY, beforeValue, afterValue, null, null, user, now);
        } else if (!oldTicket.getSubmittedBy().getId().equals(ticket.getSubmittedBy().getId())) {
            String beforeValue = oldTicket.getSubmittedBy().getId().toString();
            String afterValue = ticket.getSubmittedBy().getId().toString();
            addTicketAudit(ticket, TicketAudit.PROPERTY_SUBMITTEDBY, beforeValue, afterValue, null, null, user, now);
        }

        // estimated date
        if (oldTicket.getEstimatedDate() == null && ticket.getEstimatedDate() == null) {
            // do nothing
        } else if (oldTicket.getEstimatedDate() == null && ticket.getEstimatedDate() != null) {
            String beforeValue = null;
            String afterValue = dateFormat.format(ticket.getEstimatedDate());
            addTicketAudit(ticket, TicketAudit.PROPERTY_ESTIMATEDDATE, beforeValue, afterValue, null, null, user, now);
        } else if (oldTicket.getEstimatedDate() != null && ticket.getEstimatedDate() == null) {
            String beforeValue = dateFormat.format(oldTicket.getEstimatedDate());
            String afterValue = null;
            addTicketAudit(ticket, TicketAudit.PROPERTY_ESTIMATEDDATE, beforeValue, afterValue, null, null, user, now);
        } else if (!DateUtils.truncate(oldTicket.getEstimatedDate(), Calendar.DAY_OF_MONTH).equals(DateUtils.truncate(
                ticket.getEstimatedDate(), Calendar.DAY_OF_MONTH))) {
            String beforeValue = dateFormat.format(oldTicket.getEstimatedDate());
            String afterValue = dateFormat.format(ticket.getEstimatedDate());
            addTicketAudit(ticket, TicketAudit.PROPERTY_ESTIMATEDDATE, beforeValue, afterValue, null, null, user, now);
        }

        // asset
        if (oldTicket.getAsset() == null && ticket.getAsset() == null) {
            // do nothing
        } else if (oldTicket.getAsset() == null && ticket.getAsset() != null) {
            String beforeValue = null;
            String afterValue = ticket.getAsset().getId().toString();
            addTicketAudit(ticket, TicketAudit.PROPERTY_ASSET, beforeValue, afterValue, null, null, user, now);
        } else if (oldTicket.getAsset() != null && ticket.getAsset() == null) {
            String beforeValue = oldTicket.getAsset().getId().toString();
            String afterValue = null;
            addTicketAudit(ticket, TicketAudit.PROPERTY_ASSET, beforeValue, afterValue, null, null, user, now);
        } else if (!oldTicket.getAsset().getId().equals(ticket.getAsset().getId())) {
            String beforeValue = oldTicket.getAsset().getId().toString();
            String afterValue = ticket.getAsset().getId().toString();
            addTicketAudit(ticket, TicketAudit.PROPERTY_ASSET, beforeValue, afterValue, null, null, user, now);
        }

        // subject
        if (oldTicket.getSubject() == null && ticket.getSubject() == null) {
            // do nothing
        } else if (oldTicket.getSubject() == null && ticket.getSubject() != null) {
            String beforeText = null;
            String afterText = ticket.getSubject();
            addTicketAudit(ticket, TicketAudit.PROPERTY_SUBJECT, null, null, beforeText, afterText, user, now);
        } else if (oldTicket.getSubject() != null && ticket.getSubject() == null) {
            String beforeText = oldTicket.getSubject();
            String afterText = null;
            addTicketAudit(ticket, TicketAudit.PROPERTY_SUBJECT, null, null, beforeText, afterText, user, now);
        } else if (!oldTicket.getSubject().equals(ticket.getSubject())) {
            String beforeText = oldTicket.getSubject();
            String afterText = ticket.getSubject();
            addTicketAudit(ticket, TicketAudit.PROPERTY_SUBJECT, null, null, beforeText, afterText, user, now);
        }

        // note
        if (oldTicket.getNote() == null && ticket.getNote() == null) {
            // do nothing
        } else if (oldTicket.getNote() == null && ticket.getNote() != null) {
            String beforeText = null;
            String afterText = ticket.getNote();
            addTicketAudit(ticket, TicketAudit.PROPERTY_NOTE, null, null, beforeText, afterText, user, now);
        } else if (oldTicket.getNote() != null && ticket.getNote() == null) {
            String beforeText = oldTicket.getNote();
            String afterText = null;
            addTicketAudit(ticket, TicketAudit.PROPERTY_NOTE, null, null, beforeText, afterText, user, now);
        } else if (!oldTicket.getNote().equals(ticket.getNote())) {
            String beforeText = oldTicket.getNote();
            String afterText = ticket.getNote();
            addTicketAudit(ticket, TicketAudit.PROPERTY_NOTE, null, null, beforeText, afterText, user, now);
        }

        // zen asset id
        if (oldTicket.getZenAsset() == null && ticket.getZenAsset() == null) {
            // do nothing
        } else if (oldTicket.getZenAsset() == null && ticket.getZenAsset() != null) {
            String beforeValue = null;
            String afterValue = ticket.getZenAsset().getId().toString();
            addTicketAudit(ticket, TicketAudit.PROPERTY_ASSET, beforeValue, afterValue, null, null, user, now);
        } else if (oldTicket.getZenAsset() != null && ticket.getZenAsset() == null) {
            String beforeValue = oldTicket.getZenAsset().getId().toString();
            String afterValue = null;
            addTicketAudit(ticket, TicketAudit.PROPERTY_ASSET, beforeValue, afterValue, null, null, user, now);
        } else if (!oldTicket.getZenAsset().getId().equals(ticket.getZenAsset().getId())) {
            String beforeValue = oldTicket.getZenAsset().getId().toString();
            String afterValue = ticket.getZenAsset().getId().toString();
            addTicketAudit(ticket, TicketAudit.PROPERTY_ASSET, beforeValue, afterValue, null, null, user, now);
        }

        // worktime
        if (oldTicket.getWorkTime() == null && ticket.getWorkTime() == null) {
            // do nothing
        } else if (oldTicket.getWorkTime() == null && ticket.getWorkTime() != null) {
            String beforeValue = null;
            String afterValue = ticket.getWorkTime().toString();
            addTicketAudit(ticket, TicketAudit.PROPERTY_WORKTIME, beforeValue, afterValue, null, null, user, now);
        } else if (oldTicket.getWorkTime() != null && ticket.getWorkTime() == null) {
            String beforeValue = oldTicket.getWorkTime().toString();
            String afterValue = null;
            addTicketAudit(ticket, TicketAudit.PROPERTY_WORKTIME, beforeValue, afterValue, null, null, user, now);
        } else if (!oldTicket.getWorkTime().equals(ticket.getWorkTime())) {
            String beforeValue = oldTicket.getWorkTime().toString();
            String afterValue = ticket.getWorkTime().toString();
            addTicketAudit(ticket, TicketAudit.PROPERTY_WORKTIME, beforeValue, afterValue, null, null, user, now);
        }

        // custom fields
        Set<CustomFieldValues> ticketCFVs = ticket.getCustomeFieldValues();
        Set<CustomFieldValues> oldTicketCFVs = oldTicket.getCustomeFieldValues();
        for (CustomFieldValues ticketCFV : ticketCFVs) {
            // Find the oldTicket cfv that matches this one
            for (CustomFieldValues oldTicketCFV : oldTicketCFVs) {
                if (ticketCFV.getCustomField().getId().equals(oldTicketCFV.getCustomField().getId())) {
                    // Now check for the change in the fieldValue
                    String ticketValue = ticketCFV.getFieldValue();
                    String oldTicketValue = oldTicketCFV.getFieldValue();
                    String auditProperty = ticketCFV.getCustomField().getName();

                    if (oldTicketValue == null && ticketValue == null) {
                        // do nothing
                    } else if (oldTicketValue == null && ticketValue != null) {
                        String beforeValue = null;
                        addTicketAudit(ticket, auditProperty, null, null, beforeValue, ticketValue, user, now);
                    } else if (oldTicketValue != null && ticketValue == null) {
                        String afterValue = null;
                        addTicketAudit(ticket, auditProperty, null, null, oldTicketValue, afterValue, user, now);
                    } else if (!oldTicketValue.equals(ticketValue)) {
                        addTicketAudit(ticket, auditProperty, null, null, oldTicketValue, ticketValue, user, now);
                    }
                }
            }
        }
    }

    private void addTicketAudit(Ticket ticket, String property, String beforeValue, String afterValue,
                                String beforeText, String afterText, User user, Date now) {
        TicketAudit newAudit = new TicketAudit();
        newAudit.setProperty(property);
        newAudit.setTicket(ticket);
        newAudit.setDateStamp(now);
        newAudit.setUser(user);
        newAudit.setBeforeValue(beforeValue);
        newAudit.setAfterValue(afterValue);
        newAudit.setBeforeText(beforeText);
        newAudit.setAfterText(afterText);
        ticket.getTicketAudit().add(newAudit);
    }

    /**
     * The ticket object return by this function contains a copy of the old
     * ticket in the oldTicket field the oldTicket can be accessed by calling
     * the getOldTicket() function The oldTicket wil lbe null if the ticket is
     * a new object.
     *
     * @param id the id of the ticket to retrieve
     * @return the Ticket
     */
    public Ticket getTicketById(Integer id) {
        Ticket t = ticketDao.getTicketById(id);
        if (t != null) {
            Ticket clone = null;
            try {
                clone = (Ticket) t.clone();
            } catch (CloneNotSupportedException e) {
                log.error("error getting ticket clone", e);
            }

            t.setOldTicket(clone);
        }

        return t;
    }

    public List<Ticket> getAllTicketsById(Integer ticketId) {
        return ticketDao.getAllTicketsById(ticketId);
    }

    public List<Ticket> getTicketsByOwner(User user, Boolean showClosed) {
        return ticketDao.getTicketsByOwner(user, showClosed);
    }

    public Integer getTicketsByOwnerCount(User user) {
        return ticketDao.getTicketsByOwnerCount(user);
    }

    public Integer getTicketsByUrgIdCount(Integer urgId) {
        return ticketDao.getTicketsByUrgIdCount(urgId);
    }

    public List<Ticket> getTicketsAssignedToByUserId(Integer userId) {
        return ticketDao.getTicketsAssignedToByUserId(userId);
    }

    public List<Ticket> getFilteredTickets(User user, UserRoleGroup urg, Locale locale, String grouping,
                                           Boolean showClosed) {
        return getFilteredTickets("", user, urg, locale, null, null, null, null, null, null, null, null, grouping,
                showClosed, null);
    }

    public List<Ticket> getFilteredTickets(String byWho, User user, UserRoleGroup urg, Locale locale, String ticketId,
                                           String subject, String categoryOption, String assignedTo, String status,
                                           String creationDate, String location, String priority, String grouping,
                                           Boolean showClosed, String contact) {
//        ScrollableResults allTickets = getTicketScroll(urg);
        List<Ticket> allTickets;
//        long start = System.currentTimeMillis();
        if ("owner".equalsIgnoreCase(byWho)) {
            allTickets = getTicketsByOwner(user, showClosed);
        } else if ("assignedToMe".equalsIgnoreCase(grouping)) {
            allTickets = getTicketsAssignedToByUserRoleGroup(urg, showClosed);
        } else if ("ticketPool".equalsIgnoreCase(grouping)) {
            allTickets = getTicketsByTicketPoolByGroupId(urg.getGroup().getId(), showClosed);
        } else {
            allTickets = getAllGroupTicketsByGroupId(urg.getGroup().getId(), showClosed);
        }

        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT, locale);

        List<Ticket> tickets = new ArrayList<Ticket>();

        // Loop through each ticket for this user and check each provided filter string.
        // If a match is found in any filter field add the ticket to the return list.
        ResourceBundle bund = ResourceBundle.getBundle("messages", locale);
        for (Ticket allTicket : allTickets) {
//        while(allTickets.next()){
//            Ticket ticket = (Ticket) allTickets.get(0);

            String assignedToName;
            if (allTicket.getAssignedTo() != null && allTicket.getAssignedTo().getUserRole() != null) {
                assignedToName = allTicket.getAssignedTo().getUserRole().getUser().getFirstName() + " "
                        + allTicket.getAssignedTo().getUserRole().getUser().getLastName();
            } else {
                assignedToName = bund.getString("ticketList.ticketPool");
                //assignedToName = ticket.getAssignedTo().getGroup().getName();
            }
            String dateCreated = dateFormat.format(allTicket.getCreatedDate());

            boolean ticketBool = (StringUtils.isBlank(ticketId) ||
                    (allTicket.getTicketId().toString().indexOf(ticketId) > -1));
            boolean subjectBool = (StringUtils.isBlank(subject) ||
                    (StringUtils.isNotBlank(allTicket.getSubject()) &&
                            allTicket.getSubject().toLowerCase().indexOf(subject.toLowerCase()) > -1));
            boolean catBool = (StringUtils.isBlank(categoryOption) ||
                    (allTicket.getCategoryOption() != null &&
                            allTicket.getCategoryOption().getName().toLowerCase().indexOf(categoryOption.toLowerCase()) > -1));
            boolean assignedToBool = (StringUtils.isBlank(assignedTo) ||
                    assignedToName.toLowerCase().indexOf(assignedTo.toLowerCase()) > -1);
            boolean statusBool = (StringUtils.isBlank(status) || status.equals("") ||
                    allTicket.getStatus().getName().toLowerCase().indexOf(status.toLowerCase()) > -1);
            boolean creationDateBool = (StringUtils.isBlank(creationDate) ||
                    dateCreated.toLowerCase().indexOf(creationDate.toLowerCase()) > -1);
            boolean locationBool = (StringUtils.isBlank(location) ||
                    allTicket.getLocation().getName().toLowerCase().indexOf(location.toLowerCase()) > -1);
            boolean priorityBool = (StringUtils.isBlank(priority) ||
                    allTicket.getPriority().getId().toString().equals(priority));
            boolean groupingBool = (StringUtils.isBlank(grouping) || ticketEqGrouping(user, allTicket, grouping));
            String contactName = allTicket.getContact().getFirstName() + " " + allTicket.getContact().getLastName();
            boolean contactBool = (StringUtils.isBlank(contact) ||
                    (StringUtils.isNotBlank(contact) && contactName.toLowerCase().indexOf(contact.toLowerCase()) > -1));

            if (ticketBool && subjectBool && catBool && assignedToBool && statusBool &&
                    creationDateBool && locationBool && priorityBool && groupingBool && contactBool) {
                tickets.add(allTicket);
            }
        }

        return tickets;
    }

    private List<Ticket> getTicketsByTicketPoolByGroupId(Integer groupId, Boolean showClosed) {
        return ticketDao.getTicketsByTicketPool(groupId, showClosed);
    }

    public List<Ticket> getTicketsByAdvancedSearch(AdvancedTicketsFilter filter) {
        List<Ticket> list;

        if (StringUtils.isNotBlank(filter.getTicketNumber())) {
            list = getAllTicketsById(new Integer(filter.getTicketNumber()));
        } else {
            //List intermediateList
            list = ticketDao.getTicketsByAdvancedSearch(filter);
        }
        return list;
    }

    public void flush() {
        this.ticketDao.flush();
    }

    public Ticket getTicketByOldTicketId(Integer ticketId) {
        return ticketDao.getTicketByOldTicketId(ticketId);
    }

    public List<Ticket> getTicketsByZenAsset(ZenAsset zenAsset) {
        return ticketDao.getTicketsByZenAsset(zenAsset);
    }

    public List<Ticket> getTopLevelTicketsByTicketTemplateLaunch(TicketTemplateLaunch ttl) {
        return ticketDao.getTopLevelTicketsByTicketTemplateLaunch(ttl);
    }

    public List<Ticket> getTicketsByTicketTemplate(TicketTemplate ticketTemplate) {
        return ticketDao.getTicketsByTicketTemplate(ticketTemplate);
    }

    public CustomFieldValues getCustomFieldValue(Ticket ticket, String column) {
        CustomFieldValues cfVal = null;
        for (CustomFieldValues cfv : ticket.getCustomeFieldValues()) {
            if (StringUtils.equalsIgnoreCase(cfv.getCustomField().getName(), column)) {
                cfVal = cfv;
            }
        }
        return cfVal;
    }

    public SurveyData getSurveyData(Ticket ticket, String questionName) {
        SurveyData surveyData = null;
        for (SurveyData sd : ticket.getSurveyData()) {
            if (sd.getSurveyQuestion().getSurveyQuestionText().equals(questionName)) {
                surveyData = sd;   
            }
        }
        return surveyData; 
    }

    public TicketHistory getMostRecentTicketHistory(Ticket ticket) {
        TicketHistory latest = null;
        for (TicketHistory th : ticket.getTicketHistory()) {
            if (latest == null) {
                latest = th;
                continue;
            }

            if (th.getCreatedDate().after(latest.getCreatedDate())) {
                latest = th;
            }
        }
        return latest;
    }

    public Ticket getTicketByOldTicketIdAndGroupId(Integer oldTicketId, Integer groupId) {
        return this.ticketDao.getTicketByOldTicketIdAndGroupId(oldTicketId, groupId);
    }

    public void clear() {
        this.ticketDao.clear();
    }

    public int getTicketCountByGroupId(Integer groupId) {
        return this.ticketDao.getTicketCountByGroupId(groupId);
    }

    public int getTicketCountByCatOptId(Integer catOptId) {
        return this.ticketDao.getTicketCountByCatOptId(catOptId);
    }

    public int getTicketCountByStatusId(Integer statusId) {
        return this.ticketDao.getTicketCountByStatusId(statusId);
    }

    public int getTicketCountByPriorityId(Integer priorityId) {
        return this.ticketDao.getTicketCountByPriorityId(priorityId);
    }

    public void saveMigrationTicket(Ticket ticket) {
        this.ticketDao.saveServiceTicket(ticket);
    }

    public void sendEmailNotification(Ticket ticket, User user, String htmlMessage, String txtMessage)
            throws MessagingException, UnsupportedEncodingException {

        Boolean sendLinkToUser;
        String emailDisplayName;
        String emailFromAddress;

        EmailToTicketConfig etc1;
        MailConfig mc = mailService.getMailConfig();
        if (mailService.isMailConfigured()) {
            if (ticket.getEmailToTicketConfig() != null) {
                etc1 = ticket.getEmailToTicketConfig();
                emailFromAddress = etc1.getReplyEmailAddress();
                emailDisplayName = etc1.getReplyDisplayName();
            } else if ((etc1 = emailToTicketConfigService.getByGroup(ticket.getGroup())) != null) {
                emailFromAddress = etc1.getReplyEmailAddress();
                emailDisplayName = etc1.getReplyDisplayName();
            } else {
                emailFromAddress = mc.getEmailFromAddress();
                emailDisplayName = mc.getEmailDisplayName();
            }

            sendLinkToUser = mc.getSendLinkOnNotification();

            if (ticket.getNotifyTech() == null) {
                ticket.setNotifyTech(mc.getNotifyTechnician());
            }

            if (ticket.getNotifyUser() == null) {
                ticket.setNotifyUser(mc.getNotifyUser());
            }

            mailService.notifyParties(user, htmlMessage, txtMessage, ticket, emailFromAddress, emailDisplayName,
                    sendLinkToUser);
        }
    }

    private boolean ticketEqGrouping(User user, Ticket ticket, String grouping) {
        boolean retVal = false;
        if (grouping.equals("assignedToMe") && ticket.getAssignedTo().getUserRole() != null) {
            retVal = ticket.getAssignedTo().getUserRole().getUser().getId().equals(user.getId());
        }
        if (grouping.equals("ticketPool") && ticket.getAssignedTo().getUserRole() == null) {
            retVal = true;
        }
        if (grouping.equals("allGroupTickets")) {
            retVal = true;
        }
        return retVal;
    }

    public String createHTMLComments(Ticket newTicket, Ticket oldTicket) {

        // Get default locale
        Locale locale = LocaleContextHolder.getLocale();
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, locale);

        // Check to see if this ticket has a locale definition
        EmailToTicketConfig emailToTicketConfig = newTicket.getEmailToTicketConfig();

        if (emailToTicketConfig != null) {
            String locString = emailToTicketConfig.getLocaleString();
            if (StringUtils.isNotBlank(locString)) locale = LocaleUtils.toLocale(locString);
        }

        MessageSourceAccessor msa = getMessageSourceAccessor();
        StringBuilder text = new StringBuilder();
        if (oldTicket != null) {

            // contact
            if (!newTicket.getContact().getId().equals(oldTicket.getContact().getId())) {
                String newContact = newTicket.getContact().getFirstName() + " " + newTicket.getContact().getLastName();
                String oldContact = oldTicket.getContact().getFirstName() + " " + oldTicket.getContact().getLastName();

                text.append(msa.getMessage("ticket.contact", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{oldContact, newContact}, locale));
                text.append("<br/>");
            }

            // location
            if (!newTicket.getLocation().getId().equals(oldTicket.getLocation().getId())) {
                text.append(msa.getMessage("ticket.location", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{oldTicket.getLocation().getName(),
                        newTicket.getLocation().getName()}));
                text.append("<br/>");
            }

            // group
            if (!newTicket.getGroup().getId().equals(oldTicket.getGroup().getId())) {
                text.append(msa.getMessage("ticket.group", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{oldTicket.getGroup().getName(),
                        newTicket.getGroup().getName()}, locale));
                text.append("<br/>");
            }

            // category
            if (oldTicket.getCategory() == null && newTicket.getCategory() == null) {
                // do nothing
            } else if (oldTicket.getCategory() == null && newTicket.getCategory() != null) {
                text.append(msa.getMessage("ticket.category", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{"", newTicket.getCategory().getName()}, locale));
                text.append("<br/>");
            } else if (oldTicket.getCategory() != null && newTicket.getCategory() == null) {
                text.append(msa.getMessage("ticket.category", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{oldTicket.getCategory().getName(), ""}, locale));
                text.append("<br/>");
            } else if (!oldTicket.getCategory().getId().equals(newTicket.getCategory().getId())) {
                text.append(msa.getMessage("ticket.category", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{oldTicket.getCategory().getName(), newTicket.getCategory().getName()}, locale));
                text.append("<br/>");
            }

            // category option
            if (oldTicket.getCategoryOption() == null && newTicket.getCategoryOption() == null) {
                //do nothing
            } else if (oldTicket.getCategoryOption() == null && newTicket.getCategoryOption() != null) {
                text.append(msa.getMessage("ticket.categoryOption", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{"", newTicket.getCategoryOption().getName()}, locale));
                text.append("<br/>");
            } else if (oldTicket.getCategoryOption() != null && newTicket.getCategoryOption() == null) {
                text.append(msa.getMessage("ticket.categoryOption", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{oldTicket.getCategoryOption().getName(), ""}, locale));
                text.append("<br/>");
            } else if (!oldTicket.getCategoryOption().getId().equals(newTicket.getCategoryOption().getId())) {
                text.append(msa.getMessage("ticket.categoryOption", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{oldTicket.getCategoryOption().getName(), newTicket.getCategoryOption().getName()}, locale));
                text.append("<br/>");
            }

            // assigned to
            if (oldTicket.getAssignedTo() == null && newTicket.getAssignedTo() == null) {
                // do nothing
            } else if (oldTicket.getAssignedTo() == null && newTicket.getAssignedTo() != null) {
                text.append(msa.getMessage("ticket.assignedTo", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{"",
                        getAssignedToName(newTicket.getAssignedTo())}, locale));
                text.append("<br/>");
            } else if (oldTicket.getAssignedTo() != null && newTicket.getAssignedTo() == null) {
                text.append(msa.getMessage("ticket.assignedTo", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{
                        getAssignedToName(oldTicket.getAssignedTo()), ""}, locale));
                text.append("<br/>");
            } else if (!newTicket.getAssignedTo().getId().equals(oldTicket.getAssignedTo().getId())) {
                text.append(msa.getMessage("ticket.assignedTo", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{
                        getAssignedToName(oldTicket.getAssignedTo()), getAssignedToName(newTicket.getAssignedTo())},
                        locale));
                text.append("<br/>");
            }

            // priority
            if (!newTicket.getPriority().getId().equals(oldTicket.getPriority().getId())) {
                text.append(msa.getMessage("ticket.priority", locale)).append(": ");
                String oldPriority = oldTicket.getPriority().getName();
                String newPriority = newTicket.getPriority().getName();
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{oldPriority, newPriority}, locale));
                text.append("<br/>");
            }

            // status
            if (!newTicket.getStatus().getId().equals(oldTicket.getStatus().getId())) {
                text.append(msa.getMessage("ticket.status", locale)).append(": ");
                String oldStatus = oldTicket.getStatus().getName();
                String newStatus = newTicket.getStatus().getName();
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{oldStatus, newStatus}, locale));
                text.append("<br/>");
            }

            // estimated date
            if (oldTicket.getEstimatedDate() == null && newTicket.getEstimatedDate() == null) {
                // do nothing
            } else if (oldTicket.getEstimatedDate() == null && newTicket.getEstimatedDate() != null) {
                text.append(msa.getMessage("ticket.estimatedDate", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{
                        "", dateFormat.format(newTicket.getEstimatedDate())
                }, locale));
                text.append("<br/>");
            } else if (oldTicket.getEstimatedDate() != null && newTicket.getEstimatedDate() == null) {
                text.append(msa.getMessage("ticket.estimatedDate", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{
                        dateFormat.format(oldTicket.getEstimatedDate()), ""
                }, locale));
                text.append("<br/>");
            } else if (!newTicket.getEstimatedDate().equals(oldTicket.getEstimatedDate())) {
                text.append(msa.getMessage("ticket.estimatedDate", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{
                        dateFormat.format(oldTicket.getEstimatedDate()), dateFormat.format(newTicket.getEstimatedDate())
                }, locale));
                text.append("<br/>");
            }

            // subject
            if (!newTicket.getSubject().equals(oldTicket.getSubject())) {
                text.append(msa.getMessage("ticket.oldSubject", locale)).append(": ");
                text.append(oldTicket.getSubject());
                text.append("<br/>");
            }

            // note
            if (!StringUtils.equals(newTicket.getNote(), oldTicket.getNote())) {
                text.append(msa.getMessage("ticket.oldNotes", locale)).append(": ");
                text.append(RegexUtils.replaceNewLinesWithBRTags(oldTicket.getNote()));
                text.append("<br/>");
            }

            // custom fields
            Set<CustomFieldValues> ticketCFVs = newTicket.getCustomeFieldValues();
            Set<CustomFieldValues> oldTicketCFVs = oldTicket.getCustomeFieldValues();
            for (CustomFieldValues ticketCFV : ticketCFVs) {
                // Find the oldTicket cfv that matches this one
                for (CustomFieldValues oldTicketCFV : oldTicketCFVs) {
                    if (ticketCFV.getCustomField().getId().equals(oldTicketCFV.getCustomField().getId())) {
                        // Now check for the change in the fieldValue
                        String ticketValue = ticketCFV.getFieldValue();
                        String oldTicketValue = oldTicketCFV.getFieldValue();
                        String auditProperty = ticketCFV.getCustomField().getName();

                        if (!StringUtils.equals(ticketValue, oldTicketValue)) {

                            text.append(auditProperty).append(": ");
                            text.append(msa.getMessage("ticket.valuechanged", new Object[]{oldTicketValue, ticketValue}, locale));
                            text.append("<br/>");
                        }
                    }
                }
            }

        } else {
            // no old ticket
            text.append("<p>").append(msa.getMessage("ticket.newTicket", locale)).append("</p>");
            text.append("<p><strong>").append(msa.getMessage("ticket.subject", locale)).append(":</strong> ");
            text.append(newTicket.getSubject());
            text.append(" </p>");
            if (StringUtils.isNotBlank(newTicket.getNote())) {
                text.append(RegexUtils.replaceNewLinesWithBRTags(newTicket.getNote()));
            }

            text.append("<p><strong>").append(msa.getMessage("ticket.commentsby", locale)).append(":</strong> ");
            text.append(newTicket.getSubmittedBy().getFirstName());
            text.append(" ");
            text.append(newTicket.getSubmittedBy().getLastName());
            text.append("</p>");

            int size = newTicket.getAttachments() == null ? 0 : newTicket.getAttachments().size();
            text.append("<p><small><i>");
            if (size == 0) {
                text.append(msa.getMessage("ticket.comment.noattachments", locale));
            } else if (size == 1) {
                text.append(msa.getMessage("ticket.comment.oneattachment", locale));
            } else {
                text.append(msa.getMessage("ticket.comment.multipleattachments", new Object[]{size}, locale));
            }

            text.append("</i></small></p>");
        }

        if (StringUtils.isNotBlank(text.toString())) {
            text.insert(0, msa.getMessage("ticket.ticketChanges", locale) + ":<br/>");
        }

        return text.toString();
    }

    public String createTextComments(Ticket newTicket, Ticket oldTicket) {

        // Get default locale
        Locale locale = LocaleContextHolder.getLocale();
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, locale);

        // Check to see if this ticket has a locale definition
        EmailToTicketConfig emailToTicketConfig = newTicket.getEmailToTicketConfig();
        if (emailToTicketConfig != null) {
            String locString = emailToTicketConfig.getLocaleString();
            if (StringUtils.isNotBlank(locString)) locale = LocaleUtils.toLocale(locString);
        }

        MessageSourceAccessor msa = getMessageSourceAccessor();
        StringBuilder text = new StringBuilder();
        if (oldTicket != null) {
            // contact
            if (!newTicket.getContact().getId().equals(oldTicket.getContact().getId())) {
                String newContact = newTicket.getContact().getFirstName() + " " + newTicket.getContact().getLastName();
                String oldContact = oldTicket.getContact().getFirstName() + " " + oldTicket.getContact().getLastName();

                text.append(msa.getMessage("ticket.contact", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{oldContact, newContact}, locale));
                text.append("\n");
            }

            // location
            if (!newTicket.getLocation().getId().equals(oldTicket.getLocation().getId())) {
                text.append(msa.getMessage("ticket.location", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{oldTicket.getLocation().getName(),
                        newTicket.getLocation().getName()}, locale));
                text.append("\n");
            }

            // group
            if (!newTicket.getGroup().getId().equals(oldTicket.getGroup().getId())) {
                text.append(msa.getMessage("ticket.group", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{oldTicket.getGroup().getName(),
                        newTicket.getGroup().getName()}, locale));
                text.append("\n");
            }

            // category
            if(oldTicket.getCategory() == null && newTicket.getCategory() == null){
                // do nothing
            } else if(oldTicket.getCategory() == null && newTicket.getCategory() != null){
                text.append(msa.getMessage("ticket.category", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{"null", newTicket.getCategory().getName()}, locale));
                text.append("\n");
            } else if(oldTicket.getCategory() != null && newTicket.getCategory() == null){
                text.append(msa.getMessage("ticket.category", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{oldTicket.getCategory().getName(), "null"}, locale));
                text.append("\n");
            } else if(!oldTicket.getCategory().getId().equals(newTicket.getCategory().getId())){
                text.append(msa.getMessage("ticket.category", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{oldTicket.getCategory().getName(), newTicket.getCategory().getName()}, locale));
                text.append("\n");
            }

            // category option
            if(oldTicket.getCategoryOption() == null && newTicket.getCategoryOption() == null){
                // do nothing
            } else if(oldTicket.getCategoryOption() == null && newTicket.getCategoryOption() != null){
                text.append(msa.getMessage("ticket.categoryOption", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{"null", newTicket.getCategoryOption().getName()}, locale));
                text.append("\n");
            } else if(oldTicket.getCategoryOption() != null && newTicket.getCategoryOption() == null){
                text.append(msa.getMessage("ticket.categoryOption", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{oldTicket.getCategoryOption().getName(), "null"}, locale));
                text.append("\n");
            } else if(!oldTicket.getCategoryOption().getId().equals(newTicket.getCategoryOption().getId())){
                text.append(msa.getMessage("ticket.categoryOption", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{oldTicket.getCategoryOption().getName(), newTicket.getCategoryOption().getName()}, locale));
                text.append("\n");
            }

            // assigned to
            if (oldTicket.getAssignedTo() == null && newTicket.getAssignedTo() == null) {
                // do nothing
            } else if (oldTicket.getAssignedTo() == null && newTicket.getAssignedTo() != null) {
                text.append(msa.getMessage("ticket.assignedTo", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{
                        "", getAssignedToName(newTicket.getAssignedTo())}, locale));
                text.append("\n");
            } else if (oldTicket.getAssignedTo() != null && newTicket.getAssignedTo() == null) {
                text.append(msa.getMessage("ticket.assignedTo", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{
                        getAssignedToName(oldTicket.getAssignedTo()), ""}, locale));
                text.append("\n");
            } else if (!newTicket.getAssignedTo().getId().equals(oldTicket.getAssignedTo().getId())) {
                text.append(msa.getMessage("ticket.assignedTo", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{
                        getAssignedToName(oldTicket.getAssignedTo()), getAssignedToName(newTicket.getAssignedTo())},
                        locale));
                text.append("\n");
            }

            // priority
            if (!newTicket.getPriority().getId().equals(oldTicket.getPriority().getId())) {

                text.append(msa.getMessage("ticket.priority", locale)).append(": ");
                String oldPriority = oldTicket.getPriority().getName();
                String newPriority = newTicket.getPriority().getName();
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{oldPriority, newPriority}, locale));
                text.append("\n");
            }

            // status
            if (!newTicket.getStatus().getId().equals(oldTicket.getStatus().getId())) {

                text.append(msa.getMessage("ticket.status", locale)).append(": ");
                String oldStatus = oldTicket.getStatus().getName();
                String newStatus = newTicket.getStatus().getName();
                text.append(msa.getMessage("ticket.valuechanged", new Object[]{oldStatus, newStatus}, locale));
                text.append("\n");
            }

            // estimated date
            if (oldTicket.getEstimatedDate() == null && newTicket.getEstimatedDate() == null) {
                // do nothing
            } else if (oldTicket.getEstimatedDate() == null && newTicket.getEstimatedDate() != null) {
                text.append(msa.getMessage("ticket.estimatedDate", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged",
                        new Object[]{"", dateFormat.format(newTicket.getEstimatedDate())}, locale));
                text.append("\n");
            } else if (oldTicket.getEstimatedDate() != null && newTicket.getEstimatedDate() == null) {
                text.append(msa.getMessage("ticket.estimatedDate", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged",
                        new Object[]{dateFormat.format(oldTicket.getEstimatedDate()), ""}, locale));
                text.append("\n");
            } else if (!newTicket.getEstimatedDate().equals(oldTicket.getEstimatedDate())) {
                text.append(msa.getMessage("ticket.estimatedDate", locale)).append(": ");
                text.append(msa.getMessage("ticket.valuechanged",
                        new Object[]{dateFormat.format(oldTicket.getEstimatedDate()),
                                dateFormat.format(newTicket.getEstimatedDate())}, locale));
                text.append("\n");
            }

            // subject
            if (!newTicket.getSubject().equals(oldTicket.getSubject())) {

                text.append(msa.getMessage("ticket.oldSubject", locale)).append(": ");
                text.append(oldTicket.getSubject());
                text.append("\n");
            }

            // note
            if (!StringUtils.equals(newTicket.getNote(), oldTicket.getNote())) {
                text.append(msa.getMessage("ticket.oldNotes", locale)).append(": ");
                text.append(oldTicket.getNote());
                text.append("\n");
            }

            // custom fields
            Set<CustomFieldValues> ticketCFVs = newTicket.getCustomeFieldValues();
            Set<CustomFieldValues> oldTicketCFVs = oldTicket.getCustomeFieldValues();
            for (CustomFieldValues ticketCFV : ticketCFVs) {
                // Find the oldTicket cfv that matches this one
                for (CustomFieldValues oldTicketCFV : oldTicketCFVs) {
                    if (ticketCFV.getCustomField().getId().equals(oldTicketCFV.getCustomField().getId())) {
                        // Now check for the change in the fieldValue
                        String ticketValue = ticketCFV.getFieldValue();
                        String oldTicketValue = oldTicketCFV.getFieldValue();
                        String auditProperty = ticketCFV.getCustomField().getName();

                        if (!StringUtils.equals(ticketValue, oldTicketValue)) {

                            text.append(auditProperty).append(": ");
                            text.append(msa.getMessage("ticket.valuechanged", new Object[]{oldTicketValue, ticketValue}, locale));
                            text.append("\n");
                        }
                    }
                }
            }

        } else {
            // no old ticket
            text.append(msa.getMessage("ticket.newTicket", locale)).append("\n");
            text.append(msa.getMessage("ticket.subject", locale)).append(": ");
            text.append(newTicket.getSubject());
            text.append(" \n");
            if (StringUtils.isNotBlank(newTicket.getNote())) {
                text.append(newTicket.getNote());
            }

            text.append(msa.getMessage("ticket.commentsby", locale)).append(": ");
            text.append(newTicket.getSubmittedBy().getFirstName());
            text.append(" ");
            text.append(newTicket.getSubmittedBy().getLastName());
            text.append(" \n");

            int size = newTicket.getAttachments() == null ? 0 : newTicket.getAttachments().size();
            if (size == 0) {
                text.append(msa.getMessage("ticket.comment.noattachments", locale));
            } else if (size == 1) {
                text.append(msa.getMessage("ticket.comment.oneattachment", locale));
            } else {
                text.append(msa.getMessage("ticket.comment.multipleattachments", new Object[]{size}, locale));
            }
        }

        if (StringUtils.isNotBlank(text.toString())) {
            text.insert(0, msa.getMessage("ticket.ticketChanges", locale) + ":\n");
        }

        return text.toString();
    }

    private String getAssignedToName(UserRoleGroup urg) {
        if (urg == null) return "";
        if (urg.getUserRole() != null) {
            return urg.getUserRole().getUser().getFirstName() + " " + urg.getUserRole().getUser().getLastName();
        } else {
            return urg.getGroup().getName();
        }
    }

    /**
     * This should only be used on ticketFromController after setting up the custom field
     * on the onSubmit function
     *
     * @param ticket the ticket
     */
    public void saveTicketCustomFields(Ticket ticket) {
        ticketDao.saveServiceTicket(ticket);
    }

    public List<Ticket> getTicketsByCriteria(Group group, Set<Status> statuses, Set<TicketPriority> priorities,
                                             Category category, CategoryOption categoryOption) {
        return ticketDao.getTicketsByCriteria(group, statuses, priorities, category, categoryOption);
    }

    public List<Ticket> getTicketsAssignedToByUserRoleGroup(UserRoleGroup userRoleGroup, Boolean showClosed) {
        return ticketDao.getTicketsAssignedToByUserRoleGroup(userRoleGroup, showClosed);
    }

    public List<Ticket> getAllGroupTicketsByGroupId(Integer groupId, Boolean showClosed) {
        return ticketDao.getAllGroupTicketsByGroupId(groupId, showClosed);
    }

    public void saveTickets(Collection<Ticket> ticketList, User user) {
        for (Ticket ticket : ticketList) {
            saveTicket(ticket, user);
        }
    }

    public List<Ticket> getTicketsByEmailToTicketConfig(EmailToTicketConfig emailToTicketConfig) {
        return ticketDao.getTicketsByEmailToTicketConfig(emailToTicketConfig);
    }

    /**
     * Converts the ids that may be in the from and to to the appropriate object name
     *
     * @param ticket the command object
     */
    public void initializeAuditList(Ticket ticket) {

        for (TicketAudit ticketAudit : ticket.getTicketAudit()) {
            if (ticketAudit.getProperty().equals(TicketAudit.PROPERTY_CONTACT)) {
                if (StringUtils.isNotBlank(ticketAudit.getBeforeValue())) {
                    User beforeUser = userService.getUserById(Integer.valueOf(ticketAudit.getBeforeValue()));
                    ticketAudit.setBeforeValueObject(beforeUser);
                    ticketAudit.setBeforeValueDescription(beforeUser.getLoginId());
                }
                if (StringUtils.isNotBlank(ticketAudit.getAfterValue())) {
                    User afterUser = userService.getUserById(Integer.valueOf(ticketAudit.getAfterValue()));
                    ticketAudit.setAfterValueObject(afterUser);
                    ticketAudit.setAfterValueDescription(afterUser.getLoginId());
                }
            } else if (ticketAudit.getProperty().equals(TicketAudit.PROPERTY_LOCATION)) {
                if (StringUtils.isNotBlank(ticketAudit.getBeforeValue())) {
                    Location beforeLoc = locationService.getLocationById(Integer.valueOf(ticketAudit.getBeforeValue()));
                    ticketAudit.setBeforeValueObject(beforeLoc);
                    ticketAudit.setBeforeValueDescription(beforeLoc.getName());
                }
                if (StringUtils.isNotBlank(ticketAudit.getAfterValue())) {
                    Location afterLoc = locationService.getLocationById(Integer.valueOf(ticketAudit.getAfterValue()));
                    ticketAudit.setAfterValueObject(afterLoc);
                    ticketAudit.setAfterValueDescription(afterLoc.getName());
                }
            } else if (ticketAudit.getProperty().equals(TicketAudit.PROPERTY_GROUP)) {
                if (StringUtils.isNotBlank(ticketAudit.getBeforeValue())) {
                    Group beforeGroup = groupService.getGroupById(Integer.valueOf(ticketAudit.getBeforeValue()));
                    ticketAudit.setBeforeValueObject(beforeGroup);
                    ticketAudit.setBeforeValueDescription(beforeGroup.getName());
                }
                if (StringUtils.isNotBlank(ticketAudit.getAfterValue())) {
                    Group afterGroup = groupService.getGroupById(Integer.valueOf(ticketAudit.getAfterValue()));
                    ticketAudit.setAfterValueObject(afterGroup);
                    ticketAudit.setAfterValueDescription(afterGroup.getName());
                }
            } else if (ticketAudit.getProperty().equals(TicketAudit.PROPERTY_CATEGORY)) {
                if (StringUtils.isNotBlank(ticketAudit.getBeforeValue())) {
                    Category beforeCat = categoryService.getCategoryById(Integer.valueOf(ticketAudit.getBeforeValue()));
                    ticketAudit.setBeforeValueObject(beforeCat);
                    ticketAudit.setBeforeValueDescription(beforeCat.getName());
                }
                if (StringUtils.isNotBlank(ticketAudit.getAfterValue())) {
                    Category afterCat = categoryService.getCategoryById(Integer.valueOf(ticketAudit.getAfterValue()));
                    ticketAudit.setAfterValueObject(afterCat);
                    ticketAudit.setAfterValueDescription(afterCat.getName());
                }
            } else if (ticketAudit.getProperty().equals(TicketAudit.PROPERTY_CATEGORYOPTION)) {
                if (StringUtils.isNotBlank(ticketAudit.getBeforeValue())) {
                    CategoryOption beforeCatOpt = categoryOptionService.getCategoryOptionById(Integer.valueOf(ticketAudit.getBeforeValue()));
                    ticketAudit.setBeforeValueObject(beforeCatOpt);
                    ticketAudit.setBeforeValueDescription(beforeCatOpt.getName());
                }
                if (StringUtils.isNotBlank(ticketAudit.getAfterValue())) {
                    CategoryOption afterCatOpt = categoryOptionService.getCategoryOptionById(Integer.valueOf(ticketAudit.getAfterValue()));
                    ticketAudit.setAfterValueObject(afterCatOpt);
                    ticketAudit.setAfterValueDescription(afterCatOpt.getName());
                }
            } else if (ticketAudit.getProperty().equals(TicketAudit.PROPERTY_ASSIGNEDTO)) {
                if (StringUtils.isNotBlank(ticketAudit.getBeforeValue())) {
                    UserRoleGroup beforeUrg = userRoleGroupService.getUserRoleGroupById(Integer.valueOf(ticketAudit.getBeforeValue()));
                    ticketAudit.setBeforeValueObject(beforeUrg);
                    ticketAudit.setBeforeValueDescription((beforeUrg.getUserRole() == null) ? getMessageSourceAccessor().getMessage("ticketList.ticketPool") : beforeUrg.getUserRole().getUser().getLoginId());
                }
                if (StringUtils.isNotBlank(ticketAudit.getAfterValue())) {
                    UserRoleGroup afterUrg = userRoleGroupService.getUserRoleGroupById(Integer.valueOf(ticketAudit.getAfterValue()));
                    ticketAudit.setAfterValueObject(afterUrg);
                    ticketAudit.setAfterValueDescription((afterUrg.getUserRole() == null) ? getMessageSourceAccessor().getMessage("ticketList.ticketPool") : afterUrg.getUserRole().getUser().getLoginId());
                }
            } else if (ticketAudit.getProperty().equals(TicketAudit.PROPERTY_TICKETPRIORITY)) {
                if (StringUtils.isNotBlank(ticketAudit.getBeforeValue())) {
                    TicketPriority beforePriority = ticketPriorityService.getPriorityById(Integer.valueOf(ticketAudit.getBeforeValue()));
                    ticketAudit.setBeforeValueObject(beforePriority);
                    ticketAudit.setBeforeValueDescription(beforePriority.getName());
                }
                if (StringUtils.isNotBlank(ticketAudit.getAfterValue())) {
                    TicketPriority afterPriority = ticketPriorityService.getPriorityById(Integer.valueOf(ticketAudit.getAfterValue()));
                    ticketAudit.setAfterValueObject(afterPriority);
                    ticketAudit.setAfterValueDescription(afterPriority.getName());
                }
            } else if (ticketAudit.getProperty().equals(TicketAudit.PROPERTY_STATUS)) {
                if (StringUtils.isNotBlank(ticketAudit.getBeforeValue())) {
                    Status beforeStatus = statusService.getStatusById(Integer.valueOf(ticketAudit.getBeforeValue()));
                    ticketAudit.setBeforeValueObject(beforeStatus);
                    ticketAudit.setBeforeValueDescription(beforeStatus.getName());
                }
                if (StringUtils.isNotBlank(ticketAudit.getAfterValue())) {
                    Status afterStatus = statusService.getStatusById(Integer.valueOf(ticketAudit.getAfterValue()));
                    ticketAudit.setAfterValueObject(afterStatus);
                    ticketAudit.setAfterValueDescription(afterStatus.getName());
                }
            } else if (ticketAudit.getProperty().equals(TicketAudit.PROPERTY_SUBMITTEDBY)) {
                if (StringUtils.isNotBlank(ticketAudit.getBeforeValue())) {
                    User beforeUser = userService.getUserById(Integer.valueOf(ticketAudit.getBeforeValue()));
                    ticketAudit.setBeforeValueObject(beforeUser);
                    ticketAudit.setBeforeValueDescription(beforeUser.getLoginId());
                }
                if (StringUtils.isNotBlank(ticketAudit.getAfterValue())) {
                    User afterUser = userService.getUserById(Integer.valueOf(ticketAudit.getAfterValue()));
                    ticketAudit.setAfterValueObject(afterUser);
                    ticketAudit.setAfterValueDescription(afterUser.getLoginId());
                }
            } else if (ticketAudit.getProperty().equals(TicketAudit.PROPERTY_ESTIMATEDDATE)) {
                ticketAudit.setBeforeValueObject(ticketAudit.getBeforeValue());
                ticketAudit.setBeforeValueDescription(ticketAudit.getBeforeValue());
                ticketAudit.setAfterValueObject(ticketAudit.getAfterValue());
                ticketAudit.setAfterValueDescription(ticketAudit.getAfterValue());
            } else if (ticketAudit.getProperty().equals(TicketAudit.PROPERTY_ASSET)) {
                if (StringUtils.isNotBlank(ticketAudit.getBeforeValue())) {
                    Asset beforeAsset = assetService.getAssetById(Integer.valueOf(ticketAudit.getBeforeValue()));
                    ticketAudit.setBeforeValueObject(beforeAsset);
                    ticketAudit.setBeforeValueDescription("# " + beforeAsset.getAssetNumber());
                }
                if (StringUtils.isNotBlank(ticketAudit.getAfterValue())) {
                    Asset afterAsset = assetService.getAssetById(Integer.valueOf(ticketAudit.getAfterValue()));
                    ticketAudit.setAfterValueObject(afterAsset);
                    ticketAudit.setAfterValueDescription("# " + afterAsset.getAssetNumber());
                }
            } else if (ticketAudit.getProperty().equals(TicketAudit.PROPERTY_SUBJECT)) {
                ticketAudit.setBeforeValueObject(ticketAudit.getBeforeText());
                ticketAudit.setBeforeValueDescription(ticketAudit.getBeforeText());
                ticketAudit.setAfterValueObject(ticketAudit.getAfterText());
                ticketAudit.setAfterValueDescription(ticketAudit.getAfterText());
            } else if (ticketAudit.getProperty().equals(TicketAudit.PROPERTY_NOTE)) {
                ticketAudit.setBeforeValueObject(ticketAudit.getBeforeText());
                ticketAudit.setBeforeValueDescription(ticketAudit.getBeforeText());
                ticketAudit.setAfterValueObject(ticketAudit.getAfterText());
                ticketAudit.setAfterValueDescription(ticketAudit.getAfterText());
            } else if (ticketAudit.getProperty().equals(TicketAudit.PROPERTY_WORKTIME)) {
                if (StringUtils.isNotBlank(ticketAudit.getBeforeValue())) {
                    ticketAudit.setBeforeValueObject(convertSecondsToHMS(ticketAudit.getBeforeValue()));
                    ticketAudit.setBeforeValueDescription(convertSecondsToHMS(ticketAudit.getBeforeValue()));
                }
                if (StringUtils.isNotBlank(ticketAudit.getAfterValue())) {
                    ticketAudit.setAfterValueObject(convertSecondsToHMS(ticketAudit.getAfterValue()));
                    ticketAudit.setAfterValueDescription(convertSecondsToHMS(ticketAudit.getAfterValue()));
                }
            } else { // Assume that this is a custom field
                ticketAudit.setBeforeValueObject(ticketAudit.getBeforeText());
                ticketAudit.setBeforeValueDescription(ticketAudit.getBeforeText());
                ticketAudit.setAfterValueObject(ticketAudit.getAfterText());
                ticketAudit.setAfterValueDescription(ticketAudit.getAfterText());
            }
        }
    }

    public int getCountByCategory(Category category) {
        return ticketDao.getCountByCategory(category);
    }

    public int getCountByCategoryOption(CategoryOption categoryOption) {
        return ticketDao.getCountByCategoryOption(categoryOption);
    }

    public void evict(Ticket ticket) {
        ticketDao.evict(ticket);
    }

    private String convertSecondsToHMS(String totalSeconds) {
        int time = Integer.parseInt(totalSeconds);
        String secString;
        String minString;
        String hrsString;

        int sec = time % 60;
        secString = sec + "";
        if (sec < 10) {
            secString = "0" + sec;
        }
        int min = (time / 60) % 60;
        minString = min + "";
        if (min < 10) {
            minString = "0" + min;
        }
        int hrs = time / 60 / 60;
        hrsString = hrs + "";
        if (hrs < 10) {
            hrsString = "0" + hrs;
        }
        return hrsString + ":" + minString + ":" + secString;
    }

    public void setTicketDao(TicketDao ticketDao) {
        this.ticketDao = ticketDao;
    }

    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }

    public void setEmailToTicketConfigService(EmailToTicketConfigService emailToTicketConfigService) {
        this.emailToTicketConfigService = emailToTicketConfigService;
    }

    public void setStatusService(StatusService statusService) {
        this.statusService = statusService;
    }

    public void setSurveyService(SurveyService surveyService) {
        this.surveyService = surveyService;
    }

    public void setSurveyDataService(SurveyDataService surveyDataService) {
        this.surveyDataService = surveyDataService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setCategoryOptionService(CategoryOptionService categoryOptionService) {
        this.categoryOptionService = categoryOptionService;
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setTicketPriorityService(TicketPriorityService ticketPriorityService) {
        this.ticketPriorityService = ticketPriorityService;
    }

    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }
}
