package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.AssignmentDao;
import net.grouplink.ehelpdesk.dao.CategoryDao;
import net.grouplink.ehelpdesk.dao.GroupDao;
import net.grouplink.ehelpdesk.dao.UserRoleGroupDao;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.common.utils.TreeNode;
import net.grouplink.ehelpdesk.service.asset.AssetService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author mrollins
 * @version 1.0
 */
public class CategoryServiceImpl implements CategoryService {

    private Log log = LogFactory.getLog(getClass());

    private CategoryDao categoryDao;
    private GroupDao groupDao;
    private AssignmentDao assignmentDao;
    private List treeList;
    private int index;
    private UserRoleGroupDao userRoleGroupDao;
    private TicketService ticketService;
    private ScheduleStatusService scheduleStatusService;
    private TicketTemplateService ticketTemplateService;
    private AssetService assetService;


    public List<Category> getCategories() {
        return categoryDao.getCategories();
    }

    public Category getCategoryById(Integer id) {
        return categoryDao.getCategoryById(id);
    }

    public Category getCategoryByName(String name) {
        return categoryDao.getCategoryByName(name);
    }

    public boolean isDuplicateCategory(Category cat) {
        return categoryDao.isDuplicateCategory(cat);
    }

    public void saveCategory(Category category) {
        categoryDao.saveCategory(category);
    }

    public void deleteCategory(Category category) {
        categoryDao.deleteCategory(category);
    }

    public void setCategoryDao(CategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }

    @SuppressWarnings("unchecked")
    public List<TreeNode> getCategoryTreeByGroupId(Integer groupId) {
        treeList = new ArrayList<TreeNode>();
        Group group = this.groupDao.getGroupById(groupId);
        List<Category> catList = getCategoryByGroupId(groupId);
        Collections.sort(catList);
        this.index = 0;
        // TODO: move this view layer specific logic out of service
        treeList.add(new TreeNode(index, -1, group.getName() + " - Category List", "showLocList(\'groupId=" + groupId + "\')", "/images/ticketSettings/location.gif", "/images/ticketSettings/location.gif"));
        this.index++;
        for (Category aCatList : catList) {
            treeList.add(new TreeNode(this.index, 0, aCatList.getName(), "modifyCategory(\'" + aCatList.getId() + "\');showLocList(\'catId=" + aCatList.getId() + "&groupId=" + groupId + "\')", "/images/ticketSettings/category.gif", "/images/ticketSettings/category.gif"));
            int parent = index;
            this.index++;
            addChildren(parent, aCatList);
        }
        return treeList;
    }

    @SuppressWarnings("unchecked")
    private void addChildren(int parent, Category cat) {
        List<CategoryOption> catOptList = new ArrayList<CategoryOption>(cat.getCategoryOptions());
        Collections.sort(catOptList);
        for (CategoryOption aCatOptList : catOptList) {
            if (aCatOptList.isActive()) {
                // TODO: move this view layer specific logic out of service
                treeList.add(new TreeNode(this.index, parent, aCatOptList.getName(), "modifyCategoryOption(\'" + aCatOptList.getId() + "\');showLocList(\'groupId=" + cat.getGroup().getId() + "&catOptId=" + aCatOptList.getId() + "\')", "/images/ticketSettings/categoryOption.gif", "/images/ticketSettings/categoryOption.gif"));
                index++;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public List<TreeNode> getCategoryTreeByUrgId(Integer urgId) {
        treeList = new ArrayList<TreeNode>();
        UserRoleGroup urg = this.userRoleGroupDao.getUserRoleGroupById(urgId);
        List<Category> grpCatList = this.assignmentDao.getCategoryListAssignedToUserByUrgId(urgId);
        Collections.sort(grpCatList);
        this.index = 0;
        if (urg.getUserRole() == null) {
            // TODO: move this view layer specific logic out of service
            treeList.add(new TreeNode(index, -1, "Ticket Pool - Category List", "showLocList(\'urgId=" + urgId + "\')", "/images/ticketSettings/technician.gif", "/images/ticketSettings/technician.gif"));
        } else {
            treeList.add(new TreeNode(index, -1, urg.getUserRole().getUser().getLastName() + ", " + urg.getUserRole().getUser().getFirstName() + " - Category List", "showLocList(\'urgId=" + urgId + "\')", "/images/ticketSettings/technician.gif", "/images/ticketSettings/technician.gif"));
        }
        this.index++;
        for (Category aGrpCatList : grpCatList) {
            if (aGrpCatList.isActive()) {
                treeList.add(new TreeNode(this.index, 0, aGrpCatList.getName(), "showLocList(\'urgId=" + urgId + "&catId=" + aGrpCatList.getId() + "\')", "/images/ticketSettings/category.gif", "/images/ticketSettings/category.gif"));
                int parent = index;
                this.index++;
                addChildren(parent, urgId, aGrpCatList);
            }
        }
        return treeList;
    }

    public void flush() {
        this.categoryDao.flush();
    }

    public Category getCategoryByNameAndGroupId(String categoryName, Integer groupId) {
        return this.categoryDao.getCategoryByNameAndGroupId(categoryName, groupId);
    }

    public boolean deleteCategoryIfUnused(Category category) {
        boolean retVal;
        if(categoryCanBeDeleted(category)){
            try{
                categoryDao.justDeleteCategory(category);
                categoryDao.flush();
                retVal = true;
            } catch(DataIntegrityViolationException e){
                retVal = false;
            }
        } else {
            retVal = false;
        }

        if(!retVal){
            log.info("Unable to delete category: '" + category.getName() + "'. Setting as inactive");
            category.setActive(false);
            saveCategory(category);
        }

        return retVal;
    }

    public boolean categoryCanBeDeleted(Category category){
        if(ticketService.getCountByCategory(category) > 0) return false;
        if (scheduleStatusService.getCountByCategory(category) > 0) return false;
        if (ticketTemplateService.getCountByCategory(category) > 0) return false;
        if (assetService.getCountByCategory(category) > 0) return false;
        // Seems like Category isn't being used, so it can be deleted.
        return true;
    }

    public List<Category> getAllCategoriesByGroup(Group group) {
        return categoryDao.getAllCategoriesByGroup(group);
    }

    @SuppressWarnings("unchecked")
    private void addChildren(int parent, Integer urgId, Category cat) {
        List<CategoryOption> catOptList = this.assignmentDao.getCategoryOptionListAssignedToUserByUrgIdAndCategoryId(urgId, cat.getId());
        Collections.sort(catOptList);
        for (CategoryOption aCatOptList : catOptList) {
            if (aCatOptList.isActive()) {
                // TODO: move this view layer specific logic out of service
                treeList.add(new TreeNode(this.index, parent, aCatOptList.getName(), "showLocList(\'urgId=" + urgId + "&catOptId=" + aCatOptList.getId() + "\')", "/images/ticketSettings/categoryOption.gif", "/images/ticketSettings/categoryOption.gif"));
                index++;
            }
        }
    }

    public List<Category> getCategoryByGroupId(Integer id) {
        return this.categoryDao.getCategoryByGroupId(id);
    }

    public void setAssignmentDao(AssignmentDao techLocCatOptDao) {
        this.assignmentDao = techLocCatOptDao;
    }

    public void setGroupDao(GroupDao groupDao) {
        this.groupDao = groupDao;
    }

    public void setUserRoleGroupDao(UserRoleGroupDao userRoleGroupDao) {
        this.userRoleGroupDao = userRoleGroupDao;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setScheduleStatusService(ScheduleStatusService scheduleStatusService) {
        this.scheduleStatusService = scheduleStatusService;
    }

    public void setTicketTemplateService(TicketTemplateService ticketTemplateService) {
        this.ticketTemplateService = ticketTemplateService;
    }

    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }
}
