package net.grouplink.ehelpdesk.service.zen;

import net.grouplink.ehelpdesk.dao.zen.ZenSystemSettingDao;
import net.grouplink.ehelpdesk.domain.zen.ZenSystemSetting;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.lang.StringUtils;
import org.xml.sax.InputSource;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;

public class ZenSystemSettingServiceImpl implements ZenSystemSettingService {
    private Log log = LogFactory.getLog(getClass());
    
    private ZenSystemSettingDao zenSystemSettingDao;

    public ZenSystemSetting getByName(String name) {
        return zenSystemSettingDao.getByName(name);
    }

    public String getRemotePort() {
        String port = null;

        log.debug("getting zSystemSetting for RemoteManagement");
        ZenSystemSetting zss = getByName("RemoteManagement");
        log.debug("found zSystemSetting: " + (zss != null));
        if (zss != null) {
            String data = zss.getData();
            log.debug("zSystemSetting.data = " + data);

            if (StringUtils.isNotBlank(data)) {
                // strip byte order mark
                log.debug("stripping possible byte order mark");
                data = data.trim().replaceFirst("^([\\W]+)<", "<");

                // determine if data is xml or hex
                if (!data.startsWith("<")) {
                    // hex, convert to ascii
                    try {
                        data = new String(Hex.decodeHex(data.toCharArray()));
                    } catch (DecoderException e) {
                        log.error("error converting hex data", e);
                    }
                }
            }

            if (StringUtils.isNotBlank(data)) {
                XPath xpath = XPathFactory.newInstance().newXPath();
                try {
                    log.debug("evaluating xpath expression on data to determine port");
                    port = xpath.evaluate("/RMSettingsData/RemoteManagementService/RemoteControlService/Port",
                            new InputSource(new StringReader(data)));
                } catch (XPathExpressionException e) {
                    log.error("error getting zenworks remote settings port", e);
                }
            }
        }

        log.debug("returning port = " + port);
        return port;
    }

    public void setZenSystemSettingDao(ZenSystemSettingDao zenSystemSettingDao) {
        this.zenSystemSettingDao = zenSystemSettingDao;
    }
}
