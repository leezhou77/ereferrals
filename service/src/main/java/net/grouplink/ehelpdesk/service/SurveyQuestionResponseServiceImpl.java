package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.SurveyQuestionResponse;
import net.grouplink.ehelpdesk.dao.SurveyQuestionResponseDao;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Mar 24, 2008
 * Time: 5:33:26 PM
 */
public class SurveyQuestionResponseServiceImpl implements SurveyQuestionResponseService
{
    private SurveyQuestionResponseDao surveyQuestionResponseDao;

    public SurveyQuestionResponse getSurveyQuestionResponseById(Integer id) {
        return surveyQuestionResponseDao.getSurveyQuestionResponseById(id);
    }

    public List<SurveyQuestionResponse> getSurveyQuestionResponsesByQuestionId(Integer id) {
        return surveyQuestionResponseDao.getSurveyQuestionResponsesByQuestionId(id);
    }    

    public SurveyQuestionResponseDao getSurveyQuestionResponseDao() {
        return surveyQuestionResponseDao;
    }

    public void saveSurveyQuestionResponse(SurveyQuestionResponse surveyQuestionResponse) {
        surveyQuestionResponseDao.saveSurveyQuestionResponse(surveyQuestionResponse);
    }

    public void deleteSurveyQuestionResponse(SurveyQuestionResponse surveyQuestionResponse) {
        surveyQuestionResponseDao.deleteSurveyQuestionResponse(surveyQuestionResponse);
    }

    public boolean hasDuplicateSurveyQuestionResponseText(SurveyQuestionResponse surveyQuestionResponse) {
        return surveyQuestionResponseDao.hasDuplicateSurveyQuestionResponseText(surveyQuestionResponse);
    }

    public void flush() {
        surveyQuestionResponseDao.flush();
    }

    public void setSurveyQuestionResponseDao(SurveyQuestionResponseDao surveyQuestionResponseDao) {
        this.surveyQuestionResponseDao = surveyQuestionResponseDao;
    }




}
