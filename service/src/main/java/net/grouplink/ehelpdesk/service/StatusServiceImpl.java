package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.Status;
import net.grouplink.ehelpdesk.dao.StatusDao;

import java.util.List;

/**
 * @author mrollins
 * @version 1.0
 */
public class StatusServiceImpl implements StatusService{

    private StatusDao statusDao;

    public List<Status> getStatus() {
        return statusDao.getStatus();
    }

    public Status getStatusById(Integer id) {
        return statusDao.getStatusById(id);
    }

    public Status getStatusByName(String name) {
        return statusDao.getStatusByName(name);
    }

    public void saveStatus(Status status) {
        statusDao.saveStatus(status);
    }

    public void deleteStatus(Status status) {
        // don't delete status with following id's:
        // -2 = TT Pending
        // -1 = Deleted
        // 1 = Ticket Pool Status
        // 2 = Initial Status
        // 6 = Closed Status
        if (status.getId() <= 2 || status.getId() == 6) {
            return;
        }

        // just set status.active = false, don't actually delete status
        status.setActive(Boolean.FALSE);
        saveStatus(status);
    }

    public Status getInitialStatus() {
        return statusDao.getInitialStatus();
    }

    public Status getTicketPoolStatus() {
        return statusDao.getTicketPoolStatus();
    }

    public Status getClosedStatus() {
        return statusDao.getClosedStatus();
    }

    public List<Status> getAllStatuses() {
        return statusDao.getAllStatuses();
    }

    public List<Status> getCustomStatus() {
        return statusDao.getCustomStatus();
    }

    public List<Status> getNonClosedStatus() {
        return statusDao.getNonClosedStatus();
    }

    public void flush() {
        this.statusDao.flush();
    }

    public void init() {
        // TT Pending
        Status ttPending = getStatusById(-2);
        if(ttPending == null){
            ttPending = new Status(-2, "TT Pending", 0, true);
            saveOnly(ttPending);
        }
        // Deleted
        Status deleted = getStatusById(-1);
        if(deleted == null){
            deleted = new Status(-1, "Deleted", 0, true);
            saveOnly(deleted);
        }
        // Awaiting Dispatch
        Status awaitingDispatch = getStatusById(1);
        if(awaitingDispatch == null){
            awaitingDispatch = new Status(1, "Awaiting Dispatch", 0, true);
            saveOnly(awaitingDispatch);
        }
        // Assigned Not Updated
        Status assignedNotUpdated = getStatusById(2);
        if(assignedNotUpdated == null){
            assignedNotUpdated = new Status(2, "Assigned Not Updated", 0, true);
            saveOnly(assignedNotUpdated);
        }
        // Closed
        Status closed = getStatusById(6);
        if(closed == null){
            closed = new Status(6, "Closed", 0, true);
            saveOnly(closed);
        }
    }

    private void saveOnly(Status status) {
        statusDao.saveOnly(status);
    }

    public void setStatusDao(StatusDao statusDao) {
        this.statusDao = statusDao;
    }
}
