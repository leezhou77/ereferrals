package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.domain.asset.AccountingInfo;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.ImprovedAccountingInfo;
import org.springframework.beans.BeanUtils;

import java.util.List;

public class AccountingInfoMigrationProcessingServiceImpl implements AccountingInfoMigrationProcessingService {
    private AssetService assetService;

    public void convertAccountInfos(int batchSize) {
        List<AccountingInfo> accountingInfos = assetService.getAllAccountingInfo(0, batchSize);

        for (AccountingInfo accountingInfo : accountingInfos) {
            ImprovedAccountingInfo improvedAccountingInfo = new ImprovedAccountingInfo();

            // copy properties from old to new accountinginfo
            BeanUtils.copyProperties(accountingInfo, improvedAccountingInfo);

            // clear id
            improvedAccountingInfo.setId(null);

            Asset asset = assetService.getAssetById(accountingInfo.getId());

            if(asset != null) {
                // lookup asset with id of acct info and set improved asset to new
                asset.setAccountingInfo(improvedAccountingInfo);

                // save improved
                assetService.saveAsset(asset);
            }
        }

        // delete old accountinginfos
        assetService.deleteAccountingInfoList(accountingInfos);
    }

    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }
}
