package net.grouplink.ehelpdesk.service.mail;

import java.util.Properties;

/**
 * User: jaymehafen
 * Date: Jan 23, 2008
 * Time: 3:29:53 PM
 */
public class ImapMailMonitor extends AbstractMailMonitor {

    protected String getStoreProtocol() {
        return "imap";
    }

    protected void customizeSessionProperties(Properties props) {
        super.customizeSessionProperties(props);
        props.setProperty("mail.imap.partialfetch", "false");
        props.setProperty("mail.imap.connectiontimeout", "60000");
    }
}
