package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.CategoryOptionDao;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.CustomField;
import net.grouplink.ehelpdesk.service.asset.AssetService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author mrollins
 * @version 1.0
 */
public class CategoryOptionServiceImpl implements CategoryOptionService {

    private Log log = LogFactory.getLog(getClass());

    private CategoryOptionDao categoryOptionDao;
    private ScheduleStatusService scheduleStatusService;
    private TicketTemplateService ticketTemplateService;
    private AssetService assetService;
    private TicketService ticketService;
    private CustomFieldsService customFieldService;
    private SurveyService surveyService;
    private AssignmentService assignmentService;

    public List<CategoryOption> getCategoryOptions() {
        return categoryOptionDao.getCategoryOptions();
    }

    public CategoryOption getCategoryOptionById(Integer id) {
        return categoryOptionDao.getCategoryOptionById(id);
    }

    public CategoryOption getCategoryOptionByName(String name) {
        return categoryOptionDao.getCategoryOptionByName(name);
    }

    public void saveCategoryOption(CategoryOption catOption) {
        categoryOptionDao.saveCategoryOption(catOption);
    }

    public void deleteCategoryOption(CategoryOption catOption) {
        categoryOptionDao.deleteCategoryOption(catOption);
    }

    public CategoryOption getCategoryOptionByNameAndCategoryId(String catOptName, Category category) {
        return categoryOptionDao.getCategoryOptionByNameAndCategoryId(catOptName, category);
    }


    public CategoryOption getDuplicateCategoryOption(CategoryOption catOpt) {
        return categoryOptionDao.getDuplicateCategoryOption(catOpt);
    }

    public Set<CustomField> getCategoryOption(CategoryOption categoryOption, CustomField customField) {
        Set<CustomField> options = categoryOption.getCustomFields();
        if (options == null)
            options = new HashSet<CustomField>();
        options.add(customField);
        return options;
    }

    public List<CategoryOption> getCategoryOptionByCategoryId(Integer categoryId) {
        return this.categoryOptionDao.getCategoryOptionByCategoryId(categoryId);  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void flush() {
        this.categoryOptionDao.flush();
    }

    public CategoryOption getCategoryOptionByNameAndGroupId(String catOptName, Integer groupId) {
        return this.categoryOptionDao.getCategoryOptionByNameAndGroupId(catOptName, groupId);
    }

    public boolean deleteCategoryOptionIfUnused(CategoryOption categoryOption) {
        boolean retVal;
        if (categoryOptionCanBeDeleted(categoryOption)) {
            try {
                categoryOptionDao.justDeleteCategoryOption(categoryOption);
                categoryOptionDao.flush();
                retVal = true;
            } catch (DataIntegrityViolationException e) {
                retVal = false;
            }
        } else {
            retVal = false;
        }

        if (!retVal) {
            log.info("Unable to delete Category Option: '" + categoryOption.getName() + "'. Setting as inactive");
            categoryOption.setActive(false);
            saveCategoryOption(categoryOption);
        }

        return retVal;
    }

    public boolean categoryOptionCanBeDeleted(CategoryOption categoryOption) {
        if (customFieldService.getCountByCategoryOption(categoryOption) > 0) return false;
        if (scheduleStatusService.getCountByCategoryOption(categoryOption) > 0) return false;
        if (surveyService.getCountByCategoryOption(categoryOption) > 0) return false;
        if (assignmentService.getCountByCategoryOption(categoryOption) > 0) return false;
        if (ticketService.getCountByCategoryOption(categoryOption) > 0) return false;
        if (ticketTemplateService.getCountByCategoryOption(categoryOption) > 0) return false;
        if (assetService.getCountByCategoryOption(categoryOption) > 0) return false;
        // Seems like Category Option isn't being used, so it can be deleted
        return true;
    }

    public List<CategoryOption> getCategoryOptionByGroupId(Integer id) {
        return categoryOptionDao.getCategoryOptionByGroupId(id);
    }

    public void setCategoryOptionDao(CategoryOptionDao categoryOptionDao) {
        this.categoryOptionDao = categoryOptionDao;
    }

    public void setScheduleStatusService(ScheduleStatusService scheduleStatusService) {
        this.scheduleStatusService = scheduleStatusService;
    }

    public void setTicketTemplateService(TicketTemplateService ticketTemplateService) {
        this.ticketTemplateService = ticketTemplateService;
    }

    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setCustomFieldService(CustomFieldsService customFieldService) {
        this.customFieldService = customFieldService;
    }

    public void setSurveyService(SurveyService surveyService) {
        this.surveyService = surveyService;
    }

    public void setAssignmentService(AssignmentService assignmentService) {
        this.assignmentService = assignmentService;
    }
}
