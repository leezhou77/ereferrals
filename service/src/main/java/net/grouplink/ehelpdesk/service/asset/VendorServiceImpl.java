package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.dao.asset.VendorDao;
import net.grouplink.ehelpdesk.domain.asset.Vendor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * User: jaymehafen
 * Date: Jul 14, 2008
 * Time: 11:55:20 AM
 */
public class VendorServiceImpl implements VendorService {
    private VendorDao vendorDao;
    private AssetService assetService;
    private SoftwareLicenseService softwareLicenseService;

    public Vendor getById(Integer id) {
        return vendorDao.getById(id);
    }

    public Vendor getByName(String name) {
        return vendorDao.getByName(name);
    }

    public List<Vendor> getAllVendors() {
        return vendorDao.getAllVendors();
    }

    public List<Vendor> getUndeletableVendors() {
        List<Vendor> assetVendors = assetService.getUniqueVendors();
        List<Vendor> softLicVendors = softwareLicenseService.getUniqueVendors();
        Set<Vendor> distinctVendors = new HashSet<Vendor>(assetVendors);
        distinctVendors.addAll(softLicVendors);
        return new ArrayList<Vendor>(distinctVendors);
    }

    public void saveVendor(Vendor vendor) {
        vendorDao.saveVendor(vendor);
    }

    public void deleteVendor(Vendor vendor) {
        vendorDao.deleteVendor(vendor);
    }

    public void flush() {
        vendorDao.flush();
    }

    public void setVendorDao(VendorDao vendorDao) {
        this.vendorDao = vendorDao;
    }

    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }

    public void setSoftwareLicenseService(SoftwareLicenseService softwareLicenseService) {
        this.softwareLicenseService = softwareLicenseService;
    }
}
