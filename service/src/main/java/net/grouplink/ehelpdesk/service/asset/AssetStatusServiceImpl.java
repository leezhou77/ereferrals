package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.dao.asset.AssetStatusDao;
import net.grouplink.ehelpdesk.domain.asset.AssetStatus;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Jul 31, 2008
 * Time: 11:57:15 AM
 */
public class AssetStatusServiceImpl implements AssetStatusService {
    private AssetStatusDao assetStatusDao;
    private AssetService assetService;

    public AssetStatus getById(Integer assetStatusId) {
        return assetStatusDao.getById(assetStatusId);
    }

    public AssetStatus getByName(String name) {
        return assetStatusDao.getByName(name);
    }

    public List<AssetStatus> getAllAssetStatuses() {
        return assetStatusDao.getAllAssetStatuses();
    }

    public List<AssetStatus> getUndeletableStatus() {
        return assetService.getUniqueStatus();
    }

    public void saveAssetStatus(AssetStatus assetStatus) {
        assetStatusDao.saveAssetStatus(assetStatus);
    }

    public void deleteAssetStatus(AssetStatus assetStatus) {
        assetStatusDao.deleteAssetStatus(assetStatus);
    }

    public void flush() {
        assetStatusDao.flush();
    }

    public void setAssetStatusDao(AssetStatusDao assetStatusDao) {
        this.assetStatusDao = assetStatusDao;
    }

    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }
}
