package net.grouplink.ehelpdesk.domain.utils;

import net.grouplink.ehelpdesk.domain.CustomField;
import net.grouplink.ehelpdesk.domain.CustomFieldValues;

import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Oct 31, 2008
 * Time: 2:39:59 PM
 */
public class CustomFieldValuesComparator implements Comparator<CustomFieldValues> {

    public int compare(CustomFieldValues cfv1, CustomFieldValues cfv2) {
        CustomField cf1 = cfv1.getCustomField();
        CustomField cf2 = cfv2.getCustomField();

        int cf1Order = cf1.getOrder();
        int cf2Order = cf2.getOrder();

        if (cf1Order < cf2Order)
            return -1;
        else if (cf1Order > cf2Order)
            return 1;
        else {
            return cf1.getName().compareTo(cf2.getName());
        }

    }
}
