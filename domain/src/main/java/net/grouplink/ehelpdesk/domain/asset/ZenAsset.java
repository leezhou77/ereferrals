package net.grouplink.ehelpdesk.domain.asset;

import org.hibernate.annotations.GenericGenerator;
import org.apache.commons.codec.binary.Hex;

import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * User: jaymehafen
 * Date: Apr 3, 2009
 * Time: 12:11:10 PM
 */
@Entity
@Table(name = "ZENASSET")
public class ZenAsset implements Serializable {
    private Integer id;
    private byte[] workstationId;
    private byte[] deviceId;
    private String assetName;
    private String assetType;
    private String operatingSystem;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "WORKSTATIONID")
    public byte[] getWorkstationId() {
        return workstationId;
    }

    public void setWorkstationId(byte[] workstationId) {
        this.workstationId = workstationId;
    }

    @Column(name = "DEVICEID")
    public byte[] getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(byte[] deviceId) {
        this.deviceId = deviceId;
    }

    @Column(name = "ASSETNAME")
    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    @Column(name = "ASSETTYPE")
    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    @Column(name = "OPERATINGSYSTEM")
    public String getOperatingSystem() {
        return operatingSystem;
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem;
    }

    @Transient
    public String getDeviceIdString() {
        return String.valueOf(Hex.encodeHex(deviceId));
    }
}
