package net.grouplink.ehelpdesk.domain.asset;

import net.grouplink.ehelpdesk.domain.User;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.*;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * User: mark
 * Date: Sep 3, 2008
 * Time: 7:11:21 PM
 */
@Entity
@Table(name = "assetfilter")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AssetFilter implements Serializable, Cloneable {
    public static final String COLUMN_ASSETNUMBER = "asset.assetNumber";
    public static final String COLUMN_ASSETID = "asset.assetId";
    public static final String COLUMN_NAME = "asset.name";
    public static final String COLUMN_TYPE = "asset.type";
    public static final String COLUMN_OWNER = "asset.owner";
    public static final String COLUMN_VENDOR = "asset.vendor";
    public static final String COLUMN_ASSETLOCATION = "asset.assetlocation";
    public static final String COLUMN_ACQUISITIONDATE = "asset.acquisitionDate";
    public static final String COLUMN_LOCATION = "asset.location";
    public static final String COLUMN_GROUP = "asset.group";
    public static final String COLUMN_CATEGORY = "asset.category";
    public static final String COLUMN_CATEGORYOPTION = "asset.categoryoption";
    public static final String COLUMN_MANUFACTURER = "asset.manufacturer";
    public static final String COLUMN_MODELNUMBER = "asset.modelNumber";
    public static final String COLUMN_SERIALNUMBER = "asset.serialnumber";
    public static final String COLUMN_DESCRIPTION = "asset.description";
    public static final String COLUMN_STATUS = "asset.status";
    public static final String COLUMN_SOFTLICS = "asset.softwarelicenses";
    public static final String COLUMN_AI_PONUMBER = "accountinginfo.ponumber";
    public static final String COLUMN_AI_ACCOUNTNUMBER = "accountinginfo.accountnumber";
    public static final String COLUMN_AI_ACCUMULATEDNUMBER = "accountinginfo.accumulatednumber";
    public static final String COLUMN_AI_EXPENSENUMBER = "accountinginfo.expensenumber";
    public static final String COLUMN_AI_ACQUISITIONVALUE = "accountinginfo.acquisitionvalue";
    public static final String COLUMN_AI_LEASENUMBER = "accountinginfo.leasenumber";
    public static final String COLUMN_AI_LEASEDURATION = "accountinginfo.leaseduration";
    public static final String COLUMN_AI_LEASEFREQUENCY = "accountinginfo.leasefrequency";
    public static final String COLUMN_AI_LEASEFREQUENCYPRICE = "accountinginfo.leasefrequencyprice";
    public static final String COLUMN_AI_DISPOSALMETHOD = "accountinginfo.disposalmethod";
    public static final String COLUMN_AI_DISPOSALDATE = "accountinginfo.disposaldate";
    public static final String COLUMN_AI_INSURANCECATEGORY = "accountinginfo.insurancecategory";
    public static final String COLUMN_AI_REPLACEMENTVALUECATEGORY = "accountinginfo.replacementvaluecategory";
    public static final String COLUMN_AI_REPLACEMENTVALUE = "accountinginfo.replacementvalue";
    public static final String COLUMN_AI_MAINTENANCECOST = "accountinginfo.maintenancecost";
    public static final String COLUMN_AI_DEPRECIATIONMETHOD = "accountinginfo.depreciationmethod";
    public static final String COLUMN_AI_LEASEEXPIRATIONDATE = "accountinginfo.leaseexpirationdate";
    public static final String COLUMN_AI_WARRANTYDATE = "accountinginfo.warrantydate";

    public static final String[] ALL_COLUMNS = new String[]{
            COLUMN_ASSETNUMBER,
            COLUMN_NAME,
            COLUMN_TYPE,
            COLUMN_OWNER,
            COLUMN_VENDOR,
            COLUMN_ASSETLOCATION,
            COLUMN_ACQUISITIONDATE,
            COLUMN_LOCATION,
            COLUMN_GROUP,
            COLUMN_CATEGORY,
            COLUMN_CATEGORYOPTION,
            COLUMN_MANUFACTURER,
            COLUMN_MODELNUMBER,
            COLUMN_SERIALNUMBER,
            COLUMN_DESCRIPTION,
            COLUMN_STATUS,
            COLUMN_SOFTLICS,
            COLUMN_AI_PONUMBER,
            COLUMN_AI_ACCOUNTNUMBER,
            COLUMN_AI_ACCUMULATEDNUMBER,
            COLUMN_AI_EXPENSENUMBER,
            COLUMN_AI_ACQUISITIONVALUE,
            COLUMN_AI_LEASENUMBER,
            COLUMN_AI_LEASEDURATION,
            COLUMN_AI_LEASEFREQUENCY,
            COLUMN_AI_LEASEFREQUENCYPRICE,
            COLUMN_AI_DISPOSALMETHOD,
            COLUMN_AI_DISPOSALDATE,
            COLUMN_AI_INSURANCECATEGORY,
            COLUMN_AI_REPLACEMENTVALUECATEGORY,
            COLUMN_AI_REPLACEMENTVALUE,
            COLUMN_AI_MAINTENANCECOST,
            COLUMN_AI_DEPRECIATIONMETHOD,
            COLUMN_AI_LEASEEXPIRATIONDATE,
            COLUMN_AI_WARRANTYDATE
    };

    private Integer id;

    private User user;
    private String name;
    private Boolean privateFilter = Boolean.TRUE;

    private String[] assetNumbers;
    private String[] names;
    private Integer[] typeIds;
    private Integer[] ownerIds;
    private Integer[] vendorIds;
    private String[] notes;
    private String[] assetLocations;

    private Integer acquisitionDateOperator;
    private String acquisitionDate;
    private String acquisitionDate2;

    private Integer[] locationIds;
    private Integer[] groupIds;
    private Integer[] categoryIds;
    private Integer[] categoryOptionIds;

    private String[] manufacturers;
    private String[] modelNumbers;
    private String[] serialNumbers;
    private String[] descriptions;
    private Integer[] statusIds;
    private Integer[] softLicIds;

    // Accounting Info fields
    private String[] poNumbers;
    private String[] accountNumbers;
    private String[] accumulatedNumbers;
    private String[] expenseNumbers;
    private String[] acquisitionValues;
    private String[] leaseNumbers;
    private String[] leaseDurations;
    private String[] leaseFrequencys;
    private String[] leaseFrequencyPrices;
    private String[] disposalMethods;

    private Integer disposalDateOperator;
    private String disposalDate;
    private String disposalDate2;

    private String[] insuranceCategories;
    private String[] replacementValuecategories;
    private String[] replacementValues;
    private String[] maintenanceCosts;
    private String[] depreciationMethods;

    private Integer leaseExpirationDateOperator;
    private String leaseExpirationDate;
    private String leaseExpirationDate2;

    private Integer warrantyDateOperator;
    private String warrantyDate;
    private String warrantyDate2;

    private Map<String, String[]> customFields = new HashMap<String, String[]>();

    private String[] columnOrder;

    //Report variables
    private Boolean report = Boolean.FALSE;
    private Boolean mainReport = Boolean.TRUE;
    private Integer groupBy;
    private Boolean showDetail = Boolean.FALSE;
    private Boolean showPieChart = Boolean.TRUE;
    private String format = "html";

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userid")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "private")
    public Boolean getPrivateFilter() {
        if (privateFilter == null) {
            privateFilter = Boolean.TRUE;
        }
        
        return privateFilter;
    }

    public void setPrivateFilter(Boolean privateFilter) {
        if (privateFilter == null) {
            this.privateFilter = Boolean.TRUE;
        } else {
            this.privateFilter = privateFilter;
        }
    }

    @Column(name = "assetnumbers", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getAssetNumbers() {
        return assetNumbers;
    }

    public void setAssetNumbers(String[] assetNumbers) {
        this.assetNumbers = assetNumbers;
    }

    @Column(name = "names", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getNames() {
        return names;
    }

    public void setNames(String[] names) {
        this.names = names;
    }

    @Column(name = "typeids", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getTypeIds() {
        return typeIds;
    }

    public void setTypeIds(Integer[] typeIds) {
        this.typeIds = typeIds;
    }

    @Column(name = "ownerids", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getOwnerIds() {
        return ownerIds;
    }

    public void setOwnerIds(Integer[] ownerIds) {
        this.ownerIds = ownerIds;
    }

    @Column(name = "vendorids", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getVendorIds() {
        return vendorIds;
    }

    public void setVendorIds(Integer[] vendorIds) {
        this.vendorIds = vendorIds;
    }

    @Column(name = "notes", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getNotes() {
        return notes;
    }

    public void setNotes(String[] notes) {
        this.notes = notes;
    }

    @Column(name = "assetlocationids", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getAssetLocations() {
        return assetLocations;
    }

    public void setAssetLocations(String[] assetLocations) {
        this.assetLocations = assetLocations;
    }

    @Column(name = "acquiredateop")
    public Integer getAcquisitionDateOperator() {
        return acquisitionDateOperator;
    }

    public void setAcquisitionDateOperator(Integer acquisitionDateOperator) {
        this.acquisitionDateOperator = acquisitionDateOperator;
    }

    @Column(name = "acquiredate")
    public String getAcquisitionDate() {
        return acquisitionDate;
    }

    public void setAcquisitionDate(String acquisitionDate) {
        this.acquisitionDate = acquisitionDate;
    }

    @Column(name = "acquiredate2")
    public String getAcquisitionDate2() {
        return acquisitionDate2;
    }

    public void setAcquisitionDate2(String acquisitionDate2) {
        this.acquisitionDate2 = acquisitionDate2;
    }

    @Column(name = "locids", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getLocationIds() {
        return locationIds;
    }

    public void setLocationIds(Integer[] locationIds) {
        this.locationIds = locationIds;
    }

    @Column(name = "groupids", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getGroupIds() {
        return groupIds;
    }

    public void setGroupIds(Integer[] groupIds) {
        this.groupIds = groupIds;
    }

    @Column(name = "catids", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(Integer[] categoryIds) {
        this.categoryIds = categoryIds;
    }

    @Column(name = "catoptids", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getCategoryOptionIds() {
        return categoryOptionIds;
    }

    public void setCategoryOptionIds(Integer[] categoryOptionIds) {
        this.categoryOptionIds = categoryOptionIds;
    }

    @Column(name = "manufacturers", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getManufacturers() {
        return manufacturers;
    }

    public void setManufacturers(String[] manufacturers) {
        this.manufacturers = manufacturers;
    }

    @Column(name = "modelnumbers", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getModelNumbers() {
        return modelNumbers;
    }

    public void setModelNumbers(String[] modelNumbers) {
        this.modelNumbers = modelNumbers;
    }

    @Column(name = "serialnumbers", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getSerialNumbers() {
        return serialNumbers;
    }

    public void setSerialNumbers(String[] serialNumbers) {
        this.serialNumbers = serialNumbers;
    }

    @Column(name = "descriptions", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String[] descriptions) {
        this.descriptions = descriptions;
    }

    @Column(name = "statusids", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getStatusIds() {
        return statusIds;
    }

    public void setStatusIds(Integer[] statusIds) {
        this.statusIds = statusIds;
    }

    @Column(name = "softlicids", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getSoftLicIds() {
        return softLicIds;
    }

    public void setSoftLicIds(Integer[] softLicIds) {
        this.softLicIds = softLicIds;
    }

    @Column(name = "ponumbers", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getPoNumbers() {
        return poNumbers;
    }

    public void setPoNumbers(String[] poNumbers) {
        this.poNumbers = poNumbers;
    }

    @Column(name = "accountnums", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getAccountNumbers() {
        return accountNumbers;
    }

    public void setAccountNumbers(String[] accountNumbers) {
        this.accountNumbers = accountNumbers;
    }

    @Column(name = "accumulatednums", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getAccumulatedNumbers() {
        return accumulatedNumbers;
    }

    public void setAccumulatedNumbers(String[] accumulatedNumbers) {
        this.accumulatedNumbers = accumulatedNumbers;
    }

    @Column(name = "expensenums", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getExpenseNumbers() {
        return expenseNumbers;
    }

    public void setExpenseNumbers(String[] expenseNumbers) {
        this.expenseNumbers = expenseNumbers;
    }

    @Column(name = "acquisitionvals", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getAcquisitionValues() {
        return acquisitionValues;
    }

    public void setAcquisitionValues(String[] acquisitionValues) {
        this.acquisitionValues = acquisitionValues;
    }

    @Column(name = "leasenums", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getLeaseNumbers() {
        return leaseNumbers;
    }

    public void setLeaseNumbers(String[] leaseNumbers) {
        this.leaseNumbers = leaseNumbers;
    }

    @Column(name = "leasedurations", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getLeaseDurations() {
        return leaseDurations;
    }

    public void setLeaseDurations(String[] leaseDurations) {
        this.leaseDurations = leaseDurations;
    }

    @Column(name = "leasefreqs", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getLeaseFrequencys() {
        return leaseFrequencys;
    }

    public void setLeaseFrequencys(String[] leaseFrequencys) {
        this.leaseFrequencys = leaseFrequencys;
    }

    @Column(name = "leasefreqprices", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getLeaseFrequencyPrices() {
        return leaseFrequencyPrices;
    }

    public void setLeaseFrequencyPrices(String[] leaseFrequencyPrices) {
        this.leaseFrequencyPrices = leaseFrequencyPrices;
    }

    @Column(name = "disposalmethods", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getDisposalMethods() {
        return disposalMethods;
    }

    public void setDisposalMethods(String[] disposalMethods) {
        this.disposalMethods = disposalMethods;
    }

    @Column(name = "disposaldateop")
    public Integer getDisposalDateOperator() {
        return disposalDateOperator;
    }

    public void setDisposalDateOperator(Integer disposalDateOperator) {
        this.disposalDateOperator = disposalDateOperator;
    }

    @Column(name = "disposaldate")
    public String getDisposalDate() {
        return disposalDate;
    }

    public void setDisposalDate(String disposalDate) {
        this.disposalDate = disposalDate;
    }

    @Column(name = "disposaldate2")
    public String getDisposalDate2() {
        return disposalDate2;
    }

    public void setDisposalDate2(String disposalDate2) {
        this.disposalDate2 = disposalDate2;
    }

    @Column(name = "insurancecats", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getInsuranceCategories() {
        return insuranceCategories;
    }

    public void setInsuranceCategories(String[] insuranceCategories) {
        this.insuranceCategories = insuranceCategories;
    }

    @Column(name = "replvalcats", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getReplacementValuecategories() {
        return replacementValuecategories;
    }

    public void setReplacementValuecategories(String[] replacementValuecategories) {
        this.replacementValuecategories = replacementValuecategories;
    }

    @Column(name = "replacevals", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getReplacementValues() {
        return replacementValues;
    }

    public void setReplacementValues(String[] replacementValues) {
        this.replacementValues = replacementValues;
    }

    @Column(name = "maintenancecosts", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getMaintenanceCosts() {
        return maintenanceCosts;
    }

    public void setMaintenanceCosts(String[] maintenanceCosts) {
        this.maintenanceCosts = maintenanceCosts;
    }

    @Column(name = "deprmethods", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getDepreciationMethods() {
        return depreciationMethods;
    }

    public void setDepreciationMethods(String[] depreciationMethods) {
        this.depreciationMethods = depreciationMethods;
    }

    @Column(name = "leaseexpdateop")
    public Integer getLeaseExpirationDateOperator() {
        return leaseExpirationDateOperator;
    }

    public void setLeaseExpirationDateOperator(Integer leaseExpirationDateOperator) {
        this.leaseExpirationDateOperator = leaseExpirationDateOperator;
    }

    @Column(name = "leaseexpdate")
    public String getLeaseExpirationDate() {
        return leaseExpirationDate;
    }

    public void setLeaseExpirationDate(String leaseExpirationDate) {
        this.leaseExpirationDate = leaseExpirationDate;
    }

    @Column(name = "leaseexpdate2")
    public String getLeaseExpirationDate2() {
        return leaseExpirationDate2;
    }

    public void setLeaseExpirationDate2(String leaseExpirationDate2) {
        this.leaseExpirationDate2 = leaseExpirationDate2;
    }

    @Column(name = "warrantydateop")
    public Integer getWarrantyDateOperator() {
        return warrantyDateOperator;
    }

    public void setWarrantyDateOperator(Integer warrantyDateOperator) {
        this.warrantyDateOperator = warrantyDateOperator;
    }

    @Column(name = "warrantydate")
    public String getWarrantyDate() {
        return warrantyDate;
    }

    public void setWarrantyDate(String warrantyDate) {
        this.warrantyDate = warrantyDate;
    }

    @Column(name = "warrantydate2")
    public String getWarrantyDate2() {
        return warrantyDate2;
    }

    public void setWarrantyDate2(String warrantyDate2) {
        this.warrantyDate2 = warrantyDate2;
    }

    @Column(name = "isreport")
    public Boolean getReport() {
        if (report == null) {
            report = Boolean.FALSE;
        }

        return report;
    }

    public void setReport(Boolean report) {
        if (report == null) {
            this.report = Boolean.FALSE;
        } else {
            this.report = report;
        }
    }

    @Column(name = "ismainreport")
    public Boolean getMainReport() {
        if (mainReport == null) {
            mainReport = Boolean.TRUE;
        }
        
        return mainReport;
    }

    public void setMainReport(Boolean mainReport) {
        if (mainReport == null) {
            this.mainReport = Boolean.TRUE;
        } else {
            this.mainReport = mainReport;
        }
    }

    @Column(name = "groupby")
    public Integer getGroupBy() {
        return groupBy;
    }

    public void setGroupBy(Integer groupBy) {
        this.groupBy = groupBy;
    }

    @Column(name = "showdetail")
    public Boolean getShowDetail() {
        if (showDetail == null) {
            showDetail = Boolean.FALSE;
        }
        
        return showDetail;
    }

    public void setShowDetail(Boolean showDetail) {
        if (showDetail == null) {
            this.showDetail = Boolean.FALSE;
        } else {
            this.showDetail = showDetail;
        }
    }

    @Column(name = "showpiechart")
    public Boolean getShowPieChart() {
        if (showPieChart == null) {
            showPieChart = Boolean.TRUE;
        }
        
        return showPieChart;
    }

    public void setShowPieChart(Boolean showPieChart) {
        if (showPieChart == null) {
            this.showPieChart = Boolean.TRUE;
        } else {
            this.showPieChart = showPieChart;
        }
    }

    @Column(name = "format")
    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    @Column(name = "colorder", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getColumnOrder() {
        return columnOrder;
    }

    public void setColumnOrder(String[] columnOrder) {
        this.columnOrder = columnOrder;
    }

    @Column(name = "custfields", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Map<String, String[]> getCustomFields() {
        return customFields;
    }

    public void setCustomFields(Map<String, String[]> customFields) {
        this.customFields = customFields;
    }

    public AssetFilter clone() throws CloneNotSupportedException {
        AssetFilter newFilter = (AssetFilter)super.clone();
        newFilter.setId(null);
        newFilter.setUser(user);
        return newFilter;
    }
}
