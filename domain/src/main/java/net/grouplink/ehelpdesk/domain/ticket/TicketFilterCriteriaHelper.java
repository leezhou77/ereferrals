package net.grouplink.ehelpdesk.domain.ticket;

import java.io.Serializable;

public class TicketFilterCriteriaHelper implements Serializable {
    private Integer operatorId;
    private String value;

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
