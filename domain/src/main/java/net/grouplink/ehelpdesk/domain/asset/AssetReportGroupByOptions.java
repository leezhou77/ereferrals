package net.grouplink.ehelpdesk.domain.asset;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: Sep 12, 2008
 * Time: 5:30:30 PM
 */
public class AssetReportGroupByOptions implements Serializable {
    public static final Integer TYPE = 1;
    public static final Integer STATUS = 2;
    public static final Integer LOCATION = 3;
    public static final Integer GROUP = 4;
    public static final Integer CATEGORY = 5;
    public static final Integer CATEGORY_OPTION = 6;
    public static final Integer OWNER = 7;

    private Integer value;
    private String display;

    public AssetReportGroupByOptions(Integer value, String display) {
        this.value = value;
        this.display = display;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }
}
