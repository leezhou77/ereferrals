package net.grouplink.ehelpdesk.domain;

import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.ZenAsset;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Filter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Represents a <code>Ticket</code> in the eHelpDesk system.
 */
@Entity
@Table(name = "TICKETTEMPLATE")
@FilterDef(name = "deletedObject", parameters = @ParamDef(name = "isActive", type = "boolean"))
@Filter(name = "deletedObject", condition = "(ACTIVE is null or ACTIVE = :isActive)")
public class TicketTemplate implements Serializable, Comparable, Cloneable {

    private Integer id;

    private String templateName;
    private boolean publicTemplate;

    private User contact;
    private User submittedBy;
    private UserRoleGroup assignedTo;
    private TicketPriority priority;
    private Status status;
    private Location location;
    private Group group;
    private Category category;
    private CategoryOption categoryOption;

    private String subject;
    private String note;

    private Date createdDate;
    private Date modifiedDate;

    private String cc;
    private String bc;
    private TicketTemplate parent;
    private TicketTemplateMaster master;
    private WorkflowStep workflowStep;

    private Asset asset;
    private ZenAsset zenAsset;

    private Set<TicketTemplate> subtickets = new HashSet<TicketTemplate>();
    private Set<TicketTemplateCustomFieldValue> customeFieldValues = new HashSet<TicketTemplateCustomFieldValue>();

    /* Used for spring binding. Runtime type is MultipartFile. */
    private List<Object> attchmnt = new ArrayList<Object>();
    private Set<Attachment> attachments = new HashSet<Attachment>();

    private Boolean active = Boolean.TRUE;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @OneToMany(mappedBy = "ticketTemplate")
    @Cascade({org.hibernate.annotations.CascadeType.ALL, org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    public Set<TicketTemplateCustomFieldValue> getCustomeFieldValues() {
        return customeFieldValues;
    }

    public void setCustomeFieldValues(Set<TicketTemplateCustomFieldValue> customeFieldValues) {
        this.customeFieldValues = customeFieldValues;
    }

    public void addCustomeFieldValue(TicketTemplateCustomFieldValue cfv) {
        cfv.setTicketTemplate(this);
        customeFieldValues.add(cfv);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ASSETID")
    public Asset getAsset() {
        return asset;
    }

    public void setAsset(Asset asset) {
        this.asset = asset;
    }

    @ManyToMany
    @Cascade({org.hibernate.annotations.CascadeType.ALL})
    @JoinTable(name = "TICKETTEMPLATEATTACHMENTS",
         joinColumns = @JoinColumn(name = "TICKETTEMPLATEID"),
         inverseJoinColumns = @JoinColumn(name = "ATTACHMENTID"))
    public Set<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(Set<Attachment> attachments) {
        this.attachments = attachments;
    }

    public void addAttachment(Attachment attachment) {
        attachments.add(attachment);
    }

    /**
     * Used for spring binding. Runtime type is MultipartFile.
     * @return List of MultipartFile
     */
    @Transient
    public List<Object> getAttchmnt() {
        return attchmnt;
    }

    public void setAttchmnt(List<Object> attchmnt) {
        this.attchmnt = attchmnt;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GROUPID")
    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @Column(name = "CC")
    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    @Column(name = "BCC")
    public String getBc() {
        return bc;
    }

    public void setBc(String bc) {
        this.bc = bc;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ASSIGNEDTO")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.PERSIST})
    public UserRoleGroup getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(UserRoleGroup assignedTo) {
        this.assignedTo = assignedTo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CATEGORYID")
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CATEGORYOPTIONID")
    public CategoryOption getCategoryOption() {
        return categoryOption;
    }

    public void setCategoryOption(CategoryOption categoryOption) {
        this.categoryOption = categoryOption;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATEDDATE")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LOCATIONID")
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "MODIFIEDDATE")
    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRIORITYID")
    public TicketPriority getPriority() {
        return priority;
    }

    public void setPriority(TicketPriority priority) {
        this.priority = priority;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUSID")
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SUBMITTEDBY")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.PERSIST})
    public User getSubmittedBy() {
        return submittedBy;
    }

    public void setSubmittedBy(User submitedBy) {
        this.submittedBy = submitedBy;
    }

    @Column(name = "SUBJECT")
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Lob
    @Column(name = "NOTE", length = 2147483647)
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONTACT")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.PERSIST})
    public User getContact() {
        return contact;
    }

    public void setContact(User contact) {
        this.contact = contact;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENTID")
    public TicketTemplate getParent() {
        return parent;
    }

    public void setParent(TicketTemplate parent) {
        this.parent = parent;
    }

    @OneToMany(mappedBy = "parent")
    @Cascade({org.hibernate.annotations.CascadeType.ALL})
    @Filter(name = "deletedObject", condition = "(ACTIVE is null or ACTIVE = :isActive)")
    public Set<TicketTemplate> getSubtickets() {
        return subtickets;
    }

    public void setSubtickets(Set<TicketTemplate> subtickets) {
        this.subtickets = subtickets;
    }

    public void addSubTicket(TicketTemplate ticketTemplate) {
        ticketTemplate.setParent(this);
        subtickets.add(ticketTemplate);
    }

    @Transient
    public List<TicketTemplate> getSubTicketsList() {
        List<TicketTemplate> list = new ArrayList<TicketTemplate>();
        if (subtickets != null) {
            list.addAll(subtickets);
            Collections.sort(list);
        }
        return list;
    }

    public int compareTo(Object o) {
        return id.compareTo(((TicketTemplate) o).getId());
    }

    @Column(name = "TEMPLATENAME")
    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    @Column(name = "PUBLICTEMPLATE")
    public boolean getPublicTemplate() {
        return publicTemplate;
    }

    public void setPublicTemplate(boolean publicTemplate) {
        this.publicTemplate = publicTemplate;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TTMASTERID")
    public TicketTemplateMaster getMaster() {
        return master;
    }

    public void setMaster(TicketTemplateMaster master) {
        this.master = master;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "WORKFLOWSTEPID")
    public WorkflowStep getWorkflowStep() {
        return workflowStep;
    }

    public void setWorkflowStep(WorkflowStep workflowStep) {
        this.workflowStep = workflowStep;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ZENASSETID")
    public ZenAsset getZenAsset() {
        return zenAsset;
    }

    public void setZenAsset(ZenAsset zenAsset) {
        this.zenAsset = zenAsset;
    }

    @Column(name = "ACTIVE")
    public Boolean getActive() {
        if (active == null) {
            active = Boolean.TRUE;
        }
        
        return active;
    }

    public void setActive(Boolean active) {
        if (active == null) {
            this.active = Boolean.TRUE;
        } else {
            this.active = active;
        }
    }

    @Override
    public TicketTemplate clone() throws CloneNotSupportedException {
        TicketTemplate ticketTemplate = (TicketTemplate) super.clone();
        ticketTemplate.setId(null);
        ticketTemplate.setParent(null);
        ticketTemplate.setSubtickets(new HashSet<TicketTemplate>());
        ticketTemplate.setAttachments(new HashSet<Attachment>());

        // clone custom field values
        ticketTemplate.setCustomeFieldValues(new HashSet<TicketTemplateCustomFieldValue>());
        for (TicketTemplateCustomFieldValue customFieldValue : customeFieldValues) {
            ticketTemplate.addCustomeFieldValue(customFieldValue.clone());
        }

        // clone subtickets
        for (TicketTemplate subticket : subtickets) {
            TicketTemplate subTT = subticket.clone();
            ticketTemplate.addSubTicket(subTT);
        }

        return ticketTemplate;
    }
}
