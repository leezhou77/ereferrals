package net.grouplink.ehelpdesk.domain;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.ManyToOne;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Table;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Set;
import java.util.HashSet;
import java.util.Date;

@Entity
@Table(name = "TTLAUNCH")
public class TicketTemplateLaunch implements Serializable {
    public static final Integer STATUS_PENDING = 1;
    public static final Integer STATUS_ACTIVE = 2;
    
    private Integer id;
    private TicketTemplateMaster ticketTemplateMaster;
    private Set<Ticket> tickets = new HashSet<Ticket>();
    private Date createdDate;
    private User createdBy;
    private Integer status; // 1 = created not launched 2 = launched
    private Set<WorkflowStepStatus> workflowStepStatuses = new HashSet<WorkflowStepStatus>();

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TICKETTEMPLATEMASTERID")
    public TicketTemplateMaster getTicketTemplateMaster() {
        return ticketTemplateMaster;
    }

    public void setTicketTemplateMaster(TicketTemplateMaster ticketTemplateMaster) {
        this.ticketTemplateMaster = ticketTemplateMaster;
    }

    @OneToMany(mappedBy = "ticketTemplateLaunch")
    @Cascade(CascadeType.ALL)
    public Set<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(Set<Ticket> tickets) {
        this.tickets = tickets;
    }

    public void addTicket(Ticket ticket) {
        ticket.setTicketTemplateLaunch(this);
        tickets.add(ticket);
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATEDDATE")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USERID")
    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @OneToMany(mappedBy = "ticketTemplateLaunch")
    @Cascade(CascadeType.ALL)
    public Set<WorkflowStepStatus> getWorkflowStepStatuses() {
        return workflowStepStatuses;
    }

    public void setWorkflowStepStatuses(Set<WorkflowStepStatus> workflowStepStatuses) {
        this.workflowStepStatuses = workflowStepStatuses;
    }

    public void addWorkflowStepStatus(WorkflowStepStatus wss) {
        wss.setTicketTemplateLaunch(this);
        workflowStepStatuses.add(wss);
    }
}
