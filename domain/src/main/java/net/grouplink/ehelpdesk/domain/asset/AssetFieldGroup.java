package net.grouplink.ehelpdesk.domain.asset;

import net.grouplink.ehelpdesk.domain.asset.util.AssetCustomFieldComparator;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * User: jaymehafen
 * Date: Jul 3, 2008
 * Time: 10:39:17 PM
 */
@Entity
@Table(name = "ASSETFIELDGROUP")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class AssetFieldGroup implements Serializable, Comparable {
    private Integer id;
    private String name;
    private String description;
    private Integer tabOrder;
//    private AssetType assetType;
    private Set<AssetCustomField> customFields = new HashSet<AssetCustomField>();
    private Set<AssetType> assetTypes = new HashSet<AssetType>();

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "NAME", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "TABORDER")
    public Integer getTabOrder() {
        return tabOrder;
    }

    public void setTabOrder(Integer tabOrder) {
        this.tabOrder = tabOrder;
    }

    @OneToMany(mappedBy = "fieldGroup", fetch = FetchType.LAZY)
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    public Set<AssetCustomField> getCustomFields() {
        return customFields;
    }

    @Transient
    public List<AssetCustomField> getPrioritizedCustomFields() {
        ArrayList<AssetCustomField> cfList = new ArrayList<AssetCustomField>();
        for (AssetCustomField customField : customFields) {
            cfList.add(customField);
        }

        Comparator<AssetCustomField> acfComp = new AssetCustomFieldComparator();
        Collections.sort(cfList, acfComp);
        return cfList;
    }

    public void setCustomFields(Set<AssetCustomField> customFields) {
        this.customFields = customFields;
    }

    @Column(name = "DESCRIPTION")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @ManyToMany
    @JoinTable(name = "ASSETFIELDGROUPASSETTYPE",
        joinColumns = @JoinColumn(name = "ASSETFIELDGROUPID"),
        inverseJoinColumns = @JoinColumn(name = "ASSETTYPEID"))
    public Set<AssetType> getAssetTypes() {
        return assetTypes;
    }

    public void setAssetTypes(Set<AssetType> assetTypes) {
        this.assetTypes = assetTypes;
    }

    public int compareTo(Object o) {
        return this.getName().compareTo(((AssetFieldGroup)o).getName());
    }
}
