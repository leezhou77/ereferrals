package net.grouplink.ehelpdesk.domain.asset;

import java.io.Serializable;

/**
 * @author mrollins
 * @version 1.0
 */
public class AssetsFilter implements Serializable, Cloneable {

    private String assetNumber;
    private String name;
    private String location;
    private String assetLocation;
    private String group;
    private String category;
    private String categoryOption;

    //Report variables
    private Boolean report;
    private Boolean mainReport = Boolean.TRUE;
    private Boolean showLink = Boolean.TRUE;
    private Integer groupBy = 5;
    private Boolean reGroup = Boolean.FALSE;
    private Integer groupByBy = 5;
    private Boolean showDetail = Boolean.FALSE;
    private Boolean showGraph = Boolean.TRUE;
    private Boolean showPieChart = Boolean.TRUE;
    private String format = "html";

    private Boolean showReportByDate = Boolean.FALSE;
    private Integer byDate = 2;
    //!-----

    public boolean equals(Object obj) {
        return (obj instanceof AssetsFilter && equals((AssetsFilter) obj));
    }

    public boolean equals(AssetsFilter cf) {
        if (cf == this) return true;

        // Check each field for equality
        boolean result =(assetNumber == null ? cf.assetNumber == null : assetNumber.equals(cf.assetNumber));
        if (result) result = (name == null ? cf.name == null : name.equals(cf.name));
        if (result) result = (location == null ? cf.location == null : location.equals(cf.location));
        if (result) result = (assetLocation == null ? cf.assetLocation == null : assetLocation.equals(cf.assetLocation));
        if (result) result = (group == null ? cf.group == null : group.equals(cf.group));
        if (result) result = (category == null ? cf.category == null : category.equals(cf.category));
        if (result) result = (categoryOption == null ? cf.categoryOption == null : categoryOption.equals(cf.categoryOption));

        return result;
    }

    public int hashCode() {
        int hash = 17;
        hash = 37 * hash + (assetNumber == null ? 0 : assetNumber.hashCode());
        hash = 37 * hash + (name == null ? 0 : name.hashCode());
        hash = 37 * hash + (categoryOption == null ? 0 : categoryOption.hashCode());
        hash = 37 * hash + (location == null ? 0 : location.hashCode());
        hash = 37 * hash + (assetLocation == null ? 0 : assetLocation.hashCode());
        hash = 37 * hash + (group == null ? 0 : group.hashCode());
        hash = 37 * hash + (category == null ? 0 : category.hashCode());
        hash = 37 * hash + (categoryOption == null ? 0 : categoryOption.hashCode());

        return hash;
    }

    public String getAssetNumber() {
        return assetNumber;
    }

    public void setAssetNumber(String assetNumber) {
        this.assetNumber = assetNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAssetLocation() {
        return assetLocation;
    }

    public void setAssetLocation(String assetLocation) {
        this.assetLocation = assetLocation;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategoryOption() {
        return categoryOption;
    }

    public void setCategoryOption(String categoryOption) {
        this.categoryOption = categoryOption;
    }

    public Boolean getReport() {
        return report;
    }

    public void setReport(Boolean report) {
        this.report = report;
    }

    public Boolean getMainReport() {
        return mainReport;
    }

    public void setMainReport(Boolean mainReport) {
        this.mainReport = mainReport;
    }

    public Boolean getShowLink() {
        return showLink;
    }

    public void setShowLink(Boolean showLink) {
        this.showLink = showLink;
    }

    public Integer getGroupBy() {
        return groupBy;
    }

    public void setGroupBy(Integer groupBy) {
        this.groupBy = groupBy;
    }

    public Boolean getReGroup() {
        return reGroup;
    }

    public void setReGroup(Boolean reGroup) {
        this.reGroup = reGroup;
    }

    public Integer getGroupByBy() {
        return groupByBy;
    }

    public void setGroupByBy(Integer groupByBy) {
        this.groupByBy = groupByBy;
    }

    public Boolean getShowDetail() {
        return showDetail;
    }

    public void setShowDetail(Boolean showDetail) {
        this.showDetail = showDetail;
    }

    public Boolean getShowGraph() {
        return showGraph;
    }

    public void setShowGraph(Boolean showGraph) {
        this.showGraph = showGraph;
    }

    public Boolean getShowPieChart() {
        return showPieChart;
    }

    public void setShowPieChart(Boolean showPieChart) {
        this.showPieChart = showPieChart;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public Boolean getShowReportByDate() {
        return showReportByDate;
    }

    public void setShowReportByDate(Boolean showReportByDate) {
        this.showReportByDate = showReportByDate;
    }

    public Integer getByDate() {
        return byDate;
    }

    public void setByDate(Integer byDate) {
        this.byDate = byDate;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
