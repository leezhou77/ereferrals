package net.grouplink.ehelpdesk.domain;

import net.grouplink.ehelpdesk.domain.constants.JasperReportsConstants;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import org.apache.cxf.aegis.type.java5.IgnoreProperty;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "REPORT")
public class Report implements Serializable {

    public static final Integer REPORT_BY_DATE_DAY = 0;
    public static final Integer REPORT_BY_DATE_WEEK = 1;
    public static final Integer REPORT_BY_DATE_MONTH = 2;
    public static final Integer REPORT_BY_DATE_YEAR = 3;

    public static final String FORMAT_PDF = "pdf";
    public static final String FORMAT_HTML = "html";
    public static final String FORMAT_CSV = "csv";
    public static final String FORMAT_XLS = "xls";

    private Integer id;
    private String name;
    private User user;
    private Boolean privateReport;
    private Boolean mainReport = Boolean.TRUE;
    private Boolean showLink = Boolean.TRUE;
    private Integer groupBy = JasperReportsConstants.TECHNICIAN;
    private Boolean reGroup = Boolean.FALSE;
    private Integer groupByBy = JasperReportsConstants.TECHNICIAN;
    private Boolean showDetail = Boolean.FALSE;
    private Boolean showGraph = Boolean.TRUE;
    private Boolean showPieChart = Boolean.TRUE;
    private String format = FORMAT_PDF;
    private Integer groupByOrder = JasperReportsConstants.GROUP_BY_ORDER_ALPHA;
    private Integer showTop;
    private Boolean showReportByDate = Boolean.FALSE;
    private Integer byDate = REPORT_BY_DATE_MONTH;
    private TicketFilter ticketFilter;
    private Boolean showRecurrenceSchedule;
    private ReportRecurrenceSchedule schedule;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @IgnoreProperty
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USERID")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Column(name = "PRIVATE")
    public Boolean getPrivateReport() {
        if (privateReport == null) {
            privateReport = Boolean.TRUE;
        }

        return privateReport;
    }

    public void setPrivateReport(Boolean privateReport) {
        if (privateReport == null) {
            this.privateReport = Boolean.TRUE;
        } else {
            this.privateReport = privateReport;
        }
    }

    @Column(name = "ISMAINREPORT")
    public Boolean getMainReport() {
        if (mainReport == null) {
            mainReport = Boolean.TRUE;
        }

        return mainReport;
    }

    public void setMainReport(Boolean mainReport) {
        if (mainReport == null) {
            this.mainReport = Boolean.TRUE;
        } else {
            this.mainReport = mainReport;
        }
    }

    @Column(name = "SHOWLINK")
    public Boolean getShowLink() {
        if (showLink == null) {
            showLink = Boolean.TRUE;
        }

        return showLink;
    }

    public void setShowLink(Boolean showLink) {
        if (showLink == null) {
            this.showLink = Boolean.TRUE;
        } else {
            this.showLink = showLink;
        }
    }

    @Column(name = "GROUPBY")
    public Integer getGroupBy() {
        return groupBy;
    }

    public void setGroupBy(Integer groupBy) {
        this.groupBy = groupBy;
    }

    @Column(name = "REGROUP")
    public Boolean getReGroup() {
        if (reGroup == null) {
            reGroup = Boolean.FALSE;
        }

        return reGroup;
    }

    public void setReGroup(Boolean reGroup) {
        if (reGroup == null) {
            this.reGroup = Boolean.FALSE;
        } else {
            this.reGroup = reGroup;
        }
    }

    @Column(name = "GROUPBYBY")
    public Integer getGroupByBy() {
        return groupByBy;
    }

    public void setGroupByBy(Integer groupByBy) {
        this.groupByBy = groupByBy;
    }

    @Column(name = "SHOWDETAIL")
    public Boolean getShowDetail() {
        if (showDetail == null) {
            showDetail = Boolean.FALSE;
        }

        return showDetail;
    }

    public void setShowDetail(Boolean showDetail) {
        if (showDetail == null) {
            this.showDetail = Boolean.FALSE;
        } else {
            this.showDetail = showDetail;
        }
    }

    @Column(name = "SHOWGRAPH")
    public Boolean getShowGraph() {
        if (showGraph == null) {
            showGraph = Boolean.TRUE;
        }

        return showGraph;
    }

    public void setShowGraph(Boolean showGraph) {
        if (showGraph == null) {
            this.showGraph = Boolean.TRUE;
        } else {
            this.showGraph = showGraph;
        }
    }

    @Column(name = "SHOWPIECHART")
    public Boolean getShowPieChart() {
        if (showPieChart == null) {
            showPieChart = Boolean.TRUE;
        }

        return showPieChart;
    }

    public void setShowPieChart(Boolean showPieChart) {
        if (showPieChart == null) {
            this.showPieChart = Boolean.TRUE;
        } else {
            this.showPieChart = showPieChart;
        }
    }

    @Column(name = "FORMAT")
    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    @Column(name = "REPORTBYDATE")
    public Boolean getShowReportByDate() {
        if (showReportByDate == null) {
            showReportByDate = Boolean.FALSE;
        }

        return showReportByDate;
    }

    public void setShowReportByDate(Boolean showReportByDate) {
        if (showReportByDate == null) {
            this.showReportByDate = Boolean.FALSE;
        } else {
            this.showReportByDate = showReportByDate;
        }
    }

    @Column(name = "BYDATE")
    public Integer getByDate() {
        return byDate;
    }

    public void setByDate(Integer byDate) {
        this.byDate = byDate;
    }

    @Column(name = "GROUPBYORDER")
    public Integer getGroupByOrder() {
        return groupByOrder;
    }

    public void setGroupByOrder(Integer groupByOrder) {
        this.groupByOrder = groupByOrder;
    }

    @Column(name = "SHOWTOP")
    public Integer getShowTop() {
        return showTop;
    }

    public void setShowTop(Integer showTop) {
        this.showTop = showTop;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TICKETFILTERID")
    public TicketFilter getTicketFilter() {
        return ticketFilter;
    }

    public void setTicketFilter(TicketFilter ticketFilter) {
        this.ticketFilter = ticketFilter;
    }

    @IgnoreProperty
    @OneToOne(mappedBy = "report", fetch = FetchType.LAZY)
    @Cascade(CascadeType.ALL)
    public ReportRecurrenceSchedule getSchedule() {
        return schedule;
    }

    public void setSchedule(ReportRecurrenceSchedule schedule) {
        this.schedule = schedule;
    }

    @Column(name = "SHOWSCHEDULE")
    public Boolean getShowRecurrenceSchedule() {
        return showRecurrenceSchedule;
    }

    public void setShowRecurrenceSchedule(Boolean showRecurrenceSchedule) {
        this.showRecurrenceSchedule = showRecurrenceSchedule;
    }
}
