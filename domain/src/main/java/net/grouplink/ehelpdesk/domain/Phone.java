package net.grouplink.ehelpdesk.domain;

import net.grouplink.ehelpdesk.domain.asset.Vendor;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * Represents a <code>Phone</code> in the eHelpDesk system.
 */
@Entity
@Table(name = "PHONE")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Phone implements Serializable {

    public static final String PHONETYPE_HOME = "Home";
    public static final String PHONETYPE_OFFICE = "Office";
    public static final String PHONETYPE_CELL = "Cell";
    public static final String PHONETYPE_FAX = "Fax";

    private Integer id;
    private User user;
    private Vendor vendor;
    private Boolean primaryPhone;
    private String phoneType;
    private String number;
    private String extension;
    private String bestTimeToCall;

    private boolean marked;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "VENDORID")
    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    @Column(name = "EXTENSION")
    public String getExtension() {
        return extension;
    }

    public void setExtension(String extention) {
        this.extension = extention;
    }

    @Column(name = "PHONENUMBER")
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Column(name = "PHONETYPE")
    public String getPhoneType() {
        return phoneType;
    }

    public void setPhoneType(String phoneType) {
        this.phoneType = phoneType;
    }

    @Column(name = "PRIMARYPHONE")
    public Boolean getPrimaryPhone() {
        return primaryPhone;
    }

    public void setPrimaryPhone(Boolean primaryPhone) {
        this.primaryPhone = primaryPhone;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USERID")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Column(name = "BESTTIMETOCALL")
    public String getBestTimeToCall() {
        return bestTimeToCall;
    }

    public void setBestTimeToCall(String bestTimeToCall) {
        this.bestTimeToCall = bestTimeToCall;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Phone)) return false;

        Phone phone = (Phone) o;

        if (bestTimeToCall != null ? !bestTimeToCall.equals(phone.bestTimeToCall) : phone.bestTimeToCall != null)
            return false;
        if (extension != null ? !extension.equals(phone.extension) : phone.extension != null) return false;
        if (number != null ? !number.equals(phone.number) : phone.number != null) return false;
        if (phoneType != null ? !phoneType.equals(phone.phoneType) : phone.phoneType != null) return false;
        if (primaryPhone != null ? !primaryPhone.equals(phone.primaryPhone) : phone.primaryPhone != null) return false;
        else assert true;

        return true;
    }

    @Override
    public int hashCode() {
        int result = primaryPhone != null ? primaryPhone.hashCode() : 0;
        result = 31 * result + (phoneType != null ? phoneType.hashCode() : 0);
        result = 31 * result + (number != null ? number.hashCode() : 0);
        result = 31 * result + (extension != null ? extension.hashCode() : 0);
        result = 31 * result + (bestTimeToCall != null ? bestTimeToCall.hashCode() : 0);
        return result;
    }
}
