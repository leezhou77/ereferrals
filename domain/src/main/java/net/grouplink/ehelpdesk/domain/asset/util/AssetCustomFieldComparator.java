package net.grouplink.ehelpdesk.domain.asset.util;

import net.grouplink.ehelpdesk.domain.asset.AssetCustomField;

import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Nov 19, 2008
 * Time: 6:28:11 PM
 */
public class AssetCustomFieldComparator implements Comparator<AssetCustomField> {

    public int compare(AssetCustomField cf1, AssetCustomField cf2) {
        Integer cf1Order = cf1.getFieldOrder();
        Integer cf2Order = cf2.getFieldOrder();

        if (cf1Order < cf2Order)
            return -1;
        else {
            if (cf1Order.intValue() == cf2Order.intValue()) {
                String cf1Name = cf1.getName();
                String cf2Name = cf2.getName();
                return cf1Name.compareTo(cf2Name);
            } else {
                return 1;
            }
        }
    }
}
