package net.grouplink.ehelpdesk.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Represents a <code>LdapField</code> in the eHelpDesk system.
 */
@Entity
@Table(name = "LDAPFIELDS")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class LdapField implements Serializable, Comparable {

    private Integer id;
    private String name;
    private String label;
    private Boolean showOnTicket = Boolean.FALSE;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "showOnTicket")
    public Boolean getShowOnTicket() {
        return showOnTicket;
    }

    public void setShowOnTicket(Boolean showOnTicket) {
        this.showOnTicket = showOnTicket;
    }

    @Column(name = "LABEL")
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int compareTo(Object o) {
        return String.CASE_INSENSITIVE_ORDER.compare(name, ((LdapField) o).getName());
    }
}
