package net.grouplink.ehelpdesk.domain.acl;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ACL_PERMISSION")
public class Permission implements Serializable {

    public static final Integer PERMISSIONTYPE_GLOBAL = 1;
    public static final Integer PERMISSIONTYPE_SCHEME = 2;
    public static final Integer PERMISSIONTYPE_FIELD = 3;

    private Integer id;
    private String key;
    private Integer permissionType;
    private PermissionSubject permissionSubject;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "PERMISSIONTYPE")
    public Integer getPermissionType() {
        return permissionType;
    }

    public void setPermissionType(Integer permissionType) {
        this.permissionType = permissionType;
    }

    @Column(name = "PERMKEY")
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "PERMISSIONSUBJECTID")
    public PermissionSubject getPermissionSubject() {
        return permissionSubject;
    }

    public void setPermissionSubject(PermissionSubject permissionSubject) {
        this.permissionSubject = permissionSubject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Permission)) return false;

        Permission that = (Permission) o;

        if (!key.equals(that.key)) return false;
        if (!permissionType.equals(that.permissionType)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = key.hashCode();
        result = 31 * result + permissionType.hashCode();
        return result;
    }
}
