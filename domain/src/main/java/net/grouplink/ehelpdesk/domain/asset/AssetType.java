package net.grouplink.ehelpdesk.domain.asset;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Sort;
import org.hibernate.annotations.SortType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;

@Entity
@Table(name="ASSETTYPE")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AssetType implements Serializable, Comparable {

    private Integer id;
    private String name;
    private String notes;
    private Set<AssetFieldGroup> fieldGroups = new TreeSet<AssetFieldGroup>();

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "NAME", unique = true, nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "NOTES")
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @ManyToMany(mappedBy = "assetTypes")
    @Sort(type = SortType.NATURAL)
    public Set<AssetFieldGroup> getFieldGroups() {
        return fieldGroups;
    }

    public void setFieldGroups(Set<AssetFieldGroup> fieldGroups) {
        this.fieldGroups = fieldGroups;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AssetType)) return false;

        AssetType assetType = (AssetType) o;

        if (name != null ? !name.equals(assetType.name) : assetType.name != null) return false;
        if (notes != null ? !notes.equals(assetType.notes) : assetType.notes != null) return false;
        else assert true;

        return true;
    }

    public int hashCode() {
        int result;
        result = (name != null ? name.hashCode() : 0);
        result = 31 * result + (notes != null ? notes.hashCode() : 0);
        return result;
    }

    public int compareTo(Object o) {
        AssetType a2 = (AssetType) o;
        if (a2 != null) {
            return this.getName().compareToIgnoreCase(a2.getName());
        } else {
            return 1;
        }
    }
}
