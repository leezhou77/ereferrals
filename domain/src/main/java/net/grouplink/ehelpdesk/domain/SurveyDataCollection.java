package net.grouplink.ehelpdesk.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Jul 22, 2008
 * Time: 7:12:55 PM
 */
public class SurveyDataCollection implements Serializable {

    private List<SurveyData> surveyDataList = new ArrayList<SurveyData>();

    public boolean addSurveyData(SurveyData surveyData) {
        return surveyDataList.add(surveyData);
    }

    public SurveyData getSurveyData(int index) {
        return surveyDataList.get(index);
    }

    public List<SurveyData> getAllSurveyData() {
        return surveyDataList;
    }
}
