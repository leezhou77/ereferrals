package net.grouplink.ehelpdesk.domain;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;

public class AdvancedTicketsFilter implements Serializable {
	
	public static final int NO_GROUP_ID = -2;
	public static final int NO_LOCATION_ID = -2;
	
	private int searcherId = 0;


    private String ticketNumber;
    private String locs;
    private String grouping;

    private Category category;
	private CategoryOption categoryOption;
	private Group group;
    private List<Integer> groupList = new ArrayList<Integer>();
    private Location location;
	private TicketPriority priority;
	private Status ticketStatus;
	
	//private String category = null;
	private String submittedByName;
	private Integer submittedById;
    private Integer assignedTo;
    private String contactName;
	private Integer contactId;
	private Integer locationId;

    private String subject;
    private String note;
	private String comments;
	
	private String createDateOperator;
    private String creationDate;
    private String creationDate2;
	
	private String modifiedDateOperator;
	private String modifiedDate;
	private String modifiedDate2 ;
	
	private String estimatedDateOperator;
	private String estimatedDate;
	private String estimatedDate2;
	
	private String assignedToName;
	private String assignedToTicketPool;

	private String workTimeOperator;
	private String workTimeHours;
	private String workTimeMinutes;

    public List<Integer> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<Integer> groupList) {
        this.groupList = groupList;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Integer getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(Integer assignedTo) {
        this.assignedTo = assignedTo;
    }

	public String getWorkTimeOperator() {
		return workTimeOperator;
	}

	public void setWorkTimeOperator(String workTimeOperator) {
		this.workTimeOperator = workTimeOperator;
	}

	public Integer getContactId() {
		return contactId;
	}

	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public Integer getSubmittedById() {
		return submittedById;
	}

	public void setSubmittedById(Integer submittedById) {
		this.submittedById = submittedById;
	}

	public String getSubmittedByName() {
		return submittedByName;
	}

	public void setSubmittedByName(String submittedByName) {
		this.submittedByName = submittedByName;
	}

	public String getAssignedToName() {
		return assignedToName;
	}

	public void setAssignedToName(String assignedToName) {
		this.assignedToName = assignedToName;
	}

	public String getCreationDate2() {
		return creationDate2;
	}

	public void setCreationDate2(String creationDate2) {
		this.creationDate2 = creationDate2;
	}

	public String getCreateDateOperator() {
		return createDateOperator;
	}

	public void setCreateDateOperator(String createDateOperator) {
		this.createDateOperator = createDateOperator;
	}

	public String getEstimatedDate() {
		return estimatedDate;
	}

	public void setEstimatedDate(String estimatedDate) {
		this.estimatedDate = estimatedDate;
	}

	public String getEstimatedDate2() {
		return estimatedDate2;
	}

	public void setEstimatedDate2(String estimatedDate2) {
		this.estimatedDate2 = estimatedDate2;
	}

	public String getEstimatedDateOperator() {
		return estimatedDateOperator;
	}

	public void setEstimatedDateOperator(String estimatedDateOperator) {
		this.estimatedDateOperator = estimatedDateOperator;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedDate2() {
		return modifiedDate2;
	}

	public void setModifiedDate2(String modifiedDate2) {
		this.modifiedDate2 = modifiedDate2;
	}

	public String getModifiedDateOperator() {
		return modifiedDateOperator;
	}

	public void setModifiedDateOperator(String modifiedDateOperator) {
		this.modifiedDateOperator = modifiedDateOperator;
	}

	public String getAssignedToTicketPool() {
		return assignedToTicketPool;
	}

	public void setAssignedToTicketPool(String assignedToTicketPool) {
		this.assignedToTicketPool = assignedToTicketPool;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public int getSearcherId() {
		return searcherId;
	}

	public void setSearcherId(int searcherId) {
		this.searcherId = searcherId;
	}

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getLocs() {
        return locs;
    }

    public void setLocs(String locs) {
        this.locs = locs;
    }

    public String getGrouping() {
        return grouping;
    }

    public void setGrouping(String grouping) {
        this.grouping = grouping;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public String getWorkTimeHours() {
        return workTimeHours;
    }

    public void setWorkTimeHours(String workTimeHours) {
        this.workTimeHours = workTimeHours;
    }

    public String getWorkTimeMinutes() {
        return workTimeMinutes;
    }

    public void setWorkTimeMinutes(String workTimeMinutes) {
        this.workTimeMinutes = workTimeMinutes;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public CategoryOption getCategoryOption() {
        return categoryOption;
    }

    public void setCategoryOption(CategoryOption categoryOption) {
        this.categoryOption = categoryOption;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public TicketPriority getPriority() {
        return priority;
    }

    public void setPriority(TicketPriority priority) {
        this.priority = priority;
    }

    public Status getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(Status ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AdvancedTicketsFilter)) return false;

        AdvancedTicketsFilter that = (AdvancedTicketsFilter) o;

        if (searcherId != that.searcherId) return false;
        if (assignedTo != null ? !assignedTo.equals(that.assignedTo) : that.assignedTo != null) return false;
        if (assignedToName != null ? !assignedToName.equals(that.assignedToName) : that.assignedToName != null)
            return false;
        if (assignedToTicketPool != null ? !assignedToTicketPool.equals(that.assignedToTicketPool) : that.assignedToTicketPool != null)
            return false;
        if (category != null ? !category.equals(that.category) : that.category != null) return false;
        if (categoryOption != null ? !categoryOption.equals(that.categoryOption) : that.categoryOption != null)
            return false;
        if (comments != null ? !comments.equals(that.comments) : that.comments != null) return false;
        if (contactId != null ? !contactId.equals(that.contactId) : that.contactId != null) return false;
        if (contactName != null ? !contactName.equals(that.contactName) : that.contactName != null) return false;
        if (createDateOperator != null ? !createDateOperator.equals(that.createDateOperator) : that.createDateOperator != null)
            return false;
        if (creationDate != null ? !creationDate.equals(that.creationDate) : that.creationDate != null) return false;
        if (creationDate2 != null ? !creationDate2.equals(that.creationDate2) : that.creationDate2 != null)
            return false;
        if (estimatedDate != null ? !estimatedDate.equals(that.estimatedDate) : that.estimatedDate != null)
            return false;
        if (estimatedDate2 != null ? !estimatedDate2.equals(that.estimatedDate2) : that.estimatedDate2 != null)
            return false;
        if (estimatedDateOperator != null ? !estimatedDateOperator.equals(that.estimatedDateOperator) : that.estimatedDateOperator != null)
            return false;
        if (group != null ? !group.equals(that.group) : that.group != null) return false;
        if (grouping != null ? !grouping.equals(that.grouping) : that.grouping != null) return false;
        if (location != null ? !location.equals(that.location) : that.location != null) return false;
        if (locationId != null ? !locationId.equals(that.locationId) : that.locationId != null) return false;
        if (locs != null ? !locs.equals(that.locs) : that.locs != null) return false;
        if (modifiedDate != null ? !modifiedDate.equals(that.modifiedDate) : that.modifiedDate != null) return false;
        if (modifiedDate2 != null ? !modifiedDate2.equals(that.modifiedDate2) : that.modifiedDate2 != null)
            return false;
        if (modifiedDateOperator != null ? !modifiedDateOperator.equals(that.modifiedDateOperator) : that.modifiedDateOperator != null)
            return false;
        if (note != null ? !note.equals(that.note) : that.note != null) return false;
        if (priority != null ? !priority.equals(that.priority) : that.priority != null) return false;
        if (subject != null ? !subject.equals(that.subject) : that.subject != null) return false;
        if (submittedById != null ? !submittedById.equals(that.submittedById) : that.submittedById != null)
            return false;
        if (submittedByName != null ? !submittedByName.equals(that.submittedByName) : that.submittedByName != null)
            return false;
        if (ticketNumber != null ? !ticketNumber.equals(that.ticketNumber) : that.ticketNumber != null) return false;
        if (ticketStatus != null ? !ticketStatus.equals(that.ticketStatus) : that.ticketStatus != null) return false;
        if (workTimeHours != null ? !workTimeHours.equals(that.workTimeHours) : that.workTimeHours != null)
            return false;
        if (workTimeMinutes != null ? !workTimeMinutes.equals(that.workTimeMinutes) : that.workTimeMinutes != null)
            return false;
        if (workTimeOperator != null ? !workTimeOperator.equals(that.workTimeOperator) : that.workTimeOperator != null)
            return false;
        else assert true;
        return true;
    }

    @Override
    public int hashCode() {
        int result = searcherId;
        result = 31 * result + (ticketNumber != null ? ticketNumber.hashCode() : 0);
        result = 31 * result + (locs != null ? locs.hashCode() : 0);
        result = 31 * result + (grouping != null ? grouping.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (categoryOption != null ? categoryOption.hashCode() : 0);
        result = 31 * result + (group != null ? group.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + (priority != null ? priority.hashCode() : 0);
        result = 31 * result + (ticketStatus != null ? ticketStatus.hashCode() : 0);
        result = 31 * result + (submittedByName != null ? submittedByName.hashCode() : 0);
        result = 31 * result + (submittedById != null ? submittedById.hashCode() : 0);
        result = 31 * result + (assignedTo != null ? assignedTo.hashCode() : 0);
        result = 31 * result + (contactName != null ? contactName.hashCode() : 0);
        result = 31 * result + (contactId != null ? contactId.hashCode() : 0);
        result = 31 * result + (locationId != null ? locationId.hashCode() : 0);
        result = 31 * result + (subject != null ? subject.hashCode() : 0);
        result = 31 * result + (note != null ? note.hashCode() : 0);
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        result = 31 * result + (createDateOperator != null ? createDateOperator.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (creationDate2 != null ? creationDate2.hashCode() : 0);
        result = 31 * result + (modifiedDateOperator != null ? modifiedDateOperator.hashCode() : 0);
        result = 31 * result + (modifiedDate != null ? modifiedDate.hashCode() : 0);
        result = 31 * result + (modifiedDate2 != null ? modifiedDate2.hashCode() : 0);
        result = 31 * result + (estimatedDateOperator != null ? estimatedDateOperator.hashCode() : 0);
        result = 31 * result + (estimatedDate != null ? estimatedDate.hashCode() : 0);
        result = 31 * result + (estimatedDate2 != null ? estimatedDate2.hashCode() : 0);
        result = 31 * result + (assignedToName != null ? assignedToName.hashCode() : 0);
        result = 31 * result + (assignedToTicketPool != null ? assignedToTicketPool.hashCode() : 0);
        result = 31 * result + (workTimeOperator != null ? workTimeOperator.hashCode() : 0);
        result = 31 * result + (workTimeHours != null ? workTimeHours.hashCode() : 0);
        result = 31 * result + (workTimeMinutes != null ? workTimeMinutes.hashCode() : 0);
        return result;
    }
}
