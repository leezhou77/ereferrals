package net.grouplink.ehelpdesk.domain.constants;

/**
 * Created by IntelliJ IDEA.
 * User: dgarcia
 * Date: May 22, 2008
 * Time: 10:37:04 AM
 */
public class JasperReportsConstants {

    public static final int GROUP = 1;
    public static final int LOCATION = 2;
    public static final int CATEGORY = 3;
    public static final int CATEGORY_OPTION = 4;
    public static final int TECHNICIAN = 5;
    public static final int PRIORITY = 6;
    public static final int STATUS = 7;
    public static final int ZENASSET = 8;

    public static final int ASSET_CATEGORIES = 8;
    public static final int ASSET_DEPARTMENT = 9;
    public static final int ASSET_LOCATION = 10;
    public static final int ASSET_MANUFACTURE = 11;
    public static final int ASSET_PERSON = 12;
    public static final int ASSET_STATUS = 13;
    public static final int ASSET_TYPE = 14;
    public static final int ASSET_VENDOR = 15;

    public static final String JASPER_REPORTS_FILTER = "jasperReportsFilter";

    public static final int GROUP_BY_ORDER_ALPHA = 1;
    public static final int GROUP_BY_ORDER_TICKETS = 2;

    public static final int FIRST_DRILL_DOWN_LEVEL = 0;
    public static final int SECOND_DRILL_DOWN_LEVEL = 1;
    public static final int THIRD_DRILL_DOWN_LEVEL = 2;
}
