package net.grouplink.ehelpdesk.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Represents a <code>Status</code> in the eHelpDesk system.
 */
@Entity
@Table(name = "STATUS")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Status implements Serializable, Comparable {

    public static Integer TT_PENDING = -2;
    public static Integer DELETED = -1;
    public static Integer AWAITING_DISPATCH = 1;
    public static Integer ASSIGNED_NOT_UPDATED = 2;
    public static Integer CLOSED = 6;

    private Integer id;
	private String name;
    private Integer order;
    private Boolean active = Boolean.TRUE;
//    private Set<Ticket> tickets = new HashSet<Ticket>();

    public Status() {
    }

    public Status(Integer id, String name, Integer order, Boolean active) {
        this.id = id;
        this.name = name;
        this.order = order;
        this.active = active;
    }

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "ACTIVE")
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Column(name = "NAME", length = 50)
    @NotEmpty
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns all Ticket's that has this Status assigned to it
	 * @return a Set of Tickets
	 */
//    @OneToMany(mappedBy = "status")
//    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.PERSIST})
//    public Set<Ticket> getTickets() {
//		return tickets;
//	}
//
//	/**
//	 * Sets a group of Ticket's to this Status
//	 * @param tickets ticketsSubmited
//	 */
//	public void setTickets(Set<Ticket> tickets) {
//		this.tickets = tickets;
//	}

    @Column(name = "SORDER")
    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public int compareTo(Object o) {
        return String.CASE_INSENSITIVE_ORDER.compare(name, ((Status)o).getName());
    }

    @Override
    public String toString() {
        return "Status{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", order=" + order +
                ", active=" + active +
                '}';
    }
}
