package net.grouplink.ehelpdesk.domain.acl;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ACL_PERMISSIONSUBJECTS")
public class PermissionSubject implements Serializable {
    private Integer id;
    private String modelName;
    private String modelClass;
    private Integer objId;
    private String modelField;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "MODELNAME")
    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    @Column(name = "MODELCLASS")
    public String getModelClass() {
        return modelClass;
    }

    public void setModelClass(String modelClass) {
        this.modelClass = modelClass;
    }

    @Column(name = "OBJID")
    public Integer getObjId() {
        return objId;
    }

    public void setObjId(Integer objId) {
        this.objId = objId;
    }

    @Column(name = "MODELFIELD")
    public String getModelField() {
        return modelField;
    }

    public void setModelField(String modelField) {
        this.modelField = modelField;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PermissionSubject)) return false;

        PermissionSubject that = (PermissionSubject) o;

        if (modelClass != null ? !modelClass.equals(that.modelClass) : that.modelClass != null) return false;
        if (modelField != null ? !modelField.equals(that.modelField) : that.modelField != null) return false;
        if (modelName != null ? !modelName.equals(that.modelName) : that.modelName != null) return false;
        if (objId != null ? !objId.equals(that.objId) : that.objId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = modelName != null ? modelName.hashCode() : 0;
        result = 31 * result + (modelClass != null ? modelClass.hashCode() : 0);
        result = 31 * result + (objId != null ? objId.hashCode() : 0);
        result = 31 * result + (modelField != null ? modelField.hashCode() : 0);
        return result;
    }
}
