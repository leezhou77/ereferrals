package net.grouplink.ehelpdesk.domain.acl;

import net.grouplink.ehelpdesk.domain.Group;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "ACL_PERMISSIONSCHEME")
public class PermissionScheme implements Serializable {
    private Integer id;
    private String name;
    private Set<PermissionSchemeEntry> permissionSchemeEntries = new HashSet<PermissionSchemeEntry>();
    private Set<Group> groups = new HashSet<Group>();

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "NAME")
    @NotEmpty
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "permissionScheme", cascade = CascadeType.ALL)
    public Set<PermissionSchemeEntry> getPermissionSchemeEntries() {
        return permissionSchemeEntries;
    }

    public void setPermissionSchemeEntries(Set<PermissionSchemeEntry> permissionSchemeEntries) {
        this.permissionSchemeEntries = permissionSchemeEntries;
    }

    public PermissionSchemeEntry getPermissionSchemeEntry(Permission permission) {
        for (PermissionSchemeEntry pse : permissionSchemeEntries) {
            if (pse.getPermission().equals(permission)) {
                return pse;
            }
        }

        return null;
    }

    public void addPermissionSchemeEntry(PermissionSchemeEntry entry) {
        entry.setPermissionScheme(this);
        permissionSchemeEntries.add(entry);
    }

    @OneToMany(mappedBy = "permissionScheme")
    public Set<Group> getGroups() {
        return groups;
    }

    public void setGroups(Set<Group> groups) {
        this.groups = groups;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PermissionScheme)) return false;

        PermissionScheme that = (PermissionScheme) o;

        if (!name.equals(that.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
