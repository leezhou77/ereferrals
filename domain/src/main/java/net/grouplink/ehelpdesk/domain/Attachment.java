package net.grouplink.ehelpdesk.domain;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author mrollins
 * @version 1.0
 */
@Entity
@Table(name = "ATTACHMENTS")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Attachment implements Serializable, Cloneable {
    
    private Integer id;
	private String fileName;
	private String contentType;
	private byte[] fileData;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer attachmentId) {
        this.id = attachmentId;
    }

    @Column(name = "FILENAME")
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Column(name = "CONTENTTYPE")
    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "FILEDATA", length = 5000000)
    @Type(type = "binary", parameters = {@Parameter(name = "length", value = "5000000")})
    public byte[] getFileData() {
        return fileData;
    }

    public void setFileData(byte[] fileData) {
        this.fileData = fileData;
    }

    @Override
    public Attachment clone() throws CloneNotSupportedException {
        Attachment attachment = (Attachment) super.clone();
        attachment.setId(null);
        return attachment;
    }
}
