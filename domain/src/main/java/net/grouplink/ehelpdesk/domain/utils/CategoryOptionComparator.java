package net.grouplink.ehelpdesk.domain.utils;

import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.Group;

import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Nov 14, 2008
 * Time: 10:42:30 AM
 */
public class CategoryOptionComparator implements Comparator<CategoryOption> {

    public int compare(CategoryOption catOpt1, CategoryOption catOpt2) {
        String co1Name = catOpt1.getName();
        String co2Name = catOpt2.getName();
        Category cat1 = catOpt1.getCategory();
        Category cat2 = catOpt2.getCategory();
        String c1Name = cat1.getName();
        String c2Name = cat2.getName();
        Group group1 =  cat1.getGroup();
        Group group2 = cat2.getGroup();
        String g1Name = group1.getName();
        String g2Name = group2.getName();

        int comp = g1Name.compareTo(g2Name);
        if (comp != 0)
            return comp;
        else {
            comp = c1Name.compareTo(c2Name);
            if (comp != 0)
                return comp;
            else {
                return co1Name.compareTo(co2Name);
            }
        }
    }
}
