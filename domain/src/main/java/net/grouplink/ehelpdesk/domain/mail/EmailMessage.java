package net.grouplink.ehelpdesk.domain.mail;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: jaymehafen
 * Date: Jan 23, 2008
 * Time: 4:20:08 PM
 */
public class EmailMessage implements Serializable {
    private static Log log = LogFactory.getLog(EmailMessage.class);

    /** The entire from address */
    private String from;
    /** The email address portion of the from field */
    private String fromAddress;
    /** The personal name portion of the from field */
    private String fromPersonalName;
    private List<String> ccAddresses = new ArrayList<String>();
    private String subject;
    private Date sentDate;
    /** The text/plain part of the message */
    private String textContent;
    /** The text/html part of the mesage */
    private String htmlContent;
    private List<EmailAttachment> attachments = new ArrayList<EmailAttachment>();
    private int messageNumber;
    String fromRegex = "(.*)<(.*)>";

    public String getFromAddress() {
        if (fromAddress == null) {
            // from address wasn't set by MailMonitor, try to parse it
            if (from.matches(fromRegex)) {
                Pattern p = Pattern.compile(fromRegex);
                Matcher m = p.matcher(from);
                m.find();
                fromAddress = m.group(2);
            } else {
                fromAddress = from;
            }
        }

        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public String getTextContent() {
        return textContent;
    }

    public void setTextContent(String textContent) {
        this.textContent = textContent;
    }

    public String getHtmlContent() {
        return htmlContent;
    }

    public void setHtmlContent(String htmlContent) {
        this.htmlContent = htmlContent;
    }

    public List<EmailAttachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<EmailAttachment> attachments) {
        this.attachments = attachments;
    }

    public void addAttachment(EmailAttachment emailAttachment) {
        attachments.add(emailAttachment);
    }

    public int getMessageNumber() {
        return messageNumber;
    }

    public void setMessageNumber(int messageNumber) {
        this.messageNumber = messageNumber;
    }

    public void deleteAttachments() {
        for (EmailAttachment emailAttachment : attachments) {
            boolean deleted = emailAttachment.getFile().delete();
            log.debug("attachment file " + emailAttachment.getFile().getAbsolutePath() + " deleted: " + deleted);
        }
    }

    public String getFromPersonalName() {
        if (fromPersonalName == null) {
            if (from.matches(fromRegex)) {
                Pattern p = Pattern.compile(fromRegex);
                Matcher m = p.matcher(from);
                m.find();
                fromPersonalName = StringUtils.isNotBlank(m.group(1)) ? m.group(1).trim() : null;
            }
        }

        return fromPersonalName;
    }

    public void setFromPersonalName(String fromPersonalName) {
        this.fromPersonalName = fromPersonalName;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public List<String> getCcAddresses() {
        return ccAddresses;
    }

    public void setCcAddresses(List<String> ccAddresses) {
        this.ccAddresses = ccAddresses;
    }

    public void addCcAddress(String address) {
        ccAddresses.add(address);
    }
}
