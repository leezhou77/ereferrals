package net.grouplink.ehelpdesk.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.FetchType;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "RECURRENCESCHEDULE")
public class RecurrenceSchedule implements Serializable, Cloneable {

    private Integer id;
    private TicketTemplateMaster ticketTemplateMaster;
    private ScheduleStatus scheduleStatus;
    private Report report;

    // Recurrence Pattern
    public static final int PATTERN_DAILY = 1;
    public static final int PATTERN_WEEKLY = 2;
    public static final int PATTERN_MONTHLY = 3;
    public static final int PATTERN_YEARLY = 4;
    private Integer pattern;

    // Daily or Weekly
    private Integer dailyInterval;
    private Integer weeklyInterval;

    // Weekly
    private Boolean onSunday = Boolean.FALSE;
    private Boolean onMonday = Boolean.FALSE;
    private Boolean onTuesday = Boolean.FALSE;
    private Boolean onWednesday = Boolean.FALSE;
    private Boolean onThursday = Boolean.FALSE;
    private Boolean onFriday = Boolean.FALSE;
    private Boolean onSaturday = Boolean.FALSE;

    // Monthly
    public static final int MONTHLY_DAY_OF_MONTH = 1;
    public static final int MONTHLY_DAY_OF_WEEK = 2;
    private Integer monthlySchedule;
    private Integer monthlyDayOfMonth;
    private Integer monthlyDayOfMonthOccurrence;
    private Integer monthlyDayOfWeekInterval;
    private Integer monthlyDayOfWeekDay;
    private Integer monthlyDayOfWeekOccurrence;

    // Yearly
    public static final int YEARLY_DAY_OF_YEAR = 1;
    public static final int YEARLY_DAY_OF_MONTH = 2;
    private Integer yearlySchedule;
    private Integer yearlyDayOfYearDay;
    private Integer yearlyDayOfYearMonth;
    private Integer yearlyDayOfMonthInterval;
    private Integer yearlyDayOfMonthDay;
    private Integer yearlyDayOfMonthMonth;

    // Range of Occurence
    public static final int RANGE_END_AFTER = 1;
    public static final int RANGE_END_BY = 2;
    public static final int RANGE_END_NEVER = 3;

    private Date rangeStartDate;
    private Integer rangeEnd;
    private Integer rangeEndAfterOccurence;
    private Date rangeEndByDate;

    // Other scheduler properties
    private Boolean running = Boolean.FALSE;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TICKETTEMPLATEMASTERID")
    public TicketTemplateMaster getTicketTemplateMaster() {
        return ticketTemplateMaster;
    }

    public void setTicketTemplateMaster(TicketTemplateMaster ticketTemplateMaster) {
        this.ticketTemplateMaster = ticketTemplateMaster;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SCHEDULESTATUSID")
    public ScheduleStatus getScheduleStatus() {
        return scheduleStatus;
    }

    public void setScheduleStatus(ScheduleStatus scheduleStatus) {
        this.scheduleStatus = scheduleStatus;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REPORTID")
    public Report getReport() {
        return report;
    }

    public void setReport(Report report) {
        this.report = report;
    }

    public Integer getPattern() {
        return pattern;
    }

    public void setPattern(Integer pattern) {
        this.pattern = pattern;
    }

    public Integer getDailyInterval() {
        return dailyInterval;
    }

    public void setDailyInterval(Integer dailyInterval) {
        this.dailyInterval = dailyInterval;
    }

    public Boolean getOnSunday() {
        return onSunday;
    }

    public void setOnSunday(Boolean onSunday) {
        this.onSunday = onSunday;
    }

    public Boolean getOnMonday() {
        return onMonday;
    }

    public void setOnMonday(Boolean onMonday) {
        this.onMonday = onMonday;
    }

    public Boolean getOnTuesday() {
        return onTuesday;
    }

    public void setOnTuesday(Boolean onTuesday) {
        this.onTuesday = onTuesday;
    }

    public Boolean getOnWednesday() {
        return onWednesday;
    }

    public void setOnWednesday(Boolean onWednesday) {
        this.onWednesday = onWednesday;
    }

    public Boolean getOnThursday() {
        return onThursday;
    }

    public void setOnThursday(Boolean onThursday) {
        this.onThursday = onThursday;
    }

    public Boolean getOnFriday() {
        return onFriday;
    }

    public void setOnFriday(Boolean onFriday) {
        this.onFriday = onFriday;
    }

    public Boolean getOnSaturday() {
        return onSaturday;
    }

    public void setOnSaturday(Boolean onSaturday) {
        this.onSaturday = onSaturday;
    }

    public Integer getMonthlySchedule() {
        return monthlySchedule;
    }

    public void setMonthlySchedule(Integer monthlySchedule) {
        this.monthlySchedule = monthlySchedule;
    }

    public Integer getMonthlyDayOfMonth() {
        return monthlyDayOfMonth;
    }

    public void setMonthlyDayOfMonth(Integer monthlyDayOfMonth) {
        this.monthlyDayOfMonth = monthlyDayOfMonth;
    }

    public Integer getMonthlyDayOfMonthOccurrence() {
        return monthlyDayOfMonthOccurrence;
    }

    public void setMonthlyDayOfMonthOccurrence(Integer monthlyDayOfMonthOccurrence) {
        this.monthlyDayOfMonthOccurrence = monthlyDayOfMonthOccurrence;
    }

    public Integer getMonthlyDayOfWeekInterval() {
        return monthlyDayOfWeekInterval;
    }

    public void setMonthlyDayOfWeekInterval(Integer monthlyDayOfWeekInterval) {
        this.monthlyDayOfWeekInterval = monthlyDayOfWeekInterval;
    }

    public Integer getMonthlyDayOfWeekDay() {
        return monthlyDayOfWeekDay;
    }

    public void setMonthlyDayOfWeekDay(Integer monthlyDayOfWeekDay) {
        this.monthlyDayOfWeekDay = monthlyDayOfWeekDay;
    }

    public Integer getMonthlyDayOfWeekOccurrence() {
        return monthlyDayOfWeekOccurrence;
    }

    public void setMonthlyDayOfWeekOccurrence(Integer monthlyDayOfWeekOccurrence) {
        this.monthlyDayOfWeekOccurrence = monthlyDayOfWeekOccurrence;
    }

    public Integer getYearlySchedule() {
        return yearlySchedule;
    }

    public void setYearlySchedule(Integer yearlySchedule) {
        this.yearlySchedule = yearlySchedule;
    }

    public Integer getYearlyDayOfYearDay() {
        return yearlyDayOfYearDay;
    }

    public void setYearlyDayOfYearDay(Integer yearlyDayOfYearDay) {
        this.yearlyDayOfYearDay = yearlyDayOfYearDay;
    }

    public Integer getYearlyDayOfYearMonth() {
        return yearlyDayOfYearMonth;
    }

    public void setYearlyDayOfYearMonth(Integer yearlyDayOfYearMonth) {
        this.yearlyDayOfYearMonth = yearlyDayOfYearMonth;
    }

    public Integer getYearlyDayOfMonthInterval() {
        return yearlyDayOfMonthInterval;
    }

    public void setYearlyDayOfMonthInterval(Integer yearlyDayOfMonthInterval) {
        this.yearlyDayOfMonthInterval = yearlyDayOfMonthInterval;
    }

    public Integer getYearlyDayOfMonthDay() {
        return yearlyDayOfMonthDay;
    }

    public void setYearlyDayOfMonthDay(Integer yearlyDayOfMonthDay) {
        this.yearlyDayOfMonthDay = yearlyDayOfMonthDay;
    }

    public Integer getYearlyDayOfMonthMonth() {
        return yearlyDayOfMonthMonth;
    }

    public void setYearlyDayOfMonthMonth(Integer yearlyDayOfMonthMonth) {
        this.yearlyDayOfMonthMonth = yearlyDayOfMonthMonth;
    }

    @Temporal(TemporalType.DATE)
    public Date getRangeStartDate() {
        return rangeStartDate;
    }

    public void setRangeStartDate(Date rangeStartDate) {
        this.rangeStartDate = rangeStartDate;
    }

    public Integer getRangeEnd() {
        return rangeEnd;
    }

    public void setRangeEnd(Integer rangeEnd) {
        this.rangeEnd = rangeEnd;
    }

    public Integer getRangeEndAfterOccurence() {
        return rangeEndAfterOccurence;
    }

    public void setRangeEndAfterOccurence(Integer rangeEndAfterOccurence) {
        this.rangeEndAfterOccurence = rangeEndAfterOccurence;
    }

    @Temporal(TemporalType.DATE)
    public Date getRangeEndByDate() {
        return rangeEndByDate;
    }

    public void setRangeEndByDate(Date rangeEndByDate) {
        this.rangeEndByDate = rangeEndByDate;
    }

    public Boolean getRunning() {
        return running;
    }

    public void setRunning(Boolean running) {
        this.running = running;
    }

    public Integer getWeeklyInterval() {
        return weeklyInterval;
    }

    public void setWeeklyInterval(Integer weeklyInterval) {
        this.weeklyInterval = weeklyInterval;
    }

    @Override
    public RecurrenceSchedule clone() throws CloneNotSupportedException {
        RecurrenceSchedule clone = (RecurrenceSchedule) super.clone();
        clone.setId(null);
        return clone;
    }
}
