package net.grouplink.ehelpdesk.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "TTRECURSCHED")
public class TicketTemplateRecurrenceSchedule implements Serializable {
    private Integer id;
    private TicketTemplateMaster ticketTemplateMaster;
    private RecurrenceSchedule recurrenceSchedule;
    private Date lastAction;
    private Integer occurrenceCount;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TICKETTEMPLATEMASTERID")
    public TicketTemplateMaster getTicketTemplateMaster() {
        return ticketTemplateMaster;
    }

    public void setTicketTemplateMaster(TicketTemplateMaster ticketTemplateMaster) {
        this.ticketTemplateMaster = ticketTemplateMaster;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RECURRENCESCHEDULEID")
    public RecurrenceSchedule getRecurrenceSchedule() {
        return recurrenceSchedule;
    }

    public void setRecurrenceSchedule(RecurrenceSchedule recurrenceSchedule) {
        this.recurrenceSchedule = recurrenceSchedule;
    }

    @Temporal(TemporalType.TIMESTAMP)
    public Date getLastAction() {
        return lastAction;
    }

    public void setLastAction(Date lastAction) {
        this.lastAction = lastAction;
    }

    public Integer getOccurrenceCount() {
        return occurrenceCount;
    }

    public void setOccurrenceCount(Integer occurrenceCount) {
        this.occurrenceCount = occurrenceCount;
    }
}
