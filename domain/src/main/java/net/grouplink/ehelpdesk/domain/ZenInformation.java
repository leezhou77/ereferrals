package net.grouplink.ehelpdesk.domain;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Lob;
import java.io.Serializable;

/**
 * This class represents the ZENworks information in the HelpDesk System
 */
@Entity
@Table(name = "ZENINFORMATION")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ZenInformation implements Serializable {

	private Integer id;
	private Ticket ticket;
	private String dn;
	private String workstation;
	private String loggedInWorkstation;
	private String wsdn;
	private String ipAddress;
	private String subnetAddress;
	private String macAddress;
	private String diskInfo;
	private String memorySize;
	private String processorType;
	private String biosType;
	private String videoType;
	private String nicType;
	private String ncv;
	private String osRevision;
	private String osType;
	private String assetTag;
	private String serialNumber;
	private String computerModel;
	private String computerType;
	private String userHistory;
	private String lastUser;
	private String operator;
	private String computerName;
	private String modelNumber;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "MODELNUMBER")
	public String getModelNumber() {
		return modelNumber;
	}

	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

    @Column(name = "ASSETTAG")
	public String getAssetTag() {
		return assetTag;
	}

	public void setAssetTag(String assetTag) {
		this.assetTag = assetTag;
	}

    @Column(name = "BIOSTYPE")
	public String getBiosType() {
		return biosType;
	}

	public void setBiosType(String biosType) {
		this.biosType = biosType;
	}

    @Column(name = "COMPUTERMODEL")
	public String getComputerModel() {
		return computerModel;
	}

	public void setComputerModel(String computerModel) {
		this.computerModel = computerModel;
	}

    @Column(name = "COMPUTERNAME", length = 50)
	public String getComputerName() {
		return computerName;
	}

	public void setComputerName(String computerName) {
		this.computerName = computerName;
	}

    @Column(name = "COMPUTERTYPE")
	public String getComputerType() {
		return computerType;
	}

	public void setComputerType(String computerType) {
		this.computerType = computerType;
	}

    @Column(name = "DISKINFO")
	public String getDiskInfo() {
		return diskInfo;
	}

	public void setDiskInfo(String diskInfo) {
		this.diskInfo = diskInfo;
	}

    @Column(name = "DN")
	public String getDn() {
		return dn;
	}

	public void setDn(String dn) {
		this.dn = dn;
	}

    @Column(name = "IPADDRESS")
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

    @Column(name = "LASTUSER")
	public String getLastUser() {
		return lastUser;
	}

	public void setLastUser(String lastUser) {
		this.lastUser = lastUser;
	}

    @Column(name = "LOGGEDINWORKSTATION")
	public String getLoggedInWorkstation() {
		return loggedInWorkstation;
	}

	public void setLoggedInWorkstation(String loggedInWorkstation) {
		this.loggedInWorkstation = loggedInWorkstation;
	}

    @Column(name = "MACADDRESS")
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

    @Column(name = "MEMORYSIZE")
	public String getMemorySize() {
		return memorySize;
	}

	public void setMemorySize(String memorySize) {
		this.memorySize = memorySize;
	}

    @Column(name = "NCV")
	public String getNcv() {
		return ncv;
	}

	public void setNcv(String ncv) {
		this.ncv = ncv;
	}

    @Column(name = "NICTYPE")
	public String getNicType() {
		return nicType;
	}

	public void setNicType(String nictype) {
		this.nicType = nictype;
	}

    @Lob
    @Column(name = "OPERATOR")
	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

    @Column(name = "OSREVISION")
	public String getOsRevision() {
		return osRevision;
	}

	public void setOsRevision(String osRevision) {
		this.osRevision = osRevision;
	}

    @Column(name = "OSTYPE")
	public String getOsType() {
		return osType;
	}

	public void setOsType(String osType) {
		this.osType = osType;
	}

    @Column(name = "PROCESSORTYPE")
	public String getProcessorType() {
		return processorType;
	}

	public void setProcessorType(String processorType) {
		this.processorType = processorType;
	}

    @Column(name = "SERIALNUMBER")
	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

    @Column(name = "SUBNETADDRESS")
	public String getSubnetAddress() {
		return subnetAddress;
	}

	public void setSubnetAddress(String subnetAddress) {
		this.subnetAddress = subnetAddress;
	}

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TICKETID")
	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

    @Lob
    @Column(name = "USERHISTORY")
	public String getUserHistory() {
		return userHistory;
	}

	public void setUserHistory(String userHistory) {
		this.userHistory = userHistory;
	}

    @Column(name = "VIDEOTYPE")
	public String getVideoType() {
		return videoType;
	}

	public void setVideoType(String videoType) {
		this.videoType = videoType;
	}

    @Column(name = "WORKSTATION")
	public String getWorkstation() {
		return workstation;
	}

	public void setWorkstation(String workstation) {
		this.workstation = workstation;
	}

    @Column(name = "WSDN")
	public String getWsdn() {
		return wsdn;
	}

	public void setWsdn(String wsdn) {
		this.wsdn = wsdn;
	}

    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ZenInformation)) return false;

        ZenInformation that = (ZenInformation) o;

        if (assetTag != null ? !assetTag.equals(that.assetTag) : that.assetTag != null) return false;
        if (biosType != null ? !biosType.equals(that.biosType) : that.biosType != null) return false;
        if (computerModel != null ? !computerModel.equals(that.computerModel) : that.computerModel != null)
            return false;
        if (computerName != null ? !computerName.equals(that.computerName) : that.computerName != null) return false;
        if (computerType != null ? !computerType.equals(that.computerType) : that.computerType != null) return false;
        if (diskInfo != null ? !diskInfo.equals(that.diskInfo) : that.diskInfo != null) return false;
        if (dn != null ? !dn.equals(that.dn) : that.dn != null) return false;
        if (ipAddress != null ? !ipAddress.equals(that.ipAddress) : that.ipAddress != null) return false;
        if (lastUser != null ? !lastUser.equals(that.lastUser) : that.lastUser != null) return false;
        if (loggedInWorkstation != null ? !loggedInWorkstation.equals(that.loggedInWorkstation) : that.loggedInWorkstation != null)
            return false;
        if (macAddress != null ? !macAddress.equals(that.macAddress) : that.macAddress != null) return false;
        if (memorySize != null ? !memorySize.equals(that.memorySize) : that.memorySize != null) return false;
        if (modelNumber != null ? !modelNumber.equals(that.modelNumber) : that.modelNumber != null) return false;
        if (ncv != null ? !ncv.equals(that.ncv) : that.ncv != null) return false;
        if (nicType != null ? !nicType.equals(that.nicType) : that.nicType != null) return false;
        if (operator != null ? !operator.equals(that.operator) : that.operator != null) return false;
        if (osRevision != null ? !osRevision.equals(that.osRevision) : that.osRevision != null) return false;
        if (osType != null ? !osType.equals(that.osType) : that.osType != null) return false;
        if (processorType != null ? !processorType.equals(that.processorType) : that.processorType != null)
            return false;
        if (serialNumber != null ? !serialNumber.equals(that.serialNumber) : that.serialNumber != null) return false;
        if (subnetAddress != null ? !subnetAddress.equals(that.subnetAddress) : that.subnetAddress != null)
            return false;
        if (userHistory != null ? !userHistory.equals(that.userHistory) : that.userHistory != null) return false;
        if (videoType != null ? !videoType.equals(that.videoType) : that.videoType != null) return false;
        if (workstation != null ? !workstation.equals(that.workstation) : that.workstation != null) return false;
        if (wsdn != null ? !wsdn.equals(that.wsdn) : that.wsdn != null) return false;

        return true;
    }

    public int hashCode() {
        int result;
        result = (dn != null ? dn.hashCode() : 0);
        result = 31 * result + (workstation != null ? workstation.hashCode() : 0);
        result = 31 * result + (loggedInWorkstation != null ? loggedInWorkstation.hashCode() : 0);
        result = 31 * result + (wsdn != null ? wsdn.hashCode() : 0);
        result = 31 * result + (ipAddress != null ? ipAddress.hashCode() : 0);
        result = 31 * result + (subnetAddress != null ? subnetAddress.hashCode() : 0);
        result = 31 * result + (macAddress != null ? macAddress.hashCode() : 0);
        result = 31 * result + (diskInfo != null ? diskInfo.hashCode() : 0);
        result = 31 * result + (memorySize != null ? memorySize.hashCode() : 0);
        result = 31 * result + (processorType != null ? processorType.hashCode() : 0);
        result = 31 * result + (biosType != null ? biosType.hashCode() : 0);
        result = 31 * result + (videoType != null ? videoType.hashCode() : 0);
        result = 31 * result + (nicType != null ? nicType.hashCode() : 0);
        result = 31 * result + (ncv != null ? ncv.hashCode() : 0);
        result = 31 * result + (osRevision != null ? osRevision.hashCode() : 0);
        result = 31 * result + (osType != null ? osType.hashCode() : 0);
        result = 31 * result + (assetTag != null ? assetTag.hashCode() : 0);
        result = 31 * result + (serialNumber != null ? serialNumber.hashCode() : 0);
        result = 31 * result + (computerModel != null ? computerModel.hashCode() : 0);
        result = 31 * result + (computerType != null ? computerType.hashCode() : 0);
        result = 31 * result + (userHistory != null ? userHistory.hashCode() : 0);
        result = 31 * result + (lastUser != null ? lastUser.hashCode() : 0);
        result = 31 * result + (operator != null ? operator.hashCode() : 0);
        result = 31 * result + (computerName != null ? computerName.hashCode() : 0);
        result = 31 * result + (modelNumber != null ? modelNumber.hashCode() : 0);
        return result;
    }
}
