package net.grouplink.ehelpdesk.domain.asset;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * Date: Jul 6, 2007
 * Time: 7:48:10 AM
 */
@Entity
@Table(name = "ACCOUNTINGINFO")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class AccountingInfo implements Serializable {

    private Integer id;
    private String poNumber;
    private String accountNumber;
    private String accumulatedNumber;
    private String expenseNumber;
    private String acquisitionValue;
    private String leaseNumber;
    private Date leaseExpirationDate;
    private String leaseDuration;
    private String leaseFrequency;
    private String leaseFrequencyPrice;
    private Date warrantyDate;
    private Date disposalDate;
    private String disposalMethod;
    private String insuranceCategory;
    private String replacementValueCategory;
    private String replacementValue;
    private String maintenanceCost;
    private String depreciationMethod;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "foreign")
    @GenericGenerator(name = "foreign", strategy = "org.hibernate.id.ForeignGenerator", parameters = {@Parameter(name = "property", value = "asset")})
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "PONUMBER")
    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    @Column(name = "ACCOUNTNUMBER")
    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Column(name = "ACCUMULATEDNUMBER")
    public String getAccumulatedNumber() {
        return accumulatedNumber;
    }

    public void setAccumulatedNumber(String accumulatedNumber) {
        this.accumulatedNumber = accumulatedNumber;
    }

    @Column(name = "EXPENSENUMBER")
    public String getExpenseNumber() {
        return expenseNumber;
    }

    public void setExpenseNumber(String expenseNumber) {
        this.expenseNumber = expenseNumber;
    }

    @Column(name = "ACQUISITIONVALUE")
    public String getAcquisitionValue() {
        return acquisitionValue;
    }

    public void setAcquisitionValue(String acquisitionValue) {
        this.acquisitionValue = acquisitionValue;
    }

    @Column(name = "LEASENUMBER")
    public String getLeaseNumber() {
        return leaseNumber;
    }

    public void setLeaseNumber(String leaseNumber) {
        this.leaseNumber = leaseNumber;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LEASEEXPIRATIONDATE")
    public Date getLeaseExpirationDate() {
        return leaseExpirationDate;
    }

    public void setLeaseExpirationDate(Date leaseExpirationDate) {
        this.leaseExpirationDate = leaseExpirationDate;
    }

    @Column(name = "LEASEDURATION")
    public String getLeaseDuration() {
        return leaseDuration;
    }

    public void setLeaseDuration(String leaseDuration) {
        this.leaseDuration = leaseDuration;
    }

    @Column(name = "LEASEFREQUENCY")
    public String getLeaseFrequency() {
        return leaseFrequency;
    }

    public void setLeaseFrequency(String leaseFrequency) {
        this.leaseFrequency = leaseFrequency;
    }

    @Column(name = "LEASEFREQUENCYPRICE")
    public String getLeaseFrequencyPrice() {
        return leaseFrequencyPrice;
    }

    public void setLeaseFrequencyPrice(String leaseFrequencyPrice) {
        this.leaseFrequencyPrice = leaseFrequencyPrice;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "WARRANTYDATE")
    public Date getWarrantyDate() {
        return warrantyDate;
    }

    public void setWarrantyDate(Date warrantyDate) {
        this.warrantyDate = warrantyDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DISPOSALDATE")
    public Date getDisposalDate() {
        return disposalDate;
    }

    public void setDisposalDate(Date disposalDate) {
        this.disposalDate = disposalDate;
    }

    @Column(name = "DISPOSALMETHOD")
    public String getDisposalMethod() {
        return disposalMethod;
    }

    public void setDisposalMethod(String disposalMethod) {
        this.disposalMethod = disposalMethod;
    }

    @Column(name = "INSURANCECATEGORY")
    public String getInsuranceCategory() {
        return insuranceCategory;
    }

    public void setInsuranceCategory(String insuranceCategory) {
        this.insuranceCategory = insuranceCategory;
    }

    @Column(name = "REPLACEMENTVALUECATEGORY")
    public String getReplacementValueCategory() {
        return replacementValueCategory;
    }

    public void setReplacementValueCategory(String replacementValueCategory) {
        this.replacementValueCategory = replacementValueCategory;
    }

    @Column(name = "REPLACEMENTVALUE")
    public String getReplacementValue() {
        return replacementValue;
    }

    public void setReplacementValue(String replacementValue) {
        this.replacementValue = replacementValue;
    }

    @Column(name = "MAINTENANCECOST")
    public String getMaintenanceCost() {
        return maintenanceCost;
    }

    public void setMaintenanceCost(String maintenanceCost) {
        this.maintenanceCost = maintenanceCost;
    }

    @Column(name = "DEPRECIATIONMETHOD")
    public String getDepreciationMethod() {
        return depreciationMethod;
    }

    public void setDepreciationMethod(String depreciationMethod) {
        this.depreciationMethod = depreciationMethod;
    }
}
