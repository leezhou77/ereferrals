package net.grouplink.ehelpdesk.domain.mail;

import java.io.File;
import java.io.Serializable;

/**
 * User: jaymehafen
 * Date: Jan 24, 2008
 * Time: 11:57:18 AM
 */
public class EmailAttachment implements Serializable {
    private String filename;
    private File file;
    private String contentType;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}
