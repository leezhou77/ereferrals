package net.grouplink.ehelpdesk.domain.constraints;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.InvocationTargetException;

/**
 * author: zpearce
 * Date: 12/9/11
 * Time: 1:48 PM
 */
public class RequiredIfValidator implements ConstraintValidator<RequiredIf, Object> {
    private String property;
    private String dependentProperty;
    private boolean boolProperty;
    private Object dependentObject;

    public void initialize(RequiredIf constraintAnnotation) {
        this.property = constraintAnnotation.property();
        this.dependentProperty = constraintAnnotation.dependentProperty();
    }

    public boolean isValid(Object object, ConstraintValidatorContext context) {
        try {
            validateAndLoadProperties(object);
            if(boolProperty) {
                if(dependentObject instanceof String) {
                    String value = (String) dependentObject;
                    return StringUtils.isNotBlank(value);
                } else {
                    return dependentObject != null;
                }
            } else {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }

    private void validateAndLoadProperties(Object object) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        try {
            this.boolProperty = (Boolean) PropertyUtils.getSimpleProperty(object, property);
            this.dependentObject = PropertyUtils.getProperty(object, dependentProperty);
        } catch (ClassCastException e) {
            throw new IllegalArgumentException("Property \"" + property + "\" is not a boolean.");
        }
    }
}
