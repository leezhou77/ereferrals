package net.grouplink.ehelpdesk.domain;

import org.apache.commons.lang.StringUtils;

import java.io.Serializable;

/**
 * @author dgarcia
 * @version 1.0
 */
public class UserFilter implements Serializable {

    private String lastName;
    private String firstName;
    private String email;
    private String loginId;
    private Boolean active;
    private String role;
    private String group;

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof UserFilter && equals((UserFilter) obj));
    }

    public boolean equals(UserFilter cf) {
        if (cf == this) return true;

        // Check each field for equality
        boolean result = (StringUtils.isBlank(lastName) || lastName.equals(cf.getLastName()));
        if (result) result = (StringUtils.isBlank(firstName) || firstName.equals(cf.getFirstName()));
        if (result) result = (StringUtils.isBlank(email) || email.equals(cf.getEmail()));
        if (result) result = (StringUtils.isBlank(loginId) || loginId.equals(cf.getLoginId()));
        if (result) result = (active == null || active.equals(cf.getActive()));
        if (result) result = (StringUtils.isBlank(role) || role.equals(cf.getRole()));
        if (result) result = (StringUtils.isBlank(group) || group.equals(cf.getGroup()));

        return result;
    }

    @Override
    public int hashCode() {
        int hash = 17;
        hash = 37 * hash + (lastName == null ? 0 : lastName.hashCode());
        hash = 37 * hash + (firstName == null ? 0 : firstName.hashCode());
        hash = 37 * hash + (email == null ? 0 : email.hashCode());
        hash = 37 * hash + (loginId == null ? 0 : loginId.hashCode());
        hash = 37 * hash + (active == null ? 0 : active.hashCode());
        hash = 37 * hash + (role == null ? 0 : role.hashCode());
        hash = 37 * hash + (group == null ? 0 : group.hashCode());

        return hash;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }
}
