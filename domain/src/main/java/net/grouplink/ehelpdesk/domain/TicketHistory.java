package net.grouplink.ehelpdesk.domain;

import net.grouplink.ehelpdesk.domain.acl.SecurityLevel;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.apache.commons.lang.StringUtils;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Store;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Represents a <code>TicketHistory</code> for a <code>Ticket</code> in the eHelpDesk system.
 */
@Entity
@Table(name = "TICKETHISTORY")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TicketHistory implements Serializable, Comparable {

    public static final String TICKET_UPDATE = "ticket.update";
    public static final String TICKET_MASSASSIGNED = "ticket.massAssigned";
    public static final String TICKET_SCHEDULER_UPDATE = "ticket.scheduler.update";

    private Integer id;
    private Ticket ticket;
    private User user;
    private Date createdDate;
    private String subject;
    private String note;
    private Date startDate;
    private Date dueDate;
    private int commentType; // see net.grouplink.ehelpdesk.utils.Constants
    private String taskPriority;
//    private boolean allDayEvent;
    private AppointmentType apptType;
    private String recipients;
    private String cc;
    private String bc;
    private Boolean active;
    private String place;
    private Boolean textFormat = false;

    private Set<Attachment> attachments = new HashSet<Attachment>();

    /* Used for spring binding. Runtime type is MultipartFile. */
    private List<Object> attchmnt = new ArrayList<Object>();

    private Boolean notifyUser;
    private Boolean notifyTech;

    private Integer durationAmount;
    private String durationUnit; // Minutes, Hours, Days

    private SecurityLevel securityLevel;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Transient
    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    @Transient
    public Boolean getNotifyUser() {
        return notifyUser;
    }

    public void setNotifyUser(Boolean notifyUser) {
        this.notifyUser = notifyUser;
    }

    @Transient
    public Boolean getNotifyTech() {
        return notifyTech;
    }

    public void setNotifyTech(Boolean notifyTech) {
        this.notifyTech = notifyTech;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TICKETID")
    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USERID")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATEDDATE")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Field(index = Index.TOKENIZED, store = Store.NO)
    @Column(name = "SUBJECT")
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Field(index = Index.TOKENIZED, store = Store.NO)
    @Lob
    @Column(name = "NOTE", length = 2147483647)
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Transient
    public String getNoteText() {
        if (StringUtils.isBlank(note)) {
            return note;
        }

        if (isTextFormat()) {
            return note;
        } else {
            String text = note.replaceAll("<.*?>", "");
            text = text.replaceAll("&.*?;", " ");
            text = text.replaceAll("      ", "\n");
            text = text.trim();
            return text;
        }
	}

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "STARTDATE")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DUEDATE")
    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    @Column(name = "COMMENTTYPE")
    public int getCommentType() {
        return commentType;
    }

    public void setCommentType(int type) {
        this.commentType = type;
    }

    @Column(name = "TASKPRIORITY")
    public String getTaskPriority() {
        return taskPriority;
    }

    public void setTaskPriority(String taskPriority) {
        this.taskPriority = taskPriority;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "APPTTYPE")
    public AppointmentType getApptType() {
        return apptType;
    }

    public void setApptType(AppointmentType apptType) {
        this.apptType = apptType;
    }

    @Column(name = "ACTIVE")
    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @ManyToMany
    @Cascade({org.hibernate.annotations.CascadeType.ALL, org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    @JoinTable(name = "TICKETHISTORYATTACHMENTS",
         joinColumns = @JoinColumn(name = "TICKETHISTID"),
         inverseJoinColumns = @JoinColumn(name = "ATTACHMENTID"))
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    public Set<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(Set<Attachment> attachments) {
        this.attachments = attachments;
    }

    /**
     * Used for spring binding. Runtime type is MultipartFile.
     * @return List of MultipartFile
     */
    @Transient
    public List<Object> getAttchmnt() {
        return attchmnt;
    }

    public void setAttchmnt(List<Object> attchmnt) {
        this.attchmnt = attchmnt;
    }

    @Column(name = "RECIPIENTS")
    public String getRecipients() {
        return recipients;
    }

    public void setRecipients(String recipients) {
        this.recipients = recipients;
    }

    @Column(name = "PLACE")
    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    @Transient
	public String getBc() {
        return bc;
    }

    public void setBc(String bccs) {
        this.bc = bccs;
    }

    public int compareTo(Object o) {
//        return ((TicketHistory) o).getId().compareTo(id);
        return ((TicketHistory) o).getCreatedDate().compareTo(createdDate);
    }

    @Column(name = "TEXTFORMAT")
    public Boolean isTextFormat() {
        if(textFormat == null){
            setTextFormat(false);
        }
        
        return textFormat;
    }

    public void setTextFormat(Boolean textFormat) {
        this.textFormat = textFormat != null && textFormat;
    }

    @Transient
    public Integer getDurationAmount() {
        return durationAmount;
    }

    public void setDurationAmount(Integer durationAmount) {
        this.durationAmount = durationAmount;
    }

    @Transient
    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    @ManyToOne
    @JoinColumn(name = "SECURITYLEVELID")
    public SecurityLevel getSecurityLevel() {
        return securityLevel;
    }

    public void setSecurityLevel(SecurityLevel securityLevel) {
        this.securityLevel = securityLevel;
    }
}
