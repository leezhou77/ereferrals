package net.grouplink.ehelpdesk.domain;

import net.grouplink.ehelpdesk.domain.constraints.AtLeastOne;
import net.grouplink.ehelpdesk.domain.constraints.RequiredIf;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.ScriptAssert;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.ManyToMany;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.validation.constraints.Digits;
import java.io.Serializable;
import java.util.Set;
import java.util.HashSet;

/**
 * @author mrollins
 */
@Entity
@Table(name = "SCHEDULESTATUS")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AtLeastOne(properties = "changeStatusAction, changePriorityAction, sendMail")
@RequiredIf.List({
    @RequiredIf(property = "changeStatusAction", dependentProperty = "changeStatus", message = "{RequiredIf.schedule.changeStatusAction}"),
    @RequiredIf(property = "changePriorityAction", dependentProperty = "changePriority", message = "{RequiredIf.schedule.changePriorityAction}")
})
@ScriptAssert.List({
    @ScriptAssert(lang = "javascript", alias = "_", script = "(_.sendMail && (_.mailAssignedTo || _.mailContact || _.mailCC.length() > 0)) || !_.sendMail", message = "{ScriptAssert.schedule.recipientRequired}"),
    @ScriptAssert(lang = "javascript", alias = "_", script = "_.sendMail && isNaN(_.actionFrequencyDigit) ? false : true", message = "{ScriptAssert.schedule.frequencyNumber}")
})
public class ScheduleStatus implements Serializable {

    private Integer id;
    private String name;
    private Group group;
    private Category category;
    private CategoryOption categoryOption;
    private Status status;
    private TicketPriority priority;
    private String  elapsedTimeDigit;
    private String elapsedTimeUnit;
    private boolean fromModified;
    private boolean changeStatusAction;
    private boolean changePriorityAction;
    private boolean sendMail;
    private boolean mailAssignedTo;
    private boolean mailContact;
    private String mailCC;
    private String mailBC;
    private String mailSubject;
    private String mailNote;
    private Status changeStatus;
    private TicketPriority changePriority;
    private boolean running;
    private String actionFrequencyDigit;
    private String actionFrequencyUnit;
    private Boolean actionFrequencyOneTime;
    private String mailNoteText;
    private Set<Status> statuses = new HashSet<Status>();
    private Set<TicketPriority> priorities = new HashSet<TicketPriority>();
    private RecurrenceSchedule recurrenceSchedule;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GROUPID")
    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    /**
     * Returns the Status.
     * @return the Status
     * @deprecated replaced by getStatuses
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUSID")
    public Status getStatus() {
        return status;
    }

    /**
     * Sets the Status
     * @param status the Status
     * @deprecated replaced by setStatuses
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * Gets the TicketPriority
     * @return the TicketPriority
     * @deprecated replaced by getPriorities
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRIORITYID")
    public TicketPriority getPriority() {
        return priority;
    }

    /**
     * Sets the TicketPriority
     * @param priority the TicketPriority
     * @deprecated replaced by setPriorities
     */
    public void setPriority(TicketPriority priority) {
        this.priority = priority;
    }

    @Column(name = "SENDMAIL")
    public boolean isSendMail() {
        return sendMail;
    }

    public void setSendMail(boolean sendMail) {
        this.sendMail = sendMail;
    }

    @Column(name = "MAILASSIGNEDTO")
    public boolean isMailAssignedTo() {
        return mailAssignedTo;
    }

    public void setMailAssignedTo(boolean mailAssignedTo) {
        this.mailAssignedTo = mailAssignedTo;
    }

    @Column(name = "MAILCONTACT")
    public boolean isMailContact() {
        return mailContact;
    }

    public void setMailContact(boolean mailContact) {
        this.mailContact = mailContact;
    }

    @Column(name = "MAILCC")
    public String getMailCC() {
        return mailCC;
    }

    public void setMailCC(String mailCC) {
        this.mailCC = mailCC;
    }

    @Column(name = "MAILBC")
    public String getMailBC() {
        return mailBC;
    }

    public void setMailBC(String mailBC) {
        this.mailBC = mailBC;
    }

    @Column(name = "MAILSUBJECT")
    public String getMailSubject() {
        return mailSubject;
    }

    public void setMailSubject(String mailSubject) {
        this.mailSubject = mailSubject;
    }

    @Column(name = "MAILNOTE")
    public String getMailNote() {
        return mailNote;
    }

    public void setMailNote(String mailNote) {
        this.mailNote = mailNote;
    }

    @Lob
    @Column(name = "MAILNOTETEXT", length = 10240)
    public String getMailNoteText() {
        return mailNoteText;
    }

    public void setMailNoteText(String mailNoteText) {
        this.mailNoteText = mailNoteText;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CHANGESTATUSID")
    public Status getChangeStatus() {
        return changeStatus;
    }

    public void setChangeStatus(Status changeStatus) {
        this.changeStatus = changeStatus;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CHANGEPRIORITYID")
    public TicketPriority getChangePriority() {
        return changePriority;
    }

    public void setChangePriority(TicketPriority changePriority) {
        this.changePriority = changePriority;
    }

    @Column(name = "RUNNING")
    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    @Column(name = "NAME")
    @NotEmpty
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "ELAPSEDTIMEDIGIT")
    @Digits(integer = 5, fraction = 0)
    public String getElapsedTimeDigit() {
        return elapsedTimeDigit;
    }

    public void setElapsedTimeDigit(String elapsedTimeDigit) {
        this.elapsedTimeDigit = elapsedTimeDigit;
    }

    @Column(name = "ELAPSEDTIMEUNIT")
    public String getElapsedTimeUnit() {
        return elapsedTimeUnit;
    }

    public void setElapsedTimeUnit(String elapsedTimeUnit) {
        this.elapsedTimeUnit = elapsedTimeUnit;
    }

    @Column(name = "ACTIONFREQUENCYDIGIT")
    public String getActionFrequencyDigit() {
        return actionFrequencyDigit;
    }

    public void setActionFrequencyDigit(String actionFrequencyDigit) {
        this.actionFrequencyDigit = actionFrequencyDigit;
    }

    @Column(name = "ACTIONFREQUENCYUNIT")
    public String getActionFrequencyUnit() {
        return actionFrequencyUnit;
    }

    public void setActionFrequencyUnit(String actionFrequencyUnit) {
        this.actionFrequencyUnit = actionFrequencyUnit;
    }

    @Column(name = "CHANGESTATUSACTION")
    public boolean isChangeStatusAction() {
        return changeStatusAction;
    }

    public void setChangeStatusAction(boolean changeStatusAction) {
        this.changeStatusAction = changeStatusAction;
    }

    @Column(name = "CHANGEPRIORITYACTION")
    public boolean isChangePriorityAction() {
        return changePriorityAction;
    }

    public void setChangePriorityAction(boolean changePriorityAction) {
        this.changePriorityAction = changePriorityAction;
    }

    @Column(name = "FROMMODIFIED")
    public boolean isFromModified() {
        return fromModified;
    }

    public void setFromModified(boolean fromModified) {
        this.fromModified = fromModified;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CATEGORYID")
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CATEGORYOPTIONID")
    public CategoryOption getCategoryOption() {
        return categoryOption;
    }

    public void setCategoryOption(CategoryOption categoryOption) {
        this.categoryOption = categoryOption;
    }

    @Column(name = "ACTIONFREQUENCYONETIME")
    public Boolean getActionFrequencyOneTime() {
        if (actionFrequencyOneTime == null) return Boolean.FALSE;
        return actionFrequencyOneTime;
    }

    public void setActionFrequencyOneTime(Boolean actionFrequencyOneTime) {
        this.actionFrequencyOneTime = actionFrequencyOneTime;
    }

    @ManyToMany
    @JoinTable(name = "SCHEDULESTATUSSTATUSES",
            joinColumns = @JoinColumn(name = "SCHEDULESTATUSID"),
            inverseJoinColumns = @JoinColumn(name = "STATUSID"))
    public Set<Status> getStatuses() {
        // check pre-9.0 status property
        if (status != null) {
            statuses.add(status);
            status = null;
        }

        return statuses;
    }

    public void setStatuses(Set<Status> statuses) {
        this.statuses = statuses;
    }

    @ManyToMany
    @JoinTable(name = "SCHEDULESTATUSPRIORITIES",
            joinColumns = @JoinColumn(name = "SCHEDULESTATUSID"),
            inverseJoinColumns = @JoinColumn(name = "PRIORITYID"))
    public Set<TicketPriority> getPriorities() {
        // check pre-9.0 priority property
        if (priority != null) {
            priorities.add(priority);
            priority = null;
        }

        return priorities;
    }

    public void setPriorities(Set<TicketPriority> priorities) {
        this.priorities = priorities;
    }


    @OneToOne(mappedBy = "scheduleStatus", fetch = FetchType.LAZY)
    @Cascade(CascadeType.ALL)
    public RecurrenceSchedule getRecurrenceSchedule() {
        return recurrenceSchedule;
    }

    public void setRecurrenceSchedule(RecurrenceSchedule recurrenceSchedule) {
        this.recurrenceSchedule = recurrenceSchedule;
    }
}
