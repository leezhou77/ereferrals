package net.grouplink.ehelpdesk.domain.constraints;

import org.apache.commons.beanutils.PropertyUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * author: zpearce
 * Date: 12/9/11
 * Time: 10:45 AM
 */
public class AtLeastOneValidator implements ConstraintValidator<AtLeastOne, Object> {
    private List<String> properties;
    private List<Boolean> values;

    public void initialize(AtLeastOne constraintAnnotation) {
        properties = new ArrayList<String>();
        for (String property : constraintAnnotation.properties().split(",")) {
            properties.add(property.trim());
        }

        values = new ArrayList<Boolean>();
    }

    public boolean isValid(Object object, ConstraintValidatorContext context) {
        try {
            validateAndLoadProperties(object);

            for(boolean b : values) {
                if(b) {
                    return true;
                }
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    private void validateAndLoadProperties(Object object) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        for(String property : properties) {
            try {
                boolean value = (Boolean) PropertyUtils.getSimpleProperty(object, property);
                values.add(value);
            } catch (ClassCastException e) {
                throw new IllegalArgumentException("Property \"" + property + "\" is not a boolean.");
            }
        }
    }
}
